PROMPT *** log_pub ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package log_pub is
/* ******************************************************
*	Package:     pag_pub
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	051018 | Frode Klevstul
*			- Package created
****************************************************** */
	procedure run;
	procedure viewLogHits
	(
		  p_table_name			in		log_hits.table_name%type
		, p_foreign_key_fk		in		log_hits.foreign_key_fk%type
	);
	function viewLogHits
	(
		  p_table_name			in		log_hits.table_name%type
		, p_foreign_key_fk		in		log_hits.foreign_key_fk%type
	) return clob;

end;
/
show errors;





-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body log_pub is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'log_pub';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  13.10.2005
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        viewLogHits
-- what:        view hits in log table
-- author:      Frode Klevstul
-- start date:  17.10.2005
---------------------------------------------------------
procedure viewLogHits(p_table_name in log_hits.table_name%type, p_foreign_key_fk in log_hits.foreign_key_fk%type) is begin
	ext_lob.pri(log_pub.viewLogHits(p_table_name, p_foreign_key_fk));
end;

function viewLogHits
(
	  p_table_name			in		log_hits.table_name%type
	, p_foreign_key_fk		in		log_hits.foreign_key_fk%type
) return clob
is
begin
declare
	c_procedure			constant mod_procedure.name%type	:= 'viewLogHits';

	c_tex_heading		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'heading');
	c_tex_noHits		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'noHits');

	cursor	cur_log_hits is
	select	hits
	from	log_hits
	where	table_name		= p_table_name
	and		foreign_key_fk	= p_foreign_key_fk;

	v_html				clob;
	v_clob				clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_table_name='||p_table_name||'��p_foreign_key_fk='||p_foreign_key_fk);
	ext_lob.ini(v_clob);

	v_html := v_html||''||htm.bPage(g_package, c_procedure)||chr(13);
	v_html := v_html||'<center>'||chr(13);
	v_html := v_html||''||htm.bBox(c_tex_heading)||chr(13);
	v_html := v_html||''||htm.bTable||chr(13);
	v_html := v_html||'<tr><td>'||chr(13);

	for r_cursor in cur_log_hits
	loop
		v_html := v_html||'<tr><td>'||c_tex_noHits||': '||r_cursor.hits||'</td></tr>'||chr(13);
	end loop;

	v_html := v_html||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||''||htm.eBox||chr(13);
	v_html := v_html||'</center>'||chr(13);
	v_html := v_html||''||htm.ePage||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		return null;
end;
end viewLogHits;


-- ******************************************** --
end;
/
show errors;
