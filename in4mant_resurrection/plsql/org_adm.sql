PROMPT *** org_adm ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package org_adm is
/* ******************************************************
*	Package:     pag_pub
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	051018 | Frode Klevstul
*			- Package created
****************************************************** */
	type parameter_arr 			is table of varchar2(4000) index by binary_integer;
	empty		 				parameter_arr;

	procedure run;
	procedure viewOrg;
	function viewOrg
		return clob;
	procedure updateGeneralInformation
	(
		  p_action				in varchar2										default null
		, p_general_information	in varchar2										default null
	);
	function updateGeneralInformation
	(
		  p_action				in varchar2										default null
		, p_general_information	in varchar2										default null
	) return clob;
	procedure updateOrgName;
	function updateOrgName
		return clob;
	procedure updateAddress
	(
		  p_action				in varchar2										default null
		, p_street				in add_address.street%type						default null
		, p_zip					in add_address.zip%type							default null
		, p_landline			in add_address.landline%type					default null
		, p_fax					in add_address.fax%type							default null
		, p_email				in add_address.email%type						default null
		, p_url					in add_address.url%type							default null
	);
	function updateAddress
	(
		  p_action				in varchar2										default null
		, p_street				in add_address.street%type						default null
		, p_zip					in add_address.zip%type							default null
		, p_landline			in add_address.landline%type					default null
		, p_fax					in add_address.fax%type							default null
		, p_email				in add_address.email%type						default null
		, p_url					in add_address.url%type							default null
	) return clob;
	procedure updateListOrg
	(
		  p_action				in varchar2										default null
		, p_type				in varchar2										default null
		, p_lists				in parameter_arr								default empty
	);
	function updateListOrg
	(
		  p_action				in varchar2										default null
		, p_type				in varchar2										default null
		, p_lists				in parameter_arr								default empty
	) return clob;
	procedure updateDetailsInfo
	(
		  p_action				in varchar2										default null
		, p_day_1				in varchar2										default null
		, p_day_2				in varchar2										default null
		, p_day_3				in varchar2										default null
		, p_day_4				in varchar2										default null
		, p_day_5				in varchar2										default null
		, p_day_6				in varchar2										default null
		, p_day_7				in varchar2										default null
		, p_rights				in parameter_arr								default empty
		, p_age_limit			in org_organisation.age_limit%type				default null
		, p_age_description		in org_organisation.age_description%type		default null
	);
	function updateDetailsInfo
	(
		  p_action				in varchar2										default null
		, p_day_1				in varchar2										default null
		, p_day_2				in varchar2										default null
		, p_day_3				in varchar2										default null
		, p_day_4				in varchar2										default null
		, p_day_5				in varchar2										default null
		, p_day_6				in varchar2										default null
		, p_day_7				in varchar2										default null
		, p_rights				in parameter_arr								default empty
		, p_age_limit			in org_organisation.age_limit%type				default null
		, p_age_description		in org_organisation.age_description%type		default null
	) return clob;
	procedure updatePicture
	(
		  p_action				in varchar2										default null
		, p_type				in varchar2										default null
	);
	function updatePicture
	(
		  p_action				in varchar2										default null
		, p_type				in varchar2										default null
	) return clob;
	procedure updateOrgValues
	(
		  p_action				in varchar2										default null
		, p_org_value_type_pk	in parameter_arr								default empty
		, p_numValue			in parameter_arr								default empty
		, p_charValue			in parameter_arr								default empty
	);
	function updateOrgValues
	(
		  p_action				in varchar2										default null
		, p_org_value_type_pk	in parameter_arr								default empty
		, p_numValue			in parameter_arr								default empty
		, p_charValue			in parameter_arr								default empty
	) return clob;

end;
/
show errors;




-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body org_adm is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'org_adm';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  18.10.2005
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        viewOrg
-- what:        view organisation page in admin mode
-- author:      Frode Klevstul
-- start date:  18.10.2005
---------------------------------------------------------
procedure viewOrg is begin
	ext_lob.pri(org_adm.viewOrg);
end;

function viewOrg
	return clob
is
begin
declare
	c_procedure			constant mod_procedure.name%type	:= 'viewOrg';

	c_id				constant ses_session.id%type 						:= ses_lib.getId;
	c_organisation_pk	constant org_organisation.org_organisation_pk%type	:= ses_lib.getOrgPk(c_id);

	v_html				clob;
	v_clob				clob;

begin
	ses_lib.ini(g_package, c_procedure, null);
	ext_lob.ini(v_clob);

	use_lib.loggedInCheck(g_package, c_procedure, null);

	-- initate organisation
	org_lib.initialiseOrganisation(c_organisation_pk);

	-- view org in admin mode
	v_html := org_pub.viewOrg(c_organisation_pk, true);

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		return null;
end;
end viewOrg;


---------------------------------------------------------
-- name:        updateGeneralInformation
-- what:        update description
-- author:      Frode Klevstul
-- start date:  20.10.2005
---------------------------------------------------------
procedure updateGeneralInformation(p_action in varchar2 default null, p_general_information in varchar2 default null) is begin
	ext_lob.pri(org_adm.updateGeneralInformation(p_action, p_general_information));
end;

function updateGeneralInformation
(
	  p_action				in varchar2										default null
	, p_general_information	in varchar2										default null
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type	:= 'updateGeneralInformation';

	c_id						constant ses_session.id%type 						:= ses_lib.getId;
	c_organisation_pk			constant org_organisation.org_organisation_pk%type	:= ses_lib.getOrgPk(c_id);

	c_text_generalInfo			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'generalInfo');
	c_text_updateGeneralInfo	constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'updateGeneralInfo');
	c_text_doNotUpdate			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'doNotUpdate');
	c_text_updating				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'updating');
	c_text_wrongLength			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'wrongLength');
	c_text_textWasCut			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'textWasCut');
	c_text_browserNotFullySup	constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'browserNotFullySup');

	v_general_information		org_organisation.general_information%type			:= substr(p_general_information, 1, 4000);

	cursor	cur_org_organisation is
	select	general_information
	from	org_organisation
	where	org_organisation_pk = c_organisation_pk;

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_action='||p_action);
	ext_lob.ini(v_clob);

	use_lib.loggedInCheck(g_package, c_procedure, null);

	-- ---------------------------
	-- display update form
	-- ---------------------------
	if (p_action is null and c_organisation_pk is not null) then
		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundAdminBox')||'<tr><td>'||chr(13);

		v_html := v_html||'<script type="text/javascript" src="/FCKeditor/fckeditor.js"></script>'||chr(13);
		v_html := v_html||''||htm.bForm('org_adm.updateGeneralInformation')||chr(13);
		v_html := v_html||'<input type="hidden" name="p_action" value="update" />'||chr(13);
		v_html := v_html||''||htm.bBox(c_text_generalInfo)||chr(13);

		v_html := v_html||'

		<script type="text/javascript">
			var agt=navigator.userAgent.toLowerCase();
		
			if ( agt.indexOf("opera") > -1 ) {
				alert('''||c_text_browserNotFullySup||'''); }
		</script>

		<script type="text/javascript">
			<!--
			var oFCKeditor = new FCKeditor( ''p_general_information'' ) ;
			oFCKeditor.BasePath = ''/FCKeditor/'' ;
			oFCKeditor.Width = 535 ;
			oFCKeditor.Height = 400 ;
			oFCKeditor.ToolbarSet = "i4_generalInfo" ;
		';

		for r_cursor in cur_org_organisation
		loop
			v_html := v_html||'oFCKeditor.Value = '''||htm_lib.HTMLFix(r_cursor.general_information)||''' ;';
		end loop;

		v_html := v_html||'
			oFCKeditor.Create() ;
			//-->
			</script>
		';

		v_html := v_html||''||htm.bTable||chr(13);
		v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||htm.submitLink(c_text_updateGeneralInfo)||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||htm.historyBack(c_text_doNotUpdate)||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);
		v_html := v_html||''||htm.eForm||chr(13);

		v_html := v_html||'</td></tr>'||htm.eTable||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

	-- ---------------------------
	-- update the database
	-- ---------------------------
	elsif (p_action = 'update') then

		if (c_organisation_pk is not null) then
			update	org_organisation
			set		general_information = v_general_information
				  , date_update 		= sysdate
			where	org_organisation_pk = c_organisation_pk;
			commit;
		end if;

		if ( length(p_general_information) > 4000 ) then
			v_html := v_html||htm.jumpTo('org_adm.viewOrg', 5, c_text_textWasCut||' '||length(p_general_information));
		else
			v_html := v_html||htm.jumpTo('org_adm.viewOrg', 1, c_text_updating);
		end if;

	else

		v_html := v_html||htm.jumpTo('pag_pub.userLogin?p_url='||g_package||'.'||c_procedure||'?'||owa_util.get_cgi_env('QUERY_STRING'), 1, '...');

	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		return null;
end;
end updateGeneralInformation;


---------------------------------------------------------
-- name:        updateOrgName
-- what:        update organisation name
-- author:      Frode Klevstul
-- start date:  27.10.2005
---------------------------------------------------------
procedure updateOrgName is begin
	ext_lob.pri(org_adm.updateOrgName);
end;

function updateOrgName
	return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type	:= 'updateOrgName';

	c_text_updateOrgName		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'updateOrgName');
	c_text_contactUsToUpdate	constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'contactUsToUpdate');
	c_text_contactInformant		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'contactInformant');
	c_text_goBack				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'goBack');

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, null);
	ext_lob.ini(v_clob);

	use_lib.loggedInCheck(g_package, c_procedure, null);

	v_html := v_html||''||htm.bPage(g_package, c_procedure, 'admin')||chr(13);
	v_html := v_html||'<center>'||chr(13);
	v_html := v_html||''||htm.bTable('surroundAdminBox')||'<tr><td>'||chr(13);

	v_html := v_html||''||htm.bBox(c_text_updateOrgName)||chr(13);
	v_html := v_html||''||htm.bTable||chr(13);
	v_html := v_html||'<tr><td>'||c_text_contactUsToUpdate||' '||htm.link(c_text_contactInformant,'pag_pub.contactPage','_self')||'</td></tr>'||chr(13);
	v_html := v_html||'<tr><td style="padding-left: 20px;">'||htm.historyBack(c_text_goBack)||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||''||htm.eBox||chr(13);

	v_html := v_html||'</td></tr>'||htm.eTable||chr(13);
	v_html := v_html||'</center>'||chr(13);
	v_html := v_html||''||htm.ePage||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		return null;
end;
end updateOrgName;


---------------------------------------------------------
-- name:        updateAddress
-- what:        update address
-- author:      Frode Klevstul
-- start date:  27.10.2005
---------------------------------------------------------
procedure updateAddress(p_action in varchar2 default null, p_street in add_address.street%type default null, p_zip in add_address.zip%type default null, p_landline in add_address.landline%type default null, p_fax in add_address.fax%type default null, p_email in add_address.email%type default null, p_url in add_address.url%type default null) is begin
	ext_lob.pri(org_adm.updateAddress(p_action, p_street, p_zip, p_landline, p_fax, p_email, p_url));
end;

function updateAddress
(
	  p_action				in varchar2										default null
	, p_street				in add_address.street%type						default null
	, p_zip					in add_address.zip%type							default null
	, p_landline			in add_address.landline%type					default null
	, p_fax					in add_address.fax%type							default null
	, p_email				in add_address.email%type						default null
	, p_url					in add_address.url%type							default null
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type	:= 'updateAddress';

	c_id						constant ses_session.id%type 						:= ses_lib.getId;
	c_organisation_pk			constant org_organisation.org_organisation_pk%type	:= ses_lib.getOrgPk(c_id);

	c_text_updateContactInfo	constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'updateContactInfo');
	c_text_updateAddressInfo	constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'updateAddressInfo');
	c_text_doNotUpdate			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'doNotUpdate');
	c_text_updating				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'updating');
	c_text_street				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'street');
	c_text_zip					constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'zip');
	c_text_landline				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'landline');
	c_text_fax					constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'fax');
	c_text_email				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'email');
	c_text_url					constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'url');
	c_text_wrongZip				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'wrongZip');
	c_text_wrongLandline		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'wrongLandline');
	c_text_wrongFax				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'wrongFax');
	c_text_wrongEmail			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'wrongEmail');

	cursor	cur_org_add is
	select	street, zip, landline, fax, email, url, add_address_pk
	from	org_organisation o, add_address a
	where	org_organisation_pk = c_organisation_pk
	and		add_address_fk = add_address_pk;

	cursor	cur_add_address(b_add_address_pk add_address.add_address_pk%type) is
	select	longitude, latitude
	from	add_address
	where	add_address_pk = b_add_address_pk;

	v_address_pk				add_address.add_address_pk%type;
	v_array						parameter_arr;
	v_longitude					add_address.longitude%type;
	v_latitude					add_address.latitude%type;

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_action='||p_action||'いp_street='||p_street||'いp_zip='||p_zip||'いp_landline='||p_landline||'いp_fax='||p_fax||'いp_email='||p_email||'いp_url='||p_url);
	ext_lob.ini(v_clob);

	use_lib.loggedInCheck(g_package, c_procedure, null);

	-- ---------------------------
	-- display update form
	-- ---------------------------
	if (p_action is null and c_organisation_pk is not null) then

		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundAdminBox')||'<tr><td>'||chr(13);

		-- ---------------
		-- javascript
		-- ---------------
		v_html := v_html||'
			<script type="text/javascript">
				function submitForm(){
					var p_street	= document.getElementById(''p_street'');
					var p_zip		= document.getElementById(''p_zip'');
					var p_landline	= document.getElementById(''p_landline'');
					var p_fax		= document.getElementById(''p_fax'');
					var p_email		= document.getElementById(''p_email'');
					var p_url		= document.getElementById(''p_url'');

					if ( !(p_zip.value.match(/^\d{4}$/)) && p_zip.value.length > 0 ){
						alert("'||c_text_wrongZip||'");
						return;
					}

					if ( !(p_landline.value.match(/^\d{8}$/)) && p_landline.value.length > 0 ){
						alert("'||c_text_wrongLandline||'");
						return;
					}

					if ( !(p_fax.value.match(/^\d{8}$/)) && p_fax.value.length > 0 ){
						alert("'||c_text_wrongFax||'");
						return;
					}

					if ( !validEmail(p_email.value) && p_email.value.length > 0 ){
						alert("'||c_text_wrongEmail||'");
						return;
					}

				 	p_url.value	= p_url.value.replace(''http://'','''');

					document.getElementById(''form'').submit();
				}
			</script>
		';

		v_html := v_html||''||htm.bBox(c_text_updateContactInfo)||chr(13);
		v_html := v_html||''||htm.bForm('org_adm.updateAddress')||chr(13);
		v_html := v_html||'<input type="hidden" name="p_action" value="update">'||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);

		for r_cursor in cur_org_add
		loop
			v_html := v_html||'<tr><td width="20%">'||c_text_street||'</td><td><input type="text" name="p_street" id="p_street" value="'||r_cursor.street||'" maxlength="100" size="50" /></td></tr>'||chr(13);
			v_html := v_html||'<tr><td>'||c_text_zip||'</td><td><input type="text" name="p_zip" id="p_zip" value="'||r_cursor.zip||'" maxlength="4" size="50" /></td></tr>'||chr(13);
			v_html := v_html||'<tr><td>'||c_text_landline||'</td><td><input type="text" name="p_landline" id="p_landline" value="'||replace(r_cursor.landline, ' ')||'" maxlength="8" size="50" /></td></tr>'||chr(13);
			v_html := v_html||'<tr><td>'||c_text_fax||'</td><td><input type="text" name="p_fax" id="p_fax" value="'||replace(r_cursor.fax, ' ')||'" maxlength="8" size="50" /></td></tr>'||chr(13);
			v_html := v_html||'<tr><td>'||c_text_email||'</td><td><input type="text" name="p_email" id="p_email" value="'||r_cursor.email||'" maxlength="128" size="50" /></td></tr>'||chr(13);
			v_html := v_html||'<tr><td>'||c_text_url||'</td><td><input type="text" name="p_url" id="p_url" value="'||r_cursor.url||'" maxlength="128" size="50" /></td></tr>'||chr(13);
		end loop;

		v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||htm.submitLink(c_text_updateAddressInfo, 'form', 'submitForm()')||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||htm.historyBack(c_text_doNotUpdate)||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eForm||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);

		v_html := v_html||'</td></tr>'||htm.eTable||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

	-- ---------------------------
	-- update the database
	-- ---------------------------
	elsif (p_action = 'update') then

		if (c_organisation_pk is not null) then
			for r_cursor in cur_org_add
			loop
				v_address_pk := r_cursor.add_address_pk;
			end loop;

			if (v_address_pk is not null) then
				v_array(1) := htm_lib.HTMLEncode(p_street);

				for r_cursor in cur_add_address(v_address_pk)
				loop
					v_longitude	:= r_cursor.longitude;
					v_latitude	:= r_cursor.latitude;
				end loop;

				-- insert old long / lat into his_history
				his_lib.addEntry('add_address', 'longitude', v_longitude);
				his_lib.addEntry('add_address', 'latitude', v_latitude);

				update	add_address
				set		street		= v_array(1)
					  , zip			= p_zip
					  , landline	= p_landline
					  , fax			= p_fax
					  , email		= p_email
					  , url			= p_url
--					  , longitude	= null
--					  , latitude	= null
					  , date_update	= sysdate
				where	add_address_pk = v_address_pk;
				
				update	org_organisation
				set		date_update = sysdate
				where	org_organisation_pk = c_organisation_pk;
				
				commit;
			end if;

		end if;

		v_html := v_html||htm.jumpTo('org_adm.viewOrg', 1, c_text_updating);

	else

		v_html := v_html||htm.jumpTo('pag_pub.userLogin?p_url='||g_package||'.'||c_procedure||'?'||owa_util.get_cgi_env('QUERY_STRING'), 1, '...');

	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		return null;
end;
end updateAddress;


---------------------------------------------------------
-- name:        updateListOrg
-- what:        update organisation name
-- author:      Frode Klevstul
-- start date:  28.10.2005
---------------------------------------------------------
procedure updateListOrg(p_action in varchar2 default null, p_type in varchar2 default null, p_lists in parameter_arr default empty) is begin
	ext_lob.pri(org_adm.updateListOrg(p_action, p_type, p_lists));
end;

function updateListOrg
(
	  p_action				in varchar2										default null
	, p_type				in varchar2										default null
	, p_lists				in parameter_arr								default empty

) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type	:= 'updateListOrg';

	c_id						constant ses_session.id%type 						:= ses_lib.getId;
	c_organisation_pk			constant org_organisation.org_organisation_pk%type	:= ses_lib.getOrgPk(c_id);
	c_service_pk				constant ser_service.ser_service_pk%type			:= ses_lib.getSer(c_id);

	c_text_updateListOrg		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'updateListOrg');
	c_text_doNotUpdate			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'doNotUpdate');
	c_text_updating				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'updating');
	c_text_contactUsToUpdate	constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'contactUsToUpdate');
	c_tex_typeOfPlace			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'typeOfPlace');
	c_tex_otherLists			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'otherLists');

	v_index						number;
	v_i							number								:= 0;

	type varray_lists 			is table of lis_list.name%type 		index by lis_list.name%type;
	v_lists						varray_lists;

	cursor	cur_lis_list is
	select	name
	from	lis_list
	where	lis_type_fk = (select lis_type_pk from lis_type where name = 'org_type' and ser_service_fk = c_service_pk)
	order by lis_list_pk;

	cursor	cur_lis_list_other is
	select	name
	from	lis_list
	where	lis_type_fk in (select lis_type_pk from lis_type where name not like 'org_type' and ser_service_fk = c_service_pk)
	order by lis_list_pk;

	cursor	cur_lis_org is
	select	name
	from	lis_list, lis_org
	where	lis_list_fk_pk = lis_list_pk
	and		org_organisation_fk_pk = c_organisation_pk;

	v_firstCharsNow				varchar2(8)							:= '0';
	v_firstCharsOld				varchar2(8)							:= '0';

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_action='||p_action||'いp_type='||p_type);
	ext_lob.ini(v_clob);

	use_lib.loggedInCheck(g_package, c_procedure, null);

	-- ---------------------------
	-- display update form
	-- ---------------------------
	if (p_action is null and c_organisation_pk is not null) then

		-- ---------------------------------------------------
		-- initiate the associative array with all lists
		-- this to avoid "no data found" error
		-- ---------------------------------------------------
		if (p_type = 'org_type') then
			for r_cursor in cur_lis_list
			loop
				v_lists(r_cursor.name) :=  'false';
			end loop;
		elsif (p_type = 'other') then
			for r_cursor in cur_lis_list_other
			loop
				v_lists(r_cursor.name) :=  'false';
			end loop;
		end if;

		-- ---------------------------------------------------
		-- select out the lists the organisation is on
		-- ---------------------------------------------------
		for r_cursor in cur_lis_org
		loop
			v_lists(r_cursor.name) :=  'true';
		end loop;

		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundAdminBox')||'<tr><td>'||chr(13);

		if (p_type = 'org_type') then
			v_html := v_html||''||htm.bBox(c_tex_typeOfPlace)||chr(13);
		elsif (p_type = 'other') then
			v_html := v_html||''||htm.bBox(c_tex_otherLists)||chr(13);
		end if;

		v_html := v_html||''||htm.bForm('org_adm.updateListOrg')||chr(13);
		v_html := v_html||'<input type="hidden" name="p_action" value="update" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_type" value="'||p_type||'" />'||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);
		v_html := v_html||'<tr><td>'||chr(13);

		if (p_type = 'org_type') then
			for r_cursor in cur_lis_list
			loop
				if ( mod(v_i, 2) = 0 ) then
					v_html := v_html||'<tr>'||chr(13);
				else
					v_html := v_html||'<tr class="alternativeLine">'||chr(13);
				end if;

				v_html := v_html||'<td>'||tex_lib.get('db', 'lis_list', r_cursor.name)||'</td>'||chr(13);
				if (v_lists(r_cursor.name) = 'true') then
					v_html := v_html||'<td><input type="checkbox" name="p_lists" value="'||r_cursor.name||'" checked /></td>'||chr(13);
				else
					v_html := v_html||'<td><input type="checkbox" name="p_lists" value="'||r_cursor.name||'" /></td>'||chr(13);
				end if;
				v_html := v_html||'</tr>'||chr(13);

				v_i := v_i + 1;
			end loop;
		elsif (p_type = 'other') then
			for r_cursor in cur_lis_list_other
			loop

				-- -------------------------------------------------------
				-- write out heading for each different list type
				-- -------------------------------------------------------
				if (r_cursor.name is not null) then
					v_firstCharsNow := substr(r_cursor.name,1,(instr(r_cursor.name,'_')-1));
					v_firstCharsNow := substr(v_firstCharsNow,1,(length(v_firstCharsNow)-2));
				end if;

				if (v_firstCharsOld != v_firstCharsNow) then
					v_firstCharsOld := v_firstCharsNow;
					v_html := v_html||'<tr><td colspan="2"><br />'||tex_lib.get('db', 'lis_list', v_firstCharsOld)||'</td></tr>'||chr(13);
				end if;

				-- -------------------------------------------------------
				-- write out name of list and the checkbox
				-- -------------------------------------------------------
				if ( mod(v_i, 2) = 0 ) then
					v_html := v_html||'<tr>'||chr(13);
				else
					v_html := v_html||'<tr class="alternativeLine">'||chr(13);
				end if;

				v_html := v_html||'<td>'||tex_lib.get('db', 'lis_list', r_cursor.name)||'</td>';

				if (v_lists(r_cursor.name) = 'true') then
					v_html := v_html||'<td><input type="checkbox" name="p_lists" value="'||r_cursor.name||'" checked /></td>'||chr(13);
				else
					v_html := v_html||'<td><input type="checkbox" name="p_lists" value="'||r_cursor.name||'" /></td>'||chr(13);
				end if;
				v_html := v_html||'</tr>'||chr(13);

				v_i := v_i + 1;
			end loop;
		end if;

 		v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||htm.submitLink(c_text_updateListOrg)||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||htm.historyBack(c_text_doNotUpdate)||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eForm||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);

		v_html := v_html||'</td></tr>'||htm.eTable||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

	-- ---------------------------
	-- update the database
	-- ---------------------------
	elsif (p_action = 'update') then

		if (c_organisation_pk is not null) then

			-- --------------------------------------------------
			-- first delete all existing lists of correct type
			-- --------------------------------------------------
			if (p_type = 'org_type') then
	         	delete from lis_org
	         	where 	org_organisation_fk_pk = c_organisation_pk
	         	and		lis_list_fk_pk in (
					select	lis_list_pk
					from	lis_list
	         		where	lis_type_fk = (
	         			select	lis_type_pk
	         			from	lis_type
	         			where	name = 'org_type'
	         			and		ser_service_fk = (select ser_service_pk from ser_service where name = 'uteliv')
	         		)
	         	);
			elsif (p_type = 'other') then
	         	delete from lis_org
	         	where 	org_organisation_fk_pk = c_organisation_pk
	         	and		lis_list_fk_pk in (
					select	lis_list_pk
					from	lis_list
	         		where	lis_type_fk in (
	         			select	lis_type_pk
	         			from	lis_type
	         			where	name not like 'org_type'
	         			and		ser_service_fk = (select ser_service_pk from ser_service where name = 'uteliv')
	         		)
	         	);
			end if;

			for v_index in 1 .. p_lists.count
			loop
				if ( p_lists(v_index) is not null ) then
	         		insert into lis_org
	         		(lis_list_fk_pk, org_organisation_fk_pk)
	         		values (
	         			(
	         				select	lis_list_pk
	         				from	lis_list
	         				where	name = p_lists(v_index)
	         			)
	         		  , c_organisation_pk
					);
					commit;
				end if;
			end loop;

			update	org_organisation
			set		date_update = sysdate
			where	org_organisation_pk = c_organisation_pk;
		end if;

		v_html := v_html||htm.jumpTo('org_adm.viewOrg', 1, c_text_updating);

	else

		v_html := v_html||htm.jumpTo('pag_pub.userLogin?p_url='||g_package||'.'||c_procedure||'?'||owa_util.get_cgi_env('QUERY_STRING'), 1, '...');

	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		return null;
end;
end updateListOrg;


---------------------------------------------------------
-- name:        updateDetailsInfo
-- what:        update opening hours, rights and age limit
-- author:      Frode Klevstul
-- start date:  28.10.2005
---------------------------------------------------------
procedure updateDetailsInfo(p_action in varchar2 default null, p_day_1 in varchar2 default null, p_day_2 in varchar2 default null, p_day_3 in varchar2 default null, p_day_4 in varchar2 default null, p_day_5 in varchar2 default null, p_day_6 in varchar2 default null, p_day_7 in varchar2 default null, p_rights in parameter_arr default empty, p_age_limit in org_organisation.age_limit%type default null, p_age_description in org_organisation.age_description%type default null) is begin
	ext_lob.pri(org_adm.updateDetailsInfo(p_action, p_day_1, p_day_2, p_day_3, p_day_4, p_day_5, p_day_6, p_day_7, p_rights, p_age_limit, p_age_description));
end;

function updateDetailsInfo
(
	  p_action				in varchar2										default null
	, p_day_1				in varchar2										default null
	, p_day_2				in varchar2										default null
	, p_day_3				in varchar2										default null
	, p_day_4				in varchar2										default null
	, p_day_5				in varchar2										default null
	, p_day_6				in varchar2										default null
	, p_day_7				in varchar2										default null
	, p_rights				in parameter_arr								default empty
	, p_age_limit			in org_organisation.age_limit%type				default null
	, p_age_description		in org_organisation.age_description%type		default null
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type	:= 'updateDetailsInfo';

	c_id						constant ses_session.id%type 						:= ses_lib.getId;
	c_organisation_pk			constant org_organisation.org_organisation_pk%type	:= ses_lib.getOrgPk(c_id);

	c_dateFormat_hhmi			constant sys_parameter.value%type 	:= sys_lib.getParameter('dateFormat_hhmi');

	c_text_updateDetailsInfo	constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'updateDetailsInfo');
	c_text_openingHours			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'openingHours');
	c_text_doNotUpdate			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'doNotUpdate');
	c_text_updating				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'updating');
	c_tex_monday				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'monday');
	c_tex_tuesday				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'tuesday');
	c_tex_wednesday				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'wednesday');
	c_tex_thursday				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'thursday');
	c_tex_friday				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'friday');
	c_tex_saturday				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'saturday');
	c_tex_sunday				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'sunday');

	c_tex_fromTime				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'fromTime');
	c_tex_toTime				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'toTime');
	c_tex_addTime				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'addTime');
	c_tex_chosenTimes			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'chosenTimes');
	c_tex_deleteTime			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'deleteTime');
	c_tex_closed				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'closed');
	c_tex_wrongDateFormat		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'wrongDateFormat');
	c_tex_deleteTimes			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'deleteTimes');
	c_tex_closeDay				constant sys_parameter.value%type 	:= tex_lib.get(g_package, c_procedure, 'closeDay');
	c_tex_dayIsClosed			constant sys_parameter.value%type 	:= tex_lib.get(g_package, c_procedure, 'dayIsClosed');
	c_tex_clock					constant sys_parameter.value%type 	:= tex_lib.get(g_package, c_procedure, 'clock');

	c_text_rights				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'rights');
	c_text_choseRights			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'choseRights');

	c_text_age					constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'age');
	c_text_choseAge				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'choseAge');
	c_text_choseAgeDescription	constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'choseAgeDescription');

	type varray_hours 			is table of org_hours.monday%type 	index by tex_text.string%type;
	v_openingHours				varray_hours;

	type varray_rights 			is table of org_rights.name%type 	index by org_rights.name%type;
	v_rights					varray_rights;

	v_cursor_column				tex_text.string%type;
	v_tmp						tex_text.string%type;
	v_age_limit					org_organisation.age_limit%type;
	v_day_no					number;
	v_index						number;
	v_array						parameter_arr;

	cursor	cur_org_hours is
	select	monday, tuesday, wednesday, thursday, friday, saturday, sunday
	from	org_hours
	where	org_organisation_fk_pk = c_organisation_pk;

	cursor	cur_org_rights is
	select	name
	from	org_rights
	order by description;

	cursor	cur_org_organisation_rights is
	select	name
	from	org_rights
	where	org_rights_pk in (
		select	org_rights_fk_pk
		from	org_organisation_rights
		where	org_organisation_fk_pk = c_organisation_pk
	);

	cursor	cur_org_organisation is
	select	age_limit, age_description
	from	org_organisation
	where	org_organisation_pk = c_organisation_pk;

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_action='||p_action);
	ext_lob.ini(v_clob);

	use_lib.loggedInCheck(g_package, c_procedure, null);

	-- ---------------------------
	-- display update form
	-- ---------------------------
	if (p_action is null and c_organisation_pk is not null) then

		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'admin')||chr(13);

		-- ----------------------------------
		-- JavaScript functions needed
		-- ----------------------------------
		v_html := v_html||'
			<script type="text/javascript" language="JavaScript">
				function addTime(dayNo) {
					if (document.form) {

						var p_clockFrom	= document.getElementById(''p_clockFrom_''+dayNo);
						var p_clockTo	= document.getElementById(''p_clockTo_''+dayNo);
						var p_allHours	= document.getElementById(''p_allHours_''+dayNo);
						var p_closed	= document.getElementById(''p_closed_''+dayNo);

						// check format of input time
						if (p_clockFrom.value.length == 0 || !verifyTime(p_clockFrom.value)){
					 		alert('''||c_tex_wrongDateFormat||': '||c_dateFormat_hhmi||''');
							return;
						} else if (p_clockTo.value.length == 0 || !verifyTime(p_clockTo.value)){
					 		alert('''||c_tex_wrongDateFormat||': '||c_dateFormat_hhmi||''');
							return;
						}

						// set help variables
						var from 		= verifyTime(p_clockFrom.value);
						var to 			= verifyTime(p_clockTo.value);

						// set new all hours (chosen times)
						if (p_allHours) {
							// check if chosen day is "closed"
							if ( p_closed.checked ){
								alert("'||c_tex_dayIsClosed||'");
								return;
							}

							// -------------------------
							// set new time
							// -------------------------
						 	var prevTimes = p_allHours.value;

							if ( prevTimes.length==0 ){
								p_allHours.value = from+'' - ''+to;
							} else {
								p_allHours.value = prevTimes+'', ''+from+'' - ''+to;
							}
							p_clockFrom.value = '''';
							p_clockTo.value = '''';
						}
					}
				}

				function deleteTime(dayNo) {
						if (document.form) {

							var p_allHours	= document.getElementById(''p_allHours_''+dayNo);
							var p_closed	= document.getElementById(''p_closed_''+dayNo);

							if (p_allHours) {

								// check if chosen day is "closed"
								if ( p_closed.checked ){
									alert("'||c_tex_dayIsClosed||'");
									return;
								}

								user_input = confirm("'||c_tex_deleteTimes||'");
								if (user_input == true){
									p_allHours.value = '''';
								}
							}
						}
				}

				function closeDay(dayNo) {
						if (document.form) {

							var p_allHours	= document.getElementById(''p_allHours_''+dayNo);
							var p_closed	= document.getElementById(''p_closed_''+dayNo);

							if (p_allHours) {
								// already checked (closed)?
								if (!(p_closed.checked)){
									setFieldToBeOpened(dayNo);
								} else {
									// ask user to confirm
									user_input = confirm("'||c_tex_closeDay||'");
									if (user_input == true){
										p_allHours.value = '''';
										setFieldToBeClosed(dayNo);
									} else {
										p_closed.checked = false;
									}
								}
							}
						}
				}

				function setClosedOnLoad(dayNo) {

					var p_allHours	= document.getElementById(''p_allHours_''+dayNo);
					var p_closed	= document.getElementById(''p_closed_''+dayNo);

					if (p_allHours) {
						setFieldToBeClosed(dayNo);
						p_closed.checked = true;
					}
				}

				function setFieldToBeOpened(dayNo) {

					var p_allHours	= document.getElementById(''p_allHours_''+dayNo);

					p_allHours.style.backgroundColor	= ''white'';
					p_allHours.style.color				= ''black'';
					p_allHours.value					= '''';
				}

				function setFieldToBeClosed(dayNo) {

					var p_allHours	= document.getElementById(''p_allHours_''+dayNo);

					p_allHours.style.backgroundColor	= ''black'';
					p_allHours.style.color				= ''white'';
					p_allHours.value					= '''||c_tex_closed||''';
				}

				function submitForm(){
					var p_allHours_1	= document.getElementById(''p_allHours_1'');
					var p_allHours_2	= document.getElementById(''p_allHours_2'');
					var p_allHours_3	= document.getElementById(''p_allHours_3'');
					var p_allHours_4	= document.getElementById(''p_allHours_4'');
					var p_allHours_5	= document.getElementById(''p_allHours_5'');
					var p_allHours_6	= document.getElementById(''p_allHours_6'');
					var p_allHours_7	= document.getElementById(''p_allHours_7'');

					var p_day_1			= document.getElementById(''p_day_1'');
					var p_day_2			= document.getElementById(''p_day_2'');
					var p_day_3			= document.getElementById(''p_day_3'');
					var p_day_4			= document.getElementById(''p_day_4'');
					var p_day_5			= document.getElementById(''p_day_5'');
					var p_day_6			= document.getElementById(''p_day_6'');
					var p_day_7			= document.getElementById(''p_day_7'');

					/*
					if(p_allHours_1.value.length == 0){
						p_allHours_1.value = '''||c_tex_closed||''';
					}
					if(p_allHours_2.value.length == 0){
						p_allHours_2.value = '''||c_tex_closed||''';
					}
					if(p_allHours_3.value.length == 0){
						p_allHours_3.value = '''||c_tex_closed||''';
					}
					if(p_allHours_4.value.length == 0){
						p_allHours_4.value = '''||c_tex_closed||''';
					}
					if(p_allHours_5.value.length == 0){
						p_allHours_5.value = '''||c_tex_closed||''';
					}
					if(p_allHours_6.value.length == 0){
						p_allHours_6.value = '''||c_tex_closed||''';
					}
					if(p_allHours_7.value.length == 0){
						p_allHours_7.value = '''||c_tex_closed||''';
					}
					*/

					p_day_1.value		= p_allHours_1.value;
					p_day_2.value		= p_allHours_2.value;
					p_day_3.value		= p_allHours_3.value;
					p_day_4.value		= p_allHours_4.value;
					p_day_5.value		= p_allHours_5.value;
					p_day_6.value		= p_allHours_6.value;
					p_day_7.value		= p_allHours_7.value;

					formToSubmit.submit();
				}

				function setCorrectAge(age) {
					var p_age_limit	= document.getElementById(''p_age_limit'');
					p_age_limit.selectedIndex = (age-15);
				}

			</script>
		';

		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundElements')||chr(13);
		v_html := v_html||'<tr><td class="surroundElements">'||chr(13);

		-- ------------------------------------------------------------------------------------
		--
		-- opening hours
		--
		-- ------------------------------------------------------------------------------------
		v_html := v_html||''||htm.bBox(c_text_openingHours)||chr(13);
		v_html := v_html||''||htm.bForm('noActionNeeded', 'form')||chr(13);
		v_html := v_html||'<table border="1" width="100%">'||chr(13);
		v_html := v_html||'
			<tr>
				<td>&nbsp;</td>
				<td>'||c_tex_fromTime||' ('||c_dateFormat_hhmi||')</td>
				<td>'||c_tex_toTime||' ('||c_dateFormat_hhmi||')</td>
				<td>'||c_tex_addTime||'</td>
				<td>'||c_tex_chosenTimes||'</td>
				<td>'||c_tex_deleteTime||'</td>
				<td>'||c_tex_closed||'</td>
			</tr>
		'||chr(13);

		-- -----
		-- add HTML code to the clob
		-- -----
		ext_lob.add(v_clob, v_html);

		for r_cursor in cur_org_hours
		loop
			-- note have to add number in front to get the order right when listing out
			v_openingHours('1: '||c_tex_monday)		:= r_cursor.monday;
			v_openingHours('2: '||c_tex_tuesday)	:= r_cursor.tuesday;
			v_openingHours('3: '||c_tex_wednesday)	:= r_cursor.wednesday;
			v_openingHours('4: '||c_tex_thursday)	:= r_cursor.thursday;
			v_openingHours('5: '||c_tex_friday)		:= r_cursor.friday;
			v_openingHours('6: '||c_tex_saturday)	:= r_cursor.saturday;
			v_openingHours('7: '||c_tex_sunday)		:= r_cursor.sunday;
		end loop;

		v_cursor_column := v_openingHours.first;
		v_day_no		:= 1;
		loop
			exit when not v_openingHours.exists(v_cursor_column);

			v_tmp := substr(v_cursor_column, 3, length(v_cursor_column));

			v_html := v_html||'
				<tr>
					<td>'||v_tmp||':</td>
					<td><input type="text" name="p_clockFrom_'||v_day_no||'" id="p_clockFrom_'||v_day_no||'" size="5" />'||htm.popUp(c_tex_clock, 'htm_lib.clock?p_form=form&amp;p_field=p_clockFrom_'||v_day_no,'350', '50')||'</td>
					<td><input type="text" name="p_clockTo_'||v_day_no||'" id="p_clockTo_'||v_day_no||'" size="5" />'||htm.popUp(c_tex_clock, 'htm_lib.clock?p_form=form&amp;p_field=p_clockTo_'||v_day_no,'350', '50')||'</td>
					<td><a href="JavaScript:addTime('||v_day_no||')">&gt;&gt;&gt;</a></td>
					<td><input type="text" name="p_allHours_'||v_day_no||'" id="p_allHours_'||v_day_no||'" value="'||v_openingHours(v_cursor_column)||'" size="50" maxlength="128" /></td>
					<td><font size="3"><a href="JavaScript:deleteTime('||v_day_no||')">X</a></font></td>
					<td><input type="checkbox" name="p_closed_'||v_day_no||'" id="p_closed_'||v_day_no||'" onClick="JavaScript:closeDay('||v_day_no||')" /></td>
				</tr>
				';

				--if( (v_openingHours(v_cursor_column) is null) or (v_openingHours(v_cursor_column) = c_tex_closed) ) then
				--	v_html := v_html||'
				--		<script type="text/javascript">
				--			setClosedOnLoad('||v_day_no||');
				--		</script>
				--	';
				--end if;


			-- -----
			-- add HTML code to the clob (and reset v_html)
			-- -----
			ext_lob.add(v_clob, v_html);

			v_cursor_column := v_openingHours.next(v_cursor_column);

			v_day_no := v_day_no +1;
		end loop;

		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eForm||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);


		-- surroundElements table
		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		-- /surroundElements table


		-- ------------------------------------------
		-- start a new form that will be submitted
		-- ------------------------------------------
		v_html := v_html||''||htm.bForm('org_adm.updateDetailsInfo', 'formToSubmit')||chr(13);
		v_html := v_html||'
			<input type="hidden" name="p_action" value="update" />

			<input type="hidden" name="p_day_1" id="p_day_1" value="" />
			<input type="hidden" name="p_day_2" id="p_day_2" value="" />
			<input type="hidden" name="p_day_3" id="p_day_3" value="" />
			<input type="hidden" name="p_day_4" id="p_day_4" value="" />
			<input type="hidden" name="p_day_5" id="p_day_5" value="" />
			<input type="hidden" name="p_day_6" id="p_day_6" value="" />
			<input type="hidden" name="p_day_7" id="p_day_7" value="" />
		';


		-- surroundElements table
		v_html := v_html||''||htm.bTable('surroundElements')||chr(13);
		v_html := v_html||'<tr><td class="surroundElements">'||chr(13);
		-- /surroundElements table


		-- ------------------------------------------------------------------------------------
		--
		-- org rights
		--
		-- ------------------------------------------------------------------------------------
		v_html := v_html||''||htm.bBox(c_text_rights)||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);
		v_html := v_html||'
			<tr>
				<td colspan="2">'||c_text_choseRights||':</td>
			</tr>'||chr(13);

		-- --------------------------------------------------------------
		-- initiate the associative array to avoid "no data found" error
		-- --------------------------------------------------------------
		for r_cursor in cur_org_rights
		loop
			v_rights(r_cursor.name) :=  'false';
		end loop;

		-- ---------------------------------------------------
		-- select out the rigths the organisation has got
		-- ---------------------------------------------------
		for r_cursor in cur_org_organisation_rights
		loop
			v_rights(r_cursor.name) :=  'true';
		end loop;

		-- -----------------------------------
		-- go through all rights and print
		-- -----------------------------------
		for r_cursor in cur_org_rights
		loop
			v_html := v_html||'
				<tr>
					<td width="25%">'||tex_lib.get('db', 'org_rights', r_cursor.name)||':</td>'||chr(13);
			if (v_rights(r_cursor.name) = 'true') then
				v_html := v_html||'
					<td><input type="checkbox" name="p_rights" value="'||r_cursor.name||'" checked /></td>'||chr(13);
			else
				v_html := v_html||'
					<td><input type="checkbox" name="p_rights" value="'||r_cursor.name||'" /></td>'||chr(13);
			end if;
			v_html := v_html||'
				</tr>'||chr(13);
		end loop;

		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);


		-- surroundElements table
		v_html := v_html||'</td><td class="surroundElements">'||chr(13);
		-- /surroundElements table


		-- ------------------------------------------------------------------------------------
		--
		-- age limit
		--
		-- ------------------------------------------------------------------------------------
		v_html := v_html||''||htm.bBox(c_text_age)||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);

		for r_cursor in cur_org_organisation
		loop
			v_html := v_html||'
				<tr>
					<td>'||c_text_choseAge||':</td>
					<td>
						<select name="p_age_limit" id="p_age_limit">
						<option value="">-</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>
						<option value="24">24</option>
						<option value="25">25</option>
						<option value="26">26</option>
						<option value="27">27</option>
						<option value="28">28</option>
						<option value="29">29</option>
						<option value="30">30</option>
						<option value="31">31</option>
						<option value="32">32</option>
						<option value="33">33</option>
						<option value="34">34</option>
						<option value="35">35</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>'||c_text_choseAgeDescription||':</td>
					<td>
						<input type="text" name="p_age_description" value="'||r_cursor.age_description||'" size="50" />
					</td>
				</tr>
			';
			v_age_limit := r_cursor.age_limit;
		end loop;

		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);

		v_html := v_html||'
			<script type="text/javascript">
				setCorrectAge('||v_age_limit||');
			</script>
		';

		-- surroundElements table
		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td class="surroundElements" colspan="2">'||chr(13);
		-- /surroundElements table


		-- --------------------------------
		-- buttons
		-- --------------------------------
		v_html := v_html||'<br />'||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);

		v_html := v_html||'<tr><td colspan="7" style="padding-left: 20px;">'||htm.submitLink(c_text_updateDetailsInfo, 'formToSubmit', 'submitForm()')||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td colspan="7" style="padding-left: 20px;">'||htm.historyBack(c_text_doNotUpdate)||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);

		-- surroundElements table
		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		-- /surroundElements table
		v_html := v_html||''||htm.eForm||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

		-- -----
		-- add HTML code to the clob (and reset v_html)
		-- -----
		ext_lob.add(v_clob, v_html);

	-- ---------------------------
	-- update the database
	-- ---------------------------
	elsif (p_action = 'update') then

		if (c_organisation_pk is not null) then

			-- --------
			-- hours
			-- --------
			update	org_hours
			set		monday		= p_day_1
				  , tuesday		= p_day_2
				  , wednesday	= p_day_3
				  , thursday	= p_day_4
				  , friday		= p_day_5
				  , saturday	= p_day_6
				  , sunday		= p_day_7
			where	org_organisation_fk_pk = c_organisation_pk;

			if (sql%notfound) then
				insert into org_hours
				       (org_organisation_fk_pk, monday, tuesday, wednesday, thursday, friday, saturday, sunday)
				values
				(
					  c_organisation_pk
					, p_day_1
					, p_day_2
					, p_day_3
					, p_day_4
					, p_day_5
					, p_day_6
					, p_day_7
				);
			end if;
			commit;

			-- --------
			-- rights
			-- --------
         	delete from org_organisation_rights
         	where 	org_organisation_fk_pk = c_organisation_pk;

			for v_index in 1 .. p_rights.count
			loop
				if ( p_rights(v_index) is not null ) then
	         		insert into org_organisation_rights
	         		(org_organisation_fk_pk, org_rights_fk_pk)
	         		values (
	         			c_organisation_pk,
	         			(
	         				select	org_rights_pk
	         				from	org_rights
	         				where	name = p_rights(v_index)
	         			)
					);
					commit;
				end if;
			end loop;

			-- --------
			-- age
			-- --------
			v_array(1) := htm_lib.HTMLEncode(p_age_description);
			
			update	org_organisation
			set		age_limit			= p_age_limit
				  , age_description		= v_array(1)
				  , date_update			= sysdate
			where	org_organisation_pk	= c_organisation_pk;
			commit;

		end if;

		v_html := v_html||htm.jumpTo('org_adm.viewOrg', 1, c_text_updating);

	else
		v_html := v_html||htm.jumpTo('pag_pub.userLogin?p_url='||g_package||'.'||c_procedure||'?'||owa_util.get_cgi_env('QUERY_STRING'), 1, '...');

	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		return null;
end;
end updateDetailsInfo;


---------------------------------------------------------
-- name:        updatePicture
-- what:        update logo
-- author:      Frode Klevstul
-- start date:  03.11.2005
---------------------------------------------------------
procedure updatePicture(p_action in varchar2 default null, p_type in varchar2 default null) is begin
	ext_lob.pri(org_adm.updatePicture(p_action, p_type));
end;

function updatePicture
(
	  p_action				in varchar2										default null
	, p_type				in varchar2										default null
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type	:= 'updatePicture';

	c_id						constant ses_session.id%type 						:= ses_lib.getId;
	c_organisation_pk			constant org_organisation.org_organisation_pk%type	:= ses_lib.getOrgPk(c_id);
	c_directory_path			constant sys.all_directories.directory_path%type	:= org_lib.getDirectoryPath(c_organisation_pk, true);

	c_dir_img_org				constant sys_parameter.value%type 	:= sys_lib.getParameter('dir_img_org')||'_'||c_organisation_pk;
	c_upl_pic_formats			constant sys_parameter.value%type 	:= sys_lib.getParameter('upl_pic_formats');
	c_org_logo					constant sys_parameter.value%type 	:= sys_lib.getParameter('org_logo');
	v_org_logo					sys_parameter.value%type			:= c_organisation_pk||''||c_org_logo;
	c_max_logo_size				constant sys_parameter.value%type 	:= sys_lib.getParameter('max_logo_size');
	c_max_logo_width			constant sys_parameter.value%type 	:= sys_lib.getParameter('max_logo_width');
	c_max_logo_height			constant sys_parameter.value%type 	:= sys_lib.getParameter('max_logo_height');

	c_org_mpho					constant sys_parameter.value%type 	:= sys_lib.getParameter('org_mpho');
	v_org_mpho					sys_parameter.value%type			:= c_organisation_pk||''||c_org_mpho;
	c_max_mpho_size				constant sys_parameter.value%type 	:= sys_lib.getParameter('max_mpho_size');
	c_max_mpho_width			constant sys_parameter.value%type 	:= sys_lib.getParameter('max_mpho_width');
	c_max_mpho_height			constant sys_parameter.value%type 	:= sys_lib.getParameter('max_mpho_height');

	c_text_logo					constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'logo');
	c_text_mainphoto			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'mainphoto');
	c_text_updatePicture		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'updatePicture');
	c_text_doNotUpdate			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'doNotUpdate');
	c_text_updating				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'updating');
	c_text_pictureUpdated		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'pictureUpdated');
	c_text_uploadFailed			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'uploadFailed');
	c_text_deleteImage			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'deleteImage');
	c_text_pictureDeleted		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'pictureDeleted');
	c_text_emptyPath			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'emptyPath');
	c_text_suppFileFormats		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'suppFileFormats');

	e_stop						exception;

	v_box_heading				tex_text.string%type;
	v_max_width					sys_parameter.value%type;
	v_max_height				sys_parameter.value%type;
	v_max_size					sys_parameter.value%type;

	v_filename					org_organisation.path_logo%type;
	v_random					binary_integer						:= dbms_random.random();
	v_log_upload_pk				log_upload.log_upload_pk%type;

	cursor	cur_org_organisation is
	select	path_mainphoto, path_logo
	from	org_organisation
	where	org_organisation_pk = c_organisation_pk;

	v_sql_stmt					varchar2(2048);
	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_action='||p_action||'いp_type='||p_type);
	ext_lob.ini(v_clob);

	use_lib.loggedInCheck(p_url => g_package||'.'||c_procedure||'?p_type='||p_type);

	-- ---------------------------------------------
	-- initialize values based on type of upload
	-- ---------------------------------------------
	if (p_type = 'logo') then
		v_box_heading	:= c_text_logo;
		v_max_width		:= c_max_logo_width;
		v_max_height	:= c_max_logo_height;
		v_max_size		:= c_max_logo_size;
	elsif (p_type = 'mainphoto') then
		v_box_heading 	:= c_text_mainphoto;
		v_max_width		:= c_max_mpho_width;
		v_max_height	:= c_max_mpho_height;
		v_max_size		:= c_max_mpho_size;
	end if;

	-- ---------------------------
	-- display update form
	-- ---------------------------
	if ( (p_action = 'displayForm' or p_action is null) and c_organisation_pk is not null) then
		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundAdminBox')||'<tr><td>'||chr(13);

		v_html := v_html||'
			<script type="text/javascript">
				function deleteImage(){
					this.location.href = '''||g_package||'.'||c_procedure||'?p_action=delete&p_type='||p_type||''';
				}

				function checkFields_uploadForm(){
					var p_file_1	= document.getElementById(''p_file_1'');

					if(p_file_1.value.length == 0){
						alert('''||c_text_emptyPath||''');
						return false;
					} else {
						return true;
					}
				}
			</SCRIPT>
		';

		v_html := v_html||''||htm.bBox(v_box_heading)||chr(13);
		v_html := v_html||''||htm.bForm('htm_lib.upload" enctype="multipart/form-data', 'upload_form')||chr(13);

		v_html := v_html||'<input type="hidden" name="p_type" value="image" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_url_success" value="'||g_package||'.'||c_procedure||'?p_action=update_'||to_char(sysdate, 'SSSSS')||'-'||v_random||'&amp;p_type='||p_type||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_url_failure" value="'||g_package||'.'||c_procedure||'?p_action=failed&amp;p_type='||p_type||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_trg_directory" value="'||c_dir_img_org||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_max_width" value="'||v_max_width||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_max_height" value="'||v_max_height||'" />'||chr(13);

		if (p_type = 'logo') then
			v_html := v_html||'<input type="hidden" name="p_trg_filename" value="'||v_org_logo||'" />'||chr(13);
			v_html := v_html||'<input type="hidden" name="p_max_file_size" value="'||v_max_size||'" />'||chr(13);

			for r_cursor in cur_org_organisation
			loop
				v_html := v_html||'<img src="'||c_directory_path||'/'||r_cursor.path_logo||'" alt="" /><input type="checkbox" onClick="JavaScript:deleteImage()" />'||c_text_deleteImage||chr(13);
			end loop;

		elsif (p_type = 'mainphoto') then
			v_html := v_html||'<input type="hidden" name="p_trg_filename" value="'||v_org_mpho||'" />'||chr(13);
			v_html := v_html||'<input type="hidden" name="p_max_file_size" value="'||c_max_mpho_size||'" />'||chr(13);

			for r_cursor in cur_org_organisation
			loop
				v_html := v_html||'<img src="'||c_directory_path||'/'||r_cursor.path_mainphoto||'" alt="" /><input type="checkbox" onClick="JavaScript:deleteImage()" />'||c_text_deleteImage||chr(13);
			end loop;
		end if;

		v_html := v_html||''||htm.bTable||chr(13);
		v_html := v_html||'<tr><td>&nbsp;</td></tr>'||chr(13);
		v_html := v_html||'<tr><td>'||c_text_suppFileFormats||' '||c_upl_pic_formats||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td><input type="file" name="p_file_1" id="p_file_1" size="60" /></td></tr>'||chr(13);
		v_html := v_html||'<tr><td>&nbsp;</td></tr>'||chr(13);
		v_html := v_html||'<tr><td style="padding-left: 20px;">'||htm.submitLink(c_text_updatePicture, 'upload_form', 'if%20(checkFields_uploadForm())%20document.upload_form.submit()')||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td style="padding-left: 20px;">'||htm.historyBack(c_text_doNotUpdate)||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eForm||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);

		v_html := v_html||'</td></tr>'||htm.eTable||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

	-- ---------------------------
	-- update the database
	-- ---------------------------
	elsif (p_action like 'update%' and c_organisation_pk is not null) then

		-- -----------------------------------------------
		-- update path_logo in organisation table
		-- this has to be done in case user has
		-- uploaded a new type of photo (jpg,
		-- gif, png etc)
		-- -----------------------------------------------
		v_sql_stmt := 'update	org_organisation';

		if (p_type = 'logo') then
			v_sql_stmt := v_sql_stmt||' set path_logo =';
		elsif (p_type = 'mainphoto') then
			v_sql_stmt := v_sql_stmt||' set path_mainphoto =';
		end if;

		v_sql_stmt := v_sql_stmt||'('||
					' select trg_filename as trg_filename from log_upload '||
					' where	log_upload_pk = '||
					' ( '||
					' select max(log_upload_pk)'||
					' from log_upload '||
					' where ses_session_id = '||c_id||' '||
					' ) '||
					' ) where org_organisation_pk = '||c_organisation_pk||'
					';

		-- When character-set is Norwegian the ',' in numbers (f.ex 52,783)
		-- has to be substituted with '.'
		v_sql_stmt := replace(v_sql_stmt, ',', '.');
		execute immediate v_sql_stmt;

		-- ---------------------------------------------
		-- show new picture and jump back to admin page
		-- ---------------------------------------------
		if (p_type = 'logo') then

			for r_cursor in cur_org_organisation
			loop
				v_html := v_html||htm.jumpTo(
							  'org_adm.viewOrg'
							, 3
							, c_text_pictureUpdated||'<br /><br /><img src="'||c_directory_path||'/'||r_cursor.path_logo||'?'||p_action||'" alt="" />'
						);
			end loop;

		elsif (p_type = 'mainphoto') then

			for r_cursor in cur_org_organisation
			loop
				v_html := v_html||htm.jumpTo(
							  'org_adm.viewOrg'
							, 3
							, c_text_pictureUpdated||'<br /><br /><img src="'||c_directory_path||'/'||r_cursor.path_mainphoto||'?'||p_action||'" alt="" />'
						);
			end loop;

		end if;

		update	org_organisation
		set		date_update = sysdate
		where	org_organisation_pk = c_organisation_pk;
		commit;

	-- -------------------------------------------------------------------
	-- upload of image failed
	-- -------------------------------------------------------------------
	elsif (p_action = 'failed') then
		v_html := v_html||htm.jumpTo(
					  g_package||'.'||c_procedure||'?p_type='||p_type
					, 10
					, c_text_uploadFailed
				);

	-- -------------------------------------------------------------------
	-- delete image
	-- -------------------------------------------------------------------
	elsif (p_action = 'delete') then

		v_sql_stmt := 'update	org_organisation';

		if (p_type = 'logo') then
			v_sql_stmt := v_sql_stmt||' set path_logo = ';
		elsif (p_type = 'mainphoto') then
			v_sql_stmt := v_sql_stmt||' set path_mainphoto = ';
		end if;

		v_sql_stmt := v_sql_stmt||
					' null '||
					' , date_update = sysdate '||
					' where org_organisation_pk = '||c_organisation_pk||'
					';

		execute immediate v_sql_stmt;

		v_html := v_html||htm.jumpTo(
					  'org_adm.viewOrg'
					, 1
					, c_text_pictureDeleted
				);

	else

		v_html := v_html||htm.jumpTo('pag_pub.userLogin?p_url='||g_package||'.'||c_procedure||'?'||owa_util.get_cgi_env('QUERY_STRING'), 1, '...');

	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		return null;
end;
end updatePicture;


---------------------------------------------------------
-- name:        updateOrgValues
-- what:        update orgnisation values
-- author:      Frode Klevstul
-- start date:  16.12.2005
---------------------------------------------------------
procedure updateOrgValues(p_action in varchar2 default null, p_org_value_type_pk in parameter_arr default empty, p_numValue in parameter_arr default empty, p_charValue in parameter_arr default empty) is begin
	ext_lob.pri(org_adm.updateOrgValues(p_action, p_org_value_type_pk, p_numValue, p_charValue));
end;

function updateOrgValues
(
	  p_action				in varchar2										default null
	, p_org_value_type_pk	in parameter_arr								default empty
	, p_numValue			in parameter_arr								default empty
	, p_charValue			in parameter_arr								default empty
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type	:= 'updateOrgValues';

	c_id						constant ses_session.id%type 						:= ses_lib.getId;
	c_organisation_pk			constant org_organisation.org_organisation_pk%type	:= ses_lib.getOrgPk(c_id);

	c_text_updateOrgValues		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'updateOrgValues');
	c_text_what					constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'what');
	c_text_numValue				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'numValue');
	c_text_charValue			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'charValue');
	c_text_unit					constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'unit');

	c_text_update				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'update');
	c_text_doNotUpdate			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'doNotUpdate');

	c_text_updating				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'updating');

	v_index						number;
	v_i							number								:= 0;
	v_num_value					org_value.num_value%type;
	v_char_value				org_value.char_value%type;
	v_error						boolean								:= false;
	v_tmpError					boolean								:= false;
	v_error_messages			varchar2(256);

	cursor	cur_org_value_type is
	select	org_value_type_pk, name, unit, bool_isNumeric, bool_isCharacter
	from	org_value_type
	where	ser_service_fk = 1;

	cursor	cur_org_value
	(
		b_org_value_type_pk	org_value_type.org_value_type_pk%type
	) is
	select	num_value, char_value
	from	org_value
	where	org_organisation_fk_pk	= c_organisation_pk
	and		org_value_type_fk_pk	= b_org_value_type_pk;

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_action='||p_action);
	ext_lob.ini(v_clob);

	use_lib.loggedInCheck(g_package, c_procedure, null);

	-- --------------------------------
	--  check if value is valid
	-- --------------------------------
	if (p_action like 'update') then
		for v_index in 1 .. p_numValue.count
		loop
			if ( p_numValue(v_index) is not null ) then

				ext_lib.verifyValue(
					  p_type		=> 'number'
					, p_value		=> p_numValue(v_index)
					, p_returnError => v_tmpError
					, p_returnMsg	=> v_error_messages
				);
		
				if (v_tmpError) then
					v_error := true;
				end if;

			end if;
		end loop;
	end if;

	if ( (p_action is null and c_organisation_pk is not null) or v_error) then

		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundAdminBox')||'<tr><td>'||chr(13);

		v_html := v_html||''||htm.bBox(c_text_updateOrgValues)||chr(13);
		v_html := v_html||''||htm.bForm('org_adm.updateOrgValues')||chr(13);
		v_html := v_html||'<input type="hidden" name="p_action" value="update" />'||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);

		-- ---------------------------------
		-- show possible error messages
		-- ---------------------------------
		if (v_error_messages is not null) then
			v_html := v_html||'<tr><td colspan="4">'||chr(13);
			v_html := v_html||''||htm.printErrors(v_error_messages)||chr(13);
			v_html := v_html||'</td></tr>'||chr(13);
		end if;

		v_html := v_html||'<tr><td>'||c_text_what||'</td><td width="15%">'||c_text_numValue||'</td><td>'||c_text_unit||'</td><td>'||c_text_charValue||'</td></tr>'||chr(13);

		for r_cursor in cur_org_value_type
		loop
			for r_cursor2 in cur_org_value(r_cursor.org_value_type_pk)
			loop
				v_num_value		:= r_cursor2.num_value;
				v_char_value	:= r_cursor2.char_value;
			end loop;

			if ( mod(v_i, 2) = 0 ) then
				v_html := v_html||'<tr>'||chr(13);
			else
				v_html := v_html||'<tr class="alternativeLine">'||chr(13);
			end if;

			v_html := v_html||'<td>'||tex_lib.get('db', 'org_value_type', r_cursor.name)||'</td><td>'||chr(13);
			v_html := v_html||'<input type="hidden" name="p_org_value_type_pk" value="'||r_cursor.org_value_type_pk||'" />'||chr(13);
			
			if (r_cursor.bool_isNumeric > 0) then
				v_html := v_html||'<input type="text" name="p_numValue" id="p_numValue" value="'||v_num_value||'" maxlength="30" size="7" />'||chr(13);
			else
				v_html := v_html||'&nbsp;<input type="hidden" name="p_numValue" id="p_numValue" value="" />'||chr(13);
			end if;

			v_html := v_html||'</td><td>'||tex_lib.get('db', 'org_value_type', r_cursor.unit)||'&nbsp;</td><td>'||chr(13);

			if (r_cursor.bool_isCharacter > 0) then
				v_html := v_html||'<input type="text" name="p_charValue" id="p_charValue" value="'||v_char_value||'" maxlength="128" size="20" />'||chr(13);
			else
				v_html := v_html||'&nbsp;<input type="hidden" name="p_charValue" id="p_charValue" value="" />'||chr(13);
			end if;

			v_html := v_html||'</td></tr>'||chr(13);

			v_i := v_i + 1;
		end loop;
		v_html := v_html||'<tr><td colspan="4">&nbsp;</td></tr>'||chr(13);
		v_html := v_html||'<tr><td colspan="4" style="padding-left: 20px;">'||htm.submitLink(c_text_update)||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td colspan="4" style="padding-left: 20px;">'||htm.historyBack(c_text_doNotUpdate)||'</td></tr>'||chr(13);

		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eForm||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);

		v_html := v_html||'</td></tr>'||htm.eTable||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

	elsif (p_action like 'update') then

		for v_index in 1 .. p_org_value_type_pk.count
		loop
			if ( p_org_value_type_pk(v_index) is not null ) then
         		update org_value
         		set num_value					= p_numValue(v_index)
         		  , char_value					= p_charValue(v_index)
         		where org_organisation_fk_pk	= c_organisation_pk
         		and org_value_type_fk_pk		= p_org_value_type_pk(v_index);

				if (sql%notfound) then
	         		insert into org_value
	         		(org_organisation_fk_pk, org_value_type_fk_pk, num_value, char_value)
	         		values (
	         		    c_organisation_pk
	         		  , p_org_value_type_pk(v_index)
	         		  , p_numValue(v_index)
	         		  , p_charValue(v_index)
					);
				end if;

				update	org_organisation
				set		date_update = sysdate
				where	org_organisation_pk = c_organisation_pk;

				commit;
			end if;
		end loop;

		v_html := v_html||htm.jumpTo('org_adm.viewOrg', 1, c_text_updating);

	else

		v_html := v_html||htm.jumpTo('pag_pub.userLogin?p_url='||g_package||'.'||c_procedure||'?'||owa_util.get_cgi_env('QUERY_STRING'), 1, '...');

	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		return null;
end;
end updateOrgValues;


-- ******************************************** --
end;
/
show errors;
