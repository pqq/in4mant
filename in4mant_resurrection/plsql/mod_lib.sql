PROMPT *** mod_lib ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package mod_lib is
/* ******************************************************
*	Package:     tex_lib
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	051220 | Frode Klevstul
*			- Package created
****************************************************** */
	procedure run;
	function hasAccess
	(
		  p_package			in mod_package.name%type		default null
		, p_procedure		in mod_procedure.name%type		default null
	) return boolean;

end;
/
show errors;





-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body mod_lib is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'mod_lib';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  30.11.2005
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);
	
	htp.p(g_package||'.'||c_procedure);	
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        hasAccess
-- what:        check if user has access to given module
-- author:      Frode Klevstul
-- start date:  20.12.2005
---------------------------------------------------------
function hasAccess
(
	  p_package			in mod_package.name%type		default null
	, p_procedure		in mod_procedure.name%type		default null
) return boolean
is
begin
declare
	c_procedure			constant mod_procedure.name%type 			:= 'hasAccess';

	c_id				constant ses_session.id%type				:= ses_lib.getId;
	c_use_user_pk		constant use_user.use_user_pk%type			:= ses_lib.getPk(c_id);
	c_restriction_mode	constant sys_parameter.value%type			:= sys_lib.getParameter('restriction-mode');

	e_stop				exception;

	v_weight_module		use_type.weight%type;
	v_weight_user		use_type.weight%type;
	v_weight_adminSuper	use_type.weight%type;
	v_count				number;
	
	-- ------------------------------------------------
	-- select the weight of a restricted module
	-- ------------------------------------------------
	cursor	cur_use_type is
	select	weight
	from	use_type
	where	use_type_pk = (
		select	use_type_fk_pk
		from	mod_module, mod_restriction
		where	name = p_package||'.'||p_procedure
		and		mod_module_pk = mod_module_fk_pk	
	);

	-- ------------------------------------------------
	-- select the weight of user logged in
	-- ------------------------------------------------
	cursor	cur_use_type_2 is
	select	weight
	from	use_type, use_user
	where	use_type_fk = use_type_pk
	and		use_user_pk = c_use_user_pk;

	-- ------------------------------------------------
	-- weight of a super admin user
	-- ------------------------------------------------
	cursor	cur_use_type_3 is
	select	weight
	from	use_type
	where	name = 'admin_super';

begin
	ses_lib.ini(g_package, c_procedure);

	-- DEBUG --
	 --htp.p('p_package: '||p_package||'<br>');
	 --htp.p('p_procedure: '||p_procedure||'<br>');
	 --htp.p('c_use_user_pk: '||c_use_user_pk||'<br>');
	 --htp.p('c_restriction_mode: '||c_restriction_mode||'<br>');
	 --return false;
	-- /DEBUG --

	-- ----------------------------------------------------
	-- check if user has access through "mod_permission"
	-- ----------------------------------------------------
	select	count(*)
	into	v_count
	from	mod_permission
	where	use_user_fk_pk = c_use_user_pk
	and		mod_module_fk_pk = (
		select	mod_module_pk
		from	mod_module
		where	name = p_package||'.'||p_procedure
	);
	
	if (v_count > 0) then
		return true;
	end if;

	-- ----------------------------------------------------
	-- check if user is super admin
	-- ----------------------------------------------------
	if (c_use_user_pk is not null) then
		for r_cursor in cur_use_type_2
		loop
			v_weight_user := r_cursor.weight;
		end loop;
	
		for r_cursor in cur_use_type_3
		loop
			v_weight_adminSuper := r_cursor.weight;
		end loop;
	end if;

	-- -------------------------------------------------------------------------------
	-- no matter what weight a module has, a super admin will always have access
	-- -------------------------------------------------------------------------------
	if (v_weight_user = v_weight_adminSuper) then
		return true;
	end if;

	-- -------------------------------------------------
	-- check if module is restricted
	-- -------------------------------------------------
	for r_cursor in cur_use_type
	loop
		v_weight_module := r_cursor.weight;
	end loop;

	-- DEBUG --
	-- htp.p('v_weight_module: '||v_weight_module||'<br>');
	-- /DEBUG --
	
	if (v_weight_module is not null) then

		-- ---------------------------------------------------------------------------------
		-- if user isn't logged in and weight requires a logged in user ask user to login
		-- ---------------------------------------------------------------------------------
		if (c_use_user_pk is null and v_weight_module > 0) then
			use_pub.pleaseLogin(p_package, p_procedure);
			raise e_stop;
		end if;

		-- DEBUG --
		-- htp.p('v_weight_user: '||v_weight_user||'<br>');
		-- /DEBUG --

		-- -------------------------------------------
		-- if user has lower weight than module the 
		-- user has no access
		-- -------------------------------------------
		if (v_weight_user < v_weight_module) then			
			return false;
		-- -------------------------------------------
		-- user has access
		-- -------------------------------------------
		else
			return true;
		end if;

	-- ------------------------------------------
	-- module is not in mod_restriction table
	-- ------------------------------------------
	else
		if (c_restriction_mode = 'strict') then
			return false;
		elsif (c_restriction_mode = 'open') then
			return true;
		else
			pag_pub.errorPage(g_package, c_procedure, 'unknown_restriction_mode:'||c_restriction_mode);
			raise e_stop;
		end if;
	end if;

	-- should never come here...
	return false;

exception
	when e_stop then
		raise;
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
end;
end hasAccess;


-- ******************************************** --
end;
/
show errors;
