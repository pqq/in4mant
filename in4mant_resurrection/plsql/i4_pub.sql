PROMPT *** i4_pub ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package i4_pub is
/* ******************************************************
*	Package:     tex_lib
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	051222 | Frode Klevstul
*			- Package created
****************************************************** */
	procedure run;
	procedure i4admin;
	procedure overview;
	function overview
		return clob;

end;
/
show errors;





-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body i4_pub is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'i4_pub';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  22.12.2005
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);	
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        overview
-- what:        i4admin overview page
-- author:      Frode Klevstul
-- start date:  22.12.2005
---------------------------------------------------------
procedure i4admin is begin
    ext_lob.pri(i4_pub.overview);
end;

procedure overview is begin
    ext_lob.pri(i4_pub.overview);
end;

function overview
	return clob
is
begin
declare
	c_procedure			constant mod_procedure.name%type 		:= 'overview';

	v_html 				clob;
	v_clob				clob;

begin
	ses_lib.ini(g_package, c_procedure, null);
	ext_lob.ini(v_clob);

	use_lib.loggedInCheck(g_package, c_procedure, null);

	v_html := v_html||''||htm.bPage(g_package, c_procedure, 'i4admin')||chr(13);
	v_html := v_html||'<center>'||chr(13);
	v_html := v_html||''||htm.bTable('surroundElements')||chr(13);

	v_html := v_html||'<tr><td>'||chr(13);
	v_html := v_html||''||i4_lib.orgAdm||chr(13);
	v_html := v_html||'</td></tr>'||chr(13);

	v_html := v_html||'<tr><td>'||chr(13);
	v_html := v_html||''||i4_lib.texAdm||chr(13);
	v_html := v_html||'</td></tr>'||chr(13);

--	v_html := v_html||'<tr><td>'||chr(13);
--	v_html := v_html||''||i4_lib.memAdm||chr(13);
--	v_html := v_html||'</td></tr>'||chr(13);

	v_html := v_html||'<tr><td>'||chr(13);
	v_html := v_html||''||i4_lib.extAdm||chr(13);
	v_html := v_html||'</td></tr>'||chr(13);

	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||'</center>'||chr(13);
	v_html := v_html||''||htm.ePage||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
    when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
        return null;
end;
end overview;


-- ******************************************** --
end;
/
show errors;
