PROMPT *** org_lib ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package org_lib is
/* ******************************************************
*	Package:     pag_pub
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	051013 | Frode Klevstul
*			- Package created
****************************************************** */
	type parameter_arr 			is table of varchar2(4000) index by binary_integer;
	empty_array 				parameter_arr;

	procedure run;
	function contactInfo
	(
		  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
		, p_editMode			in boolean										default false
	) return clob;
	procedure contactInfo
	(
		  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
		, p_editMode			in boolean										default false
	);
	procedure typeOfOrg
	(
		  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
		, p_editMode			in boolean										default false
	);
	function typeOfOrg
	(
		  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
		, p_editMode			in boolean										default false
	) return clob;
	procedure attractions
	(
		  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
		, p_editMode			in boolean										default false
	);
	function attractions
	(
		  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
		, p_editMode			in boolean										default false
	) return clob;
	procedure viewContent
	(
		  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
		, p_name				in con_type.name%type							default null
		, p_editMode			in boolean										default false
	);
	function viewContent
	(
		  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
		, p_name				in con_type.name%type							default null
		, p_editMode			in boolean										default false
	) return clob;
	procedure orgValues
	(
		  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
		, p_editMode			in boolean										default false
	);
	function orgValues
	(
		  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
		, p_editMode			in boolean										default false
	) return clob;
	procedure detailsInfo
	(
		  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
		, p_editMode			in boolean										default false
	);
	function detailsInfo
	(
		  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
		, p_editMode			in boolean										default false
	) return clob;
	procedure mainPhoto
	(
		  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
		, p_editMode			in boolean										default false
	);
	function mainPhoto
	(
		  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
		, p_editMode			in boolean										default false
	) return clob;
	procedure generalInformation
	(
		  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
		, p_editMode			in boolean										default false
	);
	function generalInformation
	(
		  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
		, p_editMode			in boolean										default false
	) return clob;
	procedure showMap
	(
		  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	);
	function showMap
	(
		  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	) return clob;
	procedure pictures
	(
		  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
		, p_editMode			in boolean										default false
	);
	function pictures
	(
		  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
		, p_editMode			in boolean										default false
	) return clob;
	function getOrgPk
	(
		p_use_user_pk			in use_user.use_user_pk%type
	) return org_organisation.org_organisation_pk%type;
	function getOrgName
	(
		p_organisation_pk		in org_organisation.org_organisation_pk%type
	) return org_organisation.name%type;
	function getOrgZip
	(
		p_organisation_pk		in org_organisation.org_organisation_pk%type
	) return add_address.zip%type;
	function getOrgNo
	(
		p_organisation_pk		in org_organisation.org_organisation_pk%type
	) return org_organisation.org_number%type;
	procedure initialiseOrganisation
	(
	  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	);
	function hasStatus
	(
		  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
		, p_type				in org_status.name%type							default null
	) return boolean;
	procedure rankOrganisations
	(
		  p_number				in number										default null
		, p_type				in varchar2										default null
	);
	function rankOrganisations
	(
		  p_number				in number										default null
		, p_type				in varchar2										default null
	) return clob;
	procedure chosenOrganisation;
	function chosenOrganisation
		return clob;
	function isParent
	(
		  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	) return boolean;
	procedure organisationPicker
	(
		  p_previousUrl				varchar2									default null
		, p_nextUrl					varchar2									default null
		, p_type					varchar2									default null
		, p_org_organisation_pk		org_organisation.org_organisation_pk%type	default null
		, p_service_pk				ser_service.ser_service_pk%type				default null
		, p_name					varchar2									default null
	);
	function organisationPicker
	(
		  p_previousUrl				varchar2									default null
		, p_nextUrl					varchar2									default null
		, p_type					varchar2									default null
		, p_org_organisation_pk		org_organisation.org_organisation_pk%type	default null
		, p_service_pk				ser_service.ser_service_pk%type				default null
		, p_name					varchar2									default null
	) return clob;
	procedure sameParentOrg
	(
		p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	);
	function sameParentOrg
	(
		p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	) return clob;
	procedure getDirectoryPath
	(
		  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
		, p_webpath				in boolean										default false
	);
	function getDirectoryPath
	(
		  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
		, p_webpath				in boolean										default false
	) return all_directories.directory_path%type;

end;
/
show errors;





-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body org_lib is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'org_lib';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  13.10.2005
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;



-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
--
-- Procedures used as part of viewOrg (organisation page)
--
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++



---------------------------------------------------------
-- name:        contactInfo
-- what:        contact information
-- author:      Frode Klevstul
-- start date:  25.05.2006
---------------------------------------------------------
procedure contactInfo
(
	  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	, p_editMode			in boolean										default false
) is begin
	ext_lob.pri(org_lib.contactInfo(p_organisation_pk, p_editMode));
end;

function contactInfo
(
	  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	, p_editMode			in boolean										default false
) return clob
is
begin
declare
	c_procedure			constant mod_procedure.name%type					:= 'contactInfo';

	c_directory_path	constant sys.all_directories.directory_path%type	:= org_lib.getDirectoryPath(p_organisation_pk, true);

	c_tex_info			constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'info');
	c_tex_address		constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'address');
	c_tex_telephone		constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'telephone');
	c_tex_fax			constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'fax');
	c_tex_homepage		constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'homepage');
	c_tex_email			constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'email');
	c_tex_web			constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'web');

	cursor	cur_org_add is
	select	o.name, path_logo, street, zip, landline, fax, email, url
	from	org_organisation o, add_address a
	where	org_organisation_pk = p_organisation_pk
	and		add_address_fk = add_address_pk;

	v_html_tmp			varchar2(1024);
	v_html				clob;
	v_clob				clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	for r_cursor in cur_org_add
	loop
		-- start box
		v_html := v_html||''||htm.bBox(c_tex_info);
		v_html := v_html||''||htm.bTable;
		-- org name
		v_html := v_html||'
			<tr>
				<td style="padding-left: 6px;">'||
					htm.bHighlight('org_adm.updateOrgName', p_editMode)||'
					<span class="orgNameHeading">'||
						r_cursor.name||'
					</span>'||
					htm.eHighlight||'
				</td>
			</tr>
		';
		-- logo
		if (org_lib.hasStatus(p_organisation_pk, 'member') or p_editMode) then
			v_html := v_html||'
				<tr>
					<td style="text-align: center;">
						'||htm.bHighlight('org_adm.updatePicture?p_type=logo', p_editMode);
						-- make logo clickable if not in edit mode
						if (not p_editMode) then
							v_html := v_html||'<a href="org_pub.viewOrg?p_organisation_pk='||p_organisation_pk||'">'||chr(13);
						end if;
						v_html := v_html||'<img border="0" src="'||c_directory_path||'/'||r_cursor.path_logo||'" alt="" />'||chr(13);
						if (not p_editMode) then
							v_html := v_html||'</a>'||chr(13);
						end if;
						v_html := v_html||'
							'||htm.eHighlight||'
					</td>
				</tr>
			';
		end if;
		-- address, homepage, email
		v_html := v_html||'
			<tr>
				<td style="padding-left: 6px;">'||chr(13)||
					htm.bHighlight('org_adm.updateAddress', p_editMode)||' '||chr(13)||
					htm.bTable||'
						<tr>
							<td colspan="2" style="padding-left: 2px;">'||chr(13)||
								c_tex_address||'
							</td>
						</tr>
						<tr>
							<td colspan="2" style="padding-left: 15px;">'||chr(13)||
								r_cursor.street||'
							</td>
						</tr>
						<tr>
							<td colspan="2" style="padding-left: 15px;">'||chr(13)||
								r_cursor.zip||' '||geo_lib.getCity(r_cursor.zip)||'
							</td>
						</tr>
		';
		if (r_cursor.landline is not null or r_cursor.fax is not null) then
			v_html := v_html||'
				<tr>
					<td colspan="2">
						&nbsp;
					</td>
				</tr>
			';
		end if;
		if (r_cursor.landline is not null) then
			v_html := v_html||'
				<tr>
					<td style="padding-left: 2px;" width="30">'||
						c_tex_telephone||'
					</td>
					<td>'||
						r_cursor.landline||'
					</td>
				</tr>
			';
		end if;
		if (r_cursor.fax is not null) then
			v_html := v_html||'
				<tr>
					<td style="padding-left: 2px;">'||
						c_tex_fax||'
					</td>
					<td>'||
						r_cursor.fax||'
					</td>
				</tr>
			';
		end if;
		if (r_cursor.url is not null or r_cursor.email is not null) then
			v_html := v_html||'
				<tr>
					<td colspan="2">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left: 2px;">'||
						c_tex_web||'
					</td>
				</tr>

				<tr>
					<td colspan="2" style="padding-left: 15px;">
			';

				if (r_cursor.url is not null) then
					v_html := v_html||'<a href="http://'||r_cursor.url||'" target="_blank">'||c_tex_homepage||'</a><br />'||chr(13);
				end if;
				if (r_cursor.email is not null) then
					v_html := v_html||'<a href="mailto:'||r_cursor.email||'" target="_blank">'||c_tex_email||'</a><br />'||chr(13);
				end if;

			v_html := v_html||'
					</td>
				</tr>
			';
		end if;
		v_html := v_html||''||htm.eTable||''||htm.eHighlight||'</td></tr>'||chr(13);

		v_html := v_html||''||htm.eTable;
		v_html := v_html||''||htm.eBox;
	end loop;

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end contactInfo;


---------------------------------------------------------
-- name:        typeOfOrg
-- what:        type of place / organisation
-- author:      Frode Klevstul
-- start date:  25.05.2006
---------------------------------------------------------
procedure typeOfOrg
(
	  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	, p_editMode			in boolean										default false
) is begin
	ext_lob.pri(org_lib.typeOfOrg(p_organisation_pk, p_editMode));
end;

function typeOfOrg
(
	  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	, p_editMode			in boolean										default false
) return clob
is
begin
declare

	c_procedure			constant mod_procedure.name%type			:= 'typeOfOrg';

	c_id				constant ses_session.id%type 				:= ses_lib.getId;
	c_service_pk		constant ser_service.ser_service_pk%type	:= ses_lib.getSer(c_id);

	c_tex_typeOfOrg		constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'typeOfOrg');
	c_tex_addTypePlace	constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'addTypeOrg');

	v_typePlace			boolean										:= false;

	cursor	cur_lis_org is
	select	l.name
	from	lis_list l, lis_org o
	where	o.lis_list_fk_pk = l.lis_list_pk
	and		o.org_organisation_fk_pk = p_organisation_pk
	and		l.lis_type_fk = (select lis_type_pk from lis_type where name = 'org_type' and ser_service_fk = c_service_pk);

	v_html_typePlace	clob;
	v_html				clob;
	v_clob				clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html_typePlace := v_html_typePlace||''||htm.bBox(c_tex_typeOfOrg)||chr(13);
	v_html_typePlace := v_html_typePlace||''||htm.bHighlight('org_adm.updateListOrg?p_type=org_type', p_editMode)||chr(13);
	v_html_typePlace := v_html_typePlace||''||''||htm.bTable||chr(13);
	v_html_typePlace := v_html_typePlace||'<tr><td colspan="2">'||chr(13);

	for r_cursor in cur_lis_org
	loop
		if (r_cursor.name is not null) then v_typePlace := true; end if;
		v_html_typePlace := v_html_typePlace||'&raquo; '||tex_lib.get('db', 'lis_list', r_cursor.name)||'<br />'||chr(13);
	end loop;

	v_html_typePlace := v_html_typePlace||'</td></tr>'||chr(13);
	v_html_typePlace := v_html_typePlace||''||htm.eTable||''||htm.eHighlight||chr(13);
	v_html_typePlace := v_html_typePlace||''||htm.eBox||chr(13);

	-- concatinate if type of place has info
	if (v_typePlace) then
		v_html := v_html ||''||v_html_typePlace||chr(13);
	elsif (p_editMode) then
		v_html := v_html||''||htm.bBox(c_tex_typeOfOrg)||chr(13);
		v_html := v_html||''||htm.bHighlight('org_adm.updateListOrg?p_type=org_type', p_editMode)||''||chr(13);
		v_html := v_html||''||htm.bTable('fullHeightTable')||chr(13);
		v_html := v_html||'<tr><td class="centerMiddle">'||chr(13);
		v_html := v_html||'<span class="adminContentButton">'||c_tex_addTypePlace||'</span><br /><br />'||chr(13);
		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eHighlight||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);
	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end typeOfOrg;


---------------------------------------------------------
-- name:        attractions
-- what:        attractions (lists an org is related to (lis_list / lis_org))
-- author:      Frode Klevstul
-- start date:  25.05.2006
---------------------------------------------------------
procedure attractions
(
	  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	, p_editMode			in boolean										default false
) is begin
	ext_lob.pri(org_lib.typeOfOrg(p_organisation_pk, p_editMode));
end;

function attractions
(
	  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	, p_editMode			in boolean										default false
) return clob
is
begin
declare

	c_procedure			constant mod_procedure.name%type			:= 'attractions';

	c_id				constant ses_session.id%type 				:= ses_lib.getId;
	c_service_pk		constant ser_service.ser_service_pk%type	:= ses_lib.getSer(c_id);

	c_tex_otherLists	constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'otherLists');
	c_tex_addOtherLists	constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'addOtherLists');

	v_firstCharsNow		varchar2(8)							:= '0';
	v_firstCharsOld		varchar2(8)							:= '0';
	v_onOtherLists		boolean								:= false;
	v_firstEntry		boolean								:= true;

	cursor	cur_lis_org_other is
	select	l.name
	from	lis_list l, lis_org o
	where	o.lis_list_fk_pk = l.lis_list_pk
	and		o.org_organisation_fk_pk = p_organisation_pk
	and		l.lis_type_fk in (select lis_type_pk from lis_type where name not like 'org_type' and ser_service_fk = c_service_pk);

	v_html_onOtherLists	clob;
	v_html				clob;
	v_clob				clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html_onOtherLists := v_html_onOtherLists||''||htm.bBox(c_tex_otherLists)||chr(13);
	v_html_onOtherLists := v_html_onOtherLists||''||htm.bHighlight('org_adm.updateListOrg?p_type=other', p_editMode);
	v_html_onOtherLists := v_html_onOtherLists||''||htm.bTable||chr(13);
	v_html_onOtherLists := v_html_onOtherLists||'<tr><td colspan="2">'||chr(13);

	for r_cursor in cur_lis_org_other
	loop
		if (r_cursor.name is not null) then
			v_onOtherLists := true;

			-- ----------------------------------------------------------------------
			-- if it's a new list type it starts with another character (number)
			-- ex: 101_xxx 102_xxx 201_xxx 301_xxx 302_xxx 303_xxx
			-- ---
			-- selects out the first digits but two last (this method supports 100, 00-99, sublists):
			-- if "20001_nameOfList" -> "200"
			--
			-- if Oracle regExp was stable the following code could have been used:
			--
			-- v_firstCharsNow := regexp_replace(r_cursor.name, '^([[:digit:]]*)([[:digit:]]{2}.*)$', '\1');
			-- ----------------------------------------------------------------------
			v_firstCharsNow := substr(r_cursor.name,1,(instr(r_cursor.name,'_')-1));
			v_firstCharsNow := substr(v_firstCharsNow,1,(length(v_firstCharsNow)-2));

			if (v_firstCharsOld != v_firstCharsNow) then
				v_firstCharsOld := v_firstCharsNow;
				if (v_firstEntry) then
					v_firstEntry := false;
				else
					v_html_onOtherLists := v_html_onOtherLists||'<br />'||chr(13);
				end if;

				v_html_onOtherLists := v_html_onOtherLists||''||tex_lib.get('db', 'lis_list', v_firstCharsOld)||'<br />'||chr(13);

			end if;

			v_html_onOtherLists := v_html_onOtherLists||'&raquo; '||tex_lib.get('db', 'lis_list', r_cursor.name)||'<br />'||chr(13);
		end if;
	end loop;

	v_html_onOtherLists := v_html_onOtherLists||''||htm.eTable||''||htm.eHighlight||chr(13);
	v_html_onOtherLists := v_html_onOtherLists||''||htm.eBox||chr(13);

	-- concatinate if type of place has info
	if (v_onOtherLists and org_lib.hasStatus(p_organisation_pk, 'member')) then
		v_html := v_html ||''||v_html_onOtherLists;
	elsif (p_editMode and org_lib.hasStatus(p_organisation_pk, 'member')) then
		v_html := v_html||''||htm.bBox(c_tex_otherLists)||chr(13);
		v_html := v_html||''||htm.bHighlight('org_adm.updateListOrg?p_type=other', p_editMode)||''||chr(13);
		v_html := v_html||''||htm.bTable('fullHeightTable')||chr(13);
		v_html := v_html||'<tr><td class="centerMiddle">'||chr(13);
		v_html := v_html||'<span class="adminContentButton">'||c_tex_addOtherLists||'</span><br /><br />'||chr(13);
		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eHighlight||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);
	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end attractions;


---------------------------------------------------------
-- name:        viewContent
-- what:        present an organisation's content
-- author:      Frode Klevstul
-- start date:  20.01.2006
---------------------------------------------------------
procedure viewContent
(
	  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	, p_name				in con_type.name%type							default null
	, p_editMode			in boolean										default false
) is begin
	ext_lob.pri(org_lib.viewContent(p_organisation_pk, p_name, p_editMode));
end;

function viewContent
(
	  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	, p_name				in con_type.name%type							default null
	, p_editMode			in boolean										default false
) return clob
is
begin
declare
	c_procedure				constant mod_procedure.name%type 	:= 'viewContent';

	c_tex_addContentEvents	constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'addContentEvents');

	v_con_type_pk			con_type.con_type_pk%type;
	v_editMode				number;
	v_noFirst				number;

	v_html					clob;
	v_clob					clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	if (p_editMode) then
		v_editMode := 1;
	end if;

	-- -------------------------------
	-- get correct number to show
	-- -------------------------------
	if (p_name = 'event_org') then
		v_noFirst := 15;
	elsif (p_name = 'news_org') then
		v_noFirst := 5;
	else
		v_noFirst := 15;
	end if;

	-- --------------------------
	-- display content
	-- --------------------------
	select	con_type_pk
	into	v_con_type_pk
	from	con_type
	where	name = p_name;

	v_html := v_html||''||
	con_lib.listCon(
		  p_noFirst				=> v_noFirst
		, p_noOnOnePage			=> 30
		, p_noShowFrom			=> null
		, p_subjectMaxLength	=> 16
		, p_editMode			=> v_editMode
		, p_organisation_pk		=> p_organisation_pk
		, p_con_type_pk			=> v_con_type_pk
		, p_con_subtype_pk		=> null
		, p_date_start			=> null
		, p_date_stop			=> null
	);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end viewContent;


---------------------------------------------------------
-- name:        orgValues
-- what:        list organisation values
-- author:      Frode Klevstul
-- start date:  16.12.2005
---------------------------------------------------------
procedure orgValues
(
	  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	, p_editMode			in boolean										default false
) is begin
	ext_lob.pri(org_lib.orgValues(p_organisation_pk, p_editMode));
end;

function orgValues
(
	  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	, p_editMode			in boolean										default false
) return clob
is
begin
declare
	c_procedure			constant mod_procedure.name%type	:= 'orgValues';

	c_tex_orgValues		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'orgValues');
	c_tex_addOrgValues	constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'addOrgValues');

	cursor	cur_org_value is
	select	num_value, char_value, name, unit, bool_isNumeric, bool_isCharacter
	from	org_value v, org_value_type t
	where	org_organisation_fk_pk	= p_organisation_pk
	and		org_value_type_fk_pk	= org_value_type_pk
	and		(num_value is not null or char_value is not null);

	v_count				number;
	v_hasValue			boolean								:= false;
	v_html				clob;
	v_clob				clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	select	count(*)
	into	v_count
	from	org_value
	where	org_organisation_fk_pk = p_organisation_pk
	and		(num_value is not null or char_value is not null);

	if (v_count > 0) then
		v_html := v_html||''||htm.bBox(c_tex_orgValues)||chr(13);
		v_html := v_html||''||htm.bHighlight('org_adm.updateOrgValues', p_editMode)||''||chr(13);
		v_html := v_html||''||htm.bTable('100%')||chr(13);

		for r_cursor in cur_org_value
		loop
			if (r_cursor.name is not null) then v_hasValue := true; end if;
			v_html := v_html||'<tr><td>'||tex_lib.get('db', 'org_value_type', r_cursor.name)||chr(13);

			if (r_cursor.bool_isNumeric > 0) then
				v_html := v_html||''||r_cursor.num_value||' ';
				if (r_cursor.unit is not null) then
					v_html := v_html||''||tex_lib.get('db', 'org_value_type', r_cursor.unit)||' '||chr(13);
				end if;
			end if;

			if (r_cursor.bool_isCharacter > 0) then
				if (r_cursor.bool_isNumeric > 0) then
					v_html := v_html||' - ';
				end if;
				v_html := v_html||''||r_cursor.char_value||' ';
			end if;

			v_html := v_html||'</td></tr>'||chr(13);
		end loop;

		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eHighlight||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);
	elsif (v_count = 0 and p_editMode) then
		v_html := v_html||''||htm.bBox(c_tex_orgValues)||chr(13);
		v_html := v_html||''||htm.bHighlight('org_adm.updateOrgValues', p_editMode)||''||chr(13);
		v_html := v_html||''||htm.bTable('fullHeightTable')||chr(13);
		v_html := v_html||'<tr><td class="centerMiddle">'||chr(13);
		v_html := v_html||'<span class="adminContentButton">'||c_tex_addOrgValues||'</span><br /><br />'||chr(13);
		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eHighlight||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);
	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end orgValues;


---------------------------------------------------------
-- name:        detailsInfo
-- what:        details like opening hours, right, age limit
-- author:      Frode Klevstul
-- start date:  13.10.2005
---------------------------------------------------------
procedure detailsInfo
(
	  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	, p_editMode			in boolean										default false
) is begin
	ext_lob.pri(org_lib.detailsInfo(p_organisation_pk, p_editMode));
end;

function detailsInfo
(
	  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	, p_editMode			in boolean										default false
) return clob
is
begin
declare
	c_procedure			constant mod_procedure.name%type	:= 'detailsInfo';

	c_tex_openingHours	constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'openingHours');
	c_tex_monday		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'monday');
	c_tex_tuesday		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'tuesday');
	c_tex_wednesday		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'wednesday');
	c_tex_thursday		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'thursday');
	c_tex_friday		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'friday');
	c_tex_saturday		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'saturday');
	c_tex_sunday		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'sunday');
	c_tex_rights		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'rights');
	c_tex_ageLimit		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'ageLimit');
	c_tex_addRights		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'addRights');
	c_tex_addAgeLimit	constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'addAgeLimit');

	cursor	cur_org_hours is
	select	monday, tuesday, wednesday, thursday, friday, saturday, sunday
	from	org_hours
	where	org_organisation_fk_pk = p_organisation_pk;

	cursor	cur_org_rights is
	select	name
	from	org_rights
	where	org_rights_pk in (
		select	org_rights_fk_pk
		from	org_organisation_rights
		where	org_organisation_fk_pk = p_organisation_pk
	)
	order by description;

	cursor	cur_org_organisation is
	select	age_limit, age_description
	from	org_organisation
	where	org_organisation_pk = p_organisation_pk;

	v_rights			boolean								:= false;
	v_age				boolean								:= false;
	v_html_rights		clob;
	v_html_age			clob;
	v_html				clob;
	v_clob				clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := htm.bBox(c_tex_openingHours)||chr(13);
	v_html := v_html||''||htm.bHighlight('org_adm.updateDetailsInfo', p_editMode)||chr(13);
	v_html := v_html||''||htm.bTable||chr(13);

	-- ------------------------------
	-- opening hours
	-- ------------------------------
	for r_cursor in cur_org_hours
	loop
		v_html := v_html||'<tr><td>'||c_tex_monday||'</td><td>&nbsp;'||r_cursor.monday||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td>'||c_tex_tuesday||'</td><td>&nbsp;'||r_cursor.tuesday||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td>'||c_tex_wednesday||'</td><td>&nbsp;'||r_cursor.wednesday||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td>'||c_tex_thursday||'</td><td>&nbsp;'||r_cursor.thursday||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td>'||c_tex_friday||'</td><td>&nbsp;'||r_cursor.friday||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td>'||c_tex_saturday||'</td><td>&nbsp;'||r_cursor.saturday||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td>'||c_tex_sunday||'</td><td>&nbsp;'||r_cursor.sunday||'</td></tr>'||chr(13);
	end loop;

	v_html := v_html||'<tr><td colspan="2">&nbsp;</td></tr>'||chr(13);

	-- ------------------------------
	-- rights
	-- ------------------------------
	v_html_rights := v_html_rights||'<tr><td>'||c_tex_rights||'</td><td>&nbsp;'||chr(13);

	for r_cursor in cur_org_rights
	loop
		if (r_cursor.name is not null) then v_rights := true; end if;
		v_html_rights := v_html_rights||''||tex_lib.get('db', 'org_rights', r_cursor.name)||' '||chr(13);
	end loop;

	v_html_rights := v_html_rights||'</td></tr>'||chr(13);
	v_html_rights := v_html_rights||'<tr><td colspan="2">&nbsp;</td></tr>'||chr(13);

	-- ------------------------------
	-- age limit
	-- ------------------------------
	v_html_age := v_html_age||'<tr><td style="vertical-align: top;">'||c_tex_ageLimit||'</td><td>&nbsp;'||chr(13);

	for r_cursor in cur_org_organisation
	loop
		if (r_cursor.age_limit is not null or r_cursor.age_description is not null) then v_age := true; end if;
		v_html_age := v_html_age||''||r_cursor.age_limit;
		if (r_cursor.age_description is not null) then
			v_html_age := v_html_age||''||' (<i>'||r_cursor.age_description||'</i>)'||chr(13);
		end if;
	end loop;

	v_html_age := v_html_age||'</td></tr>'||chr(13);

	-- ------------------------------
	-- concatinate if content
	-- ------------------------------
	if (v_rights) then
		v_html := v_html||''||v_html_rights;
	elsif (p_editMode) then
		v_html := v_html||'<tr><td colspan="2">'||htm.link(c_tex_addRights,'org_adm.updateDetailsInfo')||'</td></tr>'||chr(13);
	end if;
	if (v_age) then
		v_html := v_html||''||v_html_age;
	elsif (p_editMode) then
		v_html := v_html||'<tr><td colspan="2">'||htm.link(c_tex_addAgeLimit,'org_adm.updateDetailsInfo')||'</td></tr>'||chr(13);
	end if;

	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||''||htm.eHighlight||chr(13);
	v_html := v_html||''||htm.eBox||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end detailsInfo;


---------------------------------------------------------
-- name:        mainPhoto
-- what:        display an organisation's main photo
-- author:      Frode Klevstul
-- start date:  17.10.2005
---------------------------------------------------------
procedure mainPhoto
(
	  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	, p_editMode			in boolean										default false
) is begin
	ext_lob.pri(org_lib.mainPhoto(p_organisation_pk, p_editMode));
end;

function mainPhoto
(
	  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	, p_editMode			in boolean										default false
) return clob
is
begin
declare
	c_procedure			constant mod_procedure.name%type					:= 'mainPhoto';

	c_tex_mainPhoto		constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'mainPhoto');
	c_directory_path	constant sys.all_directories.directory_path%type	:= org_lib.getDirectoryPath(p_organisation_pk, true);

	cursor	cur_org_organisation is
	select	path_mainphoto
	from	org_organisation
	where	org_organisation_pk = p_organisation_pk;

	v_html				clob;
	v_clob				clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := htm.bBox(c_tex_mainPhoto)||chr(13);
	v_html := v_html||''||htm.bHighlight('org_adm.updatePicture?p_type=mainphoto', p_editMode)||chr(13);
	v_html := v_html||''||htm.bTable||chr(13);
	v_html := v_html||'<tr><td>'||chr(13);

	for r_cursor in cur_org_organisation
	loop
		v_html := v_html||'
			<center>
				<img src="'||c_directory_path||'/'||r_cursor.path_mainphoto||'" alt="" />
			</center>
		';
	end loop;

	v_html := v_html||'</td></tr>';
	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||''||htm.eHighlight||chr(13);
	v_html := v_html||''||htm.eBox||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end mainPhoto;


---------------------------------------------------------
-- name:        generalInformation
-- what:        presents an organisation's general information
--				(org_organisation.generalInformation)
-- author:      Frode Klevstul
-- start date:  13.10.2005
---------------------------------------------------------
procedure generalInformation
(
	  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	, p_editMode			in boolean										default false
) is begin
	ext_lob.pri(org_lib.generalInformation(p_organisation_pk, p_editMode));
end;

function generalInformation
(
	  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	, p_editMode			in boolean										default false
) return clob
is
begin
declare
	c_procedure				constant mod_procedure.name%type 	:= 'generalInformation';

	c_tex_about				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'about');
	c_tex_addGeneralInfo	constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'addGeneralInfo');

	cursor	cur_org_organisation is
	select	name, general_information
	from	org_organisation
	where	org_organisation_pk = p_organisation_pk;

	v_html			clob;
	v_clob			clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	for r_cursor in cur_org_organisation
	loop
		if (r_cursor.general_information is not null) then
			v_html := v_html||''||htm.bBox(c_tex_about||' '||r_cursor.name)||chr(13);
			v_html := v_html||''||htm.bTable||chr(13);
			v_html := v_html||'<tr><td>'||chr(13);
			v_html := v_html||''||htm.bHighlight('org_adm.updateGeneralInformation', p_editMode)||chr(13);
			v_html := v_html||''||r_cursor.general_information||chr(13);
			v_html := v_html||''||htm.eHighlight||chr(13);
			v_html := v_html||'</td></tr>'||chr(13);
			v_html := v_html||''||htm.eTable||chr(13);
			v_html := v_html||''||htm.eBox||chr(13);
		elsif (p_editMode) then
			v_html := v_html||''||htm.bBox(c_tex_about||' '||r_cursor.name)||chr(13);
			v_html := v_html||''||htm.bHighlight('org_adm.updateGeneralInformation', p_editMode)||''||chr(13);
			v_html := v_html||''||htm.bTable('fullHeightTable')||chr(13);
			v_html := v_html||'<tr><td class="centerMiddle">'||chr(13);
			v_html := v_html||'<span class="adminContentButton">'||c_tex_addGeneralInfo||'</span><br /><br />'||chr(13);
			v_html := v_html||'</td></tr>'||chr(13);
			v_html := v_html||''||htm.eTable||chr(13);
			v_html := v_html||''||htm.eHighlight||chr(13);
			v_html := v_html||''||htm.eBox||chr(13);
		end if;
	end loop;

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end generalInformation;


---------------------------------------------------------
-- name:        showMap
-- what:        show map
-- author:      Frode Klevstul
-- start date:  05.07.2006
---------------------------------------------------------
procedure showMap
(
	  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
) is begin
	ext_lob.pri(org_lib.showMap(p_organisation_pk));
end;

function showMap
(
	  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
) return clob
is
begin
declare
	c_procedure				constant mod_procedure.name%type 	:= 'showMap';

	c_tex_about				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'about');
	c_tex_addGeneralInfo	constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'addGeneralInfo');

	cursor	cur_org_organisation is
	select	name, add_address_fk
	from	org_organisation
	where	org_organisation_pk = p_organisation_pk;

	v_html			clob;
	v_clob			clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	for r_cursor in cur_org_organisation
	loop
		v_html := v_html||''||add_lib.getMap(r_cursor.add_address_fk, 220, 250, r_cursor.name, p_organisation_pk)||chr(13);
	end loop;

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end showMap;


---------------------------------------------------------
-- name:        pictures
-- what:        show PIC module
-- author:      Frode Klevstul
-- start date:  08.07.2006
---------------------------------------------------------
procedure pictures
(
	  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	, p_editMode			in boolean										default false
) is begin
	ext_lob.pri(org_lib.pictures(p_organisation_pk, p_editMode));
end;

function pictures
(
	  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	, p_editMode			in boolean										default false
) return clob
is
begin
declare
	c_procedure				constant mod_procedure.name%type 	:= 'pictures';

	v_html			clob;
	v_clob			clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := v_html||''||pic_lib.pictures(p_organisation_pk, p_editMode)||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end pictures;


-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
--
-- Other procedures
--
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        getOrgPk
-- what:        returns the PK of the organisation related
--				the user that is logged in
-- author:      Frode Klevstul
-- start date:  23.11.2005
---------------------------------------------------------
function getOrgPk
(
	p_use_user_pk			in use_user.use_user_pk%type
) return org_organisation.org_organisation_pk%type
is
begin
declare
	c_procedure				constant mod_procedure.name%type 	:= 'getOrgPk';
	c_id					constant ses_session.id%type 		:= ses_lib.getId;

	cursor	cur_use_org
	(
		b_use_user_pk	use_user.use_user_pk%type
	) is
	select	org_organisation_fk_pk
	from	use_org
	where	use_user_fk_pk = b_use_user_pk;

	v_organisation_pk	org_organisation.org_organisation_pk%type;
begin
	ses_lib.ini(g_package, c_procedure);

	for r_cursor in cur_use_org(p_use_user_pk)
	loop
		v_organisation_pk := r_cursor.org_organisation_fk_pk;
	end loop;

    return v_organisation_pk;
end;
end getOrgPk;


---------------------------------------------------------
-- name:        getOrgName
-- what:        returns the name of the organisation related
--				the user that is logged in
-- author:      Frode Klevstul
-- start date:  23.11.2005
---------------------------------------------------------
function getOrgName
(
	p_organisation_pk		in org_organisation.org_organisation_pk%type
) return org_organisation.name%type
is
begin
declare
	c_procedure				constant mod_procedure.name%type 	:= 'getOrgName';

	cursor	cur_org_organisation
	(
		b_organisation_pk	org_organisation.org_organisation_pk%type
	) is
	select	name
	from	org_organisation
	where	org_organisation_pk = b_organisation_pk;

	v_name				org_organisation.name%type;
begin
	ses_lib.ini(g_package, c_procedure);

	for r_cursor in cur_org_organisation(p_organisation_pk)
	loop
		v_name := r_cursor.name;
	end loop;

    return htm_lib.HTMLEncode(v_name);
end;
end getOrgName;


---------------------------------------------------------
-- name:        getOrgZip
-- what:        returns the org's zip
-- author:      Frode Klevstul
-- start date:  08.03.2006
---------------------------------------------------------
function getOrgZip
(
	p_organisation_pk		in org_organisation.org_organisation_pk%type
) return add_address.zip%type
is
begin
declare
	c_procedure				constant mod_procedure.name%type 	:= 'getOrgZip';

	cursor	cur_org_organisation
	(
		b_organisation_pk	org_organisation.org_organisation_pk%type
	) is
	select	zip
	from	org_organisation o, add_address a
	where	o.org_organisation_pk = b_organisation_pk
	and		o.add_address_fk = a.add_address_pk;

	v_zip					add_address.zip%type;
begin
	ses_lib.ini(g_package, c_procedure);

	for r_cursor in cur_org_organisation(p_organisation_pk)
	loop
		v_zip := r_cursor.zip;
	end loop;

    return v_zip;
end;
end getOrgZip;


---------------------------------------------------------
-- name:        getOrgNo
-- what:        returns the org's org number
-- author:      Frode Klevstul
-- start date:  17.08.2006
---------------------------------------------------------
function getOrgNo
(
	p_organisation_pk		in org_organisation.org_organisation_pk%type
) return org_organisation.org_number%type
is
begin
declare
	c_procedure				constant mod_procedure.name%type 	:= 'getOrgNo';

	cursor	cur_org_organisation
	(
		b_organisation_pk	org_organisation.org_organisation_pk%type
	) is
	select	org_number
	from	org_organisation o
	where	o.org_organisation_pk = b_organisation_pk;

	v_org_number			org_organisation.org_number%type;
begin
	ses_lib.ini(g_package, c_procedure);

	for r_cursor in cur_org_organisation(p_organisation_pk)
	loop
		v_org_number := r_cursor.org_number;
	end loop;

    return v_org_number;
end;
end getOrgNo;


---------------------------------------------------------
-- name:        initialiseOrganisation
-- what:        initiate an organisation
-- author:      Frode Klevstul
-- start date:  29.12.2005
---------------------------------------------------------
procedure initialiseOrganisation
(
	  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
)
is
begin
declare
	c_procedure			constant mod_procedure.name%type	:= 'initialiseOrganisation';

	e_stop				exception;

	c_no_photo			constant sys_parameter.value%type 	:= sys_lib.getParameter('no_photo');
	c_no_logo			constant sys_parameter.value%type 	:= sys_lib.getParameter('no_logo');

	c_dir_img_org		constant sys_parameter.value%type 	:= sys_lib.getParameter('dir_img_org')||'_'||p_organisation_pk;
	c_dir_org_imgbase	constant sys_parameter.value%type 	:= sys_lib.getParameter('dir_org_imgbase');

	c_dir_modOwa		constant sys_parameter.value%type 	:= sys_lib.getParameter('dir_modOwa');
	c_ext_geocode		constant sys_parameter.value%type 	:= sys_lib.getParameter('ext_geocode');

	v_count				number;

	v_address_pk		add_address.add_address_pk%type;
	v_geo_geography_pk	geo_geography.geo_geography_pk%type;
	v_zip				add_address.zip%type;
	v_ext_geocode		varchar2(256);
	v_request_uri		varchar2(256);
	v_sysrun_res		varchar2(1024);
	v_orgSubdir			number;
	v_orgSubdirName		varchar2(16);
	v_dir_orgFullPath	sys_parameter.value%type;

	cursor	cur_add_address is
	select	add_address_fk
	from	org_organisation
	where	org_organisation_pk = p_organisation_pk;

	cursor	cur_add_address_2
	(
		b_add_address_pk add_address.add_address_pk%type
	) is
	select	zip
	from	add_address
	where	add_address_pk = b_add_address_pk
	and		geo_geography_fk is null;

	cursor	cur_address_geocode
	(
		b_organisation_pk org_organisation.org_organisation_pk%type
	) is
	select	add_address_pk, street, zip
	from	add_address, org_organisation
	where	add_address_pk = add_address_fk
	and		org_organisation_pk = b_organisation_pk;

	cursor	cur_org_extra
	(
		b_organisation_pk org_organisation.org_organisation_pk%type
	) is
	select	org_subdir
	from	org_extra
	where	org_organisation_fk_pk = b_organisation_pk;

	v_sql_stmt			varchar2(32767);

begin
	ses_lib.ini(g_package, c_procedure);

	if (p_organisation_pk is null) then
		return;
	end if;

	-- try to get the sub directory from the database (if it exists)
	for r_cursor in cur_org_extra(p_organisation_pk)
	loop
		v_orgSubdir := r_cursor.org_subdir;
	end loop;

	if (v_orgSubdir is null) then
		-- generate random number used as sub-directory
		select	round(dbms_random.value(1,100)) num
		into	v_orgSubdir
		from	dual;

		insert into org_extra
		(org_organisation_fk_pk, org_subdir)
		values (p_organisation_pk, v_orgSubdir);

		commit;
	end if;

	v_orgSubdirName := 'sub_'||v_orgSubdir;

	-- build full path for this organisation's uploaded pictures
	v_dir_orgFullPath := c_dir_org_imgbase||'/'||v_orgSubdirName||'/org_'||p_organisation_pk;

	-- ----------------------------------------------------------------
	-- create the organisations own directory to upload files into
	-- ----------------------------------------------------------------
	select	count(*)
	into	v_count
	from	sys.all_directories
	where	directory_name = c_dir_img_org;

	if (v_count = 0) then
		v_sql_stmt := 'create or replace directory '|| c_dir_img_org  ||' as '''|| v_dir_orgFullPath ||'''';
		execute immediate v_sql_stmt;

		-- create subdir under org main directory
		select	sysrun('mkdir '||c_dir_org_imgbase||'/'||v_orgSubdirName)
		into	v_sysrun_res
		from	dual;

		-- create directory for this org's images
		select	sysrun('mkdir '||v_dir_orgFullPath)
		into	v_sysrun_res
		from	dual;

		-- create directory for backed up images
		select	sysrun('mkdir '||v_dir_orgFullPath||'/backup')
		into	v_sysrun_res
		from	dual;

	end if;

	-- ------------------------
	-- geocode
	-- ------------------------
	select	count(*)
	into	v_count
	from	org_organisation, add_address
	where	add_address_fk = add_address_pk
	and		org_organisation_pk = p_organisation_pk
	and		longitude is not null
	and		latitude is not null;

/*	if (v_count = 0) then
		v_ext_geocode := c_ext_geocode;
		v_request_uri := owa_util.get_cgi_env('request_uri');

		for r_cursor in cur_address_geocode(p_organisation_pk)
		loop
			-- build a proper return url
			v_ext_geocode := replace(v_ext_geocode, '[street]', replace(htm_lib.stripIllegalTextFromUrl(r_cursor.street), ' ', '+'));
			v_ext_geocode := replace(v_ext_geocode, '[zip]', r_cursor.zip);
			v_ext_geocode := replace(v_ext_geocode, '[address_pk]', r_cursor.add_address_pk);
			v_ext_geocode := replace(v_ext_geocode, '[returnUrl]', v_request_uri);
			v_ext_geocode := replace(v_ext_geocode, '[addLongLatUrl]', substr(v_request_uri, 0, instr(v_request_uri, '/', 2))||'add_lib.addLongLat');
		end loop;

		-- jumps to script that tries to find geocode for the address
		--htp.p(v_ext_geocode);
		htm.jumpTo(v_ext_geocode, 0, '...');

		raise e_stop;
	end if;
*/
	-- ------------------------
	-- opening hours
	-- ------------------------
	update	org_hours
	set		org_organisation_fk_pk = p_organisation_pk
	where	org_organisation_fk_pk = p_organisation_pk;

	if (sql%notfound) then
		insert into org_hours (org_organisation_fk_pk)
		values(p_organisation_pk);
	end if;

	-- -----------------------
	-- logo
	-- -----------------------
	select	count(*)
	into	v_count
	from	org_organisation
	where	org_organisation_pk = p_organisation_pk
	and		path_logo is null;

	if (v_count > 0) then
		update	org_organisation
		set		path_logo = c_no_logo
		where	org_organisation_pk = p_organisation_pk;
	end if;

	-- -----------------------
	-- main photo
	-- -----------------------
	select	count(*)
	into	v_count
	from	org_organisation
	where	org_organisation_pk = p_organisation_pk
	and		path_mainphoto is null;

	if (v_count > 0) then
		update	org_organisation
		set		path_mainphoto = c_no_photo
		where	org_organisation_pk = p_organisation_pk;
	end if;

	-- ------------------------
	-- address
	-- ------------------------
	for r_cursor in cur_add_address
	loop
		v_address_pk := r_cursor.add_address_fk;
	end loop;

	if (v_address_pk is null) then
		v_address_pk := sys_lib.getEmptyPk('add_address');

		insert into add_address
		(add_address_pk, street, date_insert, date_update)
		values (v_address_pk, '...', sysdate, sysdate);

		update	org_organisation
		set		add_address_fk = v_address_pk
		where	org_organisation_pk = p_organisation_pk;
	else
		select	count(*)
		into	v_count
		from	add_address
		where	add_address_pk = v_address_pk;

		if (v_count = 0) then
			insert into add_address
			(add_address_pk, street, date_insert, date_update)
			values (v_address_pk, '...', sysdate, sysdate);
		end if;
	end if;

	-- ------------------------
	-- geography
	-- ------------------------
	for r_cursor in cur_add_address_2(v_address_pk)
	loop
		v_zip := r_cursor.zip;
	end loop;

	if (v_zip is not null) then
		-- as long as we use max() we won't
		-- get a "no data found" error
		select	max(geo_geography_pk)
		into	v_geo_geography_pk
		from	geo_geography, geo_zip
		where	geo_geography_pk = geo_geography_fk_pk
		and		zip_pk = v_zip;

		update	add_address
		set		geo_geography_fk = v_geo_geography_pk
		where	add_address_pk = v_address_pk;
	end if;

	commit;

end;
end initialiseOrganisation;


---------------------------------------------------------
-- name:        hasStatus
-- what:        check organisation status
-- author:      Frode Klevstul
-- start date:  03.01.2006
---------------------------------------------------------
function hasStatus
(
	  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	, p_type				in org_status.name%type							default null
) return boolean
is
begin
declare
	c_procedure			constant mod_procedure.name%type	:= 'hasStatus';

	type cur_type is ref cursor;
	cur cur_type;

	v_count				number;
	v_sql_stmt			varchar2(32767);

begin
	ses_lib.ini(g_package, c_procedure);

	if (p_organisation_pk is null) then
		return false;
	end if;

	v_sql_stmt := ' select count(*)'||
				'	from org_organisation'||
				'	where org_organisation_pk ='||p_organisation_pk||
				'	and org_status_fk = (select org_status_pk from org_status where name='''||p_type||''')';

	open cur for v_sql_stmt;
	loop
		fetch cur into v_count;
		exit when cur%notfound;
	end loop;
	close cur;

	if (v_count>0) then
		return true;
	else
		return false;
	end if;

end;
end hasStatus;


---------------------------------------------------------
-- name:        rankOrganisations
-- what:        lists out organisations different ways
-- author:      Frode Klevstul
-- start date:  03.01.2006
---------------------------------------------------------
procedure rankOrganisations(p_number in number default null, p_type in varchar2 default null) is begin
	ext_lob.pri(org_lib.rankOrganisations(p_number, p_type));
end;

function rankOrganisations
(
	  p_number				in number										default null
	, p_type				in varchar2										default null
) return clob
is
begin
declare
	c_procedure				constant mod_procedure.name%type 	:= 'rankOrganisations';

	c_tex_newestOrgs		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'newestOrgs');
	c_tex_newestUpdated		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'newestUpdated');
	c_tex_mostPopular		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'mostPopular');

	c_tex_number			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'number');
	c_tex_hits				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'hits');
	c_tex_organisation		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'organisation');
	c_tex_geography			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'geography');


	cur_dynCursor			sys_refcursor;

	v_array					parameter_arr						default empty_array;
	v_ok					boolean								:= false;
	v_tex_noHits			tex_text.string%type;

	v_sql_stmt				varchar2(32767);
	v_sql_stmt_tmp			varchar2(32767);
	v_html					clob;
	v_clob					clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	if (p_number is not null) then


		v_sql_stmt :=   ' select org_organisation_pk, o.name, zip'||
						' from org_organisation o, add_address a'||
						' [WHERE]'||
						' [ORDER_BY]';

		-- --------------------------------------
		-- newly registered organisations
		-- --------------------------------------
		if (p_type = 'newlyRegistered') then
			v_ok := true;

			v_sql_stmt_tmp := v_sql_stmt;
			v_sql_stmt_tmp := replace(v_sql_stmt_tmp, '[WHERE]', 'where add_address_fk = add_address_pk and org_status_fk = (select org_status_pk from org_status where name = ''member'')');
			v_sql_stmt_tmp := replace(v_sql_stmt_tmp, '[ORDER_BY]', 'order by o.date_status_change desc');

			v_html := v_html||''||htm.bBox(c_tex_newestOrgs);
		-- --------------------------------------
		-- newly updated organisations
		-- --------------------------------------
		elsif (p_type = 'newlyUpdated') then
			v_ok := true;

			v_sql_stmt_tmp := v_sql_stmt;
			v_sql_stmt_tmp := replace(v_sql_stmt_tmp, '[WHERE]', 'where add_address_fk = add_address_pk and org_status_fk = (select org_status_pk from org_status where name = ''member'')');
			v_sql_stmt_tmp := replace(v_sql_stmt_tmp, '[ORDER_BY]', 'order by o.date_update desc');

			v_html := v_html||''||htm.bBox(c_tex_newestUpdated)||chr(13);
		-- --------------------------------------
		-- most popular organisations
		-- --------------------------------------
		elsif (p_type = 'mostPopular') then
			v_ok := true;

			v_sql_stmt_tmp := v_sql_stmt;
			v_sql_stmt_tmp := replace(v_sql_stmt_tmp, '[WHERE]', ', log_hits h where table_name = ''org_organisation'' and foreign_key_fk = org_organisation_pk and add_address_fk = add_address_pk and org_status_fk = (select org_status_pk from org_status where name = ''member'')');
			v_sql_stmt_tmp := replace(v_sql_stmt_tmp, '[ORDER_BY]', 'order by hits desc');

			v_html := v_html||''||htm.bBox(c_tex_mostPopular)||chr(13);
		end if;


		-- --------------------------------------------------
		-- only do this if we have generated a proper SQL
		-- --------------------------------------------------
		if (v_ok) then

			-- ---------------------
			-- number of hits
			-- ---------------------
			-- if (p_type = 'mostPopular') then
			--	v_tex_noHits := c_tex_hits;
			-- else
			--	v_tex_noHits := c_tex_number;
			-- end if;

			v_html := v_html||''||htm.bTable||chr(13);
			-- ---------------------
			-- heading
			-- ---------------------
			--v_html := v_html||'	<tr>
			--						<td>'||v_tex_noHits||'</td>
			--						<td><span style="padding-left: 10px;">&nbsp;</span>'||c_tex_organisation||'</td>
			--						<td>'||c_tex_geography||'</td>
			--					</tr>';

			open cur_dynCursor for v_sql_stmt_tmp;
			loop
				fetch cur_dynCursor into v_array(1), v_array(2), v_array(3);
				exit when cur_dynCursor%notfound;

				-- if (p_type = 'mostPopular') then
				--	v_html := v_html||'<tr><td>'||log_lib.getHits('org_organisation', v_array(1))||':</td><td><span style="padding-left: 10px;">&nbsp;</span>'||htm.link(v_array(2),'org_pub.viewOrg?p_organisation_pk='||v_array(1))||'</td><td>('||geo_lib.getCity(v_array(3))||')</td></tr>';
				-- else
					v_html := v_html||'<tr><td>'||cur_dynCursor%rowcount||':</td><td><span style="padding-left: 10px;">&nbsp;</span>'||htm.link(v_array(2),'org_pub.viewOrg?p_organisation_pk='||v_array(1))||'</td><td>('||geo_lib.getCity(v_array(3))||')</td></tr>'||chr(13);
				-- end if;

				exit when cur_dynCursor%rowcount=p_number;
			end loop;
			close cur_dynCursor;

			v_html := v_html||''||htm.eTable||chr(13);
			v_html := v_html||''||htm.eBox||chr(13);
		end if;

	else
		v_html := v_html||'';
	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end rankOrganisations;


---------------------------------------------------------
-- name:        chosenOrganisation
-- what:        show the chosen organisation
-- author:      Frode Klevstul
-- start date:  04.01.2006
---------------------------------------------------------
procedure chosenOrganisation is begin
	ext_lob.pri(org_lib.chosenOrganisation);
end;

function chosenOrganisation
	return clob
is
begin
declare
	c_procedure				constant mod_procedure.name%type 					:= 'chosenOrganisation';

	c_tex_moreInfo			constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'moreInfo');

	v_directory_path		sys.all_directories.directory_path%type;
	v_general_information	org_organisation.general_information%type;

	cursor	cur_org_chosen is
	select	org_organisation_fk_pk
	from	org_chosen
	where	date_start_pk < sysdate
	and		date_stop > sysdate
	order by date_stop asc;

	cursor	cur_org_organisation
	(
		b_org_organisation_pk	org_organisation.org_organisation_pk%type
	)is
	select	o.name, general_information, path_logo, path_mainphoto, zip
	from	org_organisation o, add_address a
	where	org_organisation_pk	= b_org_organisation_pk
	and		add_address_fk	= add_address_pk;

	v_html					clob;
	v_clob					clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := v_html||''||htm.bTable('surroundElements')||'<tr>';
	for r_cursor in cur_org_chosen
	loop
		v_directory_path := org_lib.getDirectoryPath(r_cursor.org_organisation_fk_pk, true);

		if (cur_org_chosen%rowcount = 1) then
			v_html := v_html||'<td class="surroundElements" width="33%">'||chr(13);
		elsif (cur_org_chosen%rowcount = 2) then
			v_html := v_html||'<td class="surroundElements" width="34%">'||chr(13);
		elsif (cur_org_chosen%rowcount = 3) then
			v_html := v_html||'<td class="surroundElements" width="33%">'||chr(13);
		end if;

		for r_cursor2 in cur_org_organisation(r_cursor.org_organisation_fk_pk)
		loop
			v_html := v_html||''||htm.bBox(htm_lib.HTMLEncode(r_cursor2.name)||' ('||geo_lib.getCity(r_cursor2.zip)||')')||chr(13);
			v_html := v_html||''||htm.bTable||chr(13);
			v_html := v_html||'
				<tr>
					<td style="background-image:url('||v_directory_path||'/'||r_cursor2.path_mainphoto||');" colspan="2" onclick="document.location.href=''org_pub.viewOrg?p_organisation_pk='||r_cursor.org_organisation_fk_pk||''';">
						<br /><br />
						<br /><br />
					</td>
				</tr>';
			v_html := v_html||'<tr>
								<td height="100" width="40%" rowspan="1" style="vertical-align: top;">
									<br /><a href="org_pub.viewOrg?p_organisation_pk='||r_cursor.org_organisation_fk_pk||'"><img border="0" src="'||v_directory_path||'/'||r_cursor2.path_logo||'" alt=""/></a>
								</td>
								<td>'||chr(13);

							v_general_information := htm_lib.stripHTML(r_cursor2.general_information);

							if (length(v_general_information) > 150) then
								v_general_information := ext_lib.abbreviation(v_general_information, 150);
								v_html := v_html||''||v_general_information||chr(13);
							else
								v_html := v_html||''||v_general_information||chr(13);
							end if;

			v_html := v_html||'
								&nbsp;</td>
							  </tr>
							  <tr>
								<td style="text-align: right;" colspan="2">'||htm.link(c_tex_moreInfo,'org_pub.viewOrg?p_organisation_pk='||r_cursor.org_organisation_fk_pk,'_self', 'theButtonStyle')||'
								&nbsp;</td>
							  </tr>
							  <tr>
							  <td colspan="2" height="5"></td>
							  </tr>';
			v_html := v_html||''||htm.eTable||chr(13);
			v_html := v_html||''||htm.eBox||chr(13);
		end loop;

		v_html := v_html||'</td>'||chr(13);
	end loop;
	v_html := v_html||'</tr>'||htm.eTable||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end chosenOrganisation;


---------------------------------------------------------
-- name:        isParent
-- what:        check if org is parent
-- author:      Frode Klevstul
-- start date:  26.04.2006
---------------------------------------------------------
function isParent
(
	  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
) return boolean
is
begin
declare
	c_procedure			constant mod_procedure.name%type	:= 'isParent';

	v_count				number;

begin
	ses_lib.ini(g_package, c_procedure);

	if (p_organisation_pk is null) then
		return false;
	end if;

	select	count(*)
	into	v_count
	from	org_organisation
	where	org_organisation_pk = p_organisation_pk;

	if (v_count>0) then
		return true;
	else
		return false;
	end if;

end;
end isParent;


---------------------------------------------------------
-- name:        organisationPicker
-- what:        select an organisation
-- author:      Frode Klevstul
-- start date:  15.09.2006
---------------------------------------------------------
procedure organisationPicker
(
	  p_previousUrl				varchar2									default null
	, p_nextUrl					varchar2									default null
	, p_type					varchar2									default null
	, p_org_organisation_pk		org_organisation.org_organisation_pk%type	default null
	, p_service_pk				ser_service.ser_service_pk%type				default null
	, p_name					varchar2									default null
) is begin
	ext_lob.pri(org_lib.organisationPicker(p_previousUrl, p_nextUrl, p_type, p_org_organisation_pk, p_service_pk, p_name));
end;

function organisationPicker
(
	  p_previousUrl				varchar2									default null
	, p_nextUrl					varchar2									default null
	, p_type					varchar2									default null
	, p_org_organisation_pk		org_organisation.org_organisation_pk%type	default null
	, p_service_pk				ser_service.ser_service_pk%type				default null
	, p_name					varchar2									default null
) return clob
is
begin
declare
	c_procedure				constant mod_procedure.name%type 	:= 'organisationPicker';

	v_html					clob;
	v_clob					clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := v_html||''||htm.bPage(g_package, c_procedure, 'admin')||chr(13);
	v_html := v_html||'<center>'||chr(13);
	v_html := v_html||''||htm.bTable('surroundAdminBox')||'<tr><td>'||chr(13);
	v_html := v_html||''||htm.bTable||'<tr><td>'||chr(13);

	v_html := v_html||''||htm.bForm(g_package||'.'||c_procedure, 'form1')||chr(13);
    v_html := v_html||' <input type="hidden" name="p_previousUrl" value="'|| p_previousUrl ||'" />'||chr(13);
    v_html := v_html||' <input type="hidden" name="p_nextUrl" value="'|| p_nextUrl ||'" />'||chr(13);
    v_html := v_html||' <input type="hidden" name="p_type" value="'|| p_type ||'" />'||chr(13);
    v_html := v_html||' <input type="hidden" name="p_org_organisation_pk" value="'|| p_org_organisation_pk ||'" />'||chr(13);
    v_html := v_html||' <input type="hidden" name="p_service_pk" value="'|| p_service_pk ||'" />'||chr(13);
	v_html := v_html||''||htm.bBox('Set name')||chr(13);
	v_html := v_html||''||htm.bTable||chr(13);
	v_html := v_html||'
		<tr>
			<td>
				Where name like %<input type="text" name="p_name" size="10" maxlength="30" value="'||p_name||'">%
			</td>
			<td>
				<input type="submit" value="find">
			</td>
		</tr>
		'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||''||htm.eBox||chr(13);
	v_html := v_html||''||htm.eForm||chr(13);

	if (p_name is not null) then

		v_html := v_html||'<br /><br />'||chr(13);
		v_html := v_html||''||htm.bForm(p_nextUrl, 'form2')||chr(13);
		v_html := v_html||''||htm.bBox('Chose organisation')||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);
		v_html := v_html||'
			<tr>
				<td>
					Organisation list: '||htm_lib.selectOrganisation(p_type, p_org_organisation_pk, p_service_pk, p_name)||'
				</td>
				<td>
					<input type="submit" value="select organisation">
				</td>
			</tr>
			'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);
		v_html := v_html||''||htm.eForm||chr(13);
	end if;

	v_html := v_html||'</td></tr>'||chr(13);
	v_html := v_html||'<tr><td>&nbsp;</td></tr>'||chr(13);
	v_html := v_html||'<tr><td style="padding-left: 20px;">'||htm.link('Go back', p_previousUrl, '_self', 'theButtonStyle')||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||'</center>'||chr(13);
	v_html := v_html||''||htm.ePage||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end organisationPicker;


---------------------------------------------------------
-- name:        sameParentOrg
-- what:        show organisations having same parent
-- author:      Frode Klevstul
-- start date:  16.10.2006
---------------------------------------------------------
procedure sameParentOrg
(
	p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
) is begin
	ext_lob.pri(org_lib.sameParentOrg(p_organisation_pk));
end;

function sameParentOrg
(
	p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
) return clob
is
begin
declare
	c_procedure			constant mod_procedure.name%type	:= 'sameParentOrg';

	c_tex_sameParentOrg	constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'sameParentOrg');

	v_index				number								:= 1;

	cursor	cur_org_organisation is
	select	org_organisation_pk, name
	from	org_organisation
	where	(
				bool_umbrella_organisation is null
				or bool_umbrella_organisation = 0
			)
	and		parent_org_organisation_fk =
				(
					select parent_org_organisation_fk from org_organisation where org_organisation_pk = p_organisation_pk
				)
	and		parent_org_organisation_fk <> 900
	and		org_organisation_pk <> p_organisation_pk
	and		org_status_fk <> (select org_status_pk from org_status where lower(name) = 'banned');

	v_html				clob;
	v_clob				clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	if (p_organisation_pk is null) then
		return null;
	end if;

	for r_cursor in cur_org_organisation
	loop

		if (v_index = 1) then
			v_html := v_html||''||htm.bBox(c_tex_sameParentOrg)||chr(13);
			v_html := v_html||''||htm.bTable||chr(13);
		end if;

		v_html := v_html||'
			<tr>
				<td>
					&raquo; '||htm.link(r_cursor.name,'org_pub.viewOrg?p_organisation_pk='||r_cursor.org_organisation_pk)||'
				</td>
			</tr>
		';

		v_index := v_index + 1;
	end loop;

	if (v_index > 1) then
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);
	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end sameParentOrg;


---------------------------------------------------------
-- name:        getDirectoryPath
-- what:        get organisations directory path
-- author:      Frode Klevstul
-- start date:  29.10.2006
---------------------------------------------------------
procedure getDirectoryPath
(
	  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	, p_webpath				in boolean										default false
) is begin
	htp.p(org_lib.getDirectoryPath(p_organisation_pk, p_webpath));
end;

function getDirectoryPath
(
	  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	, p_webpath				in boolean										default false
) return all_directories.directory_path%type
is
begin
declare
	c_procedure			constant mod_procedure.name%type	:= 'getDirectoryPath';

	c_dir_img_org		constant sys_parameter.value%type 	:= sys_lib.getParameter('dir_img_org')||'_'||p_organisation_pk;
	c_img_org			constant sys_parameter.value%type 	:= sys_lib.getParameter('img_org');

	v_directory_path	sys.all_directories.directory_path%type;

	cursor	all_directories is
	select	directory_path
	from	all_directories
	where	directory_name = c_dir_img_org;

begin
	ses_lib.ini(g_package, c_procedure);

	if (p_organisation_pk is null) then
		return null;
	end if;

	for r_cursor in all_directories
	loop
		v_directory_path := r_cursor.directory_path;
	end loop;

	-- change the path so it'll be a web-path in stead of an OS path
	if (p_webpath) then
		v_directory_path	:= regexp_replace(v_directory_path, '^(.*)('||c_img_org||'.*)$', '\2', 1, 1, 'n');
	end if;

	return v_directory_path;

end;
end getDirectoryPath;


-- ******************************************** --
end;
/
show errors;
