PROMPT *** con_xca ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package con_xca is
/* ******************************************************
*	Package:     log_lib
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	060616 | Frode Klevstul
*			- Package created
****************************************************** */
	type parameter_arr					is table of varchar2(100) index by binary_integer;
	empty_array							parameter_arr;

	procedure run;
	procedure show
	(
		p_date		in varchar2			default NULL,
		p_d_date	in varchar2			default NULL,
		p_region	in parameter_arr	default empty_array,
		p_area		in parameter_arr	default empty_array,
		p_type		in parameter_arr	default empty_array,
		p_string	in varchar2			default NULL,
		p_submit	in varchar2			default NULL
	);
	function cal_form
	(
		p_date		in varchar2			default NULL,
		p_d_date	in varchar2			default NULL,
		p_region	in parameter_arr	default empty_array,
		p_area		in parameter_arr	default empty_array,
		p_type		in parameter_arr	default empty_array,
		p_string	in varchar2			default NULL,
		p_submit	in varchar2			default NULL
	) return clob;

end;
/
show errors;





-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body con_xca is


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'con_xca';

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  24.07.2005
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        show
-- what:        "What's On"
-- author:      Espen Messel
-- start date:  20.05.2006
---------------------------------------------------------
procedure show
(
	p_date		in varchar2			default null,
	p_d_date	in varchar2			default null,
	p_region	in parameter_arr	default empty_array,
	p_area		in parameter_arr	default empty_array,
	p_type		in parameter_arr	default empty_array,
	p_string	in varchar2			default null,
	p_submit	in varchar2			default null
)
is
begin
declare
	c_procedure						constant mod_procedure.name%type := 'show';

	c_dyn_cursor					sys_refcursor;
	c_dateformat					constant sys_parameter.value%type		:= sys_lib.getparameter('dateFormat_ymd');

	c_tex_searchResult				constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'searchResult');
	c_tex_nextEvents				constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'nextEvents');
	c_tex_when						constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'when');
	c_tex_what						constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'what');
	c_tex_organisation				constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'organisation');
	c_tex_location					constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'location');
	c_tex_type						constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'type');

	v_sql_stmt						varchar2(2048);
	v_date_start					varchar2(32);
	v_totalcursize					number									:= 100;
	v_con_content_pk				con_content.con_content_pk%type			default null;
	v_subject						con_content.subject%type				default null;
	v_subtype_name					con_subtype.name%type					default null;
	v_org_organisation_fk			con_content.org_organisation_fk%type	default null;
	v_name							org_organisation.name%type				default null;
	v_geo_name						geo_geography.name%type					default null;
	v_path_logo						org_organisation.path_logo%type			default null;
	v_path							con_file.path%type						default null;
	v_date							varchar2(20)							:= to_char(sysdate,'YYYYMMDD');
	v_index							number									:= 0;
	v_heading						varchar2(100)							default null;

begin
	ses_lib.ini(g_package, c_procedure, 'p_date='||p_date||'��p_d_date='||p_d_date);

	if ( p_d_date is not null ) then
		v_date := p_d_date;
	end if;

	-- Lager stammen i sql'en. Joiner inn alle tabeller som jeg trenger data fra.
	v_sql_stmt := ''||
		' select cc.con_content_pk, cc.subject, to_char(cc.date_start, '''||c_dateFormat||''') as date_start,'||
        ' cc.org_organisation_fk, oo.Name, gg.name, oo.path_logo, cs.name'||
        ' from con_content cc'||
        ' join org_organisation oo on cc.org_organisation_fk = oo.org_organisation_pk'||
        ' left outer join con_subtype cs ON cc.con_subtype_fk = cs.con_subtype_pk'||
        ' left outer join add_address aa on oo.add_address_fk = aa.add_address_pk'||
        ' left outer join geo_geography gg on aa.geo_geography_fk = gg.geo_geography_pk'||
        ' where cc.con_status_fk = (select con_status_pk from con_status where name =''published'')'||
	' and cc.con_type_fk in (select con_type_pk from con_type where lower(name) = ''event_org'')'||
        ' and (cc.date_publish < sysdate or cc.date_publish is null)';

        -- Kun valgt dato
	if ( p_submit = 'justDate' and p_date is not null ) then
		v_sql_stmt := v_sql_stmt ||'and to_char(cc.date_start, ''YYYYMMDD'') = ''' || p_date || '''' || chr(13);

	-- Kun fritekst
	elsif ( p_submit = 'freeText' AND p_string is not null ) then
		v_sql_stmt := v_sql_stmt ||'and ( upper(cc.subject) like upper(''%' || p_string || '%'') or upper(cc.ingress) like upper(''%' || p_string || '%'') or upper(cc.body) like upper(''%' || p_string || '%'') or upper(cc.footer) like upper(''%' || p_string || '%'')) ' || chr(13) || 'and ( cc.date_start > (sysdate-(60/1440)) or cc.date_stop > sysdate-(60/1440) )' || chr(13);

	-- Kj�rer fullt s�k.
	elsif ( p_submit = 'fullSearch' ) then
		if ( p_date is not null ) then
			v_sql_stmt := v_sql_stmt ||'and to_char(cc.date_start, ''YYYYMMDD'') = ''' || p_date || '''' || chr(13);
		else
			v_sql_stmt := v_sql_stmt ||'and ( cc.date_start > (sysdate-(60/1440)) or cc.date_stop > sysdate-(60/1440) )' || chr(13);
		end if;

		-- Legger til s�k p� subtype.
		if ( p_type.count > 0 ) then
			v_sql_stmt := v_sql_stmt ||'and cc.con_subtype_fk in (';

			for v_index in 1 .. p_type.count
			loop
				if ( p_type(v_index) is not null ) then
					v_sql_stmt := v_sql_stmt || p_type(v_index) || ',';
				end if;
			end loop;

			v_sql_stmt := trim(trailing ',' from v_sql_stmt );
			v_sql_stmt := v_sql_stmt ||')' || chr(13);
		end if;

		-- Henter du valgte kommunene
		if ( p_area.count > 0 ) then
			v_sql_stmt := v_sql_stmt ||'and gg.geo_geography_pk in (';
			v_index := p_area.first;

			while v_index is not null
			loop
				if ( p_area(v_index) != ' ' ) then
					v_sql_stmt := v_sql_stmt || p_area(v_index) || ',';
				end if;
				v_index := p_area.next(v_index);
			end loop;

			v_sql_stmt := trim(trailing ',' from v_sql_stmt );
			v_sql_stmt := v_sql_stmt ||')' || chr(13);
		end if;

		-- fjerner tom kommune i sql'en
		v_sql_stmt := replace(v_sql_stmt, 'and gg.geo_geography_pk in ()');

   -- Har ikke s�kt. Viser de XX f�rste
	else
	   v_sql_stmt := v_sql_stmt ||'and ( cc.date_start > (sysdate-(60/1440)) or cc.date_stop > sysdate-(60/1440) )' || chr(13);
	   v_totalCurSize := 35;
	end if;

	v_sql_stmt := v_sql_stmt ||'order by cc.date_start asc' || chr(13);

	-- setter riktig header i resultat boksen
	if ( p_submit is not null ) then
		v_heading := c_tex_searchResult;
	else
		v_heading := v_totalcursize||' '||c_tex_nextEvents;
	end if;

	-- Skriver ut header.
	htp.p(htm.bPage(g_package, c_procedure));
	htp.p(htm.bTable('surroundElements'));
	htp.p('<tr><td width="80%" class="surroundElements">' || htm.bBox(v_heading) || chr(13) || htm.bTable || chr(13));
	htp.p('
		<tr>
			<td width="10%">'||c_tex_when||'</td>
			<td width="50%">'||c_tex_what||'</td>
			<td width="20%">'||c_tex_organisation||'</td>
			<td width="10%">'||c_tex_location||'</td>
			<td width="10%">'||c_tex_type||'</td>
		</tr>
	');

	--        Brukes til � vise sql'en under debug.
	--        htp.p('<tr><td colspan="4">'||v_sql_stmt||'</td></tr>');

	-- G�r igjennom resultatet av s�ket. Skriver det rett ut p� siden.
	open c_dyn_cursor for v_sql_stmt;
	loop
		fetch c_dyn_cursor into v_con_content_pk, v_subject, v_date_start, v_org_organisation_fk, v_name, v_geo_name, v_path_logo, v_subtype_name;
		exit when c_dyn_cursor%notfound;
		htp.p('<tr><td style="vertical-align:top;">' || v_date_start || '</td>');
		htp.p('<td style="vertical-align:top;"><a href="con_pub.viewCon?p_content_pk='|| v_con_content_pk ||'">' || v_subject || '</a></td>');
		htp.p('<td style="vertical-align:top;"><a href="org_pub.viewOrg?p_organisation_pk='|| v_org_organisation_fk ||'">' || v_name || '</a></td>');
		htp.p('<td style="vertical-align:top;">' || v_geo_name || '</td>');
		htp.p('<td style="vertical-align:top;">' || v_subtype_name || '</td></tr>');
		exit when c_dyn_cursor%rowcount = v_totalCurSize;
	end loop;
	close c_dyn_cursor;

	-- Henter s�keformet. Alle data untatt kommuner og fylker skal v�re forh�ndsutfylt slik det var f�r s�ket.
	htp.p(htm.eTable || htm.eBox || '</td><td class="surroundElements">');
	ext_lob.pri(cal_form(p_date, v_date, empty_array, empty_array, p_type, p_string, p_submit));
	htp.p('</td></tr>');
	htp.p(htm.eTable);
	htp.p(htm.ePage);

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end show;


---------------------------------------------------------
-- name:        cal_form
-- what:        "Hva Skjer" search form
-- author:      Espen Messel
-- start date:  20.05.2006
---------------------------------------------------------
function cal_form (
	p_date      in varchar2         default NULL,
	p_d_date    in varchar2         default NULL,
	p_region    in parameter_arr    default empty_array,
	p_area      in parameter_arr    default empty_array,
	p_type      in parameter_arr    default empty_array,
	p_string    in varchar2         default NULL,
	p_submit    in varchar2         default NULL
) return clob
is
begin
declare
	c_procedure			constant mod_procedure.name%type		:= 'cal_form';

	c_dir_scripts		constant sys_parameter.value%type 		:= sys_lib.getParameter('dir_scripts');

	c_tex_search		constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'search');
	c_tex_choseADate	constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'choseADate');
	c_tex_week			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'week');
	c_tex_mon			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'mon');
	c_tex_tue			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'tue');
	c_tex_wed			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'wed');
	c_tex_thu			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'thu');
	c_tex_fri			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'fri');
	c_tex_sat			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'sat');
	c_tex_sun			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'sun');
	c_tex_chosenDay		constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'chosenDay');
	c_tex_removeDate	constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'removeDate');
	c_tex_searchOnDate	constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'searchOnDate');
	c_tex_county		constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'county');
	c_tex_council		constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'council');
	c_tex_eventType		constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'eventType');
	c_tex_doSearch		constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'doSearch');
	c_tex_textSearch	constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'textSearch');
	c_tex_text			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'text');

	v_subtype_pk		con_subtype.con_subtype_pk%type			default null;
	v_subtype_name		con_subtype.name%type					default null;
	v_year				varchar2(20)							:= to_char(to_date(p_d_date,'YYYYMMDD'),'YYYY');
	v_month				varchar2(20)							:= to_char(to_date(p_d_date,'YYYYMMDD'),'MM');
	v_month_name		varchar2(20)							:= to_char(to_date(v_year||v_month||'01','YYYYMMDD'),'MONTH');
	v_week_number		int										:= to_number(to_char(to_date(v_year||v_month||'01','YYYYMMDD'),'IW'));
	v_weekday			int										:= to_number(to_char(to_date(v_year||v_month||'01','YYYYMMDD'),'D'));
	v_nr_days			varchar2(20)							:= to_char(last_day(to_date(p_d_date,'YYYYMMDD')),'DD');
	v_month_prev		varchar(20)								:= to_char(add_months(to_date(v_year||v_month||'01','YYYYMMDD'),-1),'YYYYMMDD');
	v_month_next		varchar(20)								:= to_char(add_months(to_date(v_year||v_month||'01','YYYYMMDD'),+1),'YYYYMMDD');
	v_checked			varchar(20)								default null;
	v_index				int 									:= 0;
	v_nr_weeks			int;
	v_days				int;
	v_day_counter		int										:= 1;
	v_today			int			:= cast(to_char(sysdate,'YYYYMMDD') as int);

	cursor	cur_getContentSubtypes is
	select	con_subtype_pk, name
	from	con_subtype
	order by name;

	v_html				clob;
	v_clob				clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

   -- Regner ut antall uker i m�neden
   v_nr_weeks := ceil( (to_number(v_weekday) + to_number(v_nr_days) - 1)/7);

   v_html := v_html ||''||
		'<script type="text/javascript">'											||chr(13)||
		''																			||chr(13)||
		' function setDate ( p_d_date ) {'											||chr(13)||
        '	var form = document.getElementById(''cal'');'							||chr(13)||
        '	form.p_d_date.value = p_d_date;'										||chr(13)||
        '	form.submit();'															||chr(13)||
		' }'																		||chr(13)||
		''																			||chr(13)||
		' function fullSearch() {'													||chr(13)||
		'	var form = document.getElementById(''cal'');'							||chr(13)||
		'	form.p_submit.value = ''fullSearch'';'									||chr(13)||
		'	form.submit();'															||chr(13)||
		' }'																		||chr(13)||
		''																			||chr(13)||
		' function justDate ( ) {'													||chr(13)||
        '	var form = document.getElementById(''cal'');'							||chr(13)||
	        '	var fid = document.getElementById(''datefield'');'						||chr(13)||
	        '	if ( fid.innerHTML.length == 10 ) {'									||chr(13)||
			'		form.p_submit.value = ''justDate'';'								||chr(13)||
		'		form.submit();'														||chr(13)||
		' 	}'																		||chr(13)||
        '	else {'																	||chr(13)||
			'		alert('''||c_tex_choseADate||''');'									||chr(13)||
		' 	}'																		||chr(13)||
		' }'																		||chr(13)||
		''																			||chr(13)||
		' function removeDate ( ) {'											||chr(13)||
		'	var form = document.getElementById(''cal'');'								||chr(13)||
		'	var fid = document.getElementById(''datefield'');'							||chr(13)||
		'	form.p_date.value = '''';'										||chr(13)||
		'	fid.innerHTML = ''&nbsp;'';'										||chr(13)||
		'	form.p_submit.value = '''';'										||chr(13)||
		'}'														||chr(13)||
		''														||chr(13)||
		' function freeText() {'											||chr(13)||
		'	var fid = document.getElementById(''cal'');'								||chr(13)||
		'	fid.p_submit.value = ''freeText'';'									||chr(13)||
		'	fid.submit();'												||chr(13)||
		' }'														||chr(13)||
		''														||chr(13)||
		' function pickDate( p_d_date ) {'										||chr(13)||
		'	if ( p_d_date.length == 7 ) {'										||chr(13)||
		'		p_d_date = p_d_date.substring(0,6)+''0''+p_d_date.substring(6,8);'				||chr(13)||
		' 	}'													||chr(13)||
		'	var form = document.getElementById(''cal'');'								||chr(13)||
		'	form.p_d_date.value = p_d_date;'									||chr(13)||
		'	form.p_date.value = p_d_date;'										||chr(13)||
		'	var fid = document.getElementById(''datefield'');'							||chr(13)||
		'	fid.innerHTML = p_d_date.substring(6,8)+''.''+p_d_date.substring(4,6)+''.''+p_d_date.substring(0,4);'	||chr(13)||
		' }'														||chr(13)||
		''														||chr(13)||
		'</script>'													||chr(13)||
		''														||chr(13)||
		'<form name="cal" id="cal" method="post" action="con_xca.show">'			||chr(13)||
		'<input type="hidden" name="p_date" value="' || p_date || '" />'			||chr(13)||
		'<input type="hidden" name="p_d_date" value="' || p_d_date || '" />'		||chr(13)||
		'<input type="hidden" name="p_submit" value="" />'							||chr(13);

	v_html := v_html || '' || htm.bBox(c_tex_search) || chr(13);
	v_html := v_html || '' || htm.bTable || chr(13);
	v_html := v_html || '<tr><td><a href="javascript:setDate(''' || v_month_prev || ''');">&lt;</a></td><td colspan="6" style="text-align:center;">' || v_month_name || v_year || '</td><td><a href="javascript:setDate(''' || v_month_next || ''');">&gt;</a></td></tr>' || chr(13);

	-- Headingen i kalenderen
	v_html := v_html || '
		<tr>
			<td>'||c_tex_week||'</td>
			<td>'||c_tex_mon||'</td>
			<td>'||c_tex_tue||'</td>
			<td>'||c_tex_wed||'</td>
			<td>'||c_tex_thu||'</td>
			<td>'||c_tex_fri||'</td>
			<td>'||c_tex_sat||'</td>
			<td>'||c_tex_sun||'</td>
		</tr>' || chr(13);

	-- "Looper" igjennom antall uker.
	while v_nr_weeks > 0
	loop
		-- Skriver ut ukenumemr
		v_html := v_html || '<tr>' || chr(13) || '<td>' || v_week_number || '</td>' || chr(13);
		-- Setter 7 dager i uken. Det er veldig vanlig.
		v_days := 7;
		-- Looper igjennom dagene
		while v_days > 0
		loop
			if ( v_weekday > 1 ) then
				-- Hvis f�rste dag i m�neden ikke starter p� en mandag, s� m� jeg legge inn noen blanke celler.
				v_html := v_html || '<td>&nbsp;</td>' || chr(13);
				v_weekday := v_weekday - 1;
			elsif ( v_day_counter <= v_nr_days ) then
				-- Ellers legger jeg inn dagen

				if ( length(v_day_counter) = 1 AND v_today > cast(v_year || v_month || '0' || v_day_counter as int) ) then
					v_html := v_html || '<td class="conXcaPassedDays"><a href="JavaScript:pickDate('''|| v_year || v_month || v_day_counter ||''');justDate();">' || v_day_counter || '</a></td>' || chr(13);
				elsif ( length(v_day_counter) = 2 AND v_today > cast(v_year || v_month || v_day_counter as int) ) then
					v_html := v_html || '<td class="conXcaPassedDays"><a href="JavaScript:pickDate('''|| v_year || v_month || v_day_counter ||''');justDate();">' || v_day_counter || '</a></td>' || chr(13);
				else
					v_html := v_html || '<td><a href="JavaScript:pickDate('''|| v_year || v_month || v_day_counter ||''');justDate();">' || v_day_counter || '</a></td>' || chr(13);
				end if;
				v_day_counter := v_day_counter + 1;
			else
				-- S� noen blanke p� slutten igjen
				v_html := v_html || '<td>&nbsp;</td>' || chr(13);
			end if;
			v_days := v_days -1;
		end loop;
		v_nr_weeks := v_nr_weeks - 1;

		-- Fikser ukenummer i januar. Om jeg f�r uke 52/53, s� hopper jeg ned til 1 etter den f�rste uken.
		if ( v_week_number >= 52 AND v_month = 1 ) then
			v_week_number := 1;
		else
			v_week_number := v_week_number + 1;
		end if;

		v_html := v_html || '</tr>' || chr(13);
	end loop;

	-- Lager resten av formet.
	v_html := v_html || '<tr><td colspan="8">&nbsp;</td></tr>' || chr(13);
	v_html := v_html || '<tr><td colspan="3">'||c_tex_chosenDay||'</td><td colspan="5"><div id="datefield" style="font-weight:bold;">&nbsp;</div></td></tr>' || chr(13);
	v_html := v_html || '<tr>' || chr(13) || '<td colspan="8" style="text-align: right;"><a href="JavaScript:removeDate();" class="theButtonStyle">'||c_tex_removeDate||'</a></td>' || chr(13) || '</tr>' || chr(13);
	v_html := v_html || '<tr>' || chr(13) || '<td colspan="8" style="text-align: right;"><a href="JavaScript:justDate();" class="theButtonStyle">'||c_tex_searchOnDate||'</a></td>' || chr(13) || '</tr>' || chr(13);
	v_html := v_html || '<tr><td colspan="8"><hr style="border:0;width:90%;height:1px;color:#660000;background-color:#660000;" /></td></tr>' || chr(13);
	v_html := v_html || '<tr><td colspan="8">'||c_tex_county||'</td></tr>' || chr(13);
	v_html := v_html || '<tr><td colspan="8" style="text-align: center;"><select name="p_region" id="p_region" style="width: 150px;" onchange="showArea('''',''p_region'',''p_area'');" /></td></tr>' || chr(13);
	v_html := v_html || '<tr><td colspan="8">'||c_tex_council||'</td></tr>' || chr(13);
	v_html := v_html || '<tr><td colspan="8" style="text-align: center;"><select multiple size="3" name="p_area" id="p_area" style="width: 150px;" /></td></tr>' || chr(13);
	v_html := v_html || '<tr><td colspan="8">'||c_tex_eventType||'</td></tr>' || chr(13);

	-- Henter subtyper
	open cur_getContentSubtypes;
	loop
		fetch cur_getContentSubtypes into v_subtype_pk, v_subtype_name;
		exit when cur_getContentSubtypes%NOTFOUND;
		v_checked := '';
		for v_index in 1 .. p_type.count
		loop
			if ( p_type(v_index) = v_subtype_pk ) then
				v_checked := ' checked="true"';
			end if;
		end loop;
		v_html := v_html || '<tr><td><input type="checkbox" name="p_type" value="'||v_subtype_pk||'"'||v_checked||' /></td><td colspan="7">'||tex_lib.get('db', 'con_subtype', v_subtype_name)||'</td></tr>' || chr(13);

	end loop;
	close cur_getContentSubtypes;

	v_html := v_html || '<tr>' || chr(13) || '<td colspan="8" style="text-align: right;"><a href="JavaScript:fullSearch();" class="theButtonStyle">'||c_tex_doSearch||'</a></td>' || chr(13) || '</tr>' || chr(13);

	v_html := v_html || htm.eTable || chr(13);
	v_html := v_html || htm.eBox || chr(13);

	v_html := v_html || '<br />' || chr(13);

	v_html := v_html || htm.bBox(c_tex_textSearch) || chr(13);
	v_html := v_html || htm.bTable || chr(13);
	v_html := v_html || '<tr>' || chr(13) || '<td>'||c_tex_text||'</td>' || chr(13) || '<td><input type="text" size="20" name="p_string" value="'|| p_string ||'" /></td>' || chr(13) || '</tr>' || chr(13);
	v_html := v_html || '<tr>' || chr(13) || '<td colspan="2" style="text-align: right;"><a href="JavaScript:freeText();" class="theButtonStyle">'||c_tex_doSearch||'</a></td>' || chr(13) || '</tr>' || chr(13);
	v_html := v_html || htm.eTable || chr(13);
	v_html := v_html || htm.eBox || chr(13);
	v_html := v_html || '</form>' || chr(13);
	v_html := v_html || '<script type="text/javascript" language="javascript" src="'||c_dir_scripts||'/findRegions_nor.js"></script>'||chr(13);
	v_html := v_html || '<script type="text/javascript">' || chr(13) || 'initRegion('''','''',''p_region'',''p_area'');' || chr(13) || '</script>' || chr(13);

	if ( p_submit is not null AND p_date is not null ) then
		v_html := v_html || '
			<script type="text/javascript">
				pickDate('''|| p_date ||''');
			</script>
		';
	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end cal_form;


-- ******************************************** --
end;
/
show errors;
