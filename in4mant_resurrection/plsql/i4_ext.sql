PROMPT *** i4_ext ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package i4_ext is
/* ******************************************************
*	Package:     tex_lib
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	060731 | Frode Klevstul
*			- Package created
****************************************************** */
	procedure run;
	procedure cleanup
	(
		  p_expireDays			in number										default 365
		, p_table				in varchar2										default null
	);
	function cleanup
	(
		  p_expireDays			in number										default 365
		, p_table				in varchar2										default null
	) return clob;
	procedure createFileList
	(
		p_action				in varchar2									default null
	);
	function createFileList
	(
		p_action				in varchar2									default null
	) return clob;

end;
/
show errors;





-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body i4_ext is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'i4_ext';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  31.07.2006
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        cleanup
-- what:        cleanup the database
-- author:      Frode Klevstul
-- start date:  31.07.2006
---------------------------------------------------------
procedure cleanup
(
	  p_expireDays			in number										default 365
	, p_table				in varchar2										default null
) is begin
    ext_lob.pri(i4_ext.cleanup(p_expireDays, p_table));
end;

function cleanup
(
	  p_expireDays			in number										default 365
	, p_table				in varchar2										default null
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type 		:= 'cleanup';

	v_alert						integer;
	v_count						number;
	v_count2					number;

	v_expireDate				date := sysdate - p_expireDays;

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_expireDays='||p_expireDays||'��p_table='||p_table);
	ext_lob.ini(v_clob);

	use_lib.loggedInCheck(g_package, c_procedure, null);

	-- -------------------------
	--
	-- delete from database
	--
	-- -------------------------
	if (p_table is not null) then

		-- ---------------------------------------------------------
		-- delete old values from use_user
		-- ---------------------------------------------------------
		if (p_table = 'use_user') then
			delete from use_user
			where	use_type_fk in
				(
					select	use_type_pk
					from	use_type
					where	lower(name) like 'org%'
				)
			and		use_user_pk not in
		    	(
		    		select	use_user_fk_pk
		    		from	use_org
		    	);

		-- ---------------------------------------------------------
		-- org_organisation
		-- ---------------------------------------------------------
		elsif (p_table = 'org_organisation') then
			delete from org_organisation
			where	bool_umbrella_organisation = 1
			and		org_organisation_pk not in
				(
					select	parent_org_organisation_fk
					from	org_organisation
					where	(bool_umbrella_organisation is null or bool_umbrella_organisation = 0)
					and		parent_org_organisation_fk is not null
				);

		-- ---------------------------------------------------------
		-- con_file
		-- ---------------------------------------------------------
		elsif (p_table = 'con_file') then
			delete from con_file
			where	con_file_pk not in
				(
					select	con_file_fk_pk
					from	con_content_file
				);

		-- ---------------------------------------------------------
		-- pic_picture
		-- ---------------------------------------------------------
		elsif (p_table = 'pic_picture') then
			delete from pic_picture
			where	pic_picture_pk not in
				(
					select	pic_picture_fk_pk
					from	pic_album_picture
				);

		-- ---------------------------------------------------------
		-- ses_session
		-- ---------------------------------------------------------
		elsif (p_table = 'ses_session') then
			delete from ses_session
			where date_update < v_expireDate;

		-- ---------------------------------------------------------
		-- ses_instance
		-- ---------------------------------------------------------
		elsif (p_table = 'ses_instance') then
			delete from ses_instance
			where date_insert < v_expireDate;

		-- ---------------------------------------------------------
		-- his_history
		-- ---------------------------------------------------------
		elsif (p_table = 'his_history') then
			delete from his_history
			where date_insert < v_expireDate;

		-- ---------------------------------------------------------
		-- log_mod
		-- ---------------------------------------------------------
		elsif (p_table = 'log_mod') then
			delete from log_mod
			where date_update < v_expireDate;

		-- ---------------------------------------------------------
		-- log_error
		-- ---------------------------------------------------------
		elsif (p_table = 'log_error') then
			delete from log_error
			where date_insert < v_expireDate;

		-- ---------------------------------------------------------
		-- log_tex
		-- ---------------------------------------------------------
		elsif (p_table = 'log_tex') then
			delete from log_tex
			where	tex_text_fk_pk not in
				(
					select	distinct(tex_text_pk)
					from	tex_text
				);

		-- ---------------------------------------------------------
		-- log_debug
		-- ---------------------------------------------------------
		elsif (p_table = 'log_debug') then
			delete from log_debug
			where date_insert < v_expireDate;

		-- ---------------------------------------------------------
		-- log_upload
		-- ---------------------------------------------------------
		elsif (p_table = 'log_upload') then
			delete from log_upload
			where date_insert < v_expireDate;

		-- ---------------------------------------------------------
		-- log_login
		-- ---------------------------------------------------------
		elsif (p_table = 'log_login') then
			delete from log_login
			where date_insert < v_expireDate;

		-- ---------------------------------------------------------
		-- delete unrelated addresses from add_address table
		-- ---------------------------------------------------------
		elsif (p_table = 'add_address') then
			delete from add_address
			where	add_address_pk not in (select add_address_fk from bil_item)
			and		add_address_pk not in (select add_address_fk from use_details)
			and		add_address_pk not in (select add_address_fk from org_organisation)
			and		add_address_pk not in (select add_address_fk from mar_target);

		-- ---------------------------------------------------------
		-- delete old entries in tex_text
		-- ---------------------------------------------------------
		elsif (p_table = 'tex_text') then
			delete from tex_text
			where	tex_text_pk in
			(
				select	tex_text_fk_pk
				from	log_tex
				where	date_accessed < sysdate - 365	-- hardcode 365 days to avoid deleting strings in use (if user choses shorter exp period)
			);

		end if;

		commit;

	end if;

	-- ---------------------
	--
	-- start HTML page
	--
	-- ---------------------
	v_html := v_html||''||htm.bPage(g_package, c_procedure, 'i4admin')||chr(13);
	v_html := v_html||'<center>'||chr(13);
	v_html := v_html||''||htm.bTable('surroundAdminBox')||chr(13);
	v_html := v_html||'<tr><td>'||chr(13);


	-- ------------------------
	-- set expiry duration
	-- ------------------------
	v_html := v_html||''||htm.bForm(g_package||'.'||c_procedure)||chr(13);
	v_html := v_html||''||htm.bBox('Set expiry days')||chr(13);
	v_html := v_html||''||htm.bTable||chr(13);

	v_html := v_html||'
		<tr>
			<td>Number of days before data expire (only show data older than this amount of days):</td>
			<td>
				<select name="p_expireDays" onchange="this.form.submit();">
	';
					if (p_expireDays = 7) then v_html := v_html||'<option value="7" selected>7</option>'||chr(13);
					else v_html := v_html||'<option value="7">7</option>'||chr(13); end if;

					if (p_expireDays = 30) then v_html := v_html||'<option value="30" selected>30</option>'||chr(13);
					else v_html := v_html||'<option value="30">30</option>'||chr(13); end if;

					if (p_expireDays = 60) then v_html := v_html||'<option value="60" selected>60</option>'||chr(13);
					else v_html := v_html||'<option value="60">60</option>'||chr(13); end if;

					if (p_expireDays = 90) then v_html := v_html||'<option value="90" selected>90</option>'||chr(13);
					else v_html := v_html||'<option value="90">90</option>'||chr(13); end if;

					if (p_expireDays = 180) then v_html := v_html||'<option value="180" selected>180</option>'||chr(13);
					else v_html := v_html||'<option value="180">180</option>'||chr(13); end if;

					if (p_expireDays = 365) then v_html := v_html||'<option value="365" selected>365</option>'||chr(13);
					else v_html := v_html||'<option value="365">365</option>'||chr(13); end if;

	v_html := v_html||'
				</select>
			</td>
		</tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||''||htm.eBox||chr(13);
	v_html := v_html||''||htm.eForm||chr(13);


	v_html := v_html||'</td></tr><tr><td class="surroundAdminBox">'||chr(13);


	-- ------------------------
	-- display cleanup options
	-- ------------------------
	v_html := v_html||''||htm.bBox('Clean DB')||chr(13);
	v_html := v_html||''||htm.bTable||chr(13);

	v_html := v_html||'
		<tr>
			<td colspan="5" style="text-align: center">
				<b>NOTE:</b> <font color="red"><b>Take a backup</b></font> of the database before you delete any values!<br>
				<i>Only delete from tables where load is heigh (close to or above 100% when exp days = 365)</i>
			</td>
		</tr>'||chr(13);

	v_html := v_html||'
		<tr><td colspan="5">&nbsp;</td></tr>
		<tr>
			<td width="10%"><b>load:</b></td>
			<td width="30%"><b>table/area:</b></td>
			<td><b>no. to be deleted:</b></td>
			<td><b>kAdmin view:</b></td>
			<td><b>delete:</b></td>
		</tr>
		<tr><td colspan="5">&nbsp;</td></tr>'||chr(13);

	-- -----
	-- USE
	-- -----
	v_html := v_html||'<tr><td colspan="5"><b>USE:</b> (Users)</td></tr>'||chr(13);

	select	count(*)
	into	v_count
	from	use_user
	where	use_type_fk in
		(
			select	use_type_pk
			from	use_type
			where	lower(name) like 'org%'
		)
	and		use_user_pk not in
    	(
    		select	use_user_fk_pk
    		from	use_org
    	);

	v_alert := v_count / 1 * 100;

	v_html := v_html||'
		<tr>
			<td>'||v_alert||'%</td>
			<td>use_user</td>
			<td><i>'||v_count||' inactive org users</i></td>
			<td><a href="kAdmin.admin_table?p_action=list&amp;p_table_name=USE_USER&amp;p_sql_stmt=WHERE%20USE_TYPE_FK%20IN(SELECT%20USE_TYPE_PK%20FROM%20USE_TYPE%20WHERE%20UPPER(NAME)%20LIKE%20''ORG%'')AND%20USE_USER_PK%20NOT%20IN(SELECT%20USE_USER_FK_PK%20FROM%20USE_ORG)">view entries</a></td>
			<td><a href="'||g_package||'.'||c_procedure||'?p_table=use_user">delete</a></td>
		</tr>
		<tr><td colspan="5">&nbsp;</td></tr>'||chr(13);

	-- -----
	-- ORG
	-- -----
	v_html := v_html||'<tr><td colspan="5"><b>ORG:</b> (Organisation)</td></tr>'||chr(13);

	select	count(*)
	into	v_count
	from	org_organisation
	where	bool_umbrella_organisation = 1
	and		org_organisation_pk not in
		(
			select	parent_org_organisation_fk
			from	org_organisation
			where	(bool_umbrella_organisation is null or bool_umbrella_organisation = 0)
			and		parent_org_organisation_fk is not null
		);

	v_alert := v_count / 10 * 100;

	v_html := v_html||'
		<tr>
			<td>'||v_alert||'%</td>
			<td>org_organisation</td>
			<td><i>'||v_count||' parents w/o children</i></td>
			<td><a href="kAdmin.admin_table?p_action=list&amp;p_table_name=ORG_ORGANISATION&amp;p_sql_stmt=WHERE%20BOOL_UMBRELLA_ORGANISATION%20=%201%20AND%20ORG_ORGANISATION_PK%20NOT%20IN(SELECT%20PARENT_ORG_ORGANISATION_FK%20FROM%20ORG_ORGANISATION%20WHERE%20(BOOL_UMBRELLA_ORGANISATION%20IS%20NULL%20OR%20BOOL_UMBRELLA_ORGANISATION%20=%200)%20AND%20PARENT_ORG_ORGANISATION_FK%20IS%20NOT%20NULL)">view entries</a></td>
			<td><a href="'||g_package||'.'||c_procedure||'?p_table=org_organisation">delete</a></td>
		</tr>
		<tr><td colspan="5">&nbsp;</td></tr>'||chr(13);

	-- -----
	-- CON
	-- -----
	v_html := v_html||'<tr><td colspan="5"><b>CON:</b> (Content)</td></tr>'||chr(13);

	select	count(*)
	into	v_count
	from	con_file
	where	con_file_pk not in
		(
			select	con_file_fk_pk
			from	con_content_file
		);

	v_alert := v_count / 1 * 100;

	v_html := v_html||'
		<tr>
			<td>'||v_alert||'%</td>
			<td>con_file</td>
			<td><i>'||v_count||' unrelated files</i></td>
			<td><a href="kAdmin.admin_table?p_action=list&amp;p_table_name=CON_FILE&amp;p_sql_stmt=WHERE%20CON_FILE_PK%20NOT%20IN(SELECT%20CON_FILE_FK_PK%20FROM%20CON_CONTENT_FILE)">view entries</a></td>
			<td><a href="'||g_package||'.'||c_procedure||'?p_table=con_file">delete</a></td>
		</tr>
		<tr><td colspan="5">&nbsp;</td></tr>'||chr(13);

	-- -----
	-- PIC
	-- -----
	v_html := v_html||'<tr><td colspan="5"><b>PIC:</b> (Picture)</td></tr>'||chr(13);

	select	count(*)
	into	v_count
	from	pic_picture
	where	pic_picture_pk not in
		(
			select	pic_picture_fk_pk
			from	pic_album_picture
		);

	v_alert := v_count / 1 * 100;

	v_html := v_html||'
		<tr>
			<td>'||v_alert||'%</td>
			<td>pic_picture</td>
			<td><i>'||v_count||' unrelated pictures</i></td>
			<td><a href="kAdmin.admin_table?p_action=list&amp;p_table_name=PIC_PICTURE&amp;p_sql_stmt=WHERE%20PIC_PICTURE_PK%20NOT%20IN(SELECT%20PIC_PICTURE_FK_PK%20FROM%20PIC_ALBUM_PICTURE)">view entries</a></td>
			<td><a href="'||g_package||'.'||c_procedure||'?p_table=pic_picture">delete</a></td>
		</tr>
		<tr><td colspan="5">&nbsp;</td></tr>'||chr(13);

	-- -----
	-- SES
	-- -----
	v_html := v_html||'<tr><td colspan="5"><b>SES:</b> (Session)</td></tr>'||chr(13);

	select	count(*)
	into	v_count
	from	ses_session
	where	date_update < v_expireDate;

	v_alert := v_count / 10000 * 100;

	v_html := v_html||'
		<tr>
			<td>'||v_alert||'%</td>
			<td>ses_session</td>
			<td><i>'||v_count||'</i></td>
			<td><a href="kAdmin.admin_table?p_action=list&amp;p_table_name=SES_SESSION&amp;p_sql_stmt=WHERE%20DATE_UPDATE%3C&#39;'||v_expireDate||'&#39;">view entries</a></td>
			<td><a href="'||g_package||'.'||c_procedure||'?p_table=ses_session">delete</a></td>
		</tr>'||chr(13);

	select	count(*)
	into	v_count
	from	ses_instance
	where	date_insert < v_expireDate;

	v_alert := v_count / 100000 * 100;

	v_html := v_html||'
		<tr>
			<td>'||v_alert||'%</td>
			<td>ses_instance</td>
			<td><i>'||v_count||'</i></td>
			<td><a href="kAdmin.admin_table?p_action=list&amp;p_table_name=SES_INSTANCE&amp;p_sql_stmt=WHERE%20DATE_INSERT%3C&#39;'||v_expireDate||'&#39;">view entries</a></td>
			<td><a href="'||g_package||'.'||c_procedure||'?p_table=ses_instance">delete</a></td>
		</tr>'||chr(13);

	-- -----
	-- HIS
	-- -----
	v_html := v_html||'<tr><td colspan="5">&nbsp;</td></tr>'||chr(13);
	v_html := v_html||'<tr><td colspan="5"><b>HIS:</b> (History)</td></tr>'||chr(13);

	select	count(*)
	into	v_count
	from	his_history
	where	date_insert < v_expireDate;

	v_alert := v_count / 1000000 * 100;

	v_html := v_html||'
		<tr>
			<td>'||v_alert||'%</td>
			<td>his_history</td>
			<td><i>'||v_count||'</i></td>
			<td><a href="kAdmin.admin_table?p_action=list&amp;p_table_name=HIS_HISTORY&amp;p_sql_stmt=WHERE%20DATE_INSERT%3C&#39;'||v_expireDate||'&#39;">view entries</a></td>
			<td><a href="'||g_package||'.'||c_procedure||'?p_table=his_history">delete</a></td>
		</tr>'||chr(13);

	-- -----
	-- LOG
	-- -----
	v_html := v_html||'<tr><td colspan="5">&nbsp;</td></tr>'||chr(13);
	v_html := v_html||'<tr><td colspan="5"><b>LOG:</b> (Log)</td></tr>'||chr(13);

	select	count(*)
	into	v_count
	from	log_mod
	where	date_update < v_expireDate;

	v_alert := v_count / 100000 * 100;

	v_html := v_html||'
		<tr>
			<td>'||v_alert||'%</td>
			<td>log_mod</td>
			<td><i>'||v_count||'</i></td>
			<td><a href="kAdmin.admin_table?p_action=list&amp;p_table_name=LOG_MOD&amp;p_sql_stmt=WHERE%20DATE_UPDATE%3C&#39;'||v_expireDate||'&#39;">view entries</a></td>
			<td><a href="'||g_package||'.'||c_procedure||'?p_table=log_mod">delete</a></td>
		</tr>'||chr(13);

	select	count(*)
	into	v_count
	from	log_error
	where	date_insert < v_expireDate;

	v_alert := v_count / 100000 * 100;

	v_html := v_html||'
		<tr>
			<td>'||v_alert||'%</td>
			<td>log_error</td>
			<td><i>'||v_count||'</i></td>
			<td><a href="kAdmin.admin_table?p_action=list&amp;p_table_name=LOG_ERROR&amp;p_sql_stmt=WHERE%20DATE_INSERT%3C&#39;'||v_expireDate||'&#39;">view entries</a></td>
			<td><a href="'||g_package||'.'||c_procedure||'?p_table=log_error">delete</a></td>
		</tr>'||chr(13);

	select	count(*)
	into	v_count
	from	log_tex
	where	tex_text_fk_pk in
		(
			select	distinct(tex_text_pk)
			from	tex_text
		);

	select	count(*)
	into	v_count2
	from	log_tex
	where	tex_text_fk_pk not in
		(
			select	distinct(tex_text_pk)
			from	tex_text
		);

	v_alert := v_count2 / 1000 * 100;

	v_html := v_html||'
		<tr>
			<td>'||v_alert||'%</td>
			<td>log_tex</td>
			<td>'||v_count2||' / <i>'||v_count||' unrelated</i></td>
			<td><a href="kAdmin.admin_table?p_action=list&amp;p_table_name=LOG_TEX&amp;p_sql_stmt=WHERE%20TEX_TEXT_FK_PK%20NOT%20IN%20(SELECT%20DISTINCT(TEX_TEXT_PK)%20FROM%20TEX_TEXT)">view entries</a></td>
			<td><a href="'||g_package||'.'||c_procedure||'?p_table=log_tex">delete</a></td>
		</tr>'||chr(13);

	select	count(*)
	into	v_count
	from	log_debug
	where	date_insert < v_expireDate;

	v_alert := v_count / 10000 * 100;

	v_html := v_html||'
		<tr>
			<td>'||v_alert||'%</td>
			<td>log_debug</td>
			<td><i>'||v_count||'</i></td>
			<td><a href="kAdmin.admin_table?p_action=list&amp;p_table_name=LOG_DEBUG&amp;p_sql_stmt=WHERE%20DATE_INSERT%3C&#39;'||v_expireDate||'&#39;">view entries</a></td>
			<td><a href="'||g_package||'.'||c_procedure||'?p_table=log_debug">delete</a></td>
		</tr>'||chr(13);

	select	count(*)
	into	v_count
	from	log_upload
	where	date_insert < v_expireDate;

	v_alert := v_count / 100 * 100;

	v_html := v_html||'
		<tr>
			<td>'||v_alert||'%</td>
			<td>log_upload</td>
			<td><i>'||v_count||'</i></td>
			<td><a href="kAdmin.admin_table?p_action=list&amp;p_table_name=LOG_UPLOAD&amp;p_sql_stmt=WHERE%20DATE_INSERT%3C&#39;'||v_expireDate||'&#39;">view entries</a></td>
			<td><a href="'||g_package||'.'||c_procedure||'?p_table=log_upload">delete</a></td>
		</tr>'||chr(13);

	select	count(*)
	into	v_count
	from	log_login
	where	date_insert < v_expireDate;

	v_alert := v_count / 1000000 * 100;

	v_html := v_html||'
		<tr>
			<td>'||v_alert||'%</td>
			<td>log_login</td>
			<td><i>'||v_count||'</i></td>
			<td><a href="kAdmin.admin_table?p_action=list&amp;p_table_name=LOG_LOGIN&amp;p_sql_stmt=WHERE%20DATE_INSERT%3C&#39;'||v_expireDate||'&#39;">view entries</a></td>
			<td><a href="'||g_package||'.'||c_procedure||'?p_table=log_login">delete</a></td>
		</tr>'||chr(13);

	-- -----
	-- ADD
	-- -----
	v_html := v_html||'<tr><td colspan="5">&nbsp;</td></tr>'||chr(13);
	v_html := v_html||'<tr><td colspan="5"><b>ADD:</b> (Address)</td></tr>'||chr(13);

	select	count(*)
	into	v_count
	from	add_address;

	select	count(*)
	into	v_count2
	from	add_address
	where	add_address_pk not in (select add_address_fk from bil_item)
	and		add_address_pk not in (select add_address_fk from use_details)
	and		add_address_pk not in (select add_address_fk from org_organisation)
	and		add_address_pk not in (select add_address_fk from mar_target);

	v_alert := v_count2 / 10 * 100;

	v_html := v_html||'
		<tr>
			<td>'||v_alert||'%</td>
			<td>add_address</td>
			<td>'||v_count2||' / <i>'||v_count||' unrelated</i></td>
			<td>n/a</td>
			<td><a href="'||g_package||'.'||c_procedure||'?p_table=add_address">delete</a></td>
		</tr>'||chr(13);

	-- -----
	-- TEX
	-- -----
	v_html := v_html||'<tr><td colspan="5">&nbsp;</td></tr>'||chr(13);
	v_html := v_html||'<tr><td colspan="5"><b>TEX:</b> (Text)</td></tr>'||chr(13);

	select	count(*)
	into	v_count
	from	tex_text, log_tex
	where	tex_text_pk = tex_text_fk_pk
	and		date_accessed < sysdate - 365;

	v_alert := v_count / 100 * 100;

	v_html := v_html||'
		<tr>
			<td>'||v_alert||'%</td>
			<td>tex_text</td>
			<td><i>'||v_count||'</i> not used for one year</td>
			<td>n/a</td>
			<td><a href="'||g_package||'.'||c_procedure||'?p_table=tex_text">delete</a></td>
		</tr>'||chr(13);

	-- -----
	-- OS
	-- -----
	v_html := v_html||'<tr><td colspan="5">&nbsp;</td></tr>'||chr(13);
	v_html := v_html||'<tr><td colspan="5"><b>OS:</b> (Files on the OS)</td></tr>'||chr(13);

	v_html := v_html||'
		<tr>
			<td>n/a</td>
			<td>files from OS</td>
			<td><i>n/a</i></td>
			<td>n/a</td>
			<td><a href="'||g_package||'.createFileList">create exclusion list</a></td>
		</tr>'||chr(13);


	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||''||htm.eBox||chr(13);

	v_html := v_html||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||'</center>'||chr(13);
	v_html := v_html||''||htm.ePage||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		return null;
end;
end cleanup;


---------------------------------------------------------
-- name:        createFileList
-- what:        create a list with all files used
-- author:      Frode Klevstul
-- start date:  01.08.2006
---------------------------------------------------------
procedure createFileList
(
	p_action				in varchar2									default null
) is begin
	ext_lob.pri(i4_ext.createFileList(p_action));
end;

function createFileList
(
	p_action				in varchar2									default null
) return clob
is
begin
declare
	c_procedure			constant mod_procedure.name%type 			:= 'createFileList';

	c_id				constant ses_session.id%type 				:= ses_lib.getId;
	c_img_org			constant sys_parameter.value%type			:= sys_lib.getParameter('img_org');
	c_dir_img_org		constant sys_parameter.value%type			:= sys_lib.getParameter('dir_img_org');

	v_out_file			utl_file.file_type;

	v_files				clob;
	v_files_blob		blob;
	v_tmp				varchar2(64);

	cursor	cur_con_file is
	select	path
	from	con_file;

	cursor	cur_mar_stunt is
	select	path_attachment1, path_attachment2, path_attachment3
	from	mar_stunt;

	cursor	cur_pic_picture is
	select	path_filename
	from	pic_picture;

	cursor	cur_org_organisation is
	select	path_logo, path_mainphoto
	from	org_organisation;

	cursor	cur_men_object is
	select	path_picture
	from	men_object;

	cursor	cur_bil_item is
	select	path_picture
	from	bil_item;

	v_html 						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_action='||p_action);
	ext_lob.ini(v_clob);

	use_lib.loggedInCheck(g_package, c_procedure, null);

	-- -------------------
	-- print out HTML
	-- -------------------
	if (p_action is null) then
		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'i4admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundAdminBox')||chr(13);
		v_html := v_html||'<tr><td class="surroundAdminBox">'||chr(13);
	
		v_html := v_html||''||htm.bBox('List of files linked from DB to OS')||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);
	
		v_html := v_html||'<tr><td>Root directory: '||c_img_org||'</td></tr>';
	
		v_html := v_html||'<tr><td>&nbsp;</td></tr>';
	end if;

	for r_cursor in cur_con_file
	loop
		if (r_cursor.path is not null) then
			v_files := v_files||'---'||r_cursor.path;
			v_html := v_html||'<tr><td>'||r_cursor.path||'</td></tr>';
			if (p_action is null) then
				ext_lob.add(v_clob, v_html);
			end if;
		end if;
	end loop;

	for r_cursor in cur_mar_stunt
	loop
		if (r_cursor.path_attachment1 is not null) then
			v_files := v_files||'---'||r_cursor.path_attachment1;
			v_html := v_html||'<tr><td>'||r_cursor.path_attachment1||'</td></tr>';
		end if;
		if (r_cursor.path_attachment2 is not null) then
			v_files := v_files||'---'||r_cursor.path_attachment2;
			v_html := v_html||'<tr><td>'||r_cursor.path_attachment2||'</td></tr>';
		end if;
		if (r_cursor.path_attachment3 is not null) then
			v_files := v_files||'---'||r_cursor.path_attachment3;
			v_html := v_html||'<tr><td>'||r_cursor.path_attachment3||'</td></tr>';
		end if;
		if (p_action is null) then
			ext_lob.add(v_clob, v_html);
		end if;
	end loop;

	for r_cursor in cur_pic_picture
	loop
		if (r_cursor.path_filename is not null) then
			-- org photos
			v_files := v_files||'---'||r_cursor.path_filename;
			v_html := v_html||'<tr><td>'||r_cursor.path_filename||'</td></tr>';

			-- thumbnails are not stored as links in db so we add them "manually"
			v_tmp := r_cursor.path_filename;
			v_tmp := replace(v_tmp, '/', '/thumb_');
			v_tmp := replace(v_tmp, '\', '\thumb_');
			v_files := v_files||'---'||v_tmp;
			v_html := v_html||'<tr><td>'||v_tmp||'</td></tr>';
			if (p_action is null) then
				ext_lob.add(v_clob, v_html);
			end if;
		end if;
	end loop;

	for r_cursor in cur_org_organisation
	loop
		if (r_cursor.path_logo is not null) then
			v_files := v_files||'---'||r_cursor.path_logo;
			v_html := v_html||'<tr><td>'||r_cursor.path_logo||'</td></tr>';
		end if;
		if (r_cursor.path_mainphoto is not null) then
			v_files := v_files||'---'||r_cursor.path_mainphoto;
			v_html := v_html||'<tr><td>'||r_cursor.path_mainphoto||'</td></tr>';
		end if;
		if (p_action is null) then
			ext_lob.add(v_clob, v_html);
		end if;
	end loop;

	for r_cursor in cur_men_object
	loop
		if (r_cursor.path_picture is not null) then
			v_files := v_files||'---'||r_cursor.path_picture;
			v_html := v_html||'<tr><td>'||r_cursor.path_picture||'</td></tr>';
			if (p_action is null) then
				ext_lob.add(v_clob, v_html);
			end if;
		end if;
	end loop;

	for r_cursor in cur_bil_item
	loop
		if (r_cursor.path_picture is not null) then
			v_files := v_files||'---'||r_cursor.path_picture;
			v_html := v_html||'<tr><td>'||r_cursor.path_picture||'</td></tr>';
			if (p_action is null) then
				ext_lob.add(v_clob, v_html);
			end if;
		end if;
	end loop;

	v_html := v_html||'
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td>'||
				htm.link('Write exlusion file to dir '||c_dir_img_org,g_package||'.'||c_procedure||'?p_action=createFile','_self', 'theButtonStyle')
				||'
			</td>
		</tr>'||chr(13);

	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||''||htm.eBox||chr(13);

	v_html := v_html||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||'</center>'||chr(13);
	v_html := v_html||''||htm.ePage||chr(13);

	if (p_action is null) then
		ext_lob.add(v_clob, v_html);
	end if;

	-- --------------------
	-- write to file
	-- --------------------
	if (p_action = 'createFile') then
		v_files_blob := ext_lib.c2b(v_files);
	
		insert into log_upload
		(log_upload_pk, content, src_filename, trg_filename, trg_directory, bool_written, ses_session_id, date_insert)
		values (
			  seq_log_upload.nextval
			, v_files_blob
			, 'excludeFiles.txt'
			, 'excludeFiles.txt'
			, c_dir_img_org
			, null
			, c_id
			, sysdate
			);
		commit;
		
		sys_lib.writeToFile(
			  p_type			=> 'text'
			, p_max_file_size	=> 99999
			, p_url_success		=> 'i4_ext.cleanup'
			, p_url_failure		=> 'i4_pub.i4admin'
		);
		return null;
	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		return null;
end;
end createFileList;


-- ******************************************** --
end;
/
show errors;
