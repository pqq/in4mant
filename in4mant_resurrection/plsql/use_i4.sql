PROMPT *** use_i4 ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package use_i4 is
/* ******************************************************
*	Package:     tex_lib
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	051228 | Frode Klevstul
*			- Package created
****************************************************** */
	procedure run;
	procedure addUser
	(
		  p_action						varchar2											default null
		, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
		, p_username					use_user.username%type								default null
		, p_password					use_user.password%type								default null
		, p_email						add_address.email%type								default null
		, p_name						add_address.name%type								default null
		, p_landline					add_address.landline%type							default null
		, p_mobile						add_address.mobile%type								default null
		, p_fax							add_address.fax%type								default null
	);
	function addUser
	(
		  p_action						varchar2											default null
		, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
		, p_username					use_user.username%type								default null
		, p_password					use_user.password%type								default null
		, p_email						add_address.email%type								default null
		, p_name						add_address.name%type								default null
		, p_landline					add_address.landline%type							default null
		, p_mobile						add_address.mobile%type								default null
		, p_fax							add_address.fax%type								default null
	) return clob;
	procedure deleteUser
	(
		  p_action						varchar2											default null
		, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
	);
	function deleteUser
	(
		  p_action						varchar2											default null
		, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
	) return clob;

end;
/
show errors;





-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body use_i4 is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'use_i4';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  22.12.2005
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        addUser
-- what:        add a new user
-- author:      Frode Klevstul
-- start date:  28.12.2005
---------------------------------------------------------
procedure addUser(p_action varchar2 default null, p_organisation_pk org_organisation.org_organisation_pk%type default null, p_username use_user.username%type default null, p_password use_user.password%type default null, p_email add_address.email%type default null, p_name add_address.name%type default null, p_landline add_address.landline%type default null, p_mobile add_address.mobile%type default null, p_fax add_address.fax%type default null) is begin
    ext_lob.pri(use_i4.addUser(p_action, p_organisation_pk, p_username, p_password, p_email, p_name, p_landline, p_mobile, p_fax));
end;

function addUser
(
	  p_action						varchar2											default null
	, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
	, p_username					use_user.username%type								default null
	, p_password					use_user.password%type								default null
	, p_email						add_address.email%type								default null
	, p_name						add_address.name%type								default null
	, p_landline					add_address.landline%type							default null
	, p_mobile						add_address.mobile%type								default null
	, p_fax							add_address.fax%type								default null
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type 		:= 'addUser';

	v_user_pk					use_user.use_user_pk%type;

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_action='||p_action||'��p_organisation_pk='||p_organisation_pk||'��p_username='||p_username);
	ext_lob.ini(v_clob);

	use_lib.loggedInCheck(g_package, c_procedure, null);

	if (p_action is null) then
		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'i4admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundAdminBox')||chr(13);
		v_html := v_html||'<tr><td>'||chr(13);

		v_html := v_html||''||htm.bForm(g_package||'.'||c_procedure)||chr(13);
		v_html := v_html||'<input type="hidden" name="p_action" value="insert">'||chr(13);
		v_html := v_html||''||htm.bBox('Create new login for organisation')||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);

		v_html := v_html||'<tr><td colspan="2">&nbsp;</td></tr>'||chr(13);
		v_html := v_html||'<tr><td colspan="2"><b>Related to:</b></td></tr>'||chr(13);

		v_html := v_html||'<tr><td>Organisation:</td><td>'||chr(13);
		htp.p(v_html);
		v_html := '';
		htp.p(htm_lib.selectOrganisation('no_login'));
		v_html := v_html||'</td></tr>';

		v_html := v_html||'<tr><td colspan="2">&nbsp;</td></tr>'||chr(13);
		v_html := v_html||'<tr><td colspan="2"><b>Login info:</b></td></tr>'||chr(13);
		v_html := v_html||'<tr><td><font color="red">Username:</font></td><td><input type="text" name="p_username" maxlength="32" size="15"></td></tr>'||chr(13);
		v_html := v_html||'<tr><td><font color="red">Password:</font></td><td><input type="text" name="p_password" maxlength="32" size="15"></td></tr>'||chr(13);
		v_html := v_html||'<tr><td><font color="red">Email:</font></td><td><input type="text" name="p_email" maxlength="128" size="50"></td></tr>'||chr(13);

		v_html := v_html||'<tr><td colspan="2">&nbsp;</td></tr>'||chr(13);
		v_html := v_html||'<tr><td colspan="2"><b>Contact info:</b></td></tr>'||chr(13);
		v_html := v_html||'<tr><td>Name:</td><td><input type="text" name="p_name" maxlength="64" size="50"></td></tr>'||chr(13);
		v_html := v_html||'<tr><td>Landline:</td><td><input type="text" name="p_landline" maxlength="32" size="10"></td></tr>'||chr(13);
		v_html := v_html||'<tr><td>Mobile:</td><td><input type="text" name="p_mobile" maxlength="32" size="10"></td></tr>'||chr(13);
		v_html := v_html||'<tr><td>Fax:</td><td><input type="text" name="p_fax" maxlength="32" size="10"></td></tr>'||chr(13);

		v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||htm.submitLink('insert')||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||htm.historyBack('go back')||'</td></tr>'||chr(13);

		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);
		v_html := v_html||''||htm.eForm||chr(13);

		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

	elsif (p_action = 'insert' and p_email is not null and p_password is not null and p_username is not null) then

		v_user_pk := sys_lib.getEmptyPk('use_user');

		insert into use_user
		(use_user_pk, username, email, password, use_type_fk, use_status_fk)
		values (
			  v_user_pk
			, p_username
			, p_email
			, p_password
			, (select use_type_pk from use_type where name = 'organisation')
			, (select use_status_pk from use_status where name = 'activated')
		);

		insert into use_org
		(use_user_fk_pk, org_organisation_fk_pk)
		values(v_user_pk, p_organisation_pk);

		commit;

		v_html := v_html||htm.jumpTo('i4_pub.overview', 1, 'inserting...');

	else

		--htp.p(p_action||' ::: '||p_email||' ::: '||p_password||' ::: '||p_username||' ::: ');
		v_html := v_html||htm.jumpTo('i4_pub.overview', 1, 'no insert made - going back to i4admin...');

	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
    when others then
    	sys_lib.exceptionHandle(sqlcode, sqlerrm);
        return null;
end;
end addUser;


---------------------------------------------------------
-- name:        deleteUser
-- what:        delete a user related to an organisation
-- author:      Frode Klevstul
-- start date:  09.02.2007
---------------------------------------------------------
procedure deleteUser(p_action varchar2 default null, p_organisation_pk org_organisation.org_organisation_pk%type default null) is begin
    ext_lob.pri(use_i4.deleteUser(p_action, p_organisation_pk));
end;

function deleteUser
(
	  p_action						varchar2											default null
	, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type 		:= 'deleteUser';

	v_user_pk					use_user.use_user_pk%type;

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_action='||p_action||'��p_organisation_pk='||p_organisation_pk);
	ext_lob.ini(v_clob);

	use_lib.loggedInCheck(g_package, c_procedure, null);

	if (p_action is null) then
		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'i4admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundAdminBox')||chr(13);
		v_html := v_html||'<tr><td>'||chr(13);

		v_html := v_html||''||htm.bForm(g_package||'.'||c_procedure)||chr(13);
		v_html := v_html||'<input type="hidden" name="p_action" value="delete">'||chr(13);
		v_html := v_html||''||htm.bBox('Delete login for organisation')||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);

		v_html := v_html||'<tr><td colspan="2">&nbsp;</td></tr>'||chr(13);
		v_html := v_html||'<tr><td colspan="2"><b>Related to:</b></td></tr>'||chr(13);

		v_html := v_html||'<tr><td>Organisation:</td><td>'||chr(13);
		htp.p(v_html);
		v_html := '';
		htp.p(htm_lib.selectOrganisation('has_login'));
		v_html := v_html||'</td></tr>';

		v_html := v_html||'<tr><td colspan="2">&nbsp;</td></tr>'||chr(13);

		v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||htm.submitLink('delete user')||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||htm.historyBack('go back')||'</td></tr>'||chr(13);

		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);
		v_html := v_html||''||htm.eForm||chr(13);

		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

	elsif (p_action = 'delete' and p_organisation_pk is not null) then

		select	use_user_fk_pk
		into	v_user_pk
		from	use_org
		where	org_organisation_fk_pk = p_organisation_pk;
		
		delete	from use_user
		where	use_user_pk = v_user_pk;

		commit;
		
		v_html := v_html||htm.jumpTo('i4_pub.overview', 1, 'deleting...');

	else

		v_html := v_html||htm.jumpTo('i4_pub.overview', 1, 'no insert made - going back to i4admin...');

	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
    when others then
    	sys_lib.exceptionHandle(sqlcode, sqlerrm);
        return null;
end;
end deleteUser;


-- ******************************************** --
end;
/
show errors;
