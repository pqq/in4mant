PROMPT *** use_adm ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package use_adm is
/* ******************************************************
*	Package:     pag_pub
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	051125 | Frode Klevstul
*			- Package created
****************************************************** */
	procedure run;
	procedure updateUserInfo
	(
		  p_action				in varchar2										default null
		, p_username			in use_user.username%type						default null
		, p_email				in use_user.email%type							default null
		, p_first_name			in use_details.first_name%type					default null
		, p_last_name			in use_details.last_name%type					default null
		, p_landline			in add_address.landline%type					default null
		, p_mobile				in add_address.mobile%type						default null
		, p_password_old		in use_user.password%type						default null
		, p_password_new1		in use_user.password%type						default null
		, p_password_new2		in use_user.password%type						default null
	);
	function updateUserInfo
	(
		  p_action				in varchar2										default null
		, p_username			in use_user.username%type						default null
		, p_email				in use_user.email%type							default null
		, p_first_name			in use_details.first_name%type					default null
		, p_last_name			in use_details.last_name%type					default null
		, p_landline			in add_address.landline%type					default null
		, p_mobile				in add_address.mobile%type						default null
		, p_password_old		in use_user.password%type						default null
		, p_password_new1		in use_user.password%type						default null
		, p_password_new2		in use_user.password%type						default null
	) return clob;

end;
/
show errors;




-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body use_adm is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'use_adm';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  18.10.2005
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        updateUserInfo
-- what:        update address
-- author:      Frode Klevstul
-- start date:  27.10.2005
---------------------------------------------------------
procedure updateUserInfo(p_action in varchar2 default null, p_username in use_user.username%type default null, p_email in use_user.email%type default null, p_first_name in use_details.first_name%type default null, p_last_name in use_details.last_name%type default null, p_landline in add_address.landline%type default null, p_mobile in add_address.mobile%type default null, p_password_old in use_user.password%type default null, p_password_new1 in use_user.password%type default null, p_password_new2 in use_user.password%type default null) is begin
	ext_lob.pri(use_adm.updateUserInfo(p_action, p_username, p_email , p_first_name, p_last_name, p_landline, p_mobile, p_password_old, p_password_new1, p_password_new2));
end;

function updateUserInfo
(
	  p_action				in varchar2										default null
	, p_username			in use_user.username%type						default null
	, p_email				in use_user.email%type							default null
	, p_first_name			in use_details.first_name%type					default null
	, p_last_name			in use_details.last_name%type					default null
	, p_landline			in add_address.landline%type					default null
	, p_mobile				in add_address.mobile%type						default null
	, p_password_old		in use_user.password%type						default null
	, p_password_new1		in use_user.password%type						default null
	, p_password_new2		in use_user.password%type						default null
) return clob
is
begin
declare
	c_procedure						constant mod_procedure.name%type	:= 'updateUserInfo';

	c_page_width					constant sys_parameter.value%type 	:= sys_lib.getParameter('page_width');
	c_use_user_pk					constant use_user.use_user_pk%type	:= ses_lib.getPk(ses_lib.getId);
	c_username						constant use_user.username%type		:= use_lib.getUsername(c_use_user_pk);
	c_email							constant use_user.username%type		:= use_lib.getEmail(c_use_user_pk);
	c_password						constant use_user.password%type		:= use_lib.getPassword(c_use_user_pk);

	c_text_wrongUsername			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'wrongUsername');
	c_text_wrongEmail				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'wrongEmail');
	c_text_wrongLandline			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'wrongLandline');
	c_text_wrongMobile				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'wrongMobile');
	c_text_updateUserInfo			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'updateUserInfo');
	c_text_clickToUpdate1			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'clickToUpdate1');
	c_text_clickToUpdate2			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'clickToUpdate2');
	c_text_doNotUpdate				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'doNotUpdate');
	c_text_updating					constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'updating');
	c_tex_alternativeUsername		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'alternativeUsername');
	c_tex_username					constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'username');
	c_tex_email						constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'email');
	c_tex_lastName					constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'lastName');
	c_tex_landline					constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'landline');
	c_tex_mobile					constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'mobile');
	c_tex_updatePassword			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'updatePassword');
	c_tex_oldPassword				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'oldPassword');
	c_tex_newPassword				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'newPassword');
	c_tex_repeatPassword			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'repeatPassword');
	c_tex_errNewPwdNull				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'errNewPwdNull');
	c_tex_errPwdNotEqual			constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'errPwdNotEqual');
	c_tex_errWrongPwd				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'errWrongPwd');
	c_tex_viewDetails				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'viewDetails');

	cursor	cur_use_user is
	select	a.street, a.zip, a.landline, a.fax, a.email, a.url, a.add_address_pk, a.mobile, a.name,
			u.username, u.email email1, u.password, d.first_name, d.last_name, d.date_of_birth, d.sex
	from	use_user u, use_details d, add_address a
	where	u.use_user_pk = c_use_user_pk
	and		d.add_address_fk = a.add_address_pk
	and		d.use_user_fk_pk = u.use_user_pk;

	v_address_pk					add_address.add_address_pk%type;
	v_error							boolean								:= false;
	v_username						use_user.username%type;
	v_tmpError						boolean								:= false;
	v_errorMsg						varchar2(256);
	v_error_messages				varchar2(1024);
	v_count							number;

	v_html							clob;
	v_clob							clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_action='||p_action||'��p_username='||p_username);
	ext_lob.ini(v_clob);

	-- check if logged in
	use_lib.loggedInCheck(g_package, c_procedure, null);

	-- -------------------------------------------------
	--  check if username is valid
	-- -------------------------------------------------
	if (p_username is not null and (p_username not like c_username)) then
		ext_lib.verifyValue(
			  p_type		=> 'username'
			, p_value		=> p_username
			, p_returnError => v_tmpError
			, p_returnMsg	=> v_errorMsg
		);

		if (v_tmpError) then
			v_error := true;
			v_error_messages := v_error_messages||''||v_errorMsg||'<br>';

			-- suggest new username
			v_username := use_lib.suggestUsername(p_username);
			v_error_messages := v_error_messages||''||c_tex_alternativeUsername||': '||v_username||'<br>';
		end if;
	end if;

	-- -------------------------------------------------
	--  check if email is valid
	-- -------------------------------------------------
	if (p_email is not null and (p_email not like c_email)) then
		ext_lib.verifyValue(
			  p_type		=> 'email'
			, p_value		=> p_email
			, p_returnError => v_tmpError
			, p_returnMsg	=> v_errorMsg
		);

		if (v_tmpError) then
			v_error := true;
			v_error_messages := v_error_messages||''||v_errorMsg||'<br>';
		end if;
	end if;

	-- -------------------------------------------------
	--  check if password is OK
	-- -------------------------------------------------
	if (p_password_old is not null or p_password_new1 is not null or p_password_new2 is not null) then
		-- check if it is the correct password
		if not( p_password_old = c_password ) then
			v_error := true;
			v_error_messages := v_error_messages||''||c_tex_errWrongPwd||'<br>';
		else
			-- check if new passwords are equal
			if (p_password_new1 <> p_password_new2) then
				v_error := true;
				v_error_messages := v_error_messages||''||c_tex_errPwdNotEqual||'<br>';
			-- check that new passwords are not null
			elsif ( (p_password_new1 is null) or (p_password_new2 is null) ) then
					v_error := true;
					v_error_messages := v_error_messages||''||c_tex_errNewPwdNull||'<br>';
			end if;
		end if;
	end if;

	-- ---------------------------
	-- display update form
	-- ---------------------------
	if (p_action is null or v_error) then
		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'admin')||chr(13);
		v_html := v_html||''||htm.bTable('surroundElements')||chr(13);
		v_html := v_html||'<tr><td>'||chr(13);

		-- ---------------------------------
		-- show possible error messages
		-- ---------------------------------
		if (v_error_messages is not null) then
			v_html := v_html||''||htm.printErrors(v_error_messages)||chr(13);
		end if;

		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td>'||chr(13);

		-- ---------------
		-- javascript
		-- ---------------
		v_html := v_html||'

			<script type="text/javascript">
				function submitForm(){
					var p_email		= document.getElementById(''p_email'');
					var p_landline	= document.getElementById(''p_landline'');
					var p_mobile	= document.getElementById(''p_mobile'');

					if ( !validEmail(p_email.value) && p_email.value.length > 0 ){
						alert("'||c_text_wrongEmail||'");
						return;
					}

					if ( !(p_landline.value.match(/^\d{8}$/)) && p_landline.value.length > 0 ){
						alert("'||c_text_wrongLandline||'");
						return;
					}

					if ( !(p_mobile.value.match(/^\d{8}$/)) && p_mobile.value.length > 0 ){
						alert("'||c_text_wrongMobile||'");
						return;
					}

					document.getElementById(''form'').submit();
				}

			</script>
		';

		-- ---------------------------------------------------------------
		-- check if user has entry in use_details and create it if not
		-- ---------------------------------------------------------------
		select	count(*)
		into	v_count
		from	use_details d
		where	d.use_user_fk_pk = c_use_user_pk;

		if (v_count = 0) then
			insert into use_details (use_user_fk_pk)
			values (c_use_user_pk);
			commit;
		end if;

		-- ---------------------------------------------------------------
		-- in case the user has no entry in add_address we create that
		-- ---------------------------------------------------------------
		select	count(*)
		into	v_count
		from	use_details d, add_address a
		where	d.use_user_fk_pk = c_use_user_pk
		and		d.add_address_fk = a.add_address_pk;

		if (v_count = 0) then
			v_address_pk := sys_lib.getEmptyPk('add_address');

			insert into add_address (add_address_pk)
			values (v_address_pk);

			update	use_details
			set		add_address_fk = v_address_pk
			where	use_user_fk_pk = c_use_user_pk;
			commit;
		end if;

		if (c_use_user_pk is not null) then
			for r_cursor in cur_use_user
			loop
				v_html := v_html||''||htm.bForm('use_adm.updateUserInfo')||chr(13);
				v_html := v_html||'<input type="hidden" name="p_action" value="update">'||chr(13);
				v_html := v_html||''||htm.bTable('surroundElements')||'
					<tr>
						<td width="50%">'||chr(13);

				-- -------------------
				-- main user info
				-- -------------------
				v_html := v_html||''||htm.bBox(c_text_updateUserInfo)||chr(13);
				v_html := v_html||''||htm.bTable||chr(13);

				--v_html := v_html||'<tr><td width="20%">'||c_tex_username||':</td><td><input type="text" name="p_username" id="p_username" value="'||r_cursor.username||'" size="20" maxlength="32"></td></tr>'||chr(13);
				v_html := v_html||'<tr><td width="30%">'||c_tex_username||':</td><td>'||r_cursor.username||'</td></tr>'||chr(13);
				v_html := v_html||'<tr><td>'||c_tex_email||':</td><td><input type="text" name="p_email" id="p_email" value="'||r_cursor.email1||'" size="20" maxlength="64"></td></tr>'||chr(13);
				v_html := v_html||'<tr><td>'||c_tex_lastName||':</td><td><input type="text" name="p_last_name" id="p_last_name" value="'||r_cursor.last_name||'" size="20" maxlength="64"></td></tr>'||chr(13);
				v_html := v_html||'<tr><td>'||c_tex_landline||':</td><td><input type="text" name="p_landline" id="p_landline" value="'||r_cursor.landline||'" size="20" maxlength="8"></td></tr>'||chr(13);
				v_html := v_html||'<tr><td>'||c_tex_mobile||':</td><td><input type="text" name="p_mobile" id="p_mobile" value="'||r_cursor.mobile||'" size="20" maxlength="8"></td></tr>'||chr(13);

				v_html := v_html||'<tr><td colspan="2">&nbsp;</td></tr>'||chr(13);
				v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||htm.submitLink(c_text_clickToUpdate1, null, 'submitForm()')||'</td></tr>'||chr(13);
				v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||htm.historyBack(c_text_doNotUpdate)||'</td></tr>'||chr(13);
				v_html := v_html||''||htm.eTable||chr(13);
				v_html := v_html||''||htm.eBox||chr(13);

				v_html := v_html||'</td><td>'||chr(13);

				-- -------------------
				-- password
				-- -------------------
				v_html := v_html||''||htm.bBox(c_tex_updatePassword)||chr(13);
				v_html := v_html||''||htm.bTable||chr(13);

				v_html := v_html||'<tr><td width="30%">'||c_tex_oldPassword||':</td><td><input type="password" name="p_password_old" id="p_password_old" value="" size="20" maxlength="32"></td></tr>'||chr(13);
				v_html := v_html||'<tr><td>'||c_tex_newPassword||':</td><td><input type="password" name="p_password_new1" id="p_password_new1" value="" size="20" maxlength="32"></td></tr>'||chr(13);
				v_html := v_html||'<tr><td>'||c_tex_repeatPassword||':</td><td><input type="password" name="p_password_new2" id="p_password_new2" value="" size="20" maxlength="32"></td></tr>'||chr(13);

				v_html := v_html||'<tr><td colspan="2">&nbsp;</td></tr>'||chr(13);
				v_html := v_html||'<tr><td colspan="2">&nbsp;</td></tr>'||chr(13);
				v_html := v_html||'<tr><td colspan="2">&nbsp;</td></tr>'||chr(13);
				v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||htm.submitLink(c_text_clickToUpdate2, null, 'submitForm()')||'</td></tr>'||chr(13);
				v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||htm.historyBack(c_text_doNotUpdate)||'</td></tr>'||chr(13);
				v_html := v_html||''||htm.eTable||chr(13);
				v_html := v_html||''||htm.eBox||chr(13);

				v_html := v_html||'</td></tr>'||chr(13);
				v_html := v_html||''||htm.eTable;
				v_html := v_html||''||htm.eForm||chr(13);
			end loop;
		end if;

		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

	elsif (p_action = 'view') then

		v_html := v_html||''||htm.bPage(g_package, c_procedure)||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundAdminBox')||'
			<tr>
				<td>'||chr(13);

		v_html := v_html||''||htm.bBox(c_tex_viewDetails)||chr(13);
		v_html := v_html||''||htm.bTable;

		for r_cursor in cur_use_user
		loop
			v_html := v_html||'<tr><td width="20%">'||c_tex_username||':</td><td>'||r_cursor.username||'&nbsp;</td></tr>'||chr(13);
			v_html := v_html||'<tr><td>'||c_tex_email||':</td><td>'||r_cursor.email1||'&nbsp;</td></tr>'||chr(13);
			v_html := v_html||'<tr><td>'||c_tex_lastName||':</td><td>'||r_cursor.last_name||'&nbsp;</td></tr>'||chr(13);
			v_html := v_html||'<tr><td>'||c_tex_landline||':</td><td>'||r_cursor.landline||'&nbsp;</td></tr>'||chr(13);
			v_html := v_html||'<tr><td>'||c_tex_mobile||':</td><td>'||r_cursor.mobile||'&nbsp;</td></tr>'||chr(13);
		end loop;

		v_html := v_html||''||htm.eTable;
		v_html := v_html||''||htm.eBox||chr(13);

		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable;
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);


	-- ---------------------------
	-- update the database
	-- ---------------------------
	elsif (p_action = 'update') then

		if (c_use_user_pk is not null) then
			for r_cursor in cur_use_user
			loop
				v_address_pk := r_cursor.add_address_pk;

				-- add entries to history table
				his_lib.addEntry('use_user', 'email', r_cursor.email1);
				his_lib.addEntry('use_user', 'password', r_cursor.password);
			end loop;

			if (v_address_pk is not null) then
				update	add_address
				set		landline	= p_landline
					  , mobile		= p_mobile
				where	add_address_pk = v_address_pk;
				commit;
			end if;

			update	use_details
			set		last_name		= p_last_name
			where	use_user_fk_pk	= c_use_user_pk;
			commit;

			if (p_email is not null and p_password_new1 is not null) then
				update	use_user
				set		email		= lower(p_email)
					  , password	= p_password_new1
				where	use_user_pk = c_use_user_pk;
				commit;
			elsif (p_email is not null) then
				update	use_user
				set		email		= lower(p_email)
				where	use_user_pk = c_use_user_pk;
				commit;
			end if;

		end if;

		v_html := v_html||htm.jumpTo('use_adm.updateUserInfo?p_action=view', 1, c_text_updating);

	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		return null;
end;
end updateUserInfo;


-- ******************************************** --
end;
/
show errors;
