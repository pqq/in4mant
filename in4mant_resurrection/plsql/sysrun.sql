PROMPT *** sysrun ***
set define off
set serveroutput on;

-- -------------------------------------------
-- run as system:
--
-- grant create any library to i4_mirror;
-- -------------------------------------------

create OR replace library shell_lib as
'/usr/lib/oracle/xe/app/oracle/product/10.2.0/server/lib/extproc.so';
/
show errors;


CREATE OR REPLACE function sysrun (syscomm in varchar2)
				   return string
  as language C
  name "sysrun"
  library shell_lib
  parameters(syscomm string);
/
show errors;
