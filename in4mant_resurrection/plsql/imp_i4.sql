PROMPT *** imp_i4 ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package imp_i4 is
/* ******************************************************
*	Package:     tex_lib
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	051222 | Frode Klevstul
*			- Package created
****************************************************** */
	type parameter_arr 			is table of varchar2(4000) index by binary_integer;
	empty_array 				parameter_arr;

	procedure run;
	procedure importOrg
	(
		  p_action						varchar2											default null
		, p_service_pk					ser_service.ser_service_pk%type						default null
		, p_org_status_pk				org_status.org_status_pk%type						default null
		, p_zip_start					add_address.zip%type								default null
		, p_zip_stop					add_address.zip%type								default null
		, p_org_number					org_organisation.org_number%type					default null
		, p_name_parent					imp_nightclub_owner.name%type						default null
		, p_name_child					imp_nightclub.name%type								default null
		, p_status						varchar2											default null
	);
	function importOrg
	(
		  p_action						varchar2											default null
		, p_service_pk					ser_service.ser_service_pk%type						default null
		, p_org_status_pk				org_status.org_status_pk%type						default null
		, p_zip_start					add_address.zip%type								default null
		, p_zip_stop					add_address.zip%type								default null
		, p_org_number					org_organisation.org_number%type					default null
		, p_name_parent					imp_nightclub_owner.name%type						default null
		, p_name_child					imp_nightclub.name%type								default null
		, p_status						varchar2											default null
	) return clob;

end;
/
show errors;





-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body imp_i4 is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'imp_i4';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  22.12.2005
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        importOrg
-- what:        import organisations
-- author:      Frode Klevstul
-- start date:  03.04.2006

-- note:		This function outputs too much for one clob to
--				handle, so it prints out HTML directly!
---------------------------------------------------------
procedure importOrg
(
	  p_action						varchar2											default null
	, p_service_pk					ser_service.ser_service_pk%type						default null
	, p_org_status_pk				org_status.org_status_pk%type						default null
	, p_zip_start					add_address.zip%type								default null
	, p_zip_stop					add_address.zip%type								default null
	, p_org_number					org_organisation.org_number%type					default null
	, p_name_parent					imp_nightclub_owner.name%type						default null
	, p_name_child					imp_nightclub.name%type								default null
	, p_status						varchar2											default null
) is begin
   ext_lob.pri(imp_i4.importOrg(p_action, p_service_pk, p_org_status_pk, p_zip_start, p_zip_stop, p_org_number, p_name_parent, p_name_child, p_status));
end;

function importOrg
(
	  p_action						varchar2											default null
	, p_service_pk					ser_service.ser_service_pk%type						default null
	, p_org_status_pk				org_status.org_status_pk%type						default null
	, p_zip_start					add_address.zip%type								default null
	, p_zip_stop					add_address.zip%type								default null
	, p_org_number					org_organisation.org_number%type					default null
	, p_name_parent					imp_nightclub_owner.name%type						default null
	, p_name_child					imp_nightclub.name%type								default null
	, p_status						varchar2											default null
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type 		:= 'importOrg';

	v_count						number;
	v_count_ncOwn				number;
	v_count_nc					number;
	v_count_orgUmb				number;
	v_count_org					number;
	v_count_ins					number;
	v_count_upd					number;
	v_count_noc					number;
	v_count_ownNoZip			number;
	v_count_ncNoZip				number;
	v_count_ncOwnWoChi			number;
	v_count_ncWoPar				number;
	v_count_noImpOwn			number;
	v_count_noImpNc				number;
	v_count_noNotImpOwn			number;
	v_count_noNotImpNc			number;

	v_i_ncOwn					number;
	v_i_nc						number;
	v_i_tot						number;
	v_i_arrayNcOwn				number;
	v_i_arrayNc					number;

	v_lastInsert				imp_nightclub.date_insert%type;
	v_lastImport				imp_nightclub.date_import%type;
	v_status					varchar2(64);
	v_org_status_name			org_status.name%type;

	v_error						boolean								:= false;

	v_organisation_pk			org_organisation.org_organisation_pk%type;
	v_address_pk				add_address.add_address_pk%type;
	v_geography_pk				geo_geography.geo_geography_pk%type;

	v_updated_ncOwn				parameter_arr;
	v_updated_nc				parameter_arr;

	cursor	cur_imp_nightclub_owner is
	select	distinct(nco.org_number), nco.name, nco.street, nco.council, nco.zip, nco.landline, nco.fax, nco.email, nco.url, nco.source, nco.date_insert, nco.date_import
	from	imp_nightclub_owner nco, imp_nightclub nc
	where	nco.org_number 		= nc.org_number
	and		to_number(nco.zip) >= nvl(p_zip_start, 0)
	and		to_number(nco.zip) <= nvl(p_zip_stop, 9999)
	and		nco.org_number		like nvl(p_org_number, '%')
	and		lower(nco.name)		like '%'||lower(p_name_parent)||'%'
	and		lower(nc.name) 		like '%'||lower(p_name_child)||'%';

	cursor	cur_imp_nightclub(b_org_number imp_nightclub.org_number%type) is
	select	imp_nightclub_pk, org_number, name, street, council, zip, landline, fax, email, url, bool_has_beer, bool_has_wine, bool_has_spirits, source, date_insert, date_import
	from	imp_nightclub
	where	org_number = b_org_number
	and		zip is not null
	and		lower(name) like '%'||lower(p_name_child)||'%'
	and		date_import is null;

	cursor	cur_org_organisation is
	select	org_organisation_pk, name, org_number
	from	org_organisation
	where	org_organisation_pk not in
	(
		select	org_organisation_fk_pk
		from	org_original
	);

	v_html 						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_action='||p_action||'いp_service_pk='||p_service_pk||'いp_org_status_pk='||p_org_status_pk||'いp_zip_start='||p_zip_start||'いp_zip_stop='||p_zip_stop||'いp_org_number='||p_org_number||'いp_name_parent='||p_name_parent||'いp_name_child='||p_name_child||'いp_status='||p_status);
	ext_lob.ini(v_clob);

	use_lib.loggedInCheck(g_package, c_procedure, null);

	-- --------------------------------------------------------------------------
	-- add fictious parent to imp_organisation_owner if it doesn't exist
	-- --------------------------------------------------------------------------
	select	count(*)
	into	v_count
	from	imp_nightclub_owner
	where	org_number = '900'
	and		lower(name) = '900 - fictitious parent';

	if (v_count = 0) then
		insert into imp_nightclub_owner
		(org_number, name, zip, source, date_insert)
		values ('900', '900 - Fictitious Parent', '0000', 'imp_i4.sql auto creation', sysdate);

		commit;
	end if;

	-- --------------------------------------------------------------------------
	-- make sure there are no missing entries in org_original
	-- --------------------------------------------------------------------------
	select	count(*)
	into	v_count
	from	org_organisation
	where	org_organisation_pk not in
	(
		select org_organisation_fk_pk from org_original
	);

	if (v_count > 0) then
		for r_cursor in cur_org_organisation
		loop
			insert into org_original
			(org_organisation_fk_pk, name, org_number)
			values (r_cursor.org_organisation_pk, r_cursor.name, r_cursor.org_number);
		end loop;

		commit;
	end if;

	-- -------------------------------------------------------------------------------
	-- p_action is null : ask user to set service
	-- -------------------------------------------------------------------------------
	if (p_action is null) then
		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'i4admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundAdminBox')||chr(13);
		v_html := v_html||'<tr><td>'||chr(13);

		v_html := v_html||''||htm.bBox('Set service')||chr(13);
		v_html := v_html||''||htm.bForm(g_package||'.'||c_procedure)||chr(13);
		v_html := v_html||'<input type="hidden" name="p_action" value="mainForm">'||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);


		v_html := v_html||'<tr><td>Service:</td><td>'||htm_lib.selectService||'</td></tr>'||chr(13);

		v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||htm.submitLink('set service')||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||htm.historyBack('go back')||'</td></tr>'||chr(13);

		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eForm||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);

		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

	-- -------------------------------------------------------------------------------
	-- p_action = 'mainForm' : display stats and option to generate import list
	-- -------------------------------------------------------------------------------
	elsif (p_action = 'mainForm' and p_service_pk is not null) then

		-- ------------------------------------
		-- select some stat from the database
		-- ------------------------------------
		select	count(*)
		into	v_count_ncOwn
		from	imp_nightclub_owner;

		select	count(*)
		into	v_count_nc
		from	imp_nightclub;

		select	count(*)
		into	v_count_orgUmb
		from	org_organisation o, org_ser s
		where	o.org_organisation_pk = s.org_organisation_fk_pk
		and		s.ser_service_fk_pk = p_service_pk
		and		bool_umbrella_organisation > 0;

		select	count(*)
		into	v_count_org
		from	org_organisation o, org_ser s
		where	o.org_organisation_pk = s.org_organisation_fk_pk
		and		s.ser_service_fk_pk = p_service_pk
		and		(bool_umbrella_organisation is null or bool_umbrella_organisation = 0);

		select	max(date_insert)
		into	v_lastInsert
		from	imp_nightclub;

		select	max(date_import)
		into	v_lastImport
		from	imp_nightclub;

		select	count(*)
		into	v_count_ncNoZip
		from	imp_nightclub
		where	zip is null;

		select	count(*)
		into	v_count_ownNoZip
		from	imp_nightclub_owner
		where	zip is null;

		select	count(*)
		into	v_count_ncNoZip
		from	imp_nightclub
		where	zip is null;

		select	count(*)
		into	v_count_ncOwnWoChi
		from	imp_nightclub_owner 
		where	
			(	org_number not in
					(
						select	org_number
						from	imp_nightclub
					)
				and org_number not like '900'
			)
		or org_number is null;

		select	count(*)
		into	v_count_ncWoPar
		from	imp_nightclub
		where	org_number not in
			(
				select org_number from imp_nightclub_owner where org_number is not null
			)
		or		org_number is null;

		select	count(*)
		into	v_count_noImpOwn
		from	imp_nightclub_owner
		where	date_import is not null;

		select	count(*)
		into	v_count_noImpNc
		from	imp_nightclub
		where	date_import is not null;

		select	count(*)
		into	v_count_noNotImpOwn
		from	imp_nightclub_owner
		where	date_import is null;

		select	count(*)
		into	v_count_noNotImpNc
		from	imp_nightclub
		where	date_import is null;

		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'i4admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundAdminBox')||chr(13);
		v_html := v_html||'<tr><td>'||chr(13);

		-- ------------------------------------
		-- form to change service
		-- ------------------------------------
		v_html := v_html||''||htm.bBox('Change service')||chr(13);
		v_html := v_html||''||htm.bForm(g_package||'.'||c_procedure)||chr(13);
		v_html := v_html||'<input type="hidden" name="p_action" value="mainForm">'||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);

		v_html := v_html||'
			<tr>
				<td>
					Service:
				</td>
				<td>
					'||htm_lib.selectService(p_service_pk)||'
				</td>
				<td style="padding-left: 20px;">
					'||htm.submitLink('change service')||'
				</td>
			</tr>
		';

		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eForm||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);

		v_html := v_html||'</td></tr><tr><td class="surroundAdminBox">'||chr(13);

		-- ------------------------------------
		-- import organisations stats
		-- ------------------------------------
		v_html := v_html||''||htm.bBox('Import Organisations')||chr(13);
		v_html := v_html||''||htm.bForm(g_package||'.'||c_procedure, 'importForm')||chr(13);
		v_html := v_html||'<input type="hidden" name="p_action" value="importVerify">'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_service_pk" value="'||p_service_pk||'">'||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);

		v_html := v_html||'
			<tr bgcolor="#FFCC66"><td colspan="4"><b>Entires in the database:</b></td></tr>
			<tr bgcolor="#FFCC66"><td colspan="4"><i>
				External data is inserted into the import tables. Data in those tables can not be
				viewed or accessed by the normal end user. Running the "imp i4" (this module) will
				move data from the import tables to the organisation table (ORG_ORGANISATION) where
				it can be accessed. Each entry in both the import tables and the organisation table
				has to be of type child or parent. Each child has to be connected to a parent. Example:
				The company "Umbrella Organisation Ltd." is the owner, the company "Is owned" is the
				child. Each parent should also have a child.
				</i>
			</td></tr>
			<tr>
				<td colspan="2" bgcolor="#aaaaaa" width="50%"><b>IMPORT TABLES</b></td>
				<td colspan="2" bgcolor="#888888"><b>ORG TABLES</b></td>
			</tr>
			<tr>
				<td bgcolor="#cccccc">
					Nightclub owners:
				</td>
				<td bgcolor="#cccccc">
					'||v_count_ncOwn||'
				</td>
				<td bgcolor="#bbbbbb">
					Umbrella Organisations:
				</td>
				<td bgcolor="#bbbbbb">
					'||v_count_orgUmb||'
				</td>
			</tr>
			<tr>
				<td bgcolor="#cccccc">
					Nightclubs:
				</td>
				<td bgcolor="#cccccc">
					'||v_count_nc||'
				</td>
				<td bgcolor="#bbbbbb">
					Child Organisations:
				</td>
				<td bgcolor="#bbbbbb">
					'||v_count_org||'
				</td>
			</tr>
			<tr><td colspan="4">&nbsp;</td></tr>
			<tr><td colspan="4">&nbsp;</td></tr>
		';

		-- ---------------------------------------------------
		-- check if a new update / import should be done
		-- ---------------------------------------------------
		v_html := v_html||'
			<tr bgcolor="#FFCC66"><td colspan="4"><b>Time to update:</b></td></tr>
			<tr bgcolor="#FFCC66"><td colspan="4"><i>
				Below you can see the last time when new data was inserted / added to the IMP tables
				(last insert) and the last time data was imported into the ORG tables (last import).
				You can see how many owners and nightclups there are to be imported. It''s also possible
				to view new data in kAdmin (if the number of new data is high, more than 100 entries,
				it''ll take a while to load kAdmin.<br />
				<b>Note:</b> if you reset last import previously imported data will be processed again. 
				You should not reset unless it''s absolutely neccesary to avoid wrongly updating data
				that might have been changed since last import.
				</i>
			</td></tr>'||chr(13);
		v_html := v_html||'
			<tr bgcolor="#cccccc"><td colspan="4"><b>Last insert:</b> '||v_lastInsert||' <b>Last import:</b> '||v_lastImport||' ( '||htm.link('reset', g_package||'.'||c_procedure||'?p_action=reset_last_import&amp;p_service_pk='||p_service_pk)||')<br />
			<tr bgcolor="#cccccc"><td colspan="4">
				<b>OWNERS:</b>&nbsp;
				new:&nbsp;
				<a href="kAdmin.admin_table?p_action=list&amp;p_table_name=IMP_NIGHTCLUB_OWNER&amp;p_sql_stmt=WHERE%20DATE_IMPORT%20IS%20NULL" target="_blank">'||v_count_noNotImpOwn||' entries</a>&nbsp;
				imported: 
				<a href="kAdmin.admin_table?p_action=list&amp;p_table_name=IMP_NIGHTCLUB_OWNER&amp;p_sql_stmt=WHERE%20DATE_IMPORT%20IS%20NOT%20NULL" target="_blank">'||v_count_noImpOwn||' entries</a><br />
				<b>CLUBS:</b>&nbsp;
				new:&nbsp;
				<a href="kAdmin.admin_table?p_action=list&amp;p_table_name=IMP_NIGHTCLUB&amp;p_sql_stmt=WHERE%20DATE_IMPORT%20IS%20NULL" target="_blank">'||v_count_noNotImpNc||' entries</a>&nbsp;
				imported:
				<a href="kAdmin.admin_table?p_action=list&amp;p_table_name=IMP_NIGHTCLUB&amp;p_sql_stmt=WHERE%20DATE_IMPORT%20IS%20NOT%20NULL" target="_blank">'||v_count_noImpNc||' entries</a><br />
				</td></tr>
		';

		v_html := v_html||'<tr bgcolor="#cccccc"><td colspan="4">'||chr(13);
		if (v_count_noNotImpOwn > 0 or v_count_noNotImpNc > 0) then
			v_html := v_html||'<font color="green">There are new entries in IMP. <b>You should update now!</b></font>'||chr(13);
		else
			v_html := v_html||'<font color="red">No new data to be imported!</b></font>'||chr(13);
		end if;
		v_html := v_html||'</td></tr>
			<tr><td colspan="4">&nbsp;</td></tr>
			<tr><td colspan="4">&nbsp;</td></tr>
		'||chr(13);

		-- ------------------------------------------------
		-- error handling
		-- ------------------------------------------------
		if (v_count_ownNoZip > 0 or v_count_ncNoZip > 0) then
			v_error := true;
			v_html := v_html||'
				<tr bgcolor="#ff5555">
					<td colspan="4">
						<b>Error : missing ZIP :</b><br />
						There are
						<a href="kAdmin.admin_table?p_action=list&amp;p_table_name=IMP_NIGHTCLUB_OWNER&amp;p_sql_stmt=WHERE%20ZIP%20IS%20NULL" target="_blank">'||v_count_ownNoZip||' owners</a>
						and
						<a href="kAdmin.admin_table?p_action=list&amp;p_table_name=IMP_NIGHTCLUB&amp;p_sql_stmt=WHERE%20ZIP%20IS%20NULL" target="_blank">'||v_count_ncNoZip||' nightclubs</a>
						 with no zip that won''t be inserted!
					</td>
				</tr>
			';
		end if;

		if (v_count_ncOwnWoChi > 0 or v_count_ncWoPar > 0) then
			v_error := true;
			v_html := v_html||'
				<tr bgcolor="#ff5555">
					<td colspan="4">
						<b>Error : someone is lonely :</b><br />
						There are
						<a href="kAdmin.admin_table?p_action=list&amp;p_table_name=IMP_NIGHTCLUB_OWNER&amp;p_sql_stmt=WHERE%20ORG_NUMBER%20NOT%20IN%20(SELECT%20ORG_NUMBER%20FROM%20IMP_NIGHTCLUB%20WHERE%20ORG_NUMBER%20IS%20NOT%20NULL)%20OR%20ORG_NUMBER%20IS%20NULL%20AND%20ORG_NUMBER%20NOT%20LIKE%20''900''" target="_blank">'||v_count_ncOwnWoChi||' parents</a>
						without children and
						<a href="kAdmin.admin_table?p_action=list&amp;p_table_name=IMP_NIGHTCLUB&amp;p_sql_stmt=WHERE%20ORG_NUMBER%20NOT%20IN(SELECT%20ORG_NUMBER%20FROM%20IMP_NIGHTCLUB_OWNER%20WHERE%20ORG_NUMBER%20IS%20NOT%20NULL)OR%20ORG_NUMBER%20IS%20NULL" target="_blank">'||v_count_ncWoPar||' children</a>
						 without parents!
					</td>
				</tr>
				<tr bgcolor="#ff5555">
					<td colspan="4">
						Delete statement to "kill" lonely parents:<br />
						<i>
							delete
							from	imp_nightclub_owner 
							where   (org_number not in
								(
									select org_number
									from imp_nightclub
								)
							and org_number not like ''900'')
							or org_number is null;
						</i>
					</td>
				</tr>
				<tr bgcolor="#ff5555">
					<td colspan="4">
						Delete statement to "kill" orphans:<br />
						<i>
							delete 
							from imp_nightclub
							where org_number not in(
								select org_number from imp_nightclub_owner where org_number is not null
							) or org_number is null;</i>
					</td>
				</tr>
			';
		end if;

		if (v_error) then
			v_html := v_html||'
				<tr bgcolor="#ff5555"><td colspan="4">&nbsp;</td></tr>
				<tr bgcolor="#ff5555"><td colspan="4"><i>
					Click on the links in the text above to fix the errors using kAdmin.
					</i>
				</td></tr>
				<tr><td colspan="4">&nbsp;</td></tr>
			';
		end if;

		-- ------------------------------------------------
		-- set organisation status
		-- ------------------------------------------------
		v_html := v_html||'
			<tr bgcolor="#FFCC66"><td colspan="4"><b>Set org status:</b></td></tr>
			<tr bgcolor="#FFCC66"><td colspan="4"><i>
				Here you chose what status new organisations (nightclubs), that are inserted for the first time, will get.<br />
				1 : open : normal mode<br />
				2 : member : profiled organisations<br />
				3 : closed : will show as "closed"<br />
				4 : banned : won''t show anywhere on the site<br />
				</i>
			</td></tr>
		';

		v_html := v_html||'<tr bgcolor="#cccccc"><td colspan="4"><b>org_status:</b> '||htm_lib.selectOrgStatus(1)||'</td></tr>'||chr(13);

		-- ------------------------------------------------
		-- limit import data
		-- ------------------------------------------------
		v_html := v_html||'
			<tr><td colspan="4">&nbsp;</td></tr>
			<tr><td colspan="4">&nbsp;</td></tr>
			<tr bgcolor="#FFCC66"><td colspan="4"><b>Limit import data:</b></td></tr>
			<tr bgcolor="#FFCC66"><td colspan="4"><i>
				Below you can limit the data to be imported into the ORG tables. If you want to process all data in the IMP tabels you do not have to change anything.
				</i>
			</td></tr>
		';

		v_html := v_html||'<tr bgcolor="#cccccc"><td colspan="4"><b>ZIP:</b> <i>This import script will only import parent organisations that has ZIP bigger or equal to "ZIP start" and less or equal to "ZIP stop". The script will also import all these parent organisations children.</i></td></tr>'||chr(13);
		v_html := v_html||'<tr bgcolor="#cccccc"><td colspan="4">ZIP start: <input type="tex" size="4" maxlength="4" name="p_zip_start" value="0"> ZIP stop: <input type="tex" size="4" maxlength="4" name="p_zip_stop" value="9999"></td></tr>'||chr(13);
		v_html := v_html||'<tr bgcolor="#aaaaaa"><td colspan="4"><b>Organisation number:</b> <i>If you only want to import organisations with a given org. number please specify that number here. Leave the field empty to avoid any restriction on org number.</i></td></tr>'||chr(13);
		v_html := v_html||'<tr bgcolor="#aaaaaa"><td colspan="4">Organisation number: <input type="tex" size="10" maxlength="9" name="p_org_number"></td></tr>'||chr(13);
		v_html := v_html||'<tr bgcolor="#cccccc"><td colspan="4"><b>Parent name:</b> <i>Limit the import by specifying a name. Only parent organisations with name like the string given in this field will be imported (a non case sensitive ''%string%'' search is used).</i></td></tr>'||chr(13);
		v_html := v_html||'<tr bgcolor="#cccccc"><td colspan="4">Parent name like: <input type="tex" size="10" maxlength="64" name="p_name_parent"></td></tr>'||chr(13);
		v_html := v_html||'<tr bgcolor="#aaaaaa"><td colspan="4"><b>Child name:</b> <i>Only import children with name like given string.</i></td></tr>'||chr(13);
		v_html := v_html||'<tr bgcolor="#aaaaaa"><td colspan="4">Child name like: <input type="tex" size="10" maxlength="64" name="p_name_child"></td></tr>'||chr(13);

		-- ------------------------------------------------
		-- specify data view options
		-- ------------------------------------------------
		v_html := v_html||'
			<tr><td colspan="4">&nbsp;</td></tr>
			<tr><td colspan="4">&nbsp;</td></tr>
			<tr bgcolor="#FFCC66"><td colspan="4"><b>Specify data view:</b></td></tr>
			<tr bgcolor="#FFCC66"><td colspan="4"><i>
				Specify what data you will view when importing. Note that this will not affect what is going to be imported, only what data you will see. To view all data that is processed by this import script you have to chose "show all".
				</i>
			</td></tr>
		';

		v_html := v_html||'
			<tr>
				<td colspan="4">
					Insert:<input type="radio" name="p_status" value="insert">&nbsp;&nbsp;
					Update:<input type="radio" name="p_status" value="update">&nbsp;&nbsp;
					No change:<input type="radio" name="p_status" value="no change">&nbsp;&nbsp;
					Show all:<input type="radio" name="p_status" value="" checked>&nbsp;&nbsp;
					Show none:<input type="radio" name="p_status" value="show_none">
				</td>
			</tr>'||chr(13);

		if (v_error) then
			v_html := v_html||'<tr><td colspan="4" style="padding-left: 20px;"><b>You cannot run the import now. If any errors fix them and reload this page!</b></td></tr>'||chr(13);
		else
			v_html := v_html||'
				<tr><td colspan="4">&nbsp;</td></tr>
				<tr><td colspan="4">&nbsp;</td></tr>
				<tr bgcolor="#FFCC66"><td colspan="4"><b>Generate import list:</b></td></tr>
				<tr bgcolor="#FFCC66"><td colspan="4"><i>
					When you''re ready start generating the import list. Note that this might take several minutes if there is a lot of data in the database!
					</i>
				</td></tr>
			';
			v_html := v_html||'<tr><td colspan="4" style="padding-left: 20px;">'||htm.submitLink('generate import list', 'importForm')||'</td></tr>'||chr(13);
			v_html := v_html||'<tr><td colspan="4" style="padding-left: 20px;">'||htm.historyBack('go back')||'</td></tr>'||chr(13);
		end if;

		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eForm||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);

		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

	-- -------------------------------------------------------------------------------------------------------
	-- p_action = 'import%' ('import' or 'importVerify') : display import list and/or insert into database
	-- -------------------------------------------------------------------------------------------------------
	elsif (p_action like 'import%' and p_service_pk is not null) then

		if (p_org_status_pk is null) then
			htp.p('p_org_status_pk is null');
			return null;
		else
			select	name
			into	v_org_status_name
			from	org_status
			where	org_status_pk = p_org_status_pk;
		end if;

		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'i4admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundAdminBox')||chr(13);
		v_html := v_html||'<tr><td>'||chr(13);

		v_html := v_html||''||htm.bBox('Import List')||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);
		v_html := v_html||'<tr bgcolor="#cccccc"><td colspan="6" style="text-align: center; font-size: 15px;"> New and updated child organisations will get status:</td></tr>';
		v_html := v_html||'<tr bgcolor="#cccccc"><td colspan="6" style="text-align: center; font-size: 20px;"> <b>'||v_org_status_name||'</b></td></tr>';
		v_html := v_html||'<tr><td colspan="6">&nbsp;</td></tr>';
		v_html := v_html||'
			<tr>
				<td><b>no:</b></td>
				<td><b>org no:</b></td>
				<td><b>name:</b></td>
				<td><b>council:</b></td>
				<td><b>zip:</b></td>
				<td><b>status:</b></td>
			</tr>
		';

		v_i_ncOwn		:= 0;
		v_i_nc			:= 0;
		v_i_tot			:= 0;
		v_count_ins		:= 0;
		v_count_upd		:= 0;
		v_count_noc		:= 0;
		v_i_arrayNcOwn	:= 0;
		v_i_arrayNc		:= 0;

		-- ------------------------------------
		--
		-- prepare nightclub owners (parent)
		--
		-- ------------------------------------
		for r_cursor in cur_imp_nightclub_owner
		loop
			-- --------------------------------------------------------
			-- do we have this parent/umbrella/owner in the org table?
			-- --------------------------------------------------------
			select	count(*)
			into	v_count
			from	org_organisation
			where	org_number = r_cursor.org_number
			and		bool_umbrella_organisation > 0;

			-- -------------------------
			-- owner is a new entry
			-- -------------------------
			if (v_count = 0) then
				v_count_ins := v_count_ins + 1;
				v_status := '<font color="green">NEW - insert</font>';

			-- -------------------------
			-- owner exists from before
			-- -------------------------
			else

				-- --------------------------------------------
				-- check if this parent org is locked (IMPLock)
				-- --------------------------------------------
				select	count(*)
				into	v_count
				from	org_organisation
				where	org_number = r_cursor.org_number
				and		bool_umbrella_organisation > 0
				and		bool_implock > 0;

				if (v_count > 0) then
					v_count_noc := v_count_noc + 1;
					v_status := '<font color="red">LOCKED - no change</font>';

				else
					-- -------------------------------------------------------
					-- check if this parent has children that are members
					-- -------------------------------------------------------
					select	count(*)
					into	v_count
					from	org_organisation
					where	parent_org_organisation_fk in
						(
							select	org_organisation_pk
							from	org_organisation
							where	org_number = r_cursor.org_number
						)
					and		org_status_fk =
						(
							select org_status_pk
							from org_status
							where lower(name) = 'member'
						);

					-- ----------------------
					-- has member children
					-- ----------------------
					if (v_count > 0) then
						v_count_noc := v_count_noc + 1;
						v_status := '<font color="gray">HAS CHILD MEMBER - no change</font>';

					-- ----------------------
					-- no member children
					-- ----------------------
					else
						v_count_upd := v_count_upd + 1;
						v_status := '<font color="blue">EXISTS - update</font>';
					end if;
				end if;

			end if;

			-- -------------------------------------------------
			--
			-- insert parent / umbrella / nightclub owner
			--
			-- -------------------------------------------------
			if (p_action = 'import') then

				-- --------------------------
				-- if update select out PKs
				-- --------------------------
				if (lower(v_status) like '%update%') then

					select	org_organisation_pk, add_address_fk
					into	v_organisation_pk, v_address_pk
					from	org_organisation
					where	bool_umbrella_organisation > 0
					and		org_number = r_cursor.org_number;

				-- ---------------------------------------------------
				-- if new insert create new entries in ORG and ADD
				-- ---------------------------------------------------
				elsif (lower(v_status) like '%insert%') then

					v_organisation_pk	:= sys_lib.getEmptyPk('org_organisation');
					v_address_pk		:= sys_lib.getEmptyPk('add_address');

					-- -------------------------
					-- insert new organisation
					-- -------------------------
					insert into org_organisation
					(org_organisation_pk, org_number, source, date_status_change, date_insert)
					values (
						  v_organisation_pk
						, r_cursor.org_number
						, 'imp_i4:'||r_cursor.source
						, sysdate
						, sysdate
					);

					insert into org_original
					(org_organisation_fk_pk, name, org_number)
					values (
						  v_organisation_pk
						, r_cursor.name
						, r_cursor.org_number
					);

					-- -------------------------
					-- insert new address
					-- -------------------------
					insert into add_address
					(add_address_pk, date_insert)
					values (
						  v_address_pk
						, sysdate
					);

					-- ------------------------------------------
					-- connect the organisation and the address
					-- ------------------------------------------
					update	org_organisation
					set		bool_umbrella_organisation	= 1
							, add_address_fk			= v_address_pk
					where	org_organisation_pk			= v_organisation_pk;

					-- ------------------------------------------
					-- connect organisation on selected service
					-- ------------------------------------------
					insert into org_ser
					(org_organisation_fk_pk, ser_service_fk_pk)
					values(v_organisation_pk, p_service_pk);

				end if;

				-- -----------------------------------------------
				-- for both insert and update we modify the entries
				-- -----------------------------------------------
				if (lower(v_status) like '%insert%' or lower(v_status) like '%update%' ) then

					update	org_organisation
					set		name		= r_cursor.name
						  , date_update = sysdate
					where	org_organisation_pk = v_organisation_pk;

					update	add_address
					set		street		= r_cursor.street
						  , zip			= r_cursor.zip
						  , landline	= r_cursor.landline
						  , fax			= r_cursor.fax
						  , email		= r_cursor.email
						  , url			= r_cursor.url
						  , geo_geography_fk =
						  	(
								select	nvl(geo_geography_pk, null)
								from	geo_geography, geo_zip
								where	geo_geography_pk = geo_geography_fk_pk
								and		zip_pk = r_cursor.zip
						  	)
						  , date_update	= sysdate
					where add_address_pk = v_address_pk;

					commit;

					-- update the array containing all updated organisations
					v_i_arrayNcOwn := v_i_arrayNcOwn + 1;
					v_updated_ncOwn(v_i_arrayNcOwn) := r_cursor.org_number;

				end if;
			end if;

			ext_lob.add(v_clob, v_html);
			ext_lob.pri(v_clob);
			dbms_lob.trim(v_clob, 0);

			-- ------------------------------------------------
			-- only print the status the user wants to view
			-- ------------------------------------------------
			if (p_status is null or v_status like '%'||p_status||'%') then
				v_i_ncOwn := v_i_ncOwn + 1;
				v_i_tot := v_i_tot + 1;
				v_html := v_html||'
					<tr>
						<td><b>'||v_i_ncOwn||'</b> ('||v_i_tot||'):</td>
						<td>'||r_cursor.org_number||'</td>
						<td bgcolor="#FFCC66">'||r_cursor.name||'</td>
						<td>'||r_cursor.council||'</td>
						<td>'||r_cursor.zip||'</td>
						<td>'||v_status||'</td>
					</tr>
				';
			end if;

			-- --------------------------------------------
			--
			-- prepare nigthclub (child)
			--
			-- --------------------------------------------
			for r_cursor2 in cur_imp_nightclub(r_cursor.org_number)
			loop

				-- ---------------------------------------
				-- do we have this child-org from before
				-- ---------------------------------------
				select	count(*)
				into	v_count
				from	org_organisation o, add_address a, org_original i
				where	parent_org_organisation_fk in
					(
						select	org_organisation_pk
						from	org_organisation
						where	org_number = r_cursor.org_number
					)
				and	(
						bool_umbrella_organisation is null or
						bool_umbrella_organisation = 0
				)
				and		lower(i.name) = lower(ltrim(rtrim(r_cursor2.name)))
				and		a.add_address_pk		= o.add_address_fk
				and		o.org_organisation_pk	= i.org_organisation_fk_pk
				and		a.zip					= r_cursor2.zip;

				if (v_count = 0) then
					v_count_ins := v_count_ins + 1;
					v_status := '<font color="green">NEW - insert</font>';

				else
					-- --------------------------------------------
					-- check if this child org is locked (IMPLock)
					-- --------------------------------------------
					select	count(*)
					into	v_count
					from	org_organisation o, add_address a, org_original i
					where	parent_org_organisation_fk in
						(
							select	org_organisation_pk
							from	org_organisation
							where	org_number = r_cursor.org_number
						)
					and		lower(i.name)			= lower(ltrim(rtrim(r_cursor2.name)))
					and		a.add_address_pk		= o.add_address_fk
					and		o.org_organisation_pk	= i.org_organisation_fk_pk
					and		a.zip 					= r_cursor2.zip
					and		o.bool_implock 			> 0;

					if (v_count > 0) then
						v_count_noc := v_count_noc + 1;
						v_status := '<font color="red">LOCKED - no change</font>';

					else

						-- --------------------------------------------------
						-- check if this child's parent is locked (IMPLock)
						-- --------------------------------------------------
						select	count(*)
						into	v_count
						from	org_organisation
						where	org_number = r_cursor.org_number
						and		bool_implock > 0;

						if (v_count > 0) then
							v_count_noc := v_count_noc + 1;
							v_status := '<font color="red">PARENT LOCKED - no change</font>';

						else

							-- --------------------------------------
							-- check if this child org is a member
							-- --------------------------------------
							select	count(*)
							into	v_count
							from	org_organisation o, add_address a, org_original i
							where	parent_org_organisation_fk in
								(
									select	org_organisation_pk
									from	org_organisation
									where	org_number = r_cursor.org_number
								)
							and		org_status_fk =
								(
									select org_status_pk
									from org_status
									where lower(name) = 'member'
								)
							and		lower(i.name)			= lower(ltrim(rtrim(r_cursor2.name)))
							and		a.add_address_pk 		= o.add_address_fk
							and		o.org_organisation_pk	= i.org_organisation_fk_pk
							and		a.zip 					= r_cursor2.zip;

							if (v_count > 0) then
								v_count_noc := v_count_noc + 1;
								v_status := '<font color="gray">MEMBER - no change</font>';
							else
								v_count_upd := v_count_upd + 1;
								v_status := '<font color="blue">EXISTS - update</font>';
							end if;
						end if;
					end if;

				end if;

				-- -------------------------------------------------
				--
				-- insert nightclub
				--
				-- -------------------------------------------------
				if (p_action = 'import') then

					-- --------------------------
					-- if update select out PKs
					-- --------------------------
					if (lower(v_status) like '%update%') then

						select	org_organisation_pk, add_address_fk
						into	v_organisation_pk, v_address_pk
						from	org_organisation o, add_address a, org_original i
						where	parent_org_organisation_fk in
							(
								select	org_organisation_pk
								from	org_organisation
								where	org_number = r_cursor.org_number
							)
						and	(
								bool_umbrella_organisation is null or
								bool_umbrella_organisation = 0
						)
						and		lower(i.name) = lower(ltrim(rtrim(r_cursor2.name)))
						and		a.add_address_pk		= o.add_address_fk
						and		o.org_organisation_pk	= i.org_organisation_fk_pk
						and		a.zip					= r_cursor2.zip;

					-- ---------------------------------------------------
					-- if new insert create new entries in ORG and ADD
					-- ---------------------------------------------------
					elsif (lower(v_status) like '%insert%') then

						v_organisation_pk	:= sys_lib.getEmptyPk('org_organisation');
						v_address_pk		:= sys_lib.getEmptyPk('add_address');

						-- -------------------------
						-- insert new organisation
						-- -------------------------
						insert into org_organisation
						(org_organisation_pk, source, parent_org_organisation_fk, org_status_fk, date_status_change, date_insert)
						values (
							  v_organisation_pk
							, 'imp_i4:'||r_cursor2.source
							, (select min(org_organisation_pk) from org_organisation where org_number = r_cursor.org_number)
							, p_org_status_pk
							, sysdate
							, sysdate
						);

						insert into org_original
						(org_organisation_fk_pk, name)
						values (
							  v_organisation_pk
							, r_cursor2.name
						);

						-- -------------------------
						-- insert new address
						-- -------------------------
						insert into add_address
						(add_address_pk, date_insert)
						values (
							  v_address_pk
							, sysdate
						);

						-- ------------------------------------------
						-- connect the organisation and the address
						-- ------------------------------------------
						update	org_organisation
						set		bool_umbrella_organisation	= null
							  , add_address_fk				= v_address_pk
						where	org_organisation_pk			= v_organisation_pk;

						-- ------------------------------------------
						-- connect organisation on selected service
						-- ------------------------------------------
						insert into org_ser
						(org_organisation_fk_pk, ser_service_fk_pk)
						values(v_organisation_pk, p_service_pk);

					end if;

					-- -----------------------------------------------
					-- for both insert and update modify the entries
					-- -----------------------------------------------
					if (lower(v_status) like '%insert%' or lower(v_status) like '%update%' ) then

						update	org_organisation
						set		name 				= r_cursor2.name
							  , date_update 		= sysdate
						where	org_organisation_pk	= v_organisation_pk;

						update	add_address
						set		street		= r_cursor2.street
							  , zip			= r_cursor2.zip
							  , landline	= r_cursor2.landline
							  , fax			= r_cursor2.fax
							  , email		= r_cursor2.email
							  , url			= r_cursor2.url
							  , geo_geography_fk =
							  	(
									select	nvl(geo_geography_pk, null)
									from	geo_geography, geo_zip
									where	geo_geography_pk = geo_geography_fk_pk
									and		zip_pk = r_cursor2.zip
							  	)
							  , date_update	= sysdate
						where add_address_pk = v_address_pk;

						delete from org_organisation_rights
						where org_organisation_fk_pk = v_organisation_pk;

						if (r_cursor2.bool_has_beer > 0) then
							insert into org_organisation_rights
							(org_organisation_fk_pk, org_rights_fk_pk)
							values
							(
								  v_organisation_pk
								, (select org_rights_pk from org_rights where lower(name) = 'beer')
							);
						end if;

						if (r_cursor2.bool_has_wine > 0) then
							insert into org_organisation_rights
							(org_organisation_fk_pk, org_rights_fk_pk)
							values
							(
								  v_organisation_pk
								, (select org_rights_pk from org_rights where lower(name) = 'wine')
							);
						end if;

						if (r_cursor2.bool_has_spirits > 0) then
							insert into org_organisation_rights
							(org_organisation_fk_pk, org_rights_fk_pk)
							values
							(
								  v_organisation_pk
								, (select org_rights_pk from org_rights where lower(name) = 'spirits')
							);
						end if;

						commit;

						-- update the array containing all updated organisations
						v_i_arrayNc := v_i_arrayNc + 1;
						v_updated_nc(v_i_arrayNc) := r_cursor2.imp_nightclub_pk;

					end if;
				end if;

			if (p_status is null or v_status like '%'||p_status||'%') then
					v_i_nc	:= v_i_nc + 1;
					v_i_tot	:= v_i_tot + 1;
					v_html := v_html||'
						<tr>
							<td><i>'||v_i_nc||' ('||v_i_tot||')</i>:</td>
							<td>'||r_cursor2.org_number||'</td>
							<td>'||r_cursor2.name||'</td>
							<td>'||r_cursor2.council||'&nbsp;</td>
							<td>'||r_cursor2.zip||'</td>
							<td>'||v_status||'</td>
						</tr>
					';
				end if;
			end loop;

			ext_lob.add(v_clob, v_html);
			ext_lob.pri(v_clob);
			dbms_lob.trim(v_clob, 0);

		end loop;

		-- ------------------------------------------
		-- update date for import in IMP module
		-- ------------------------------------------
		for v_i_arrayNcOwn in 1 .. v_updated_ncOwn.count
		loop
			if (v_updated_ncOwn(v_i_arrayNcOwn) is not null) then
				update	imp_nightclub_owner
				set		date_import = sysdate
				where	org_number = v_updated_ncOwn(v_i_arrayNcOwn);
				commit;
			end if;
		end loop;

		for v_i_arrayNc in 1 .. v_updated_nc.count
		loop
			if (v_updated_nc(v_i_arrayNc) is not null) then
				update	imp_nightclub
				set		date_import = sysdate
				where	imp_nightclub_pk = v_updated_nc(v_i_arrayNc);
				commit;
			end if;
		end loop;

		v_html := v_html||'
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="6"><b>Summary:</b> '||v_i_ncOwn||' umbrella orgs, '||v_i_nc||' children orgs - a total of '||v_i_tot||' orgs parsed. <font color="green">INSERT: '||v_count_ins||'</font> - <font color="blue">UPDATE: '||v_count_upd||'</font> - <font color="gray">NO CHANGE: '||v_count_noc||'</font></td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="6" style="padding-left: 20px;">
		';

		if (p_action = 'import') then
			v_html := v_html||'<b>All data imported!</b>';
		elsif (p_action = 'importVerify') then
			v_html := v_html||''||htm.link('import data (might take several minutes)', g_package||'.'||c_procedure||'?p_action=import&amp;p_service_pk='||p_service_pk||'&amp;p_org_status_pk='||p_org_status_pk||'&amp;p_zip_start='||p_zip_start||'&amp;p_zip_stop='||p_zip_stop||'&amp;p_org_number='||p_org_number||'&amp;p_name_parent='||htm_lib.HTMLEncode(p_name_parent)||'&amp;p_name_child='||htm_lib.HTMLEncode(p_name_child)||'&amp;p_status=', '_self' ,'theButtonStyle')||'
					</td>
				</tr>
				<tr>
					<td colspan="6" style="padding-left: 20px;">'||htm.historyBack('go back');
		end if;

		v_html := v_html||'
				</td>
			</tr>
			<tr>
				<td colspan="6" style="padding-left: 20px;">'||htm.link('back to import overview', g_package||'.'||c_procedure||'?p_action=mainForm&amp;p_service_pk='||p_service_pk, '_self', 'theButtonStyle')||'</td>
			</tr>
		';

		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);

		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

	elsif (p_action='reset_last_import') then
		
		update	imp_nightclub_owner
		set		date_import = null
		where	date_import is not null;

		update	imp_nightclub
		set		date_import = null
		where	date_import is not null;
		
		commit;
		
		v_html := v_html||htm.jumpTo(g_package||'.'||c_procedure||'?p_action=mainForm&amp;p_service_pk='||p_service_pk, 1, 'resetting date_import...');
		
	else

		v_html := v_html||htm.jumpTo('i4_pub.overview', 1, 'no insert made - going back to i4admin...');

	end if;

	ext_lob.add(v_clob, v_html);
	ext_lob.pri(v_clob);
	dbms_lob.trim(v_clob, 0);

	return v_clob;

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		return null;
end;
end importOrg;


-- ******************************************** --
end;
/
show errors;
