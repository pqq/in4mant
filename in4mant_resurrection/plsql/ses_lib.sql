PROMPT *** ses_lib ***
set define off
set serveroutput on;
alter session set plsql_warnings='enable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package ses_lib is
/* ******************************************************
*	Package:     ses_lib
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	050723 | Frode Klevstul
*			- Package created
****************************************************** */
	procedure run;
	procedure ini
	(
		  p_package		in				mod_package.name%type
		, p_procedure	in				mod_procedure.name%type
		, p_parameters	in				ses_instance.parameters%type
	);
	procedure ini
	(
		  p_package		in				mod_package.name%type
		, p_procedure	in				mod_procedure.name%type
	);
	function getId
		return number;
	function getPk
	(
		p_id		in ses_session.id%type
	) return number;
	function getOrgPk
	(
		p_id		in ses_session.id%type
	) return number;
	function timeout
		return number;
	function getLang
	(
		p_id		in ses_session.id%type
	)	return tex_language.tex_language_pk%type;
	function getSer
	(
		p_id		in ses_session.id%type
	)	return ser_service.ser_service_pk%type;
	function getDebugMode
	(
		p_id		in ses_session.id%type
	)	return ses_session.debug_mode%type;

end;
/
show errors;





-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body ses_lib is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'ses_lib';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
---------------------------------------------------------
-- name:        generateId
-- what:        generates and returns an unique session id
-- author:      Frode Klevstul
-- start date:  23.07.2005
---------------------------------------------------------
function generateId
	return number
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'generateId';

	v_id			ses_session.id%type;
begin
	ses_lib.ini(g_package, c_procedure);

	-- finds the average of all session ids
	select decode(
		 avg(id+1)	-- select this...
	   , null		--    if null
	   , 1			--    then 1
	   , avg(id+1)	--    else average
	)
	into v_id
	from ses_session;

	-- generates an unique session id with maximum 15 "characters"
	v_id := substr(  (to_number(to_char(sysdate, 'yyyymmddhh24miss'))/v_id/180876), 1, 15);

	--sys_lib.debug(g_package, c_procedure, 'new session id generated: '||v_id);

	-- returns the session id
	return v_id;
end;
end generateId;


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  23.07.2005
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        ini
-- what:        initiate all web pages and asks user to
--				login if needed
--				this should not be used for single modules,
--				just full web pages
-- author:      Frode Klevstul
-- start date:  23.07.2005
---------------------------------------------------------
procedure ini
(
	  p_package		in				mod_package.name%type
	, p_procedure	in				mod_procedure.name%type
	, p_parameters	in				ses_instance.parameters%type
)
is
begin
declare
	c_procedure			constant mod_procedure.name%type 			:= 'ini';

	c_remote_host		constant ses_session.host%type 				:= owa_util.get_cgi_env('REMOTE_HOST');
	c_remote_addr		constant ses_session.ip%type   				:= owa_util.get_cgi_env('REMOTE_ADDR');
	c_language_pk		constant tex_language.tex_language_pk%type 	:= sys_lib.getParameter('language');

	e_stop				exception;
	e_return			exception;

	v_id				ses_session.id%type;
	v_ses_session_pk	ses_session.ses_session_pk%type;
	v_count_logged		number									default 0;
	v_count_no_fake		number									default 0;
	v_date_insert		ses_session.date_insert%type;
	v_session_problems	boolean									default false;
	v_display_error		boolean									default false;
	v_service_name		ser_service.name%type;
	v_service_pk		ser_service.ser_service_pk%type;
	v_server_name		varchar2(64);

	cursor	cur_ses_session
	(
		b_id	ses_session.id%type
	) is
	select	ses_session_pk
	from	ses_session
	where	id = b_id;

	cursor	cur_ses_session2
	(
		b_id	ses_session.id%type
	) is
	select	ses_session_pk
	from	ses_session
	where	id in (
		select	ses_session_id_new
		from	log_error
		where	ses_session_id = b_id
		and		message like '%fake_session_id%'
	);

	cursor	cur_ses_session3
	(
		b_ip	ses_session.ip%type
	) is
	select	date_insert
	from	ses_session
	where	ses_session_pk =
	(
		select max(ses_session_pk)
		from ses_session
		where ip = b_ip
	);

	-- -------------------------------------------------------
	-- selects out the date for the logging of the latest
	-- "fake_session_id".
	-- -------------------------------------------------------
	cursor	cur_log_error
	(
		b_id	ses_session.id%type
	) is
	select	date_insert
	from	log_error
	where	log_error_pk =
	(
		select	max(log_error_pk)
		from	log_error
		where	ses_session_id = b_id
		and		message like '%fake_session_id%'
	);

	-- find the service pk based on name
	cursor	cur_ser_service
	(
		b_name	ser_service.name%type
	) is
	select	ser_service_pk
	from	ser_service
	where	name = b_name;

begin
	ses_lib.ini(g_package, c_procedure);

	-- try to get an session id from the browser's in4mant cookie
	v_id := ses_lib.getId;

	-- ----------------------------------------------------
	-- if we found a session id in an in4mant cookie
	-- ----------------------------------------------------
	if (v_id is not null) then
		-- check if session id is valid
		-- if it exists in database we'll get it from this cursor
		for r_cursor in cur_ses_session(v_id)
		loop
			v_ses_session_pk := r_cursor.ses_session_pk;
		end loop;

		-- -------------------------------------------------------------------
		-- if the session ID is not valid AND
		-- the id is not generated from before...
		--
		-- since this procedure is called several times in one session / each
		-- reload of a page (several procedures call this) we have to do an
		-- additional check. When we set a cookie, this new cookie will not be
		-- valid until next reload. So we check if a fake_session_id error
		-- already exists for this session id (if it exists all this has been
		-- done before)
		-- -------------------------------------------------------------------

		-- -------------------------------------------------------------------
		-- if this session id is logged before we select the date for when
		-- it's logged (logged as an fake_id)
		-- -------------------------------------------------------------------
		for r_cursor in cur_log_error(v_id)
		loop
			v_date_insert := r_cursor.date_insert;
		end loop;

		-- ----------------------------------------------------------
		-- check if the last error is older than 3 seconds
		-- if it is the error was logged in another reload of
		-- the page. If that is the case the old error is not from
		-- this same reload of the page. We want the error to be
		-- logged, and the user to get a new id stored in the browser.
		-- ----------------------------------------------------------
		if (v_date_insert < (sysdate-(0.05/1440)) ) then
			v_count_logged		:= 0;																-- equal to '0' will case the error to be logged (we have not logged error from before - in same reload)
			v_display_error		:= false;															-- since we have logged this fake id before we do not show the error screen again, so this is "false"

		-- ----------------------------------------------------------------------
		-- if v_date_insert is null we haven't logged this fake id before
		-- we want to log it and print an error message if "max_fake_ses_id"
		-- is reached
		-- ----------------------------------------------------------------------
		elsif (v_date_insert is null) then
			v_count_logged		:= 0;
			v_display_error		:= true;

		-- ----------------------------------------------------------------------
		-- v_date_insert is less than 3 seconds old, that means we have logged
		-- this fake id before in the same reload of the page
		-- we don't want a new id generated or log this error
		-- ----------------------------------------------------------------------
		else
			v_count_logged 		:= 1;
		end if;

		if (v_ses_session_pk is null and v_count_logged = 0) then
			-- generate a new id
			v_id := generateId;

			-- log error (as a warning, that's why second last parameter is 1)
			--log_lib.logError(g_package, c_procedure, 'fake_session_id', 1, v_id);

			-- -----
			-- check if this IP has any old fake session IDs (logged previous before with other id)
			-- if it has too many we show an error page
			-- -----
			select count(*)
			into v_count_no_fake
			from log_error l, ses_session s
			where l.message like '%fake_session_id%'
			and l.ses_session_id_new = s.id
			and s.ip = c_remote_addr;

			-- have to subtract 1 to get it right (if param = 1, and we have 0 errors, but this new error makes it error 1...)
			if ( v_count_no_fake >= (to_number(sys_lib.getParameter('max_fake_ses_id'))-1) and v_display_error ) then
				pag_pub.errorPage(g_package, c_procedure, 'several_attempts_to_fake_a_session_id_has_been_logged', 1, v_id);
				raise e_stop;
			end if;

		end if;
	-- ----------------------------------------------------
	-- user don't have an in4mant session cookie but it might
	-- be because the browser doesn't support cookies
	-- we check if this is true here...
	-- ----------------------------------------------------
	else
		-- ----------------------------------------------------
		-- select out insert date for the last session that
		-- was generated from the same IP as the user has now
		-- ----------------------------------------------------
		for r_cursor in cur_ses_session3(c_remote_addr)
		loop
			v_date_insert := r_cursor.date_insert;
		end loop;

		if (v_date_insert is not null) then
			-- -----
			-- check if we have a session in the database that was just inserted (within one minute ago)
			-- since sysdate is in days we have to convert 1 minute into day.
			-- since there are 24h * 60min = 1440 minutes in a day, one minute is 1/1440 days
			--
			-- first check:
			-- if a cookie has been inserted in less than 3 seconds (1/20 = 0.05 minutes = 3 sec)
			-- ago we assume that it's done in this same instance, then we don't want to create or set
			-- a new cookie, so we jump out of this procedure
			-- -----
			if ( v_date_insert > (sysdate - 0.05/1440) ) then
				raise e_return;
			end if;
			-- -----
			-- second check:
			-- if the cookie has not been inserted in this session we have to check if the cookie
			-- has been set in an earlier session younger than half a minute old. If it has we (might)
			-- have problems setting a cookie in the browser.
			-- -----
			if ( v_date_insert >= (sysdate - 0.5/1440) ) then
				v_session_problems := true;
			end if;
		end if;
	end if;

	-- ----------------------------------------------------------------------
	-- if v_id is null it's the first time the user access an in4mant page
	-- so we generate a new session id
	-- ----------------------------------------------------------------------
	if (v_id is null) then
		v_id := generateId;
	end if;

	-- ----------------------------------------------------------------------
	-- at this stage we (should) have a valid session id
	-- if v_count_logged > 0 : fake_session_id has been logged before in same
	-- instance and we don't want to create a new session, so only do this
	-- if v_count_logged = 0
	-- ----------------------------------------------------------------------
	if (v_id is not null and v_count_logged = 0) then

		-- ----------------------------------------------------------------
		-- if v_ses_session_pk is not null that means it has been checked
		-- and is already in the database, only if this is null we have to
		-- insert a new session
		-- ----------------------------------------------------------------
		if (v_ses_session_pk is null) then

			-- -----------------------------------------
			--
			-- logic to find correct service pk
			--
			-- -----------------------------------------

			-- -----------------------------------------
			-- NOTE:
			-- These values are hardcoded and has to be
			-- changed if new domains are added etc.
			-- -----------------------------------------
			v_server_name := owa_util.get_cgi_env('SERVER_NAME');

			-- ------------------------------------------
			-- uteliv.no
			-- ------------------------------------------
			if (v_server_name like 'uteliv.no') then
				v_service_name := 'uteliv';
			-- ------------------------------------------
			-- bytur.no
			-- ------------------------------------------
			elsif (v_server_name like 'bytur.no') then
				v_service_name := 'bytur';
			-- ------------------------------------------
			-- default service
			-- ------------------------------------------
			else
				v_service_name := sys_lib.getParameter('service');
			end if;

			-- ----------------------
			-- get ser_service_pk
			-- ----------------------
			for r_cursor in cur_ser_service(v_service_name)
			loop
				v_service_pk := r_cursor.ser_service_pk;
			end loop;

			-- -------------------------------------
			--
			-- insert into session table
			--
			-- -------------------------------------
	        select 	seq_ses_session.nextval
	        into 	v_ses_session_pk
	        from 	dual;

			insert into ses_session
			(ses_session_pk, id, use_user_fk, tex_language_fk, ser_service_fk, host, ip, date_insert, date_update)
			values(
				  v_ses_session_pk
				, v_id
				, null
				, c_language_pk
				, v_service_pk
				, c_remote_host
				, c_remote_addr
				, sysdate
				, sysdate
			);
			commit;

			-- set cookie
			htm_lib.setCookie('in4mant_session', v_id);

			-- -----
			-- if we have any problems setting a cookie we'll deal with that now,
			-- since we've now tried setting a new cookie and don't want to continue
			-- further down in this procedure if it doesn't work
			-- -----
			if (v_session_problems) then
				pag_pub.errorPage(g_package, c_procedure, 'browser_might_not_support_cookie', 0, v_id);
				raise e_stop;
			end if;
		end if;
	-- ----------------------------------------------------------------------
	-- only check this
	-- if v_count_logged=0 : fake_session_id has not been logged before
	-- because if it has we'll come here and v_id will be null
	-- ----------------------------------------------------------------------
	elsif (v_id is null and v_count_logged = 0) then
		pag_pub.errorPage(g_package, c_procedure, 'unable_to_generate_session_id');
		return;
	-- ----------------------------------------------------------------------
	-- else (v_count_logged > 0) : fake_session_id has been logged before, so
	-- we do have to find the ses_session_pk for the new session that has
	-- been created earlier (so this can be inserted into ses_instance)
	-- ----------------------------------------------------------------------
	elsif (v_count_logged > 0) then
		for r_cursor in cur_ses_session2(v_id)
		loop
			v_ses_session_pk := r_cursor.ses_session_pk;
		end loop;
	end if;

	-- ----------------------------------------------------------------------
	-- user restriction:
	-- check if user has access to the module
	-- ----------------------------------------------------------------------
	if not (mod_lib.hasAccess(p_package, p_procedure)) then
		-- show error page
		pag_pub.errorPage(g_package, c_procedure, 'no_access_to_module:'||p_package||'.'||p_procedure);

		-- log activity to get module registered in database
		ses_lib.ini(p_package, p_procedure);
		raise e_stop;
	end if;

	-- -------------------------------------------------------------------
	-- create a new instance, if "ses_instance = 1"
	-- -------------------------------------------------------------------
	if (sys_lib.getParameter('ses_instance') = '1') then
		insert into ses_instance
		(ses_instance_pk, ses_session_fk, mod_module_name, parameters, date_insert)
		values
		(
			  seq_ses_instance.nextval
			, v_ses_session_pk
			, p_package||'.'||p_procedure
			, p_parameters
			, sysdate
		);
		commit;
	end if;

	-- -------------------------------------------------------------------
	-- update last action time (date_update) in "ses_session"
	-- this is only done if we don't have a timeout
	-- -------------------------------------------------------------------
	if (ses_lib.timeout > 0) then
		update	ses_session
		set		date_update = sysdate
		where	ses_session_pk = v_ses_session_pk;
	end if;

	-- log activity (has to be done to check mod_module and log in log_mod)
	ses_lib.ini(p_package, p_procedure);

exception
	when e_stop then
		raise e_stop; -- re-raise exception
	when e_return then
		null;
end;
end ini;


---------------------------------------------------------
-- name:        ini
-- what:        logs activity of modules (log_mod)
--              and make sure module exists (in "mod" tables)
--              (this doesn't check the session id)
-- author:      Frode Klevstul
-- start date:  23.07.2005
---------------------------------------------------------
procedure ini
(
	  p_package		in				mod_package.name%type
	, p_procedure	in				mod_procedure.name%type
)
is
begin
declare
	c_procedure			constant mod_procedure.name%type := 'ini';

	v_sequence1			number;
	v_sequence2			number;
	v_sequence3			number;
	v_count				number;
	v_name				mod_module.name%type := p_package||'.'||p_procedure;
begin
	-- -------------------------------------------------------------------------------------------------------
	-- !!! IMPORTANT !!!
	-- -------------------------------------------------------------------------------------------------------
	-- DO NOT CALL ANY EXTERNAL PROCEDURES HERE!!!
	-- DO NOT CALL ANY EXTERNAL PROCEDURES HERE!!!
	-- DO NOT CALL ANY EXTERNAL PROCEDURES HERE!!!
	--
	-- that will cause an infinite loop...
	-- -------------------------------------------------------------------------------------------------------

	-- ----------------------------------------------------------------------------------
	-- DEBUG
	-- note: we can't use "sys_lib.getParameter()" here, since that would have caused
	--       an infinite loop, so we do it manually...
	-- ----------------------------------------------------------------------------------
	select	count(*)
	into	v_count
	from	sys_parameter
	where	name in ('debug', 'debug-mode')
	and		value in ('1', 'web', 'all');

	if (v_count = 2) then
		htp.p('<font color="blue"><b>- - - - - - - - - -'||p_package||'.'||p_procedure||'</b></font><br>');
	end if;

	-- -----
	-- checks if the module exist
	-- -----
	select	count(*)
	into	v_count
	from	mod_module
	where	name = v_name;

	-- -----
	-- if the module isn't in mod_module
	-- -----
	if (v_count = 0) then

		-- -----
		-- we know for sure that the procedure doesn't exist
		-- so we insert it into mod_procedure
		-- -----
        select 	seq_mod_procedure.nextval
        into 	v_sequence1
        from 	dual;

		insert into mod_procedure
		(mod_procedure_pk, name, description, date_insert)
		values
		(
			  v_sequence1
			, p_procedure
			, 'inserted by: '||g_package||'.'||c_procedure
			, sysdate
		);
		commit;

		-- -----
		-- we have to check if the package exists
		-- -----
		select	count(*)
		into	v_sequence2
		from	mod_package
		where	name = p_package;

		-- -----
		-- if the package doesn't exist we insert that as well
		-- -----
		if (v_sequence2 = 0) then

	        select 	seq_mod_package.nextval
	        into 	v_sequence2
	        from 	dual;

			insert into mod_package
			(mod_package_pk, name, description, date_insert)
			values
			(
				  v_sequence2
				, p_package
				, 'inserted by: '||g_package||'.'||c_procedure
				, sysdate
			);
			commit;
		-- -----
		-- else: package exist, so get the pk
		-- -----
		else
			select	mod_package_pk
			into	v_sequence2
			from	mod_package
			where	name = p_package;
		end if;

		-- -----
		-- now we can insert the new module
		-- -----
        select 	seq_mod_module.nextval
        into 	v_sequence3
        from 	dual;

		insert into mod_module
		(mod_module_pk, mod_package_fk, mod_procedure_fk, name)
		values
		(
			  v_sequence3
			, v_sequence2
			, v_sequence1
			, v_name
		);
		commit;
	end if;

	-- -----
	-- if module didn't exist v_count is 0, if v_count is 0 we'll insert a new
	-- entry into log_mod. we always want to insert a new entry into log_mod
	-- if system parameter 'log_mod-mode' equals 'insert' and 'log_mod' is '1'
	-- 'log_mod-mode' = 'insert' : new insert each time
	-- 'log_mod-mode' = 'update' : update date_update only
	-- -----
	if (v_count > 0) then										-- if module exist
		select	count(*)
		into	v_count
		from	sys_parameter
		where	name in ('log_mod', 'log_mod-mode')
		and		value in ('1', 'insert');

		if (v_count = 2) then
			v_count := 0;										-- v_count = 0 means insert (look further down)
		else
			v_count := 1;										-- v_count was bigger than 0 when we came into this "if-statement" so we have to reset it in case the count resultet in 0.
		end if;
	end if;

	-- -----
	-- log activity in log_mod if module exist (always once when module doesn't exist,
	-- or if log_mod-mode is "insert")
	-- -----
	if (v_count = 0) then
		insert into log_mod
		(log_mod_pk, mod_module_name, date_insert, date_update)
		values
		(
			  seq_log_mod.nextval
			, v_name
			, sysdate
			, sysdate
		);
		commit;
	-- -----
	-- update log_mod (only if v_count <> 0, that is if system paramter "log_mod" = "1")
	-- -----
	else
		-- ----------------------------------------------------------------------------------
		-- note: we can't use "sys_lib.getParameter()" here, since that would have caused
		--       an infinite loop, so we do check sys_parameter manually...
		-- ----------------------------------------------------------------------------------
		select	count(*)
		into	v_count
		from	sys_parameter
		where	name = 'log_mod'
		and		value = '1';

		if (v_count > 0) then
			update 		log_mod
				set 	date_update = sysdate
				where 	mod_module_name = v_name;
			if (sql%notfound) then
				insert into log_mod
				       (log_mod_pk, mod_module_name, date_insert, date_update)
				values
				(
					  seq_log_mod.nextval
					, v_name
					, sysdate
					, sysdate
				);
			end if;
			commit;

		end if;

	end if;
end;
end ini;


---------------------------------------------------------
-- name:        getId
-- what:        returns the session id
-- author:      Frode Klevstul
-- start date:  24.07.2005
---------------------------------------------------------
function getId
	return number
is
begin
declare
	c_procedure			constant mod_procedure.name%type := 'getId';

begin
	ses_lib.ini(g_package, c_procedure);

    return htm_lib.getCookie('in4mant_session');
end;
end getId;


---------------------------------------------------------
-- name:        getPk
-- what:        returns the user pk related to the session ID
-- author:      Frode Klevstul
-- start date:  14.08.2005
---------------------------------------------------------
function getPk
(
	p_id		in ses_session.id%type
)	return number
is
begin
declare
	c_procedure			constant mod_procedure.name%type := 'getPk';

	cursor	cur_ses_session is
	select	use_user_fk
	from	ses_session
	where	id = p_id;

	v_use_user_pk		use_user.use_user_pk%type;
begin
	ses_lib.ini(g_package, c_procedure);

	for r_cursor in cur_ses_session
	loop
	    v_use_user_pk := r_cursor.use_user_fk;
	end loop;

	if (ses_lib.timeout<0) then
		return null;
	else
	    return v_use_user_pk;
	end if;
end;
end getPk;


---------------------------------------------------------
-- name:        getOrgPk
-- what:        returns the organisation pk related to the session ID
-- author:      Frode Klevstul
-- start date:  13.12.2005
---------------------------------------------------------
function getOrgPk
(
	p_id		in ses_session.id%type
)	return number
is
begin
declare
	c_procedure			constant mod_procedure.name%type := 'getOrgPk';

	cursor	cur_ses_session is
	select	org_organisation_fk
	from	ses_session
	where	id = p_id;

	v_org_organisation_pk		org_organisation.org_organisation_pk%type;
begin
	ses_lib.ini(g_package, c_procedure);

	for r_cursor in cur_ses_session
	loop
	    v_org_organisation_pk := r_cursor.org_organisation_fk;
	end loop;

	if (ses_lib.timeout<0) then
		return null;
	else
	    return v_org_organisation_pk;
	end if;
end;
end getOrgPk;


---------------------------------------------------------
-- name:        timeout
-- what:        returns number of minutes till / since timeout
-- author:      Frode Klevstul
-- start date:  11.08.2005
---------------------------------------------------------
function timeout
	return number
is
begin
declare
	c_procedure			constant mod_procedure.name%type	:= 'timeout';
	c_id				constant ses_session.id%type 		:= ses_lib.getId;

	v_return			number								default 0;
	v_last_action		ses_session.date_insert%type;
	v_idle_time			use_type.idle_time%type;
    v_rounded_value		number (20, 0)						default NULL;							-- number with no decimals
	v_timeout			date;

begin
	ses_lib.ini(g_package, c_procedure);

	-- ------------------------------------------------
	-- if c_id is NULL the user is not logged in,
	-- (actually cookies has to be disabled to not have
	-- a session in the browser, cause we always try to
	-- set a cookie with session information)
	-- then we return 0
	-- ------------------------------------------------
	if (c_id is null) then
		return 0;
	end if;

	-- ------------------------------------------------
	-- find idle time and the time of the user's last action
	-- ------------------------------------------------
	select	idle_time, date_update
	into	v_idle_time, v_last_action
	from	ses_session
	where	id = c_id;

	if (v_idle_time is null) then
		return 0;
	end if;

	-- ------------------------------------------------
	-- calculate the time when the user got / will get timeout
	-- ------------------------------------------------
	v_timeout := ( v_last_action + (v_idle_time/1440) );

	-- ------------------------------------------------
	-- calculate if we have a timeout or not, if this number is
	-- bigger than 0 we do not have timeout yet
	-- ------------------------------------------------
    v_rounded_value := to_number(to_char(v_timeout,'YYYYMMDDHH24MISS')) - to_number(to_char(sysdate,'YYYYMMDDHH24MISS'));
    v_return		:= v_rounded_value;

	-- ------------------------------------------------
	-- if we don't have timeout we update the time of last action
	-- ------------------------------------------------
    if ( v_return > 0 ) then
		update	ses_session
		set		date_update = sysdate
		where 	id = c_id;
		commit;

    	return v_return;
	-- ------------------------------------------------
	-- if we have timeout we return the number less than 0
	-- ------------------------------------------------
	else
	    return v_return;
	end if;

exception
	when others then
		return 0;

end;
end timeout;


---------------------------------------------------------
-- name:        getLang
-- what:        returns language from session
-- author:      Frode Klevstul
-- start date:  02.12.2005
---------------------------------------------------------
function getLang
(
	p_id		in ses_session.id%type
)	return tex_language.tex_language_pk%type
is
begin
declare
	c_procedure			constant mod_procedure.name%type := 'getLang';

	cursor	cur_ses_session is
	select	tex_language_fk
	from	ses_session
	where	id = p_id;

	v_tex_language_pk		tex_language.tex_language_pk%type;
begin
	ses_lib.ini(g_package, c_procedure);

	for r_cursor in cur_ses_session
	loop
	    v_tex_language_pk := r_cursor.tex_language_fk;
	end loop;

    return v_tex_language_pk;

end;
end getLang;


---------------------------------------------------------
-- name:        getSer
-- what:        returns ser_service_fk/pk from session
-- author:      Frode Klevstul
-- start date:  09.01.2006
---------------------------------------------------------
function getSer
(
	p_id		in ses_session.id%type
)	return ser_service.ser_service_pk%type
is
begin
declare
	c_procedure			constant mod_procedure.name%type := 'getSer';

	cursor	cur_ses_session is
	select	ser_service_fk
	from	ses_session
	where	id = p_id;

	v_service_pk			ser_service.ser_service_pk%type;
begin
	ses_lib.ini(g_package, c_procedure);

	for r_cursor in cur_ses_session
	loop
	    v_service_pk := r_cursor.ser_service_fk;
	end loop;

    return v_service_pk;

end;
end getSer;


---------------------------------------------------------
-- name:        getDebugMode
-- what:        returns debug mode from session
--				NOTE: this is not the same as "debug-mode"
--				in sys_parameter! This work for one user
--				only, sys_parameter will change for everyone.
-- author:      Frode Klevstul
-- start date:  16.03.2006
-- description:
--				DEBUG MODES
--				1: TEX show all PKs in stead of the string
---------------------------------------------------------
function getDebugMode
(
	p_id		in ses_session.id%type
)	return ses_session.debug_mode%type
is
begin
declare
	c_procedure			constant mod_procedure.name%type := 'getDebugMode';

	cursor	cur_ses_session is
	select	debug_mode
	from	ses_session
	where	id = p_id;

	v_debug_mode			ses_session.debug_mode%type;
begin
	ses_lib.ini(g_package, c_procedure);

	for r_cursor in cur_ses_session
	loop
	    v_debug_mode := r_cursor.debug_mode;
	end loop;

    return v_debug_mode;

end;
end getDebugMode;


-- ******************************************** --
end;
/
show errors;
