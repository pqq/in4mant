PROMPT *** org_i4 ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package org_i4 is
/* ******************************************************
*	Package:     tex_lib
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	051222 | Frode Klevstul
*			- Package created
****************************************************** */
	type parameter_arr 			is table of varchar2(4000) index by binary_integer;
	empty_array 				parameter_arr;

	procedure run;
	procedure addOrg
	(
		  p_action						varchar2											default null
		, p_bool_umbrella_organisation	org_organisation.bool_umbrella_organisation%type	default null
		, p_org_status_pk				org_status.org_status_pk%type						default null
		, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
		, p_org_number					org_organisation.org_number%type					default null
		, p_name						org_organisation.name%type							default null
		, p_service_pk					ser_service.ser_service_pk%type						default null
		, p_source						org_organisation.source%type						default null
		, p_email						add_address.email%type								default null
		, p_url							add_address.url%type								default null
		, p_street						add_address.street%type								default null
		, p_zip							add_address.zip%type								default null
		, p_geo_main					geo_geography.geo_geography_pk%type					default null
		, p_geo_sub						geo_geography.geo_geography_pk%type					default null
		, p_landline					add_address.landline%type							default null
		, p_mobile						add_address.mobile%type								default null
		, p_fax							add_address.fax%type								default null
		, p_bool						org_organisation.bool_event_based%type				default null
		, p_add_name					add_address.name%type								default null
	);
	function addOrg
	(
		  p_action						varchar2											default null
		, p_bool_umbrella_organisation	org_organisation.bool_umbrella_organisation%type	default null
		, p_org_status_pk				org_status.org_status_pk%type						default null
		, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
		, p_org_number					org_organisation.org_number%type					default null
		, p_name						org_organisation.name%type							default null
		, p_service_pk					ser_service.ser_service_pk%type						default null
		, p_source						org_organisation.source%type						default null
		, p_email						add_address.email%type								default null
		, p_url							add_address.url%type								default null
		, p_street						add_address.street%type								default null
		, p_zip							add_address.zip%type								default null
		, p_geo_main					geo_geography.geo_geography_pk%type					default null
		, p_geo_sub						geo_geography.geo_geography_pk%type					default null
		, p_landline					add_address.landline%type							default null
		, p_mobile						add_address.mobile%type								default null
		, p_fax							add_address.fax%type								default null
		, p_bool						org_organisation.bool_event_based%type				default null
		, p_add_name					add_address.name%type								default null
	) return clob;
	procedure updateOrgStatus
	(
		  p_action						varchar2											default null
		, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
		, p_org_status_pk				org_status.org_status_pk%type						default null
	);
	function updateOrgStatus
	(
		  p_action						varchar2											default null
		, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
		, p_org_status_pk				org_status.org_status_pk%type						default null
	) return clob;
	procedure loginAsOrg
	(
		  p_action						varchar2											default null
		, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
	);
	function loginAsOrg
	(
		  p_action						varchar2											default null
		, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
	) return clob;
	procedure updateOrgService
	(
		  p_action						varchar2											default null
		, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
		, p_service_pk					ser_service.ser_service_pk%type						default null
	);
	function updateOrgService
	(
		  p_action						varchar2											default null
		, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
		, p_service_pk					ser_service.ser_service_pk%type						default null
	) return clob;
	procedure admChosenOrg
	(
		  p_action						varchar2											default null
		, p_date_start_pk				varchar2											default null
		, p_service_pk					ser_service.ser_service_pk%type						default null
		, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
		, p_date_stop					varchar2											default null
	);
	function admChosenOrg
	(
		  p_action						varchar2											default null
		, p_date_start_pk				varchar2											default null
		, p_service_pk					ser_service.ser_service_pk%type						default null
		, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
		, p_date_stop					varchar2											default null
	) return clob;
	procedure deleteOrg
	(
		  p_action						varchar2											default null
		, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
	);
	function deleteOrg
	(
		  p_action						varchar2											default null
		, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
	) return clob;
	procedure updateImplock
	(
		  p_action						varchar2											default null
		, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
		, p_bool						org_organisation.bool_implock%type					default null
	);
	function updateImplock
	(
		  p_action						varchar2											default null
		, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
		, p_bool						org_organisation.bool_implock%type					default null
	) return clob;
	procedure updateOrg
	(
		  p_action						varchar2											default null
		, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
	);
	function updateOrg
	(
		  p_action						varchar2											default null
		, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
	) return clob;
	procedure fixOrgNames;
	function fixOrgNames
		return clob;
	procedure totalOrgAdm
	(
		  p_action						varchar2											default null
		, p_name						org_organisation.name%type							default null
		, p_firstCharacters				org_organisation.name%type							default null
		, p_zip_start					add_address.zip%type								default null
		, p_zip_stop					add_address.zip%type								default null
		, p_showLocked					number												default null
		, p_showBanned					number												default null
		, p_showPOrgNo					number												default null
		, p_sort						number												default null
		, p_organisation_pk				parameter_arr										default empty_array
		, p_address_pk					parameter_arr										default empty_array
		, p_org_name					parameter_arr										default empty_array
		, p_street						parameter_arr										default empty_array
		, p_zip							parameter_arr										default empty_array
		, p_bool						parameter_arr										default empty_array
		, p_org_status_pk				parameter_arr										default empty_array
		, p_delete						parameter_arr										default empty_array
	);
	function totalOrgAdm
	(
		  p_action						varchar2											default null
		, p_name						org_organisation.name%type							default null
		, p_firstCharacters				org_organisation.name%type							default null
		, p_zip_start					add_address.zip%type								default null
		, p_zip_stop					add_address.zip%type								default null
		, p_showLocked					number												default null
		, p_showBanned					number												default null
		, p_showPOrgNo					number												default null
		, p_sort						number												default null
		, p_organisation_pk				parameter_arr										default empty_array
		, p_address_pk					parameter_arr										default empty_array
		, p_org_name					parameter_arr										default empty_array
		, p_street						parameter_arr										default empty_array
		, p_zip							parameter_arr										default empty_array
		, p_bool						parameter_arr										default empty_array
		, p_org_status_pk				parameter_arr										default empty_array
		, p_delete						parameter_arr										default empty_array
	) return clob;
	function deleteOrganisation
	(
		p_organisation_pk				org_organisation.org_organisation_pk%type			default null
	) return boolean;

end;
/
show errors;





-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body org_i4 is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'org_i4';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  22.12.2005
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        addOrg
-- what:        add a new organisation
-- author:      Frode Klevstul
-- start date:  22.12.2005
---------------------------------------------------------
procedure addOrg
(
	  p_action						varchar2											default null
	, p_bool_umbrella_organisation	org_organisation.bool_umbrella_organisation%type	default null
	, p_org_status_pk				org_status.org_status_pk%type						default null
	, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
	, p_org_number					org_organisation.org_number%type					default null
	, p_name						org_organisation.name%type							default null
	, p_service_pk					ser_service.ser_service_pk%type						default null
	, p_source						org_organisation.source%type						default null
	, p_email						add_address.email%type								default null
	, p_url							add_address.url%type								default null
	, p_street						add_address.street%type								default null
	, p_zip							add_address.zip%type								default null
	, p_geo_main					geo_geography.geo_geography_pk%type					default null
	, p_geo_sub						geo_geography.geo_geography_pk%type					default null
	, p_landline					add_address.landline%type							default null
	, p_mobile						add_address.mobile%type								default null
	, p_fax							add_address.fax%type								default null
	, p_bool						org_organisation.bool_event_based%type				default null
	, p_add_name					add_address.name%type								default null
) is begin
    ext_lob.pri(org_i4.addOrg(p_action, p_bool_umbrella_organisation, p_org_status_pk, p_organisation_pk, p_org_number, p_name, p_service_pk, p_source, p_email, p_url, p_street, p_zip, p_geo_main, p_geo_sub, p_landline, p_mobile, p_fax, p_bool, p_add_name));
end;

function addOrg
(
	  p_action						varchar2											default null
	, p_bool_umbrella_organisation	org_organisation.bool_umbrella_organisation%type	default null
	, p_org_status_pk				org_status.org_status_pk%type						default null
	, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
	, p_org_number					org_organisation.org_number%type					default null
	, p_name						org_organisation.name%type							default null
	, p_service_pk					ser_service.ser_service_pk%type						default null
	, p_source						org_organisation.source%type						default null
	, p_email						add_address.email%type								default null
	, p_url							add_address.url%type								default null
	, p_street						add_address.street%type								default null
	, p_zip							add_address.zip%type								default null
	, p_geo_main					geo_geography.geo_geography_pk%type					default null
	, p_geo_sub						geo_geography.geo_geography_pk%type					default null
	, p_landline					add_address.landline%type							default null
	, p_mobile						add_address.mobile%type								default null
	, p_fax							add_address.fax%type								default null
	, p_bool						org_organisation.bool_event_based%type				default null
	, p_add_name					add_address.name%type								default null
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type 		:= 'addOrg';

	v_organisation_pk			org_organisation.org_organisation_pk%type;
	v_address_pk				add_address.add_address_pk%type;
	v_geography_pk				geo_geography.geo_geography_pk%type;

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_action='||p_action||'いp_name='||p_name||'いp_organisation_pk='||p_organisation_pk||'いp_bool_umbrella_organisation='||p_bool_umbrella_organisation);
	ext_lob.ini(v_clob);

	use_lib.loggedInCheck(g_package, c_procedure, null);

	if (p_action is null) then
		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'i4admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundAdminBox')||chr(13);
		v_html := v_html||'<tr><td>'||chr(13);

		v_html := v_html||''||htm.bForm(g_package||'.'||c_procedure, 'advancedSearch')||chr(13);			-- note: form name has to be advacedSearch so region JS code will work
		v_html := v_html||'<input type="hidden" name="p_action" value="insert">'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_bool_umbrella_organisation" value="'||p_bool_umbrella_organisation||'">'||chr(13);

		-- create the javascript code for regions in Norway
		v_html := v_html||''||htm.jsFindRegion('Norway')||chr(13);

		if (p_bool_umbrella_organisation = 1) then
			v_html := v_html||''||htm.bBox('Create parent organisation')||chr(13);
			v_html := v_html||''||htm.bTable||chr(13);

			v_html := v_html||'<tr><td colspan="2"><b>Parent info:</b></td></tr>'||chr(13);
			v_html := v_html||'<tr><td>Organisation number:</td><td><input type="text" name="p_org_number" maxlength="16" size="20"></td></tr>'||chr(13);
		else
			v_html := v_html||''||htm.bBox('Create organisation')||chr(13);
			v_html := v_html||''||htm.bTable||chr(13);

			v_html := v_html||'<tr><td colspan="2"><b>Child info:</b></td></tr>'||chr(13);
			v_html := v_html||'<tr><td>Status:</td><td>'||htm_lib.selectOrgStatus||'</td></tr>'||chr(13);
			v_html := v_html||'<tr><td>Parent:</td><td>'||chr(13);
			v_html := v_html||htm_lib.selectOrganisation('umbrella', null, p_service_pk)||chr(13);
			v_html := v_html||'</td></tr>'||chr(13);
			v_html := v_html||'<tr><td>Event based:</td><td>'||htm_lib.selectBool(0)||'</td></tr>'||chr(13);
		end if;

		v_html := v_html||'<tr><td colspan="2">&nbsp;</td></tr>'||chr(13);
		v_html := v_html||'<tr><td colspan="2"><b>General info:</b></td></tr>'||chr(13);
		v_html := v_html||'<tr><td>Name:</td><td><input type="text" name="p_name" maxlength="64" size="50"></td></tr>'||chr(13);
		v_html := v_html||'<tr><td>Service:</td><td>'||htm_lib.selectService||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td>Source:</td><td><input type="text" name="p_source" maxlength="18" size="15"></td></tr>'||chr(13);
		v_html := v_html||'<tr><td>Email:</td><td><input type="text" name="p_email" maxlength="64" size="50"></td></tr>'||chr(13);
		v_html := v_html||'<tr><td>URL:</td><td><input type="text" name="p_url" maxlength="128" size="50"></td></tr>'||chr(13);
		v_html := v_html||'<tr><td>Street:</td><td><input type="text" name="p_street" maxlength="512" size="50"></td></tr>'||chr(13);
		v_html := v_html||'<tr><td>Zip:</td><td><input type="text" name="p_zip" maxlength="16" size="10"></td></tr>'||chr(13);
		v_html := v_html||'<tr>
								<td>
									Region:
								</td>
								<td>
									<select id="opt_id_type" name="p_geo_main" onChange="showMunicipal();"></select>
									<select id="opt_id_style" name="p_geo_sub"></select>
									<script type="text/javascript">start()</script>
								</td>
							</tr>'||chr(13);
		v_html := v_html||'<tr><td>Landline:</td><td><input type="text" name="p_landline" maxlength="32" size="10"></td></tr>'||chr(13);
		v_html := v_html||'<tr><td>Mobile:</td><td><input type="text" name="p_mobile" maxlength="32" size="10"></td></tr>'||chr(13);
		v_html := v_html||'<tr><td>Fax:</td><td><input type="text" name="p_fax" maxlength="32" size="10"></td></tr>'||chr(13);

		v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||htm.submitLink('insert', 'advancedSearch')||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||htm.historyBack('go back')||'</td></tr>'||chr(13);

		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);

		v_html := v_html||''||htm.eForm||chr(13);

		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);
	elsif (p_action = 'insert' and p_name is not null) then

		v_organisation_pk	:= sys_lib.getEmptyPk('org_organisation');
		v_address_pk		:= sys_lib.getEmptyPk('add_address');

		-- -----------------------
		-- Organisation
		-- -----------------------
		insert into org_organisation
		(org_organisation_pk, name, org_number, source, org_status_fk, date_status_change, date_insert, date_update)
		values (
			  v_organisation_pk
			, p_name
			, p_org_number
			, 'org_i4.addOrg:'||p_source
			, p_org_status_pk
			, sysdate
			, sysdate
			, sysdate
		);

		insert into org_original
		(org_organisation_fk_pk, name, org_number)
		values (
			  v_organisation_pk
			, p_name
			, p_org_number
		);

		-- -----------------------
		-- Address
		-- -----------------------
		if (p_geo_sub is not null) then
			v_geography_pk := p_geo_sub;
		else
			v_geography_pk := p_geo_main;
		end if;

		insert into add_address
		(add_address_pk, street, zip, landline, mobile, fax, email, url, geo_geography_fk, date_insert, date_update)
		values (
			  v_address_pk
			, p_street
			, p_zip
			, p_landline
			, p_mobile
			, p_fax
			, p_email
			, p_url
			, v_geography_pk
			, sysdate
			, sysdate
		);

		-- --------------------------------
		-- parent organisation
		-- --------------------------------
		if (p_bool_umbrella_organisation = 1) then
			update	org_organisation
			set		bool_umbrella_organisation		= p_bool_umbrella_organisation
					, add_address_fk				= v_address_pk
					, org_status_fk					= null
			where	org_organisation_pk				= v_organisation_pk;
		-- --------------------------------
		-- child organisation
		-- --------------------------------
		else
			update	org_organisation
			set		add_address_fk					= v_address_pk
					, parent_org_organisation_fk	= p_organisation_pk
					, bool_event_based				= p_bool
			where	org_organisation_pk				= v_organisation_pk;
		end if;

		-- -----------------------
		-- Service
		-- -----------------------
		insert into org_ser (org_organisation_fk_pk, ser_service_fk_pk)
		values (v_organisation_pk, p_service_pk);

		commit;

		if (p_bool_umbrella_organisation = 1) then
			v_html := v_html||htm.jumpTo('org_i4.addOrg', 1, 'inserting...');
		else
			-- -----------------------------------------------
			-- we login as the org and jump to admin page
			-- this is done to initiate all org values
			-- -----------------------------------------------
			org_i4.loginAsOrg('update', v_organisation_pk);
		end if;
	else

		v_html := v_html||htm.jumpTo('i4_pub.overview', 1, 'no insert made - going back to i4admin...');

	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
			return null;
end;
end addOrg;


---------------------------------------------------------
-- name:        updateOrgStatus
-- what:        update an organisation's status
-- author:      Frode Klevstul
-- start date:  28.12.2005
---------------------------------------------------------
procedure updateOrgStatus(p_action varchar2 default null, p_organisation_pk org_organisation.org_organisation_pk%type default null, p_org_status_pk org_status.org_status_pk%type default null) is begin
    ext_lob.pri(org_i4.updateOrgStatus(p_action, p_organisation_pk, p_org_status_pk));
end;

function updateOrgStatus
(
	  p_action						varchar2											default null
	, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
	, p_org_status_pk				org_status.org_status_pk%type						default null
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type 		:= 'updateOrgStatus';

	cursor	cur_org is
	select	org_status_fk
	from	org_organisation
	where	org_organisation_pk = p_organisation_pk;

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_action='||p_action||'いp_organisation_pk='||p_organisation_pk||'いp_org_status_pk='||p_org_status_pk);
	ext_lob.ini(v_clob);

	use_lib.loggedInCheck(g_package, c_procedure, null);

	if (p_action is null) then

		v_html := v_html||''||org_lib.organisationPicker('i4_pub.i4admin', g_package||'.'||c_procedure||'?p_action=updateForm', 'non_umbrella')||chr(13);

	elsif (p_action = 'updateForm') then

		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'i4admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundAdminBox')||chr(13);
		v_html := v_html||'<tr><td>'||chr(13);

		v_html := v_html||''||htm.bForm(g_package||'.'||c_procedure)||chr(13);
		v_html := v_html||'<input type="hidden" name="p_action" value="update">'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_organisation_pk" value="'||p_organisation_pk||'">'||chr(13);
		v_html := v_html||''||htm.bBox('Set status for '||org_lib.getOrgName(p_organisation_pk))||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);

		for r_cursor in cur_org
		loop
			v_html := v_html||'<tr><td>Status:</td><td>'||htm_lib.selectOrgStatus(r_cursor.org_status_fk)||'</td></tr>'||chr(13);
		end loop;

		v_html := v_html||'<tr><td colspan="2">&nbsp;</td></tr>'||chr(13);
		v_html := v_html||'<tr><td style="padding-left: 20px;">'||htm.submitLink('update')||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td style="padding-left: 20px;">'||htm.historyBack('go back')||'</td></tr>'||chr(13);

		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);
		v_html := v_html||''||htm.eForm||chr(13);

		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

	elsif (p_action = 'update') then

		if (p_organisation_pk is not null) then
			update	org_organisation
			set		org_status_fk 		= p_org_status_pk
				  , date_status_change	= sysdate
			where	org_organisation_pk = p_organisation_pk;
			commit;
		end if;

		v_html := v_html||htm.jumpTo('i4_pub.overview', 1, 'updating...');

	else

		v_html := v_html||htm.jumpTo('i4_pub.overview', 1, 'no update made - going back to i4admin...');

	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
    when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
        return null;
end;
end updateOrgStatus;


---------------------------------------------------------
-- name:        loginAsOrg
-- what:        login as an organisation
-- author:      Frode Klevstul
-- start date:  28.12.2005
---------------------------------------------------------
procedure loginAsOrg(p_action varchar2 default null, p_organisation_pk org_organisation.org_organisation_pk%type default null) is begin
    ext_lob.pri(org_i4.loginAsOrg(p_action, p_organisation_pk));
end;

function loginAsOrg
(
	  p_action						varchar2											default null
	, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
) return clob
is
begin
declare
	c_procedure			constant mod_procedure.name%type 					:= 'loginAsOrg';

	c_id				constant ses_session.id%type 						:= ses_lib.getId;

	cursor	cur_org is
	select	org_status_fk
	from	org_organisation
	where	org_organisation_pk = p_organisation_pk;

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_action='||p_action||'いp_organisation_pk='||p_organisation_pk);
	ext_lob.ini(v_clob);

	use_lib.loggedInCheck(g_package, c_procedure, null);

	if (p_action is null) then

		v_html := v_html||''||org_lib.organisationPicker('i4_pub.i4admin', g_package||'.'||c_procedure||'?p_action=update', 'members')||chr(13);

	elsif (p_action = 'update') then

		if (p_organisation_pk is not null) then
			update	ses_session
			set		org_organisation_fk = p_organisation_pk
			where	id = c_id;
			commit;
		end if;

		v_html := v_html||htm.jumpTo('org_adm.viewOrg', 1, 'logging in...');

	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
    when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
        return null;
end;
end loginAsOrg;


---------------------------------------------------------
-- name:        updateOrgService
-- what:        update an organisation's relation to services
-- author:      Frode Klevstul
-- start date:  29.12.2005
---------------------------------------------------------
procedure updateOrgService(p_action varchar2 default null, p_organisation_pk org_organisation.org_organisation_pk%type default null, p_service_pk ser_service.ser_service_pk%type default null) is begin
    ext_lob.pri(org_i4.updateOrgService(p_action, p_organisation_pk, p_service_pk));
end;

function updateOrgService
(
	  p_action						varchar2											default null
	, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
	, p_service_pk					ser_service.ser_service_pk%type						default null
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type 		:= 'updateOrgService';

	cursor	cur_service is
	select	ser_service_pk, name
	from	ser_service, org_ser
	where	org_organisation_fk_pk = p_organisation_pk
	and		ser_service_fk_pk = ser_service_pk;

	cursor	cur_service2 is
	select	ser_service_pk, name
	from	ser_service
	where	ser_service_pk not in (
		select	ser_service_fk_pk
		from	org_ser
		where	org_organisation_fk_pk = p_organisation_pk
	);

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_action='||p_action||'いp_organisation_pk='||p_organisation_pk||'いp_service_pk='||p_service_pk);
	ext_lob.ini(v_clob);

	use_lib.loggedInCheck(g_package, c_procedure, null);

	if (p_action is null) then

		v_html := v_html||''||org_lib.organisationPicker('i4_pub.i4admin', g_package||'.'||c_procedure||'?p_action=updateForm', 'non_umbrella')||chr(13);

	elsif (p_action = 'updateForm') then

		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'i4admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundAdminBox')||chr(13);
		v_html := v_html||'<tr><td>'||chr(13);

		v_html := v_html||''||htm.bBox('Update service for '||org_lib.getOrgName(p_organisation_pk))||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);

		v_html := v_html||'<tr><td colspan="2"><b>Delete from service:</b></td></tr>'||chr(13);

		for r_cursor in cur_service
		loop
			v_html := v_html||'<tr><td width="30%">'||r_cursor.name||':</td><td>'||htm.link('remove',g_package||'.'||c_procedure||'?p_organisation_pk='||p_organisation_pk||'&amp;p_action=delete&amp;p_service_pk='||r_cursor.ser_service_pk)||'</td></tr>'||chr(13);
		end loop;

		v_html := v_html||'<tr><td colspan="2">&nbsp;</td></tr>'||chr(13);
		v_html := v_html||'<tr><td colspan="2"><b>Relate to new service:</b></td></tr>'||chr(13);

		for r_cursor in cur_service2
		loop
			v_html := v_html||'<tr><td>'||r_cursor.name||':</td><td>'||htm.link('add',g_package||'.'||c_procedure||'?p_organisation_pk='||p_organisation_pk||'&amp;p_action=insert&amp;p_service_pk='||r_cursor.ser_service_pk)||'</td></tr>'||chr(13);
		end loop;

		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);

		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

	elsif (p_action = 'delete') then

		if (p_organisation_pk is not null and p_service_pk is not null) then
			delete	from org_ser
			where	org_organisation_fk_pk	= p_organisation_pk
			and		ser_service_fk_pk		= p_service_pk;
			commit;
		end if;

		v_html := v_html||htm.jumpTo(g_package||'.'||c_procedure||'?p_action=updateForm&amp;p_organisation_pk='||p_organisation_pk, 1, 'deleting...');

	elsif (p_action = 'insert') then

		if (p_organisation_pk is not null and p_service_pk is not null) then
			insert into org_ser
			(org_organisation_fk_pk, ser_service_fk_pk)
			values (p_organisation_pk, p_service_pk);
			commit;
		end if;

		v_html := v_html||htm.jumpTo(g_package||'.'||c_procedure||'?p_action=updateForm&amp;p_organisation_pk='||p_organisation_pk, 1, 'inserting...');

	else

		v_html := v_html||htm.jumpTo('i4_pub.overview', 1, 'no db change made - going back to i4admin...');

	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
    when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
        return null;
end;
end updateOrgService;


---------------------------------------------------------
-- name:        admChosenOrg
-- what:        administrate what org is chosen
-- author:      Frode Klevstul
-- start date:  04.01.2006
---------------------------------------------------------
procedure admChosenOrg(p_action varchar2 default null, p_date_start_pk varchar2 default null, p_service_pk ser_service.ser_service_pk%type default null, p_organisation_pk org_organisation.org_organisation_pk%type default null, p_date_stop varchar2 default null) is begin
    ext_lob.pri(org_i4.admChosenOrg(p_action, p_date_start_pk, p_service_pk, p_organisation_pk, p_date_stop));
end;

function admChosenOrg
(
	  p_action						varchar2											default null
	, p_date_start_pk				varchar2											default null
	, p_service_pk					ser_service.ser_service_pk%type						default null
	, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
	, p_date_stop					varchar2											default null
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type 		:= 'admChosenOrg';

	cursor	cur_org_chosen is
	select	org_organisation_pk, name, date_start_pk, ser_service_fk_pk, date_stop
	from	org_organisation, org_chosen
	where	org_organisation_pk = org_organisation_fk_pk
	order by date_start_pk asc;

	v_days_in_between			int;
	v_days_in_between2			int;
	v_days_difference			int;

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_action='||p_action||'いp_organisation_pk='||p_organisation_pk||'いp_date_start_pk='||p_date_start_pk||'いp_date_stop='||p_date_stop);
	ext_lob.ini(v_clob);

	use_lib.loggedInCheck(g_package, c_procedure, null);

	if (p_action is null) then

		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'i4admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundElements')||chr(13);
		v_html := v_html||'<tr><td class="surroundElements">'||chr(13);

		-- -----------------------
		-- delete exisiting
		-- -----------------------
		v_html := v_html||''||htm.bBox('Delete chosen organisation')||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);

		v_html := v_html||'<tr><td valign="top"><b>status:</b></td><td valign="top" align="center"><b>days:</b></td><td valign="top" align="center"><b>pk:</b></td><td valign="top"><b>name:</b></td><td valign="top"><b>service:</b></td><td><b>from date:</b></td><td><b>to date:</b></td><td>&nbsp;</td></tr>'||chr(13);

		for r_cursor in cur_org_chosen
		loop
			-- calculate how many days till stop_date
			select	r_cursor.date_stop - sysdate as days_between
			into	v_days_in_between
			from	dual;

			-- calculate how many days till start_date
			select	r_cursor.date_start_pk - sysdate as days_between
			into	v_days_in_between2
			from	dual;

			-- how many days in between start and stop date
			v_days_difference := v_days_in_between - v_days_in_between2;

			v_html := v_html||'<tr>'||chr(13);

			-- -------------
			-- status & days
			-- -------------
			v_html := v_html||'	'||chr(13);

			-- active
			if (r_cursor.date_start_pk < sysdate and r_cursor.date_stop > sysdate) then
				v_html := v_html||'<td><span style="color: black; background-color: green;">active</span></td>'||chr(13);
				v_html := v_html||'<td align="right">'||v_days_in_between||' ('||v_days_difference||')</td>'||chr(13);

			-- idle
			elsif (r_cursor.date_start_pk > sysdate and r_cursor.date_stop > sysdate and r_cursor.date_start_pk < r_cursor.date_stop) then
				v_html := v_html||'<td><span style="color: black; background-color: yellow;">idle...</span></td>'||chr(13);
				v_html := v_html||'<td align="right">'||v_days_in_between2||'-'||v_days_in_between||' ('||v_days_difference||')</td>'||chr(13);

			-- expired
			elsif (r_cursor.date_start_pk < sysdate and r_cursor.date_stop < sysdate) then
				v_html := v_html||'<td><span style="color: black; background-color: gray;">expired</span></td>'||chr(13);
				v_html := v_html||'<td align="right">'||v_days_in_between||' ('||v_days_difference||')</td>'||chr(13);

			-- error
			else
				v_html := v_html||'<td><span style="color: white; background-color: red;">ERROR</span></td>'||chr(13);
				v_html := v_html||'<td align="right">'||v_days_in_between||'</td>'||chr(13);
			end if;

			-- -------------
			-- other
			-- -------------
			v_html := v_html||'
								<td align="right">'||r_cursor.org_organisation_pk||'&nbsp;&nbsp;</td>
								<td>'||htm_lib.HTMLencode(r_cursor.name)||'</td>
								<td>'||r_cursor.ser_service_fk_pk||'</td>
								<td>'||to_char(r_cursor.date_start_pk, 'YYYY-MON-DD')||'</td>
								<td>'||to_char(r_cursor.date_stop, 'YYYY-MON-DD')||'</td>
								<td>'||htm.link('remove',g_package||'.'||c_procedure||'?p_action=delete&amp;p_organisation_pk='||r_cursor.org_organisation_pk||'&amp;p_date_start_pk='||to_char(r_cursor.date_start_pk, 'DD-MON-YY')||'&amp;p_service_pk='||r_cursor.ser_service_fk_pk)||'</td>
							  </tr>'||chr(13);
		end loop;

		v_html := v_html||'
			<tr><td colspan="8"><br><b>days explained:</b> alternative 1: <b>X-Y (Z)</b> | alternative 2: <b>Y (Z)</b><br>&nbsp;&nbsp;&nbsp;&nbsp;
				<b>X</b> - number of days till/since start |
				<b>Y</b> - number of days till stop |
				<b>Z</b> - duration / total days |
				</td>
			</tr>'||chr(13);

		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);

		v_html := v_html||'</td></tr><tr><td class="surroundElements">'||chr(13);

		-- -----------------------
		-- add new
		-- -----------------------
		v_html := v_html||''||htm.bForm(g_package||'.'||c_procedure)||chr(13);
		v_html := v_html||'<input type="hidden" name="p_action" value="insert">'||chr(13);
		v_html := v_html||''||htm.bBox('Add new chosen organisation')||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);

		v_html := v_html||'<tr><td valign="top"><b>organisation:</b></td><td valign="top"><b>service:</b></td><td valign="top"><b>from date:</b><br><i>(yymmdd)</i></td><td valign="top"><b>to date:</b><br><i>(yymmdd)</i></td><td>&nbsp;</td></tr>'||chr(13);
		v_html := v_html||'<tr>
							<td>'||chr(13);
		v_html := v_html||''||htm_lib.selectOrganisation('members')||chr(13);
		v_html := v_html||'</td>
							<td>'||htm_lib.selectService||'</td>
							<td><input type="text" name="p_date_start_pk" size="6" maxlength="6" value="'||to_char(sysdate, 'YYMMDD')||'"></td>
							<td><input type="text" name="p_date_stop" size="6" maxlength="6" value="'||to_char(sysdate+7, 'YYMMDD')||'"></td>
							<td>'||htm.submitLink('add')||'</td>
						  </tr>'||chr(13);

		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);
		v_html := v_html||''||htm.eForm||chr(13);

		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

	elsif (p_action = 'delete') then

		if (p_organisation_pk is not null and p_service_pk is not null and p_date_start_pk is not null) then
			delete	from org_chosen
			where	date_start_pk			= to_date(p_date_start_pk, 'DD-MON-YY')
			and		ser_service_fk_pk		= p_service_pk
			and		org_organisation_fk_pk	= p_organisation_pk;
			commit;
		end if;

		v_html := v_html||htm.jumpTo(g_package||'.'||c_procedure, 1, 'deleting...');

	elsif (p_action = 'insert') then

		if (p_organisation_pk is not null and p_service_pk is not null and p_date_start_pk is not null) then
			insert into org_chosen
			(date_start_pk, ser_service_fk_pk, org_organisation_fk_pk, date_stop, date_insert)
			values (to_date(p_date_start_pk, 'YYMMDD'), p_service_pk, p_organisation_pk, to_date(p_date_stop, 'YYMMDD'), sysdate);
			commit;
		end if;

		v_html := v_html||htm.jumpTo(g_package||'.'||c_procedure, 1, 'inserting...');

	else

		v_html := v_html||htm.jumpTo('i4_pub.overview', 1, 'no db change made - going back to i4admin...');

	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
    when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
        return null;
end;
end admChosenOrg;


---------------------------------------------------------
-- name:        deleteOrg
-- what:        deleteOrganisation
-- author:      Frode Klevstul
-- start date:  31.03.2006
---------------------------------------------------------
procedure deleteOrg
(
	  p_action varchar2 default null
	, p_organisation_pk org_organisation.org_organisation_pk%type default null
) is begin
    ext_lob.pri(org_i4.deleteOrg(p_action, p_organisation_pk));
end;

function deleteOrg
(
	  p_action						varchar2											default null
	, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
) return clob
is
begin
declare

	c_procedure					constant mod_procedure.name%type 		:= 'deleteOrg';

	v_tmp						boolean;

	cursor	cur_organisation is
	select	org_organisation_pk, name, org_number
	from	org_organisation
	where	org_organisation_pk = p_organisation_pk;

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_action='||p_action||'いp_organisation_pk='||p_organisation_pk);
	ext_lob.ini(v_clob);

	use_lib.loggedInCheck(g_package, c_procedure, null);

	if (p_action is null) then

		v_html := v_html||''||org_lib.organisationPicker('i4_pub.i4admin', g_package||'.'||c_procedure||'?p_action=deleteForm', 'all')||chr(13);

	elsif (p_action = 'deleteForm' and p_organisation_pk is not null) then

		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'i4admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundAdminBox')||chr(13);
		v_html := v_html||'<tr><td>'||chr(13);

		v_html := v_html||''||htm.bForm(g_package||'.'||c_procedure)||chr(13);
		v_html := v_html||'<input type="hidden" name="p_action" value="delete">'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_organisation_pk" value="'||p_organisation_pk||'">'||chr(13);
		v_html := v_html||''||htm.bBox('Delete organisation')||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);

		for r_cursor in cur_organisation
		loop
			v_html := v_html||'
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td colspan="2" align="center"><b><a href="org_pub.viewOrg?p_organisation_pk='||r_cursor.org_organisation_pk||'" target="_blank">View organisation</a></b></td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td valign="top" colspan="2" align="center">
						<font color="red">
							<b>WARNING!!! WARNING!!! WARNING!!!</b>
							<br>
							THIS action can NOT be undone!
							<br>
							 ALL data (menu, album, forum, users, news, events +++) connected to this organisation will be lost FOREVER!</font><br><br>
							 Note: deleting an umbrella organisation will delete ALL sub / child organisations as well!
					</td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td>PK:</td><td>'||r_cursor.org_organisation_pk||'</td>
				</tr>
				<tr>
					<td>Name:</td><td><b>'||r_cursor.name||'</b></td>
				</tr>
				<tr>
					<td>Orgnumber:</td><td>'||r_cursor.org_number||'</td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
			';
		end loop;

		v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||htm.submitLink('!!! DELETE ORGANISATION !!!')||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||htm.historyBack('GO BACK - Do not delete')||'</td></tr>'||chr(13);

		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);
		v_html := v_html||''||htm.eForm||chr(13);

		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);


	elsif (p_action = 'delete' and p_organisation_pk is not null) then

		v_html := v_html||'';

		v_tmp := org_i4.deleteOrganisation(p_organisation_pk);

		v_html := v_html||htm.jumpTo('i4_pub.overview', 1, 'Organisation deleted, going back...');

	else

		v_html := v_html||htm.jumpTo('i4_pub.overview', 1, 'no db change made - going back to i4admin...');

	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
    when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
        return null;
end;
end deleteOrg;


---------------------------------------------------------
-- name:        updateImplock
-- what:        update an organisation's Import Lock
-- author:      Frode Klevstul
-- start date:  05.04.2006
---------------------------------------------------------
procedure updateImplock
(
	  p_action						varchar2											default null
	, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
	, p_bool						org_organisation.bool_implock%type					default null
) is begin
    ext_lob.pri(org_i4.updateImplock(p_action, p_organisation_pk, p_bool));
end;

function updateImplock
(
	  p_action						varchar2											default null
	, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
	, p_bool						org_organisation.bool_implock%type					default null
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type 		:= 'updateImplock';

	cursor	cur_org is
	select	bool_implock
	from	org_organisation
	where	org_organisation_pk = p_organisation_pk;

	v_html							clob;
	v_clob							clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_action='||p_action||'いp_organisation_pk='||p_organisation_pk||'いp_bool='||p_bool);
	ext_lob.ini(v_clob);

	use_lib.loggedInCheck(g_package, c_procedure, null);

	if (p_action is null) then

		v_html := v_html||''||org_lib.organisationPicker('i4_pub.i4admin', g_package||'.'||c_procedure||'?p_action=updateForm', 'non_umbrella')||chr(13);

	elsif (p_action = 'updateForm') then

		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'i4admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundAdminBox')||chr(13);
		v_html := v_html||'<tr><td>'||chr(13);

		v_html := v_html||''||htm.bForm(g_package||'.'||c_procedure)||chr(13);
		v_html := v_html||'<input type="hidden" name="p_action" value="update">'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_organisation_pk" value="'||p_organisation_pk||'">'||chr(13);
		v_html := v_html||''||htm.bBox('Update IMPLock for '||org_lib.getOrgName(p_organisation_pk))||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);

		for r_cursor in cur_org
		loop
			v_html := v_html||'<tr><td>IMPLock:</td><td>'||htm_lib.selectBool(r_cursor.bool_implock)||'</td></tr>'||chr(13);
		end loop;

		v_html := v_html||'<tr><td colspan="2">&nbsp;</td></tr>'||chr(13);
		v_html := v_html||'<tr><td style="padding-left: 20px;">'||htm.submitLink('update')||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td style="padding-left: 20px;">'||htm.historyBack('go back')||'</td></tr>'||chr(13);

		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);
		v_html := v_html||''||htm.eForm||chr(13);

		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

	elsif (p_action = 'update') then

		if (p_organisation_pk is not null) then
			update	org_organisation
			set		bool_implock 		= p_bool
			where	org_organisation_pk = p_organisation_pk;
			commit;
		end if;

		v_html := v_html||htm.jumpTo('i4_pub.overview', 1, 'updating...');

	else

		v_html := v_html||htm.jumpTo('i4_pub.overview', 1, 'no update made - going back to i4admin...');

	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
    when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
        return null;
end;
end updateImplock;


---------------------------------------------------------
-- name:        updateOrg
-- what:        update an organisation
-- author:      Frode Klevstul
-- start date:  25.04.2006
---------------------------------------------------------
procedure updateOrg
(
	  p_action						varchar2											default null
	, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
) is begin
    ext_lob.pri(org_i4.updateOrg(p_action, p_organisation_pk));
end;

function updateOrg
(
	  p_action						varchar2											default null
	, p_organisation_pk				org_organisation.org_organisation_pk%type			default null
) return clob
is
begin
declare
	c_procedure						constant mod_procedure.name%type 					:= 'updateOrg';

	cursor	cur_org is
	select	bool_implock
	from	org_organisation
	where	org_organisation_pk = p_organisation_pk;

	v_html							clob;
	v_clob							clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_action='||p_action||'いp_organisation_pk='||p_organisation_pk);
	ext_lob.ini(v_clob);

	use_lib.loggedInCheck(g_package, c_procedure, null);

	if (p_action is null) then

		v_html := v_html||''||org_lib.organisationPicker('i4_pub.i4admin', g_package||'.'||c_procedure||'?p_action=updateForm', 'non_umbrella')||chr(13);

	elsif (p_action = 'updateForm') then

		v_html := v_html||htm.jumpTo('kAdmin.admin_table?p_action=list&p_table_name=ORG_ORGANISATION&p_sql_stmt=WHERE%20ORG_ORGANISATION_PK%20=%20'||p_organisation_pk, 1, 'Loading kAdmin...');

	else

		v_html := v_html||htm.jumpTo('i4_pub.overview', 1, 'going back to i4admin...');

	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
    when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
        return null;
end;
end updateOrg;


---------------------------------------------------------
-- name:        fixOrgNames
-- what:        fixes organisation names
-- author:      Frode Klevstul
-- start date:  13.10.2006
---------------------------------------------------------
procedure fixOrgNames is begin
    ext_lob.pri(org_i4.fixOrgNames);
end;

function fixOrgNames
	return clob
is
begin
declare
	c_procedure						constant mod_procedure.name%type 					:= 'fixOrgNames';

	v_noUpdates						number												:= 0;

	-- initcap
	cursor	cur_org_fix01 is
	select	org_organisation_pk, name
	from	org_organisation
	where	upper(name) = name;

	-- find child orgs ending with AS, DA, KS
	cursor	cur_org_fix02 is
	select	org_organisation_pk, name
	from	org_organisation
	where	(bool_umbrella_organisation is null or bool_umbrella_organisation = 0)
	and
			(
				upper(name) like '% AS'
				or
				upper(name) like '% DA'
				or
				upper(name) like '% KS'
				or
				upper(name) like '% ANS'
			);

	-- find child orgs ending with ANS
	cursor	cur_org_fix03 is
	select	org_organisation_pk, name
	from	org_organisation
	where	(bool_umbrella_organisation is null or bool_umbrella_organisation = 0)
	and
			(
				upper(name) like '% ANS'
			);

	v_html							clob;
	v_clob							clob;

begin
	ses_lib.ini(g_package, c_procedure, null);

	use_lib.loggedInCheck(g_package, c_procedure, null);

	ext_lob.ini(v_clob);

	v_html := v_html||''||htm.bPage(g_package, c_procedure, 'i4admin')||chr(13);
	v_html := v_html||'<center>'||chr(13);
	v_html := v_html||''||htm.bTable('surroundAdminBox')||chr(13);
	v_html := v_html||'<tr><td>'||chr(13);

	v_html := v_html||''||htm.bBox('Fix organisation names')||chr(13);
	v_html := v_html||''||htm.bTable||chr(13);

	for r_cursor in cur_org_fix01
	loop
		if (
			-- name has to be longer than one char
			(length(r_cursor.name) > 1)
			-- name not like one word character and one digit
			and not (owa_pattern.match(r_cursor.name, '(\w\d)' ))
			-- name not like one word char and one non word char
			and not (owa_pattern.match(r_cursor.name, '^\w\W' ))
		) then
			v_noUpdates := v_noUpdates + 1;
			v_html := v_html||'<tr><td>'||r_cursor.name||'</td></tr>'||chr(13);
			ext_lob.add(v_clob, v_html);

			update	org_organisation
			set		name = initcap(name)
			where	org_organisation_pk = r_cursor.org_organisation_pk;
		end if;
	end loop;

	commit;

	for r_cursor in cur_org_fix02
	loop
		v_noUpdates := v_noUpdates + 1;
		v_html := v_html||'<tr><td>'||r_cursor.name||'</td></tr>'||chr(13);
		ext_lob.add(v_clob, v_html);

		update	org_organisation
		set		name = SUBSTR(name, 1, length(name)-3)
		where	org_organisation_pk = r_cursor.org_organisation_pk;
	end loop;

	commit;

	for r_cursor in cur_org_fix03
	loop
		v_noUpdates := v_noUpdates + 1;
		v_html := v_html||'<tr><td>'||r_cursor.name||'</td></tr>'||chr(13);
		ext_lob.add(v_clob, v_html);

		update	org_organisation
		set		name = SUBSTR(name, 1, length(name)-4)
		where	org_organisation_pk = r_cursor.org_organisation_pk;
	end loop;

	commit;

	v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;"><br><br>A total of <b>'||v_noUpdates||'</b> organisation names were fixed.</td></tr>'||chr(13);
	v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||chr(13);
	v_html := v_html||htm.link('Back to i4admin', 'i4_pub.overview')||chr(13);
	v_html := v_html||'</td></tr>'||chr(13);

	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||''||htm.eBox||chr(13);

	v_html := v_html||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||'</center>'||chr(13);
	v_html := v_html||''||htm.ePage||chr(13);

	ext_lob.add(v_clob, v_html);

	return v_clob;

exception
    when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
        return null;
end;
end fixOrgNames;


---------------------------------------------------------
-- name:        totalOrgAdm
-- what:        Total / full org admin interface
-- author:      Frode Klevstul
-- start date:  11.11.2006
---------------------------------------------------------
procedure totalOrgAdm
(
	  p_action						varchar2											default null
	, p_name						org_organisation.name%type							default null
	, p_firstCharacters				org_organisation.name%type							default null
	, p_zip_start					add_address.zip%type								default null
	, p_zip_stop					add_address.zip%type								default null
	, p_showLocked					number												default null
	, p_showBanned					number												default null
	, p_showPOrgNo					number												default null
	, p_sort						number												default null
	, p_organisation_pk				parameter_arr										default empty_array
	, p_address_pk					parameter_arr										default empty_array
	, p_org_name					parameter_arr										default empty_array
	, p_street						parameter_arr										default empty_array
	, p_zip							parameter_arr										default empty_array
	, p_bool						parameter_arr										default empty_array
	, p_org_status_pk				parameter_arr										default empty_array
	, p_delete						parameter_arr										default empty_array
) is begin
    ext_lob.pri(org_i4.totalOrgAdm(
		  p_action
		, p_name
		, p_firstCharacters
		, p_zip_start
		, p_zip_stop
		, p_showLocked
		, p_showBanned
		, p_showPOrgNo
		, p_sort
		, p_organisation_pk
		, p_address_pk
		, p_org_name
		, p_street
		, p_zip
		, p_bool
		, p_org_status_pk
		, p_delete
    ));
end;

function totalOrgAdm
(
	  p_action						varchar2											default null
	, p_name						org_organisation.name%type							default null
	, p_firstCharacters				org_organisation.name%type							default null
	, p_zip_start					add_address.zip%type								default null
	, p_zip_stop					add_address.zip%type								default null
	, p_showLocked					number												default null
	, p_showBanned					number												default null
	, p_showPOrgNo					number												default null
	, p_sort						number												default null
	, p_organisation_pk				parameter_arr										default empty_array
	, p_address_pk					parameter_arr										default empty_array
	, p_org_name					parameter_arr										default empty_array
	, p_street						parameter_arr										default empty_array
	, p_zip							parameter_arr										default empty_array
	, p_bool						parameter_arr										default empty_array
	, p_org_status_pk				parameter_arr										default empty_array
	, p_delete						parameter_arr										default empty_array
) return clob
is
begin
declare
	c_procedure						constant mod_procedure.name%type 					:= 'totalOrgAdm';

	cur_dynCursor					sys_refcursor;

	v_i								number												default 0;
	v_array							parameter_arr										default empty_array;

	v_checked1						varchar2(8);
	v_checked2						varchar2(8);
	v_checked3						varchar2(8);
	v_checked4						varchar2(8);
	v_checked5						varchar2(8);
	v_checked6						varchar2(8);
	v_checked7						varchar2(8);

	v_locked						varchar2(8);
	v_status_name					org_status.name%type;
	v_pOrgNo						varchar2(32);
	v_tmp							boolean;

	v_sql_stmt						varchar2(2048);
	v_html							clob;
	v_clob							clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_action='||p_action||'いp_name='||p_name||'いp_firstCharacters='||p_firstCharacters||'いp_zip_start='||p_zip_start||'いp_zip_stop='||p_zip_stop);
	ext_lob.ini(v_clob);

	use_lib.loggedInCheck(g_package, c_procedure, null);


	-- -----------------------------
	-- print out search form
	-- -----------------------------
	if (p_action = 'run' or p_action is null) then

		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'i4admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundElements')||chr(13);
		v_html := v_html||'<tr><td>'||chr(13);

		-- ----------------------
		-- search form
		-- ----------------------
		v_html := v_html||''||htm.bForm(g_package||'.'||c_procedure)||chr(13);
		v_html := v_html||'<input type="hidden" name="p_action" value="run">'||chr(13);
		v_html := v_html||''||htm.bBox('Total Organisation Administration')||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);

		v_html := v_html||'<tr bgcolor="#cccccc"><td width="20%"><b>Org name:</b></td><td>%<input type="text" name="p_name" size="20" maxlength="64" value="'||htm_lib.HTMLEncode(p_name)||'">%</td></tr>'||chr(13);
		v_html := v_html||'<tr bgcolor="#aaaaaa"><td width="20%"><b>First letters:</b></td><td><input type="text" name="p_firstCharacters" size="3" maxlength="10" value="'||htm_lib.HTMLEncode(p_firstCharacters)||'">%</td></tr>'||chr(13);
		v_html := v_html||'<tr bgcolor="#cccccc"><td><b>ZIP:</b></td><td>start: <input type="tex" size="4" maxlength="4" name="p_zip_start" value="'||p_zip_start||'"> stop: <input type="tex" size="4" maxlength="4" name="p_zip_stop" value="'||p_zip_stop||'"></td></tr>'||chr(13);

		if (p_showLocked = 2) then
			v_checked1 := '';
			v_checked2 := 'checked';
		else
			v_checked1 := 'checked';
			v_checked2 := '';
		end if;
		v_html := v_html||'<tr bgcolor="#aaaaaa"><td><b>Show locked:</b></td><td> yes: <input type="radio" name="p_showLocked" value="1" '||v_checked1||'> no: <input type="radio" name="p_showLocked" value="2" '||v_checked2||'></td></tr>'||chr(13);

		if (p_showBanned = 2) then
			v_checked1 := '';
			v_checked2 := 'checked';
		else
			v_checked1 := 'checked';
			v_checked2 := '';
		end if;
		v_html := v_html||'<tr bgcolor="#cccccc"><td><b>Show banned:</b></td><td> yes: <input type="radio" name="p_showBanned" value="1" '||v_checked1||'> no: <input type="radio" name="p_showBanned" value="2" '||v_checked2||'></td></tr>'||chr(13);

		if (p_showPOrgNo = 2) then
			v_checked1 := '';
			v_checked2 := 'checked';
		else
			v_checked1 := 'checked';
			v_checked2 := '';
		end if;
		v_html := v_html||'<tr bgcolor="#aaaaaa"><td><b>Show parents org number:</b></td><td> yes: <input type="radio" name="p_showPOrgNo" value="1" '||v_checked1||'> no: <input type="radio" name="p_showPOrgNo" value="2" '||v_checked2||'> (note: generating the list will take a bit longer when this option is set to "yes")</td></tr>'||chr(13);

		v_checked1 := '';
		v_checked2 := '';
		if (p_sort = 1) then
			v_checked1 := 'checked';
		elsif (p_sort = 2) then
			v_checked2 := 'checked';
		elsif (p_sort = 3) then
			v_checked3 := 'checked';
		elsif (p_sort = 4) then
			v_checked4 := 'checked';
		elsif (p_sort = 5) then
			v_checked5 := 'checked';
		elsif (p_sort = 6) then
			v_checked6 := 'checked';
		elsif (p_sort = 7) then
			v_checked7 := 'checked';
		else
			v_checked1 := 'checked';
		end if;
		v_html := v_html||'<tr bgcolor="#cccccc">
			<td><b>Sort on:</b></td>
			<td>
				| pk: <input type="radio" name="p_sort" value="5" '||v_checked5||'>
				| name: <input type="radio" name="p_sort" value="1" '||v_checked1||'>
				| street: <input type="radio" name="p_sort" value="2" '||v_checked2||'>
				| zip: <input type="radio" name="p_sort" value="3" '||v_checked3||'>
				| source: <input type="radio" name="p_sort" value="4" '||v_checked4||'>
				| lock: <input type="radio" name="p_sort" value="6" '||v_checked6||'>
				| status: <input type="radio" name="p_sort" value="7" '||v_checked7||'>
				|
			</td></tr>'||chr(13);
		v_html := v_html||'<tr bgcolor="#aaaaaa"><td colspan="2">&nbsp;</td></tr>'||chr(13);
		v_html := v_html||'<tr bgcolor="#aaaaaa"><td colspan="2" style="padding-left: 20px;">'||htm.submitLink('generate list')||'</td></tr>'||chr(13);
		v_html := v_html||'<tr bgcolor="#aaaaaa"><td colspan="2" style="padding-left: 20px;">'||htm.historyBack('go back')||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td colspan="2">&nbsp;</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);
		v_html := v_html||''||htm.eForm||chr(13);
	end if;

	-- -----------------------------
	-- print out result of search
	-- -----------------------------
	if (p_action = 'run') then

		v_html := v_html||'</td></tr><tr><td>'||chr(13);

		v_html := v_html||''||htm.bForm(g_package||'.'||c_procedure, 'updateForm')||chr(13);
		v_html := v_html||'<input type="hidden" name="p_action" value="update">'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_name" value="'||p_name||'">'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_firstCharacters" value="'||p_firstCharacters||'">'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_zip_start" value="'||p_zip_start||'">'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_zip_stop" value="'||p_zip_stop||'">'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_showLocked" value="'||p_showLocked||'">'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_showBanned" value="'||p_showBanned||'">'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_showPOrgNo" value="'||p_showPOrgNo||'">'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_sort" value="'||p_sort||'">'||chr(13);
		v_html := v_html||''||htm.bBox('Generated output')||chr(13);
		v_html := v_html||'<table border="1">'||chr(13);

		v_sql_stmt :=   ' select org_organisation_pk, o.name, add_address_fk, bool_implock, parent_org_organisation_fk, org_status_fk, source, zip, street, case when longitude > 0 then ''OK'' else ''<font color="red">NO</font>'' end as longitude '||
						' from org_organisation o, add_address a'||
						' where add_address_fk = add_address_pk'||
						' and lower(o.name) like lower(''%'||p_name||'%'')'||
						' and lower(o.name) like lower('''||p_firstCharacters||'%'')'||
						' and (bool_umbrella_organisation = 0 or bool_umbrella_organisation is NULL)';

		if (p_zip_start is not null) then
			v_sql_stmt := v_sql_stmt||' and zip >= '||p_zip_start;
		end if;

		if (p_zip_stop is not null) then
			v_sql_stmt := v_sql_stmt||' and zip <= '||p_zip_stop;
		end if;

		-- do not show locked
		if (p_showLocked = 2) then
			v_sql_stmt := v_sql_stmt||' and (bool_implock is null or bool_implock = 0)';
		end if;

		-- do not show banned
		if (p_showBanned = 2) then
			v_sql_stmt := v_sql_stmt||' and org_status_fk <> (select org_status_pk from org_status where lower(name) = ''banned'')';
		end if;

		if (p_sort = 1) then
			v_sql_stmt := v_sql_stmt||' order by name asc';
		elsif (p_sort = 2) then
			v_sql_stmt := v_sql_stmt||' order by street asc';
		elsif (p_sort = 3) then
			v_sql_stmt := v_sql_stmt||' order by zip asc';
		elsif (p_sort = 4) then
			v_sql_stmt := v_sql_stmt||' order by source asc';
		elsif (p_sort = 5) then
			v_sql_stmt := v_sql_stmt||' order by org_organisation_pk asc';
		elsif (p_sort = 6) then
			v_sql_stmt := v_sql_stmt||' order by bool_implock asc';
		elsif (p_sort = 7) then
			v_sql_stmt := v_sql_stmt||' order by org_status_fk asc';
		end if;

		v_html := v_html||'
			<tr>
				<td><b>PK:</b></td>
				<td><b>Name:</b></td>
				<td><b>Street:</b></td>
				<td><b>Zip:</b></td>
				<td><b>Source:</b></td>
				<td><b>Parent:</b></td>
				<td><b>POrgNo:</b></td>
				<td><b>ILock:</b></td>
				<td><b>Status:</b></td>
				<td><b>Map:</b></td>
				<td><b>Delete:</b></td>
			</tr>'||chr(13);

		open cur_dynCursor for v_sql_stmt;
		loop
			fetch cur_dynCursor into v_array(1), v_array(2), v_array(3), v_array(4), v_array(5), v_array(6), v_array(7), v_array(8), v_array(9), v_array(10);
			exit when cur_dynCursor%notfound;

			if (v_array(4) > 0) then
				v_locked := 'locked';
			else
				v_locked := 'no';
			end if;

			if (p_showPOrgNo = 1) then
				v_pOrgNo := org_lib.getOrgNo(v_array(5));
			else
				v_pOrgNo := '&nbsp;';
			end if;

			select	name
			into	v_status_name
			from	org_status
			where	org_status_pk = to_number(v_array(6));

			v_html := v_html||'
				<input type="hidden" name="p_organisation_pk" value="'||v_array(1)||'">
				<input type="hidden" name="p_address_pk" value="'||v_array(3)||'">

				<tr>
					<td style="vertical-align: top;"><a target="_blank" href="org_i4.loginAsOrg?p_action=update&amp;p_organisation_pk='||v_array(1)||'">'||v_array(1)||'</a>&nbsp;</td>
					<td style="vertical-align: top;"><input type="text" name="p_org_name" size="20" maxlength="64" value="'||v_array(2)||'"></td>
					<td style="vertical-align: top;"><input type="text" name="p_street" size="15" maxlength="512" value="'||v_array(9)||'"></td>
					<td style="vertical-align: top;"><input type="text" name="p_zip" size="4" maxlength="4" value="'||v_array(8)||'"><br />('||v_array(8)||' '||geo_lib.getCity(v_array(8))||')&nbsp;</td>
					<td style="vertical-align: top;">'||v_array(7)||'&nbsp;</td>
					<td style="vertical-align: top;"><a target="_blank" href="kAdmin.admin_table?p_action=list&amp;p_table_name=ORG_ORGANISATION&amp;p_sql_stmt=WHERE%20ORG_ORGANISATION_PK='||v_array(5)||'">'||org_lib.getOrgName(v_array(5))||'</a>&nbsp;</td>
					<td style="vertical-align: top;">'||v_pOrgNo||'&nbsp;</td>
					<td style="vertical-align: top;">'||htm_lib.selectBool(v_array(4))||'</td>
					<td style="vertical-align: top;">'||htm_lib.selectOrgStatus(v_array(6))||'</td>
					<td style="vertical-align: top;"><a target="_blank" href="/scripts/map/index.cgi?action=1&amp;pk='||v_array(3)||'&amp;search='||v_array(9)||', '||geo_lib.getCity(v_array(8))||'">'||v_array(10)||'</a><a name="'||v_array(3)||'"></td>
					<td style="vertical-align: top;"><input type="checkbox" name="p_delete" value="'||v_array(1)||'"></td>
				</tr>'||chr(13);

			ext_lob.add(v_clob, v_html);
		end loop;
		close cur_dynCursor;

		v_html := v_html||'<tr bgcolor="#aaaaaa"><td colspan="11" style="text-align: right;"><br />'||htm.submitLink('update data', 'updateForm')||'</td></tr>'||chr(13);

		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);
		v_html := v_html||''||htm.eForm||chr(13);

	-- ------------------------------------
	-- update database
	-- ------------------------------------
	elsif (p_action = 'update') then

		if (p_delete.exists(1)) then
			for i in p_delete.first..p_delete.last loop
				v_tmp := org_i4.deleteOrganisation(p_delete(i));
			end loop;
		end if;

		for i in p_organisation_pk.first..p_organisation_pk.last loop
			update	org_organisation
			set		  name				= p_org_name(i)
					, bool_implock		= p_bool(i)
					, org_status_fk		= p_org_status_pk(i)
					, date_status_change= (select (case when org_status_fk = p_org_status_pk(i) then date_status_change else sysdate end) from dual)
					, date_update		= sysdate
			where	org_organisation_pk	= p_organisation_pk(i);

			update	add_address
			set		  street			= p_street(i)
					, zip				= p_zip(i)
					, date_update		= (select (case when street = p_street(i) and zip = p_zip(i) then date_update else sysdate end) from dual)
			where	add_address_pk		= p_address_pk(i);

			commit;
		end loop;

		v_html := v_html||''||htm.jumpTo(g_package||'.'||c_procedure||'?p_action=run&p_name='||p_name||'&p_firstCharacters='||p_firstCharacters||'&p_zip_start='||p_zip_start||'&p_zip_stop='||p_zip_stop||'&p_showLocked='||p_showLocked||'&p_showBanned='||p_showBanned||'&p_showPOrgNo='||p_showPOrgNo||'&p_sort='||p_sort, '1', 'updating data, please wait...')||chr(13);

	end if;

	-- ------------------------------------
	-- end page, if we're not updating
	-- ------------------------------------
	if (p_action = 'run' or p_action is null) then
		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);
	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		return null;
end;
end totalOrgAdm;


---------------------------------------------------------
-- name:        deleteOrganisation
-- what:        delete organisation and data in related tables
-- author:      Frode Klevstul
-- start date:  19.11.2006
---------------------------------------------------------
function deleteOrganisation
(
	p_organisation_pk				org_organisation.org_organisation_pk%type			default null
) return boolean
is
begin
declare
	c_procedure						constant mod_procedure.name%type 					:= 'deleteOrganisation';

	v_count							number;

begin
	ses_lib.ini(g_package, c_procedure, 'p_organisation_pk='||p_organisation_pk);

	if (p_organisation_pk is null) then
		return false;
	end if;

	-- -----------
	-- ORG
	-- -----------
	delete from org_chosen
	where org_organisation_fk_pk = p_organisation_pk;

	delete from org_organisation_rights
	where org_organisation_fk_pk = p_organisation_pk;

	delete from org_ser
	where org_organisation_fk_pk = p_organisation_pk;

	delete from org_hours
	where org_organisation_fk_pk = p_organisation_pk;

	delete from org_value
	where org_organisation_fk_pk = p_organisation_pk;

	delete from org_original
	where org_organisation_fk_pk = p_organisation_pk;

	-- -----------
	-- USE
	-- -----------
	select	count(*)
	into	v_count
	from	use_org
	where	org_organisation_fk_pk = p_organisation_pk;

	if (v_count>0) then
		select	use_user_fk_pk
		into	v_count
		from	use_org
		where	org_organisation_fk_pk = p_organisation_pk;

		delete from use_org
		where org_organisation_fk_pk = p_organisation_pk;

		delete from use_details
		where use_user_fk_pk = v_count;

		delete from use_user
		where use_user_pk = v_count;
	end if;

	-- -----------
	-- MEM
	-- -----------
	delete from mem_org
	where org_organisation_fk_pk = p_organisation_pk;

	-- -----------
	-- CON
	-- -----------
	delete from con_content
	where org_organisation_fk = p_organisation_pk;

	-- -----------
	-- PIC
	-- -----------
	delete from pic_album
	where org_organisation_fk = p_organisation_pk;

	-- -----------
	-- MEN
	-- -----------
	delete from men_menu
	where org_organisation_fk = p_organisation_pk;

	-- -----------
	-- COM
	-- -----------
	delete from com_org
	where org_organisation_fk_pk = p_organisation_pk;

	-- -----------
	-- ADD
	-- -----------
	delete from add_address
	where add_address_pk =
		(
			select	add_address_fk
			from	org_organisation
			where	org_organisation_pk = p_organisation_pk
		);

	-- -----------
	-- FOR
	-- -----------
	delete from for_type
	where org_organisation_fk = p_organisation_pk;

	-- -----------
	-- FINAL DELETE
	-- -----------
	delete from org_organisation
	where org_organisation_pk = p_organisation_pk;

	commit;

	return true;

end;
end deleteOrganisation;


-- ******************************************** --
end;
/
show errors;
