PROMPT *** pic_lib ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package pic_lib is
/* ******************************************************
*	Package:     pic_lib
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	060708 | Frode Klevstul
*			- Package created
****************************************************** */
	type parameter_arr 			is table of varchar2(4000) index by binary_integer;
	empty_array 				parameter_arr;

	procedure run;
	procedure albumIndex
	(
		  p_organisation_pk				org_organisation.org_organisation_pk%type			default null
		, p_album_pk					pic_album.pic_album_pk%type							default null
	);
	function albumIndex
	(
		  p_organisation_pk				org_organisation.org_organisation_pk%type			default null
		, p_album_pk					pic_album.pic_album_pk%type							default null
	) return clob;
	procedure pictureIndex
	(
		p_album_pk					pic_album.pic_album_pk%type							default null
	);
	function pictureIndex
	(
		p_album_pk					pic_album.pic_album_pk%type							default null
	) return clob;
	procedure viewPicture
	(
		  p_picture_pk					pic_picture.pic_picture_pk%type					default null
		, p_slideshow					number											default null
	);
	function viewPicture
	(
		  p_picture_pk					pic_picture.pic_picture_pk%type					default null
		, p_slideshow					number											default null
	) return clob;
	procedure pictures
	(
		  p_organisation_pk		org_organisation.org_organisation_pk%type			default null
		, p_editMode			in boolean											default false
	);
	function pictures
	(
		  p_organisation_pk		org_organisation.org_organisation_pk%type			default null
		, p_editMode			in boolean											default false
	) return clob;
	function noAlbums
	(
		  p_organisation_pk		org_organisation.org_organisation_pk%type			default null
		, p_bool_activeOnly		boolean												default false
		, p_bool_withPicOnly	boolean												default false
	) return number;
	function noPictures
	(
		  p_album_pk			pic_album.pic_album_pk%type							default null
		, p_bool_activeOnly		boolean												default false
	) return number;
	function noActiveOrgPictures
	(
		  p_organisation_pk		org_organisation.org_organisation_pk%type			default null
	) return number;
	function isActive
	(
		  p_album_pk			pic_album.pic_album_pk%type							default null
		, p_picture_pk			pic_picture.pic_picture_pk%type						default null
	) return boolean;
	procedure latestPictures
	(
		p_fullMode					boolean											default false
	);
	function latestPictures
	(
		p_fullMode					boolean											default false
	) return clob;
	procedure showThumb
	(
		  p_picture_pk				pic_picture.pic_picture_pk%type						default null
		, p_link					boolean												default false
	);
	function showThumb
	(
		  p_picture_pk				pic_picture.pic_picture_pk%type						default null
		, p_link					boolean												default false
	) return clob;
	function getAlbumText
	(
		  p_album_pk			pic_album.pic_album_pk%type							default null
		, p_type				varchar2											default null
	) return varchar2;
	procedure miniThumbs
	(
		  p_organisation_pk		org_organisation.org_organisation_pk%type			default null
		, p_album_pk			pic_album.pic_album_pk%type							default null
		, p_picture_pk			pic_picture.pic_picture_pk%type						default null
		, p_link				boolean												default true
	);
	function miniThumbs
	(
		  p_organisation_pk		org_organisation.org_organisation_pk%type			default null
		, p_album_pk			pic_album.pic_album_pk%type							default null
		, p_picture_pk			pic_picture.pic_picture_pk%type						default null
		, p_link				boolean												default true
	) return clob;
	procedure navigationLinks
	(
		  p_organisation_pk		org_organisation.org_organisation_pk%type			default null
		, p_album_pk			pic_album.pic_album_pk%type							default null
		, p_picture_pk			pic_picture.pic_picture_pk%type						default null
	);
	function navigationLinks
	(
		  p_organisation_pk		org_organisation.org_organisation_pk%type			default null
		, p_album_pk			pic_album.pic_album_pk%type							default null
		, p_picture_pk			pic_picture.pic_picture_pk%type						default null
	) return clob;
	procedure getThumb
	(
		  p_organisation_pk		org_organisation.org_organisation_pk%type			default null
		, p_filename			pic_picture.path_filename%type						default null
	);
	function getThumb
	(
		  p_organisation_pk		org_organisation.org_organisation_pk%type			default null
		, p_filename			pic_picture.path_filename%type						default null
	) return varchar2;
	function getPrevNext
	(
		  p_picture_pk			pic_picture.pic_picture_pk%type						default null
		, p_type				varchar2											default null
	) return pic_picture.pic_picture_pk%type;

end;
/
show errors;





-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body pic_lib is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'pic_lib';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  08.07.2006
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        albumIndex
-- what:        create an index over all albums
-- author:      Frode Klevstul
-- start date:  10.07.2006
---------------------------------------------------------
procedure albumIndex
(
	  p_organisation_pk				org_organisation.org_organisation_pk%type			default null
	, p_album_pk					pic_album.pic_album_pk%type							default null
) is begin
	ext_lob.pri(pic_lib.albumIndex(p_organisation_pk, p_album_pk));
end;

function albumIndex
(
	  p_organisation_pk				org_organisation.org_organisation_pk%type			default null
	, p_album_pk					pic_album.pic_album_pk%type							default null

) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type					:= 'albumIndex';

	c_tex_albums				constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'albums');
	c_tex_orgPage				constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'orgPage');
	c_tex_albumIndex			constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'albumIndex');

	v_noActivePictures			number;

	cursor	cur_pic_album is
	select	pic_album_pk, title
	from	pic_album
	where	org_organisation_fk = p_organisation_pk
	and		bool_active > 0
	order by title;

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := v_html||''||htm.bBox(c_tex_albums)||chr(13);
	v_html := v_html||''||htm.bTable||chr(13);

	v_html := v_html||'
		<tr>
			<td>&laquo; [<a href="org_pub.viewOrg?p_organisation_pk='||p_organisation_pk||'"> '||c_tex_orgPage||' </a>]</td>
		</tr>
		<tr><td>&nbsp;</td></tr>'||chr(13);

	for r_cursor in cur_pic_album
	loop
		v_noActivePictures := pic_lib.noPictures(r_cursor.pic_album_pk, true);
		if (v_noActivePictures > 0) then
			v_html := v_html||''||'
				<tr>
					<td>&raquo; <a href="pic_pub.viewAlbum?p_organisation_pk='||p_organisation_pk||'&amp;p_album_pk='||r_cursor.pic_album_pk||'">'||substr(r_cursor.title,1,26)||' ('||v_noActivePictures||')</a></td>
				</tr>'||chr(13);
				ext_lob.add(v_clob, v_html);
		end if;
	end loop;

	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||''||htm.eBox||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end albumIndex;


---------------------------------------------------------
-- name:        pictureIndex
-- what:        create an index over all pictures in an album
-- author:      Frode Klevstul
-- start date:  10.07.2006
---------------------------------------------------------
procedure pictureIndex
(
	p_album_pk					pic_album.pic_album_pk%type							default null
) is begin
	ext_lob.pri(pic_lib.pictureIndex(p_album_pk));
end;

function pictureIndex
(
	p_album_pk					pic_album.pic_album_pk%type							default null
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type					:= 'pictureIndex';

	c_tex_viewAsSlideshow		constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'viewAsSlideshow');

	v_directory_path			sys.all_directories.directory_path%type;

	v_organisation_pk			org_organisation.org_organisation_pk%type;
	v_topWritten				boolean												:= false;
	v_firstEntryDone			boolean												:= false;

	cursor	cur_pic_album is
	select	title, subtitle, org_organisation_fk
	from	pic_album
	where	bool_active > 0
	and		pic_album_pk = p_album_pk
	order by title;

	cursor	cur_pic_picture is
	select	pic_picture_pk, description, path_filename
	from	pic_picture, pic_album_picture, pic_album
	where	pic_picture_pk = pic_picture_fk_pk
	and		pic_album_fk_pk = p_album_pk
	and		pic_album_pk = pic_album_fk_pk
	and		pic_picture.bool_active > 0
	and		pic_album.bool_active > 0
	order by pic_picture.date_insert;

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	for r_cursor in cur_pic_album
	loop
		v_organisation_pk := r_cursor.org_organisation_fk;
		v_directory_path := org_lib.getDirectoryPath(v_organisation_pk, true);
		v_topWritten := true;

		v_html := v_html||''||htm.bBox(org_lib.getOrgName(v_organisation_pk))||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);
		v_html := v_html||''||'<tr><td class="picTitleHeading" style="text-align: center;">'||r_cursor.title||'</td></tr>'||chr(13);
		v_html := v_html||''||'<tr><td class="picSubtitleHeading" style="text-align: center;">'||r_cursor.subtitle||'</td></tr>'||chr(13);
		v_html := v_html||''||'<tr><td>&nbsp;</td></tr>'||chr(13);
	end loop;

	for r_cursor in cur_pic_picture
	loop
		if not (v_firstEntryDone) then
			v_firstEntryDone := true;
			v_html := v_html||''||'<tr><td style="text-align: center;">'||htm.link(c_tex_viewAsSlideshow,'pic_pub.viewPicture?p_organisation_pk='||v_organisation_pk||'&amp;p_picture_pk='||r_cursor.pic_picture_pk||'&amp;p_album_pk='||p_album_pk||'&amp;p_slideshow=1')||'</td></tr>'||chr(13);
			v_html := v_html||''||'<tr><td>'||chr(13);
		end if;

		v_html := v_html||'
			<a class="thumbs" href="pic_pub.viewPicture?p_organisation_pk='||v_organisation_pk||'&amp;p_picture_pk='||r_cursor.pic_picture_pk||'&amp;p_album_pk='||p_album_pk||'">
			<img alt="'||r_cursor.description||'" title="'||r_cursor.description||'" src="'||v_directory_path||'/'||pic_lib.getThumb(v_organisation_pk, r_cursor.path_filename)||'" width="180" height="180">
			</a>'||chr(13);
		ext_lob.add(v_clob, v_html);
	end loop;

	if (v_topWritten) then
		v_html := v_html||''||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);
	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end pictureIndex;


---------------------------------------------------------
-- name:        viewPicture
-- what:        show one picture
-- author:      Frode Klevstul
-- start date:  10.07.2006
---------------------------------------------------------
procedure viewPicture
(
	  p_picture_pk					pic_picture.pic_picture_pk%type					default null
	, p_slideshow					number											default null
) is begin
	ext_lob.pri(pic_lib.viewPicture(p_picture_pk, p_slideshow));
end;

function viewPicture
(
	  p_picture_pk					pic_picture.pic_picture_pk%type					default null
	, p_slideshow					number											default null
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type					:= 'viewPicture';

	v_nextImage					pic_picture.pic_picture_pk%type;

	cursor	cur_pic_picture is
	select	description, path_filename, pic_album_fk_pk, org_organisation_fk
	from	pic_picture, pic_album_picture, pic_album
	where	pic_picture_pk = pic_picture_fk_pk
	and		pic_album_pk = pic_album_fk_pk
	and		pic_picture.bool_active > 0
	and		pic_album.bool_active > 0
	and		pic_picture_pk = p_picture_pk;

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	for r_cursor in cur_pic_picture
	loop
		v_html := v_html||''||htm.bBox(org_lib.getOrgName(r_cursor.org_organisation_fk)||' : '||pic_lib.getAlbumText(r_cursor.pic_album_fk_pk, 'all')) ||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);

		v_html := v_html||'
			<tr>
				<td style="text-align: center;">
					'||pic_lib.navigationLinks(r_cursor.org_organisation_fk, r_cursor.pic_album_fk_pk, p_picture_pk)||'
				</td>
			<tr>
				<td style="text-align: center; background-color: #000000;">
					<br /><br /><br />
					<img alt="'||r_cursor.description||'" title="'||r_cursor.description||'" src="'||org_lib.getDirectoryPath(r_cursor.org_organisation_fk, true)||'/'||r_cursor.path_filename||'">
					<br />
					<div class="picCaption">'||r_cursor.description||'</div>
					<br /><br /><br /><br />
				</td>
			</tr>';
		v_html := v_html||''||'<tr><td>'||chr(13);
		ext_lob.add(v_clob, v_html);
		v_html := v_html||pic_lib.miniThumbs(r_cursor.org_organisation_fk, r_cursor.pic_album_fk_pk, p_picture_pk);
		v_html := v_html||''||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);

		v_nextImage := pic_lib.getPrevNext(p_picture_pk, 'next');

		if ( (v_nextImage is not null) and (p_slideshow = 1) ) then
			v_html := v_html||'
				<script type="text/javascript">
				<!--
					function loadNextPicture(){
						window.location = "pic_pub.viewPicture?p_organisation_pk='||r_cursor.org_organisation_fk||'&p_picture_pk='||v_nextImage||'&p_album_pk='||r_cursor.pic_album_fk_pk||'&p_slideshow='||p_slideshow||'"
					}

					setTimeout(''loadNextPicture()'', 5000);
				//-->
				</script>'||chr(13);
		end if;

	end loop;

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end viewPicture;


---------------------------------------------------------
-- name:        pictures
-- what:        display PIC module for use on org page
-- author:      Frode Klevstul
-- start date:  10.07.2006
---------------------------------------------------------
procedure pictures
(
	  p_organisation_pk		org_organisation.org_organisation_pk%type			default null
	, p_editMode			in boolean											default false
) is begin
	ext_lob.pri(pic_lib.pictures(p_organisation_pk, p_editMode));
end;

function pictures
(
	  p_organisation_pk		org_organisation.org_organisation_pk%type			default null
	, p_editMode			in boolean											default false
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type					:= 'pictures';

	c_tex_pic					constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'pic');
	c_tex_addPic				constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'addPic');
	c_tex_viewAll				constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'viewAll');

	v_i							number												:= 0;
	v_count						number;
	v_noActiveAlbums			number												:= pic_lib.noAlbums(p_organisation_pk, null, true);
	v_noActivePictures			number;
	v_noAlbumsToShow			number												:= 8;
	v_noPicturesToShow			number												:= 5;
	c_directory_path			constant sys.all_directories.directory_path%type	:= org_lib.getDirectoryPath(p_organisation_pk, true);

	cursor	cur_pic_album is
	select	pic_album_pk, title, org_organisation_fk
	from	pic_album
	where	org_organisation_fk = p_organisation_pk
	and		bool_active > 0
	order by title;

	cursor	cur_pic_picture is
	select	pic_picture_pk, description, path_filename, pic_album_pk
	from	pic_picture, pic_album_picture, pic_album
	where	pic_picture_pk = pic_picture_fk_pk
	and		pic_album_pk = pic_album_fk_pk
	and		pic_picture.bool_active > 0
	and		pic_album.bool_active > 0
	and		org_organisation_fk = p_organisation_pk
	order by pic_picture.date_insert desc;

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_count := noActiveOrgPictures(p_organisation_pk);

	if (v_count > 0 or p_editMode) then
		v_html := v_html||''||htm.bBox(c_tex_pic)||chr(13);
		v_html := v_html||''||htm.bHighlight('pic_adm.editAlbums', p_editMode)||''||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);

		-- if in editMode
		if (v_count = 0) then
			v_html := v_html||'<tr><td class="centerMiddle" colspan="2">'||chr(13);
			v_html := v_html||'<br /><br /><span class="adminContentButton">'||c_tex_addPic||'</span><br /><br /><br />'||chr(13);
			v_html := v_html||'</td></tr>'||chr(13);
		end if;

		v_html := v_html||'<tr><td style="vertical-align: top; width: 450px;">'||chr(13);

		for r_cursor in cur_pic_album
		loop
			v_noActivePictures := pic_lib.noPictures(r_cursor.pic_album_pk, true);
			if (v_noActivePictures > 0) then
				v_i := v_i + 1;
				if (p_editMode) then
					v_html := v_html||'&raquo; <a href="pic_adm.editPictures?p_album_pk='||r_cursor.pic_album_pk||'">'||substr(r_cursor.title,1,26)||' ('||v_noActivePictures||')</a><br>'||chr(13);
				else
					v_html := v_html||'&raquo; <a href="pic_pub.viewAlbum?p_organisation_pk='||p_organisation_pk||'&amp;p_album_pk='||r_cursor.pic_album_pk||'">'||substr(r_cursor.title,1,26)||' ('||v_noActivePictures||')</a><br>'||chr(13);
				end if;
			end if;
			exit when v_i = v_noAlbumsToShow;
		end loop;

		if (v_i < v_noActiveAlbums) then
			v_html := v_html||'<br>&raquo; <a href="pic_pub.viewAlbum?p_organisation_pk='||p_organisation_pk||'">'||c_tex_viewAll||'</a><br>'||chr(13);
		end if;

		v_html := v_html||''||'</td>'||chr(13);

		v_i := 0;
		for r_cursor in cur_pic_picture
		loop
			v_i := v_i + 1;
			if (p_editMode) then
				v_html := v_html||'<td class="thumbs"><img alt="'||r_cursor.description||'" src="'||c_directory_path||'/'||pic_lib.getThumb(p_organisation_pk, r_cursor.path_filename)||'" height="135" width="135" border="0"></td>'||chr(13);
			else
				v_html := v_html||'
					<td>
						<a class="thumbs" href="pic_pub.viewPicture?p_organisation_pk='||p_organisation_pk||'&amp;p_picture_pk='||r_cursor.pic_picture_pk||'&amp;p_album_pk='||r_cursor.pic_album_pk||'">
						<img alt="'||r_cursor.description||'" title="'||r_cursor.description||'" src="'||c_directory_path||'/'||pic_lib.getThumb(p_organisation_pk, r_cursor.path_filename)||'" height="135" width="135" border="0">
						</a>
					</td>'||chr(13);
			end if;
			exit when v_i = v_noPicturesToShow;
		end loop;

		v_html := v_html||''||'</tr>'||chr(13);

		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eHighlight||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);
	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end pictures;


---------------------------------------------------------
-- name:        noAlbums
-- what:        returns the number of albums for an organisation
-- author:      Frode Klevstul
-- start date:  11.07.2006
---------------------------------------------------------
function noAlbums
(
	  p_organisation_pk		org_organisation.org_organisation_pk%type			default null
	, p_bool_activeOnly		boolean												default false
	, p_bool_withPicOnly	boolean												default false
) return number
is
begin
declare
	c_procedure				constant mod_procedure.name%type					:= 'noAlbums';

	v_count					number;

begin
	ses_lib.ini(g_package, c_procedure);

	-- p_bool_withPicOnly = active albums with active pictures
	if (p_bool_withPicOnly) then
		select	count(distinct(pic_album_pk))
		into	v_count
		from	pic_album, pic_album_picture, pic_picture
		where	pic_picture_pk = pic_picture_fk_pk
		and		pic_album_pk = pic_album_fk_pk
		and		org_organisation_fk = p_organisation_pk
		and		pic_album.bool_active > 0
		and		pic_picture.bool_active > 0;
	elsif (p_bool_activeOnly) then
		select	count(*)
		into	v_count
		from	pic_album
		where	org_organisation_fk = p_organisation_pk
		and		bool_active > 0;
	else
		select	count(*)
		into	v_count
		from	pic_album
		where	org_organisation_fk = p_organisation_pk;
	end if;

	return v_count;

end;
end noAlbums;


---------------------------------------------------------
-- name:        noPictures
-- what:        returns the number of pictures in an album (no matter the album being active or not)
-- author:      Frode Klevstul
-- start date:  11.07.2006
---------------------------------------------------------
function noPictures
(
	  p_album_pk			pic_album.pic_album_pk%type							default null
	, p_bool_activeOnly		boolean												default false
) return number
is
begin
declare
	c_procedure				constant mod_procedure.name%type					:= 'noPictures';

	v_count					number;

begin
	ses_lib.ini(g_package, c_procedure);

	if (p_bool_activeOnly) then
		select	count(*)
		into	v_count
		from	pic_album_picture, pic_picture
		where	pic_picture_fk_pk = pic_picture_pk
		and		pic_album_fk_pk = p_album_pk
		and		bool_active > 0;
	else
		select	count(*)
		into	v_count
		from	pic_album_picture
		where	pic_album_fk_pk = p_album_pk;
	end if;

	return v_count;

end;
end noPictures;


---------------------------------------------------------
-- name:        noActiveOrgPictures
-- what:        returns the number of active pictures for an org, in active albums only
-- author:      Frode Klevstul
-- start date:  18.09.2006
---------------------------------------------------------
function noActiveOrgPictures
(
	  p_organisation_pk		org_organisation.org_organisation_pk%type			default null
) return number
is
begin
declare
	c_procedure				constant mod_procedure.name%type					:= 'noActiveOrgPictures';

	cursor	cur_pic_album is
	select	pic_album_pk
	from	pic_album
	where	org_organisation_fk = p_organisation_pk
	and		bool_active > 0;

	v_count					number := 0;

begin
	ses_lib.ini(g_package, c_procedure);

	for r_cursor in cur_pic_album
	loop
		v_count := v_count + pic_lib.noPictures(r_cursor.pic_album_pk, true);
	end loop;

	return v_count;

end;
end noActiveOrgPictures;


---------------------------------------------------------
-- name:        isActive
-- what:        returns the active status of an album or picture
-- author:      Frode Klevstul
-- start date:  11.07.2006
---------------------------------------------------------
function isActive
(
	  p_album_pk			pic_album.pic_album_pk%type							default null
	, p_picture_pk			pic_picture.pic_picture_pk%type						default null
) return boolean
is
begin
declare
	c_procedure				constant mod_procedure.name%type					:= 'isActive';

	v_count					number;

begin
	ses_lib.ini(g_package, c_procedure);

	if (p_album_pk is not null) then
		select	count(*)
		into	v_count
		from	pic_album
		where	pic_album_pk = p_album_pk
		and		bool_active > 0;
	elsif (p_picture_pk is not null) then
		select	count(*)
		into	v_count
		from	pic_picture
		where	pic_picture_pk = p_picture_pk
		and		bool_active > 0;
	else
		return false;
	end if;

	if (v_count > 0) then
		return true;
	else
		return false;
	end if;

end;
end isActive;


---------------------------------------------------------
-- name:        latestPictures
-- what:        displays the lates pictures
-- author:      Frode Klevstul
-- start date:  12.07.2006
---------------------------------------------------------
procedure latestPictures
(
	p_fullMode					boolean											default false
)
is begin
	ext_lob.pri(pic_lib.latestPictures(p_fullMode));
end;

function latestPictures
(
	p_fullMode					boolean											default false
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type				:= 'latestPictures';

	c_tex_latestPics			constant tex_text.string%type					:= tex_lib.get(g_package, c_procedure, 'latestPics');
	c_tex_allPics				constant tex_text.string%type					:= tex_lib.get(g_package, c_procedure, 'allPics');
	c_tex_viewMorePics			constant tex_text.string%type					:= tex_lib.get(g_package, c_procedure, 'viewMorePics');

	v_i							number											:= 0;
	v_noPicsToShow				number;
	v_boxHeading				varchar2(64);
	v_imgTitle					varchar2(128);

	cursor	cur_pic_album is
	select	pic_album_pk, title, org_organisation_fk
	from	pic_album
	where	bool_active > 0
	order by date_update desc;

	-- ----------------------------------------------------------------------------------------
	-- select random picture
	-- alt 1: from	pic_picture sample(50), pic_album_picture
	-- alt 2: order by dbms_random.value()
	-- ref: http://www.orafaq.com/faq/can_one_select_a_random_collection_of_rows_from_a_table
	-- ----------------------------------------------------------------------------------------
	cursor	cur_pic_picture
	(
		b_album_pk pic_album.pic_album_pk%type
	) is
	select	pic_picture_pk, description, path_filename, org_organisation_fk
	from	pic_picture, pic_album_picture, pic_album
	where	pic_picture_fk_pk = pic_picture_pk
	and		pic_picture.bool_active > 0
	and		pic_album_fk_pk = b_album_pk
	and		pic_album_pk = pic_album_fk_pk
	order by dbms_random.value();

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	if (p_fullMode) then
		v_boxHeading	:= c_tex_allPics;
		v_noPicsToShow	:= 999;
	else
		v_boxHeading	:= c_tex_latestPics;
		v_noPicsToShow	:= 6;
	end if;

	v_i := 0;
	for r_cursor in cur_pic_album
	loop
		for r_cursor2 in cur_pic_picture(r_cursor.pic_album_pk)
		loop
			v_i := v_i + 1;

			-- can't use %rowcount since it's a "non-cursor"
			-- PLS-00324: cursor attribute may not be applied to non-cursor 'R_CURSOR'
			if (v_i = 1) then
				v_html := v_html||''||htm.bBox(v_boxHeading)||chr(13);
				v_html := v_html||''||htm.bTable||chr(13);
				v_html := v_html||'<tr>'||chr(13);
			end if;

			v_imgTitle := org_lib.getOrgName(r_cursor.org_organisation_fk) ||' :: '||r_cursor.title||' :: '||r_cursor2.description;

			if (v_i <= v_noPicsToShow) then
				v_html := v_html||'
					<td class="centerMiddle">
						<a class="thumbs" href="pic_pub.viewPicture?p_organisation_pk='||r_cursor.org_organisation_fk||'&amp;p_picture_pk='||r_cursor2.pic_picture_pk||'&amp;p_album_pk='||r_cursor.pic_album_pk||'">
						<img alt="'||v_imgTitle||'" title="'||v_imgTitle||'" src="'||org_lib.getDirectoryPath(r_cursor.org_organisation_fk, true)||'/'||pic_lib.getThumb(r_cursor.org_organisation_fk, r_cursor2.path_filename)||'" height="135" width="135" border="0">
						</a>
					</td>'||chr(13);
				if (mod(v_i,6)=0 and p_fullMode) then
					v_html := v_html||'</tr><tr>';
				end if;
			end if;
			exit;
		end loop;
		exit when v_i > v_noPicsToShow;															-- do this to find out if we shall add the "show more" link
	end loop;

	if (v_i > 0) then
		v_html := v_html||''||'</tr>'||chr(13);

		if (v_i > v_noPicsToShow) then
			v_html := v_html||''||'
				<tr>
					<td colspan="'||v_i||'" style="text-align: right;">'||
						htm.link(c_tex_viewMorePics,'pic_pub.viewAlbums','_self', 'theButtonStyle')
					||'
					</td>
				</tr>
			'||chr(13);
		end if;

		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);
	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end latestPictures;


---------------------------------------------------------
-- name:        showThumb
-- what:        display a thumb of a picture
-- author:      Frode Klevstul
-- start date:  12.07.2006
---------------------------------------------------------
procedure showThumb
(
	  p_picture_pk				pic_picture.pic_picture_pk%type						default null
	, p_link					boolean												default false
) is begin
	ext_lob.pri(pic_lib.showThumb(p_picture_pk, p_link));
end;

function showThumb
(
	  p_picture_pk				pic_picture.pic_picture_pk%type						default null
	, p_link					boolean												default false
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type					:= 'showThumb';

	v_directory_path			sys.all_directories.directory_path%type;
	v_organisation_pk			org_organisation.org_organisation_pk%type;
	v_album_pk					pic_album.pic_album_pk%type;

	cursor	cur_pic_album is
	select	org_organisation_fk, pic_album_fk_pk
	from	pic_album, pic_album_picture
	where	pic_album_pk = pic_album_fk_pk
	and		pic_picture_fk_pk = p_picture_pk;

	cursor	cur_pic_picture is
	select	pic_picture_pk, description, path_filename
	from	pic_picture
	where	pic_picture_pk = p_picture_pk;

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	for r_cursor in cur_pic_album
	loop
		v_organisation_pk	:= r_cursor.org_organisation_fk;
		v_album_pk			:= r_cursor.pic_album_fk_pk;
		v_directory_path	:= org_lib.getDirectoryPath(v_organisation_pk, true);
	end loop;

	for r_cursor in cur_pic_picture
	loop
		if (p_link) then
			v_html := v_html||'<a class="thumbs" href="pic_pub.viewPicture?p_organisation_pk='||v_organisation_pk||'&amp;p_picture_pk='||p_picture_pk||'&amp;p_album_pk='||v_album_pk||'">'||chr(13);
		end if;
		v_html := v_html||'<img alt="'||r_cursor.description||'" title="'||r_cursor.description||'" src="'||v_directory_path||'/'||pic_lib.getThumb(v_organisation_pk, r_cursor.path_filename)||'" border="0">'||chr(13);
		if (p_link) then
			v_html := v_html||'</a>'||chr(13);
		end if;
	end loop;

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end showThumb;


---------------------------------------------------------
-- name:        getAlbumText
-- what:        returns title or subtitle or both for an album
-- author:      Frode Klevstul
-- start date:  12.07.2006
---------------------------------------------------------
function getAlbumText
(
	  p_album_pk			pic_album.pic_album_pk%type							default null
	, p_type				varchar2											default null
) return varchar2
is
begin
declare
	c_procedure				constant mod_procedure.name%type					:= 'getAlbumText';

	v_sub_title				pic_album.subtitle%type;

begin
	ses_lib.ini(g_package, c_procedure);

	if (p_album_pk is null) then
		return null;
	end if;

	if (p_type = 'title') then
		select	title
		into	v_sub_title
		from	pic_album
		where	pic_album_pk = p_album_pk;
	elsif (p_type = 'subtitle') then
		select	subtitle
		into	v_sub_title
		from	pic_album
		where	pic_album_pk = p_album_pk;
	elsif (p_type = 'all') then
		select	title ||' - '|| subtitle
		into	v_sub_title
		from	pic_album
		where	pic_album_pk = p_album_pk;
	end if;

	return v_sub_title;

exception
	when no_data_found then
		return null;
end;
end getAlbumText;


---------------------------------------------------------
-- name:        miniThumbs
-- what:        returns a list of mini thumbs
-- author:      Frode Klevstul
-- start date:  14.07.2006
---------------------------------------------------------
procedure miniThumbs
(
	  p_organisation_pk		org_organisation.org_organisation_pk%type			default null
	, p_album_pk			pic_album.pic_album_pk%type							default null
	, p_picture_pk			pic_picture.pic_picture_pk%type						default null
	, p_link				boolean												default true
) is begin
	ext_lob.pri(pic_lib.miniThumbs(p_organisation_pk, p_album_pk, p_picture_pk, p_link));
end;

function miniThumbs
(
	  p_organisation_pk		org_organisation.org_organisation_pk%type			default null
	, p_album_pk			pic_album.pic_album_pk%type							default null
	, p_picture_pk			pic_picture.pic_picture_pk%type						default null
	, p_link				boolean												default true
) return clob
is
begin
declare
	c_procedure				constant mod_procedure.name%type					:= 'miniThumbs';

	v_class					varchar2(16);
	v_directory_path		sys.all_directories.directory_path%type;

	cursor	cur_pic_album is
	select	pic_picture_pk, description, path_filename
	from	pic_album, pic_album_picture, pic_picture
	where	pic_album_pk = pic_album_fk_pk
	and		pic_picture_pk = pic_picture_fk_pk
	and		pic_album.bool_active > 0
	and		pic_picture.bool_active > 0
	and		pic_album_pk = p_album_pk
	order by pic_picture.date_insert;

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	for r_cursor in cur_pic_album
	loop
		v_directory_path	:= org_lib.getDirectoryPath(p_organisation_pk, true);

		if (p_picture_pk = r_cursor.pic_picture_pk) then
			v_class := 'selectedThumb';
		else
			v_class := 'thumbs';
		end if;

		if (p_link) then
			v_html := v_html ||'<a class="'||v_class||'" href="pic_pub.viewPicture?p_organisation_pk='||p_organisation_pk||'&amp;p_picture_pk='||r_cursor.pic_picture_pk||'&amp;p_album_pk='||p_album_pk||'">'||chr(13);
		end if;

		v_html := v_html ||'<img alt="'||r_cursor.description||'" title="'||r_cursor.description||'" src="'||v_directory_path||'/'||pic_lib.getThumb(p_organisation_pk, r_cursor.path_filename)||'" width="50" height="50">'||chr(13);

		if (p_link) then
			v_html := v_html ||'</a>'||chr(13);
		end if;

		ext_lob.add(v_clob, v_html);
	end loop;

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end miniThumbs;


---------------------------------------------------------
-- name:        navigationLinks
-- what:        creates back, forth and index links
-- author:      Frode Klevstul
-- start date:  18.09.2006
---------------------------------------------------------
procedure navigationLinks
(
	  p_organisation_pk		org_organisation.org_organisation_pk%type			default null
	, p_album_pk			pic_album.pic_album_pk%type							default null
	, p_picture_pk			pic_picture.pic_picture_pk%type						default null
) is begin
	ext_lob.pri(pic_lib.navigationLinks(p_organisation_pk, p_album_pk, p_picture_pk));
end;

function navigationLinks
(
	  p_organisation_pk		org_organisation.org_organisation_pk%type			default null
	, p_album_pk			pic_album.pic_album_pk%type							default null
	, p_picture_pk			pic_picture.pic_picture_pk%type						default null
) return clob
is
begin
declare
	c_procedure				constant mod_procedure.name%type					:= 'navigationLinks';

	c_tex_index				constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'index');
	c_tex_previous			constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'previous');
	c_tex_next				constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'next');

	cursor	cur_pic_picture is
	select	pic_picture_pk
	from	pic_picture, pic_album_picture
	where	pic_picture_fk_pk = pic_picture_pk
	and		pic_album_fk_pk = p_album_pk
	and		pic_picture.bool_active > 0
	order by pic_picture.date_insert;

	v_prevImage				pic_picture.pic_picture_pk%type;
	v_nextImage				pic_picture.pic_picture_pk%type;

	v_imageFound			boolean := false;

	v_html					clob;
	v_clob					clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	for r_cursor in cur_pic_picture
	loop

		if (p_picture_pk = r_cursor.pic_picture_pk) then
			v_imageFound := true;
		end if;

		if not (v_imageFound) then
			v_prevImage := r_cursor.pic_picture_pk;
		elsif (p_picture_pk != r_cursor.pic_picture_pk) then
			v_nextImage := r_cursor.pic_picture_pk;
			exit;
		end if;

	end loop;

	v_html := v_html ||'<div class="picNavigationLink">'||chr(13);
	v_html := v_html ||'| <a href="pic_pub.viewAlbum?p_organisation_pk='||p_organisation_pk||'&amp;p_album_pk='||p_album_pk||'">'||chr(13);
	v_html := v_html ||'&nbsp;'||c_tex_index||'&nbsp;'||chr(13);
	v_html := v_html ||'</a> '||chr(13);

	if (v_prevImage is not null) then
		v_html := v_html ||' | <a href="pic_pub.viewPicture?p_organisation_pk='||p_organisation_pk||'&amp;p_picture_pk='||v_prevImage||'&amp;p_album_pk='||p_album_pk||'">'||chr(13);
		v_html := v_html ||'&nbsp;&laquo;&nbsp;'||c_tex_previous||'&nbsp;'||chr(13);
		v_html := v_html ||'</a>'||chr(13);
	end if;

	if (v_nextImage is not null) then
		v_html := v_html ||' | <a href="pic_pub.viewPicture?p_organisation_pk='||p_organisation_pk||'&amp;p_picture_pk='||v_nextImage||'&amp;p_album_pk='||p_album_pk||'">'||chr(13);
		v_html := v_html ||'&nbsp;'||c_tex_next||'&nbsp;&raquo;&nbsp;'||chr(13);
		v_html := v_html ||'</a> |'||chr(13);
	end if;

	if (v_prevImage is null and v_nextImage is null) then
		v_html := v_html ||' |'||chr(13);
	end if;

	v_html := v_html ||'</div>'||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end navigationLinks;


---------------------------------------------------------
-- name:        getThumb
-- what:        returns the name of a picture's thumb
-- author:      Frode Klevstul
-- start date:  27.09.2006
---------------------------------------------------------
procedure getThumb
(
	  p_organisation_pk		org_organisation.org_organisation_pk%type			default null
	, p_filename			pic_picture.path_filename%type						default null
) is begin
	htp.p(pic_lib.navigationLinks(p_organisation_pk, p_filename));
end;

function getThumb
(
	  p_organisation_pk		org_organisation.org_organisation_pk%type			default null
	, p_filename			pic_picture.path_filename%type						default null
) return varchar2
is
begin
declare
	c_procedure				constant mod_procedure.name%type					:= 'getThumb';

	c_upl_pic_formats		constant sys_parameter.value%type 					:= sys_lib.getParameter('upl_pic_formats');

	v_i						number												:=1;
	v_filetype				varchar2(16)										:= '.jpg';
	v_filename				pic_picture.path_filename%type;

begin
	ses_lib.ini(g_package, c_procedure);

	v_filename := regexp_replace(p_filename, '^([[:digit:]]{0,}[[:punct:]]?)('||p_organisation_pk||'_[[:digit:]]{1,}.*)', '\1thumb_\2');

	while v_filetype is not null loop
		v_filetype := ext_lib.split(c_upl_pic_formats, v_i, ' ');
		exit when v_filetype is null;
		v_filename := replace(lower(v_filename), lower('.'||v_filetype), '.jpg');
		v_i := v_i + 1;
	end loop;

	return v_filename;

end;
end getThumb;


---------------------------------------------------------
-- name:        getPrevNext
-- what:        returns previous or next picture in album
-- author:      Frode Klevstul
-- start date:  10.10.2006
---------------------------------------------------------
function getPrevNext
(
	  p_picture_pk			pic_picture.pic_picture_pk%type						default null
	, p_type				varchar2											default null
) return pic_picture.pic_picture_pk%type
is
begin
declare
	c_procedure				constant mod_procedure.name%type					:= 'getPrevNext';

	cursor	cur_pic_picture is
	select	pic_picture_pk
	from	pic_picture, pic_album_picture
	where	pic_picture_fk_pk = pic_picture_pk
	and		pic_album_fk_pk =
		(
			select	pic_album_fk_pk
			from	pic_album_picture
			where	pic_picture_fk_pk = p_picture_pk
		)
	and		pic_picture.bool_active > 0
	order by pic_picture.date_insert;

	v_prevImage				pic_picture.pic_picture_pk%type;
	v_nextImage				pic_picture.pic_picture_pk%type;
	v_returnImage			pic_picture.pic_picture_pk%type;

	v_imageFound			boolean := false;

begin
	ses_lib.ini(g_package, c_procedure);

	for r_cursor in cur_pic_picture
	loop

		if (p_picture_pk = r_cursor.pic_picture_pk) then
			v_imageFound := true;
		end if;

		if not (v_imageFound) then
			v_prevImage := r_cursor.pic_picture_pk;
		elsif (p_picture_pk != r_cursor.pic_picture_pk) then
			v_nextImage := r_cursor.pic_picture_pk;
			exit;
		end if;

	end loop;

	if (p_type = 'previous') then
		v_returnImage := v_prevImage;
	elsif (p_type = 'next') then
		v_returnImage := v_nextImage;
	end if;

	return v_returnImage;

end;
end getPrevNext;


-- ******************************************** --
end;
/
show errors;
