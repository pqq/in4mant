PROMPT *** ext_fee ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package ext_fee is
/* ******************************************************
*	Package:     ext_fee
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	060616 | Frode Klevstul
*			- Package created
****************************************************** */
	procedure run;	
	procedure JS
	(
		  p_place		in varchar2	default 'norway'
		, p_script		in varchar2	default null 
	);
	function JS
	(
		  p_place		in varchar2	default 'norway'
		, p_script		in varchar2	default null 
	) return clob;

end;
/
show errors;





-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body ext_fee is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'ext_fee';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  11.08.2005
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        JS
-- what:        list out content - JS feed style
-- author:      espen messel
-- start date:  22.05.2006
---------------------------------------------------------
procedure JS
(
	  p_place		in varchar2	default 'norway'
	, p_script		in varchar2	default null 
) is begin
	ext_lob.pri(ext_fee.JS(p_place, p_script));
end;

function JS
(
	  p_place		in varchar2	default 'norway'
	, p_script		in varchar2	default null 
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type 	:= 'JS';

	c_dateformat				constant sys_parameter.value%type		:= sys_lib.getparameter('dateformat_ymd');
	cur_dyncursor				sys_refcursor;
	v_sql_stmt					varchar2(4096);
	v_date_start				varchar2(32);
	v_totalcursize				number									:= 10;
	v_con_content_pk			con_content.con_content_pk%type			default null;
	v_subject					con_content.subject%type				default null;
	v_org_organisation_fk		con_content.org_organisation_fk%type	default null;
	v_name						org_organisation.name%type				default null;
	v_geo_name					geo_geography.name%type					default null;
	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

    v_sql_stmt := ''||
    	' select cc.con_content_pk, replace(cc.subject, chr(39),''&#39;'') as subject, to_char(cc.date_start, '''||c_dateformat||''') as date_start, cc.org_organisation_fk, oo.name, gg.name'||
		' from con_content cc'||
		' join org_organisation oo on cc.org_organisation_fk = oo.org_organisation_pk'||
		' left outer join add_address aa on oo.add_address_fk = aa.add_address_pk'||
		' left outer join geo_geography gg on aa.geo_geography_fk = gg.geo_geography_pk'||
		' where cc.con_status_fk = (select con_status_pk from con_status where name =''published'')'||
		' and ( cc.date_start > (sysdate-(60/1440)) or cc.date_stop > sysdate-(60/1440) )'||
		' and ( cc.date_publish < sysdate or cc.date_publish is null)'||
		' and gg.geo_geography_pk in ( '||
		' select distinct g1.geo_geography_pk from geo_geography g1'||
		' where upper(g1.name) = upper('''|| p_place ||''')'||
		' union'||
		' select distinct g2.geo_geography_pk from geo_geography g1'||
		' left outer join geo_geography g2 on g2.parent_geo_geography_fk = g1.geo_geography_pk'||
		' where upper(g1.name) = upper('''|| p_place ||''')'||
		' union'||
		' select distinct g3.geo_geography_pk from geo_geography g1'||
		' left outer join geo_geography g2 on g2.parent_geo_geography_fk = g1.geo_geography_pk'||
		' left outer join geo_geography g3 on g3.parent_geo_geography_fk = g2.geo_geography_pk'||
		' where upper(g1.name) = upper('''|| p_place ||''')'||
		' )'||
		' order by cc.date_start asc'||
		' ';

	--htp.p(v_sql_stmt);

	if ( p_script is not null ) then
		v_html := v_html ||'<script type="text/javascript">';
	end if;

	v_html := v_html ||'
		document.write(''\<a target="new" href="http://www.uteliv.no/"\>\<img src="http://uteliv.no/img/i4/topBanner.jpg" width="400" height="50" border="0"\>\<\/a\>'');
		document.write(''\<table width="400" cellpadding="1" border="0" cellspacing="0" bgcolor="#000000"\>\<tr\>\<td\>'');
		document.write(''\<table width="100%" cellpadding="3" border="0" cellspacing="0" bgcolor="#ffffff"\>'');
	';

	open cur_dyncursor for v_sql_stmt;
	loop
		fetch cur_dyncursor into v_con_content_pk, v_subject, v_date_start, v_org_organisation_fk, v_name, v_geo_name;
		exit when cur_dyncursor%notfound;
		exit when v_totalcursize = 0;
		v_totalcursize := v_totalcursize - 1;

		v_html := v_html ||'document.write(''\<tr\>\<td valign="top" nowrap="nowrap"\>'|| v_date_start ||'\<\/td\>\<td valign="top"\>\<a target="_new" href="http://uteliv.no/i4/con_pub.viewcon?p_content_pk='|| v_con_content_pk ||'"\>'|| v_subject ||'\<\/a\>\<\/td\>\<td valign="top"\>\<a target="_new" href="http://uteliv.no/i4/org_pub.vieworg?p_organisation_pk='|| v_org_organisation_fk ||'">'|| v_name ||'('|| v_geo_name ||')\<\/a\>\<\/td\>\<\/tr\>'');'||chr(13);
      end loop;
      close cur_dyncursor;

	v_html := v_html ||'
		document.write(''\<\/table\>\<\/td\>\<\/tr\>'');
		document.write(''\<\/table\>'');
	';

	if ( p_script is not null ) then
		v_html := v_html ||'</script>';
	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end JS;


-- ******************************************** --
end;
/
show errors;
