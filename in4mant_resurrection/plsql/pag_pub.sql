PROMPT *** pag_pub ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package pag_pub is
/* ******************************************************
*	Package:     pag_pub
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	050724 | Frode Klevstul
*			- Package created
****************************************************** */
	procedure run;
	procedure frontPage;
	function frontPage
		return clob;
	procedure errorPage
	(
		  p_package				in		mod_package.name%type
		, p_procedure			in		mod_procedure.name%type
		, p_tex_text_pk			in		tex_text.tex_text_pk%type
	);
	procedure errorPage
	(
		  p_package				in		mod_package.name%type
		, p_procedure			in		mod_procedure.name%type
		, p_tex_text_pk			in		tex_text.tex_text_pk%type
		, p_bool_critical		in		log_error.bool_critical%type
	);
	procedure errorPage
	(
		  p_package				in		mod_package.name%type
		, p_procedure			in		mod_procedure.name%type
		, p_tex_text_pk			in		tex_text.tex_text_pk%type
		, p_bool_critical		in		log_error.bool_critical%type
		, p_ses_session_id_new	in		log_error.ses_session_id_new%type
	);
	function errorPage
	(
		  p_package				in		mod_package.name%type
		, p_procedure			in		mod_procedure.name%type
		, p_tex_text_pk			in		tex_text.tex_text_pk%type
		, p_bool_critical		in		log_error.bool_critical%type
		, p_ses_session_id_new	in		log_error.ses_session_id_new%type
	) return clob;
	procedure errorPageSimple
	(
		  p_errorString			in		varchar2						default null
	);
	function errorPageSimple
	(
		  p_errorString			in		varchar2						default null
	) return clob;
	procedure owaSqlError
	(
		  proc					in varchar2					default null
		, errcode				in varchar2					default null
		, errmsg				in varchar2					default null
	);
	function owaSqlError
	(
		  proc					in varchar2					default null
		, errcode				in varchar2					default null
		, errmsg				in varchar2					default null
	) return clob;
	procedure displayHiddenValues;
	function displayHiddenValues
		return clob;
	procedure advertisePage;
	function advertisePage
	    return clob;
	procedure partnerPage;
	function partnerPage
	    return clob;
	procedure contactPage;
	function contactPage
	    return clob;
	procedure aboutPage;
	function aboutPage
	    return clob;
	procedure sendLinkPage;
	function sendLinkPage
	    return clob;
	procedure jobsPage;
	function jobsPage
	    return clob;
	procedure admLogin;
	procedure userLogin
	(
		  p_type			in varchar2					default 'normal'
		, p_url				in varchar2					default null
	);
	function userLogin
	(
		  p_type			in varchar2					default 'normal'
		, p_url				in varchar2					default null
	) return clob;

end;
/
show errors;




-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body pag_pub is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'pag_pub';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  22.07.2005
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	pag_pub.frontPage;
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        frontPage
-- what:        display front page
-- author:      Frode Klevstul
-- start date:  24.07.2005
---------------------------------------------------------
procedure frontPage is begin
	ext_lob.pri(pag_pub.frontPage);
end;

function frontPage
	return clob
is
begin
declare
	c_procedure			constant mod_procedure.name%type	:= 'frontPage';

	v_con_type_pk		con_type.con_type_pk%type;

	v_html				clob;
	v_clob				clob;

begin
	ses_lib.ini(g_package, c_procedure, null);
	ext_lob.ini(v_clob);

	v_html := v_html||''||htm.bPage(g_package, c_procedure)||chr(13);
	v_html := v_html||'<center>'||chr(13);


	-- -----------------------------
	-- chosenOrganisation
	-- -----------------------------
	v_html := v_html||''||htm.bTable||chr(13);
	v_html := v_html||'<tr><td class="surroundElements">'||chr(13);
	v_html := v_html||''||org_lib.chosenOrganisation||chr(13);
	v_html := v_html||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);


	-- -----------------------------
	-- events & news
	-- -----------------------------
	v_html := v_html||''||htm.bTable('surroundElements')||chr(13);
	v_html := v_html||'<tr><td class="surroundElements" width="50%">'||chr(13);
	-- events
	select	con_type_pk
	into	v_con_type_pk
	from	con_type
	where	name = 'event_org';

	v_html := v_html||''||
	con_lib.listCon(
		  p_noFirst				=> 10
		, p_noOnOnePage			=> 30
		, p_noShowFrom			=> null
		, p_subjectMaxLength	=> 30
		, p_editMode			=> null
		, p_organisation_pk		=> null
		, p_con_type_pk			=> v_con_type_pk
		, p_con_subtype_pk		=> null
		, p_date_start			=> null
		, p_date_stop			=> null
		, p_fullMode			=> 1
	);

	v_html := v_html||'</td><td class="surroundElements">'||chr(13);

	-- news
	select	con_type_pk
	into	v_con_type_pk
	from	con_type
	where	name = 'news_org';

	v_html := v_html||''||
	con_lib.listCon(
		  p_noFirst				=> 10
		, p_noOnOnePage			=> 30
		, p_noShowFrom			=> null
		, p_subjectMaxLength	=> 30
		, p_editMode			=> null
		, p_organisation_pk		=> null
		, p_con_type_pk			=> v_con_type_pk
		, p_con_subtype_pk		=> null
		, p_date_start			=> null
		, p_date_stop			=> null
		, p_fullMode			=> 1
	);

	v_html := v_html||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);


	-- -----------------------------
	-- pictures
	-- -----------------------------
	v_html := v_html||''||htm.bTable('surroundElements')||chr(13);
	v_html := v_html||'<tr><td class="surroundElements">'||chr(13);

	v_html := v_html||''||pic_lib.latestPictures||chr(13);

	v_html := v_html||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);


	-- -----------------------------
	-- rankOrganisations
	-- -----------------------------
	v_html := v_html||''||htm.bTable('surroundElements')||chr(13);
	v_html := v_html||'	<tr>'||
					  '		<td width="33%" class="surroundElements">'||org_lib.rankOrganisations(10, 'newlyRegistered')||'</td>'||
					  '		<td width="33%" class="surroundElements">'||org_lib.rankOrganisations(10, 'newlyUpdated')||'</td>'||
					  '		<td width="33%" class="surroundElements">'||org_lib.rankOrganisations(10, 'mostPopular')||'</td>'||
					  ' </tr>';
	v_html := v_html||''||htm.eTable||chr(13);


	v_html := v_html||'</center>'||chr(13);
	v_html := v_html||''||htm.ePage||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		return null;
end;
end frontPage;


---------------------------------------------------------
-- name:        errorPage
-- what:        ...
-- author:      Frode Klevstul
-- start date:  26.07.2005
---------------------------------------------------------
procedure errorPage(p_package in mod_package.name%type, p_procedure in mod_procedure.name%type, p_tex_text_pk in tex_text.tex_text_pk%type) is begin
	pag_pub.errorPage(p_package, p_procedure, p_tex_text_pk, 0, null);
end;

procedure errorPage(p_package in mod_package.name%type, p_procedure in mod_procedure.name%type, p_tex_text_pk in tex_text.tex_text_pk%type, p_bool_critical in log_error.bool_critical%type) is begin
	pag_pub.errorPage(p_package, p_procedure, p_tex_text_pk, p_bool_critical, null);
end;

procedure errorPage(p_package in mod_package.name%type, p_procedure in mod_procedure.name%type, p_tex_text_pk in tex_text.tex_text_pk%type, p_bool_critical in log_error.bool_critical%type, p_ses_session_id_new in log_error.ses_session_id_new%type) is begin
	ext_lob.pri(pag_pub.errorPage(p_package, p_procedure, p_tex_text_pk, p_bool_critical, p_ses_session_id_new));
end;

function errorPage
(
	  p_package				in		mod_package.name%type
	, p_procedure			in		mod_procedure.name%type
	, p_tex_text_pk			in		tex_text.tex_text_pk%type
	, p_bool_critical		in		log_error.bool_critical%type
	, p_ses_session_id_new	in		log_error.ses_session_id_new%type
) return clob
is
begin
declare
    c_procedure constant mod_procedure.name%type := 'errorPage';

    v_html clob;
    v_clob clob;

begin
	-- ------------------------------------------------------------
	-- NOTE:
	-- have to call the two param version of ini to prevent infinite
	-- loop (since this is called from ini itself...)
	-- ------------------------------------------------------------
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	-- -------------------
	-- log the error
	-- -------------------
	log_lib.logError(p_package, p_procedure, p_tex_text_pk, p_bool_critical, p_ses_session_id_new);

	v_html := v_html||''||htm.bPage(g_package, c_procedure)||chr(13);
	v_html := v_html||''||log_lib.errorString(p_package, p_procedure, p_tex_text_pk)||chr(13);
	v_html := v_html||''||htm.ePage||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
    when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
        return null;
end;
end errorPage;


---------------------------------------------------------
-- name:        errorPageSimple
-- what:        The error page to show to the public
-- author:      Frode Klevstul
-- start date:  26.07.2005
---------------------------------------------------------
procedure errorPageSimple
(
	  p_errorString			in		varchar2						default null
) is begin
	ext_lob.pri(pag_pub.errorPageSimple(p_errorString));
end;

function errorPageSimple
(
	  p_errorString			in		varchar2						default null
) return clob
is
begin
declare
    c_procedure constant mod_procedure.name%type := 'errorPageSimple';

    v_html clob;
    v_clob clob;

begin
	-- note: only calling with two parameters to avoid cookie error since this
	-- is called through error.php
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := v_html||''||htm.bPage(g_package, c_procedure)||chr(13);
	v_html := v_html||'<center>'||chr(13);

	v_html := v_html||''||htm.bTable||chr(13);
	v_html := v_html||'<tr><td class="surroundElements" style="text-align:center">'||chr(13);
	v_html := v_html||'<h1>SORRY BUD BUT OUR SOMETHING SEEMS TO BE BROKE</h1>THIS ERROR HAS BEEN LOGGED<br />We''ll try to fix it soon(ish)...<br /><br />'||chr(13);
	v_html := v_html||p_errorString||chr(13);
	v_html := v_html||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);

	v_html := v_html||'</center>'||chr(13);
	v_html := v_html||''||htm.ePage||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
    when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
        return null;
end;
end errorPageSimple;


---------------------------------------------------------
-- name:        owaSqlError
-- what:        handling the owaSqlErrors
-- author:      Frode Klevstul
-- start date:  18.10.2006
---------------------------------------------------------
procedure owaSqlError
(
	  proc					in varchar2					default null
	, errcode				in varchar2					default null
	, errmsg				in varchar2					default null
) is begin
	ext_lob.pri(pag_pub.owaSqlError(proc, errcode, errmsg));
end;

function owaSqlError
(
	  proc					in varchar2					default null
	, errcode				in varchar2					default null
	, errmsg				in varchar2					default null
) return clob
is
begin
declare
    c_procedure constant mod_procedure.name%type := 'owaSqlError';

	v_errMsg				varchar2(1024) 				:= errmsg;

    v_html					clob;
    v_clob					clob;

begin
	ses_lib.ini(g_package, c_procedure, 'proc='||proc||'��errcode='||errcode);
	ext_lob.ini(v_clob);

	v_errMsg	:= replace(v_errMsg, 'ORA-', 'IN4-');
	v_errMsg	:= replace(v_errMsg, 'PL/SQL', 'IN4/SQL');
	v_errMsg	:= replace(v_errMsg, 'PLS-', 'IN4S-');

	v_html := v_html||''||htm.bPage(g_package, c_procedure)||chr(13);
	v_html := v_html||
		'<center>
		 <table width="70%">
		 <tr><td colspan="2"><h1>IN4MANT ERROR</h1></td></tr>
		 <tr><td width="10%"><b>proc:</b></td><td>'||proc||'</td></tr>
		 <tr><td><b>errcode:</b></td><td>'||errcode||'<a href="http://ora-'||errcode||'.ora-code.com" target="_blank">&nbsp;</a></td></tr>
		 <tr><td style="vertical-align: top"><b>errmsg:</b></td><td>'||v_errMsg||'</td></tr>
		 </table>
		 </center>'||chr(13);
	v_html := v_html||''||htm.ePage||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
    when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
        return null;
end;
end owaSqlError;


---------------------------------------------------------
-- name:        displayHiddenValues
-- what:        display hidden values on screen (for debugging)
-- author:      Frode Klevstul
-- start date:  25.07.2005
---------------------------------------------------------
procedure displayHiddenValues is begin
	ext_lob.pri(pag_pub.displayHiddenValues);
end;

function displayHiddenValues
	return clob
is
begin
declare
	c_procedure			constant mod_procedure.name%type	:= 'displayHiddenValues';

	c_id				constant ses_session.id%type		:= ses_lib.getId;
	c_use_user_pk		constant use_user.use_user_pk%type	:= ses_lib.getPk(c_id);

	v_html				clob;
	v_clob				clob;

begin
	ses_lib.ini(g_package, c_procedure, null);
	ext_lob.ini(v_clob);

	v_html := v_html||''||htm.bPage(g_package, c_procedure)||chr(13);
	v_html := v_html||'<center>'||chr(13);
	v_html := v_html||''||htm.bTable('surroundElements')||chr(13);
	v_html := v_html||'<tr><td>'||chr(13);

	v_html := v_html||''||htm.bBox('Display Hidden Values')||chr(13);
	v_html := v_html||''||htm.bTable||chr(13);
	v_html := v_html||'<tr><td>IP address:</td><td>'||owa_util.get_cgi_env('REMOTE_ADDR')||'</td></tr>'||chr(13);
	v_html := v_html||'<tr><td>Host address:</td><td>'||owa_util.get_cgi_env('REMOTE_HOST')||'</td></tr>'||chr(13);
	v_html := v_html||'<tr><td>Session ID:</td><td>'||c_id||'</td></tr>'||chr(13);
	v_html := v_html||'<tr><td>User PK:</td><td>'||c_use_user_pk||'</td></tr>'||chr(13);
	v_html := v_html||'<tr><td>Username:</td><td>'||use_lib.getUsername(c_use_user_pk)||'</td></tr>'||chr(13);
	v_html := v_html||'<tr><td>Organisation PK:</td><td>'||ses_lib.getOrgPk(c_id)||'</td></tr>'||chr(13);
	v_html := v_html||'<tr><td>Language:</td><td>'||ses_lib.getLang(c_id)||'</td></tr>'||chr(13);
	v_html := v_html||'<tr><td>Service PK:</td><td>'||ses_lib.getSer(c_id)||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||''||htm.eBox||chr(13);

	v_html := v_html||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||'</center>'||chr(13);
	v_html := v_html||''||htm.ePage||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		return null;
end;
end displayHiddenValues;


---------------------------------------------------------
-- name:        advertisePage
-- what:        page with info about advertising
-- author:      Frode Klevstul
-- start date:  22.11.2005
---------------------------------------------------------
procedure advertisePage is begin
    ext_lob.pri(pag_pub.advertisePage);
end;

function advertisePage
    return clob
is
begin
declare
    c_procedure constant mod_procedure.name%type := 'advertisePage';

	c_tex_advertise		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'advertise');
	c_tex_advertiseTxt	constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'advertiseTxt');

    v_html 				clob;
	v_clob				clob;

begin
    ses_lib.ini(g_package, c_procedure, null);
	ext_lob.ini(v_clob);

    v_html := v_html||''||htm.bPage(g_package, c_procedure)||chr(13);
    v_html := v_html||'<center>'||chr(13);
	v_html := v_html||''||htm.bTable('surroundElements')||chr(13);
	v_html := v_html||'<tr><td>'||chr(13);

    v_html := v_html||''||htm.bBox(c_tex_advertise)||chr(13);
	v_html := v_html||''||htm.bTable||chr(13);
	v_html := v_html||'<tr><td>'||chr(13);
    v_html := v_html||''||c_tex_advertiseTxt||chr(13);
	v_html := v_html||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
    v_html := v_html||''||htm.eBox||chr(13);

	v_html := v_html||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
    v_html := v_html||'</center>'||chr(13);
    v_html := v_html||''||htm.ePage||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
    when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
        return null;
end;
end advertisePage;


---------------------------------------------------------
-- name:        partnerPage
-- what:        info about partners
-- author:      Frode Klevstul
-- start date:  12.12.2006
---------------------------------------------------------
procedure partnerPage is begin
    ext_lob.pri(pag_pub.partnerPage);
end;

function partnerPage
    return clob
is
begin
declare
    c_procedure constant mod_procedure.name%type := 'partnerPage';

	c_tex_partner		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'partner');
	c_tex_partnerTxt	constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'partnerTxt');

    v_html 				clob;
	v_clob				clob;

begin
    ses_lib.ini(g_package, c_procedure, null);
	ext_lob.ini(v_clob);

    v_html := v_html||''||htm.bPage(g_package, c_procedure)||chr(13);
    v_html := v_html||'<center>'||chr(13);
	v_html := v_html||''||htm.bTable('surroundElements')||chr(13);
	v_html := v_html||'<tr><td>'||chr(13);

    v_html := v_html||''||htm.bBox(c_tex_partner)||chr(13);
	v_html := v_html||''||htm.bTable||chr(13);
	v_html := v_html||'<tr><td>'||chr(13);
    v_html := v_html||''||c_tex_partnerTxt||chr(13);
	v_html := v_html||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
    v_html := v_html||''||htm.eBox||chr(13);

	v_html := v_html||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
    v_html := v_html||'</center>'||chr(13);
    v_html := v_html||''||htm.ePage||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
    when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
        return null;
end;
end partnerPage;


---------------------------------------------------------
-- name:        contactPage
-- what:        contact information
-- author:      Frode Klevstul
-- start date:  22.11.2005
---------------------------------------------------------
procedure contactPage is begin
    ext_lob.pri(pag_pub.contactPage);
end;

function contactPage
    return clob
is
begin
declare
    c_procedure constant mod_procedure.name%type := 'contactPage';

	c_tex_contact		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'contact');
	c_tex_contactTxt	constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'contactTxt');

    v_html 				clob;
	v_clob				clob;

begin
    ses_lib.ini(g_package, c_procedure, null);
	ext_lob.ini(v_clob);

	v_html := v_html||''||htm.bPage(g_package, c_procedure)||chr(13);
	v_html := v_html||'<center>'||chr(13);
	v_html := v_html||''||htm.bTable('surroundElements')||chr(13);
	v_html := v_html||'<tr><td>'||chr(13);

	v_html := v_html||''||htm.bBox(c_tex_contact)||chr(13);
	v_html := v_html||''||htm.bTable||chr(13);
	v_html := v_html||'<tr><td>'||chr(13);
	v_html := v_html||''||c_tex_contactTxt||chr(13);
	v_html := v_html||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||''||htm.eBox||chr(13);

	v_html := v_html||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||'</center>'||chr(13);
	v_html := v_html||''||htm.ePage||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
    when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
        return null;
end;
end contactPage;


---------------------------------------------------------
-- name:        aboutPage
-- what:        ...
-- author:      Frode Klevstul
-- start date:  22.11.2005
---------------------------------------------------------
procedure aboutPage is begin
    ext_lob.pri(pag_pub.aboutPage);
end;

function aboutPage
    return clob
is
begin
declare
    c_procedure constant mod_procedure.name%type := 'aboutPage';

	c_tex_about		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'about');
	c_tex_aboutTxt	constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'aboutTxt');

    v_html 			clob;
	v_clob			clob;

begin
    ses_lib.ini(g_package, c_procedure, null);
	ext_lob.ini(v_clob);

    v_html := v_html||''||htm.bPage(g_package, c_procedure)||chr(13);
    v_html := v_html||'<center>'||chr(13);
	v_html := v_html||''||htm.bTable('surroundElements')||chr(13);
	v_html := v_html||'<tr><td>'||chr(13);

    v_html := v_html||''||htm.bBox(c_tex_about)||chr(13);
	v_html := v_html||''||htm.bTable||chr(13);
	v_html := v_html||'<tr><td>'||chr(13);
    v_html := v_html||''||c_tex_aboutTxt||chr(13);
	v_html := v_html||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
    v_html := v_html||''||htm.eBox||chr(13);

	v_html := v_html||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
    v_html := v_html||'</center>'||chr(13);
    v_html := v_html||''||htm.ePage||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
    when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
        return null;
end;
end aboutPage;


---------------------------------------------------------
-- name:        sendLinkPage
-- what:        send a link to a page
-- author:      Frode Klevstul
-- start date:  22.11.2005
---------------------------------------------------------
procedure sendLinkPage is begin
    ext_lob.pri(pag_pub.sendLinkPage);
end;

function sendLinkPage
    return clob
is
begin
declare
    c_procedure constant mod_procedure.name%type := 'sendLinkPage';

	c_tex_sendLink		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'sendLink');
	c_tex_sendLinkTxt	constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'sendLinkTxt');

    v_html				clob;
	v_clob				clob;

begin
    ses_lib.ini(g_package, c_procedure, null);
	ext_lob.ini(v_clob);

    v_html := v_html||''||htm.bPage(g_package, c_procedure)||chr(13);
    v_html := v_html||'<center>'||chr(13);
	v_html := v_html||''||htm.bTable('surroundElements')||chr(13);
	v_html := v_html||'<tr><td>'||chr(13);

    v_html := v_html||''||htm.bBox(c_tex_sendLink)||chr(13);
	v_html := v_html||''||htm.bTable||chr(13);
	v_html := v_html||'<tr><td>'||chr(13);
    v_html := v_html||''||c_tex_sendLinkTxt||chr(13);
	v_html := v_html||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
    v_html := v_html||''||htm.eBox||chr(13);

	v_html := v_html||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
    v_html := v_html||'</center>'||chr(13);
    v_html := v_html||''||htm.ePage||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
    when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
        return null;
end;
end sendLinkPage;


---------------------------------------------------------
-- name:        jobsPage
-- what:        info about available jobs
-- author:      Frode Klevstul
-- start date:  12.12.2006
---------------------------------------------------------
procedure jobsPage is begin
    ext_lob.pri(pag_pub.jobsPage);
end;

function jobsPage
    return clob
is
begin
declare
    c_procedure constant mod_procedure.name%type := 'jobsPage';

	c_tex_jobs		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'jobs');
	c_tex_jobsTxt	constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'jobsTxt');

    v_html 				clob;
	v_clob				clob;

begin
    ses_lib.ini(g_package, c_procedure, null);
	ext_lob.ini(v_clob);

    v_html := v_html||''||htm.bPage(g_package, c_procedure)||chr(13);
    v_html := v_html||'<center>'||chr(13);
	v_html := v_html||''||htm.bTable('surroundElements')||chr(13);
	v_html := v_html||'<tr><td>'||chr(13);

    v_html := v_html||''||htm.bBox(c_tex_jobs)||chr(13);
	v_html := v_html||''||htm.bTable||chr(13);
	v_html := v_html||'<tr><td>'||chr(13);
    v_html := v_html||''||c_tex_jobsTxt||chr(13);
	v_html := v_html||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
    v_html := v_html||''||htm.eBox||chr(13);

	v_html := v_html||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
    v_html := v_html||'</center>'||chr(13);
    v_html := v_html||''||htm.ePage||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
    when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
        return null;
end;
end jobsPage;


---------------------------------------------------------
-- name:        admLogin
-- what:        Login page for administrators
-- author:      Frode Klevstul
-- start date:  21.02.2006
---------------------------------------------------------
procedure admLogin
 is begin
    ext_lob.pri(pag_pub.userLogin('admin'));
end;


---------------------------------------------------------
-- name:        userLogin
-- what:        Login page
-- author:      Frode Klevstul
-- start date:  20.02.2006
---------------------------------------------------------
procedure userLogin
(
	  p_type			in varchar2					default 'normal'
	, p_url				in varchar2					default null
) is begin
    ext_lob.pri(pag_pub.userLogin(p_type, p_url));
end;

function userLogin
(
	  p_type			in varchar2					default 'normal'
	, p_url				in varchar2					default null
) return clob
is
begin
declare
    c_procedure constant mod_procedure.name%type := 'userLogin';

	c_admlogin_package			constant sys_parameter.value%type	:= sys_lib.getParameter('admlogin_package');
	c_login_package				constant sys_parameter.value%type	:= sys_lib.getParameter('login_package');

	v_url						sys_parameter.value%type;

    v_html						clob;
	v_clob						clob;

begin
    ses_lib.ini(g_package, c_procedure, 'p_type='||p_type||'��p_url='||p_url);
	ext_lob.ini(v_clob);

    if (p_url is null) then
	    if (p_type = 'admin') then
	    	v_url := c_admlogin_package;
	    else
			v_url := c_login_package;
	    end if;
	else
		v_url := p_url;
	end if;

	v_html := use_pub.pleaseLogin(p_url => v_url);

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
    when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
        return null;
end;
end userLogin;

-- ******************************************** --
end;
/
show errors;
