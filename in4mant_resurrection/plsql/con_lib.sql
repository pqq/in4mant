PROMPT *** con_lib ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package con_lib is
/* ******************************************************
*	Package:     con_lib
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	060117 | Frode Klevstul
*			- Package created
****************************************************** */
	type parameter_arr 			is table of varchar2(4000) index by binary_integer;
	empty_array 				parameter_arr;

	procedure run;
	procedure viewCon
	(
		  p_content_pk			in con_content.con_content_pk%type				default null
	);
	function viewCon
	(
		  p_content_pk			in con_content.con_content_pk%type				default null
	) return clob;
	procedure listCon
	(
		  p_noFirst				in number										default null
		, p_noOnOnePage			in number										default null
		, p_noShowFrom			in number										default null
		, p_subjectMaxLength	in number										default null
		, p_editMode			in number										default null
		, p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
		, p_con_type_pk			in con_type.con_type_pk%type					default null
		, p_con_subtype_pk		in con_subtype.con_subtype_pk%type				default null
		, p_date_start			in varchar2										default null
		, p_date_stop			in varchar2										default null
		, p_fullMode			in varchar2										default null
	);
	function listCon
	(
		  p_noFirst				in number										default null
		, p_noOnOnePage			in number										default null
		, p_noShowFrom			in number										default null
		, p_subjectMaxLength	in number										default null
		, p_editMode			in number										default null
		, p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
		, p_con_type_pk			in con_type.con_type_pk%type					default null
		, p_con_subtype_pk		in con_subtype.con_subtype_pk%type				default null
		, p_date_start			in varchar2										default null
		, p_date_stop			in varchar2										default null
		, p_fullMode			in varchar2										default null
	) return clob;

end;
/
show errors;





-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body con_lib is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'con_lib';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  11.08.2005
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        viewCon
-- what:        view content page
-- author:      Frode Klevstul
-- start date:  17.01.2006
---------------------------------------------------------
procedure viewCon(p_content_pk in con_content.con_content_pk%type default null) is begin
	ext_lob.pri(con_lib.viewCon(p_content_pk));
end;

function viewCon
(
	  p_content_pk			in con_content.con_content_pk%type				default null
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type			:= 'viewCon';

	c_dateFormat_full			constant sys_parameter.value%type 			:= sys_lib.getParameter('dateFormat_full');

	c_tex_orgPage				constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'orgPage');
	c_tex_relatedContent		constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'relatedContent');
	c_tex_externalLinks			constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'externalLinks');
	c_tex_published				constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'published');

	v_directory_path			sys.all_directories.directory_path%type;

	cursor	cur_con_content is
	select	con_content_pk, subject, ingress, body, footer, date_start, org_organisation_fk, org_organisation.name as name, path_logo, con_type.name as cname
	from	con_content, org_organisation, con_type
	where	org_organisation_pk = org_organisation_fk
	and		con_type_fk = con_type_pk
	and		con_content_pk = p_content_pk;

	cursor	cur_con_file is
	select	path
	from	con_file, con_content_file
	where	con_file_pk = con_file_fk_pk
	and		con_content_fk_pk = p_content_pk
	and		con_file_type_fk = (select con_file_type_pk from con_file_type where lower(name) = 'jpg');

	cursor	cur_con_url is
	select	url, name
	from	con_url
	where	con_content_fk = p_content_pk
	order by name;

	cursor	cur_con_relation is
	select	con_content_pk, subject
	from	con_relation, con_content
	where	con_content_pk = con_content_2_fk_pk
	and		con_content_1_fk_pk = p_content_pk;

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	if (p_content_pk is not null) then

		for r_cursor in cur_con_content
		loop
			v_directory_path := org_lib.getDirectoryPath(r_cursor.org_organisation_fk, true);
			
			v_html := v_html||''||htm.bTable('normalTable')||chr(13);
			v_html := v_html||'<tr><td width="180" class="surroundElements">'||chr(13);
			v_html := v_html||''||org_lib.contactInfo(r_cursor.org_organisation_fk)||chr(13);
			v_html := v_html||'</td><td class="surroundElements">'||chr(13);
			v_html := v_html||''||htm.bBox(r_cursor.subject||' @ '||r_cursor.name)||chr(13);
			v_html := v_html||''||htm.bTable||chr(13);
			for r_cursor2 in cur_con_file
			loop
				v_html := v_html||''||'<tr><td><img src="'||v_directory_path||'/'||r_cursor2.path||'" alt=""/></td></tr>'||chr(13);
			end loop;
			
			if (r_cursor.cname like 'news%') then
				v_html := v_html||''||'
					<tr>
						<td>
							<hr noshade size="1">
							'||c_tex_published||' '||to_char(r_cursor.date_start, c_dateFormat_full)||'
							<hr noshade size="1">
						</td>
					</tr>
				';
			else
				v_html := v_html||''||'
					<tr>
						<td>
							<hr noshade size="1">
							<span class="conSubHeading">'||r_cursor.name||' : '||to_char(r_cursor.date_start, c_dateFormat_full)||'</span>
							<hr noshade size="1">
						</td>
					</tr>
				';
			end if;

			v_html := v_html||''||'
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td class="conTitleHeading">
					<b>'||r_cursor.subject||'</b>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				';
			v_html := v_html||''||'<tr><td class="conIngressHeading">'||r_cursor.ingress||'</td></tr>'||chr(13);
			v_html := v_html||''||'<tr><td>&nbsp;</td></tr>'||chr(13);
			v_html := v_html||''||'<tr><td>'||r_cursor.body||'</td></tr>'||chr(13);
			v_html := v_html||''||'<tr><td>&nbsp;</td></tr>'||chr(13);
			v_html := v_html||''||'<tr><td class="conFooterHeading">'||r_cursor.footer||'</td></tr>'||chr(13);

			-- ---------------------
			-- related content
			-- ---------------------
			for r_cursor3 in cur_con_relation
			loop
				if (cur_con_relation%rowcount = 1) then
					v_html := v_html||''||'<tr><td>&nbsp;</td></tr>'||chr(13);
					v_html := v_html||''||'<tr><td><hr noshade size="1"></td></tr>'||chr(13);
					v_html := v_html||''||'<tr><td class="conRelatedConHeading">'||chr(13);
					v_html := v_html||''||''||c_tex_relatedContent||chr(13);
					v_html := v_html||' | '||chr(13);
				end if;
				v_html := v_html||'<a href="con_pub.viewCon?p_content_pk='||r_cursor3.con_content_pk||'" target="_blank">'||r_cursor3.subject||'</a> | '||chr(13);
			end loop;
			v_html := v_html||''||'&nbsp;</td></tr>'||chr(13);

			-- ---------------------
			-- URLs
			-- ---------------------
			for r_cursor2 in cur_con_url
			loop
				if (cur_con_url%rowcount = 1) then
					v_html := v_html||''||'<tr><td class="conLinksHeading">'||chr(13);
					v_html := v_html||''||''||c_tex_externalLinks||chr(13);
					v_html := v_html||' | '||chr(13);
				end if;
				v_html := v_html||'<a href="http://'||r_cursor2.url||'" target="_blank">'||r_cursor2.name||'</a> | '||chr(13);
			end loop;
			v_html := v_html||''||'&nbsp;</td></tr>'||chr(13);
			v_html := v_html||''||'<tr><td><hr noshade size="1"></td></tr>'||chr(13);

			v_html := v_html||''||'
				<tr>
					<td style="text-align: center;">
						<!--[subscribe] | -->
						[<a href="org_pub.viewOrg?p_organisation_pk='||r_cursor.org_organisation_fk||'"> '||c_tex_orgPage||' </a>]
						<!-- | [print] |
						[join]-->
					</td>
				</tr>
			';
			v_html := v_html||''||htm.eTable||chr(13);
			v_html := v_html||''||htm.eBox||chr(13);

			v_html := v_html||'</td></tr>'||chr(13);
			v_html := v_html||''||htm.eTable||chr(13);

		end loop;

	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end viewCon;


---------------------------------------------------------
-- name:        listCon
-- what:        list out content
-- author:      Frode Klevstul
-- start date:  20.01.2006
---------------------------------------------------------
procedure listCon(
	  p_noFirst				in number										default null
	, p_noOnOnePage			in number										default null
	, p_noShowFrom			in number										default null
	, p_subjectMaxLength	in number										default null
	, p_editMode			in number										default null
	, p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	, p_con_type_pk			in con_type.con_type_pk%type					default null
	, p_con_subtype_pk		in con_subtype.con_subtype_pk%type				default null
	, p_date_start			in varchar2										default null
	, p_date_stop			in varchar2										default null
	, p_fullMode			in varchar2										default null
) is begin
	ext_lob.pri(con_lib.listCon(p_noFirst, p_noOnOnePage, p_noShowFrom, p_subjectMaxLength, p_editMode, p_organisation_pk, p_con_type_pk, p_con_subtype_pk, p_date_start, p_date_stop, p_fullMode));
end;

function listCon
(
	  p_noFirst				in number										default null	-- how many to show the first time
	, p_noOnOnePage			in number										default null	-- how many to show on a page (except for the first time)
	, p_noShowFrom			in number										default null	-- where to start and show from
	, p_subjectMaxLength	in number										default null	-- max length of subject on first page
	, p_editMode			in number										default null	-- null = no edit mode, 1 = normal, 2 = full
	, p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	, p_con_type_pk			in con_type.con_type_pk%type					default null
	, p_con_subtype_pk		in con_subtype.con_subtype_pk%type				default null
	, p_date_start			in varchar2										default null
	, p_date_stop			in varchar2										default null
	, p_fullMode			in varchar2										default null	-- if full_mode then we'll show name and place of org as well
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type			:= 'listCon';

	c_id						constant ses_session.id%type 				:= ses_lib.getId;
	c_service_pk				constant ser_service.ser_service_pk%type	:= ses_lib.getSer(c_id);

	c_dateFormat				constant sys_parameter.value%type 			:= sys_lib.getParameter('dateFormat_ymd');

	c_tex_delete				constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'delete');
	c_tex_update				constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'update');
	c_tex_hits					constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'hits');
	c_tex_showing				constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'showing');

	cur_dynCursor				sys_refcursor;
	v_array						parameter_arr								default empty_array;
	v_con_type_name				con_type.name%type;
	v_first						boolean										:= false;
	v_hasEntry					boolean										:= false;
	v_showExpired				boolean										:= true;
	v_limitReached				boolean										:= false;
	v_jumpNext					boolean										:= false;
	v_boxWidth					varchar2(8);
	v_boxHeight					varchar2(8);
	v_href						varchar2(128);
	v_href_tmp					varchar2(128);
	v_parameters				varchar2(128);
	v_parametersTmp				varchar2(128);
	v_subjectMaxLength			number;
	v_count						number;
	v_limit						number;
	v_limitTmp					number;
	v_totalCurSize				number										:= 0;
	v_noPrevious				number;
	v_noNext					number;
	v_noNextStop				number;
	v_noShowFrom				number;
	v_noShowTo					number;
	v_fullModeString			varchar2(128);
	v_con_status_name			con_status.name%type;
	v_i							number										:= 0;

	v_sql_stmt					varchar2(2048);
	v_html						clob;
	v_clob						clob;

begin
	-- Note: this function can work as a page on it's own (but should always be called through the wrapper in con_pub)
	-- , or as a module in another page
	ses_lib.ini(g_package, c_procedure);

	-- -----------------------
	-- is this first page?
	-- -----------------------
	if (p_noFirst is not null) then
		v_first := true;
	end if;

	ext_lob.ini(v_clob);

	-- ---------------------------
	-- set layout on page
	-- ---------------------------
	if (not v_first) then
		v_html := v_html||''||htm.bPage(g_package, c_procedure)||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundElements')||chr(13);
		v_html := v_html||'<tr><td>'||chr(13);

		v_boxWidth := '90%';
		v_boxHeight	:= '100%';
	else
		v_boxWidth	:= '100%';
		v_boxHeight	:= '100%';
	end if;

	-- ---------------------------
	-- set maxlength of subject
	-- ---------------------------
	if (v_first) then
		if (p_subjectMaxLength is null) then
			v_subjectMaxLength := 10;
		else
			v_subjectMaxLength := p_subjectMaxLength;
		end if;
	else
		v_subjectMaxLength := 128;
	end if;

	-- ----------------------
	-- set preview link
	-- ----------------------
	v_href := 'con_pub.viewCon?p_content_pk=[p_content_pk]';

	-- -------------------------------
	-- show expired content or not
	-- -------------------------------
	if (p_con_type_pk is not null) then
		select	count(*)
		into	v_count
		from	con_type
		where	con_type_pk = p_con_type_pk
		and		name like 'event_%';

		-- don't show exipred events
		if (v_count > 0) then
			v_showExpired := false;
		else
			v_showExpired := true;
		end if;

	end if;

	-- ------------------------
	-- set limit
	-- ------------------------
	if (v_first) then
		if (p_noFirst is not null and p_noFirst != 0) then
			v_limit := p_noFirst;
		else
			v_limit := 100;
		end if;
	else
		if (p_noOnOnePage is not null) then
			v_limit := p_noOnOnePage;
		else
			v_limit := 100;
		end if;
	end if;
	v_limitTmp := v_limit;

	-- --------------------------------------------------------------------------------------
	-- if we don't have a show from value, or the value is less than 1, we set it to be 1
	-- --------------------------------------------------------------------------------------
	if (p_noShowFrom is null or p_noShowFrom < 1) then
		v_noShowFrom := 1;
	else
		v_noShowFrom := p_noShowFrom;
	end if;

	-- --------------
	-- build SQL ++
	-- --------------
	v_sql_stmt := ' select '||
				' 		con_content_pk'||
				'		, subject '||
				'		, to_char(date_start, '''||c_dateFormat||''') as date_start2'||
				'		, to_char(date_stop, '''||c_dateFormat||''') as date_stop2'||
				'		, to_char(date_publish, '''||c_dateFormat||''') as date_publish2'||
				'		, to_char(date_update, '''||c_dateFormat||''') as date_update2'||
				'		, org_organisation_fk'||
				'		, con_status_fk'||
				'	from con_content';

	-- show content with preview mode when p_editMode = 2
	if (p_editMode = 2) then
		v_sql_stmt := v_sql_stmt||' where con_status_fk in (select con_status_pk from con_status where name in(''published'', ''preview''))';
	else
		v_sql_stmt := v_sql_stmt||' where con_status_fk = (select con_status_pk from con_status where name =''published'')';
	end if;

	-- show expired events?
	if (not v_showExpired) then
		-- show expired events for 60 minutes after they have started
		-- or untill one hour after they have stopped
		v_sql_stmt := v_sql_stmt||' and ( date_start > (sysdate-(60/1440)) or date_stop > sysdate-(60/1440) )';
	end if;

	-- if not in edit mode we won't show news that has not been published
	if (p_editMode is null) then
		v_sql_stmt := v_sql_stmt||' and (date_publish < sysdate or date_publish is null)';
	end if;

	if (p_organisation_pk is not null) then
		v_sql_stmt := v_sql_stmt||' and org_organisation_fk='||p_organisation_pk;
	end if;

	if (p_con_type_pk is not null) then
		v_sql_stmt := v_sql_stmt||' and con_type_fk='||p_con_type_pk;

		select	name
		into	v_con_type_name
		from	con_type
		where	con_type_pk = p_con_type_pk;
	end if;

	-- show content on the right service
	if (c_service_pk is not null) then
		v_sql_stmt := v_sql_stmt||' and ( con_content_pk in (select con_content_fk_pk from con_ser where ser_service_fk_pk = '||c_service_pk||')';
		v_sql_stmt := v_sql_stmt||' or con_content_pk not in (select con_content_fk_pk from con_ser) )';
	end if;

	-- ---------------------------------------------------------------
	-- events are sorted differently from other types of content
	-- ---------------------------------------------------------------
	if (v_con_type_name = 'event_org') then
		v_sql_stmt := v_sql_stmt||' order by date_start asc';
	else
		v_sql_stmt := v_sql_stmt||' order by date_start desc';
	end if;

	-- -------------------------
	-- initialize array
	-- -------------------------
	v_array(1) := '';
	v_array(2) := '';
	v_array(3) := '';
	v_array(4) := '';
	v_array(5) := '';
	v_array(6) := '';
	v_array(7) := '';
	v_array(8) := '';

	-- ----------------------------
	-- find totalt cursor size
	-- ----------------------------
	open cur_dynCursor for v_sql_stmt;
	loop
		fetch cur_dynCursor into v_array(1), v_array(2), v_array(3), v_array(4), v_array(5), v_array(6), v_array(7), v_array(8);
		exit when cur_dynCursor%notfound;
		v_totalCurSize := v_totalCurSize + 1;
	end loop;
	close cur_dynCursor;

	-- -------------------
	-- run the cursor
	-- -------------------
	open cur_dynCursor for v_sql_stmt;
	loop
		if (cur_dynCursor%rowcount < v_noShowFrom-1) then
			v_jumpNext	:= true;
			-- --------------------------------------------------------------------
			-- we can't mess around with the limit so we use a tmp limit value
			-- we add 1 to the limit for each entry we skip to avoid jumping out
			-- of the loop too early
			-- --------------------------------------------------------------------
			v_limitTmp := v_limitTmp + 1;
		else
			v_jumpNext := false;
		end if;

		fetch cur_dynCursor into v_array(1), v_array(2), v_array(3), v_array(4), v_array(5), v_array(6), v_array(7), v_array(8);

		-- ----------------------------------
		-- first row and last row logic
		-- ----------------------------------
		-- first row
		if (cur_dynCursor%rowcount=1 and cur_dynCursor%found) then
			if (p_editMode = 1) then
				v_html := v_html||htm.bHighlight('con_adm.conOverview?p_con_type_pk='||p_con_type_pk, true)||''||chr(13);
				v_html := v_html||''||htm.bTable('fullHeightTable')||chr(13);
				v_html := v_html||'<tr><td>'||chr(13);
			end if;

			v_html := v_html||''||htm.bBox(tex_lib.get('db', 'con_type', v_con_type_name))||chr(13);
			v_html := v_html||''||htm.bTable||chr(13);
			v_hasEntry := true;
		end if;

		-- last row
		if ((cur_dynCursor%notfound and cur_dynCursor%rowcount > 0) or (cur_dynCursor%rowcount > v_limitTmp)) then
			-- check if we have reached the limit
			if (cur_dynCursor%rowcount > v_limit) then
				v_limitReached := true;
				exit;
			end if;
		end if;

		-- ---------------------------------------
		-- exit (has to be done after last row)
		-- ---------------------------------------
		exit when cur_dynCursor%notfound;

		-- -------------------------------------
		-- all other rows but first and last
		-- -------------------------------------
		if (not v_jumpNext) then
			-- set correct link
			v_href_tmp := replace(v_href, '[p_content_pk]', v_array(1));

			if (p_editMode > 0) then
				v_href_tmp := replace(v_href_tmp, 'con_pub.viewCon', 'con_adm.admContent');
				v_href_tmp := v_href_tmp ||'&amp;p_action=preview';
			end if;

			-- set max length on subject
			if (length(v_array(2)) > v_subjectMaxLength) then
				v_array(2) := ext_lib.abbreviation(v_array(2), v_subjectMaxLength);
			end if;

			if (p_fullMode is not null) then
				v_fullModeString := '<a href="org_pub.viewOrg?p_organisation_pk='||v_array(7)||'">'||org_lib.getOrgName(v_array(7))||'</a> ('||geo_lib.getCity(org_lib.getOrgZip(v_array(7)))||')';
			end if;

			if ( mod(v_i, 2) = 0 and p_editMode = 2) then
				v_html := v_html||'<tr class="alternativeLine">'||chr(13);
			else
				v_html := v_html||'<tr>'||chr(13);
			end if;

			v_html := v_html||
						'	<td width="10%" nowrap="nowrap">'||v_array(3)||' :</td>
							<td><b><a href="'||v_href_tmp||'">'||v_array(2)||'</a></b></td>
							<td width="10%" nowrap="nowrap">'||v_fullModeString||'&nbsp;</td>
						';
			if (p_editMode = 2) then

				select	name
				into	v_con_status_name
				from	con_status
				where	con_status_pk = v_array(8);

				v_html := v_html||'
						<td>'||tex_lib.get('db', 'con_status', v_con_status_name)||'</td>
						<td>'||htm.link(c_tex_update, 'con_adm.admContent?p_action=form&amp;p_content_pk='||v_array(1),'_self', 'theButtonStyle')||'</td>
						<td>'||htm.link(c_tex_delete, 'con_adm.admContent?p_action=delete&amp;p_content_pk='||v_array(1),'_self', 'theButtonStyle')||'&nbsp;&nbsp;&nbsp;</td>
					';
			end if;

			v_html := v_html||'</tr>'||chr(13);
		end if;

		v_i := v_i + 1;
	end loop;
	close cur_dynCursor;

	-- ----------------------
	-- add view more link
	-- ----------------------
	if (v_limitReached) then

		-- -----------------
		-- fix parameters
		-- -----------------
		if (p_noOnOnePage is not null) then
			v_parameters := v_parameters ||'p_noOnOnePage='||p_noOnOnePage||'&amp;';
		end if;

		if (not v_first) then
			v_parameters := v_parameters ||'p_noShowFrom=[p_noShowFrom]&amp;';
		end if;

		if (p_editMode is not null) then
			v_parameters := v_parameters ||'p_editMode='||p_editMode||'&amp;';
		end if;

		if (p_organisation_pk is not null) then
			v_parameters := v_parameters ||'p_organisation_pk='||p_organisation_pk||'&amp;';
		end if;

		if (p_con_type_pk is not null) then
			v_parameters := v_parameters ||'p_con_type_pk='||p_con_type_pk||'&amp;';
		end if;

		if (p_con_subtype_pk is not null) then
			v_parameters := v_parameters ||'p_con_subtype_pk='||p_con_subtype_pk||'&amp;';
		end if;

		if (p_date_start is not null) then
			v_parameters := v_parameters ||'p_date_start='||p_date_start||'&amp;';
		end if;

		if (p_date_stop is not null) then
			v_parameters := v_parameters ||'p_date_stop='||p_date_stop||'&amp;';
		end if;

		if (p_fullMode is not null) then
			v_parameters := v_parameters ||'p_fullMode=1&amp;';
		end if;

		-- --------------
		-- fix links
		-- --------------
		-- first page / show some (limited view, as part of another page)
		if (v_first) then
			v_html := v_html||'
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr>
					<td colspan="3" class="buttonInBox">
				';

			-- do not make the link clickable if in Edit Mode
			if (p_editMode is null) then
				v_html := v_html||''||htm.link(tex_lib.get(g_package, c_procedure,'showMore{'||v_con_type_name||'}'),'con_pub.listCon?'||v_parameters,'_self', 'theButtonStyle')||chr(13);
			else
				v_html := v_html||'| '||tex_lib.get(g_package, c_procedure,'showMore{'||v_con_type_name||'}')||' |'||chr(13);
			end if;

			v_html := v_html||'
					</td>
				</tr>
			';

		-- we're displaying a full page with content, so we need back and forth links
		else
			-- set different values
			v_noPrevious 	:= v_noShowFrom - v_limit;
			v_noNext		:= v_noShowFrom + v_limit;
			v_noNextStop	:= v_noNext + v_limit - 1;
			v_noShowTo		:= v_noNext - 1;

			-- if we want to start on something larger than
			-- the total cursor size we're all done, so we
			-- adjust the next number to be same as the size
			if (v_noShowTo > v_totalCurSize) then
				v_noShowTo := v_totalCurSize;
			end if;

			-- stop can never be larger than the total size
			if (v_noNextStop > v_totalCurSize) then
				v_noNextStop := v_totalCurSize;
			end if;

			v_html := v_html||'
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr>
					<td colspan="3" style="text-align: center;">
			';

			-- if we're not showing the first entries we need a "show previous" link
			if (v_noShowFrom > 1) then
				v_parametersTmp := replace(v_parameters, '[p_noShowFrom]', v_noPrevious);
				v_html := v_html||''||htm.link('<< '||v_noPrevious||'-'||(v_noShowFrom-1),'con_pub.listCon?'||v_parametersTmp,'_self" class="buttonStyle')||'<span style="padding-right: 40px;"></span>';
			end if;

			-- showing now
			v_html := v_html||''||v_totalCurSize||' '||c_tex_hits||' ('||c_tex_showing||' '||(v_noShowFrom)||'-'||v_noShowTo||')<span style="padding-right: 40px;"></span>';

			-- view "show next" link if there are more entries
			-- if there are f.ex 15 entries we also have to show number 15, so we have to
			-- add 1 to the total size in the if check
			if (v_noNext < v_totalCurSize+1) then
				v_parametersTmp := replace(v_parameters, '[p_noShowFrom]', v_noNext);
				v_html := v_html||''||htm.link( v_noNext||'-'||v_noNextStop||' >>','con_pub.listCon?'||v_parametersTmp,'_self" class="buttonStyle');
			end if;

			v_html := v_html||'
					</td>
				</tr>
			';

		end if;
	end if;

	-- ------------------------
	-- end box layout
	-- ------------------------
	if (v_hasEntry) then
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);

		if (p_editMode = 1) then
			v_html := v_html||'</td></tr>'||chr(13);
			v_html := v_html||''||htm.eTable;
			v_html := v_html||''||htm.eHighlight||chr(13);
		end if;
	end if;

	-- ---------------------------
	-- set layout on page
	-- ---------------------------
	if (not v_first) then
		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);
	end if;

	-- -----------------------------
	-- show add new content button
	-- -----------------------------
	-- if p_noFirst is 0 we still write out bPage / ePage code etc (it's still first page)
	-- but we don't want the add new content button (this is so we can call this from con_adm.conOverview)
	if (v_first and p_editMode > 0 and not v_hasEntry and p_noFirst > 0) then
		v_html := v_html||''||htm.bBox(tex_lib.get('db', 'con_type', v_con_type_name))||chr(13);
		v_html := v_html||htm.bHighlight('con_adm.conOverview?p_con_type_pk='||p_con_type_pk, true)||''||chr(13);
		v_html := v_html||''||htm.bTable('fullHeightTable')||chr(13);
		v_html := v_html||'<tr><td class="centerMiddle">'||chr(13);
		v_html := v_html||'<span class="adminContentButton">'||tex_lib.get(g_package, c_procedure, 'add_'||v_con_type_name)||'</span><br /><br />'||chr(13);
		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eHighlight||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);
	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end listCon;


-- ******************************************** --
end;
/
show errors;
