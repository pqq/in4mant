PROMPT *** triggers ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





/* ******************************************************
*	script:     triggers.sql
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	060426 | Frode Klevstul
*			- Created
****************************************************** */





---------------------------------------------------------
-- name:        org_organisation_before_insert
-- what:        before insert on org_organisation
-- author:      Frode Klevstul
-- start date:  26.04.2006
---------------------------------------------------------
create or replace trigger org_organisation_before_insert
before insert
	on org_organisation
	for each row

declare
	e_fakeParent	exception;
	e_childIsParent	exception;
	v_count			number;

begin

	if (:new.parent_org_organisation_fk is not null) then

		-- ------------------------------------------------------
		-- check that parent_fk (if any) is related to a real
		-- parent in the org table
		-- ------------------------------------------------------
		select	count(*)
		into	v_count
		from	org_organisation
		where	org_organisation_pk = :new.parent_org_organisation_fk
		and		bool_umbrella_organisation > 0;

		if (v_count = 0) then
			raise e_fakeParent;
		end if;

		-- ------------------------------------------------------
		-- check that this new entry is not a parent if it has
		-- a relation to a parent as well (a parent can not have
		-- a parent)
		-- ------------------------------------------------------
		if (:new.bool_umbrella_organisation > 0) then
			raise e_childIsParent;
		end if;

	end if;

exception
	when e_fakeParent then
		raise_application_error(-20000, 'parent_org_organisation_fk pointing to a none parent');
	when e_childIsParent then
		raise_application_error(-20000, 'parent can not have another parent');

end org_organisation_before_insert;
/





---------------------------------------------------------
-- name:        org_organisation_before_update
-- what:        before update on org_organisation
-- author:      Frode Klevstul
-- start date:  26.04.2006
---------------------------------------------------------
create or replace trigger org_organisation_before_update
before update
	on org_organisation
	for each row

declare
	e_fakeParent			exception;
	e_childIsParent			exception;
	e_canNotChangeAddress	exception;
	v_count					number;

begin

	-- ------------------------------------------------------
	-- if this has a parent it can't be a parent...
	-- ------------------------------------------------------
	if (
			:new.parent_org_organisation_fk is not null
		and	:new.bool_umbrella_organisation > 0
	) then
		raise e_childIsParent;
	end if;

	-- ------------------------------------------------------
	-- check that parent_fk (if any) is related to a real
	-- parent in the org table
	-- ------------------------------------------------------
	if (:new.parent_org_organisation_fk = :new.org_organisation_pk) then
		raise e_fakeParent;
	end if;

	-- --------------------------------------------------------------------------
	-- no way to check this due to
	-- ORA-04091: table XXXX is mutating, trigger/function may not see it
	-- 
	-- for a more complex solution, check:
	-- http://asktom.oracle.com/~tkyte/Mutate/index.html
	-- --------------------------------------------------------------------------
	-- select	count(*)
	-- into		v_count
	-- from		org_organisation
	-- where	org_organisation_pk = :new.parent_org_organisation_fk
	-- and		bool_umbrella_organisation > 0;

	-- if (v_count = 0) then
	--		raise e_fakeParent;
	-- end if;

	-- ------------------------------------------------------
	-- prevent changing the address fk
	-- ------------------------------------------------------
	if (:new.add_address_fk <> :old.add_address_fk) then
		raise e_canNotChangeAddress;
	end if;

exception
	when e_fakeParent then
		raise_application_error(-20000, 'parent_org_organisation_fk pointing to a none parent');
	when e_childIsParent then
		raise_application_error(-20000, 'parent can not have another parent');
	when e_canNotChangeAddress then
		raise_application_error(-20000, 'an address fk can not be changed');

end org_organisation_before_update;
/




-- -------------------------------------------------------
-- show errors
-- -------------------------------------------------------
show errors;
