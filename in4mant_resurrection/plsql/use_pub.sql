PROMPT *** use_pub ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package use_pub is
/* ******************************************************
*	Package:     use_pub
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	050723 | Frode Klevstul
*			- Package created
****************************************************** */
	procedure run;
	procedure login;
	procedure login
	(
		p_url			in varchar2
	);
	procedure login
	(
		  p_url 		in varchar2
		, p_username 	in use_user.username%type
		, p_password 	in use_user.password%type
	);
	function login
	(
		  p_url			in varchar2
		, p_username	in use_user.username%type
		, p_password	in use_user.password%type
	) return clob;
	procedure logout;
	procedure register;
	procedure register
	(
		  p_url			in varchar2
		, p_username	in use_user.username%type
		, p_password	in use_user.password%type
		, p_email		in use_user.email%type
		, p_emailRepeat	in use_user.email%type
		, p_acceptTerms in varchar2							default NULL
	);
	function register
	(
		  p_url			in varchar2
		, p_username	in use_user.username%type
		, p_password	in use_user.password%type
		, p_email		in use_user.email%type
		, p_emailRepeat	in use_user.email%type
		, p_acceptTerms in varchar2							default NULL
	) return clob;
	procedure forgotPwd;
	function forgotPwd
		return clob;
	procedure pleaseLogin
	(
		  p_package			in mod_package.name%type		default null
		, p_procedure		in mod_procedure.name%type		default null
		, p_url				in varchar2						default null
	);
	function pleaseLogin
	(
		  p_package			in mod_package.name%type		default null
		, p_procedure		in mod_procedure.name%type		default null
		, p_url				in varchar2						default null
	) return clob;

end;
/
show errors;




-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body use_pub is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'use_pub';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  22.07.2005
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        login
-- what:        login
-- author:      Frode Klevstul
-- start date:  23.07.2005
---------------------------------------------------------
procedure login is begin
	use_pub.login(null, null, null);
end;

procedure login( p_url in varchar2 )is begin
	use_pub.login(p_url, null, null);
end;

procedure login
(
	  p_url			in varchar2
	, p_username	in use_user.username%type
	, p_password	in use_user.password%type
)
is
begin
	ext_lob.pri(use_pub.login(p_url, p_username, p_password));
end;

function login
(
	  p_url			in varchar2
	, p_username	in use_user.username%type
	, p_password	in use_user.password%type
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type 			:= 'login';

    c_tex_username				constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'loginName');
    c_tex_password				constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'password');
    c_tex_login					constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'login');
    c_tex_register				constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'register');
    c_tex_forgotPwd				constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'forgotPwd');
    c_tex_unamePwdNotEntered	constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'unamePwdNotEntered');
    c_tex_loggedInAs			constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'loggedInAs');
    c_tex_logOut				constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'logOut');
	c_tex_loggingIn				constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'loggingIn');
	c_tex_administrator			constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'administrator');
	c_tex_organisation			constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'organisation');
	c_tex_admPage				constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'admPage');
	c_tex_viewPage				constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'viewPage');
	c_tex_statistic				constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'statistic');
	c_tex_sendPage				constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'sendPage');
	c_tex_editUserInfo			constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'editUserInfo');
	c_tex_support				constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'support');

	c_id						constant ses_session.id%type 				:= ses_lib.getId;
	c_remote_host				constant ses_session.host%type 				:= owa_util.get_cgi_env('REMOTE_HOST');
	c_remote_addr				constant ses_session.ip%type   				:= owa_util.get_cgi_env('REMOTE_ADDR');

	c_login_package				constant sys_parameter.value%type			:= sys_lib.getParameter('login_package');
	c_page_width				constant sys_parameter.value%type 			:= sys_lib.getParameter('page_width');
	c_loginBar					constant sys_parameter.value%type 			:= sys_lib.getParameter('loginBar');
	c_i4admin_link				constant sys_parameter.value%type 			:= sys_lib.getParameter('i4admin_link');

	e_return					exception;

	v_count						number;
	v_use_user_pk				use_user.use_user_pk%type;
	v_no_failed_logins			use_login_status.no_failed_logins%type;
	v_no_successful_logins		use_login_status.no_successful_logins%type;
	v_no_failed_now				use_login_status.no_failed_now%type;
	v_wrong_logins				use_type.wrong_logins%type;
	v_us_name					use_status.name%type;
	v_idle_time					use_type.idle_time%type;
	v_url						varchar2(256);
	v_name						use_type.name%type;
	v_organisation_pk			org_organisation.org_organisation_pk%type;

	cursor	cur_use_user is
	select	use_user_pk
	from	use_user
	where	(
				(lower(username)= lower(p_username)) or
				(lower(email)	= lower(p_username))
			)
	and		password = p_password;

	cursor	cur_use_user_checkUsername
	(
		b_username	use_user.username%type
	) is
	select	use_user_pk
	from	use_user
	where	(lower(username) = lower(p_username)) or
			(lower(email) = lower(p_username));

	cursor	cur_use_login_status_failed
	(
		b_use_user_pk	use_user.use_user_pk%type
	) is
	select	no_failed_logins, no_failed_now
	from	use_login_status
	where	use_user_fk_pk = b_use_user_pk;

	cursor	cur_use_login_status_succ
	(
		b_use_user_pk	use_user.use_user_pk%type
	) is
	select	no_successful_logins
	from	use_login_status
	where	use_user_fk_pk = b_use_user_pk;

	v_html						clob;
	v_clob						clob;

begin
	-- -----------------------------------------------------------
	-- NOTE: only log with two parameters to avoid infinite loop
	-- -----------------------------------------------------------
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	-- -----------------------------------------------------------------
	-- if we have username and password we have a login attempt
	-- -----------------------------------------------------------------
	if (p_username is not null and p_password is not null) then
		
		for r_cursor in cur_use_user
		loop
		    v_use_user_pk := r_cursor.use_user_pk;
		end loop;

		-- -------------------------------------------------
		-- if v_use_user_pk is null we have a wrong login
		-- -------------------------------------------------
		if (v_use_user_pk is null and p_password) then

			-- ------------------------------
			-- check if password is correct
			-- ------------------------------
			select	count(*)
			into	v_count
			from	use_user
			where	password = p_password;

			if (v_count>0) then
				-- ---------------------------------------------
				-- the password is correct, several users
				-- might have the same password so we can't
				-- know for sure which user this is.
				-- log mistake in log_login with
				-- use_user_fk like null
				-- ---------------------------------------------
				insert into	log_login
				(log_login_pk, use_user_fk, ses_session_id, username, password, host, ip, bool_success, date_insert)
				values
				(
					  seq_log_login.nextval
					, null
					, c_id
					, p_username
					, p_password
					, c_remote_host
					, c_remote_addr
					, 0
					, sysdate
				);

				commit;
			end if;

			-- ------------------------------
			-- check if username is correct
			-- ------------------------------
			for r_cursor in cur_use_user_checkUsername(p_username)
			loop
			    v_use_user_pk := r_cursor.use_user_pk;
			end loop;

			if (v_use_user_pk is not null) then
				-- ---------------------------------------------
				-- the username is correct, log wrong login entry
				-- ---------------------------------------------
				insert into	log_login
				(log_login_pk, use_user_fk, ses_session_id, username, password, host, ip, bool_success, date_insert)
				values
				(
					  seq_log_login.nextval
					, v_use_user_pk
					, c_id
					, p_username
					, p_password
					, c_remote_host
					, c_remote_addr
					, 0
					, sysdate
				);

				commit;

				-- ---------------------------------------
				-- the username is correct, we register
				-- that this username has a wrong login
				-- attempt. this due to prevent someone
				-- trying to find the password by logging
				-- in several times
				-- ---------------------------------------
				for r_cursor in cur_use_login_status_failed(v_use_user_pk)
				loop
				    v_no_failed_logins	:= r_cursor.no_failed_logins;
				    v_no_failed_now		:= r_cursor.no_failed_now;
				end loop;

				-- ------------------------------------------------
				-- update number of wrong logins in the database
				-- ------------------------------------------------
			    update	use_login_status
			    set		no_failed_logins 	= (v_no_failed_logins + 1)
			    		, no_failed_now		= (v_no_failed_now + 1)
			    where	use_user_fk_pk = v_use_user_pk;

				if (sql%notfound) then
					insert into use_login_status
					(use_user_fk_pk, no_successful_logins, no_failed_logins, no_failed_now)
					values
					(
						  v_use_user_pk
						, 0
						, 1
						, 1
					);
				end if;

			    commit;

				-- -----------------------------------------------------
				-- check if this user has reached maximum wrong logins
				-- -----------------------------------------------------
			    select	wrong_logins
			    into	v_wrong_logins
			    from	use_user uu, use_type ut
			    where	uu.use_type_fk = ut.use_type_pk
			    and		uu.use_user_pk = v_use_user_pk;

			    if (v_no_failed_now >= v_wrong_logins) then
			    	update	use_user
			    	set		use_status_fk = (select use_status_pk from use_status where name = 'wrong_login')
			    	where	use_user_pk = v_use_user_pk;

					log_lib.logError(g_package, c_procedure, 'max_wrong_logins_reached');
			    end if;

			end if;

			-- show errorpage (and log to database)
			pag_pub.errorPage(g_package, c_procedure, 'wrong_login');
			raise e_return;

		-- ---------------------------------
		-- we have a correct login
		-- ---------------------------------
		else

			-- ---------------------------------
			-- check if user status is OK
			-- ---------------------------------
			select	count(*)
			into	v_count
			from	use_user uu, use_status us
			where	uu.use_status_fk = us.use_status_pk
			and		uu.use_user_pk = v_use_user_pk
			and		us.use_status_pk = (select use_status_pk from use_status where name = 'activated');

			if (v_count > 0) then
				-- ----------------------------------------------------
				-- log a successful login in the log_login
				-- ----------------------------------------------------
				insert into	log_login
				(log_login_pk, use_user_fk, ses_session_id, username, password, host, ip, bool_success, date_insert)
				values
				(
					  seq_log_login.nextval
					, v_use_user_pk
					, c_id
					, p_username
					, p_password
					, c_remote_host
					, c_remote_addr
					, 1
					, sysdate
				);

				commit;

				-- ---------------------------------------
				-- find and update number of succesful
				-- logins this user has had
				-- ---------------------------------------
				for r_cursor in cur_use_login_status_succ(v_use_user_pk)
				loop
				    v_no_successful_logins	:= r_cursor.no_successful_logins;
				end loop;

				if (v_no_successful_logins is null) then
					v_no_successful_logins := 0;
				end if;

				-- ------------------------------------------------
				-- update number of successful logins in the database
				-- ------------------------------------------------
			    update	use_login_status
			    set		no_successful_logins = (v_no_successful_logins + 1)
			    		, no_failed_now		= 0
			    where	use_user_fk_pk = v_use_user_pk;

				if (sql%notfound) then
					insert into use_login_status
					(use_user_fk_pk, no_successful_logins, no_failed_logins, no_failed_now)
					values
					(
						  v_use_user_pk
						, 1
						, 0
						, 0
					);
				end if;

				commit;

				-- -------------------------------------------------------------------
				-- update the session to be related to the user that just logged in
				-- -------------------------------------------------------------------
				select	idle_time
				into	v_idle_time
				from	use_user uu, use_type ut
				where	uu.use_type_fk = ut.use_type_pk
				and		uu.use_user_pk = v_use_user_pk;

				update	ses_session
				set		use_user_fk			= v_use_user_pk
						, date_update		= sysdate
						, idle_time			= v_idle_time
				where	id = c_id;

				commit;

				-- --------------------------------------------------------------
				-- set organisation_pk in ses_session if an organisation user
				-- --------------------------------------------------------------
				v_name := use_lib.getUsertype(v_use_user_pk);
				if (v_name = 'organisation') then
					v_organisation_pk := org_lib.getOrgPk(v_use_user_pk);

					update	ses_session
					set		org_organisation_fk = v_organisation_pk
					where	id = c_id;

					commit;
				end if;


			else

				select	name
				into	v_us_name
				from	use_user uu, use_status us
				where	uu.use_status_fk = us.use_status_pk
				and		uu.use_user_pk = v_use_user_pk;

				-- show errorpage
				pag_pub.errorPage(g_package, c_procedure, 'wrong_user_status{'||v_us_name||'}');
				raise e_return;

			end if;

		end if;

	end if;

	-- -----------------------------------------------------------------
	-- if we don't have timeout we show info about the user logged in,
	-- links and give the user an option to log out
	-- -----------------------------------------------------------------
	if (ses_lib.timeout > 0) then
		-- -----------------------------
		-- the user has logged in earlier
		-- -----------------------------
		if (p_username is null and p_password is null) then

			-- get user_pk
			v_use_user_pk := ses_lib.getPk(c_id);

			-- ----------------------------------------
			-- get the type of user that is logged in
			-- ----------------------------------------
			v_name := use_lib.getUsertype(v_use_user_pk);

			-- -----------------------------------------------
			--
			-- normal: normal user that has logged in
			--
			-- -----------------------------------------------
			if (v_name = 'normal') then

				-- ------------------------------------
				-- write out the user's full name
				-- plus give the option to log out
				-- ------------------------------------
				v_html := v_html||''||htm.bTable('loginForm')||chr(13);
				v_html := v_html||'<tr><td>'||chr(13);
				v_html := v_html||'<span class="loginBarHeading">'||c_tex_loggedInAs||' <b>'||use_lib.getFullName(v_use_user_pk)||'</b></span>'||chr(13);
				v_html := v_html||'</td><td>'||chr(13);
				v_html := v_html||htm.link(c_tex_editUserInfo,'use_adm.updateUserinfo','_self', 'theButtonStyle')||chr(13);
				v_html := v_html||'</td><td>'||chr(13);
				v_html := v_html||htm.link(c_tex_logout,'use_pub.logout','_self', 'theButtonStyle')||chr(13);
				v_html := v_html||'</td></tr>'||chr(13);
				v_html := v_html||''||htm.eTable||chr(13);

			-- -----------------------------------------------
			--
			-- organisation: organisation user
			--
			-- -----------------------------------------------
			elsif (v_name = 'organisation') then
				v_organisation_pk := ses_lib.getOrgPk(c_id);

				v_html := v_html||''||htm.bTable('loginForm')||chr(13);
				v_html := v_html||'<tr><td width="30%">'||chr(13);
				v_html := v_html||'<span class="loginBarHeading">'||c_tex_loggedInAs||' <b>'||org_lib.getOrgName(v_organisation_pk)||'</b> ('||c_tex_organisation||')</span>'||chr(13);
				v_html := v_html||'</td><td style="padding-right:20px">'||chr(13);
				v_html := v_html||htm.link(c_tex_admPage, 'org_adm.viewOrg', '_self', 'theButtonStyle')||chr(13);
				v_html := v_html||'&nbsp;'||chr(13);
				v_html := v_html||htm.link(c_tex_viewPage, 'org_pub.viewOrg?p_organisation_pk='||v_organisation_pk, '_self', 'theButtonStyle')||chr(13);
			--	v_html := v_html||'&nbsp;'||chr(13);
			--	v_html := v_html||htm.link(c_tex_statistic, 'log_pub.viewLogHits?p_table_name=org_organisation&p_foreign_key_fk='||v_organisation_pk, '_self', 'theButtonStyle')||chr(13);
			--	v_html := v_html||'&nbsp;'||chr(13);
			--	v_html := v_html||htm.link(c_tex_sendPage, 'pag_pub.sendLinkPage','_self', 'theButtonStyle')||chr(13);
				v_html := v_html||'&nbsp;'||chr(13);
				v_html := v_html||htm.link(c_tex_editUserInfo, 'use_adm.updateUserinfo', '_self', 'theButtonStyle')||chr(13);
				v_html := v_html||'&nbsp;'||chr(13);
				v_html := v_html||htm.link(c_tex_support,'pag_pub.contactPage','_self', 'theButtonStyle')||chr(13);
				v_html := v_html||'</td><td width="10%" style="padding-right:20px">'||chr(13);
				v_html := v_html||htm.link(c_tex_logout,'use_pub.logout','_self', 'theButtonStyle')||chr(13);
				v_html := v_html||'</td></tr>'||chr(13);
				v_html := v_html||''||htm.eTable||chr(13);

			-- -----------------------------------------------
			--
			-- admin: administrator user
			--
			-- -----------------------------------------------
			elsif (v_name like 'admin_%') then
				v_organisation_pk := ses_lib.getOrgPk(c_id);
				
				v_html := v_html||''||htm.bTable('loginForm')||chr(13);
				v_html := v_html||'<tr><td>'||chr(13);
				v_html := v_html||'<span class="loginBarHeading">'||c_tex_loggedInAs||' <b>'||use_lib.getFullName(ses_lib.getPk(c_id))||'</b> ('||c_tex_administrator||' - '||v_name||')</span>'||chr(13);
				v_html := v_html||'</td><td>'||chr(13);
				if (c_i4admin_link = '1') then
					v_html := v_html||htm.link('i4admin','i4_pub.i4admin','_self', 'theButtonStyle')||chr(13);
				else
					v_html := v_html||'&nbsp;'||chr(13);
				end if;
				if (v_organisation_pk is not null) then
					v_html := v_html||htm.link('orgadm','org_adm.viewOrg','_self', 'theButtonStyle')||chr(13);
				else
					v_html := v_html||'&nbsp;'||chr(13);
				end if;
				v_html := v_html||'</td><td>'||chr(13);
				v_html := v_html||htm.link(c_tex_editUserInfo,'use_adm.updateUserinfo','_self', 'theButtonStyle')||chr(13);
				v_html := v_html||'</td><td>'||chr(13);
				v_html := v_html||htm.link(c_tex_logout,'use_pub.logout','_self', 'theButtonStyle')||chr(13);
				v_html := v_html||'</td></tr>'||chr(13);
				v_html := v_html||''||htm.eTable||chr(13);
			end if;

		-- -----------------------------
		-- the user is logging in now
		-- -----------------------------
		else
			-- -------------------------------------------------------
			-- on some pages it shall not be possible to login
			-- in case the user is logging in here we redirect him to
			-- the default page
			-- -------------------------------------------------------
			if (
				   lower(p_url) like lower('pag_pub.errorPage%')
				or lower(p_url) like lower('use_pub.login%')
			) then
				v_url := c_login_package;
			elsif (lower(p_url) like lower('org_pub.viewOrg?')) then
				v_url := 'org_adm.viewOrg';
			else
				v_url := p_url;
			end if;

			v_html := v_html||htm.jumpTo(v_url, 1, c_tex_loggingIn);
		end if;

	-- -----------------------------------------------------------------
	-- we have a timeout (or user not logged in), ask the user to login...
	-- -----------------------------------------------------------------
	else

		if (c_loginBar = '1') then

			v_html := '
				<!-- Javascript for login box -->
				<script type="text/javascript">

	                function checkFields_loginForm(){
	                        var f = document.login_form;
	                        if((!f.p_password.value) || (!f.p_username.value)){
	                                alert("'||c_tex_unamePwdNotEntered||'");
	                                return false;
	                        }
	                        else {
	                                return true;
	                        }
	                }

				</SCRIPT>
				<!-- /Javascript for login box -->
			';

			v_url := replace(p_url, '&', '&amp;');

			v_html := v_html||''||htm.bForm('use_pub.login','login_form', '_top',  'onSubmit="Javascript: return checkFields_loginForm();"')||chr(13);
			v_html := v_html||'
				<input type="hidden" name="p_url" value="'|| v_url ||'" />
			';

			v_html := v_html||''||htm.bTable('loginForm')||chr(13);
			v_html := v_html||'
				<tr>
					<td width="10%">
						<span class="loginBarHeading">'||c_tex_username||'</span>
					</td>
					<td width="10%">
						<input type="text" name="p_username" maxlength="150" size="12" value="'||p_username||'" />
					</td>
					<td width="7%">
						<span class="loginBarHeading">'||c_tex_password||'</span>
					</td>
					<td width="10%">
						<input class="small" type="password" name="p_password" maxlength="50" size="12" />
					</td>
					<td width="10%" colspan="2">
						'||htm.submitLink(c_tex_login, 'login_form', 'if(checkFields_loginForm())document.login_form.submit()')||'
					</td>
					<td width="23%">
						&nbsp;
					</td>
					<td width="15%">'||htm.link(c_tex_register, 'use_pub.register', '_self', 'theButtonStyle')||'</td>
					<td width="15%">'||htm.link(c_tex_forgotPwd,'use_pub.forgotPwd','_self', 'theButtonStyle')||'</td>
				</tr>
			';
			v_html := v_html||''||htm.eTable||chr(13);
			v_html := v_html||''||htm.eForm||chr(13);
		else
			v_html := v_html||''||htm.bTable('loginForm')||'<tr><td>&nbsp;</td></tr>'||htm.eTable||chr(13);
		end if;

	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
	when e_return then
		return null;
	when others then
		return null;
end;
end login;


---------------------------------------------------------
-- name:        logout
-- what:        logout
-- author:      Frode Klevstul
-- start date:  14.08.2005
---------------------------------------------------------
procedure logout
is
begin
declare
	c_procedure					constant mod_procedure.name%type 		:= 'logout';

	c_id						constant ses_session.id%type 			:= ses_lib.getId;

    c_tex_loggingOut			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'loggingOut');
	c_logout_package			constant sys_parameter.value%type		:= sys_lib.getParameter('logout_package');

begin
	ses_lib.ini(g_package, c_procedure, null);

	update	ses_session
	set		date_update = (sysdate - 365)															-- this causes "PLW-07202" error if "alter session set plsql_warnings='enable:all';"
	where	id = c_id;
	commit;

	htm_lib.removeCookie('in4mant_session');

	htm.jumpTo(c_logout_package, 1, c_tex_loggingOut);

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end logout;


---------------------------------------------------------
-- name:        register
-- what:        register new user
-- author:      Frode Klevstul
-- start date:  13.09.2005
---------------------------------------------------------
procedure register is begin
	use_pub.register(null, null, null, null, null, null);
end;

procedure register(p_url in varchar2, p_username in use_user.username%type, p_password in use_user.password%type, p_email in use_user.email%type, p_emailRepeat in use_user.email%type, p_acceptTerms in varchar2 default NULL) is begin
	ext_lob.pri(use_pub.register(p_url, p_username, p_password, p_email, p_emailRepeat, p_acceptTerms));
end;

function register
(
	  p_url			in varchar2
	, p_username	in use_user.username%type
	, p_password	in use_user.password%type
	, p_email		in use_user.email%type
	, p_emailRepeat	in use_user.email%type
	, p_acceptTerms in varchar2					default NULL
) return clob
is
begin
declare
	c_procedure						constant mod_procedure.name%type 		:= 'register';

	c_id							constant ses_session.id%type 			:= ses_lib.getId;
	c_login_package					constant sys_parameter.value%type		:= sys_lib.getParameter('login_package');

	c_tex_username					constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'username');
	c_tex_password					constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'password');
	c_tex_email						constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'email');
	c_tex_emailRepeat				constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'emailRepeat');
	c_tex_register					constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'register');
	c_tex_unamePwdEmailNotEntered	constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'unamePwdEmailNotEntered');
	c_tex_loggingIn					constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'loggingIn');
	c_tex_usernameTaken				constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'usernameTaken');
	c_tex_alternativeUsername		constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'alternativeUsername');
	c_tex_invalidEmailAddress		constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'invalidEmailAddress');
	c_tex_theTwoEmailsNotEqual		constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'theTwoEmailsNotEqual');
	c_tex_readAndAccepted			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'readAndAccepted');
	c_tex_notAcceptedTerms			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'notAcceptedTerms');
	c_tex_registerNewUser			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'registerNewUser');
	c_tex_invalidUsername			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'invalidUsername');

	v_error							boolean									:= false;
	v_tmpError						boolean									:= false;
	v_username						use_user.username%type;
	v_address_pk					add_address.add_address_pk%type;
	v_user_pk						use_user.use_user_pk%type;
	v_errorMsg						varchar2(256);
	v_error_messages				varchar2(1024);

	v_html							clob;
	v_clob							clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_username='||p_username||'��p_email='||p_email);
	ext_lob.ini(v_clob);

	-- --------------------------------
	--  check if username is valid
	-- --------------------------------
	if (p_username is not null) then
		ext_lib.verifyValue(
			  p_type		=> 'username'
			, p_value		=> p_username
			, p_returnError => v_tmpError
			, p_returnMsg	=> v_errorMsg
		);

		if (v_tmpError) then
			v_error := true;
			v_error_messages := v_error_messages||''||v_errorMsg||'<br>';

			-- suggest new username
			v_username := use_lib.suggestUsername(p_username);
			v_error_messages := v_error_messages||''||c_tex_alternativeUsername||': '||v_username||'<br>';
		end if;
	end if;

	-- --------------------------------
	--  check if email address is valid
	-- --------------------------------
	if (p_email is not null) then
		ext_lib.verifyValue(
			  p_type		=> 'email'
			, p_value		=> p_email
			, p_returnError => v_tmpError
			, p_returnMsg	=> v_errorMsg
		);

		if (v_tmpError) then
			v_error := true;
			v_error_messages := v_error_messages||''||v_errorMsg||'<br>';
		end if;
	end if;

	-- check if the first email address is equal to the second
	if (p_email != p_emailRepeat) then
		v_error := true;
		v_error_messages := v_error_messages||''||c_tex_theTwoEmailsNotEqual||'<br>';
	end if;

	--  check if user accepts the terms
	if (p_username is not null) then
		if (p_acceptTerms is null) then
			v_error := true;
			v_error_messages := v_error_messages||''||c_tex_notAcceptedTerms||'<br>';
		end if;
	end if;


	-- ------------------------------------------------------------
	-- insert new userdata into database and login user
	-- ------------------------------------------------------------
	if (p_username is not null and p_password is not null and p_email is not null and not v_error) then

		v_user_pk		:= sys_lib.getEmptyPk('use_user');
		v_address_pk	:= sys_lib.getEmptyPk('add_address');

		insert into use_user
		(use_user_pk, username, email, password, use_type_fk, use_status_fk)
		values (
			  v_user_pk
			, lower(p_username)
			, lower(p_email)
			, p_password
			, (select use_type_pk from use_type where name = 'normal')
			, (select use_status_pk from use_status where name = 'not_activated')
		);

		insert into add_address
		(add_address_pk, date_insert, date_update)
		values(
			  v_address_pk
			, sysdate
			, sysdate
		);

		insert into use_details
		(use_user_fk_pk, add_address_fk, date_insert, date_update)
		values(
			  v_user_pk
			, v_address_pk
			, sysdate
			, sysdate
		);

		-- login user by connecting the newly created user's id to the session
		-- first time user logs in / register the idle time is 30 minutes
		update	ses_session
		set		use_user_fk = v_user_pk
				, idle_time = 30
		where	id = c_id;

		commit;

		if (p_url is not null) then
			htm.jumpTo(p_url, 1, c_tex_loggingIn);
		else
			htm.jumpTo(c_login_package, 1, c_tex_loggingIn);
		end if;

	-- ------------------------------------------------------------
	-- show register form
	-- ------------------------------------------------------------
	else
		v_html := v_html||''||htm.bPage(g_package, c_procedure)||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('miniBox')||chr(13);
		v_html := v_html||'<tr><td>'||chr(13);

		v_html := v_html||''||htm.bBox(c_tex_registerNewUser)||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);
		v_html := v_html||''||htm.bForm('use_pub.register','register_form')||chr(13);

		-- ---------------------------------
		-- show possible error messages
		-- ---------------------------------
		if (v_error_messages is not null) then
			v_html := v_html||'<tr><td colspan="2">'||chr(13);
			v_html := v_html||''||htm.printErrors(v_error_messages)||chr(13);
			v_html := v_html||'</td></tr>'||chr(13);
		end if;

		if (v_username is null) then
			v_username := p_username;
		end if;

		v_html := v_html||'
			<input type="hidden" name="p_url" value="'|| p_url ||'" />
			<tr>
				<td>
					'||c_tex_email||':
				</td>
				<td>
					<input type="text" name="p_email" maxlength="150" size="12" value="'||p_email||'" />
				</td>
			</tr>
			<tr>
				<td>
					'||c_tex_emailRepeat||':
				</td>
				<td>
					<input type="text" name="p_emailRepeat" maxlength="150" size="12" value="'||p_emailRepeat||'" />
				</td>
			</tr>
			<tr>
				<td>
					'||c_tex_username||':
				</td>
				<td>
					<input type="text" name="p_username" maxlength="150" size="12" value="'||v_username||'" />
				</td>
			</tr>
			<tr>
				<td>
					'||c_tex_password||':
				</td>
				<td>
					<input type="password" name="p_password" maxlength="50" size="12" />
				</td>
			</tr>
			<tr>
				<td>
					'||c_tex_readAndAccepted||':
				</td>
				<td>
					<input type="checkbox" name="p_acceptTerms" value="accepted" />
				</td>
			</tr>
			<tr>
				<td colspan="2">'||htm.submitLink(c_tex_register, 'register_form', 'if (checkFields_registerForm()) document.register_form.submit()')||'</td>
			</tr>

			<!-- Javascript for login box -->
			<SCRIPT language="JavaScript">

				document.login_form.p_username.focus();

	            function checkFields_registerForm(){
	                    var f = document.register_form;

	                    if((!f.p_password.value) || (!f.p_username.value) || (!f.p_email.value)){
	                            alert("'||c_tex_unamePwdEmailNotEntered||'");
	                            return false;
	                    }
	                    else {
	                            return true;
	                    }
	            }

			</SCRIPT>
			<!-- /Javascript for login box -->
		';

		v_html := v_html||''||htm.eForm||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);

		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);
	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
    when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
        return null;
end;
end register;


---------------------------------------------------------
-- name:        forgotPwd
-- what:        if user has forgotten password, send new
-- author:      Frode Klevstul
-- start date:  10.10.2005
---------------------------------------------------------
procedure forgotPwd is begin
    ext_lob.pri(use_pub.forgotPwd);
end;

function forgotPwd
	return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type 		:= 'forgotPwd';

	c_tex_forgottenPwd			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'forgottenPwd');
	c_tex_forgottenPwdTxt		constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'forgottenPwdTxt');

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, null);
	ext_lob.ini(v_clob);

	v_html := v_html||''||htm.bPage(g_package, c_procedure)||chr(13);
	v_html := v_html||'<center>'||chr(13);
	v_html := v_html||''||htm.bTable('miniBox')||chr(13);
	v_html := v_html||'<tr><td>'||chr(13);

	v_html := v_html||''||htm.bBox(c_tex_forgottenPwd)||chr(13);
	v_html := v_html||''||c_tex_forgottenPwdTxt||chr(13);
	v_html := v_html||''||htm.eBox||chr(13);

	v_html := v_html||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||'</center>'||chr(13);
	v_html := v_html||''||htm.ePage||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end forgotPwd;


---------------------------------------------------------
-- name:        pleaseLogin
-- what:        ask user to login
-- author:      Frode Klevstul
-- start date:  20.12.2005
---------------------------------------------------------
procedure pleaseLogin
(
	  p_package			in mod_package.name%type		default null
	, p_procedure		in mod_procedure.name%type		default null
	, p_url				in varchar2						default null
) is begin
	ext_lob.pri(use_pub.pleaseLogin(p_package, p_procedure, p_url));
end;

function pleaseLogin
(
	  p_package			in mod_package.name%type		default null
	, p_procedure		in mod_procedure.name%type		default null
	, p_url				in varchar2						default null
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type 		:= 'pleaseLogin';

	c_tex_pleaseLogin			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'pleaseLogin');
	c_tex_pleaseLoginTxt		constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'pleaseLoginTxt');
	c_tex_username				constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'username');
	c_tex_password				constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'password');
	c_tex_loginSubmit			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'loginSubmit');
    c_tex_unamePwdNotEntered	constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'unamePwdNotEntered');

	v_url						varchar2(128);

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_package='||p_package||'��p_procedure='||p_procedure);
	ext_lob.ini(v_clob);

	-- ------------------------------------------------------
	-- note: using p_package and p_procedure to login into
	-- correct module if not p_url is set
	-- ------------------------------------------------------
	if (p_url is not null) then
		v_url := p_url;
	else
		v_url := p_package ||'.'|| p_procedure;
	end if;

    v_html := v_html||''||htm.bPage(g_package, c_procedure, 'simple')||chr(13);
	v_html := v_html||'<center>'||chr(13);

	v_html := v_html||''||htm.bTable('miniBox')||chr(13);
	v_html := v_html||'<tr><td>'||chr(13);
	v_html := v_html||'
			<!-- Javascript for login box -->
			<SCRIPT language="JavaScript">

				document.adm_login_form.p_username.focus();

                function checkFields_userLoginForm(){
                        var f = document.adm_login_form;
                        if((!f.p_password.value) || (!f.p_username.value)){
                                alert("'||c_tex_unamePwdNotEntered||'");
                                return false;
                        }
                        else {
                                return true;
                        }
                }

			</SCRIPT>
			<!-- /Javascript for login box -->
	';

    v_html := v_html||''||htm.bBox(c_tex_pleaseLogin)||chr(13);
	v_html := v_html||''||htm.bTable||chr(13);
	v_html := v_html||''||htm.bForm('use_pub.login','adm_login_form')||chr(13);
    v_html := v_html||'<input type="hidden" name="p_url" value="'|| v_url ||'" />'||chr(13);
    v_html := v_html||'<tr>'||chr(13);
    v_html := v_html||'	<td colspan="2">'||chr(13);
    v_html := v_html||c_tex_pleaseLoginTxt||chr(13);
    v_html := v_html||'	</td>'||chr(13);
    v_html := v_html||'</tr>'||chr(13);
    v_html := v_html||'<tr><td colspan="2">&nbsp;</td></tr>'||chr(13);
    v_html := v_html||'<tr>'||chr(13);
    v_html := v_html||'	<td width="50%">'||chr(13);
    v_html := v_html||'		'||c_tex_username||':'||chr(13);
    v_html := v_html||'	</td>'||chr(13);
    v_html := v_html||'	<td>'||chr(13);
    v_html := v_html||'		<input type="text" size="10" maxlength="30" name="p_username" />'||chr(13);
    v_html := v_html||'	</td>'||chr(13);
    v_html := v_html||'</tr>'||chr(13);
    v_html := v_html||'<tr>'||chr(13);
    v_html := v_html||'	<td>'||chr(13);
    v_html := v_html||'		'||c_tex_password||':'||chr(13);
    v_html := v_html||'	</td>'||chr(13);
    v_html := v_html||'	<td>'||chr(13);
    v_html := v_html||'		<input type="password" size="10" maxlength="30" name="p_password" />'||chr(13);
    v_html := v_html||'	</td>'||chr(13);
    v_html := v_html||'</tr>'||chr(13);
    v_html := v_html||'<tr><td colspan="2">&nbsp;</td></tr>'||chr(13);
    v_html := v_html||'<tr>'||chr(13);
    v_html := v_html||'	<td colspan="2">'||chr(13);
    v_html := v_html||'		'||htm.submitLink(c_tex_loginSubmit, 'adm_login_form', 'if (checkFields_userLoginForm()) document.adm_login_form.submit()')||chr(13);
    v_html := v_html||'	</td>'||chr(13);
    v_html := v_html||'</tr>'||chr(13);
	v_html := v_html||''||htm.eForm||chr(13);
    v_html := v_html||''||htm.eTable||chr(13);
    v_html := v_html||''||htm.eBox||chr(13);

	v_html := v_html||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);

	v_html := v_html||'</center>'||chr(13);
    v_html := v_html||''||htm.ePage||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
    when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
        return null;
end;
end pleaseLogin;

-- ******************************************** --
end;
/
show errors;
