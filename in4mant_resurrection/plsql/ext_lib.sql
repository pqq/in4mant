PROMPT *** ext_lib ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package ext_lib is
/* ******************************************************
*	Package:     ext_lib
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	051130 | Frode Klevstul
*			- Package created
****************************************************** */
	procedure run;
	procedure verifyValue
	(
		  p_type			in varchar2				default null
		, p_value			in varchar2				default null
		, p_returnError		in out boolean
		, p_returnMsg		in out varchar2
	);
	function c2b
	(
		c						in clob
	) return blob;
	function b2c
	(
		b						in blob
	) return clob;
	function split
	(
	   pc$chaine in varchar2,         -- input string
	   pn$pos in pls_integer,         -- token number
	   pc$sep in varchar2 default ',' -- separator character
	)
	return varchar2;
	function abbreviation
	(
		  string		varchar2
		, ret_len		number
	) return varchar2;

end;
/
show errors;





-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body ext_lib is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'ext_lib';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  30.11.2005
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        verifyValue
-- what:        verify an input value
-- author:      Frode Klevstul
-- start date:  30.11.2005
---------------------------------------------------------
procedure verifyValue
(
	  p_type			in varchar2				default null
	, p_value			in varchar2				default null
	, p_returnError		in out boolean
	, p_returnMsg		in out varchar2
)
is
begin
declare
	c_procedure					constant mod_procedure.name%type 	:= 'verifyValue';

	c_tex_usernameNotUnique		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'usernameNotUnique');
	c_tex_emailNotUnique		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'emailNotUnique');
	c_tex_invalidUsername		constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'invalidUsername');
	c_tex_invalidEmailAddress	constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'invalidEmailAddress');
	c_tex_notNumber				constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'notNumber');

begin
	ses_lib.ini(g_package, c_procedure);

	-- ----------------------------------------
	-- username
	-- ----------------------------------------
	if (p_type = 'username') then

		-- 1: check if username is unique
		if (not use_lib.uniqueUsernameEmail(p_value)) then
			p_returnError	:= true;
			p_returnMsg		:= c_tex_usernameNotUnique;
			return;
		end if;

		-- 2: check if username is valid
		if (not use_lib.validUsername(p_value)) then
			p_returnError 	:= true;
			p_returnMsg		:= c_tex_invalidUsername;
			return;
		end if;

	-- ----------------------------------------
	-- email
	-- ----------------------------------------
	elsif (p_type = 'email') then

		-- 1: check if email is unique
		if (not use_lib.uniqueUsernameEmail(p_value)) then
			p_returnError	:= true;
			p_returnMsg		:= c_tex_emailNotUnique;
			return;
		end if;

		-- 2: check if email address is valid
		if (not use_lib.validEmail(p_value) ) then
			p_returnError 	:= true;
			p_returnMsg 	:= c_tex_invalidEmailAddress;
		end if;

	-- ----------------------------------------
	-- number
	-- ----------------------------------------
	elsif (p_type = 'number') then

		-- 1: check if value consists of numbers only
		if ( not owa_pattern.match(p_value, '^\d{1,}$') ) then
			p_returnError	:= true;
			p_returnMsg		:= c_tex_notNumber;
			return;
		end if;

	end if;

end;
end verifyValue;


---------------------------------------------------------
-- name:        c2b
-- what:        typecasts CLOB to BLOB (binary conversion)
--				http://www.dynamicpsp.com/!go?m$id=10127&ln=Tips-View-Eng.dpsp&f$id=10127
-- author:      Frode Klevstul
-- start date:  02.08.2006
---------------------------------------------------------
function c2b
(
	c						in clob
) return blob
is
begin
declare
	c_procedure		constant mod_procedure.name%type 			:= 'c2b';

	pos				pls_integer									:= 1;
	buffer			raw(32767);
	res				blob;
	lob_len			pls_integer									:= dbms_lob.getlength(c);

begin
	ses_lib.ini(g_package, c_procedure);

	dbms_lob.createtemporary(res, true);
	dbms_lob.open(res, dbms_lob.lob_readwrite);

	loop
		buffer := utl_raw.cast_to_raw(dbms_lob.substr(c, 16000, pos));

		if utl_raw.length(buffer) > 0 then
			dbms_lob.writeappend(res, utl_raw.length(buffer), buffer);
		end if;

		pos := pos + 16000;
		exit when pos > lob_len;
	end loop;

	return res; -- res is open here

end;
end c2b;


---------------------------------------------------------
-- name:        b2c
-- what:        typecasts BLOB to CLOB (binary conversion)
--				http://www.dynamicpsp.com/dpsp/prod/!go?m$id=10128&ln=Tips-View-Eng.dpsp&f$id=10128
-- author:      Frode Klevstul
-- start date:  02.08.2006
---------------------------------------------------------
function b2c
(
	b						in blob
) return clob
is
begin
declare
	c_procedure		constant mod_procedure.name%type 			:= 'b2c';

	pos				pls_integer									:= 1;
	buffer			raw(32767);
	res				clob;
	lob_len			pls_integer									:= dbms_lob.getlength(b);

begin
	ses_lib.ini(g_package, c_procedure);

	dbms_lob.createtemporary(res, true);
	dbms_lob.open(res, dbms_lob.lob_readwrite);

	loop
		buffer := utl_raw.cast_to_varchar2(dbms_lob.substr(b, 16000, pos));

		if length( buffer ) > 0 then
			dbms_lob.writeappend(res, length(buffer), buffer);
		end if;

		pos := pos + 16000;
		exit when pos > lob_len;
	end loop;

	return res; -- res is open here

end;
end b2c;


---------------------------------------------------------
-- name:        split
-- what:        split a string
--				http://fdegrelle.over-blog.com/article-1342263.html
-- author:      Frode Klevstul
-- start date:  17.10.2006
---------------------------------------------------------
function split
(
   pc$chaine in varchar2,         -- input string
   pn$pos in pls_integer,         -- token number
   pc$sep in varchar2 default ',' -- separator character
)
return varchar2
is
	c_procedure		constant mod_procedure.name%type 			:= 'b2c';

	lc$chaine varchar2(32767) := pc$sep || pc$chaine ;
	li$i      pls_integer ;
	li$i2     pls_integer ;

begin
	ses_lib.ini(g_package, c_procedure);

	li$i := instr( lc$chaine, pc$sep, 1, pn$pos ) ;
	if li$i > 0 then
		li$i2 := instr( lc$chaine, pc$sep, 1, pn$pos + 1) ;
		if li$i2 = 0 then li$i2 := length( lc$chaine ) + 1 ; end if ;
		return( substr( lc$chaine, li$i+1, li$i2 - li$i-1 ) ) ;
	else
		return null ;
	end if ;
end split;


---------------------------------------------------------
-- name:        abbreviation
-- what:        abbreviates a string, takes care off HTML codes
-- author:      Espen Messel
-- start date:  31.01.2007
---------------------------------------------------------
function abbreviation
(
	  string		varchar2
	, ret_len		number
) return varchar2
is
	c_procedure		constant mod_procedure.name%type 			:= 'abbreviation';

	semi			number;
	str_start		number;
	space			number;
	str_len			number;
	max_nr			number;

begin
	ses_lib.ini(g_package, c_procedure);

	max_nr := 1;
	if ( ret_len <= 0 ) then
		str_len := 20;
	else
		str_len := ret_len;
	end if;

	str_start := instr(string, '&', 1, 1);
	if ( str_start > 0 and str_len >= str_start ) then
		loop
			semi := instr(string, ';', str_start, 1);
			space := instr(string, ' ', str_start, 1);

			if (( space > semi and semi > 0 ) or ( semi > 0 and space = 0 )) then
				str_len := str_len + ( semi - str_start );
				str_start := semi + 1;
			end if;

			str_start := instr(string, '&', str_start, 1);

			exit when str_start > str_len;
			exit when semi = 0;
			exit when max_nr = 20;
			max_nr := max_nr + 1;
	  	end loop;
	end if;

	if ( length(string) <= str_len ) then
		return substr(string, 1, str_len);
	else
		return substr(string, 1, str_len)||'...';
	end if;

end abbreviation;



-- ******************************************** --
end;
/
show errors;
