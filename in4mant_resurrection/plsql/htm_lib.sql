PROMPT *** htm_lib ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package htm_lib is
/* ******************************************************
*	Package:     htm_lib
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	050723 | Frode Klevstul
*			- Package created
****************************************************** */
	type parameter_arr 			is table of varchar2(4000) index by binary_integer;
	empty_array 				parameter_arr;

	procedure run;
	procedure setCookie
	(
		  p_name				in varchar2
		, p_value				in varchar2
	);
	function getCookie
	(
		p_name					in varchar2
	) return varchar2;
	procedure removeCookie
	(
		p_name					in varchar2
	);
	procedure clock
	(
		p_style					in varchar2									default 'normal'
	  , p_form					in varchar2									default	'form'
	  , p_field					in varchar2									default	'p_clock'
	);
	function clock
	(
		p_style					in varchar2									default 'normal'
	  , p_form					in varchar2									default	'form'
	  , p_field					in varchar2									default	'p_clock'
	) return clob;
	procedure upload
	(
		  p_noPairs				in number
		, p_parNames			in parameter_arr
		, p_parValues			in parameter_arr
		, p_notInUse			in parameter_arr
		, p_docMimeType			in out varchar2
		, p_blob				out blob
		, p_clob				out clob
		, p_nclob				out nclob
	);
	function HTMLFix
	(
		  p_code				in clob
	) return clob;
	function HTMLEncode
	(
		  p_code				in varchar2
	) return clob;
	procedure selectService
	(
		p_ser_service_pk		ser_service.ser_service_pk%type				default null
	);
	function selectService
	(
		p_ser_service_pk		ser_service.ser_service_pk%type				default null
	) return clob;
	procedure selectOrgStatus
	(
		p_org_status_pk			org_status.org_status_pk%type				default null
	);
	function selectOrgStatus
	(
		p_org_status_pk			org_status.org_status_pk%type				default null
	) return clob;
	procedure selectBool
	(
		  p_choice				number										default null
		, p_no					number										default null
	);
	function selectBool
	(
		  p_choice				number										default null
		, p_no					number										default null
	) return clob;
	procedure selectUseStatus
	(
		p_use_status_pk			use_status.use_status_pk%type				default null
	);
	function selectUseStatus
	(
		p_use_status_pk			use_status.use_status_pk%type				default null
	) return clob;
	procedure selectUseType
	(
		p_use_type_pk			use_type.use_type_pk%type					default null
	);
	function selectUseType
	(
		p_use_type_pk			use_type.use_type_pk%type					default null
	) return clob;
	procedure selectOrganisation
	(
		  p_type				varchar2									default null
		, p_org_organisation_pk	org_organisation.org_organisation_pk%type	default null
		, p_service_pk			ser_service.ser_service_pk%type				default null
		, p_name				org_organisation.name%type					default null
	);
	function selectOrganisation
	(
		  p_type				varchar2									default null
		, p_org_organisation_pk	org_organisation.org_organisation_pk%type	default null
		, p_service_pk			ser_service.ser_service_pk%type				default null
		, p_name				org_organisation.name%type					default null
	) return clob;
	procedure selectConSubtype
	(
		p_con_subtype_pk		con_subtype.con_subtype_pk%type				default null
	);
	function selectConSubtype
	(
		p_con_subtype_pk		con_subtype.con_subtype_pk%type				default null
	) return clob;
	procedure selectRating
	(
		  p_maxRating			number										default null
		, p_rating				number										default null
	);
	function selectRating
	(
		  p_maxRating			number										default null
		, p_rating				number										default null
	) return clob;
	procedure selectContent
	(
		  p_content_pk			con_content.con_content_pk%type				default null
		, p_organisation_pk		org_organisation.org_organisation_pk%type	default null
		, p_id					number										default null
	);
	function selectContent
	(
		  p_content_pk			con_content.con_content_pk%type				default null
		, p_organisation_pk		org_organisation.org_organisation_pk%type	default null
		, p_id					number										default null
	) return clob;
	procedure selectGeography
	(
		  p_geography_pk		geo_geography.geo_geography_pk%type			default null
		, p_parent				geo_geography.name%type						default null
		, p_id					number										default null
	);
	function selectGeography
	(
		  p_geography_pk		geo_geography.geo_geography_pk%type			default null
		, p_parent				geo_geography.name%type						default null
		, p_id					number										default null
	) return clob;
	procedure pathField
	(
		  p_package				in mod_package.name%type
		, p_procedure			in mod_procedure.name%type
	);
	function pathField
	(
		  p_package				in mod_package.name%type
		, p_procedure			in mod_procedure.name%type
	) return clob;
	procedure selectLanguage
	(
		  p_language_pk			tex_language.tex_language_pk%type			default null
	);
	function selectLanguage
	(
		  p_language_pk			tex_language.tex_language_pk%type			default null
	) return clob;
	function stripHTML
	(
		  p_code				in varchar2
	) return clob;
	function stripNewline
	(
		  p_code			in varchar2
	) return clob;
	function stripIllegalTextFromUrl
	(
		  p_code			in varchar2
	) return clob;

end;
/
show errors;




-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body htm_lib is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'htm_lib';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  23.07.2005
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        setCookie
-- what:        sets a cookie in the browser
-- author:      Frode Klevstul
-- start date:  23.07.2005
---------------------------------------------------------
procedure setCookie
(
	  p_name			in varchar2
	, p_value			in varchar2
)
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'setCookie';

	cursor	cur_ser_service is
	select	domain
	from	ser_service
	where	bool_active > 0;

begin
	ses_lib.ini(g_package, c_procedure);

    owa_util.mime_header('text/html', false);

    -- format:
    -- owa_cookie.send(name, value, expires, path, domain, secure);

	-- note: on OAS we needed to set '/' as domain:
	-- owa_cookie.send(p_name, p_value, null, '/');
	-- however, on Apache domain must be 'null'

	-- the cookie has to be stored in two different ways
	-- since the first method (when the cookie is valid for all domains)
	-- doesn't work everywhere

	-- set the cookie for all domains
    owa_cookie.send(p_name, p_value, null );

	-- set domain specific cookie for each domain that's registered
	for r_cursor in cur_ser_service
	loop
	    owa_cookie.send(p_name, p_value, null, null, '.'||r_cursor.domain);
	end loop;

    owa_util.http_header_close;
end;
end setCookie;


---------------------------------------------------------
-- name:        getCookie
-- what:        gets a cookie's value
-- author:      Frode Klevstul
-- start date:  23.07.2005
---------------------------------------------------------
function getCookie
(
	p_name				in varchar2
) return varchar2
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'getCookie';

	v_cookie       	owa_cookie.cookie;  	            		-- the variable we'll store the cookie in
	v_value        	varchar2(128);
begin
	ses_lib.ini(g_package, c_procedure);

	v_cookie	:= owa_cookie.get(p_name);						-- gets the cookie
	v_value		:= v_cookie.vals(1);							-- gets the cookie's value

	return v_value;												-- returns the value of the cookie
exception
	when no_data_found then										-- if the cookie doesn't exist
		return null;
	when others then
		return null;
end;
end getCookie;


---------------------------------------------------------
-- name:        removeCookie
-- what:        removes a cookie
-- author:      Frode Klevstul
-- start date:  23.07.2005
---------------------------------------------------------
procedure removeCookie
(
	p_name				in varchar2
)
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'removeCookie';

	v_cookie        owa_cookie.cookie;							-- the cookie we'll get
	v_value         number;										-- the cookie's value
begin
	ses_lib.ini(g_package, c_procedure);

    v_cookie := owa_cookie.get(p_name);							-- gets the cookie
    v_value := (v_cookie.vals(1));								-- gets the cookie's value

    owa_util.mime_header('text/html', false);
    owa_cookie.remove(p_name,v_value);							-- removes the cookie
    owa_util.http_header_close;
end;
end removeCookie;


---------------------------------------------------------
-- name:        clock
-- what:        display a clock
-- author:      Frode Klevstul
-- start date:  31.10.2005
---------------------------------------------------------
procedure clock(p_style in varchar2 default 'normal', p_form in varchar2 default 'form', p_field in varchar2 default 'p_clock') is begin
	ext_lob.pri(htm_lib.clock(p_style, p_form, p_field));
end;

function clock
(
	  p_style			in varchar2			default 'normal'
	, p_form			in varchar2			default	'form'
	, p_field			in varchar2			default	'p_clock'
) return clob
is
begin
declare
	c_procedure			constant mod_procedure.name%type 	:= 'clock';

	v_html				clob;
	v_clob				clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	if (p_style = 'normal') then
		v_html := v_html||'
			<html><head><title>i4</title>'||htm.css||'</head><body>

			<SCRIPT type="text/javascript" language="JavaScript">
				function updateForm(hour, min) {
						if (window.opener.document.'||p_form||') {
							if (window.opener.document.'||p_form||'.'||p_field||') {
								window.opener.document.'||p_form||'.'||p_field||'.value = hour+'':''+min;
							}
						}
						window.close();
				}
			</SCRIPT>

			<table width="100%" style="height: 100%" border="0">
			<tr><td valign="middle" align="center">
			<nobr><form name="clockForm" action="http://x.x">
			<select name="p_hours">
			<option value="00">00</option>
			<option value="01">01</option>
			<option value="02">02</option>
			<option value="03">03</option>
			<option value="04">04</option>
			<option value="05">05</option>
			<option value="06">06</option>
			<option value="07">07</option>
			<option value="08">08</option>
			<option value="09">09</option>
			<option value="10">10</option>
			<option value="11">11</option>
			<option value="12" selected>12</option>
			<option value="13">13</option>
			<option value="14">14</option>
			<option value="15">15</option>
			<option value="16">16</option>
			<option value="17">17</option>
			<option value="18">18</option>
			<option value="19">19</option>
			<option value="20">20</option>
			<option value="21">21</option>
			<option value="22">22</option>
			<option value="23">23</option>
			</select>
			:
			<select name="p_minutes">
			<option value="00">00</option>
			<option value="15">15</option>
			<option value="30">30</option>
			<option value="45">45</option>
			</select>
			<input type="submit" value="OK" onclick="updateForm(p_hours.value, p_minutes.value)" />
			</nobr></form>
			</td></tr>
			</table>
			</body>
			</html>
		';
	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end clock;


---------------------------------------------------------
-- name:        upload
-- what:        upload a file to database
-- author:      Frode Klevstul
-- start date:  07.11.2005
---------------------------------------------------------
procedure upload
(
	  p_noPairs			in number
	, p_parNames		in parameter_arr
	, p_parValues		in parameter_arr
	, p_notInUse		in parameter_arr
	, p_docMimeType		in out varchar2
	, p_blob			out blob
	, p_clob			out clob
	, p_nclob			out nclob
)
is
begin
declare
	c_procedure					constant mod_procedure.name%type := 'upload';

	v_id						ses_session.id%type;
	v_index						number;
	v_content_length			number;
	v_document_path				varchar2(128);
	v_file						varchar2(128);
	v_filetype					varchar2(8);
	v_url_success				varchar2(256);
	v_url_failure				varchar2(256);
	v_trg_filename				log_upload.trg_filename%type;
	v_trg_directory				log_upload.trg_directory%type;
	v_crop						varchar2(1);
	v_watermark					varchar2(1);
	v_max_file_size				sys_parameter.value%type;
	v_max_width					sys_parameter.value%type;
	v_max_height				sys_parameter.value%type;
	v_type						varchar2(16);
	v_parameters				varchar2(32767);

	v_out_file					utl_file.file_type;
	v_offset					number								:= 1;
	v_lob_len					number;

	v_organisation_pk			org_organisation.org_organisation_pk%type;

	v_debug						boolean								:= false;
begin
	ses_lib.ini(g_package, c_procedure);

	if (v_debug) then
		htp.p('<b>htm_lib.upload:</b><br />');
		htp.p('p_noPairs: '||p_noPairs||'<br />');
		htp.p('p_docMimeType: '||p_docMimeType||'<br />');
	end if;

	for v_index in 1 .. p_parNames.count
	loop
		if ( p_parNames(v_index) is not null ) then

			if (p_parNames(v_index) = 'content_length') then
				v_content_length	:= p_parValues(v_index);
			elsif (p_parNames(v_index) = 'p_type') then
				v_type				:= p_parValues(v_index);
			elsif (p_parNames(v_index) = 'document_path') then
				v_document_path			:= p_parValues(v_index);
			elsif (p_parNames(v_index) = 'p_url_success') then
				v_url_success		:= p_parValues(v_index);
			elsif (p_parNames(v_index) = 'p_url_failure') then
				v_url_failure		:= p_parValues(v_index);
			elsif (p_parNames(v_index) = 'p_trg_filename') then
				v_trg_filename		:= p_parValues(v_index);
			elsif (p_parNames(v_index) = 'p_trg_directory') then
				v_trg_directory		:= p_parValues(v_index);
			elsif (p_parNames(v_index) = 'p_crop') then
				v_crop				:= p_parValues(v_index);
			elsif (p_parNames(v_index) = 'p_watermark') then
				v_watermark			:= p_parValues(v_index);
			elsif (p_parNames(v_index) = 'p_file_1') then
				v_file				:= p_parValues(v_index);
			elsif (p_parNames(v_index) = 'p_file_2') then
				v_file				:= p_parValues(v_index);
			elsif (p_parNames(v_index) = 'p_file_3') then
				v_file				:= p_parValues(v_index);
			elsif (p_parNames(v_index) = 'p_file_4') then
				v_file				:= p_parValues(v_index);
			elsif (p_parNames(v_index) = 'p_file_5') then
				v_file				:= p_parValues(v_index);
			elsif (p_parNames(v_index) = 'p_file_6') then
				v_file				:= p_parValues(v_index);
			elsif (p_parNames(v_index) = 'p_file_7') then
				v_file				:= p_parValues(v_index);
			elsif (p_parNames(v_index) = 'p_file_8') then
				v_file				:= p_parValues(v_index);
			elsif (p_parNames(v_index) = 'p_file_9') then
				v_file				:= p_parValues(v_index);
			elsif (p_parNames(v_index) = 'p_file_10') then
				v_file				:= p_parValues(v_index);
			elsif (p_parNames(v_index) = 'p_max_file_size') then
				v_max_file_size		:= p_parValues(v_index);
			elsif (p_parNames(v_index) = 'p_max_width') then
				v_max_width			:= p_parValues(v_index);
			elsif (p_parNames(v_index) = 'p_max_height') then
				v_max_height		:= p_parValues(v_index);
			-- ---------------------------------------------------------------------------
			-- all extra parameters are stored in v_parameters so we have the option
			-- to add these parameters to both success and failure url
			-- ---------------------------------------------------------------------------
			else
				v_parameters		:= v_parameters||''||p_parNames(v_index)||'='|| p_parValues(v_index)||'&';
			end if;

			if (v_debug) then
				htp.p(p_parNames(v_index)||'='||p_parValues(v_index)||'<br />');
			end if;
		end if;
	end loop;

	-- -----------------------------------------------------
	-- if final call we have finished uploading all files,
	-- time for response page
	-- -----------------------------------------------------
	if ( p_docMimeType is null ) then

		if (v_content_length is not null) then

			if (v_debug) then
				htp.p('<font size="2"><b>ACTION:</b> write to file</font><br />');
			end if;

			-- ----------------------------------------------------
			-- replace [parameters] in urls with v_parameters
			-- ----------------------------------------------------
			v_url_success := replace(lower(v_url_success), '[parameters]', v_parameters);
			v_url_failure := replace(lower(v_url_failure), '[parameters]', v_parameters);

			-- call function to write file to OS
			sys_lib.writeToFile(
				  v_type
				, to_number(v_max_width)
				, to_number(v_max_height)
				, to_number(v_max_file_size)
				, v_url_success
				, v_url_failure
				, to_number(v_crop)
				, to_number(v_watermark)
			);

		-- ------------------------------------------------
		-- no data uploaded
		-- ------------------------------------------------
		else
			if (v_debug) then
				htp.p('<font size="2"><b>ACTION:</b> jump to '||v_url_failure||'</font><br />');
			end if;

			htm.jumpTo(v_url_failure, 1, null);
		end if;

	-- -----------------------------------------------------
	-- if we haven't received v_content_length yet we're in
	-- the process of uploading a file
	-- -----------------------------------------------------
	else
		if (v_debug) then
			htp.p('<font size="2"><b>ACTION:</b> upload file and store in db</font><br />');
		end if;
		v_id := ses_lib.getId;

		-- -------------------------------------------------------------
		-- only do the insert into the database if a file is provided,
		-- the user might submit the upload form without adding any
		-- files
		-- -------------------------------------------------------------
		if (v_file is not null) then

			-- get filetype from filename
			-- for reference check: http://forums.oracle.com/forums/thread.jspa?threadID=342368
			select regexp_substr(lower(v_file), '[^\.]*$') into v_filetype from dual;

			-- in IE v_file is the full path, in FireFox it's the filename only
			-- do a regExp to only keep the filename

			-- replace '/' with '\'
			select regexp_replace(v_file, '/', '\') into v_file from dual;

			-- replace everything in front of the last '/' with nothing
			select regexp_replace(v_file, '^(.*\\)', '') into v_file from dual;

			insert into log_upload
			(log_upload_pk, content, src_filename, trg_filename, trg_directory, bool_written, ses_session_id, date_insert)
			values (
				  seq_log_upload.nextval
				, EMPTY_BLOB()
				, v_file
				, v_trg_filename||'.'||v_filetype
				, v_trg_directory
				, null
				, v_id
				, sysdate
				)
				returning content into p_blob;

		end if;
	end if;

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end upload;


---------------------------------------------------------
-- name:        HTMLFix
-- what:        Fixes HTML code for FCKeditor
-- author:      Frode Klevstul
-- start date:  12.12.2005
---------------------------------------------------------
function HTMLFix
(
	  p_code			in clob
) return clob
is
begin
declare
	c_procedure			constant mod_procedure.name%type 	:= 'HTMLFix';

	v_html				clob;
begin
	ses_lib.ini(g_package, c_procedure);

	v_html := p_code;

	-- replace into different characters to avoid FCKeditor interpreting them as HTML
	v_html := replace(v_html, '&lt;', '&lsaquo;');
	v_html := replace(v_html, '&gt;', '&rsaquo;');
	v_html := replace(v_html, '''', '&#146;');
	v_html := replace(v_html, chr(10), '');															-- carriage return
	v_html := replace(v_html, chr(13), '');															-- carriage return

	return v_html;

end;
end HTMLFix;


---------------------------------------------------------
-- name:        HTMLEncode
-- what:        Encodes characters into HTML code
-- author:      Frode Klevstul
-- start date:  01.02.2006
---------------------------------------------------------
function HTMLEncode
(
	  p_code			in varchar2
) return clob
is
begin
declare
	c_procedure			constant mod_procedure.name%type 	:= 'HTMLEncode';

	v_html				clob;
begin
	ses_lib.ini(g_package, c_procedure);

	v_html := p_code;

	v_html := replace(v_html, '&', '&amp;');														-- has to be first
	v_html := replace(v_html, '<', '&lt;');
	v_html := replace(v_html, '>', '&gt;');
	v_html := replace(v_html, '"', '&#34;');
	v_html := replace(v_html, '�', '&AElig;');
	v_html := replace(v_html, '�', '&aelig;');
	v_html := replace(v_html, '�', '&Oslash;');
	v_html := replace(v_html, '�', '&oslash;');
	v_html := replace(v_html, '�', '&Aring;');
	v_html := replace(v_html, '�', '&aring;');

	return v_html;

end;
end HTMLEncode;


---------------------------------------------------------
-- name:        selectService
-- what:        HTML select form for service
-- author:      Frode Klevstul
-- start date:  27.12.2005
---------------------------------------------------------
procedure selectService(p_ser_service_pk ser_service.ser_service_pk%type default null) is begin
	ext_lob.pri(htm_lib.selectService(p_ser_service_pk));
end;

function selectService
(
	p_ser_service_pk		ser_service.ser_service_pk%type		default null
) return clob
is
begin
declare
	c_procedure			constant mod_procedure.name%type 	:= 'selectService';

	cursor	cur_ser_service is
	select	ser_service_pk, name, domain
	from	ser_service
	where	bool_active > 0;

	v_html				clob;
	v_clob				clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := v_html||'<select name="p_service_pk">'||chr(13);
	for r_cursor in cur_ser_service
	loop
		if (p_ser_service_pk = r_cursor.ser_service_pk) then
			v_html := v_html||'<option value="'||r_cursor.ser_service_pk||'" selected>'||r_cursor.name||' ('||r_cursor.domain||')</option>'||chr(13);
		else
			v_html := v_html||'<option value="'||r_cursor.ser_service_pk||'">'||r_cursor.name||' ('||r_cursor.domain||')</option>'||chr(13);
		end if;
	end loop;
	v_html := v_html||'</select>'||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end selectService;


---------------------------------------------------------
-- name:        selectOrgStatus
-- what:        HTML select form for org status
-- author:      Frode Klevstul
-- start date:  27.12.2005
---------------------------------------------------------
procedure selectOrgStatus(p_org_status_pk org_status.org_status_pk%type default null) is begin
	ext_lob.pri(htm_lib.selectOrgStatus(p_org_status_pk));
end;

function selectOrgStatus
(
	p_org_status_pk			org_status.org_status_pk%type		default null
) return clob
is
begin
declare
	c_procedure			constant mod_procedure.name%type 	:= 'selectOrgStatus';

	cursor	cur_select is
	select	org_status_pk, name
	from	org_status
	order by org_status_pk asc;

	v_html				clob;
	v_clob				clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := v_html||'<select name="p_org_status_pk">'||chr(13);
	for r_cursor in cur_select
	loop

		if (p_org_status_pk = r_cursor.org_status_pk) then
			v_html := v_html||'<option value="'||r_cursor.org_status_pk||'" selected>'||r_cursor.name||'</option>'||chr(13);
		else
			v_html := v_html||'<option value="'||r_cursor.org_status_pk||'">'||r_cursor.name||'</option>'||chr(13);
		end if;
	end loop;
	v_html := v_html||'</select>'||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end selectOrgStatus;


---------------------------------------------------------
-- name:        selectBool
-- what:        HTML select form for bool (yes / no)
-- author:      Frode Klevstul
-- start date:  27.12.2005
---------------------------------------------------------
procedure selectBool(p_choice number default null, p_no number default null) is begin
	ext_lob.pri(htm_lib.selectBool(p_choice, p_no));
end;

function selectBool
(
	  p_choice		number								default null
	, p_no			number								default null
) return clob
is
begin
declare
	c_procedure			constant mod_procedure.name%type 	:= 'selectBool';

	v_html				clob;
	v_clob				clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := v_html||'<select name="p_bool'||p_no||'">'||chr(13);

	v_html := v_html||'<option value="0">no</option>'||chr(13);

	if (p_choice > 0) then
		v_html := v_html||'<option value="1" selected>yes</option>'||chr(13);
	else
		v_html := v_html||'<option value="1">yes</option>'||chr(13);
	end if;

	v_html := v_html||'</select>'||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end selectBool;


---------------------------------------------------------
-- name:        selectUseStatus
-- what:        HTML select form for user status
-- author:      Frode Klevstul
-- start date:  28.12.2005
---------------------------------------------------------
procedure selectUseStatus(p_use_status_pk use_status.use_status_pk%type default null) is begin
	ext_lob.pri(htm_lib.selectUseStatus(p_use_status_pk));
end;

function selectUseStatus
(
	p_use_status_pk			use_status.use_status_pk%type		default null
) return clob
is
begin
declare
	c_procedure			constant mod_procedure.name%type 	:= 'selectUseStatus';

	cursor	cur_select is
	select	use_status_pk, name
	from	use_status;

	v_html				clob;
	v_clob				clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := v_html||'<select name="p_use_status_pk">'||chr(13);
	for r_cursor in cur_select
	loop
		if (p_use_status_pk = r_cursor.use_status_pk) then
			v_html := v_html||'<option value="'||r_cursor.use_status_pk||'" selected>'||r_cursor.name||'</option>'||chr(13);
		else
			v_html := v_html||'<option value="'||r_cursor.use_status_pk||'">'||r_cursor.name||'</option>'||chr(13);
		end if;
	end loop;
	v_html := v_html||'</select>'||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end selectUseStatus;


---------------------------------------------------------
-- name:        selectUseType
-- what:        HTML select form for user type
-- author:      Frode Klevstul
-- start date:  28.12.2005
---------------------------------------------------------
procedure selectUseType(p_use_type_pk use_type.use_type_pk%type default null) is begin
	ext_lob.pri(htm_lib.selectUseType(p_use_type_pk));
end;

function selectUseType
(
	p_use_type_pk			use_type.use_type_pk%type		default null
) return clob
is
begin
declare
	c_procedure			constant mod_procedure.name%type 	:= 'selectUseType';

	cursor	cur_select is
	select	use_type_pk, name, description
	from	use_type
	where	weight < 100
	and		name not like 'no_login'
	and		name not like 'org_full'
	order by use_type_pk asc;

	v_html				clob;
	v_clob				clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := v_html||'<select name="p_use_type_pk">'||chr(13);
	for r_cursor in cur_select
	loop
		if (p_use_type_pk = r_cursor.use_type_pk) then
			v_html := v_html||'<option value="'||r_cursor.use_type_pk||'" selected>'||r_cursor.name||' ('||r_cursor.description||')</option>'||chr(13);
		else
			v_html := v_html||'<option value="'||r_cursor.use_type_pk||'">'||r_cursor.name||' ('||r_cursor.description||')</option>'||chr(13);
		end if;
	end loop;
	v_html := v_html||'</select>'||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end selectUseType;


---------------------------------------------------------
-- name:        selectOrganisation
-- what:        HTML select form for organisation
-- author:      Frode Klevstul
-- start date:  28.12.2005
---------------------------------------------------------
procedure selectOrganisation(p_type varchar2 default null, p_org_organisation_pk org_organisation.org_organisation_pk%type default null, p_service_pk ser_service.ser_service_pk%type default null, p_name org_organisation.name%type default null) is begin
	ext_lob.pri(htm_lib.selectOrganisation(p_type, p_org_organisation_pk, p_service_pk, p_name));
end;

function selectOrganisation
(
	  p_type				varchar2									default null
	, p_org_organisation_pk	org_organisation.org_organisation_pk%type	default null
	, p_service_pk			ser_service.ser_service_pk%type				default null
	, p_name				org_organisation.name%type					default null
) return clob
is
begin
declare
	c_procedure						constant mod_procedure.name%type 	:= 'selectOrganisation';

	cur_dynCursor					sys_refcursor;

	v_organisation_pk				org_organisation.org_organisation_pk%type;
	v_name							org_organisation.name%type;
	v_service_pk					ser_service.ser_service_pk%type;
	v_bool_umbrella_organisation	org_organisation.bool_umbrella_organisation%type;
	v_zip							add_address.zip%type;
	v_i								number								:= 0;
	v_selectText					varchar2(256);

	v_html							clob;
	v_clob							clob;
	v_sql_stmt						varchar2(2048);

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	-- -----------------------------------------------------------
	-- no_login : organisations that hasn't got a user related
	-- -----------------------------------------------------------
	if (p_type = 'no_login') then

		if (p_service_pk is not null) then
			v_service_pk := p_service_pk;
		else
			v_service_pk := ses_lib.getSer(ses_lib.getId);
		end if;

		v_sql_stmt := ' select	org_organisation_pk, o.name, bool_umbrella_organisation, zip'||
					'	from	org_organisation o, org_ser, add_address'||
					'	where	org_organisation_fk_pk = org_organisation_pk'||
					'	and		(bool_umbrella_organisation is null or bool_umbrella_organisation = 0)'||	-- we don't want umbrella organisations
					'	and		org_organisation_pk not in ('||												-- we want orgs that don't have any users related
					'		select	org_organisation_fk_pk'||
					'		from	use_org'||
					'	) and ser_service_fk_pk in ('||
					'		select	ser_service_pk'||
					'		from	ser_service'||
					'		where	ser_service_pk = '||v_service_pk||
					'	)'||
					'	and add_address_fk = add_address_pk'||
					'	and org_status_fk = (select org_status_pk from org_status where name = ''member'')'||
					'	and lower(o.name) like lower(''%'||p_name||'%'') '||
					'	order by org_organisation_pk desc';

	-- -----------------------------------------------------------
	-- has_login : organisations has a user related
	-- -----------------------------------------------------------
	elsif (p_type = 'has_login') then

		if (p_service_pk is not null) then
			v_service_pk := p_service_pk;
		else
			v_service_pk := ses_lib.getSer(ses_lib.getId);
		end if;

		v_sql_stmt := ' select	org_organisation_pk, o.name, bool_umbrella_organisation, zip'||
					'	from	org_organisation o, org_ser, add_address'||
					'	where	org_organisation_fk_pk = org_organisation_pk'||
					'	and		(bool_umbrella_organisation is null or bool_umbrella_organisation = 0)'||	-- we don't want umbrella organisations
					'	and		org_organisation_pk in ('||												-- we want orgs that don't have any users related
					'		select	org_organisation_fk_pk'||
					'		from	use_org'||
					'	) and ser_service_fk_pk in ('||
					'		select	ser_service_pk'||
					'		from	ser_service'||
					'		where	ser_service_pk = '||v_service_pk||
					'	)'||
					'	and add_address_fk = add_address_pk'||
					'	and org_status_fk = (select org_status_pk from org_status where name = ''member'')'||
					'	and lower(o.name) like lower(''%'||p_name||'%'') '||
					'	order by org_organisation_pk desc';

	-- -----------------------------------------------------------
	-- non_umbrella : all non umbrella organisations
	-- -----------------------------------------------------------
	elsif (p_type = 'non_umbrella') then

		v_sql_stmt := ' select	org_organisation_pk, o.name, bool_umbrella_organisation, zip'||
					'	from	org_organisation o, add_address'||
					'	where	(bool_umbrella_organisation is null or bool_umbrella_organisation = 0)'||
					'	and add_address_fk = add_address_pk'||
					'	and lower(o.name) like lower(''%'||p_name||'%'') '||
					'	order by o.name asc';

	-- -----------------------------------------------------------
	-- all members organisations (non umbrella)
	-- -----------------------------------------------------------
	elsif (p_type = 'members') then

		v_sql_stmt := ' select	org_organisation_pk, o.name, bool_umbrella_organisation, zip'||
					'	from	org_organisation o, add_address'||
					'	where	(bool_umbrella_organisation is null or bool_umbrella_organisation = 0)'||
					'	and		org_status_fk = (select org_status_pk from org_status where name = ''member'')'||
					'	and add_address_fk = add_address_pk'||
					'	and lower(o.name) like lower(''%'||p_name||'%'') '||
					'	order by o.name asc';

	-- -----------------------------------------------------------
	-- umbrella organisations
	-- -----------------------------------------------------------
	elsif (p_type = 'umbrella') then

		v_sql_stmt := ' select	org_organisation_pk, o.name, bool_umbrella_organisation, zip'||
					'	from	org_organisation o, add_address'||
					'	where bool_umbrella_organisation > 0'||
					'	and add_address_fk = add_address_pk'||
					'	and lower(o.name) like lower(''%'||p_name||'%'') '||
					'	order by o.date_insert desc';

	-- -----------------------------------------------------------
	-- all organisations
	-- -----------------------------------------------------------
	elsif (p_type = 'all') then

		v_sql_stmt := ' select	org_organisation_pk, o.name, bool_umbrella_organisation, zip'||
					'	from	org_organisation o, add_address'||
					'	where add_address_fk = add_address_pk'||
					'	and lower(o.name) like lower(''%'||p_name||'%'') '||
					'	order by o.name asc';

	end if;

	-- ------------------------------
	-- print out select box
	-- ------------------------------
	v_html := v_html||'<select name="p_organisation_pk">'||chr(13);

	open cur_dynCursor for v_sql_stmt;
	loop
		v_i := v_i + 1;
		fetch cur_dynCursor into v_organisation_pk, v_name, v_bool_umbrella_organisation, v_zip;
		exit when cur_dynCursor%notfound;
		exit when v_i = 2000;																		-- have to exit after v_i rows to prevent everything from hanging.

		v_selectText := htm_lib.HTMLEncode(v_name)||' ::: '||v_zip||' '||geo_lib.getCity(v_zip)||' ::: PK='||v_organisation_pk;
		if (v_bool_umbrella_organisation is not null and v_bool_umbrella_organisation > 0) then
			v_selectText := v_selectText||' ::: Parent organisation';
		end if;

		if (p_org_organisation_pk = v_organisation_pk) then
			v_html := v_html||'<option value="'||v_organisation_pk||'" selected>'||v_selectText||'</option>'||chr(13);
		else
			v_html := v_html||'<option value="'||v_organisation_pk||'">'||v_selectText||'</option>'||chr(13);
		end if;

		ext_lob.add(v_clob, v_html);
	end loop;
	close cur_dynCursor;

	v_html := v_html||'</select>'||chr(13);

	if (v_i = 2000) then
		v_html := v_html||'Error: Too many organisations! Stopped after '||v_i||' hits.'||chr(13);
	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end selectOrganisation;


---------------------------------------------------------
-- name:        selectConSubtype
-- what:        HTML select form for content subtype
-- author:      Frode Klevstul
-- start date:  28.12.2005
---------------------------------------------------------
procedure selectConSubtype(p_con_subtype_pk con_subtype.con_subtype_pk%type default null) is begin
	ext_lob.pri(htm_lib.selectConSubtype(p_con_subtype_pk));
end;

function selectConSubtype
(
	p_con_subtype_pk			con_subtype.con_subtype_pk%type		default null
) return clob
is
begin
declare
	c_procedure			constant mod_procedure.name%type 	:= 'selectConSubtype';

	cursor	cur_select is
	select	con_subtype_pk, name
	from	con_subtype;

	v_html				clob;
	v_clob				clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := v_html||'<select name="p_con_subtype_pk" id="p_con_subtype_pk">'||chr(13);
	for r_cursor in cur_select
	loop
		if (p_con_subtype_pk = r_cursor.con_subtype_pk) then
			v_html := v_html||'<option value="'||r_cursor.con_subtype_pk||'" selected>'||tex_lib.get('db','con_subtype',r_cursor.name)||'</option>'||chr(13);
		else
			v_html := v_html||'<option value="'||r_cursor.con_subtype_pk||'">'||tex_lib.get('db','con_subtype',r_cursor.name)||'</option>'||chr(13);
		end if;
	end loop;
	v_html := v_html||'</select>'||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end selectConSubtype;


---------------------------------------------------------
-- name:        selectRating
-- what:        HTML select form for content subtype
-- author:      Frode Klevstul
-- start date:  28.12.2005
---------------------------------------------------------
procedure selectRating(p_maxRating number default null, p_rating number default null) is begin
	ext_lob.pri(htm_lib.selectRating(p_rating));
end;

function selectRating
(
	  p_maxRating			number								default null
	, p_rating				number								default null
) return clob
is
begin
declare
	c_procedure			constant mod_procedure.name%type 	:= 'selectRating';

	v_i					number;

	v_html				clob;
	v_clob				clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := v_html||'<select name="p_rating" id="p_rating">'||chr(13);

	for v_i in 1 .. p_maxRating
	loop
		if (p_rating = v_i) then
			v_html := v_html||'<option value="'||v_i||'" selected>'||v_i||'</option>'||chr(13);
		else
			v_html := v_html||'<option value="'||v_i||'">'||v_i||'</option>'||chr(13);
		end if;
	end loop;

	v_html := v_html||'</select>'||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end selectRating;


---------------------------------------------------------
-- name:        selectContent
-- what:        HTML select form for content
-- author:      Frode Klevstul
-- start date:  28.12.2005
---------------------------------------------------------
procedure selectContent(p_content_pk con_content.con_content_pk%type default null, p_organisation_pk org_organisation.org_organisation_pk%type default null, p_id number default null) is begin
	ext_lob.pri(htm_lib.selectContent(p_content_pk, p_organisation_pk, p_id));
end;

function selectContent
(
	  p_content_pk			con_content.con_content_pk%type				default null
	, p_organisation_pk		org_organisation.org_organisation_pk%type	default null
	, p_id					number										default null
) return clob
is
begin
declare
	c_procedure			constant mod_procedure.name%type 	:= 'selectContent';

	cursor	cur_content is
	select	con_content_pk, substr(subject, 1, 30) as subject, name
	from	con_type, con_content
	where	con_type_pk = con_type_fk
	and		org_organisation_fk = p_organisation_pk
	and		con_status_fk = (select con_status_pk from con_status where name = 'published')
	order by name, date_publish;

	v_html				clob;
	v_clob				clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := v_html||'<select name="p_content_pk_'||p_id||'" id="p_content_pk_'||p_id||'">'||chr(13);
	v_html := v_html||'<option value="">&nbsp;</option>'||chr(13);
	for r_cursor in cur_content
	loop
		if (p_content_pk = r_cursor.con_content_pk) then
			v_html := v_html||'<option value="'||r_cursor.con_content_pk||'" selected>'||r_cursor.con_content_pk||' ('||r_cursor.name||'): '||r_cursor.subject||'</option>'||chr(13);
		else
			v_html := v_html||'<option value="'||r_cursor.con_content_pk||'">'||r_cursor.con_content_pk||' ('||r_cursor.name||'): '||r_cursor.subject||'</option>'||chr(13);
		end if;
	end loop;
	v_html := v_html||'</select>'||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end selectContent;


---------------------------------------------------------
-- name:        selectGeography
-- what:        HTML select form for content
-- author:      Frode Klevstul
-- start date:  28.12.2005
---------------------------------------------------------
procedure selectGeography
(
	  p_geography_pk		geo_geography.geo_geography_pk%type				default null
	, p_parent				geo_geography.name%type							default null
	, p_id					number											default null
)is begin
	ext_lob.pri(htm_lib.selectGeography(p_geography_pk, p_parent, p_id));
end;

function selectGeography
(
	  p_geography_pk		geo_geography.geo_geography_pk%type				default null
	, p_parent				geo_geography.name%type							default null
	, p_id					number											default null
) return clob
is
begin
declare
	c_procedure			constant mod_procedure.name%type 	:= 'selectGeography';

	cursor	cur_geography is
	select	geo_geography_pk, name
	from	geo_geography
	where	parent_geo_geography_fk = (select geo_geography_pk from geo_geography where lower(name) = lower(p_parent))
	order by name;

	v_html				clob;
	v_clob				clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := v_html||'<select name="p_geography_pk_'||p_id||'">'||chr(13);
	v_html := v_html||'<option value="">&nbsp;</option>'||chr(13);
	for r_cursor in cur_geography
	loop
		if (p_geography_pk = r_cursor.geo_geography_pk) then
			v_html := v_html||'<option value="'||r_cursor.geo_geography_pk||'" selected>'||r_cursor.name||'</option>'||chr(13);
		else
			v_html := v_html||'<option value="'||r_cursor.geo_geography_pk||'">'||r_cursor.name||'</option>'||chr(13);
		end if;
	end loop;
	v_html := v_html||'</select>'||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end selectGeography;


---------------------------------------------------------
-- name:        pathField
-- what:        returns path field
-- author:      Frode Klevstul
-- start date:  05.03.2006
---------------------------------------------------------
procedure pathField
(
	  p_package			in mod_package.name%type
	, p_procedure		in mod_procedure.name%type
) is begin
	ext_lob.pri(htm_lib.pathField(p_package, p_procedure));
end;

function pathField
(
	  p_package			in mod_package.name%type
	, p_procedure		in mod_procedure.name%type
) return clob
is
begin
declare
	c_procedure			constant mod_procedure.name%type 	:= 'pathField';

	c_tex_frontPage		constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'frontPage');
	c_tex_historyBack	constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'historyBack');
	c_tex_viewOrg		constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'viewOrg');
	c_tex_viewCon		constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'viewCon');
	c_tex_viewAlbum		constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'viewAlbum');
	c_tex_viewPicture	constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'viewPicture');

	v_links				varchar2(1024);
	v_queryString		varchar2(1024)							:= owa_util.get_cgi_env('QUERY_STRING');

	v_tmpText			varchar2(1024);
	v_organisation_pk	org_organisation.org_organisation_pk%type;

	cursor	cur_con_content
	(
		b_con_content_pk con_content.con_content_pk%type
	)is
	select	org_organisation_fk
	from	con_content
	where	con_content_pk = b_con_content_pk;

	v_html				clob;
	v_clob				clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	-- -----------------------
	-- link to frontPage
	-- -----------------------
	if (p_package = 'pag_pub' and p_procedure = 'frontPage') then
		v_links := '[frontPage] ';
	else
		v_links := '<a href="pag_pub.frontPage">[frontPage]</a> ';
	end if;

	-- -----------------------
	-- ORG
	-- -----------------------
	if (p_package = 'org_pub' and p_procedure = 'viewOrg') then
		v_links := v_links||'&raquo; [viewOrg] ';

	-- -----------------------
	-- CON
	-- -----------------------
	elsif (p_package = 'con_pub' and p_procedure = 'viewCon') then
		v_tmpText :=
			substr(
				  v_queryString
				, instr(v_queryString, 'p_content_pk=')+13
				, length(v_queryString)
			);

		for r_cursor in cur_con_content(to_number(v_tmpText))
		loop
			v_organisation_pk := r_cursor.org_organisation_fk;
		end loop;
		v_links := v_links||'&raquo; <a href="org_pub.viewOrg?p_organisation_pk='||v_organisation_pk||'">[viewOrg]</a> &raquo; [viewCon] ';

	-- -----------------------
	-- PIC
	-- -----------------------
	elsif (p_package = 'pic_pub' and (p_procedure = 'viewAlbums' or p_procedure = 'viewPicture')) then

		-- strip all in front of org number
		v_tmpText :=
			substr(
				  v_queryString
				, instr(v_queryString, 'p_organisation_pk=')+18
				, length(v_queryString)
			);

		-- remove "&" and out...
		v_tmpText :=
			substr(
				  v_tmpText
				, 1
				, instr(v_tmpText, '&')-1
			);

		v_organisation_pk := to_number(v_tmpText);

		if (p_procedure = 'viewAlbums') then
			v_links := v_links||'&raquo; <a href="org_pub.viewOrg?p_organisation_pk='||v_organisation_pk||'">[viewOrg]</a> &raquo; [viewAlbum] ';
		elsif (p_procedure = 'viewPicture') then
			v_tmpText := v_queryString;
			owa_pattern.change(v_tmpText, '.*p_album_pk=', '', 'g');
			owa_pattern.change(v_tmpText, '&.*', '', 'g');
			v_links := v_links||'&raquo; <a href="org_pub.viewOrg?p_organisation_pk='||v_organisation_pk||'">[viewOrg]</a> &raquo; <a href="pic_pub.viewAlbum?p_organisation_pk='||v_organisation_pk||'&amp;p_album_pk='||v_tmpText||'">[viewAlbum]</a> &raquo; [viewPicture]';
		end if;
	end if;

	-- -----------------------
	-- go-back link
	-- -----------------------
	if (p_package is not null) then
		v_links := v_links||' <span style="padding-left: 40px;">&nbsp;</span>[[goBack]]';
	end if;

	-- -----------------------
	-- replace text
	-- -----------------------
	v_links := replace(v_links, '[goBack]', '<a href="javascript:history.go(-1)">'||c_tex_historyBack||'</a>');
	v_links := replace(v_links, '[frontPage]', c_tex_frontPage);
	v_links := replace(v_links, '[viewOrg]', c_tex_viewOrg);
	v_links := replace(v_links, '[viewCon]', c_tex_viewCon);
	v_links := replace(v_links, '[viewAlbum]', c_tex_viewAlbum);
	v_links := replace(v_links, '[viewPicture]', c_tex_viewPicture);

	-- -----------------------
	-- generate HTML
	-- -----------------------
	v_html := v_html||'
		<table width="100%" class="pathField" border="1">
		<tr>
			<td style="padding-left: 10px; font-size: 9px;" width="50%">
			'||v_links||'
			</td>
			<td style="padding-right: 15px; font-size: 9px; text-align: right;">
				<div id="clock"></div>
			</td>
		</tr>
		</table>
	';

	-- ------------------
	-- start clock
	-- ------------------
	v_html := v_html||'<script type="text/javascript" language="javascript">StartClock24();</script>'||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end pathField;


---------------------------------------------------------
-- name:        selectLanguage
-- what:        HTML select form for language
-- author:      Frode Klevstul
-- start date:  14.03.2006
---------------------------------------------------------
procedure selectLanguage
(
	  p_language_pk			tex_language.tex_language_pk%type				default null
)is begin
	ext_lob.pri(htm_lib.selectLanguage(p_language_pk));
end;

function selectLanguage
(
	  p_language_pk			tex_language.tex_language_pk%type				default null
) return clob
is
begin
declare
	c_procedure			constant mod_procedure.name%type 	:= 'selectLanguage';

	cursor	cur_language is
	select	tex_language_pk
	from	tex_language;

	v_html				clob;
	v_clob				clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := v_html||'<select name="p_language_pk">'||chr(13);
	v_html := v_html||'<option value="">&nbsp;</option>'||chr(13);
	for r_cursor in cur_language
	loop
		if (p_language_pk = r_cursor.tex_language_pk) then
			v_html := v_html||'<option value="'||r_cursor.tex_language_pk||'" selected>'||r_cursor.tex_language_pk||'</option>'||chr(13);
		else
			v_html := v_html||'<option value="'||r_cursor.tex_language_pk||'">'||r_cursor.tex_language_pk||'</option>'||chr(13);
		end if;
	end loop;
	v_html := v_html||'</select>'||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end selectLanguage;


---------------------------------------------------------
-- name:        stripHTML
-- what:        Removes HTML tags
-- author:      Frode Klevstul
-- start date:  21.04.2006
---------------------------------------------------------
function stripHTML
(
	  p_code			in varchar2
) return clob
is
begin
declare
	c_procedure			constant mod_procedure.name%type 	:= 'stripHTML';

	v_code				clob;
begin
	ses_lib.ini(g_package, c_procedure);

	v_code := p_code;

	select	regexp_replace(v_code,'<[^>]*>',' ')
	into	v_code
	from	dual;

	return v_code;

end;
end stripHTML;


---------------------------------------------------------
-- name:        stripNewline
-- what:        Removes newlines
-- author:      Frode Klevstul
-- start date:  05.07.2006
---------------------------------------------------------
function stripNewline
(
	  p_code			in varchar2
) return clob
is
begin
declare
	c_procedure			constant mod_procedure.name%type 	:= 'stripNewline';

	v_code				clob;
begin
	ses_lib.ini(g_package, c_procedure);

	v_code := p_code;

	v_code := replace(v_code, chr(13), '');
	v_code := replace(v_code, chr(10), '');

	return v_code;

end;
end stripNewline;


---------------------------------------------------------
-- name:        stripIllegalTextFromUrl
-- what:        Removes/replaces illegal characters from URLs
-- author:      Frode Klevstul
-- start date:  28.07.2006
---------------------------------------------------------
function stripIllegalTextFromUrl
(
	  p_code			in varchar2
) return clob
is
begin
declare
	c_procedure			constant mod_procedure.name%type 	:= 'stripIllegalTextFromUrl';

	v_ascii_ae1			number;
	v_ascii_oe1			number;
	v_ascii_aa1			number;
	v_ascii_ae2			number;
	v_ascii_oe2			number;
	v_ascii_aa2			number;
	v_ascii_ouml2		number;
	v_ascii_ouml1		number;
	v_ascii_s1			number;

	v_code				clob;

begin
	ses_lib.ini(g_package, c_procedure);

	-- find ascii values for special characters
	select ascii('�') into v_ascii_ae2 from dual;
	select ascii('�') into v_ascii_ae1 from dual;
	select ascii('�') into v_ascii_oe2 from dual;
	select ascii('�') into v_ascii_oe1 from dual;
	select ascii('�') into v_ascii_aa2 from dual;
	select ascii('�') into v_ascii_aa1 from dual;
	select ascii('�') into v_ascii_ouml2 from dual;
	select ascii('�') into v_ascii_ouml1 from dual;
	select ascii('�') into v_ascii_s1 from dual;

	v_code := p_code;

	-- start replacing the different characters
	v_code := replace(v_code, chr(v_ascii_s1), '');

	v_code := replace(v_code, '&Aring;',	chr(v_ascii_aa2));
	v_code := replace(v_code, '&aring;',	chr(v_ascii_aa1));
	v_code := replace(v_code, '&Oslash;',	chr(v_ascii_oe2));
	v_code := replace(v_code, '&oslash;',	chr(v_ascii_oe1));
	v_code := replace(v_code, '&AElig;',	chr(v_ascii_ae2));
	v_code := replace(v_code, '&aelig;',	chr(v_ascii_ae1));
	v_code := replace(v_code, '&Ouml;',		chr(v_ascii_ouml2));
	v_code := replace(v_code, '&ouml;',		chr(v_ascii_ouml1));

	-- replace to unicode values
	-- http://i-technica.com/whitestuff/urlencodechart.html

	v_code := replace(v_code, chr(v_ascii_ae2), '%E6');
	v_code := replace(v_code, chr(v_ascii_ae1), '%E6');
	v_code := replace(v_code, chr(v_ascii_oe2), '%F8');
	v_code := replace(v_code, chr(v_ascii_oe1), '%F8');
	v_code := replace(v_code, chr(v_ascii_aa2), '%E5');
	v_code := replace(v_code, chr(v_ascii_aa1), '%E5');
	v_code := replace(v_code, chr(v_ascii_ouml2), '%F6');
	v_code := replace(v_code, chr(v_ascii_ouml1), '%F6');

	return v_code;

end;
end stripIllegalTextFromUrl;



-- ******************************************** --
end;
/
show errors;
