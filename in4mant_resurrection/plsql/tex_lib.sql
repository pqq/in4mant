PROMPT *** tex_lib ***
set define off
set serveroutput on;
alter session set plsql_warnings='enable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package tex_lib is
/* ******************************************************
*	Package:     tex_lib
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	050811 | Frode Klevstul
*			- Package created
****************************************************** */
	procedure run;
	function get
	(
		  p_mod_name_package		in mod_package.name%type
		, p_mod_name_procedure		in mod_procedure.name%type
		, p_text					in tex_text.tex_text_pk%type
	) return tex_text.string%type;

end;
/
show errors;





-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body tex_lib is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'tex_lib';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  11.08.2005
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);
	
	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        get
-- what:        returns a string in tex_text
-- author:      Frode Klevstul
-- start date:  11.08.2005
---------------------------------------------------------
function get
(
	  p_mod_name_package		in mod_package.name%type
	, p_mod_name_procedure		in mod_procedure.name%type
	, p_text					in tex_text.tex_text_pk%type
) return tex_text.string%type
is
begin
declare
	c_procedure				constant mod_procedure.name%type 			:= 'get';

	c_id					constant ses_session.id%type 				:= ses_lib.getId;
	c_def_language_pk		constant tex_language.tex_language_pk%type 	:= sys_lib.getParameter('language');
	c_language_pk			constant tex_language.tex_language_pk%type 	:= ses_lib.getLang(ses_lib.getId);

	v_language_pk			tex_language.tex_language_pk%type;
	v_tex_text_pk			tex_text.tex_text_pk%type;
	v_string				tex_text.string%type;

	cursor cur_tex_text
	(
		  b_language_pk		tex_language.tex_language_pk%type
		, b_tex_text_pk		tex_text.tex_text_pk%type
	) is
	select 	string
	from	tex_text
	where	tex_text_pk = b_tex_text_pk
	and		tex_language_fk_pk = b_language_pk;

begin
	ses_lib.ini(g_package, c_procedure);

	if (c_language_pk is null) then
		v_language_pk := c_def_language_pk;
	else
		v_language_pk := c_language_pk;
	end if;

	if (p_text is not null) then
		v_tex_text_pk := p_mod_name_package ||'.'|| p_mod_name_procedure ||':'|| p_text;
	
		log_lib.logTex(v_tex_text_pk, v_language_pk);
	
		for r_cursor in cur_tex_text(v_language_pk, v_tex_text_pk)
		loop
		    v_string := r_cursor.string;
		end loop;
	
		if (v_string is null or ses_lib.getDebugMode(c_id) = 1) then
			v_string := '['||v_language_pk||'] '||v_tex_text_pk;
		end if;
	end if;
	
	return v_string;
end;
end get;


-- ******************************************** --
end;
/
show errors;
