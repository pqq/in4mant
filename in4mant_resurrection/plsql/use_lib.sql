PROMPT *** use_lib ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package use_lib is
/* ******************************************************
*	Package:     tex_lib
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	050814 | Frode Klevstul
*			- Package created
****************************************************** */
	procedure run;
	function getFullName
	(
		p_use_user_pk		in use_user.use_user_pk%type
	) return varchar2;
	function validEmail
	(
		p_email				in use_user.email%type
	) return boolean;
	function validUsername
	(
		p_username			in use_user.username%type
	) return boolean;
	function suggestUsername
	(
		p_username			in use_user.username%type
	) return varchar2;
	function uniqueUsernameEmail
	(
		p_usernameEmail		in use_user.email%type
	) return boolean;
	function getUsername
	(
		p_use_user_pk		in use_user.use_user_pk%type
	)	return varchar2;
	function getUsertype
	(
		p_use_user_pk		in use_user.use_user_pk%type
	)	return varchar2;
	function getPassword
	(
		p_use_user_pk		in use_user.use_user_pk%type
	)	return varchar2;
	function getEmail
	(
		p_use_user_pk		in use_user.use_user_pk%type
	)	return varchar2;
	procedure loggedInCheck
	(
		  p_package			in mod_package.name%type		default null
		, p_procedure		in mod_procedure.name%type		default null
		, p_url				in varchar2						default null
	);

end;
/
show errors;





-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body use_lib is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'use_lib';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  14.08.2005
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);
	
	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        getFullname
-- what:        returns a user's full name
-- author:      Frode Klevstul
-- start date:  14.08.2005
---------------------------------------------------------
function getFullName
(
	p_use_user_pk			in use_user.use_user_pk%type
) return varchar2
is
begin
declare
	c_procedure				constant mod_procedure.name%type 	:= 'getFullname';

	v_fullname				varchar2(128);

	cursor	cur_use_details 
	(
		b_use_user_fk_pk use_details.use_user_fk_pk%type
	)is
	select	first_name, last_name
	from	use_details
	where	use_user_fk_pk = b_use_user_fk_pk;

begin
	ses_lib.ini(g_package, c_procedure);

	for r_cursor in cur_use_details(p_use_user_pk)
	loop
		if (r_cursor.first_name is not null or r_cursor.last_name is not null) then
	    	v_fullname := r_cursor.first_name ||' '||r_cursor.last_name;
	    end if;
	end loop;

	-- return username if the user hasn't entered any name
	if (v_fullname is null) then
		v_fullname := use_lib.getUsername(p_use_user_pk);
	end if;

	return v_fullname;
end;
end getFullname;


---------------------------------------------------------
-- name:        validEmail
-- what:        checks is email is valid
-- author:      Frode Klevstul
-- start date:  04.10.2005
---------------------------------------------------------
function validEmail
(
	p_email			in use_user.email%type
) return boolean
is
begin
declare
	c_procedure			constant mod_procedure.name%type 	:= 'validEmail';

	isValid		boolean;
begin
	ses_lib.ini(g_package, c_procedure);

	if (p_email is not null) then
		isValid := regexp_like(p_email ,'([a-z]+|[0-9]+)@[[:alpha:]]+[.][a-z]{2,4}','i');
	else
		isValid := false;
	end if;

	return isValid;
end;
end validEmail;


---------------------------------------------------------
-- name:        validUsername
-- what:        checks if username is valid
-- author:      Frode Klevstul
-- start date:  04.10.2005
---------------------------------------------------------
function validUsername
(
	p_username			in use_user.username%type
) return boolean
is
begin
declare
	c_procedure			constant mod_procedure.name%type 	:= 'validUsername';

	isValid		boolean;
begin
	ses_lib.ini(g_package, c_procedure);

	if (p_username is not null) then
		isValid := regexp_like(p_username ,'.*','i');
	else
		isValid := false;
	end if;

	return isValid;
end;
end validUsername;


---------------------------------------------------------
-- name:        suggestUsername
-- what:        return suggestions for new username
-- author:      Frode Klevstul
-- start date:  04.10.2005
---------------------------------------------------------
function suggestUsername
(
	p_username			in use_user.username%type
) return varchar2
is
begin
declare
	c_procedure			constant mod_procedure.name%type 	:= 'suggestUsername';

	v_username			use_user.username%type;
	v_new_username		use_user.username%type;
	v_counter			number 			:= 1;
begin
	ses_lib.ini(g_package, c_procedure);

	v_username := p_username;
	
	-- replace 'space' with '_'
	v_username := replace(v_username, ' ', '_');
	
	-- if the username now is unique return it
	if (use_lib.uniqueUsernameEmail(v_username)) then
		return v_username;
	end if;

	-- endless loop until we find a unique name and returns that
	while (true) loop
		v_new_username := v_username ||''||v_counter;
		if (use_lib.uniqueUsernameEmail(v_new_username)) then
			return v_new_username;
		end if;
		v_counter := v_counter+1;
	end loop;
	
end;
end suggestUsername;


---------------------------------------------------------
-- name:        uniqueUsernameEmail
-- what:        checks if username is unique
-- author:      Frode Klevstul
-- start date:  04.10.2005
---------------------------------------------------------
function uniqueUsernameEmail
(
	p_usernameEmail			in use_user.email%type
) return boolean
is
begin
declare
	c_procedure			constant mod_procedure.name%type 	:= 'uniqueUsernameEmail';

	v_count				number;
begin
	ses_lib.ini(g_package, c_procedure);

	select	count(*)
	into	v_count
	from	use_user
	where	(lower(username) = lower(p_usernameEmail)) or
			(lower(email) = lower(p_usernameEmail));
	
	if (v_count > 0) then
		return false;
	else
		return true;
	end if;

end;
end uniqueUsernameEmail;


---------------------------------------------------------
-- name:        getUsername
-- what:        returns the full name give a user pk
-- author:      Frode Klevstul
-- start date:  05.10.2005
---------------------------------------------------------
function getUsername
(
	p_use_user_pk		in use_user.use_user_pk%type
)	return varchar2
is
begin
declare
	c_procedure			constant mod_procedure.name%type 	:= 'getUsername';

	c_id				constant ses_session.id%type 		:= ses_lib.getId;

	cursor	cur_use_user
	(
		b_use_user_pk	use_user.use_user_pk%type
	) is
	select	username
	from	use_user
	where	use_user_pk = b_use_user_pk;

	v_username			use_user.username%type;
begin
	ses_lib.ini(g_package, c_procedure);

	for r_cursor in cur_use_user(p_use_user_pk)
	loop
		v_username := r_cursor.username;
	end loop;
	
    return v_username;
end;
end getUsername;


---------------------------------------------------------
-- name:        getUsertype
-- what:        returns the usertype for a user
-- author:      Frode Klevstul
-- start date:  22.11.2005
---------------------------------------------------------
function getUsertype
(
	p_use_user_pk		in use_user.use_user_pk%type
)	return varchar2
is
begin
declare
	c_procedure			constant mod_procedure.name%type 	:= 'getUsertype';

	c_id				constant ses_session.id%type 		:= ses_lib.getId;

	cursor	cur_use_user
	(
		b_use_user_pk	use_user.use_user_pk%type
	) is
	select	t.name
	from	use_user u, use_type t
	where	u.use_type_fk = t.use_type_pk
	and		use_user_pk = b_use_user_pk;

	v_name				use_type.name%type;
begin
	ses_lib.ini(g_package, c_procedure);

	for r_cursor in cur_use_user(p_use_user_pk)
	loop
		v_name := r_cursor.name;
	end loop;
	
    return v_name;
end;
end getUsertype;


---------------------------------------------------------
-- name:        getPassword
-- what:        returns the password for a user
-- author:      Frode Klevstul
-- start date:  30.11.2005
---------------------------------------------------------
function getPassword
(
	p_use_user_pk		in use_user.use_user_pk%type
)	return varchar2
is
begin
declare
	c_procedure			constant mod_procedure.name%type 	:= 'getPassword';

	c_id				constant ses_session.id%type 		:= ses_lib.getId;

	cursor	cur_use_user
	(
		b_use_user_pk	use_user.use_user_pk%type
	) is
	select	password
	from	use_user
	where	use_user_pk = b_use_user_pk;

	v_password			use_user.password%type;
begin
	ses_lib.ini(g_package, c_procedure);

	for r_cursor in cur_use_user(p_use_user_pk)
	loop
		v_password := r_cursor.password;
	end loop;
	
    return v_password;
end;
end getPassword;


---------------------------------------------------------
-- name:        getEmail
-- what:        returns the email for a user
-- author:      Frode Klevstul
-- start date:  04.12.2005
---------------------------------------------------------
function getEmail
(
	p_use_user_pk		in use_user.use_user_pk%type
)	return varchar2
is
begin
declare
	c_procedure			constant mod_procedure.name%type 	:= 'getEmail';

	c_id				constant ses_session.id%type 		:= ses_lib.getId;

	cursor	cur_use_user
	(
		b_use_user_pk	use_user.use_user_pk%type
	) is
	select	email
	from	use_user
	where	use_user_pk = b_use_user_pk;

	v_email			use_user.email%type;
begin
	ses_lib.ini(g_package, c_procedure);

	for r_cursor in cur_use_user(p_use_user_pk)
	loop
		v_email := r_cursor.email;
	end loop;
	
    return v_email;
end;
end getEmail;


---------------------------------------------------------
-- name:        loggedInCheck
-- what:        checks if user is logged in
-- author:      Frode Klevstul
-- start date:  13.10.2006
---------------------------------------------------------
procedure loggedInCheck
(
	  p_package			in mod_package.name%type		default null
	, p_procedure		in mod_procedure.name%type		default null
	, p_url				in varchar2						default null
) is
begin
declare
	c_procedure			constant mod_procedure.name%type 	:= 'loggedInCheck';

	e_stop				exception;

begin
	ses_lib.ini(g_package, c_procedure);

	if not (ses_lib.timeout > 0) then
		use_pub.pleaseLogin(p_package, p_procedure, p_url);
		raise e_stop;
	end if;

exception
	when e_stop then
		raise e_stop;
end;
end loggedInCheck;


-- ******************************************** --
end;
/
show errors;
