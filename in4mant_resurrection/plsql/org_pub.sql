PROMPT *** org_pub ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package org_pub is
/* ******************************************************
*	Package:     pag_pub
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	051018 | Frode Klevstul
*			- Package created
****************************************************** */
	procedure run;
	procedure viewOrg
	(
		  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
		, p_editMode			in boolean										default false
	);	
	function viewOrg
	(
		  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
		, p_editMode			in boolean										default false
	) return clob;

end;
/
show errors;





-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body org_pub is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'org_pub';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  13.10.2005
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        viewOrg
-- what:        view org page
-- author:      Frode Klevstul
-- start date:  17.10.2005
---------------------------------------------------------
procedure viewOrg(p_organisation_pk in org_organisation.org_organisation_pk%type default null, p_editMode in boolean default false) is begin
	ext_lob.pri(org_pub.viewOrg(p_organisation_pk, false));
end;

function viewOrg
(
	  p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	, p_editMode			in boolean										default false
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type					:= 'viewOrg';

	c_login_package				constant sys_parameter.value%type					:= sys_lib.getParameter('login_package');

	c_tex_adminMode				constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'adminMode');
	c_tex_adminModeDescription	constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'adminModeDescription');
	c_tex_beChosenOrganisation	constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'beChosenOrganisation');
	c_tex_contactUs				constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'contactUs');
	c_tex_extraOptions			constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'extraOptions');
	c_tex_closedOrg				constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'closedOrg');
	c_tex_closedOrgTxt			constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'closedOrgTxt');
	c_tex_openOrg				constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'openOrg');
	c_tex_openOrgTxt			constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'openOrgTxt');

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_organisation_pk='||p_organisation_pk);
	ext_lob.ini(v_clob);

	if (p_organisation_pk is null and p_editMode) then
		use_lib.loggedInCheck(g_package, c_procedure, null);
	end if;

	if (p_editMode) then
		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'admin');
	else
		-- log hits
		log_lib.logHits('org_organisation', p_organisation_pk);
		
		v_html := v_html||''||htm.bPage(g_package, c_procedure, null);
	end if;

	if (p_editMode and p_organisation_pk is not null) then
		v_html := v_html||''||htm.bTable('surroundElements')||'
			<tr>
				<td width="50%">'||chr(13);

					v_html := v_html||''||htm.bBox(c_tex_extraOptions)||chr(13);
					v_html := v_html||''||htm.bTable||chr(13);
					v_html := v_html||'
						<tr>
							<td>
								&raquo; '||c_tex_beChosenOrganisation||' '||htm.link(c_tex_contactUs,'pag_pub.contactPage','_self', 'theButtonStyle')||'
							</td>
						</tr>
					';
					v_html := v_html||'<tr><td>&nbsp;</td></tr>'||chr(13);
					v_html := v_html||''||htm.eTable||chr(13);
					v_html := v_html||''||htm.eBox||chr(13);

		v_html := v_html||'
				</td>
				<td>&nbsp;</td>
			</tr>'||chr(13);

		v_html := v_html||'<tr><td colspan="2">&nbsp;</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);

		v_html := v_html||''||
				htm.bTable('adminModeTable')||'
					<tr>
						<td class="adminModeTable">
							<center>
							<b>'||c_tex_adminMode||'</b><br />
							<i>'||c_tex_adminModeDescription||'</i>
							</center>
						</td>
					</tr>'||
				htm.eTable||'
		';
		v_html := v_html||'<br /><br />'||chr(13);
	end if;

	if (
		-- member
		(p_organisation_pk is not null and org_lib.hasStatus(p_organisation_pk, 'member'))
		or
		-- open and in edit mode (we want all org's to be able to edit all info)
		(p_organisation_pk is not null and org_lib.hasStatus(p_organisation_pk, 'open') and p_editMode)
	) then
		v_html := v_html||''||
			htm.bTable('surroundElements')||'
				<tr>
					<td width="20%" class="surroundElements">
						'||org_lib.contactInfo(p_organisation_pk, p_editMode)||'
					</td>
					<td width="30%" class="surroundElements">
						'||org_lib.detailsInfo(p_organisation_pk, p_editMode)||'
					</td>
					<td width="25%" class="surroundElements">
						'||org_lib.mainPhoto(p_organisation_pk, p_editMode)||'
					</td>
					<td class="surroundElements">
						'||org_lib.showMap(p_organisation_pk)||'
					</td>
				</tr>
				<tr>
					<td class="surroundElements">
						'||org_lib.typeOfOrg(p_organisation_pk, p_editMode)||'
					</td>
					<td colspan="2" rowspan="3" class="surroundElements">
						'||org_lib.generalInformation(p_organisation_pk, p_editMode)||'
					</td>
					<td class="surroundElements">
						'||org_lib.viewContent(p_organisation_pk, 'event_org', p_editMode)||'
					</td>
				</tr>
				<tr>
					<td class="surroundElements">
						'||org_lib.attractions(p_organisation_pk, p_editMode)||'
					</td>
					<td rowspan="2" class="surroundElements">
						'||org_lib.viewContent(p_organisation_pk, 'news_org', p_editMode)||'
					</td>
				</tr>
				<tr>
					<td class="surroundElements">
						'||org_lib.orgValues(p_organisation_pk, p_editMode)||'
					</td>
				</tr>
				<tr>
					<td colspan="4" class="surroundElements">
						'||org_lib.pictures(p_organisation_pk, p_editMode)||'
					</td>
				</tr>
				<tr>
					<td class="surroundElements">
						&nbsp;
					</td>
					<td colspan="2" class="surroundElements">
						'||org_lib.sameParentOrg(p_organisation_pk)||'
					</td>
					<td class="surroundElements">
						&nbsp;
					</td>
				</tr>

		';
		v_html := v_html||''||htm.eTable||chr(13);

	elsif (p_organisation_pk is not null and org_lib.hasStatus(p_organisation_pk, 'open')) then
		v_html := v_html||''||
			htm.bTable('surroundElements')||'
				<tr>
					<td rowspan="2" width="25%" class="surroundElements">
						'||org_lib.contactInfo(p_organisation_pk, p_editMode)||'
					</td>
					<td class="orgStatusOpen">
						<span class="orgStatusHeading">'||c_tex_openOrg||'</span><br /><br /><br />
						'||c_tex_openOrgTxt||'<br />
					</td>
					<td rowspan="2" width="25%" class="surroundElements">
						'||org_lib.showMap(p_organisation_pk)||'
					</td>
				</tr>
				<tr>
					<td class="surroundElements">
						'||org_lib.generalInformation(p_organisation_pk, p_editMode)||'
					</td>
				</tr>
		';
		v_html := v_html||''||htm.eTable||chr(13);

	elsif (p_organisation_pk is not null and org_lib.hasStatus(p_organisation_pk, 'closed')) then
		v_html := v_html||''||
			htm.bTable('surroundElements')||'
				<tr>
					<td rowspan="2" width="25%" class="surroundElements">
						'||org_lib.contactInfo(p_organisation_pk, p_editMode)||'
					</td>
					<td class="orgStatusOpen">
						<span class="orgStatusHeading">'||c_tex_closedOrg||'</span><br /><br /><br />
						'||c_tex_closedOrgTxt||'<br />
					</td>
					<td rowspan="2" width="25%" class="surroundElements">
						'||org_lib.showMap(p_organisation_pk)||'
					</td>
				</tr>
				<tr>
					<td class="surroundElements">
						'||org_lib.generalInformation(p_organisation_pk, p_editMode)||'
					</td>
				</tr>
		';
		v_html := v_html||''||htm.eTable||chr(13);

	-- ---------------------
	-- not a valid PK
	-- ---------------------
	else
		v_html := htm.jumpTo(c_login_package, 1, '...');
		ext_lob.add(v_clob, v_html);
		return v_clob;

	end if;

	v_html := v_html||''||htm.ePage||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		return null;
end;
end viewOrg;


-- ******************************************** --
end;
/
show errors;
