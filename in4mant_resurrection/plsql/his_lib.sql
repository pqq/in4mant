PROMPT *** his_lib ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package his_lib is
/* ******************************************************
*	Package:     his_lib
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	051125 | Frode Klevstul
*			- Package created
****************************************************** */
	procedure run;
	procedure addEntry
	(
		  p_table_name				in		his_history.table_name%type
		, p_column_name				in		his_history.column_name%type
		, p_old_value				in		his_history.old_value%type
	);
	function getEntry
	(
		  p_table_name				in		his_history.table_name%type
		, p_column_name				in		his_history.column_name%type
	) return his_history.old_value%type;

end;
/
show errors;





-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body his_lib is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'his_lib';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  11.08.2005
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);
	
	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        addEntry
-- what:        add entry in history table
-- author:      Frode Klevstul
-- start date:  25.11.2005
---------------------------------------------------------
procedure addEntry
(
	  p_table_name				in		his_history.table_name%type
	, p_column_name				in		his_history.column_name%type
	, p_old_value				in		his_history.old_value%type
)
is
begin
declare
	c_procedure			constant mod_procedure.name%type	:= 'addEntry';

	c_id				constant ses_session.id%type 		:= ses_lib.getId;

	v_use_user_pk		use_user.use_user_pk%type			:= ses_lib.getPk(ses_lib.getId);
begin
	ses_lib.ini(g_package, c_procedure);

	insert into his_history
	(his_history_pk, table_name, column_name, old_value, use_user_fk, ses_session_id, date_insert)
	values
	(
		  seq_his_history.nextval
		, p_table_name
		, p_column_name
		, p_old_value
		, v_use_user_pk
		, c_id
		, sysdate
	);
	commit;

end;
end addEntry;


---------------------------------------------------------
-- name:        getEntry
-- what:        return entry in history table created in same session
-- author:      Frode Klevstul
-- start date:  09.08.2006
---------------------------------------------------------
function getEntry
(
	  p_table_name				in		his_history.table_name%type
	, p_column_name				in		his_history.column_name%type
) return his_history.old_value%type
is
begin
declare
	c_procedure			constant mod_procedure.name%type	:= 'getEntry';

	c_id				constant ses_session.id%type 		:= ses_lib.getId;

	v_use_user_pk		use_user.use_user_pk%type			:= ses_lib.getPk(ses_lib.getId);
	v_old_value			his_history.old_value%type;

	cursor	cur_his_history is
	select	old_value
	from	his_history
	where	ses_session_id = c_id
	and		table_name = p_table_name
	and 	column_name = p_column_name
	order by date_insert desc;

begin
	ses_lib.ini(g_package, c_procedure);

	for r_cursor in cur_his_history
	loop
		v_old_value := r_cursor.old_value;
		exit;
	end loop;
	
	return v_old_value;

end;
end getEntry;



-- ******************************************** --
end;
/
show errors;
