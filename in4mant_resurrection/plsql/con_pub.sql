PROMPT *** con_pub ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package con_pub is
/* ******************************************************
*	Package:     con_pub
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	060117 | Frode Klevstul
*			- Package created
****************************************************** */
	procedure run;
	procedure viewCon
	(
		  p_content_pk			in con_content.con_content_pk%type				default null
	);
	function viewCon
	(
		  p_content_pk			in con_content.con_content_pk%type				default null
	) return clob;
	procedure listCon
	(
		  p_noFirst				in number										default null
		, p_noOnOnePage			in number										default null
		, p_noShowFrom			in number										default null
		, p_subjectMaxLength	in number										default null
		, p_editMode			in number										default null
		, p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
		, p_con_type_pk			in con_type.con_type_pk%type					default null
		, p_con_subtype_pk		in con_subtype.con_subtype_pk%type				default null
		, p_date_start			in varchar2										default null
		, p_date_stop			in varchar2										default null
		, p_fullMode			in varchar2										default null
	);
	function listCon
	(
		  p_noFirst				in number										default null
		, p_noOnOnePage			in number										default null
		, p_noShowFrom			in number										default null
		, p_subjectMaxLength	in number										default null
		, p_editMode			in number										default null
		, p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
		, p_con_type_pk			in con_type.con_type_pk%type					default null
		, p_con_subtype_pk		in con_subtype.con_subtype_pk%type				default null
		, p_date_start			in varchar2										default null
		, p_date_stop			in varchar2										default null
		, p_fullMode			in varchar2										default null
	) return clob;

end;
/
show errors;





-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body con_pub is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'con_pub';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  11.08.2005
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        viewCon
-- what:        view content page
-- author:      Frode Klevstul
-- start date:  17.01.2006
---------------------------------------------------------
procedure viewCon(p_content_pk in con_content.con_content_pk%type default null) is begin
	ext_lob.pri(con_pub.viewCon(p_content_pk));
end;

function viewCon
(
	  p_content_pk			in con_content.con_content_pk%type				default null
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type					:= 'viewCon';

	c_tex_adminMode				constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'adminMode');
	c_tex_adminModeDescription	constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'adminModeDescription');
	c_tex_pleaseLogin			constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'pleaseLogin');
	c_tex_beChosenOrganisation	constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'beChosenOrganisation');
	c_tex_contactUs				constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'contactUs');

	cursor	cur_con_content is
	select	con_content_pk, subject, ingress, body, footer, org_organisation_fk, name, path_logo
	from	con_content, org_organisation
	where	org_organisation_pk = org_organisation_fk
	and		con_content_pk = p_content_pk;

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_content_pk='||p_content_pk);
	ext_lob.ini(v_clob);

	if (p_content_pk is not null) then
		-- log hits
		log_lib.logHits('con_content', p_content_pk);

		v_html := v_html||''||htm.bPage(g_package, c_procedure)||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html ||''||con_lib.viewCon(p_content_pk)||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);
	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		return null;
end;
end viewCon;


---------------------------------------------------------
-- name:        listCon
-- what:        list out content (wrapper for lib package)
-- author:      Frode Klevstul
-- start date:  20.11.2006
---------------------------------------------------------
procedure listCon(
	  p_noFirst				in number										default null
	, p_noOnOnePage			in number										default null
	, p_noShowFrom			in number										default null
	, p_subjectMaxLength	in number										default null
	, p_editMode			in number										default null
	, p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	, p_con_type_pk			in con_type.con_type_pk%type					default null
	, p_con_subtype_pk		in con_subtype.con_subtype_pk%type				default null
	, p_date_start			in varchar2										default null
	, p_date_stop			in varchar2										default null
	, p_fullMode			in varchar2										default null
) is begin
	ext_lob.pri(con_pub.listCon(p_noFirst, p_noOnOnePage, p_noShowFrom, p_subjectMaxLength, p_editMode, p_organisation_pk, p_con_type_pk, p_con_subtype_pk, p_date_start, p_date_stop, p_fullMode));
end;

function listCon
(
	  p_noFirst				in number										default null
	, p_noOnOnePage			in number										default null
	, p_noShowFrom			in number										default null
	, p_subjectMaxLength	in number										default null
	, p_editMode			in number										default null
	, p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	, p_con_type_pk			in con_type.con_type_pk%type					default null
	, p_con_subtype_pk		in con_subtype.con_subtype_pk%type				default null
	, p_date_start			in varchar2										default null
	, p_date_stop			in varchar2										default null
	, p_fullMode			in varchar2										default null
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type			:= 'listCon';

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_noShowFrom='||p_noShowFrom||'��p_organisation_pk='||p_organisation_pk||'��p_con_type_pk='||p_con_type_pk||'��p_con_subtype_pk='||p_con_subtype_pk);
	ext_lob.ini(v_clob);

	v_html := v_html||''||con_lib.listCon(p_noFirst, p_noOnOnePage, p_noShowFrom, p_subjectMaxLength, p_editMode, p_organisation_pk, p_con_type_pk, p_con_subtype_pk, p_date_start, p_date_stop, p_fullMode);

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		return null;
end;
end listCon;



-- ******************************************** --
end;
/
show errors;
