PROMPT *** ext_lob ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package ext_lob is
/* ******************************************************
*	Package:     pag_pub
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	060904 | Frode Klevstul
*			- Package created
****************************************************** */
	procedure run;
	procedure ini
	(
		p_lob			in out clob
	);
	procedure add
	(
		  p_lob			in out clob
		, p_data		in out clob
	);
	procedure pri
	(
		  p_lob			in clob
	);

end;
/
show errors;




-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body ext_lob is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'ext_lob';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  04.09.2006
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        ini
-- what:        initialize the clob
-- author:      Frode Klevstul
-- start date:  04.09.2006
---------------------------------------------------------
procedure ini
(
	p_lob			in out clob
)
is
begin
declare
	c_procedure		constant mod_procedure.name%type	:= 'ini';

begin
	ses_lib.ini(g_package, c_procedure);

	-- Initialize the clob
	dbms_lob.createtemporary(p_lob, false);

end;
end ini;


---------------------------------------------------------
-- name:        add
-- what:        add data to the clob
-- author:      Frode Klevstul
-- start date:  04.09.2006
---------------------------------------------------------
procedure add
(
	  p_lob			in out clob
	, p_data		in out clob
)
is
begin
declare
	c_procedure		constant mod_procedure.name%type	:= 'add';

	v_clob			clob;
	v_buffer		varchar2(4000);
 	v_read_amount	binary_integer	:= 4000;
	v_read_offset	integer			:= 1;

begin
	ses_lib.ini(g_package, c_procedure);

	if (length(p_data) > 0) then
		-- write append to the clob
		dbms_lob.append (p_lob, p_data);
	end if;

	-- reset p_data
	p_data := '';

end;
end add;


---------------------------------------------------------
-- name:        pri
-- what:        print clob content
-- author:      Frode Klevstul
-- start date:  04.09.2006
---------------------------------------------------------
procedure pri
(
	  p_lob			in clob
)
is
begin
declare
	c_procedure		constant mod_procedure.name%type	:= 'pri';

	v_clob			clob;
	v_buffer		varchar2(4000);
 	v_read_amount	binary_integer	:= 4000;
	v_read_offset	integer			:= 1;

begin
	ses_lib.ini(g_package, c_procedure);

	v_clob := p_lob;
	dbms_lob.open (v_clob, dbms_lob.lob_readonly);
	loop
		dbms_lob.read(v_clob, v_read_amount, v_read_offset, v_buffer);
		--owa_pattern.change(v_buffer, '^\s*', '', 'g');
		htp.prn(v_buffer);
		v_read_offset := v_read_offset + v_read_amount;
	end loop;
	dbms_lob.close (v_clob);

exception
	when no_data_found then
		null;
	when value_error then
		null;
end;
end pri;



-- ******************************************** --
end;
/
show errors;
