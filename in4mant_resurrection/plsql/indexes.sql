PROMPT *** indexes ***
set define off
set serveroutput on;


-- --------------
-- con_content
-- --------------
drop index idx_1_con_content;
create index idx_1_con_content on con_content (date_start desc);

drop index idx_2_con_content;
create index idx_2_con_content on con_content (date_publish desc);

-- --------------
-- org_organisation
-- --------------
drop index idx_1_org_organisation;
create index idx_1_org_organisation on org_organisation (date_status_change desc);

drop index idx_2_org_organisation;
create index idx_2_org_organisation on org_organisation (name asc, org_status_fk asc);

drop index idx_3_org_organisation;
create index idx_3_org_organisation on org_organisation (date_update desc);

-- --------------
-- alb_album
-- --------------
drop index idx_1_pic_album;
create index idx_1_pic_album on pic_album (date_update desc);

-- --------------
-- log_hits
-- --------------
drop index idx_1_log_hits;
create index idx_1_log_hits on log_hits (hits desc);



commit;

show errors;
