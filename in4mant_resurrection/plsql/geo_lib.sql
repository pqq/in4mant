PROMPT *** geo_lib ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package geo_lib is
/* ******************************************************
*	Package:     tex_lib
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	060213 | Frode Klevstul
*			- Package created
****************************************************** */
	procedure run;
	procedure getCity
	(
		p_zip		in add_address.zip%type				default null
	);
	function getCity
	(
		p_zip		in add_address.zip%type				default null
	) return varchar2;
	procedure getCountry
	(
		p_geo_geography_pk		geo_geography.geo_geography_pk%type		default null
	);
	function getCountry
	(
		p_geo_geography_pk		geo_geography.geo_geography_pk%type		default null
	) return varchar2;
	procedure getName
	(
		p_geo_geography_pk		geo_geography.geo_geography_pk%type		default null
	);
	function getName
	(
		p_geo_geography_pk		geo_geography.geo_geography_pk%type		default null
	) return varchar2;

end;
/
show errors;





-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body geo_lib is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'geo_lib';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  13.02.2006
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        getCity
-- what:        return city given zip
-- author:      Frode Klevstul
-- start date:  13.02.2006
---------------------------------------------------------
procedure getCity
(
	p_zip		in add_address.zip%type				default null
) is begin
	htp.p(geo_lib.getCity(p_zip));
end;

function getCity
(
	p_zip		in add_address.zip%type				default null
) return varchar2
is
begin
declare
	c_procedure				constant mod_procedure.name%type 	:= 'getCity';

	cursor	cur_geo_geography is
	select	name
	from	geo_geography, geo_zip
	where	geo_geography_pk = geo_geography_fk_pk
	and		zip_pk = p_zip;

	v_name					geo_geography.name%type;

begin
	ses_lib.ini(g_package, c_procedure);

	for r_cursor in cur_geo_geography
	loop
		v_name := r_cursor.name;
	end loop;

	if (v_name is null) then
		return p_zip;
	else
		return v_name;
	end if;

end;
end getCity;


---------------------------------------------------------
-- name:        getCountry
-- what:        return name of country given a geo pk
-- author:      Frode Klevstul
-- start date:  17.07.2006
---------------------------------------------------------
procedure getCountry
(
	p_geo_geography_pk		geo_geography.geo_geography_pk%type		default null
) is begin
	htp.p(geo_lib.getCountry(p_geo_geography_pk));
end;

function getCountry
(
	p_geo_geography_pk		geo_geography.geo_geography_pk%type		default null
) return varchar2
is
begin
declare
	c_procedure				constant mod_procedure.name%type 	:= 'getCountry';

	cursor	cur_geo_geography 
	(b_geo_geography_pk geo_geography.geo_geography_pk%type) is
	select	name, parent_geo_geography_fk
	from	geo_geography
	where	geo_geography_pk = b_geo_geography_pk;

	v_geo_geography_pk		geo_geography.geo_geography_pk%type;
	v_name					geo_geography.name%type;

begin
	ses_lib.ini(g_package, c_procedure);

	-- ----------------------------------------
	-- Taken a couple of assumptions here:
	-- ----------------------------------------
	-- * All continents has PK < 10
	-- * All countries has PK < 100
	-- * There won't be more than two levels below country
	-- ----------------------------------------

	-- ---------------------------------------
	-- example of data
	-- ---------------------------------------
	-- geo_pk	| name		| parent_pk
	-- - - - - - - - - - - - - - - - - - - - -
	-- 1		  Europe	  NULL
	-- 10		  Norway	  1
	-- 101		  Akershus	  10
	-- 102		  Aust-Agder  10
	-- ---------------------------------------

	-- only continents has PK less than 10
	if (p_geo_geography_pk < 10) then
		v_name := 'error: isContinent';

	-- only countries has PK less than 100
	elsif (p_geo_geography_pk < 100) then
		v_name := 'error: isCountry';
	end if;

	-- return error
	if (v_name is not null) then
		return v_name;
	end if;

	-- try first level
	for r_cursor in cur_geo_geography(p_geo_geography_pk)
	loop
		v_geo_geography_pk	:= r_cursor.parent_geo_geography_fk;
	end loop;

	-- name of country found as the parent
	if (v_geo_geography_pk < 10 and v_name is not null) then
		v_name := geo_lib.getName(v_geo_geography_pk);
		return v_name;
	end if;

	-- try going one more level up
	for r_cursor in cur_geo_geography(v_geo_geography_pk)
	loop
		v_geo_geography_pk	:= r_cursor.parent_geo_geography_fk;
	end loop;

	-- name of country found as the parent
	if (v_geo_geography_pk < 10 and v_name is not null) then
		v_name := geo_lib.getName(v_geo_geography_pk);
		return v_name;
	end if;

	if (v_name is null) then
		v_name := 'error: country not found';
	end if;

	return v_name;

end;
end getCountry;


---------------------------------------------------------
-- name:        getName
-- what:        return name given PK
-- author:      Frode Klevstul
-- start date:  17.07.2006
---------------------------------------------------------
procedure getName
(
	p_geo_geography_pk		geo_geography.geo_geography_pk%type		default null
) is begin
	htp.p(geo_lib.getName(p_geo_geography_pk));
end;

function getName
(
	p_geo_geography_pk		geo_geography.geo_geography_pk%type		default null
) return varchar2
is
begin
declare
	c_procedure				constant mod_procedure.name%type 	:= 'getName';

	cursor	cur_geo_geography is
	select	name
	from	geo_geography
	where	geo_geography_pk = p_geo_geography_pk;

	v_name					geo_geography.name%type;

begin
	ses_lib.ini(g_package, c_procedure);

	for r_cursor in cur_geo_geography
	loop
		v_name := r_cursor.name;
	end loop;

	return v_name;

end;
end getName;


-- ******************************************** --
end;
/
show errors;
