PROMPT *** ext_sea ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package ext_sea is
/* ******************************************************
*	Package:     ext_sea
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	061017 | Frode Klevstul
*			- Package created
****************************************************** */
	type parameter_arr 			is table of varchar2(4000) index by binary_integer;
	empty_array 				parameter_arr;

	procedure run;
	procedure generateList
	(
		  p_type				in varchar2										default null
		, p_name				in parameter_arr								default empty_array
		, p_value				in parameter_arr								default empty_array
	);
	function generateList
	(
		  p_type				in varchar2										default null
		, p_name				in parameter_arr								default empty_array
		, p_value				in parameter_arr								default empty_array
	) return clob;
	procedure advancedSearch;
	function advancedSearch
		return clob;

end;
/
show errors;





-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body ext_sea is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'ext_sea';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  17.10.2006
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        generateList
-- what:        generate list based on search
-- author:      Frode Klevstul
-- start date:  02.01.2006
---------------------------------------------------------
procedure generateList
(
	  p_type				in varchar2										default null
	, p_name				in parameter_arr								default empty_array
	, p_value				in parameter_arr								default empty_array
) is begin
	ext_lob.pri(ext_sea.generateList(p_type, p_name, p_value));
end;

function generateList
(
	  p_type				in varchar2										default null
	, p_name				in parameter_arr								default empty_array
	, p_value				in parameter_arr								default empty_array
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type			:= 'generateList';

	c_id						constant ses_session.id%type 				:= ses_lib.getId;
	c_service_pk				constant ser_service.ser_service_pk%type	:= ses_lib.getSer(c_id);
	c_login_package				constant sys_parameter.value%type			:= sys_lib.getParameter('login_package');
	c_no_searchHits				constant sys_parameter.value%type			:= sys_lib.getParameter('no_searchHits');
	c_dir_modOwa				constant sys_parameter.value%type			:= sys_lib.getParameter('dir_modOwa');

	c_tex_memberOrg				constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'memberOrg');
	c_tex_openOrg				constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'openOrg');
	c_tex_closedOrg				constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'closedOrg');
	c_tex_moreInfo				constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'moreInfo');
	c_tex_closed				constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'closed');
	c_tex_hasError				constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'hasError');
	c_tex_noHits				constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'noHits');
	c_tex_showingHits			constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'showingHits');
	c_tex_to					constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'to');
	c_tex_hits					constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'hits');

	cur_dynCursor				sys_refcursor;

	v_array						parameter_arr								default empty_array;
	v_nameArray					parameter_arr								default empty_array;
	v_valueArray				parameter_arr								default empty_array;
	v_resultArray				parameter_arr								default empty_array;
	v_index						number;
	v_noShowNext				number;
	v_noShowNextStop			number;
	v_noShowFromNow				number;
	v_noOnOnePage				number										:= c_no_searchHits;
	v_noShowPrevious			number;
	v_totalCurSize				number										:= 0;
	v_bool1						boolean										:= false;
	v_limitReached				boolean										:= false;
	v_jumpNext					boolean										:= false;
	v_boxOpened					boolean										:= false;
	v_fakeReferer				boolean										:= false;

	v_html						clob;
	v_clob						clob;

	v_sql_stmt					varchar2(2048);
	v_sql_stmt_tmp				varchar2(2048);
	v_sql_stmt_tmp2				varchar2(2048);
	v_sql_stmt_tmp3				varchar2(2048);

begin
	ses_lib.ini(g_package, c_procedure, 'p_type='||p_type);
	ext_lob.ini(v_clob);

	-- ---------------------------------------------
	-- check that request comes from our own site
	-- ---------------------------------------------
	if ( owa_util.get_cgi_env('HTTP_REFERER') not like '%'||c_dir_modOwa||'%' ) then
		v_fakeReferer := true;
	end if;

	v_html := v_html||''||htm.bPage(g_package, c_procedure)||chr(13);
	v_html := v_html||'<center>'||chr(13);
	v_html := v_html||''||htm.bTable('surroundOrgSearchResult')||chr(13);
	v_html := v_html||'<tr><td>'||chr(13);

	v_nameArray		:= p_name;
	v_valueArray	:= p_value;

	if (v_nameArray.exists(1)) then
		for cursor_column in v_nameArray.first .. v_nameArray.last
		loop
			-- -----------------------------------------------------------------
			-- only do this if both name and value exists (no empty value)
			-- -----------------------------------------------------------------
			if (v_nameArray.exists(cursor_column) and v_valueArray.exists(cursor_column)) then
				--htp.p('v_name('||cursor_column||') : v_value('||cursor_column||') = '''||v_nameArray(cursor_column)||''' : '''||v_valueArray(cursor_column)||'''<br>');

				if ( length(v_valueArray(cursor_column)) is null) then
					v_valueArray.delete(cursor_column);
				else
					-- ---------------------------------------------
					-- prepare user's input values for serarch
					-- ---------------------------------------------
					-- replace ' with '' to avoid server crash
					v_valueArray(cursor_column) := replace(v_valueArray(cursor_column), '''', '''''');
				end if;

				-- --------------
				-- for geography dropdowns (select boxes) IE returns ' ' (one space) while FireFox returns '' (empty)
				-- --------------
				if (v_nameArray(cursor_column) = 'region_main' or v_nameArray(cursor_column) = 'region_sub') then
					if (v_valueArray.exists(cursor_column)) then
						if (v_valueArray(cursor_column) = ' ') then
							v_valueArray.delete(cursor_column);
						end if;
					end if;
				end if;

				if (v_nameArray(cursor_column) = 'noShowFrom') then
					v_noShowFromNow		:= v_valueArray(cursor_column);
					v_noShowPrevious	:= v_noShowFromNow - v_noOnOnePage;
					v_noShowNext		:= v_noShowFromNow + v_noOnOnePage;
				end if;

				if (v_nameArray(cursor_column) like 'list' and v_valueArray(cursor_column) like 'lis_list_pk%') then
					v_sql_stmt_tmp2 := v_sql_stmt_tmp2||' and org_organisation_pk in (select org_organisation_fk_pk from lis_org where lis_list_fk_pk = '||substr(v_valueArray(cursor_column), 13, length(v_valueArray(cursor_column)))||' )';
				end if;

				if (v_nameArray(cursor_column) like 'list' and v_valueArray(cursor_column) like 'org_status_pk%') then
					-- if first org_status_pk entry
					if (not v_bool1) then
						v_sql_stmt_tmp3 := v_sql_stmt_tmp3||' and ( org_status_fk =  '||substr(v_valueArray(cursor_column), 15, length(v_valueArray(cursor_column)));
					-- not first entry
					else
						v_sql_stmt_tmp3 := v_sql_stmt_tmp3||' or org_status_fk =  '||substr(v_valueArray(cursor_column), 15, length(v_valueArray(cursor_column)));
					end if;

					v_bool1 := true;
				end if;

				-- -------------------------
				-- build the result array
				-- -------------------------
				if (v_nameArray(cursor_column) like 'name' or v_nameArray(cursor_column) like 'miscText') then
					if (v_valueArray.exists(cursor_column)) then
						v_resultArray(1) := rtrim(ltrim(v_valueArray(cursor_column)));
						-- if name contains comma the string after comma is supposed to be the city / place
						if ( owa_pattern.match(v_resultArray(1), ',') ) then
							v_resultArray(6) := ltrim(ext_lib.split(v_resultArray(1), 2, ','));
							v_resultArray(1) := ext_lib.split(v_resultArray(1), 1, ',');
						end if;
					else
						v_resultArray(1) := '';
					end if;
				elsif (v_nameArray(cursor_column) like 'nameFirstLetter') then
					if (v_valueArray.exists(cursor_column)) then
						v_resultArray(2) := v_valueArray(cursor_column);
					end if;
				elsif (v_nameArray(cursor_column) like 'lowAgeLimit') then
					if (v_valueArray.exists(cursor_column)) then
						v_resultArray(3) := v_valueArray(cursor_column);
					end if;
				elsif (v_nameArray(cursor_column) like 'region_main') then
					if (v_valueArray.exists(cursor_column)) then
						v_resultArray(4) := v_valueArray(cursor_column);
					end if;
				elsif (v_nameArray(cursor_column) like 'region_sub') then
					if (v_valueArray.exists(cursor_column)) then
						v_resultArray(5) := v_valueArray(cursor_column);
					end if;
				elsif (v_nameArray(cursor_column) like 'geography_name') then
					if (v_valueArray.exists(cursor_column)) then
						v_resultArray(6) := v_valueArray(cursor_column);
					end if;
				end if;

			end if;
		end loop;
	end if;

	if (v_noShowNext is null) then
		v_noShowNext := v_noOnOnePage + 1;
	end if;

	if (v_noShowFromNow is null) then
		v_noShowFromNow := 1;
	end if;

	if (v_bool1) then
		v_sql_stmt_tmp3 := v_sql_stmt_tmp3||')';
	end if;

	-- ----------------------------------
	-- simple organisation search
	-- ----------------------------------
	if (p_type = 'orgSimple') then
		if (not v_resultArray.exists(1)) then
			v_resultArray(1) := '';
		end if;
		v_sql_stmt :=   ' select org_organisation_pk, o.name, street, zip, path_logo'||
						' from org_organisation o, org_ser s, add_address a'||
						' where org_organisation_pk = org_organisation_fk_pk'||
						' and add_address_fk = add_address_pk'||
						' and lower(o.name) like lower(''%'||v_resultArray(1)||'%'')'||
						' and org_status_fk = '||
						' (select org_status_pk from org_status where name=''[ORG_STATUS.NAME]'')'||
						' and (bool_umbrella_organisation = 0 or bool_umbrella_organisation is NULL)'||
						' and ser_service_fk_pk = '||c_service_pk||
						' order by lower(name) asc';

	-- ----------------------------------
	-- advanced organisation search
	-- ----------------------------------
	elsif (p_type = 'orgAdvanced') then
		v_sql_stmt :=   ' select org_organisation_pk, o.name, street, zip, path_logo'||
						' from org_organisation o, org_ser s, add_address a'||
						' where org_organisation_pk = org_organisation_fk_pk'||
						' and add_address_fk = add_address_pk'||
						' and org_status_fk = '||
						' (select org_status_pk from org_status where name=''[ORG_STATUS.NAME]'')'||
						' and (bool_umbrella_organisation = 0 or bool_umbrella_organisation is NULL)'||
						' and ser_service_fk_pk = '||c_service_pk;

		-- text
		if (v_resultArray.exists(1)) then
			v_sql_stmt := v_sql_stmt||
							' and ( lower(o.general_information) like lower(''%'||v_resultArray(1)||'%'')'||
							' or lower(o.age_description) like lower(''%'||v_resultArray(1)||'%'') '||
							' or lower(o.name) like lower(''%'||v_resultArray(1)||'%'') '||
							' or lower(a.street) like lower(''%'||v_resultArray(1)||'%'') '||
							' ) ';
		end if;

		-- name starts with
		if (v_resultArray.exists(2)) then
			v_sql_stmt := v_sql_stmt||
						' and lower(o.name) like lower('''||v_resultArray(2)||'%'')';
		end if;

		-- lower age limit
		if (v_resultArray.exists(3)) then
			if (v_resultArray(3) > 0) then
				v_sql_stmt := v_sql_stmt||
							' and (age_limit >= '||v_resultArray(3)||')';
			else
				v_sql_stmt := v_sql_stmt||
							' and (age_limit >= '||v_resultArray(3)||' or age_limit is null)';
			end if;
		end if;

		-- geography main pk (only if sub isn't chosen)
		if (v_resultArray.exists(4) and not v_resultArray.exists(5)) then
			v_sql_stmt := v_sql_stmt||
						' and a.geo_geography_fk in (select geo_geography_pk from geo_geography where parent_geo_geography_fk = '||v_resultArray(4)||')';
		end if;

		-- geography sub pk
		if (v_resultArray.exists(4) and v_resultArray.exists(5)) then
			v_sql_stmt := v_sql_stmt||
						' and a.geo_geography_fk = '||v_resultArray(5)||'';
		end if;

		-- geography name
		if (v_resultArray.exists(6)) then
			v_sql_stmt := v_sql_stmt||
				' and a.geo_geography_fk in (
				select gg2.geo_geography_pk
				from geo_geography gg1
				left outer join  geo_geography gg2 ON gg1.geo_geography_pk = gg2.parent_geo_geography_fk
				where lower(gg1.name) like lower(''%'||v_resultArray(6)||'%'')
				or lower(gg2.name) like lower(''%'||v_resultArray(6)||'%''))';
		end if;

		-- add other sqls
		v_sql_stmt := v_sql_stmt ||''|| v_sql_stmt_tmp2;
		v_sql_stmt := v_sql_stmt ||''|| v_sql_stmt_tmp3;

		-- order
		v_sql_stmt := v_sql_stmt||
					' order by lower(name) asc';
	else
		return htm.jumpTo(c_login_package, 1, null, false);
	end if;

	--htp.p('<br>'||v_sql_stmt||'<br><br>');
	--return null;

	-- ----------------------------
	--
	-- find totalt cursor size
	--
	-- ----------------------------
	v_sql_stmt_tmp := replace(v_sql_stmt, '[ORG_STATUS.NAME]', 'member');
	open cur_dynCursor for v_sql_stmt_tmp;
	loop
		fetch cur_dynCursor into v_array(1), v_array(2), v_array(3), v_array(4), v_array(5);
		exit when cur_dynCursor%notfound;
		v_totalCurSize := v_totalCurSize + 1;
	end loop;
	close cur_dynCursor;

	v_sql_stmt_tmp := replace(v_sql_stmt, '[ORG_STATUS.NAME]', 'open');
	open cur_dynCursor for v_sql_stmt_tmp;
	loop
		fetch cur_dynCursor into v_array(1), v_array(2), v_array(3), v_array(4), v_array(5);
		exit when cur_dynCursor%notfound;
		v_totalCurSize := v_totalCurSize + 1;
	end loop;
	close cur_dynCursor;

	v_sql_stmt_tmp := replace(v_sql_stmt, '[ORG_STATUS.NAME]', 'closed');
	open cur_dynCursor for v_sql_stmt_tmp;
	loop
		fetch cur_dynCursor into v_array(1), v_array(2), v_array(3), v_array(4), v_array(5);
		exit when cur_dynCursor%notfound;
		v_totalCurSize := v_totalCurSize + 1;
	end loop;
	close cur_dynCursor;

	-- ---------------------------
	-- reset v_index for use of showing the correct numbers
	-- ---------------------------
	v_index := 1;

	-- ----------------------------
	--
	-- start running the SQLs
	--
	-- ----------------------------

	-- ---------------------
	-- members
	-- ---------------------
	v_sql_stmt_tmp := replace(v_sql_stmt, '[ORG_STATUS.NAME]', 'member');
	open cur_dynCursor for v_sql_stmt_tmp;
	loop
		fetch cur_dynCursor into v_array(1), v_array(2), v_array(3), v_array(4), v_array(5);

		-- ---------------------------------------------------------------------
		-- deal with fake referers (people that tries to steal our lists)
		-- ---------------------------------------------------------------------
		if (v_fakeReferer) then
			-- Array: | org_organisation_pk | o.name | street | zip | path_logo |
			v_array(1) := '1000';
			v_array(2) := lower(substr(v_array(2), 3, 5) ||''|| substr(v_array(2), 1, 2));
			v_array(3) := substr(v_array(3), 1, 5) ||'...';
			v_array(4) := substr(v_array(4), 1, 3);
			v_array(5) := substr(v_array(5), 1, 5);
		end if;

		if (v_index < v_noShowFromNow) then
			v_jumpNext	:= true;
		elsif (v_index = v_noShowFromNow) then
			v_jumpNext := false;
		elsif (v_index = (v_noShowFromNow+v_noOnOnePage)) then
			v_limitReached := true;
		end if;
		v_index := v_index + 1;

		if ( (cur_dynCursor%notfound and cur_dynCursor%rowcount > 0 and not v_jumpNext and v_boxOpened) or (v_limitReached and v_boxOpened) ) then
			--htp.p('CLOSE MEMBERS!!!');
			v_html := v_html||''||htm.eTable||chr(13);
			v_html := v_html||'</center>'||chr(13);
			v_html := v_html||''||htm.eBox||chr(13);
			v_html := v_html||'<br><br>'||chr(13);
			v_boxOpened := false;
		end if;
		exit when cur_dynCursor%notfound;
		exit when v_limitReached;

		if (cur_dynCursor%rowcount>0 and not v_jumpNext and not v_boxOpened) then
			--htp.p('OPEN MEMBERS!!!');
			v_html := v_html||''||htm.bBox(c_tex_memberOrg)||chr(13);
			v_html := v_html||'<center><table width="95%" border="0">'||chr(13);
			v_boxOpened := true;
		end if;

		if (not v_jumpNext) then
			v_html := v_html||'
						<tr>
							<td rowspan="5" width="15%">
								<a href="org_pub.viewOrg?p_organisation_pk='||v_array(1)||'"><img border="0" src="'||org_lib.getDirectoryPath(v_array(1), true)||'/'||v_array(5)||'" alt=""></a>
							</td>
							<td style="font-size: 15;"><a href="org_pub.viewOrg?p_organisation_pk='||v_array(1)||'"><b>'||v_array(2)||'</b></a></td>
						</tr>
						<tr>
							<td>'||v_array(3)||'&nbsp;</td>
						</tr>
						<tr>
							<td>'||v_array(4)||' '||geo_lib.getCity(v_array(4))||'</td>
						</tr>
						<tr><td>&nbsp;</td></tr>
						<tr>
							<td>'||htm.link(c_tex_moreInfo,'org_pub.viewOrg?p_organisation_pk='||v_array(1), '_self', 'theButtonStyle')||'
							<span style="padding-left: 40px;">&nbsp;</span>'||htm.link(c_tex_hasError,'pag_pub.contactpage', '_self', 'buttonStyleDiscreet')||'</td>
						</tr>
						<tr><td colspan="2"><hr size="1" width="100%"></td></tr>'||chr(13);
		end if;
	end loop;
	close cur_dynCursor;

	-- ---------------------
	-- open/normal
	-- ---------------------
	v_sql_stmt_tmp := replace(v_sql_stmt, '[ORG_STATUS.NAME]', 'open');
	open cur_dynCursor for v_sql_stmt_tmp;
	loop
		fetch cur_dynCursor into v_array(1), v_array(2), v_array(3), v_array(4), v_array(5);

		-- ---------------------------------------------------------------------
		-- deal with fake referers (people that tries to steal our lists)
		-- ---------------------------------------------------------------------
		if (v_fakeReferer) then
			-- Array: | org_organisation_pk | o.name | street | zip | path_logo |
			v_array(1) := '1000';
			v_array(2) := lower(substr(v_array(2), 3, 5) ||''|| substr(v_array(2), 1, 2));
			v_array(3) := substr(v_array(3), 1, 5) ||'...';
			v_array(4) := substr(v_array(4), 1, 3);
			v_array(5) := substr(v_array(5), 1, 5);
		end if;

		if (v_index < v_noShowFromNow) then
			v_jumpNext	:= true;
		elsif (v_index = v_noShowFromNow) then
			v_jumpNext := false;
		elsif (v_index = (v_noShowFromNow+v_noOnOnePage-1)) then
			v_limitReached := true;
		end if;
		v_index := v_index + 1;

		if ( (cur_dynCursor%notfound and cur_dynCursor%rowcount > 0 and not v_jumpNext and v_boxOpened) or (v_limitReached and v_boxOpened) ) then
			--htp.p('CLOSE NORMAL!!!');
			v_html := v_html||''||htm.eTable||chr(13);
			v_html := v_html||'</center>'||chr(13);
			v_html := v_html||''||htm.eBox||chr(13);
			v_html := v_html||'<br><br>'||chr(13);
			v_boxOpened := false;
		end if;
		exit when cur_dynCursor%notfound;
		exit when v_limitReached;

		if (cur_dynCursor%rowcount>0 and not v_jumpNext and not v_boxOpened) then
			--htp.p('OPEN NORMAL!!!');
			v_html := v_html||''||htm.bBox(c_tex_openOrg)||chr(13);
			v_html := v_html||'<center><table width="95%" border="0">'||chr(13);
			v_boxOpened := true;
		end if;

		if (not v_jumpNext) then
			v_html := v_html||'
						<tr>
							<td rowspan="5" width="15%">&nbsp;</td>
							<td style="font-size: 10;"><a href="org_pub.viewOrg?p_organisation_pk='||v_array(1)||'"><b>'||v_array(2)||'</b></a></td>
						</tr>
						<tr>
							<td>'||v_array(3)||'&nbsp;</td>
						</tr>
						<tr>
							<td>'||v_array(4)||' '||geo_lib.getCity(v_array(4))||'</td>
						</tr>
						<tr><td>&nbsp;</td></tr>
						<tr>
							<td>'||
							htm.link(c_tex_moreInfo,'org_pub.viewOrg?p_organisation_pk='||v_array(1), '_self', 'theButtonStyle')||'
								<span style="padding-left: 40px;">&nbsp;</span>'||htm.link(c_tex_hasError,'pag_pub.contactpage', '_self', 'buttonStyleDiscreet')||'
							</td>
						</tr>
						<tr><td colspan="2"><hr size="1" width="100%"></td></tr>'||chr(13);
		end if;
	end loop;
	close cur_dynCursor;

	-- ---------------------
	-- closed
	-- ---------------------
	v_sql_stmt_tmp := replace(v_sql_stmt, '[ORG_STATUS.NAME]', 'closed');
	open cur_dynCursor for v_sql_stmt_tmp;
	loop
		fetch cur_dynCursor into v_array(1), v_array(2), v_array(3), v_array(4), v_array(5);

		-- ---------------------------------------------------------------------
		-- deal with fake referers (people that tries to steal our lists)
		-- ---------------------------------------------------------------------
		if (v_fakeReferer) then
			-- Array: | org_organisation_pk | o.name | street | zip | path_logo |
			v_array(1) := '1000';
			v_array(2) := lower(substr(v_array(2), 3, 5) ||''|| substr(v_array(2), 1, 2));
			v_array(3) := substr(v_array(3), 1, 5) ||'...';
			v_array(4) := substr(v_array(4), 1, 3);
			v_array(5) := substr(v_array(5), 1, 5);
		end if;

		if (v_index < v_noShowFromNow) then
			v_jumpNext	:= true;
		elsif (v_index = v_noShowFromNow) then
			v_jumpNext := false;
		elsif (v_index = (v_noShowFromNow+v_noOnOnePage-1)) then
			v_limitReached := true;
		end if;
		v_index := v_index + 1;

		if ( (cur_dynCursor%notfound and cur_dynCursor%rowcount > 0 and not v_jumpNext and v_boxOpened) or (v_limitReached and v_boxOpened) ) then
			--htp.p('CLOSE CLOSED!!!');
			v_html := v_html||''||htm.eTable||chr(13);
			v_html := v_html||'</center>'||chr(13);
			v_html := v_html||''||htm.eBox||chr(13);
			v_html := v_html||'<br><br>'||chr(13);
			v_boxOpened := false;
		end if;
		exit when cur_dynCursor%notfound;
		exit when v_limitReached;

		if (cur_dynCursor%rowcount>0 and not v_jumpNext and not v_boxOpened) then
			--htp.p('OPEN CLOSED!!!');
			v_html := v_html||''||htm.bBox(c_tex_closedOrg)||chr(13);
			v_html := v_html||'<center><table width="95%" border="0">'||chr(13);
			v_boxOpened := true;
		end if;

		if (not v_jumpNext) then
			v_html := v_html||'
						<tr>
							<td rowspan="5" width="15%">&nbsp;</td>
							<td style="font-size: 10;"><i>'||v_array(2)||' ('||c_tex_closed||')</i></td>
						</tr>
						<tr>
							<td>'||v_array(3)||'&nbsp;</td>
						</tr>
						<tr>
							<td>'||v_array(4)||' '||geo_lib.getCity(v_array(4))||'</td>
						</tr>
						<tr><td>&nbsp;</td></tr>
						<tr>
							<td>'||htm.link(c_tex_moreInfo,'org_pub.viewOrg?p_organisation_pk='||v_array(1), '_self', 'theButtonStyle')||'
							<span style="padding-left: 40px;">&nbsp;</span>'||htm.link(c_tex_hasError, 'pag_pub.contactpage', '_self', 'buttonStyleDiscreet')||'</td>
						</tr>
						<tr><td colspan="2"><hr size="1" width="100%"></td></tr>'||chr(13);
		end if;
	end loop;
	close cur_dynCursor;

	v_noShowNextStop := v_noShowNext + v_noOnOnePage - 1;

	if (v_noShowNextStop > v_totalCurSize) then
		v_noShowNextStop := v_totalCurSize;
	end if;

	-- ending surroundElements table
	v_html := v_html||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);

	-- -----------------------------
	--
	-- next / previous link
	--
	-- -----------------------------
	v_html := v_html ||'
		<script type="text/javascript">
			function goPrevious(){
				saveParameters();
				document.form_previous.submit();
			};

			function goNext(){
				saveParameters();
				document.form_next.submit();
			};
		</script>
	'||chr(13);

	v_html := v_html ||''||htm.bTable('preNextLink')||'<tr><td width="20%" class="preNextLink">&nbsp;'||chr(13);

	-- -----------------------------
	-- previous link
	-- -----------------------------
	if (v_noShowPrevious is not null and v_noShowFromNow > v_noOnOnePage) then
		v_html := v_html||''||htm.bForm(g_package||'.'||c_procedure, 'form_previous');
		for cursor_column in v_nameArray.first .. v_nameArray.last
		loop
			if (v_valueArray.exists(cursor_column) and v_nameArray.exists(cursor_column)) then
				if (v_nameArray(cursor_column) not like 'noShowFrom') then
					v_html := v_html||'
						<input type="hidden" name="p_name" value="'||v_nameArray(cursor_column)||'">
						<input type="hidden" name="p_value" value="'||v_valueArray(cursor_column)||'">
					';
				end if;
			end if;
		end loop;
		v_html := v_html||'
			<input type="hidden" name="p_type" value="'||p_type||'">
			<input type="hidden" name="p_name" value="noShowFrom">
			<input type="hidden" name="p_value" value="'||v_noShowPrevious||'">
			'||htm.submitLink('<< '||v_noShowPrevious||'-'||(v_noShowFromNow-1), 'form_previous', 'goPrevious()')||'
		';
		v_html := v_html||''||htm.eForm;
	end if;

	v_html := v_html ||'</td><td width="60%" class="preNextLink">&nbsp;'||chr(13);

	-- -----------------------------
	-- what numbers we show now
	-- -----------------------------
	if (v_totalCurSize > 0) then

		v_html := v_html||'
			<span style="padding-left: 20px;">&nbsp;</span>
			'||v_totalCurSize||' '||c_tex_hits||'<span style="padding-left: 10px;">&nbsp;</span>('||c_tex_showingHits||' '||v_noShowFromNow||' '||c_tex_to||' '||chr(13);

		if ( (v_noShowNext-1) < v_totalCurSize) then
			v_html := v_html||' '||(v_noShowNext-1);
		else
			v_html := v_html||' '||v_totalCurSize;
		end if;
		v_html := v_html||')<span style="padding-left: 20px;">&nbsp;</span>'||chr(13);
	else
		v_html := v_html||''||c_tex_noHits;
	end if;

	v_html := v_html ||'</td><td width="20%" class="preNextLink">&nbsp;'||chr(13);

	-- -----------------------------
	-- next link
	-- -----------------------------
	if ( (v_noShowNext-1) < v_totalCurSize) then
		v_html := v_html||''||htm.bForm(g_package||'.'||c_procedure, 'form_next');
		for cursor_column in v_nameArray.first .. v_nameArray.last
		loop
			if (v_valueArray.exists(cursor_column) and v_nameArray.exists(cursor_column)) then
				if (v_nameArray(cursor_column) not like 'noShowFrom') then
					v_html := v_html||'
						<input type="hidden" name="p_name" value="'||v_nameArray(cursor_column)||'">
						<input type="hidden" name="p_value" value="'||v_valueArray(cursor_column)||'">
					';
				end if;
			end if;
		end loop;
		v_html := v_html||'
			<input type="hidden" name="p_type" value="'||p_type||'">
			<input type="hidden" name="p_name" value="noShowFrom">
			<input type="hidden" name="p_value" value="'||v_noShowNext||'">
			'||htm.submitLink(v_noShowNext||'-'||v_noShowNextStop||' >>', 'form_next', 'goNext()')||'
			</form>
		';
	end if;

	v_html := v_html ||'</td></tr>'||htm.eTable||chr(13);
	v_html := v_html||'<br /><br /><br />'||chr(13);

	v_html := v_html||'</center>'||chr(13);
	v_html := v_html||''||htm.ePage||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		return null;
end;
end generateList;


---------------------------------------------------------
-- name:        advancedSearch
-- what:        advanced search page
-- author:      Frode Klevstul
-- start date:  18.10.2006
---------------------------------------------------------
procedure advancedSearch is begin
	ext_lob.pri(ext_sea.advancedSearch);
end;

function advancedSearch
	return clob
is
begin
declare
	c_procedure			constant mod_procedure.name%type	:= 'advancedSearch';

	v_con_type_pk		con_type.con_type_pk%type;

	v_html				clob;
	v_clob				clob;
begin
	ses_lib.ini(g_package, c_procedure, null);
	ext_lob.ini(v_clob);

	v_html := v_html||''||htm.bPage(g_package, c_procedure)||chr(13);
	v_html := v_html||'<center>'||chr(13);


	v_html := v_html||''||htm.bTable||chr(13);
	v_html := v_html||'<tr><td class="surroundElements">'||chr(13);

	v_html := v_html||''||htm.bBox('Advanced search under development...')||chr(13);
	v_html := v_html||''||htm.bTable||chr(13);
	v_html := v_html||'<tr><td>Your IP address:</td><td>'||owa_util.get_cgi_env('REMOTE_ADDR')||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||''||htm.eBox||chr(13);

	v_html := v_html||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);



	v_html := v_html||'</center>'||chr(13);
	v_html := v_html||''||htm.ePage||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		return null;
end;
end advancedSearch;



/*
---------------------------------------------------------
-- name:        swapAdvancedSearch
-- what:        wrapper for openCloseAdvancedSearch
-- author:      Frode Klevstul
-- start date:  06.02.2006
---------------------------------------------------------
procedure swapAdvancedSearch
is
begin
declare
	c_procedure			constant mod_procedure.name%type 	:= 'swapAdvancedSearch';

	c_login_package		constant sys_parameter.value%type	:= sys_lib.getParameter('login_package');

	v_url				varchar2(256);
	v_openStatus		boolean								:= false;

begin
	ses_lib.ini(g_package, c_procedure);

	-- ---------------------------------------------------
	-- change URL in case it shouldn't be possible to
	-- jump back to the page
	-- ---------------------------------------------------
	v_url := owa_util.get_cgi_env('HTTP_REFERER');

	if (
			lower(v_url) is null
		or	lower(v_url) like lower('%use_pub.login%')
		or	lower(v_url) like lower('%htm_lib.swapAdvancedSearch%')
	) then
		v_url := c_login_package;
	end if;

	-- swap mode
	v_openStatus := htm_lib.openCloseAdvancedSearch(true);

	-- jump back
	htp.p(htm.jumpTo(v_url, 0, null));

end;
end swapAdvancedSearch;


---------------------------------------------------------
-- name:        openCloseAdvancedSearch
-- what:        Open if closed, or close if open the advanced search
-- author:      Frode Klevstul
-- start date:  06.02.2006
---------------------------------------------------------
function openCloseAdvancedSearch
(
	  p_swap				boolean					default false
) return boolean
is
begin
declare
	c_procedure			constant mod_procedure.name%type 	:= 'openCloseAdvancedSearch';

	v_value				varchar2(256);

begin
	ses_lib.ini(g_package, c_procedure);

	-- try to get values from the browser
	v_value := htm_lib.getCookie('in4mant_advancedSearch');

	-- swap open/closed search field
	if (p_swap) then
		if (v_value = 'open') then
			v_value := 'closed';
		else
			v_value := 'open';
		end if;

		-- set cookie
		htm_lib.setCookie('in4mant_advancedSearch', v_value);
	end if;

	if (v_value = 'open') then
		return true;
	else
		return false;
	end if;

end;
end openCloseAdvancedSearch;
*/

---------------------------------------------------------
-- name:        advancedSearch
-- what:        Advanced Search Field
-- author:      Frode Klevstul
-- start date:  06.02.2006
---------------------------------------------------------
/*procedure advancedSearch is begin
	htp.p(htm_lib.advancedSearch);
end;

function advancedSearch
	return clob
is
begin
declare
	c_procedure				constant mod_procedure.name%type 			:= 'advancedSearch';

	c_id					constant ses_session.id%type 				:= ses_lib.getId;
	c_service_pk			constant ser_service.ser_service_pk%type	:= ses_lib.getSer(c_id);
	c_logout_package		constant sys_parameter.value%type			:= sys_lib.getParameter('logout_package');

	c_tex_miscText			constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'miscText');
	c_tex_nameStart			constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'nameStart');
	c_tex_lowAgeLimit		constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'lowAgeLimit');
	c_tex_attractions		constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'attractions');
	c_tex_regionMain		constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'regionMain');
	c_tex_regionSub			constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'regionSub');
	c_tex_typeOfPlace		constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'typeOfPlace');
	c_tex_orgStatus			constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'orgStatus');
	c_tex_doSearch			constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'doSearch');

	v_firstCharsNow			varchar2(8)									:= '0';
	v_firstCharsOld			varchar2(8)									:= '0';
	v_tmp					number;

	cursor	cur_lis_org is
	select	l.name, l.lis_list_pk
	from	lis_list l
	where	l.lis_type_fk = (select lis_type_pk from lis_type where name = 'org_type' and ser_service_fk = c_service_pk);

	cursor	cur_lis_org_other is
	select	l.name, l.lis_list_pk
	from	lis_list l
	where	l.lis_type_fk in (select lis_type_pk from lis_type where name not like 'org_type' and ser_service_fk = c_service_pk);

	cursor	cur_org_status is
	select	name, org_status_pk
	from	org_status
	where	name not like 'banned';

	v_html					clob;

begin
	ses_lib.ini(g_package, c_procedure);

	-- ---------------------------------------
	-- if c_service_pk is null this is due to a recent logout. We have to
	-- reload page (jump to logout page) to make sure the service_pk is a assigned a value.
	-- ---------------------------------------
	if (c_service_pk is null) then
		v_html := htm.jumpTo(c_logout_package, 1, null, false);
		return v_html;
	end if;

	v_html := v_html||''||htm.bForm('ext_lib.generateList', 'advancedSearch')||chr(13);
	v_html := v_html||'<input type="hidden" name="p_type" value="orgAdvanced" />'||chr(13);

	-- create the javascript code for regions in Norway
	v_html := v_html||''||htm.jsFindRegion('Norway')||chr(13);

	htp.p(v_html);
	v_html := '';

	v_html := v_html||'
		<script language="JavaScript" type="text/javascript">

			function saveParameters(parameters){
*/
				/* select out all values on the page */
/*				if(document.getElementById(''p_text_advanced'')){
					var p_text_advanced = document.getElementById(''p_text_advanced'');
				} else {
					var p_text_advanced = document.createElement(''tmp'');
					p_text_advanced.value = '''';
				}
				if(document.getElementById(''p_first_letter'')){
					var p_first_letter = document.getElementById(''p_first_letter'');
				} else {
					var p_first_letter = document.createElement(''tmp'');
					p_first_letter.value = '''';
				}
				if(document.getElementById(''p_age_limit'')){
					var p_age_limit = document.getElementById(''p_age_limit'');
				} else {
					var p_age_limit = document.createElement(''tmp'');
					p_age_limit.value = '''';
				}
				if(document.getElementById(''opt_id_type'')){
					var opt_id_type = document.getElementById(''opt_id_type'');
				} else {
					var opt_id_type = document.createElement(''tmp'');
					opt_id_type.value = '''';
				}
				if(document.getElementById(''opt_id_style'')){
					var opt_id_style = document.getElementById(''opt_id_style'');
				} else {
					var opt_id_style = document.createElement(''tmp'');
					opt_id_style.value = '''';
				}
				if(document.getElementById(''p_org_status_1'')){
					if(document.getElementById(''p_org_status_1'').checked){
						var p_org_status_1 = document.getElementById(''p_org_status_1'');
					} else {
						var p_org_status_1 = document.createElement(''tmp'');
						p_org_status_1.value = '''';
					}
				} else {
					var p_org_status_1 = document.createElement(''tmp'');
					p_org_status_1.value = '''';
				}
				if(document.getElementById(''p_org_status_2'')){
					if(document.getElementById(''p_org_status_2'').checked){
						var p_org_status_2 = document.getElementById(''p_org_status_2'');
					} else {
						var p_org_status_2 = document.createElement(''tmp'');
						p_org_status_2.value = '''';
					}
				} else {
					var p_org_status_2 = document.createElement(''tmp'');
					p_org_status_2.value = '''';
				}
				if(document.getElementById(''p_org_status_3'')){
					if(document.getElementById(''p_org_status_3'').checked){
						var p_org_status_3 = document.getElementById(''p_org_status_3'');
					} else {
						var p_org_status_3 = document.createElement(''tmp'');
						p_org_status_3.value = '''';
					}
				} else {
					var p_org_status_3 = document.createElement(''tmp'');
					p_org_status_3.value = '''';
				}
		';
	-- --------------------------------------------------------------------------------------------------
	-- the reason for calling the parameters 't' (type of place) and 'a' (attractions) is to
	-- avoid the parameter string getting too long. If it's too long it can't be stored as a cookie
	-- except for this the parameters would have been called p_type_of_place and p_attraction
	-- --------------------------------------------------------------------------------------------------
	for v_tmp in 1..30 loop
		v_html := v_html||'
			if(document.getElementById(''t'||v_tmp||''')){
				if(document.getElementById(''t'||v_tmp||''').checked){
					var t'||v_tmp||' = document.getElementById(''t'||v_tmp||''');
				} else {
					var t'||v_tmp||' = document.createElement(''tmp'');
					t'||v_tmp||'.value = '''';
				}
			} else {
				var t'||v_tmp||' = document.createElement(''tmp'');
				t'||v_tmp||'.value = '''';
			}
		';
	end loop;
	for v_tmp in 1..150 loop
		v_html := v_html||'
			if(document.getElementById(''a'||v_tmp||''')){
				if(document.getElementById(''a'||v_tmp||''').checked){
					var a'||v_tmp||' = document.getElementById(''a'||v_tmp||''');
				} else {
					var a'||v_tmp||' = document.createElement(''tmp'');
					a'||v_tmp||'.value = '''';
				}
			} else {
				var a'||v_tmp||' = document.createElement(''tmp'');
				a'||v_tmp||'.value = '''';
			}
		';
		htp.p(v_html);
		v_html := '';
	end loop;
	v_html := v_html||'
*/				/* build a parameter string */
/*				var parameterString = ''''
				+''p_text_advanced=''+p_text_advanced.value
				+''&p_first_letter=''+p_first_letter.value
				+''&p_age_limit=''+p_age_limit.value
				+''&opt_id_type=''+opt_id_type.value
				+''&opt_id_style=''+opt_id_style.value
				+''&p_org_status_1=''+p_org_status_1.value
				+''&p_org_status_2=''+p_org_status_2.value
				+''&p_org_status_3=''+p_org_status_3.value
	';
	for v_tmp in 1..30 loop
		v_html := v_html||'
			+''&t'||v_tmp||'=''+t'||v_tmp||'.value
		';
	end loop;
	for v_tmp in 1..150 loop
		v_html := v_html||'
			+''&a'||v_tmp||'=''+a'||v_tmp||'.value
		';
	end loop;
	v_html := v_html||'
				+'''';

*/				/* set the cookie using javascript */
/*				createCookie(''in4mant_search'', parameterString, 0.0001);
			}

			function loadParameters(){
				var parameters = readCookie(''in4mant_search'');

				if (parameters == null){
					return;
				}

*/				/* add all parameters in an array */
/*				var myarray 	= parameters.split(/&/);
				var mysubarray;

*/				/* go through the parameter array and get the values */
/*				for (var i = 0; i < myarray.length; i++) {
					mysubarray = myarray[i].split(/=/);

					if ( mysubarray[1].length > 0 ) {
*/						/* set form values */
						/*alert(mysubarray[0] +'':''+ mysubarray[1]);*/
/*
						if(document.getElementById(mysubarray[0])){
							document.getElementById(mysubarray[0]).value = mysubarray[1];

*/							/* load councils if county is found */
/*							if (mysubarray[0] == ''opt_id_type''){
								showMunicipal();
							}

*/							/* set org_status checkboxes */
/*							if (mysubarray[0] == ''p_org_status_1''){
								document.getElementById(''p_org_status_1'').checked = true;
							}
							if (mysubarray[0] == ''p_org_status_2''){
								document.getElementById(''p_org_status_2'').checked = true;
							}
							if (mysubarray[0] == ''p_org_status_3''){
								document.getElementById(''p_org_status_3'').checked = true;
							}
	';
	for v_tmp in 1..30 loop
		v_html := v_html||'
							if (mysubarray[0] == ''t'||v_tmp||'''){
								document.getElementById(''t'||v_tmp||''').checked = true;
							}
		';
	end loop;
	for v_tmp in 1..150 loop
		v_html := v_html||'
							if (mysubarray[0] == ''a'||v_tmp||'''){
								document.getElementById(''a'||v_tmp||''').checked = true;
							}
		';
		htp.p(v_html);
		v_html := '';
	end loop;
	v_html := v_html||'
						}
					}
				}
			}

			function submitForm(){
				saveParameters();
				document.advancedSearch.submit();
			}

		</script>
	';


	v_html := v_html||''||htm.bTable('advancedSearch')||chr(13);

	-- ------------------------------
	--
	-- different search options...
	--
	-- ------------------------------
	v_html := v_html||'
		<tr class="greyBackground">
			<td valign="top" width="28%">
	';

	-- -------------------
	-- text
	-- -------------------
	v_html := v_html||'<input type="hidden" name="p_name" value="miscText" />'||chr(13);
	v_html := v_html||''||c_tex_miscText||' <input type="text" name="p_value" id="p_text_advanced" maxlength="50" size="30" />'||chr(13);

	v_html := v_html||'
			</td>
			<td valign="top" width="16%">
	';

	-- -----------------------------------
	-- name first letter
	-- -----------------------------------
	v_html := v_html||'<input type="hidden" name="p_name" value="nameFirstLetter" />'||chr(13);
	v_html := v_html||'
					&nbsp;'||c_tex_nameStart||'
					<select name="p_value" id="p_first_letter" style="width: 50px">
					<option value="">&nbsp;</option>
					<option value="A">A</option>
					<option value="B">B</option>
					<option value="C">C</option>
					<option value="D">D</option>
					<option value="E">E</option>
					<option value="F">F</option>
					<option value="G">G</option>
					<option value="H">H</option>
					<option value="I">I</option>
					<option value="J">J</option>
					<option value="K">K</option>
					<option value="L">L</option>
					<option value="M">M</option>
					<option value="N">N</option>
					<option value="O">O</option>
					<option value="P">P</option>
					<option value="Q">Q</option>
					<option value="R">R</option>
					<option value="S">S</option>
					<option value="T">T</option>
					<option value="U">U</option>
					<option value="V">V</option>
					<option value="W">W</option>
					<option value="X">X</option>
					<option value="Y">Y</option>
					<option value="Z">Z</option>
					<option value="�">�</option>
					<option value="�">�</option>
					<option value="�">�</option>
					</select>';

	v_html := v_html||'
			</td>
			<td valign="top" width="17%">
	';

	-- -------------------
	-- lower age limit
	-- -------------------
	v_html := v_html||'<input type="hidden" name="p_name" value="lowAgeLimit" />';
	v_html := v_html||'
					&nbsp;'||c_tex_lowAgeLimit||'
						<select name="p_value" id="p_age_limit" style="width: 50px">
						<option value="0">&nbsp;</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>
						<option value="24">24</option>
						<option value="25">25</option>
						<option value="26">26</option>
						<option value="27">27</option>
						<option value="28">28</option>
						<option value="29">29</option>
						<option value="30">30</option>
						<option value="31">31</option>
						<option value="32">32</option>
						<option value="33">33</option>
						<option value="34">34</option>
						<option value="35">35</option>
						</select>';

	v_html := v_html||'
			</td>
			<td style="padding-left: 10px;" valign="bottom">
	';

	v_html := v_html||''||c_tex_attractions;

	v_html := v_html||'
			</td>
		</tr>

		<tr class="greyBackground">
			<td colspan="3">
	';

	-- -------------------
	-- region
	-- -------------------
	v_html := v_html||'
		<input type="hidden" name="p_name" value="region_main" />'||
		c_tex_regionMain||' <span style="padding-left: 11px;">&nbsp;</span>
		<select id="opt_id_type" name="p_value" onChange="showMunicipal();" style="width: 200px"></select>
		<span style="padding-left: 10px;">&nbsp;</span>

		<input type="hidden" name="p_name" value="region_sub" />
		'||c_tex_regionSub||'
		<span style="padding-left: 6px;">&nbsp;</span>
		<select id="opt_id_style" name="p_value" style="width: 216px"></select>
		<script type="text/javascript" language="javascript">start()</script>
	';

	v_html := v_html||'</td>'||chr(13);
	v_html := v_html||'<td valign="top" rowspan="2" style="padding-left: 10px;">'||chr(13);

	htp.p(v_html);
	v_html := '';

	-- -------------------
	-- other values / attractions
	-- -------------------
	v_html := v_html||'<div class="attractionsScroll">'||chr(13);
	v_tmp := 0;
	for r_cursor in cur_lis_org_other
	loop
		v_tmp := v_tmp + 1;
		-- -------------------------------------------------
		-- write out heading for each different list type
		-- -------------------------------------------------
		if (r_cursor.name is not null) then
			v_firstCharsNow := substr(r_cursor.name,1,(instr(r_cursor.name,'_')-1));
			v_firstCharsNow := substr(v_firstCharsNow,1,(length(v_firstCharsNow)-2));
		end if;

		if (v_firstCharsOld != v_firstCharsNow) then
			v_firstCharsOld := v_firstCharsNow;
			v_html := v_html||'<br />'||tex_lib.get('db', 'lis_list', v_firstCharsOld)||'<br />'||chr(13);
		end if;

		v_html := v_html||'<input type="hidden" name="p_name" value="list" />'||chr(13);
		v_html := v_html||'<input type="checkbox" name="p_value" id="a'||v_tmp||'" value="lis_list_pk:'||r_cursor.lis_list_pk||'" /> '||tex_lib.get('db', 'lis_list', r_cursor.name)||'<br />'||chr(13);
	end loop;
	v_html := v_html||'</div>'||chr(13);

	htp.p(v_html);
	v_html := '';

	v_html := v_html||'</td>'||chr(13);
	v_html := v_html||'</tr>'||chr(13);
	v_html := v_html||'<tr class="greyBackground">'||chr(13);
	v_html := v_html||'<td colspan="1" valign="bottom">'||chr(13);

	-- -------------------
	-- type of place
	-- -------------------
	v_html := v_html||' '||c_tex_typeOfPlace||chr(13);
	v_html := v_html||'<br /><div class="typeOfOrgScroll">'||chr(13);
	v_tmp := 0;
	for r_cursor in cur_lis_org
	loop
		v_tmp := v_tmp + 1;
		v_html := v_html||'
			<input type="hidden" name="p_name" value="list" />
			<input type="checkbox" name="p_value" id="t'||v_tmp||'" value="lis_list_pk:'||r_cursor.lis_list_pk||'" /> '||tex_lib.get('db', 'lis_list', r_cursor.name)||'<br />
		';
	end loop;
	v_html := v_html||'</div>'||chr(13);

	v_html := v_html||'</td>'||chr(13);
	v_html := v_html||'<td colspan="2" valign="top">'||chr(13);

	-- ---------------------------
	-- membership / org_status
	-- ---------------------------
	v_tmp := 0;
	for r_cursor in cur_org_status
	loop
		v_tmp := v_tmp + 1;
		v_html := v_html||'<input type="hidden" name="p_name" value="list" />'||chr(13);
		v_html := v_html||'<input type="checkbox" name="p_value" id="p_org_status_'||v_tmp||'" value="org_status_pk:'||r_cursor.org_status_pk||'" /> '||tex_lib.get('db', 'org_status', r_cursor.name)||'
							<span style="padding-right: 10px">&nbsp;</span>
		';
	end loop;

	v_html := v_html||'</td>'||chr(13);
	v_html := v_html||'</tr>'||chr(13);

	v_html := v_html||'
		<tr>
			<td colspan="4">
				<span style="padding-left: 770px;">
				'||htm.submitLink(c_tex_doSearch, 'advancedSearch', 'submitForm()')||'
				</span>
			</td>
		</tr>';
	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||''||htm.eForm||chr(13);

	v_html := v_html||'
			<script type="text/javascript">
			<!--
	      		loadParameters();
			//-->
			</script>'||chr(13);

	return v_html;

end;
end advancedSearch;
*/


-- ******************************************** --
end;
/
show errors;
