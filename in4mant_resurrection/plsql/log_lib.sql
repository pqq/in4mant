PROMPT *** log_lib ***
set define off
set serveroutput on;
alter session set plsql_warnings='enable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package log_lib is
/* ******************************************************
*	Package:     log_lib
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	050724 | Frode Klevstul
*			- Package created
****************************************************** */
	procedure run;
	procedure logError
	(
		  p_package				in		mod_package.name%type
		, p_procedure			in		mod_procedure.name%type
		, p_message				in		log_error.message%type
	);
	procedure logError
	(
		  p_package				in		mod_package.name%type
		, p_procedure			in		mod_procedure.name%type
		, p_message				in		log_error.message%type
		, p_bool_critical		in		log_error.bool_critical%type
	);
	procedure logError
	(
		  p_package				in		mod_package.name%type
		, p_procedure			in		mod_procedure.name%type
		, p_message				in		log_error.message%type
		, p_bool_critical		in		log_error.bool_critical%type
		, p_ses_session_id_new	in		log_error.ses_session_id_new%type
	);
	function errorString
	(
		  p_package				in		mod_package.name%type
		, p_procedure			in		mod_procedure.name%type
		, p_tex_text_pk			in		varchar2
	) return varchar2;
	procedure logDebug
	(
		  p_message				in		log_debug.message%type
	);
	procedure logTex
	(
		  p_tex_text_pk			in		log_tex.tex_text_fk_pk%type
		, p_tex_language_pk		in		log_tex.tex_language_fk_pk%type
	);
	procedure logHits
	(
		  p_table_name			in		log_hits.table_name%type
		, p_foreign_key_fk		in		log_hits.foreign_key_fk%type
	);
	procedure getHits
	(
		  p_table_name			in		log_hits.table_name%type
		, p_foreign_key_fk		in		log_hits.foreign_key_fk%type
	);
	function getHits
	(
		  p_table_name			in		log_hits.table_name%type
		, p_foreign_key_fk		in		log_hits.foreign_key_fk%type
	) return number;

end;
/
show errors;





-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body log_lib is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'log_lib';
g_id		ses_session.id%type	:= ses_lib.getId;

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  24.07.2005
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        logError
-- what:        log error in error log table
-- author:      Frode Klevstul
-- start date:  25.07.2005
---------------------------------------------------------
procedure logError (p_package in mod_package.name%type, p_procedure in mod_procedure.name%type, p_message in log_error.message%type)
is
begin
	log_lib.logError(p_package, p_procedure, p_message, 0, null);
end;

procedure logError (p_package in mod_package.name%type, p_procedure in mod_procedure.name%type, p_message in log_error.message%type, p_bool_critical in log_error.bool_critical%type)
is
begin
	log_lib.logError(p_package, p_procedure, p_message, p_bool_critical, null);
end;

procedure logError
(
	  p_package				in		mod_package.name%type
	, p_procedure			in		mod_procedure.name%type
	, p_message				in		log_error.message%type
	, p_bool_critical		in		log_error.bool_critical%type
	, p_ses_session_id_new	in		log_error.ses_session_id_new%type
)
is
begin
declare
	c_procedure			constant mod_procedure.name%type := 'logError';
begin
	ses_lib.ini(g_package, c_procedure);

	insert into log_error
	(log_error_pk, message, ses_session_id, ses_session_id_new, bool_critical, date_insert)
	values
	(
		  seq_log_error.nextval
		, p_procedure||'.'||p_package||':'||p_message
		, g_id
		, p_ses_session_id_new
		, p_bool_critical
		, sysdate
	);
	commit;

end;
end logError;


---------------------------------------------------------
-- name:        errorString
-- what:        return formatted error string
-- author:      Frode Klevstul
-- start date:  03.03.2006
---------------------------------------------------------
function errorString
(
	  p_package				in		mod_package.name%type
	, p_procedure			in		mod_procedure.name%type
	, p_tex_text_pk			in		varchar2
) return varchar2
is
begin
declare
    c_procedure constant mod_procedure.name%type := 'errorString';

	v_error_string		tex_text.string%type;
	v_string			varchar2(4096);

begin
	ses_lib.ini(g_package, c_procedure);

	if ( (length(p_tex_text_pk) <= 128) and  (p_package is not null) and (p_procedure is not null) ) then
		v_error_string := tex_lib.get(p_package, p_procedure, p_tex_text_pk);
	else
		v_error_string := p_tex_text_pk;
	end if;

	v_string := '<center><h1>E R R O R :</h1> '||v_error_string||'<br></center>'||chr(13);

	return v_string;

end;
end errorString;


---------------------------------------------------------
-- name:        logDebug
-- what:        log debug info in debug table
-- author:      Frode Klevstul
-- start date:  25.07.2005
---------------------------------------------------------
procedure logDebug
(
	  p_message				in		log_debug.message%type
)
is
begin
declare
	c_procedure			constant mod_procedure.name%type := 'logDebug';
begin
	ses_lib.ini(g_package, c_procedure);

	insert into log_debug
	(log_debug_pk, message, date_insert)
	values
	(
		  seq_log_debug.nextval
		, p_message
		, sysdate
	);
	commit;

end;
end logDebug;


---------------------------------------------------------
-- name:        logDebug
-- what:        log TEX module usage
-- author:      Frode Klevstul
-- start date:  25.07.2005
---------------------------------------------------------
procedure logTex
(
	  p_tex_text_pk			in		log_tex.tex_text_fk_pk%type
	, p_tex_language_pk		in		log_tex.tex_language_fk_pk%type
)
is
begin
declare
	c_procedure			constant mod_procedure.name%type := 'logDebug';
	c_log_tex			constant sys_parameter.value%type := sys_lib.getParameter('log_tex');

begin
	ses_lib.ini(g_package, c_procedure);

	if (c_log_tex='1') then
		update	log_tex
		set		date_accessed 		= sysdate
		where	tex_text_fk_pk 		= p_tex_text_pk
		and		tex_language_fk_pk 	= p_tex_language_pk;
		if (sql%notfound) then
			insert into log_tex
			(tex_text_fk_pk, tex_language_fk_pk, date_accessed)
			values(
				  p_tex_text_pk
				, p_tex_language_pk
				, sysdate
			);
		end if;
		commit;
	end if;

end;
end logTex;


---------------------------------------------------------
-- name:        logHits
-- what:        log hits in log_hits table
-- author:      Frode Klevstul
-- start date:  25.11.2005
---------------------------------------------------------
procedure logHits
(
	  p_table_name			in		log_hits.table_name%type
	, p_foreign_key_fk		in		log_hits.foreign_key_fk%type
)
is
begin
declare
	c_procedure			constant mod_procedure.name%type := 'logHits';
begin
	ses_lib.ini(g_package, c_procedure);

	update	log_hits
	set		hits 			= hits + 1
	where	table_name 		= p_table_name
	and		foreign_key_fk 	= p_foreign_key_fk;
	if (sql%notfound) then
		insert into log_hits
		(log_hits_pk, hits, table_name, foreign_key_fk)
		values(
			  seq_log_hits.nextval
			, 1
			, p_table_name
			, p_foreign_key_fk
		);
	end if;
	commit;

end;
end logHits;


---------------------------------------------------------
-- name:        getHits
-- what:        return hits in log_hits
-- author:      Frode Klevstul
-- start date:  04.01.2005
---------------------------------------------------------
procedure getHits(p_table_name in log_hits.table_name%type, p_foreign_key_fk in log_hits.foreign_key_fk%type) is begin
	htp.p(log_lib.getHits(p_table_name, p_foreign_key_fk));
end;

function getHits
(
	  p_table_name			in		log_hits.table_name%type
	, p_foreign_key_fk		in		log_hits.foreign_key_fk%type
) return number
is
begin
declare
	c_procedure			constant mod_procedure.name%type	:= 'viewLogHits';

	cursor	cur_log_hits is
	select	hits
	from	log_hits
	where	table_name		= p_table_name
	and		foreign_key_fk	= p_foreign_key_fk;

begin
	ses_lib.ini(g_package, c_procedure);

	for r_cursor in cur_log_hits
	loop
		return r_cursor.hits;
	end loop;

	return 0;

end;
end getHits;


-- ******************************************** --
end;
/
show errors;
