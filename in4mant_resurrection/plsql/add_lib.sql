PROMPT *** add_lib ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package add_lib is
/* ******************************************************
*	Package:     tex_lib
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	060705 | Frode Klevstul
*			- Package created
****************************************************** */
	type parameter_arr 			is table of varchar2(4000) index by binary_integer;
	empty_array 				parameter_arr;

	procedure run;
	procedure getMap
	(
		  p_address_pk			in add_address.add_address_pk%type				default null
		, p_mapWidth			in number										default 300
		, p_mapHeight			in number										default 300
		, p_mapTxt				in varchar2										default null
		, p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	);
	function getMap
	(
		  p_address_pk			in add_address.add_address_pk%type				default null
		, p_mapWidth			in number										default 300
		, p_mapHeight			in number										default 300
		, p_mapTxt				in varchar2										default null
		, p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	) return clob;
	procedure addLongLat
	(
		  p_address_pk			in add_address.add_address_pk%type				default null
		, p_longitude			in varchar2										default null
		, p_latitude			in varchar2										default null
		, p_returnUrl			in varchar2										default null
	);
	function addLongLat
	(
		  p_address_pk			in add_address.add_address_pk%type				default null
		, p_longitude			in varchar2										default null
		, p_latitude			in varchar2										default null
		, p_returnUrl			in varchar2										default null
	) return clob;

end;
/
show errors;





-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body add_lib is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'add_lib';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  05.07.2006
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        getMap
-- what:        get map for an address
-- author:      Frode Klevstul
-- start date:  05.07.2006
---------------------------------------------------------
procedure getMap
(
	  p_address_pk			in add_address.add_address_pk%type				default null
	, p_mapWidth			in number										default 300
	, p_mapHeight			in number										default 300
	, p_mapTxt				in varchar2										default null
	, p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
) is begin
	ext_lob.pri(add_lib.getMap(p_address_pk, p_mapWidth, p_mapHeight, p_mapTxt));
end;

function getMap
(
	  p_address_pk			in add_address.add_address_pk%type				default null
	, p_mapWidth			in number										default 300
	, p_mapHeight			in number										default 300
	, p_mapTxt				in varchar2										default null
	, p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type			:= 'getMap';

	c_tex_map					constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'map');
	c_tex_noMap					constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'noMap');
	c_tex_biggerMap				constant tex_text.string%type				:= tex_lib.get(g_package, c_procedure, 'biggerMap');

	v_ext_geocode				varchar2(256);
	v_longitude					add_address.longitude%type;
	v_latitude					add_address.latitude%type;
	v_longitudeTxt				varchar2(32);
	v_latitudeTxt				varchar2(32);
	v_mapKey					varchar2(128);
	v_servername				varchar2(32)								:= owa_util.get_cgi_env('SERVER_NAME');
	v_street					add_address.street%type;
	v_zip						add_address.zip%type;
	v_mapInfoText				varchar2(512);
	v_noMap						boolean										:= false;

	cursor	cur_address is
	select	longitude, latitude, street, zip
	from	add_address
	where	add_address_pk = p_address_pk;

	v_html						clob;
	v_clob						clob;
	v_SSL	varchar2(100)	:= owa_util.get_cgi_env('HTTPS');

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	for r_cursor in cur_address
	loop
		v_longitude			:= r_cursor.longitude;
		v_latitude			:= r_cursor.latitude;
		v_longitudeTxt		:= replace(v_longitude, ',', '.');
		v_latitudeTxt		:= replace(v_latitude, ',', '.');

		v_street			:= htm_lib.stripIllegalTextFromUrl(r_cursor.street);
		v_zip				:= r_cursor.zip;
	end loop;

	v_html := v_html||''||htm.bBox(c_tex_map)||chr(13);
	v_html := v_html||''||htm.bTable||chr(13);
	v_html := v_html||'<tr><td>'||chr(13);

	if ( (v_longitude is null or v_latitude is null) or (v_longitude = 0 or v_latitude = 0) ) then
		--v_html := v_html||''||c_tex_noMap||chr(13);
		v_html := v_html||''||chr(13);
		v_noMap := true;

	else
		-- -------------------------------
		-- set Google Map API key
		-- -------------------------------
		-- us.uteliv.no
		if (v_servername like '%us.uteliv.no%') then
			v_mapKey := 'ABQIAAAAwEsOwz1n49q-0lRdw3ZO_BQkApc8rdUtJGwIxGEb-h6d7FIiPBRbStqrxyAYltZURz0_ea-sJC_G1Q';

		-- host.uteliv.no
		elsif (v_servername like '%host.uteliv.no%') then
			v_mapKey := 'ABQIAAAAwEsOwz1n49q-0lRdw3ZO_BTsxcmIMvvGj9T0T2h7sOOBc8RtOhSUBwDH0Zf-F5gktOvRH9CS3DiHsg';

		-- uteliv.no
		elsif (v_servername like '%uteliv.no%') then
			v_mapKey := 'ABQIAAAAwEsOwz1n49q-0lRdw3ZO_BReyKE7i3NlIBoChoN82LY0YGPgGxTQNZjpdbcj9GaqQjUA0GPRnUYJcg';

		-- test/tryout.uteliv.no (http://84.48.134.192/)
		elsif (v_servername like '%84.48.134.192%') then
			v_mapKey := 'ABQIAAAAwEsOwz1n49q-0lRdw3ZO_BSiKeQGNEVhL5SSNnIbZkpQ-EZaoxTXwOpaEI5JTUVGtr6H70SwC4GEXA';

		-- test/tryout.uteliv.no (http://192.168.2.102/)
		elsif (v_servername like '%192.168.2.102%') then
			v_mapKey := 'ABQIAAAAwEsOwz1n49q-0lRdw3ZO_BTwYZrfuq8RhPrMzx_6oEMr4Xj-YxTSW3Edmj4wFiCVaVuvlRR2SP2oyg';

		-- test/tryout.uteliv.no (http://192.168.1.32/)
		elsif (v_servername like '%192.168.1.32%') then
			v_mapKey := 'ABQIAAAAwEsOwz1n49q-0lRdw3ZO_BSmjfopdTb9fvKiBpPAMc-LMAIRfxQdDZEP2UfN_ePh3HCNOrmyjApRNQ';

		else
			htm.printErrors('unknown servername: '||v_servername);
		end if;

		-- build text to show on map
		if (p_mapTxt is not null) then
			v_mapInfoText := '<b>'||p_mapTxt||'&nbsp;</b><br />';
		end if;

		v_street := htm_lib.stripNewline(v_street);
		v_mapInfoText := v_mapInfoText||''||v_street||'<br />'||''||v_zip||' '||geo_lib.getCity(v_zip);


	        IF ( v_SSL = 'on' ) THEN
			-- https not supported yet for google maps
                	v_SSL := 'http';
        	ELSE
                	v_SSL := 'http';
	        END IF;




		v_html := v_html||'
			    <script src="'||v_SSL||'://maps.google.com/maps?file=api&amp;v=2&amp;key='||v_mapKey||'"
			      type="text/javascript"></script>
			    <script type="text/javascript">

			    //<![CDATA[

				// A TextualZoomControl is a GControl that displays textual "Zoom In"
				// and "Zoom Out" buttons (as opposed to the iconic buttons used in
				// Google Maps).
				function TextualZoomControl() {
				}
				TextualZoomControl.prototype = new GControl();

				// Creates a one DIV for each of the buttons and places them in a container
				// DIV which is returned as our control element. We add the control to
				// to the map container and return the element for the map class to
				// position properly.
				TextualZoomControl.prototype.initialize = function(map) {
				  var container = document.createElement("div");

				  var zoomInDiv = document.createElement("div");
				  this.setButtonStyle_(zoomInDiv);
				  container.appendChild(zoomInDiv);
				  zoomInDiv.appendChild(document.createTextNode("+"));
				  GEvent.addDomListener(zoomInDiv, "click", function() {
				    map.zoomIn();
				  });

				  var zoomOutDiv = document.createElement("div");
				  this.setButtonStyle_(zoomOutDiv);
				  container.appendChild(zoomOutDiv);
				  zoomOutDiv.appendChild(document.createTextNode("-"));
				  GEvent.addDomListener(zoomOutDiv, "click", function() {
				    map.zoomOut();
				  });

				  var normalMapDiv = document.createElement("div");
				  this.setButtonStyle_(normalMapDiv);
				  container.appendChild(normalMapDiv);
				  normalMapDiv.appendChild(document.createTextNode("N"));
				  GEvent.addDomListener(normalMapDiv, "click", function() {
				    map.setMapType(G_NORMAL_MAP);
				  });

				  var sateliteMapDiv = document.createElement("div");
				  this.setButtonStyle_(sateliteMapDiv);
				  container.appendChild(sateliteMapDiv);
				  sateliteMapDiv.appendChild(document.createTextNode("S"));
				  GEvent.addDomListener(sateliteMapDiv, "click", function() {
				    map.setMapType(G_SATELLITE_MAP);
				  });

				  var hybridMapDiv = document.createElement("div");
				  this.setButtonStyle_(hybridMapDiv);
				  container.appendChild(hybridMapDiv);
				  hybridMapDiv.appendChild(document.createTextNode("H"));
				  GEvent.addDomListener(hybridMapDiv, "click", function() {
				    map.setMapType(G_HYBRID_MAP);
				  });

				  map.getContainer().appendChild(container);
				  return container;
				}

				// By default, the control will appear in the top left corner of the
				// map with 7 pixels of padding.
				TextualZoomControl.prototype.getDefaultPosition = function() {
				  return new GControlPosition(G_ANCHOR_TOP_LEFT, new GSize(7, 7));
				}

				// Sets the proper CSS for the given button element.
				TextualZoomControl.prototype.setButtonStyle_ = function(button) {
				  button.style.textDecoration = "none";
				  button.style.color = "#0000cc";
				  button.style.backgroundColor = "white";
				  button.style.font = "small Arial";
				  button.style.border = "1px solid black";
				  button.style.padding = "0px";
				  button.style.marginBottom = "3px";
				  button.style.textAlign = "center";
				  button.style.width = "1em";
				  button.style.cursor = "pointer";
				}

			    function load() {
			      if (GBrowserIsCompatible()) {
			        var map = new GMap2(document.getElementById("map"));
			        var point = new GLatLng('||v_latitudeTxt||', '||v_longitudeTxt||');
					map.addControl(new TextualZoomControl());
					/* map.addControl(new GSmallMapControl()); */
			        map.setCenter(point, 13);

					// Create our own marker icon
					var icon = new GIcon();
					icon.image = "http://uteliv.no/img/i4/mapMarker_36.png";
					//icon.shadow = "http://uteliv.no/img/i4/mapMarker_30_shadow.png";
					//icon.iconSize = new GSize(20, 30);
					icon.iconSize = new GSize(36, 36);
					//icon.shadowSize = new GSize(22, 30);
					icon.iconAnchor = new GPoint(18, 18);
					//icon.iconAnchor = new GPoint(6, 30);
					icon.infoWindowAnchor = new GPoint(5, 1);

			        var marker = new GMarker(map.getCenter(), icon);
			        GEvent.addListener(marker, "click", function() {
						marker.openInfoWindowHtml("'||v_mapInfoText||'");
					});
			        map.addOverlay(marker);
			        map.openInfoWindow(map.getCenter());
			      }
			    }

			    //]]>
			    </script>
			  </head>
			  <body onload="load()" onunload="GUnload()">
			    <div id="map" style="width: '||p_mapWidth||'px; height: '||p_mapHeight||'px"></div>
			  </body>
		'||chr(13);

	end if;

	v_html := v_html||'</td></tr>'||chr(13);

	if (v_noMap = false and p_mapWidth < 300) then
		v_html := v_html||'<tr><td style="text-align: center;">'||chr(13);
		v_html := v_html||''||htm.link(c_tex_biggerMap,'add_pub.viewMap?p_address_pk='||p_address_pk||'&amp;p_organisation_pk='||p_organisation_pk)||''||chr(13);
		v_html := v_html||'</td></tr>'||chr(13);
	end if;


	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||''||htm.eBox||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end getMap;


---------------------------------------------------------
-- name:        addLongLat
-- what:        add longitude and latitude for a record in ADD
-- author:      Frode Klevstul
-- start date:  27.07.2006
---------------------------------------------------------
procedure addLongLat
(
	  p_address_pk			in add_address.add_address_pk%type				default null
	, p_longitude			in varchar2										default null
	, p_latitude			in varchar2										default null
	, p_returnUrl			in varchar2										default null
) is begin
	ext_lob.pri(add_lib.addLongLat(p_address_pk, p_longitude, p_latitude, p_returnUrl));
end;

function addLongLat
(
	  p_address_pk			in add_address.add_address_pk%type				default null
	, p_longitude			in varchar2										default null
	, p_latitude			in varchar2										default null
	, p_returnUrl			in varchar2										default null
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type			:= 'addLongLat';

	v_longitude					add_address.longitude%type;
	v_latitude					add_address.latitude%type;
	v_longitudeTxt				varchar2(32);
	v_latitudeTxt				varchar2(32);

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	if (p_address_pk is not null and p_longitude is not null and p_latitude is not null) then
		v_longitudeTxt	:= replace(p_longitude, '.', ',');
		v_latitudeTxt	:= replace(p_latitude, '.', ',');
		v_longitude		:= to_number(v_longitudeTxt);
		v_latitude		:= to_number(v_latitudeTxt);

		-- --------------------------------------------------------------------------------------------
		-- if this function is called with long / lat equals to 0 we do check the his_history
		-- table for old coordinates that do work. This is to avoid the coordinates being 0 after
		-- an adjustment of the address.
		-- --------------------------------------------------------------------------------------------
		if (v_longitude = 0 and v_latitude = 0) then
			v_longitude	:= his_lib.getEntry('add_address', 'longitude');
			v_latitude	:= his_lib.getEntry('add_address', 'latitude');
			
			if (v_longitude is null or v_latitude is null) then
				v_longitude	:= 0;
				v_latitude	:= 0;
			end if;
		end if;

		update	add_address
		set		  longitude = v_longitude
				, latitude = v_latitude
		where	add_address_pk = p_address_pk;
		
		commit;			
	end if;

	if (p_returnUrl is not null) then
		htm.jumpTo(p_returnUrl, 0, null);
	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end addLongLat;


-- ******************************************** --
end;
/
show errors;
