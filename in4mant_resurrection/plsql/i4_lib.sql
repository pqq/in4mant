PROMPT *** i4_lib ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package i4_lib is
/* ******************************************************
*	Package:     tex_lib
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	051222 | Frode Klevstul
*			- Package created
****************************************************** */
	procedure run;
	procedure orgAdm;
	function orgAdm
		return clob;
	procedure memAdm;
	function memAdm
		return clob;
	procedure extAdm;
	function extAdm
		return clob;
	procedure texAdm;
	function texAdm
		return clob;

end;
/
show errors;





-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body i4_lib is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'i4_lib';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  22.12.2005
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);	
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        orgAdm
-- what:        admin of an organisation
-- author:      Frode Klevstul
-- start date:  22.12.2005
---------------------------------------------------------
procedure orgAdm is begin
    ext_lob.pri(i4_lib.orgAdm);
end;

function orgAdm
	return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type 		:= 'orgAdm';

	v_html 						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := v_html||''||htm.bBox('ORG')||chr(13);
	v_html := v_html||''||htm.bTable||chr(13);
	v_html := v_html||'<tr><td colspan="3"><b>Insert:</b></td></tr>'||chr(13);
	v_html := v_html||'<tr><td width="10%">org_i01:</td><td width="30%"><a href="org_i4.addOrg?p_bool_umbrella_organisation=1">New parent organisation</a></td><td><i>The owner of one or more organisations</i></td></tr>'||chr(13);
	v_html := v_html||'<tr><td>org_i02:</td><td><a href="org_i4.addOrg">New organisation</a></td><td><i>A normal (child) organisation owned by a parent</i></td></tr>'||chr(13);
	v_html := v_html||'<tr><td>org_i03:</td><td><a href="use_i4.addUser">New login/contactperson</a></td><td><i>Create contactperson and login for a new organisation</i></td></tr>'||chr(13);
	v_html := v_html||'<tr><td colspan="3">&nbsp;</td></tr>'||chr(13);
	v_html := v_html||'<tr><td colspan="3"><b>Update:</b></td></tr>'||chr(13);
	v_html := v_html||'<tr><td>org_u01:</td><td><a href="org_i4.updateOrgStatus">Update org status</a></td><td><i>open, member or closed</i></td></tr>'||chr(13);
	v_html := v_html||'<tr><td>org_u02:</td><td><a href="org_i4.updateOrgService">Update org service</a></td><td><i>what services an org is on</i></td></tr>'||chr(13);
	v_html := v_html||'<tr><td>org_u03:</td><td><a href="org_i4.updateImplock">Update IMP lock</a></td><td><i>Lock an org from beeing updating when importing</i></td></tr>'||chr(13);
	v_html := v_html||'<tr><td>org_u04:</td><td><a href="org_i4.updateOrg" target="_blank">Update organisation</a></td><td><i>Update an organisation using kAdmin</i></td></tr>'||chr(13);
	v_html := v_html||'<tr><td colspan="3">&nbsp;</td></tr>'||chr(13);
	v_html := v_html||'<tr><td colspan="3"><b>Delete:</b></td></tr>'||chr(13);
	v_html := v_html||'<tr><td>org_d01:</td><td><a href="org_i4.deleteOrg">Delete organisation</a></td><td><i>Delete an organisation</i></td></tr>'||chr(13);
	v_html := v_html||'<tr><td>org_d02:</td><td><a href="use_i4.deleteUser">Delete login</a></td><td><i>Delete an organisation''s login/user</i></td></tr>'||chr(13);
	v_html := v_html||'<tr><td colspan="3">&nbsp;</td></tr>'||chr(13);
	v_html := v_html||'<tr><td colspan="3"><b>Miscellaneous:</b></td></tr>'||chr(13);
	v_html := v_html||'<tr><td>org_m01:</td><td><a href="org_i4.loginAsOrg">Login as organisation</a></td><td><i>Login as an organisation</i></td></tr>'||chr(13);
	v_html := v_html||'<tr><td>org_m02:</td><td><a href="org_i4.admChosenOrg">Frontpage organisation</a></td><td><i>What organisation to be the chosen one</i></td></tr>'||chr(13);
	v_html := v_html||'<tr><td>org_m03:</td><td><a href="org_i4.totalOrgAdm">Total Org Administration</a></td><td><i>Full org administration</i></td></tr>'||chr(13);
	v_html := v_html||'<tr><td>org_m04:</td><td><a href="org_i4.fixOrgNames">Fix org names</a></td><td><i>Go through and fix all organisation names</i></td></tr>'||chr(13);
--	v_html := v_html||'<tr><td>org_m02:</td><td><a href="">Email organisations</a></td><td><i>Send email to organisations</i></td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||''||htm.eBox||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end orgAdm;


---------------------------------------------------------
-- name:        memAdm
-- what:        admin of membership
-- author:      Frode Klevstul
-- start date:  29.12.2005
---------------------------------------------------------
procedure memAdm is begin
    ext_lob.pri(i4_lib.memAdm);
end;

function memAdm
	return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type 		:= 'memAdm';

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := v_html||''||htm.bBox('MEM')||chr(13);
	v_html := v_html||''||htm.bTable||chr(13);
	v_html := v_html||'<tr><td colspan="3"><b>Insert:</b></td></tr>'||chr(13);
	v_html := v_html||'<tr><td width="10%">org_i01:</td><td width="30%"><a href="">New membership</a></td><td><i>Add new membership</i></td></tr>'||chr(13);
	v_html := v_html||'<tr><td>org_i02:</td><td><a href="">Set org membership</a></td><td><i>Set organisation membership</i></td></tr>'||chr(13);
	v_html := v_html||'<tr><td colspan="3">&nbsp;</td></tr>'||chr(13);
	v_html := v_html||'<tr><td colspan="3"><b>Update:</b></td></tr>'||chr(13);
	v_html := v_html||'<tr><td>org_i03:</td><td><a href="">Update membership</a></td><td><i>name, price, duration</i></td></tr>'||chr(13);
	v_html := v_html||'<tr><td>org_i03:</td><td><a href="">Specify modules</a></td><td><i>Set access modules for a membership</i></td></tr>'||chr(13);
	v_html := v_html||'<tr><td colspan="3">&nbsp;</td></tr>'||chr(13);
	v_html := v_html||'<tr><td colspan="3"><b>Delete:</b></td></tr>'||chr(13);
	v_html := v_html||'<tr><td>org_d01:</td><td><a href="">Delete membership</a></td><td><i>Delete a membership</i></td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||''||htm.eBox||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end memAdm;


---------------------------------------------------------
-- name:        extAdm
-- what:        admin of extra stuff
-- author:      Frode Klevstul
-- start date:  29.12.2005
---------------------------------------------------------
procedure extAdm is begin
    ext_lob.pri(i4_lib.extAdm);
end;

function extAdm
	return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type 		:= 'extAdm';

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := v_html||''||htm.bBox('EXT')||chr(13);
	v_html := v_html||''||htm.bTable||chr(13);
	v_html := v_html||'<tr><td colspan="3"><b>Miscellaneous:</b></td></tr>'||chr(13);
	v_html := v_html||'<tr><td width="10%">ext_m01:</td><td width="30%"><a href="pag_pub.displayHiddenValues">View session values</a></td><td><i>Display hidden session values</i></td></tr>'||chr(13);
	v_html := v_html||'<tr><td>ext_m02:</td><td><a href="imp_i4.importOrg">Import organisation</a></td><td><i>Import org from IMP module</i></td></tr>'||chr(13);
	v_html := v_html||'<tr><td>ext_m03:</td><td><a href="i4_ext.cleanup">Clean DB</a></td><td><i>Clean database for data not needed</i></td></tr>'||chr(13);
	v_html := v_html||'<tr><td>ext_m04:</td><td><a href="/scripts/map/index.cgi" target="_blank">Geocode</a></td><td><i>Geocode addresses</i></td></tr>'||chr(13);
--	v_html := v_html||'<tr><td>ext_m03:</td><td width="30%"><a href="kAdmin.run" target="_blank">kAdmin</a></td><td><i>DB webinterface</i></td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||''||htm.eBox||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end extAdm;


---------------------------------------------------------
-- name:        texAdm
-- what:        admin of TEX module
-- author:      Frode Klevstul
-- start date:  14.03.2006
---------------------------------------------------------
procedure texAdm is begin
    ext_lob.pri(i4_lib.texAdm);
end;

function texAdm
	return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type 		:= 'texAdm';

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := v_html||''||htm.bBox('TEX')||chr(13);
	v_html := v_html||''||htm.bTable||chr(13);
	v_html := v_html||'<tr><td colspan="3"><b>Update:</b></td></tr>'||chr(13);
	v_html := v_html||'<tr><td width="10%">tex_u01:</td><td width="30%"><a href="tex_i4.updateText">Update text</a></td><td><i>Update text</i></td></tr>'||chr(13);
	v_html := v_html||'<tr><td width="10%">tex_u02:</td><td width="30%"><a href="tex_i4.setLanguage">Set language</a></td><td><i>Change display language / set debug mode</i></td></tr>'||chr(13);
	v_html := v_html||'<tr><td colspan="3">&nbsp;</td></tr>'||chr(13);
	v_html := v_html||'<tr><td colspan="3"><b>Miscellaneous:</b></td></tr>'||chr(13);
	v_html := v_html||'<tr><td>tex_m01:</td><td><a href="tex_i4.initialise">Initialise</a></td><td><i>Initialise text on all languages</i></td></tr>'||chr(13);
	v_html := v_html||'<tr><td>tex_m02:</td><td><a href="kAdmin.admin_table?p_action=list&amp;p_table_name=LOG_TEX&amp;p_sql_stmt=WHERE%20TEX_TEXT_FK_PK%20NOT%20IN%20(SELECT%20DISTINCT(TEX_TEXT_PK)%20FROM%20TEX_TEXT)">Missing translations</a></td><td><i>View missing translated text (delete from log_text using <a href="i4_ext.cleanup">"Clean DB"</a> some days before checking this)</i></td></tr>'||chr(13);
	v_html := v_html||'<tr><td>tex_m03:</td><td><a href="tex_i4.insertMissingText">Insert missing text</a></td><td><i>Insert text that are missing</i></td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||''||htm.eBox||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end texAdm;


-- ******************************************** --
end;
/
show errors;
