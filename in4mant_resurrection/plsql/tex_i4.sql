PROMPT *** tex_i4 ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package tex_i4 is
/* ******************************************************
*	Package:     tex_lib
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	060314 | Frode Klevstul
*			- Package created
****************************************************** */
	type parameter_arr 			is table of varchar2(4000) index by binary_integer;
	empty_array 				parameter_arr;

	procedure run;
	procedure updateText
	(
		  p_action						varchar2											default null
		, p_url							varchar2											default null
		, p_language_pk					tex_language.tex_language_pk%type					default null
		, p_text_pk						tex_text.tex_text_pk%type							default null
		, p_string						varchar2											default null
	);
	function updateText
	(
		  p_action						varchar2											default null
		, p_url							varchar2											default null
		, p_language_pk					tex_language.tex_language_pk%type					default null
		, p_text_pk						tex_text.tex_text_pk%type							default null
		, p_string						varchar2											default null
	) return clob;
	procedure initialise;
	function initialise
		return clob;
	procedure setLanguage
	(
		  p_action						varchar2											default null
		, p_language_pk					tex_language.tex_language_pk%type					default null
		, p_debug_mode					ses_session.debug_mode%type							default null
	);
	function setLanguage
	(
		  p_action						varchar2											default null
		, p_language_pk					tex_language.tex_language_pk%type					default null
		, p_debug_mode					ses_session.debug_mode%type							default null
	)	return clob;
	procedure insertMissingText
	(
		  p_action						varchar2											default null
		, p_language_pk					tex_language.tex_language_pk%type					default null
		, p_text_pk						tex_text.tex_text_pk%type							default null
		, p_string						varchar2											default null
	);
	function insertMissingText
	(
		  p_action						varchar2											default null
		, p_language_pk					tex_language.tex_language_pk%type					default null
		, p_text_pk						tex_text.tex_text_pk%type							default null
		, p_string						varchar2											default null
	) return clob;

end;
/
show errors;





-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body tex_i4 is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'tex_i4';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  14.03.2006
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        updateText
-- what:        update text
-- author:      Frode Klevstul
-- start date:  14.03.2006
---------------------------------------------------------
procedure updateText
(
	  p_action						varchar2											default null
	, p_url							varchar2											default null
	, p_language_pk					tex_language.tex_language_pk%type					default null
	, p_text_pk						tex_text.tex_text_pk%type							default null
	, p_string						varchar2											default null
) is begin
    ext_lob.pri(tex_i4.updateText(p_action, p_url, p_language_pk, p_text_pk, p_string));
end;

function updateText
(
	  p_action						varchar2											default null
	, p_url							varchar2											default null
	, p_language_pk					tex_language.tex_language_pk%type					default null
	, p_text_pk						tex_text.tex_text_pk%type							default null
	, p_string						varchar2											default null
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type 			:= 'updateText';

	c_id						constant ses_session.id%type 				:= ses_lib.getId;
	c_lang						constant tex_text.tex_text_pk%type 			:= ses_lib.getLang(c_id);

	cur_dynCursor				sys_refcursor;
	v_array						parameter_arr								default empty_array;
	v_url						add_address.url%type;

	v_sql_stmt					varchar2(1024);
	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_action='||p_action||'いp_language_pk='||p_language_pk||'いp_text_pk='||p_text_pk);
	ext_lob.ini(v_clob);

	use_lib.loggedInCheck(g_package, c_procedure, null);

	if (p_action is null) then
		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'i4admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundAdminBox')||chr(13);
		v_html := v_html||'<tr><td>'||chr(13);

		v_html := v_html||''||htm.bForm(g_package||'.'||c_procedure)||chr(13);
		v_html := v_html||'<input type="hidden" name="p_action" value="load">'||chr(13);
		v_html := v_html||''||htm.bBox('Load text on page')||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);

		v_html := v_html||'<tr><td colspan="2">&nbsp;</td></tr>'||chr(13);
		v_html := v_html||'<tr><td>URL / PCK:</td><td><input type="text" name="p_url" size="70"></td></tr>'||chr(13);
		v_html := v_html||'<tr><td>String:</td><td><input type="text" name="p_string" size="70"></td></tr>'||chr(13);
		v_html := v_html||'<tr><td>Language:</td><td>'||htm_lib.selectLanguage(c_lang)||'</td></tr>'||chr(13);

		v_html := v_html||'<tr><td colspan="2">&nbsp;</td></tr>'||chr(13);
		v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||htm.submitLink('load text')||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||htm.historyBack('go back')||'</td></tr>'||chr(13);

		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);
		v_html := v_html||''||htm.eForm||chr(13);

		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

	elsif (p_action = 'load') then

		v_url := p_url;
		if (v_url like '%owa/%') then
			v_url := substr(v_url, instr(v_url, 'owa/')+4, length(v_url));
		end if;

		v_sql_stmt := ' select '||
					' 		  tex_text_pk'||
					'		, string '||
					'	from tex_text'||
					'	where lower(tex_language_fk_pk) = lower('''||p_language_pk||''')'||
					'	and lower(tex_text_pk) like lower(''%'||v_url||'%'')'||
					'	and lower(string) like lower (''%'||p_string||'%'') ';

		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'i4admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundAdminBox')||chr(13);
		v_html := v_html||'<tr><td>'||chr(13);

		v_html := v_html||''||htm.bBox('Text on page '''||v_url||''' on language '''||p_language_pk||'''')||chr(13);

		ext_lob.add(v_clob, v_html);

		-- -------------------------
		-- initialize array
		-- -------------------------
		v_array(1) := '';
		v_array(2) := '';

		open cur_dynCursor for v_sql_stmt;
		loop
			fetch cur_dynCursor into v_array(1), v_array(2);
			exit when cur_dynCursor%notfound;
			v_html := v_html||''||htm.bForm(g_package||'.'||c_procedure, 'form_'||cur_dynCursor%rowcount)||chr(13);
			v_html := v_html||'<input type="hidden" name="p_action" value="update">'||chr(13);
			v_html := v_html||'<input type="hidden" name="p_text_pk" value="'||v_array(1)||'">'||chr(13);
			v_html := v_html||'<input type="hidden" name="p_language_pk" value="'||p_language_pk||'">'||chr(13);
			v_html := v_html||''||htm.bTable||chr(13);

			v_html := v_html||'
				<tr>
					<td width="15%">
						'||v_array(1)||'<br><br>
						'||htm.link('edit w/FCK', g_package||'.'||c_procedure||'?p_action=editSpecial&amp;p_text_pk='||v_array(1)||'&amp;p_language_pk='||p_language_pk,'_self', 'theButtonStyle')||'
					</td>
					<td>
 						<textarea name="p_string" rows="5" cols="50">'||v_array(2)||'</textarea>
					</td>
					<td>
						'||htm.submitLink('update', 'form_'||cur_dynCursor%rowcount)||'
					</td>
				</tr>
				<tr><td colspan="3"><hr noshade size="1"></td></tr>
				';

			v_html := v_html||''||htm.eTable||chr(13);
			v_html := v_html||''||htm.eForm||chr(13);

			ext_lob.add(v_clob, v_html);
		end loop;
		close cur_dynCursor;

		v_html := v_html||''||htm.eBox||chr(13);

		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

	elsif (p_action = 'update' and p_language_pk is not null and p_text_pk is not null and p_string is not null) then

		update	tex_text
		set		string 				= p_string
		where	tex_text_pk 		= p_text_pk
		and		tex_language_fk_pk 	= p_language_pk;

		commit;

		v_html := v_html||htm.jumpTo('tex_i4.updateText', 1, 'updating...');

	elsif (p_action = 'editSpecial') then

		select	string
		into	v_array(2)
		from	tex_text
		where	lower(tex_language_fk_pk) = lower(p_language_pk)
		and		lower(tex_text_pk) like lower(p_text_pk);


		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'i4admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);

		v_html := v_html||'<script type="text/javascript" src="/FCKeditor/fckeditor.js"></script>'||chr(13);
		v_html := v_html||''||htm.bBox('Edit '''||p_text_pk||''' on language '''||p_language_pk||'''')||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);
		v_html := v_html||''||htm.bForm(g_package||'.'||c_procedure)||chr(13);
		v_html := v_html||'<input type="hidden" name="p_action" value="update">'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_text_pk" value="'||p_text_pk||'">'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_language_pk" value="'||p_language_pk||'">'||chr(13);

		v_html := v_html||'
			<tr>
				<td>
					<b>'||p_text_pk||'</b>
				</td>
			</tr>
			<tr>
				<td>
					<script type="text/javascript">
					<!--
						var oFCKeditor = new FCKeditor( ''p_string'' ) ;
						oFCKeditor.BasePath = ''/FCKeditor/'' ;
						oFCKeditor.Width = 700 ;
						oFCKeditor.Height = 500 ;
						oFCKeditor.ToolbarSet = "i4_full" ;
						oFCKeditor.Value = '''||htm_lib.HTMLFix(v_array(2))||''' ;
						oFCKeditor.Create() ;
					//-->
					</script>
				</td>
			</tr>
			<tr>
				<td>
					'||htm.submitLink('update')||'
				</td>
			</tr>
			<tr><td colspan="3"><hr noshade size="1"></td></tr>
			';

		v_html := v_html||''||htm.eForm||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);

		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

	else

		v_html := v_html||htm.jumpTo('i4_pub.overview', 1, 'no insert made - going back to i4admin...');

	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
    when others then
    	sys_lib.exceptionHandle(sqlcode, sqlerrm);
        return null;
end;
end updateText;


---------------------------------------------------------
-- name:        initialise
-- what:        initialise text tables for all languages
-- author:      Frode Klevstul
-- start date:  14.03.2006
---------------------------------------------------------
procedure initialise is begin
	ext_lob.pri(tex_i4.initialise);
end;

function initialise
	return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type 			:= 'initialise';

	c_id						constant ses_session.id%type 				:= ses_lib.getId;
	c_lang						constant tex_text.tex_text_pk%type 			:= ses_lib.getLang(c_id);

	cursor	cur_language is
	select	tex_language_pk
	from	tex_language;

	cursor	cur_text is
	select	tex_text_pk
	from	tex_text;

	v_tmp						number;
	v_noInserts					number										:= 0;

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, null);
	ext_lob.ini(v_clob);

	use_lib.loggedInCheck(g_package, c_procedure, null);

	v_html := v_html||''||htm.bPage(g_package, c_procedure, 'i4admin')||chr(13);
	v_html := v_html||'<center>'||chr(13);
	v_html := v_html||''||htm.bTable('surroundAdminBox')||chr(13);
	v_html := v_html||'<tr><td>'||chr(13);

	v_html := v_html||''||htm.bBox('Initialising TEX module')||chr(13);
	v_html := v_html||''||htm.bTable||chr(13);

	for r_cursor in cur_text
	loop
		for r_cursor2 in cur_language
		loop

			select	count(*)
			into	v_tmp
			from	tex_text
			where	tex_text_pk			= r_cursor.tex_text_pk
			and		tex_language_fk_pk	= r_cursor2.tex_language_pk;

			if (v_tmp > 0) then
				--v_html := v_html||'<tr><td>'||r_cursor.tex_text_pk||'</td><td><b>OK</b></td></tr>';
				v_html := v_html;	-- do nothing
			else
				insert into tex_text
				(tex_text_pk, tex_language_fk_pk, string)
				values (r_cursor.tex_text_pk, r_cursor2.tex_language_pk, r_cursor.tex_text_pk);

				commit;

				v_noInserts := v_noInserts + 1;
				v_html := v_html||'<tr><td>'||r_cursor.tex_text_pk||'</td><td><b>INSERT</b> ('||r_cursor2.tex_language_pk||')</td></tr>'||chr(13);
			end if;

			ext_lob.add(v_clob, v_html);
		end loop;
	end loop;

	v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;"><br><br>A total of <b>'||v_noInserts||'</b> inserts were made.</td></tr>'||chr(13);
	v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||chr(13);
	v_html := v_html||htm.link('Back to i4admin', 'i4_pub.overview')||chr(13);
	v_html := v_html||'</td></tr>'||chr(13);

	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||''||htm.eBox||chr(13);

	v_html := v_html||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||'</center>'||chr(13);
	v_html := v_html||''||htm.ePage||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
    when others then
    	sys_lib.exceptionHandle(sqlcode, sqlerrm);
        return null;
end;
end initialise;


---------------------------------------------------------
-- name:        setLanguage
-- what:        change/set language
-- author:      Frode Klevstul
-- start date:  16.03.2006
---------------------------------------------------------
procedure setLanguage
(
	  p_action						varchar2											default null
	, p_language_pk					tex_language.tex_language_pk%type					default null
	, p_debug_mode					ses_session.debug_mode%type							default null
) is begin
    ext_lob.pri(tex_i4.setLanguage(p_action, p_language_pk, p_debug_mode));
end;

function setLanguage
(
	  p_action						varchar2											default null
	, p_language_pk					tex_language.tex_language_pk%type					default null
	, p_debug_mode					ses_session.debug_mode%type							default null
)	return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type 			:= 'setLanguage';

	c_id						constant ses_session.id%type 				:= ses_lib.getId;
	c_lang						constant tex_text.tex_text_pk%type 			:= ses_lib.getLang(c_id);

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_action='||p_action||'いp_language_pk='||p_language_pk||'いp_debug_mode='||p_debug_mode);
	ext_lob.ini(v_clob);

	use_lib.loggedInCheck(g_package, c_procedure, null);

	if (p_action is null) then

		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'i4admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundAdminBox')||chr(13);
		v_html := v_html||'<tr><td>'||chr(13);

		v_html := v_html||''||htm.bBox('Update language')||chr(13);
		v_html := v_html||''||htm.bForm(g_package||'.'||c_procedure)||chr(13);
		v_html := v_html||'<input type="hidden" name="p_action" value="update">'||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);

		v_html := v_html||'<tr><td>Language:</td><td>'||htm_lib.selectLanguage(c_lang)||'</td></tr>'||chr(13);
		v_html := v_html||'
			<tr>
				<td>Debug mode:</td>
				<td>'||chr(13);

		if (ses_lib.getDebugMode(c_id) > 0) then
			v_html := v_html ||'<input type="checkbox" name="p_debug_mode" value="1" checked>'||chr(13);
		else
			v_html := v_html ||'<input type="checkbox" name="p_debug_mode" value="1">'||chr(13);
		end if;

		v_html := v_html||'
				</td>
			</tr>'||chr(13);
		v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||htm.submitLink('set language')||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||htm.historyBack('go back')||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||chr(13);
		v_html := v_html||htm.link('Back to i4admin', 'i4_pub.overview','_self', 'theButtonStyle')||chr(13);
		v_html := v_html||'</td></tr>'||chr(13);

		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eForm||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);

		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

	elsif (p_action ='update' and p_language_pk is not null) then

		update	ses_session
		set		  tex_language_fk 	= p_language_pk
				, debug_mode 		= p_debug_mode
		where	id = c_id;

		commit;

		v_html := v_html||htm.jumpTo('i4_pub.overview', 1, 'changing language...');

	else

		v_html := v_html||htm.jumpTo('i4_pub.overview', 1, 'no change made, going back...');

	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
    when others then
    	sys_lib.exceptionHandle(sqlcode, sqlerrm);
        return null;
end;
end setLanguage;


---------------------------------------------------------
-- name:        insertMissingText
-- what:        make it possible to insert missing text
-- author:      Frode Klevstul
-- start date:  06.11.2006
---------------------------------------------------------
procedure insertMissingText
(
	  p_action						varchar2											default null
	, p_language_pk					tex_language.tex_language_pk%type					default null
	, p_text_pk						tex_text.tex_text_pk%type							default null
	, p_string						varchar2											default null
) is begin
    ext_lob.pri(tex_i4.insertMissingText(p_action, p_language_pk, p_text_pk, p_string));
end;

function insertMissingText
(
	  p_action						varchar2											default null
	, p_language_pk					tex_language.tex_language_pk%type					default null
	, p_text_pk						tex_text.tex_text_pk%type							default null
	, p_string						varchar2											default null
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type 			:= 'insertMissingText';

	c_id						constant ses_session.id%type 				:= ses_lib.getId;
	c_lang						constant tex_text.tex_text_pk%type 			:= ses_lib.getLang(c_id);

	v_i							number										:= 0;

	cursor	cur_log_tex is
	select	tex_text_fk_pk, date_accessed
	from	log_tex
	where	tex_text_fk_pk not in (select distinct(tex_text_pk) from tex_text)
	order by tex_text_fk_pk asc;

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_action='||p_action||'いp_language_pk='||p_language_pk||'いp_text_pk='||p_text_pk);
	ext_lob.ini(v_clob);

	use_lib.loggedInCheck(g_package, c_procedure, null);

	if (p_action is null) then
		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'i4admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundAdminBox')||chr(13);
		v_html := v_html||'<tr><td>'||chr(13);
		v_html := v_html||''||htm.bBox('Select string to insert')||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);
		v_html := v_html||'
				<td><b>p_text_pk</b></td>
				<td><b>date_accessed</b></td>
			</tr>'||chr(13);

		for r_cursor in cur_log_tex
		loop
			if ( mod(v_i, 2) = 0 ) then
				v_html := v_html||'<tr>'||chr(13);
			else
				v_html := v_html||'<tr class="alternativeLine">'||chr(13);
			end if;

			v_html := v_html||'
					<td>'||htm.link(r_cursor.tex_text_fk_pk, g_package||'.'||c_procedure||'?p_action=insert&p_text_pk='||r_cursor.tex_text_fk_pk)||'</td>
					<td>'||r_cursor.date_accessed||'</td>
				</tr>'||chr(13);
				ext_lob.add(v_clob, v_html);

			v_i := v_i + 1;
		end loop;

		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);
		v_html := v_html||''||htm.eForm||chr(13);

		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

	elsif (p_action = 'insert') then

		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'i4admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundAdminBox')||chr(13);
		v_html := v_html||'<tr><td>'||chr(13);

		v_html := v_html||''||htm.bForm(g_package||'.'||c_procedure)||chr(13);
		v_html := v_html||'<input type="hidden" name="p_action" value="insert_action">'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_text_pk" value="'||p_text_pk||'">'||chr(13);
		v_html := v_html||''||htm.bBox('Prepare string and insert')||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);

		v_html := v_html||'<tr><td colspan="2">&nbsp;</td></tr>'||chr(13);
		v_html := v_html||'<tr><td>String PK:</td><td>'||p_text_pk||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td>Language:</td><td>'||htm_lib.selectLanguage(c_lang)||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td>String:</td><td><input type="text" name="p_string" size="70"></td></tr>'||chr(13);

		v_html := v_html||'<tr><td colspan="2">&nbsp;</td></tr>'||chr(13);
		v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||htm.submitLink('insert text')||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||htm.historyBack('go back')||'</td></tr>'||chr(13);

		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);
		v_html := v_html||''||htm.eForm||chr(13);

		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

	elsif (p_action = 'insert_action' and p_language_pk is not null and p_text_pk is not null and p_string is not null) then

		insert into tex_text
		(tex_text_pk, tex_language_fk_pk, string)
		values (p_text_pk, p_language_pk, p_string);

		commit;

		v_html := v_html||htm.jumpTo(g_package||'.'||c_procedure, 1, 'inserting...');

	else

		v_html := v_html||htm.jumpTo('i4_pub.overview', 1, 'no insert made - going back to i4admin...');

	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
    when others then
    	sys_lib.exceptionHandle(sqlcode, sqlerrm);
        return null;
end;
end insertMissingText;


-- ******************************************** --
end;
/
show errors;
