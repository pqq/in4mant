PROMPT *** sys_lib ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package sys_lib is
/* ******************************************************
*	Package:     sys_lib
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	050723 | Frode Klevstul
*			- Package created
****************************************************** */
	procedure run;
	procedure debug
	(
		  p_package				in		mod_package.name%type
		, p_procedure			in		mod_procedure.name%type
		, p_txt					in		varchar2
	);
	function getParameter
	(
		p_name				in	sys_parameter.name%type
	) return sys_parameter.value%type;
	procedure writeToFile
	(
		  p_type				in varchar2
		, p_max_width			in number								default null
		, p_max_height			in number								default null
		, p_max_file_size		in number
		, p_url_success			in varchar2
		, p_url_failure			in varchar2
		, p_crop				in number								default 0
		, p_watermark			in number								default 0
	);
	procedure manipulateImage
	(
		  p_log_upload_pk		in log_upload.log_upload_pk%type		default null
		, p_max_width			in number								default 10
		, p_max_height			in number								default 10
		, p_max_file_size		in number								default null
		, p_crop				in number								default 0
		, p_watermark			in number								default 0
	);
	procedure exceptionHandle
	(
		  p_errCode			in number
		, p_errMsg			in varchar2
	);
	function getEmptyPk
	(
		  p_table			varchar2
	) return number;

end;
/
show errors;





-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body sys_lib is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'sys_lib';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  23.07.2005
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        debug
-- what:        writes out debug information for testing
-- author:      Frode Klevstul
-- start date:  23.07.2005
---------------------------------------------------------
procedure debug
(
	  p_package				in		mod_package.name%type
	, p_procedure			in		mod_procedure.name%type
	, p_txt					in		varchar2
)
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'debug';
	c_debug_mode	constant sys_parameter.value%type := sys_lib.getParameter('debug-mode');
	c_debug			constant sys_parameter.value%type := sys_lib.getParameter('debug');
begin
	ses_lib.ini(g_package, c_procedure);

	if (c_debug='1') then
		if (c_debug_mode = 'web' or c_debug_mode = 'all') then
			htp.p('<font color="red"><b>DEBUG:</font> '||p_txt||'</b><br>');
		end if;
		if (c_debug_mode = 'db' or c_debug_mode = 'all') then
			log_lib.logDebug(p_txt);
		end if;
	end if;

end;
end debug;


---------------------------------------------------------
-- name:        getParameter
-- what:        returns a parameters value
-- author:      Frode Klevstul
-- start date:  23.07.2005
---------------------------------------------------------
function getParameter
(
	p_name				in	sys_parameter.name%type
) return sys_parameter.value%type
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'getParameter';

	v_value		sys_parameter.value%type;

	cursor cur_sys_parameter
	(
		b_name		sys_parameter.name%type
	) is
	select 	value
	from	sys_parameter
	where	name = b_name;
begin
	ses_lib.ini(g_package, c_procedure);

	for r_cursor in cur_sys_parameter(p_name)
	loop
	    v_value := r_cursor.value;
	end loop;

	return v_value;
end;
end getParameter;


---------------------------------------------------------
-- name:        writeToFile
-- what:        writes all unwritten data in log_upload to OS
-- author:      Frode Klevstul
-- start date:  07.11.2005
---------------------------------------------------------
procedure writeToFile
(
	  p_type				in varchar2
	, p_max_width			in number								default null
	, p_max_height			in number								default null
	, p_max_file_size		in number
	, p_url_success			in varchar2
	, p_url_failure			in varchar2
	, p_crop				in number								default 0
	, p_watermark			in number								default 0
)
is
begin
declare
	c_procedure				constant mod_procedure.name%type 	:= 'writeToFile';

	c_text_writingToFile	constant tex_text.string%type		:= tex_lib.get(g_package, c_procedure, 'writingToFile');

	c_upl_pic_formats		constant sys_parameter.value%type 	:= sys_lib.getParameter('upl_pic_formats');
	c_login_package			constant sys_parameter.value%type 	:= sys_lib.getParameter('login_package');

	v_lob_loc				blob;
	v_lob_len				number;
	v_buffer				raw(32767);
	v_buffer_size			binary_integer						:= 32767;
	v_offset				number								:= 1;
	v_out_file				utl_file.file_type;
	v_filetype				varchar2(32);
	v_message				log_upload.message%type;

	cursor	cur_log_upload is
	select 	log_upload_pk, content, src_filename, trg_filename, trg_directory
	from	log_upload
	where	bool_written	is null
	and		(src_filename	is not null or src_filename != '')
	and		trg_filename	is not null
	and		trg_directory	is not null
	and		content			is not null
	order by log_upload_pk asc;

begin
	ses_lib.ini(g_package, c_procedure);

	-- this line prevents anyone not logged in writing file to OS
	use_lib.loggedInCheck(p_url => c_login_package);

	-- ---------------------------------------------------
	-- step 1: check that image is of right type
	-- ---------------------------------------------------
	if (p_type = 'image') then
		for r_cursor in cur_log_upload
		loop
			-- --------------------------------------------------
			-- check that the image is of a valid file type
			-- --------------------------------------------------
			-- get filetype
			select	regexp_substr(r_cursor.src_filename, '[^.]*$')
			into	v_filetype
			from	dual;

			-- check if the filetype is valid / legal
			if (not owa_pattern.match(lower(c_upl_pic_formats), lower(v_filetype))) then
				update	log_upload
				set		bool_written = 0
					  , message = 'ERROR: wrong file type: '||v_filetype
				where	log_upload_pk	= r_cursor.log_upload_pk;
				commit;

				htm.jumpTo(p_url_failure, 1, null);

				return;
			end if;
		end loop;
	end if;

	-- ---------------------------------------------------
	-- step 2: write valid file to OS
	-- ---------------------------------------------------
	for r_cursor in cur_log_upload
	loop
		v_lob_loc := r_cursor.content;
		v_lob_len := dbms_lob.getlength(v_lob_loc);

		-- ------------------------------------------------------
		-- if file size is bigger than wanted and we're not
		-- uploading an image (images will reduced in size when
		-- we manipulate them later on) we don't write the file
		-- to the operating system
		-- ------------------------------------------------------
		if (v_lob_len > p_max_file_size and p_type <> 'image') then

			update	log_upload
			set		bool_written = 0
				  , message = 'ERROR: file size too large (actual/max):'||
				  			  ' size='||v_lob_len||'/'||p_max_file_size
			where	log_upload_pk	= r_cursor.log_upload_pk;
			commit;

			htm.jumpTo(p_url_failure, 1, null);

			return;

		-- -----------------------------------------------------
		-- if the fileszise is 0 we don't write anything
		-- -----------------------------------------------------
		elsif (v_lob_len = 0) then

			update	log_upload
			set		bool_written = 0
				  , message = 'ERROR: filesize is 0'
			where	log_upload_pk	= r_cursor.log_upload_pk;
			commit;

			htm.jumpTo(p_url_failure, 1, null);

			return;

		end if;

		-- ------------------------------------------------------
		-- at this point file attributes are verified and we
		-- can write the file to the OS/disk
		-- ------------------------------------------------------
		--sys_lib.debug(g_package, c_procedure, 'utl_file.fopen(location=> '||r_cursor.trg_directory||', filename=> '||r_cursor.trg_filename||')');
		v_out_file := utl_file.fopen(
							  location      => r_cursor.trg_directory
							, filename      => r_cursor.trg_filename
							, open_mode     => 'wb'
							, max_linesize  => 32767
						);

		dbms_lob.open(v_lob_loc, dbms_lob.lob_readonly);

		while (v_offset <= v_lob_len) loop
			dbms_lob.read(
					  lob_loc => v_lob_loc
					, amount  => v_buffer_size
					, offset  => v_offset
					, buffer  => v_buffer
			);

      		v_offset := v_offset + v_buffer_size;
			utl_file.put_raw(
				  file   => v_out_file
				, buffer => v_buffer
				, autoflush => true
			);
		end loop;

		utl_file.fclose(v_out_file);
		dbms_lob.close(v_lob_loc);

		if (p_type = 'image') then
			v_message := 'OK: image size='||v_lob_len;
		else
			v_message := 'OK: (actual/max): file size='||v_lob_len||'/'||p_max_file_size;
		end if;
		
		update	log_upload
		set		bool_written = 1
			  , message = v_message
		where	log_upload_pk	= r_cursor.log_upload_pk;
		commit;

		-- -----------------------------------------------------
		-- if we uploaded an image we have to manipulate it
		-- -----------------------------------------------------
		if (p_type = 'image') then
			sys_lib.manipulateImage(r_cursor.log_upload_pk, p_max_width, p_max_height, p_max_file_size, p_crop, p_watermark);
		end if;

	end loop;

	htm.jumpTo(p_url_success, 1, c_text_writingToFile);

end;
end writeToFile;


---------------------------------------------------------
-- name:        manipulateImage
-- what:        manipulate image
-- author:      Frode Klevstul
-- start date:  08.11.2005
---------------------------------------------------------
procedure manipulateImage
(
	  p_log_upload_pk		in log_upload.log_upload_pk%type		default null
	, p_max_width			in number								default 10
	, p_max_height			in number								default 10
	, p_max_file_size		in number								default null
	, p_crop				in number								default 0
	, p_watermark			in number								default 0
)
is
begin
declare
	c_procedure				constant mod_procedure.name%type 	:= 'manipulateImage';

	c_convert				constant sys_parameter.value%type 	:= sys_lib.getParameter('convert');
	c_composite				sys_parameter.value%type			:= replace(c_convert, 'convert', 'composite');
	c_identify				sys_parameter.value%type			:= replace(c_convert, 'convert', 'identify');
	c_dir_i4_imgbase		constant sys_parameter.value%type 	:= sys_lib.getParameter('dir_i4_imgbase');
	c_watermark				constant sys_parameter.value%type 	:= sys_lib.getParameter('watermark');

	e_stop					exception;

	v_id					ses_session.id%type;
	v_src_filename			log_upload.src_filename%type;
	v_trg_filename_org		log_upload.trg_filename%type;
	v_trg_filename_jpg		log_upload.trg_filename%type;
	v_trg_filename_new		log_upload.trg_filename%type;
	v_trg_filename_tmp		log_upload.trg_filename%type;
	v_trg_directory			log_upload.trg_directory%type;
	v_directory_path		all_directories.directory_path%type;

	v_img					blob;
	v_width					number;
	v_height				number;
	v_xCut					number;
	v_yCut					number;
	v_sysrun_res			varchar2(32767);
	v_info					varchar2(64);
	v_resizeNeeded			boolean								:= true;
	v_folderNo				number;
	v_bool_written			log_upload.bool_written%type;

begin
	ses_lib.ini(g_package, c_procedure);

	v_id := ses_lib.getId;

	-- -------------------------------------------------------------------------
	-- Note that you must use "FOR UPDATE" in the select statement or you
	-- will receive "ORA-22920: row containing the LOB value is not locked"
	-- when writing to the LOB.
	-- -------------------------------------------------------------------------
	select 	content, src_filename, trg_filename, trg_directory
	into	v_img, v_src_filename, v_trg_filename_org, v_trg_directory
	from	log_upload
	where	log_upload_pk = p_log_upload_pk
	for	update;

	select	directory_path
	into	v_directory_path
	from	all_directories
	where	directory_name = v_trg_directory;

	-- ----------------------------------------------------------------------
	-- corrupted files might be very small, we don't want to process them
	-- ----------------------------------------------------------------------
	if (length(v_img) < 10 or v_img is null) then
		update	log_upload
		set		bool_written	= 0
			  , message			= 'ERROR: file size too small ('||length(v_img)||')'
		where	log_upload_pk	= p_log_upload_pk;
		commit;

		return;
	end if;

	-- ----------------------------
	-- get image width and height
	-- ----------------------------
	select	sysrun(c_identify||' -format "%w %h " '||v_directory_path||'/'||v_trg_filename_org)
	into	v_sysrun_res
	from	dual;

	-- ------------------------------------------
	-- REGEXP_REPLACE(<source_string>, <pattern>, <replace_string>, <position>, <occurrence>, <match_parameter>)
	-- Reference: http://www.psoug.org/reference/regexp.html
	-- ------------------------------------------
	--htp.p('v_sysrun_res:<br />'||v_sysrun_res||'<br /><br />');
	v_width	:= regexp_replace(v_sysrun_res, '^([[:digit:]]{1,})[[:space:]]([[:digit:]]{1,})(.*)', '\1', 1, 1, 'n');
	v_height := regexp_replace(v_sysrun_res, '^([[:digit:]]{1,})[[:space:]]([[:digit:]]{1,})(.*)', '\2', 1, 1, 'n');
	--htp.p('v_width: '||v_width||'<br />');
	--htp.p('v_height: '||v_height||'<br />');

	if (v_width is null or v_height is null) then
		v_info			:= v_info||' ERROR: v_width or v_height is NULL';
		v_bool_written	:= 0;

	else
		v_bool_written	:= 2;

		-- change the filename to be of type jpg
		select regexp_replace(v_trg_filename_org,'\..*$','.jpg') into v_trg_filename_jpg from dual;
		v_trg_filename_new := v_trg_filename_org;
	
		-- -----------------------------------------------------------
		-- we don't have to manipulate the image if the size is OK
		-- (but we do add a watermark)
		-- -----------------------------------------------------------
		if (v_width <= p_max_width and v_height <= p_max_height and dbms_lob.getlength(v_img) <= p_max_file_size) then
			v_info := v_info||' sizeOK=1';
			v_resizeNeeded := false;
	
			-- ------------------------------------------------------------------------
			-- when we crop an image we generate thumbnails (at least normally)
			-- cropped images are asumed to be of type .jpg, so we have to convert
			-- the image before we return
			-- ------------------------------------------------------------------------
			if (p_crop = 1 and v_trg_filename_org <> v_trg_filename_jpg) then
				v_info := v_info||' thumbToJpg=1';
				v_trg_filename_new := v_trg_filename_jpg;
	
				select	sysrun(c_convert||' '||v_directory_path||'/'||v_trg_filename_org||' '||v_directory_path||'/'||v_trg_filename_jpg)
				into	v_sysrun_res
				from	dual;
			end if;
	
		else
			v_info := v_info||' sizeOK=0';
			v_trg_filename_new := v_trg_filename_jpg;
	
			-- if we want to crop and the width and height of the image is larger than the new / reduced size we crop it
			if (p_crop = 1 and (v_width > p_max_width and v_height > p_max_height)) then
				v_info := v_info||' crop=1';
		
				-- this is done to cut out the center of the image
				v_xCut := round(v_width/2 - p_max_width/2);
				v_yCut := round(v_height/3 - p_max_height/2);
		
				-- we have to avoid that v_yCut becomes less than 0, which might occure if the image is small
				if (v_yCut < 0) then
					v_yCut := 0;
				end if;
		
				select	sysrun(c_convert||' '||v_directory_path||'/'||v_trg_filename_org||' -size '||p_max_width||'x'||p_max_height||' -crop '||p_max_width||'x'||p_max_height||'+'||v_xCut||'+'||v_yCut||' +repage  '||v_directory_path||'/'||v_trg_filename_jpg)
				into	v_sysrun_res
				from	dual;
		
			else
				v_info := v_info||' crop=0';
		
				-- scale the image down
				select	sysrun(c_convert||' '||v_directory_path||'/'||v_trg_filename_org||' -size '||p_max_width||'x'||p_max_height||' -resize '||p_max_width||'x'||p_max_height||' '||v_directory_path||'/'||v_trg_filename_jpg)
				into	v_sysrun_res
				from	dual;
		
			end if;
	
		end if;
	
		-- -----------------------------------------------------------
		-- add watermark
		-- -----------------------------------------------------------
		if (p_watermark = 1) then
			v_info := v_info||' watermark=1';
	
			-- take backup of picture before we add a watermark
			select	sysrun('cp '||v_directory_path||'/'||v_trg_filename_new||' '||v_directory_path||'/backup/'||v_trg_filename_new)
			into	v_sysrun_res
			from	dual;
	
			select	sysrun(c_composite||' -gravity south-east -geometry +10+5 '||c_dir_i4_imgbase||'/'||c_watermark||' '||v_directory_path||'/'||v_trg_filename_new||' '||v_directory_path||'/'||v_trg_filename_new)
			into	v_sysrun_res
			from	dual;
		end if;
	
		-- --------------------------------------------------------------------------------
		-- Animated images:
		-- if for example an animated gif has been uploaded the result is
		-- a series of jpg, names filename.jpg.0, filename.jpg.1, filename.jpg.3 etc...
		-- --------------------------------------------------------------------------------
		-- use the first frame, frame 0
		select	sysrun('mv '||v_directory_path||'/'||v_trg_filename_jpg||'.0 '||v_directory_path||'/'||v_trg_filename_jpg)
		into	v_sysrun_res
		from	dual;
	
		-- remove the other frames
		select	sysrun('rm '||v_directory_path||'/'||v_trg_filename_jpg||'.*')
		into	v_sysrun_res
		from	dual;
	
		-- --------------------------------------------------------------------------------
		-- Delete original uploaded image, if this was another filetype than wanted
		-- if a resize wasn't needed (not v_resizeNeeded) we can't delete the original file
		-- since this will be used
		-- --------------------------------------------------------------------------------
		if (v_trg_filename_org <> v_trg_filename_jpg and v_resizeNeeded) then
			select	sysrun('rm '||v_directory_path||'/'||v_trg_filename_org)
			into	v_sysrun_res
			from	dual;
		end if;
	
		-- ----------------------------------------
		-- if directory is given in the trg_filename
		-- (on the format: "DIR_[dirno]_[filename]")
		-- we put the image in that directory
		-- ----------------------------------------
		if (owa_pattern.match(v_trg_filename_new, '^DIR')) then
			v_info := v_info||' dir=1';
			
			v_folderNo			:= regexp_replace(v_trg_filename_new, '^DIR([[:digit:]]{1,})_(.*)', '\1');
			v_trg_filename_tmp	:= regexp_replace(v_trg_filename_new, '^DIR([[:digit:]]{1,})_(.*)', '\2');
	
			select	sysrun('mv  '||v_directory_path||'/'||v_trg_filename_new||' '||v_directory_path||'/'||v_folderNo||'/'||v_trg_filename_tmp)
			into	v_sysrun_res
			from	dual;
	
			v_trg_filename_new := v_folderNo||'/'||v_trg_filename_tmp;
	
		else
			v_info := v_info||' subf=1';
	
			-- generate random number used as folder number
			select	round(dbms_random.value(1,100)) num
			into	v_folderNo
			from	dual;
	
			select	sysrun('mkdir '||v_directory_path||'/'||v_folderNo)
			into	v_sysrun_res
			from	dual;
		
			select	sysrun('mv  '||v_directory_path||'/'||v_trg_filename_new||' '||v_directory_path||'/'||v_folderNo||'/')
			into	v_sysrun_res
			from	dual;
	
			v_trg_filename_new := v_folderNo||'/'||v_trg_filename_new;
	
		end if;
	end if;

	-- ----------------------------------------
	-- update log upload with new values
	-- ----------------------------------------
	update	log_upload
	set		bool_written	= v_bool_written
		  , content			= v_img
		  , src_filename	= v_src_filename
		  , trg_filename	= nvl(v_trg_filename_new, trg_filename)
		  , message			= message||': manipulated '||v_info
	where	log_upload_pk	= p_log_upload_pk;
	commit;

end;
end manipulateImage;


---------------------------------------------------------
-- name:        exceptionHandle
-- what:        writes out error on screen (and hides traces to Oracle)
-- author:      Frode Klevstul
-- start date:  29.12.2005
---------------------------------------------------------
procedure exceptionHandle
(
	  p_errCode			number
	, p_errMsg			varchar2
)
is
begin
declare
	c_procedure			constant mod_procedure.name%type 	:= 'exceptionHandle';

	v_errMsg1			varchar2(1024) := p_errMsg;
	v_errMsg2			varchar2(1024) := dbms_utility.format_error_backtrace;
	v_errMsg3			varchar2(1024) := dbms_utility.format_error_stack;
begin
	ses_lib.ini(g_package, c_procedure);

	-- do not write out user-defined error messages with error code 1
	if (p_errCode != 1) then
		v_errMsg1	:= replace(v_errMsg1, 'ORA-', 'IN4-');
		v_errMsg2	:= replace(v_errMsg2, 'ORA-', 'IN4-');
		v_errMsg3	:= replace(v_errMsg3, 'ORA-', 'IN4-');

		v_errMsg1	:= replace(v_errMsg1, 'PL/SQL', 'IN4/SQL');

		htp.p('
			<center>
				<font color="red"><b>::: IN4MANT ERROR :::</b>
				<br /><br />
				'||v_errMsg1||'
				<br /><pre>'||v_errMsg2||'</pre>
				</font>
			</center>
		');
	end if;

end;
end exceptionHandle;


---------------------------------------------------------
-- name:        getEmptyPk
-- what:        returns first empty pk number bigger than 0
-- author:      Frode Klevstul
-- start date:  16.01.2006
---------------------------------------------------------
function getEmptyPk
(
	  p_table			varchar2
) return number
is
begin
declare
	c_procedure			constant mod_procedure.name%type 	:= 'getEmptyPk';

	v_pk				number;
	v_sql_stmt			varchar2(1024);
begin
	ses_lib.ini(g_package, c_procedure);

	v_sql_stmt := ' select nvl( min(l.'||p_table||'_pk + 1), 1) as empty_value'||
				'	from '||p_table||' l'||
				'	left outer join '||p_table||' r on l.'||p_table||'_pk + 1 = r.'||p_table||'_pk'||
				'	where l.'||p_table||'_pk > 0 '||
				'	and r.'||p_table||'_pk is null';

	execute immediate v_sql_stmt into v_pk;

	return v_pk;

end;
end getEmptyPk;


-- ******************************************** --
end;
/
show errors;
