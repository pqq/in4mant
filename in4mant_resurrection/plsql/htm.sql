PROMPT *** htm ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package htm is
/* ******************************************************
*	Package:     htm
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	060525 | Frode Klevstul
*			- Package created
****************************************************** */
	procedure run;
	procedure bPage
	(
		  p_package			in mod_package.name%type				default null
		, p_procedure		in mod_procedure.name%type				default null
		, p_type			in varchar2								default null
	);
	function bPage
	(
		  p_package			in mod_package.name%type				default null
		, p_procedure		in mod_procedure.name%type				default null
		, p_type			in varchar2								default null
	) return clob;
	procedure ePage;
	function ePage
		return clob;
	procedure bBox
	(
		  p_heading			in varchar2					default null
	);
	function bBox
	(
		  p_heading			in varchar2					default null
	) return clob;
	procedure eBox;
	function eBox
		return clob;
	procedure bTable
	(
		  p_class			in varchar2					default 'normalTable'
	);
	function bTable
	(
		  p_class			in varchar2					default 'normalTable'
	) return clob;
	procedure eTable;
	function eTable
		return clob;
	procedure bForm
	(
		  p_action			in varchar2
		, p_name			in varchar2		default 'form'
		, p_target			in varchar2		default '_top'
		, p_other			in varchar2		default null
	);
	function bForm
	(
		  p_action			in varchar2
		, p_name			in varchar2		default 'form'
		, p_target			in varchar2		default '_top'
		, p_other			in varchar2		default null
	) return clob;
	procedure eForm;
	function eForm
		return clob;
	procedure submitButton
	(
		p_value				in varchar2
	);
	function submitButton
	(
		p_value				in varchar2
	) return clob;
	procedure submitLink
	(
		  p_text			in varchar2
		, p_form			in varchar2				default 'form'
		, p_jsAction		in varchar2				default null
	);
	function submitLink
	(
		  p_text			in varchar2
		, p_form			in varchar2				default 'form'
		, p_jsAction		in varchar2				default null
	) return clob;
	procedure link
	(
		  p_text          	in varchar2
	    , p_href         	in varchar2
	    , p_target        	in varchar2     default '_self'
	    , p_class			in varchar2		default 'normalLink'
	);
	function link
	(
		  p_text          	in varchar2
	    , p_href         	in varchar2
	    , p_target        	in varchar2     default '_self'
	    , p_class			in varchar2		default 'normalLink'
	) return clob;
	procedure javascript;
	function javascript
		return clob;
	procedure jsFindRegion
	(
		p_name			in geo_geography.name%type			default null
	);
	function jsFindRegion
	(
		p_name			in geo_geography.name%type			default null
	) return clob;
	procedure css;
	function css
		return clob;
	procedure jumpTo
	(
		  p_url				in varchar2		default null
		, p_seconds			in number		default null
		, p_message			in varchar2		default null
		, p_fullPage		in boolean		default true
	);
	function jumpTo
	(
		  p_url				in varchar2		default null
		, p_seconds			in number		default null
		, p_message			in varchar2		default null
		, p_fullPage		in boolean		default true
	) return clob;
	procedure bHighlight
	(
		  p_url				in varchar2
		, p_on				in boolean
	);
	function bHighlight
	(
		  p_url				in varchar2
		, p_on				in boolean
	) return clob;
	procedure eHighlight;
	function eHighlight
		return clob;
	procedure historyBack (
		  p_text		in varchar2
	);
	function historyBack (
		  p_text		in varchar2
	) return clob;
	procedure popUp(
		  p_name			in varchar2		default null
		, p_url				in varchar2		default null
		, p_width			in varchar2		default 100
		, p_height			in varchar2		default 100
		, p_toolbar			in varchar2		default 0
		, p_scrollbars		in varchar2		default 0
		, p_mode			in varchar2		default 'link'
	);
	function popUp(
		  p_name			in varchar2		default null
		, p_url				in varchar2		default null
		, p_width			in varchar2		default 100
		, p_height			in varchar2		default 100
		, p_toolbar			in varchar2		default 0
		, p_scrollbars		in varchar2		default 0
		, p_mode			in varchar2		default 'link'
	) return clob;
	procedure printErrors(
		  p_error_messages	in varchar2		default null
	);
	function printErrors(
		  p_error_messages	in varchar2		default null
	) return clob;
	procedure selfClose
	(
		p_timeout		in number		default 0
	);
	function selfClose
	(
		p_timeout		in number		default 0
	) return clob;
end;
/
show errors;





-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body htm is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'htm';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  11.08.2005
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';

begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;

end;
end run;


---------------------------------------------------------
-- name:        bPage
-- what:        begins HTML code for a page
-- author:      Frode Klevstul
-- start date:  12.08.2005
---------------------------------------------------------
procedure bPage
(
	  p_package			in mod_package.name%type				default null
	, p_procedure		in mod_procedure.name%type				default null
	, p_type			in varchar2								default null
) is begin
	ext_lob.pri(htm.bPage(p_package, p_procedure, p_type));
end;

function bPage
(
	  p_package			in mod_package.name%type				default null
	, p_procedure		in mod_procedure.name%type				default null
	, p_type			in varchar2								default null
) return clob
is
begin
declare
	c_procedure				constant mod_procedure.name%type 		:= 'bPage';

	c_page_width			constant sys_parameter.value%type 		:= sys_lib.getParameter('page_width');
	c_img_i4				constant sys_parameter.value%type 		:= sys_lib.getParameter('img_i4');

	c_tex_orgnameSearch		constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'orgnameSearch');
	c_tex_advancedSearch	constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'advancedSearch');
	c_tex_search			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'search');
	c_tex_regionSearch		constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'regionSearch');
	c_tex_calendar			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'calendar');

	v_page_width			number									:= c_page_width - 10;
	v_simpleMode			boolean									default false;
	v_adminMode				boolean									default false;
	v_i4adminMode			boolean									default false;

	v_parameters			varchar2(1024)							:= owa_util.get_cgi_env('QUERY_STRING');
	v_html					clob;
	v_clob					clob;

begin
	ext_lob.ini(v_clob);
	
	if ( p_type = 'simple' ) then
		v_simpleMode := true;
	end if;

	if ( p_type = 'admin' ) then
		v_adminMode := true;
	end if;

	if ( p_type = 'i4admin' ) then
		v_i4adminMode := true;
	end if;

	-- --------------------------------------------------------------------------------------------------
	-- NOTE: if "v_simpleMode = true" we do NOT want to log the use of this package. Reason being is that
	-- 		 "bPage" with "v_simple = true" is used from "use_pub.login" (through "htm.jumpTo")
	--       That will cause an infinite loop if "ses_lib.ini" is called.
	-- --------------------------------------------------------------------------------------------------
	if (v_simpleMode = false) then
		ses_lib.ini(g_package, c_procedure);
	end if;

	v_html := '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
		<html>
			<head>
				<title>'||owa_util.get_cgi_env('SERVER_NAME')||'</title>
				<META HTTP-EQUIV="Pragma" CONTENT="text/html; charset=ISO-8859-1" />
				<META HTTP-EQUIV="Expires" CONTENT="0" />
				<META HTTP-EQUIV="Pragma" CONTENT="no-cache" />
				<META Name="description" Content="Uteliv.no - Norges utesteder p&aring; ett sted" />
				<META Name="keywords" Content="uteliv,ut,p&aring;,byen,bytur,kafe,konserter,pub,klubb,bar,dj,musikk,elektronica,puls,dans,disco,utested,techno,bilder,avis,fakta,nyheter,meny,natt,guide" />
				<link rel="shortcut icon" href="'||c_img_i4||'/favicon.ico" />
				<link rel="icon" href="'||c_img_i4||'/animated_favicon1.gif" type="image/gif" />
				'||htm.css||'
			</head>
			<body>
			'||htm.javascript||'
			';

	if not (v_simpleMode) then

		-- javascript code to store search value
		v_html := v_html||'
			<script language="JavaScript" type="text/javascript">

				function saveParameters(parameters){

					/* select out all values on the page */
					if(document.getElementById(''p_search_orgname'')){
						var p_search_orgname = document.getElementById(''p_search_orgname'');
					} else {
						var p_search_orgname = document.createElement(''tmp'');
						p_search_orgname.value = '''';
					}
					if(document.getElementById(''p_search_geo'')){
						var p_search_geo = document.getElementById(''p_search_geo'');
					} else {
						var p_search_geo = document.createElement(''tmp'');
						p_search_geo.value = '''';
					}

					/* build a parameter string */
					var parameterString = ''''
					+''p_search_orgname=''+p_search_orgname.value
					+''&p_search_geo=''+p_search_geo.value
					+'''';

					/* set the cookie using javascript */
					createCookie(''in4mant_search'', parameterString, 0.0005);
				}

				function loadParameters(){
					var parameters = readCookie(''in4mant_search'');

					if (parameters == null){
						return;
					}

					/* add all parameters in an array */
					var myarray 	= parameters.split(/&/);
					var mysubarray;

					/* go through the parameter array and get the values */
					for (var i = 0; i < myarray.length; i++) {
						mysubarray = myarray[i].split(/=/);

						if ( mysubarray[1].length > 0 ) {
							/* set form values */
							/*alert(mysubarray[0] +'':''+ mysubarray[1]); */

							if(document.getElementById(mysubarray[0])){
								document.getElementById(mysubarray[0]).value = mysubarray[1];
							}
						}
					}
				}

				function submitSearchForm(){
					saveParameters();
					document.searchForm.submit();
				}

				function submitSearchOnEnter ( e ) {
					var characterCode;
					if(e && e.which){
						 e = e;
						characterCode = e.which;
					} else {
						e = event;
						characterCode = e.keyCode;
					}
					if(characterCode == 13){
						submitSearchForm();
						return false;
					} else {
						return true;
					}
				}

			</script>
		';

	end if;

	v_html := v_html ||'
			<center>
			<table class="createBoxTable" width="'||c_page_width||'"><tr><td style="padding: 1px;">
	';

	if (v_adminMode) then
		v_html := v_html ||'<a href="pag_pub.frontPage"><img border="0" src="'||c_img_i4||'/topBanner_adm.jpg" alt="" /></a>'||chr(13);
	elsif (v_i4adminMode) then
		v_html := v_html ||'<a href="pag_pub.frontPage"><img border="0" src="'||c_img_i4||'/topBanner_i4.jpg" alt="" /></a>'||chr(13);
	else
		v_html := v_html ||'<a href="pag_pub.frontPage"><img border="0" src="'||c_img_i4||'/topBanner.jpg" alt="" /></a>'||chr(13);
		--v_html := v_html ||'<a href="pag_pub.jobsPage"><img border="0" src="'||c_img_i4||'/sale01.jpg" alt="" /></a><a href="pag_pub.frontPage"><img border="0" src="'||c_img_i4||'/topBanner_800.jpg" alt="" /></a>'||chr(13);
	end if;

	-- ---------------------------------------------------------------------
	-- if "p_simple" is true we only display a simple top
	-- ---------------------------------------------------------------------
	if (v_simpleMode = false) then

		-- -------------------------
		--
		-- top links (login / logout etc)
		--
		-- -------------------------
		v_html := v_html ||''|| use_pub.login(p_package||'.'||p_procedure||'?'||v_parameters, null, null)||chr(13);


		-- -------------------------
		--
		-- path field
		--
		-- -------------------------
		v_html := v_html ||''||htm_lib.pathField(p_package, p_procedure)||chr(13);

		v_html := v_html ||'<br /><br />'||chr(13);


		-- -------------------------
		--
		-- search field
		--
		-- -------------------------
		if (v_i4adminMode = false) then

			-- start search form
			v_html := v_html ||'
				'||htm.bForm('ext_sea.generateList', 'searchForm', '_top')||'
				<input type="hidden" name="p_type" value="orgAdvanced" />
			';

			-- ----------------------------
			-- start search table
			-- ----------------------------
			v_html := v_html||''||htm.bTable('searchForm')||'
				<tr>
					<td width="16%"><span class="searchBarHeading">'||c_tex_orgnameSearch||'</span></td>
			';

			-- -----------------
			-- search org name
			-- -----------------
			v_html := v_html ||'
				<td width="18%">
					<input type="hidden" name="p_name" value="name" />
					<input type="text" size="20" maxlength="50" name="p_value" id="p_search_orgname" onKeyPress="submitSearchOnEnter(event);" />
					&nbsp;'||htm.submitLink(c_tex_search, 'advancedSearch', 'submitSearchForm()')||'
					</td>
			';

			-- -------------------
			-- search geography (region)
			-- -------------------
			v_html := v_html||'
				<td width="10%" style="text-align: right;"><span class="searchBarHeading">'||c_tex_regionSearch||'</span></td>
				<td width="22%" style="padding-left: 10px; vertical-align: middle;">
				<input type="hidden" name="p_name" value="geography_name" />
				<input type="text" size="20" maxlength="50" name="p_value" id="p_search_geo"  onKeyPress="submitSearchOnEnter(event);" />
				&nbsp;'||htm.submitLink(c_tex_search, 'advancedSearch', 'submitSearchForm()')||'
				</td>
			';

			-- ---------------------------------------
			-- advanced search button
			-- ---------------------------------------
			v_html := v_html ||'<td width="15%" style="text-align: right; padding-right: 20px;">'||chr(13);
			v_html := v_html||'&nbsp;'||chr(13);
			--v_html := v_html||''||htm.link(c_tex_advancedSearch, 'ext_sea.advancedSearch', '_top', 'theButtonStyle')||chr(13);
			v_html := v_html ||'</td>'||chr(13);

			-- ------------------------
			-- calendar
			-- ------------------------
			v_html := v_html ||'<td style="text-align: right; padding-right: 35px;">'||chr(13);
			v_html := v_html||''||htm.link(c_tex_calendar, 'con_xca.show', '_top', 'theButtonStyle')||chr(13);
			v_html := v_html||'&nbsp;'||chr(13);
			v_html := v_html ||'</td>'||chr(13);

			-- ------------------------
			-- end of search field
			-- ------------------------
			v_html := v_html||''
							||' </tr>'
							||htm.eTable
							||htm.eForm
							||chr(13);
		end if;

		-- -------------------------
		-- load search parameters
		-- -------------------------
		v_html := v_html||'
			<script type="text/javascript">
			<!--
	      		loadParameters();
			//-->
			</script>'||chr(13);

		-- -------------------------
		-- closing top
		-- -------------------------
		v_html := v_html ||'<br /><br />'||chr(13);

	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end bPage;


---------------------------------------------------------
-- name:        ePage
-- what:        ends the HTML code for a page
-- author:      Frode Klevstul
-- start date:  12.08.2005
---------------------------------------------------------
procedure ePage is begin
	ext_lob.pri(htm.ePage);
end;

function ePage
	return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type 		:= 'ePage';
	c_page_width				constant sys_parameter.value%type 		:= sys_lib.getParameter('page_width');
    c_tex_advertise				constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'advertise');
    c_tex_partners				constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'partners');
    c_tex_jobs					constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'jobs');
    c_tex_contactUs				constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'contactUs');
    c_tex_aboutUs				constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'aboutUs');
    c_tex_emailThisPage			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'emailThisPage');
	c_img_i4					constant sys_parameter.value%type 		:= sys_lib.getParameter('img_i4');

	v_html						clob;
	v_clob						clob;
	v_SSL	varchar2(100)	:= owa_util.get_cgi_env('HTTPS');

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html :=
	'<br /><table width="100%" class="bottomTable"><tr><td style="text-align: center;">
	 |<a href="pag_pub.advertisePage">&nbsp;'||c_tex_advertise||'&nbsp;</a>
	 |<a href="pag_pub.partnerPage">&nbsp;'||c_tex_partners||'&nbsp;</a>
	 |<a href="pag_pub.jobsPage">&nbsp;'||c_tex_jobs||'&nbsp;</a>
	 |<a href="pag_pub.contactPage">&nbsp;'||c_tex_contactUs||'&nbsp;</a>
	 |<a href="pag_pub.aboutPage">&nbsp;'||c_tex_aboutUs||'&nbsp;</a>
	 |<!--<a href="pag_pub.sendLinkPage">&nbsp;'||c_tex_emailThisPage||'&nbsp;</a>|--><br />
	Copyright &copy; '||to_char(sysdate,'YYYY')||' Informant DA
	</td></tr></table> <!-- closing bottom table -->
	<br /><center><img src="'||c_img_i4||'/bottomImage.gif" alt="" /></center>
	</td></tr></table> <!-- closing box table (started in bPage) -->
	</center>'||chr(13);

	IF ( v_SSL = 'on' ) THEN
		v_html := v_html ||'<script src="https://ssl.google-analytics.com/urchin.js" type="text/javascript"></script>'||chr(13);
	ELSE
	       v_html := v_html ||'<script src="http://www.google-analytics.com/urchin.js" type="text/javascript"></script>'||chr(13);
	END IF;

	v_html := v_html ||'<script type="text/javascript">
	_uacct = "UA-396840-1";
	urchinTracker();
	</script>
	</body></html>';

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end ePage;


---------------------------------------------------------
-- name:        bBox
-- what:        begins HTML code for a box
-- author:      Frode Klevstul
-- start date:  11.08.2005
---------------------------------------------------------
procedure bBox
(
	  p_heading			in varchar2					default null
) is begin
	ext_lob.pri(htm.bBox(p_heading));
end;

function bBox
(
	  p_heading			in varchar2					default null
) return clob
is
begin
declare
	c_procedure		constant mod_procedure.name%type 	:= 'bBox';
	c_img_i4		constant sys_parameter.value%type 	:= sys_lib.getParameter('img_i4');

	v_html			clob;
	v_clob			clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := '
		<table class="box">
		<tr>
			<td class="boxHeading">
				'||p_heading||'
			</td>
		</tr>
		<tr>
			<td>
				<table class="boxTable">
					<tr>
						<td class="boxTable">';

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end bBox;


---------------------------------------------------------
-- name:        eBox
-- what:        ends HTML code for a box
-- author:      Frode Klevstul
-- start date:  11.08.2005
---------------------------------------------------------
procedure eBox is begin
	ext_lob.pri(htm.eBox);
end;

function eBox
	return clob
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'eBox';

	v_html			clob;
	v_clob			clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := '			</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>';

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end eBox;


---------------------------------------------------------
-- name:        bTable
-- what:        begins HTML code for a table
-- author:      Frode Klevstul
-- start date:  11.08.2005
---------------------------------------------------------
procedure bTable
(
	  p_class			in varchar2					default 'normalTable'
) is begin
	ext_lob.pri(htm.bTable(p_class));
end;

function bTable
(
	  p_class			in varchar2					default 'normalTable'
) return clob
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'bTable';

	v_html			clob;
	v_clob			clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := '<table class="'||p_class||'" border="0">'||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end bTable;


---------------------------------------------------------
-- name:        eTable
-- what:        ends HTML code for a table
-- author:      Frode Klevstul
-- start date:  11.08.2005
---------------------------------------------------------
procedure eTable is begin
	ext_lob.pri(htm.eTable);
end;

function eTable
	return clob
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'eTable';

	v_html			clob;
	v_clob			clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := '</table>'||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end eTable;


---------------------------------------------------------
-- name:        bForm
-- what:        begins HTML code for a form
-- author:      Frode Klevstul
-- start date:  11.08.2005
---------------------------------------------------------
procedure bForm
(
	  p_action			in varchar2
	, p_name			in varchar2		default 'form'
	, p_target			in varchar2		default '_top'
	, p_other			in varchar2		default null
) is begin
	ext_lob.pri(htm.bForm(p_action, p_name, p_target));
end;

function bForm
(
	  p_action			in varchar2
	, p_name			in varchar2		default 'form'
	, p_target			in varchar2		default '_top'
	, p_other			in varchar2		default null
) return clob
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'bForm';

	v_html			clob;
	v_clob			clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := '<form name="'||p_name||'" id="'||p_name||'" action="'||p_action||'" target="'||p_target||'" method="post" '||p_other||'>'||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end bForm;


---------------------------------------------------------
-- name:        eForm
-- what:        ends HTML code for a form
-- author:      Frode Klevstul
-- start date:  11.08.2005
---------------------------------------------------------
procedure eForm is begin
	ext_lob.pri(htm.eForm);
end;

function eForm
	return clob
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'eForm';
	v_html			clob;
	v_clob			clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := '</form>'||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end eForm;


---------------------------------------------------------
-- name:        submitButton
-- what:        HTML code for a submit button
-- author:      Frode Klevstul
-- start date:  12.08.2005
---------------------------------------------------------
procedure submitButton
(
	p_value		in varchar2
) is begin
	ext_lob.pri(htm.submitButton(p_value));
end;

function submitButton
(
	p_value		in varchar2
) return clob
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'submitButton';
	v_html			clob;
	v_clob			clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := '<input type="submit" value="'||p_value||'" class="theButtonStyle" />'||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end submitButton;


---------------------------------------------------------
-- name:        submitLink
-- what:        HTML code for a submit link for a form
-- author:      Frode Klevstul
-- start date:  12.08.2005
---------------------------------------------------------
procedure submitLink
(
	  p_text		in varchar2
	, p_form		in varchar2				default 'form'
	, p_jsAction	in varchar2				default null
) is begin
	ext_lob.pri(htm.submitLink(p_text, p_form, p_jsAction));
end;

function submitLink
(
	  p_text		in varchar2
	, p_form		in varchar2				default 'form'
	, p_jsAction	in varchar2				default null
) return clob
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'submitLink';
	v_html			clob;
	v_clob			clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	if (p_jsAction is null) then
		v_html := v_html||''||htm.link(p_text, 'javascript:document.'||p_form||'.submit();', '_self', 'theButtonStyle')||chr(13);
	else
		v_html := v_html||''||htm.link(p_text, 'javascript:'||p_jsAction||';', '_self', 'theButtonStyle')||chr(13);
	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end submitLink;


---------------------------------------------------------
-- name:        link
-- what:        HTML code for a href link
-- author:      Frode Klevstul
-- start date:  12.08.2005
---------------------------------------------------------
procedure link
(
	  p_text		in varchar2
    , p_href		in varchar2
    , p_target		in varchar2		default '_self'
    , p_class		in varchar2		default 'normalLink'
) is begin
	ext_lob.pri(htm.link(p_text, p_href, p_target));
end;

function link
(
	  p_text		in varchar2
	, p_href		in varchar2
	, p_target		in varchar2		default '_self'
    , p_class		in varchar2		default 'normalLink'
) return clob
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'link';
	v_html			clob;
	v_clob			clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	if (p_target = '_top') then
		v_html := '<a href="JavaScript:OpenLink('''|| p_href ||''')" class="'||p_class||'">'|| p_text ||'</a>'||chr(13);
	else
		v_html := '<a href="'||p_href||'" target="'||p_target||'" class="'||p_class||'">'|| p_text ||'</a>'||chr(13);
	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end link;


---------------------------------------------------------
-- name:        javascript
-- what:        all javascript code for a page
-- author:      Frode Klevstul
-- start date:  12.08.2005
---------------------------------------------------------
procedure javascript is begin
	ext_lob.pri(htm.javascript);
end;

function javascript
	return clob
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'javascript';

	c_dir_scripts	constant sys_parameter.value%type 	:= sys_lib.getParameter('dir_scripts');

	v_html			clob;
	v_clob			clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := '<script type="text/javascript" language="javascript" src="'||c_dir_scripts||'/commonScripts.js"></script>'||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end javascript;


---------------------------------------------------------
-- name:        jsFindRegion
-- what:        find region javascript
-- author:      Frode Klevstul
-- start date:  08.02.2006
---------------------------------------------------------
procedure jsFindRegion
(
	p_name			in geo_geography.name%type			default null
) is begin
	ext_lob.pri(htm.jsFindRegion(p_name));
end;

function jsFindRegion
(
	p_name			in geo_geography.name%type			default null
) return clob
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'jsFindRegion';

	c_dir_scripts	constant sys_parameter.value%type 	:= sys_lib.getParameter('dir_scripts');

	cursor	cur_geo_main is
	select	geo_geography_pk, name
	from	geo_geography
	where	parent_geo_geography_fk = (select geo_geography_pk from geo_geography where lower(name) = lower(p_name));

	cursor	cur_geo_sub
	(
		b_geo_geography_pk geo_geography.geo_geography_pk%type
	) is
	select	geo_geography_pk, name
	from	geo_geography
	where	parent_geo_geography_fk = b_geo_geography_pk;

	v_html			clob;
	v_clob			clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := v_html||'
		<script type="text/javascript" language="javascript">
		function start() {

			if (!typeArrayInitialized) {
				typeArrayInitialized = true;

				// Initialize Arrays Data for Type and Style
				TypeArray = new Array(
	';

	-- ------------------------
	-- type array (main level)
	-- ------------------------
	for r_cursor in cur_geo_main
	loop
		if (cur_geo_main%rowcount=1) then
			v_html := v_html||'new Type("'||r_cursor.geo_geography_pk||'", "'||r_cursor.name||'")'||chr(13);
		else
			v_html := v_html||', new Type("'||r_cursor.geo_geography_pk||'", "'||r_cursor.name||'")'||chr(13);
		end if;
	end loop;

	v_html := v_html||'
				);

				StyleArray = new Array(
	';

	-- ------------------------
	-- style array (sub level)
	-- ------------------------
	for r_cursor in cur_geo_main
	loop
		for r_cursor2 in cur_geo_sub(r_cursor.geo_geography_pk)
		loop
			if (cur_geo_main%rowcount=1 and cur_geo_sub%rowcount=1) then
				v_html := v_html||'new Style("'||r_cursor2.geo_geography_pk||'", "'||r_cursor.geo_geography_pk||'", "'||r_cursor2.name||'")'||chr(13);
			else
				v_html := v_html||', new Style("'||r_cursor2.geo_geography_pk||'", "'||r_cursor.geo_geography_pk||'", "'||r_cursor2.name||'")'||chr(13);
			end if;
		end loop;
	end loop;

	v_html := v_html||'
				);
			}

			if (document.advancedSearch) {
  				init(); // Default initialize comboboxes for Type and Style
			}
		}
		</script>
	';

	v_html := v_html||'<script type="text/javascript" language="javascript" src="'||c_dir_scripts||'/findRegions.js"></script>'||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end jsFindRegion;


---------------------------------------------------------
-- name:        css
-- what:        all cascading style sheet code
-- author:      Frode Klevstul
-- start date:  07.10.2005
---------------------------------------------------------
procedure css is begin
	ext_lob.pri(htm.css);
end;

function css
	return clob
is
begin
declare
	c_procedure		constant mod_procedure.name%type 	:= 'css';

	c_dir_scripts	constant sys_parameter.value%type 	:= sys_lib.getParameter('dir_scripts');

	v_html			clob;
	v_clob			clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := '<link href="'||c_dir_scripts||'/i4.css" rel="stylesheet" type="text/css" />'||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end css;


---------------------------------------------------------
-- name:        jumpTo
-- what:        HTML code to make a jump to another page
-- author:      Frode Klevstul
-- start date:  14.08.2005
---------------------------------------------------------
procedure jumpTo
(
	  p_url				in varchar2		default null
	, p_seconds			in number		default null
	, p_message			in varchar2		default null
	, p_fullPage		in boolean		default true
) is begin
	ext_lob.pri(htm.jumpTo(p_url, p_seconds, p_message, p_fullPage));
end;

function jumpTo
(
	  p_url				in varchar2		default null
	, p_seconds			in number		default null
	, p_message			in varchar2		default null
	, p_fullPage		in boolean		default true
) return clob
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'jumpTo';

	v_html			clob;
	v_clob			clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	if (p_fullPage) then
		v_html := v_html||''||htm.bPage(null, null, 'simple');
	end if;

    v_html := v_html||'
            <META HTTP-EQUIV="Refresh" CONTENT="'||p_seconds||'; URL='||p_url||'" />
            <!-- redirect user to address above --><br />
            <center><i>&nbsp;'||p_message||'&nbsp;</i></center><br />
    ';

	if (p_fullPage) then
		v_html := v_html||''||htm.ePage;
	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end jumpTo;


---------------------------------------------------------
-- name:        bHighlight
-- what:        HTML code to highlight something 
-- author:      Frode Klevstul
-- start date:  20.08.2005
---------------------------------------------------------
procedure bHighlight
(
	  p_url			in varchar2
	, p_on			in boolean
) is begin
	ext_lob.pri(htm.bHighlight(p_url, p_on));
end;

function bHighlight
(
	  p_url			in varchar2
	, p_on			in boolean
) return clob
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'bHighlight';

	v_html			clob;
	v_clob			clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	if (p_on) then
	    v_html := v_html||'<div class="highlight" onclick="JavaScript: location.href='''||p_url||'''" onMouseOver="highlight(this, ''#6699FF'')" onMouseOut="highlight(this, '''')">'||chr(13);
	else
	    v_html := v_html||'<div>'||chr(13);
	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end bHighlight;


---------------------------------------------------------
-- name:        eHighlight
-- what:        end HTML code for highlighting
-- author:      Frode Klevstul
-- start date:  20.08.2005
---------------------------------------------------------
procedure eHighlight is begin
	ext_lob.pri(htm.eHighlight);
end;

function eHighlight
	return clob
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'eHighlight';

	v_html			clob;
	v_clob			clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

    v_html := v_html||'</div>'||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end eHighlight;


---------------------------------------------------------
-- name:        historyBack
-- what:        HTML code for history back
-- author:      Frode Klevstul
-- start date:  25.10.2005
---------------------------------------------------------
procedure historyBack
(
	  p_text		in varchar2
) is begin
	ext_lob.pri(htm.historyBack(p_text));
end;

function historyBack (
	  p_text		in varchar2
) return clob
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'historyBack';

	v_html			clob;
	v_clob			clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := v_html||''||htm.submitLink(p_text, '', 'history.back()')||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end historyBack;


---------------------------------------------------------
-- name:        popUp
-- what:        HTML/Javascript code for popup window
-- author:      Frode Klevstul
-- start date:  31.10.2005
---------------------------------------------------------
procedure popUp
(
	  p_name			in varchar2		default null
	, p_url				in varchar2		default null
	, p_width			in varchar2		default 100
	, p_height			in varchar2		default 100
	, p_toolbar			in varchar2		default 0
	, p_scrollbars		in varchar2		default 0
	, p_mode			in varchar2		default 'link'
) is begin
	ext_lob.pri(htm.popUp(p_name, p_url, p_width, p_height, p_toolbar, p_scrollbars, p_mode));
end;

function popUp(
	  p_name			in varchar2		default null
	, p_url				in varchar2		default null
	, p_width			in varchar2		default 100
	, p_height			in varchar2		default 100
	, p_toolbar			in varchar2		default 0
	, p_scrollbars		in varchar2		default 0
	, p_mode			in varchar2		default 'link'
) return clob
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'popUp';

	v_html			clob;
	v_clob			clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	v_html := v_html||'
		<!-- popUp -->
		<SCRIPT type="text/javascript" language="JavaScript">
		// --------------------------------------------------------
		// function for popup
		// --------------------------------------------------------
		function openWin_'||p_name||'(url){
			var Win = window.open(
				  url
				, "window_'||''||to_char(sysdate, 'SSSSS')||'"
				, config = ''width = '||p_width||', height = '||p_height||', top = 100, left = 50, toolbar='||p_toolbar||', location = 0, directories = 0, status = 1, menubar = 0, scrollbars='||p_scrollbars||', copyhistory = 1, resizable = 1, alwaysRaised = yes''
			);

			Win.opener = this;
		}

		</SCRIPT>
		<!-- /popUp -->
	';

	-- ----------------------------------
	-- alternative popup code
	-- ----------------------------------
	--	function popUp_'||p_name||'(URL) {
	--		day = new Date();
	--		id = day.getTime();
	--		eval("page" + id + " = window.open(URL, ''" + id + "'', ''toolbar='||p_toolbar||',scrollbars='||p_scrollbars||',location=0,statusbar=1,menubar=0,resizable=1,width='||p_width||',height='||p_height||',left=100,top=100'');");
	--	}
	--
	--		v_html := v_html || '<a href="JavaScript:popUp_'||p_name||'('''||p_url||''')">'||p_name||'</a>';
	--
	-- 		v_html := v_html || '<script language="JavaScript">popUp_'||p_name||'('''||p_url||''')</script>';

	if (p_mode = 'link') then
		v_html := v_html || '<a href="JavaScript:openWin_'||p_name||'('''||p_url||''')">'||p_name||'</a>'||chr(13);
	-- -------------------------------------------
	-- NOTE: run mode is NOT a good idea since most new browsers block popups
	--       the code fails in FireFox as well.
	-- -------------------------------------------
	elsif (p_mode = 'run') then
		v_html := v_html || '<script type="text/javascript" language="JavaScript">openWin_'||p_name||'('''||p_url||''')</script>'||chr(13);
	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end popUp;


---------------------------------------------------------
-- name:        printErrors
-- what:        print error messages
-- author:      Frode Klevstul
-- start date:  30.11.2005
---------------------------------------------------------
procedure printErrors
(
	p_error_messages		in varchar2		default null
) is begin
	ext_lob.pri(htm.printErrors(p_error_messages));
end;

function printErrors
(
	  p_error_messages		in varchar2		default null
) return clob
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'printErrors';

	v_html			clob;
	v_clob			clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

	if (p_error_messages is not null) then
		v_html := v_html||''||htm.bTable||chr(13);

		v_html := v_html||''||'<tr><td bgcolor="#ff0000"><font color="white">'||chr(13);
		v_html := v_html||''||p_error_messages||chr(13);
		v_html := v_html||''||'</font></td></tr>'||chr(13);

		v_html := v_html||''||htm.eTable||chr(13);
	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end printErrors;


---------------------------------------------------------
-- name:        selfClose
-- what:        code to close window
-- author:      Frode Klevstul
-- start date:  28.03.2006
---------------------------------------------------------
procedure selfClose
(
	p_timeout		in number		default 0
) is begin
	ext_lob.pri(htm.selfClose(p_timeout));
end;

function selfClose
(
	p_timeout		in number		default 0
) return clob
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'selfClose';

	v_html			clob;
	v_clob			clob;

begin
	ses_lib.ini(g_package, c_procedure);
	ext_lob.ini(v_clob);

    v_html := v_html||'<script type="text/javascript" language="JavaScript">setTimeout(''window.close()'', '||p_timeout||')</script>'||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

end;
end selfClose;


-- ******************************************** --
end;
/
show errors;
