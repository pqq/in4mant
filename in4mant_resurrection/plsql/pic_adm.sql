PROMPT *** pic_adm ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package pic_adm is
/* ******************************************************
*	Package:     pic_adm
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	060704 | Frode Klevstul
*			- Package created
****************************************************** */

	procedure run;
	procedure editAlbums
	(
		  p_album_pk					pic_album.pic_album_pk%type						default null
		, p_title						pic_album.title%type							default null
		, p_subtitle					pic_album.subtitle%type							default null
		, p_bool_active					varchar2										default null
		, p_action						varchar2										default null
	);
	function editAlbums
	(
		  p_album_pk					pic_album.pic_album_pk%type						default null
		, p_title						pic_album.title%type							default null
		, p_subtitle					pic_album.subtitle%type							default null
		, p_bool_active					varchar2										default null
		, p_action						varchar2										default null
	) return clob;
	procedure editPictures
	(
		  p_album_pk					pic_album.pic_album_pk%type						default null
		, p_picture_pk					pic_picture.pic_picture_pk%type					default null
		, p_description					pic_picture.description%type					default null
		, p_bool_active					varchar2										default null
		, p_action						varchar2										default null
	);
	function editPictures
	(
		  p_album_pk					pic_album.pic_album_pk%type						default null
		, p_picture_pk					pic_picture.pic_picture_pk%type					default null
		, p_description					pic_picture.description%type					default null
		, p_bool_active					varchar2										default null
		, p_action						varchar2										default null
	) return clob;

end;
/
show errors;




-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body pic_adm is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'pic_adm';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  18.10.2005
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        editAlbums
-- what:        edit (add, delete, +++) albums
-- author:      Frode Klevstul
-- start date:  04.07.2006
---------------------------------------------------------
procedure editAlbums
(
	  p_album_pk					pic_album.pic_album_pk%type						default null
	, p_title						pic_album.title%type							default null
	, p_subtitle					pic_album.subtitle%type							default null
	, p_bool_active					varchar2										default null
	, p_action						varchar2										default null
) is begin
	ext_lob.pri(pic_adm.editAlbums(p_album_pk, p_title, p_subtitle, p_bool_active, p_action));
end;

function editAlbums
(
	  p_album_pk					pic_album.pic_album_pk%type						default null
	, p_title						pic_album.title%type							default null
	, p_subtitle					pic_album.subtitle%type							default null
	, p_bool_active					varchar2										default null
	, p_action						varchar2										default null
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type					:= 'editAlbums';

	c_id						constant ses_session.id%type 						:= ses_lib.getId;
	c_organisation_pk			constant org_organisation.org_organisation_pk%type	:= ses_lib.getOrgPk(c_id);

	c_tex_titleIsNull			constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'titleIsNull');
	c_text_delete				constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'delete');
	c_text_doNotDelete			constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'doNotDelete');
	c_text_editAlbums			constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'editAlbums');
	c_text_deactivate			constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'deactivate');
	c_text_activate				constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'activate');
	c_text_pics					constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'pics');
	c_text_edit					constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'edit');
	c_text_rename				constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'rename');
	c_text_deleteAlbum			constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'deleteAlbum');
	c_text_addNewAlbum			constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'addNewAlbum');
	c_text_title				constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'title');
	c_text_subtitle				constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'subtitle');
	c_text_addNew				constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'addNew');
	c_text_updateAlbum			constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'updateAlbum');
	c_text_update				constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'update');
	c_text_confirmDelete		constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'confirmDelete');
	c_text_confirmDeleteTxt		constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'confirmDeleteTxt');

	v_album_pk					pic_album.pic_album_pk%type;
	v_i							number												:= 0;
	v_deActiveLink				varchar2(64);
	v_title						pic_album.title%type;
	v_subtitle					pic_album.subtitle%type;
	v_buttonText				varchar2(64);
	v_boxHeadingText			varchar2(64);
	v_count						number;

	cursor	cur_pic_album is
	select	pic_album_pk, title, subtitle, bool_active
	from	pic_album
	where	org_organisation_fk = c_organisation_pk
	order by title;

	cursor	cur_pic_album2
	(b_album_pk pic_album.pic_album_pk%type) is
	select	title, subtitle
	from	pic_album
	where	pic_album_pk = b_album_pk;

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_album_pk='||p_album_pk);
	ext_lob.ini(v_clob);

	-- ------------------------------------
	-- check that org_user is logged in
	-- ------------------------------------
	use_lib.loggedInCheck(g_package, c_procedure, null);

	-- ---------------------------------
	-- check that org owns the album
	-- ---------------------------------
	if (p_album_pk is not null) then
		select	count(*)
		into	v_count
		from	pic_album
		where	pic_album_pk = p_album_pk
		and		org_organisation_fk = c_organisation_pk;
		if (v_count = 0) then
			v_html := v_html||htm.jumpTo('pag_pub.frontPage', 1, '...');
			return v_html;
		end if;
	end if;

	-- ------------------------------------
	-- show form
	-- ------------------------------------
	if (p_action is null or p_action = 'rename') then
		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundElements')||chr(13);
		v_html := v_html||'<tr><td class="surroundElements">'||chr(13);

		v_html := v_html||'
			<script language="JavaScript" type="text/javascript">

				function submitAddAlbumForm(){

					var	p_title		= document.getElementById(''p_title'');
					var	p_subtitle	= document.getElementById(''p_subtitle'');

					if (p_title.value.length == 0){
						alert("'||c_tex_titleIsNull||'");
						return;
					}

					document.add_album_form.submit();
				}

			</script>
		';

		-- -----------------------------------------------------------------
		-- don't show a list of all albums if renaming the album title
		-- -----------------------------------------------------------------
		if (p_action is null) then

			v_html := v_html||''||htm.bBox(c_text_editAlbums)||chr(13);
			v_html := v_html||''||htm.bTable||chr(13);

			for r_cursor in cur_pic_album
			loop
				if ( mod(v_i, 2) = 0 ) then
					v_html := v_html||'<tr>'||chr(13);
				else
					v_html := v_html||'<tr class="alternativeLine">'||chr(13);
				end if;

				if (pic_lib.isActive(p_album_pk => r_cursor.pic_album_pk)) then
					v_deActiveLink := c_text_deactivate;
				else
					v_deActiveLink := c_text_activate;
				end if;

				v_html := v_html||'
						<td><a href="'||g_package||'.editPictures?p_album_pk='||r_cursor.pic_album_pk||'">'||r_cursor.title||'</a></td>
						<td>'||pic_lib.noPictures(r_cursor.pic_album_pk)||' '||c_text_pics||'</td>
						<td width="10%" style="text-align: center;">'||htm.link(c_text_edit, g_package||'.editPictures?p_album_pk='||r_cursor.pic_album_pk, '_self', 'theButtonStyle')||'</td>
						<td width="10%" style="text-align: center;">'||htm.link(v_deActiveLink, g_package||'.'||c_procedure||'?p_action=de_activate&amp;p_album_pk='||r_cursor.pic_album_pk, '_self', 'theButtonStyle')||'</td>
						<td width="10%" style="text-align: center;">'||htm.link(c_text_rename, g_package||'.'||c_procedure||'?p_action=rename&amp;p_album_pk='||r_cursor.pic_album_pk, '_self', 'theButtonStyle')||'</td>
						<td width="10%" style="text-align: center;">'||htm.link(c_text_deleteAlbum, g_package||'.'||c_procedure||'?p_action=delete&amp;p_album_pk='||r_cursor.pic_album_pk, '_self', 'theButtonStyle')||'</td>
					</tr>'||chr(13);

				ext_lob.add(v_clob, v_html);

				v_i := v_i + 1;

			end loop;

			v_html := v_html||''||htm.eTable||chr(13);
			v_html := v_html||''||htm.eBox||chr(13);
		else
			v_html := v_html||'&nbsp;'||chr(13);
		end if;

		v_html := v_html||'</td></tr><tr><td class="surroundElements">'||chr(13);

		v_html := v_html||''||htm.bForm(g_package||'.'||c_procedure, 'add_album_form')||chr(13);
		if (p_action = 'rename') then
			v_buttonText		:= c_text_update;
			v_boxHeadingText	:= c_text_updateAlbum;
			v_html := v_html||'<input type="hidden" name="p_action" value="rename_action">'||chr(13);
			v_html := v_html||'<input type="hidden" name="p_album_pk" value="'||p_album_pk||'">'||chr(13);
			for r_cursor in cur_pic_album2(p_album_pk)
			loop
				v_title		:= r_cursor.title;
				v_subtitle	:= r_cursor.subtitle;
			end loop;
		else
			v_buttonText		:= c_text_addNew;
			v_boxHeadingText	:= c_text_addNewAlbum;
			v_html := v_html||'<input type="hidden" name="p_action" value="insert">'||chr(13);
		end if;
		v_html := v_html||''||htm.bBox(v_boxHeadingText)||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);
		v_html := v_html||'
			<tr>
				<td>'||c_text_title||'</td>
				<td><input type="text" name="p_title" id="p_title" maxlength="64" size="20" value="'||v_title||'"></td>
			</tr>
			<tr>
				<td>'||c_text_subtitle||'</td>
				<td><input type="text" name="p_subtitle" id="p_subtitle" maxlength="128" size="50" value="'||v_subtitle||'"></td>
			</tr>
		'||chr(13);
		v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||htm.submitLink(v_buttonText, 'add_album_form', 'submitAddAlbumForm()')||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);

		v_html := v_html||''||htm.eForm||chr(13);

		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

	-- ------------------------------------
	-- insert
	-- ------------------------------------
	elsif (p_action = 'insert' and p_title is not null) then

		v_album_pk := sys_lib.getEmptyPk('pic_album');

		insert into pic_album
		(pic_album_pk, org_organisation_fk, title, subtitle, bool_active, date_insert, date_update)
		values
		(
			  v_album_pk
			, c_organisation_pk
			, p_title
			, p_subtitle
			, 1
			, sysdate
			, sysdate
		);
		
		update	org_organisation
		set		date_update = sysdate
		where	org_organisation_pk = c_organisation_pk;
		
		commit;

		v_html := v_html||htm.jumpTo(g_package||'.'||c_procedure, 1, '...');

	-- ------------------------------------
	-- delete
	-- ------------------------------------
	elsif (p_action = 'delete' and p_album_pk is not null) then

		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundAdminBox')||'<tr><td>'||chr(13);

		v_html := v_html||''||htm.bBox(c_text_confirmDelete)||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);
		v_html := v_html||'
			<tr>
				<td>
					'||c_text_confirmDeleteTxt||'<br /><br />
					<i>'||pic_lib.getAlbumText(p_album_pk ,'all')||'</i>
					('||pic_lib.noPictures(p_album_pk)||' '||c_text_pics||')<br /><br />
				</td>
			</tr>'||chr(13);
		v_html := v_html||'<tr><td>'||chr(13);
		v_html := v_html||''||htm.link(c_text_delete, g_package||'.'||c_procedure||'?p_action=delete_action&amp;p_album_pk='||p_album_pk, '_self', 'theButtonStyle')||chr(13);
		v_html := v_html||'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'||chr(13);
		v_html := v_html||''||htm.link(c_text_doNotDelete, owa_util.get_cgi_env('http_referer'), '_self', 'theButtonStyle')||chr(13);
		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);

		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

	-- ------------------------------------
	-- delete_action
	-- ------------------------------------
	elsif (p_action = 'delete_action' and p_album_pk is not null) then

		-- delete pictures connected to this album
		delete from pic_album_picture
		where pic_album_fk_pk = p_album_pk;

		delete from pic_picture
		where pic_picture_pk not in
		(
			select	pic_picture_fk_pk
			from	pic_album_picture
		);

		delete from pic_album
		where pic_album_pk = p_album_pk;

		update	org_organisation
		set		date_update = sysdate
		where	org_organisation_pk = c_organisation_pk;

		commit;

		v_html := v_html||htm.jumpTo(g_package||'.'||c_procedure, 1, '...');

	-- ------------------------------------
	-- de_activate
	-- ------------------------------------
	elsif (p_action = 'de_activate' and p_album_pk is not null) then

		if (pic_lib.isActive(p_album_pk => p_album_pk)) then
			update	pic_album
			set		  bool_active = 0
					, date_update = sysdate
			where	pic_album_pk = p_album_pk;

			update	org_organisation
			set		date_update = sysdate
			where	org_organisation_pk = c_organisation_pk;
	
			commit;
		else
			update	pic_album
			set		  bool_active = 1
					, date_update = sysdate
			where	pic_album_pk = p_album_pk;

			update	org_organisation
			set		date_update = sysdate
			where	org_organisation_pk = c_organisation_pk;

			commit;
		end if;

		v_html := v_html||htm.jumpTo(g_package||'.'||c_procedure, 1, '...');

	-- ------------------------------------
	-- rename_action
	-- ------------------------------------
	elsif (p_action = 'rename_action' and p_album_pk is not null) then

		update	pic_album
		set		  title = p_title
				, subtitle = p_subtitle
				, date_update = sysdate
		where	pic_album_pk = p_album_pk;

		update	org_organisation
		set		date_update = sysdate
		where	org_organisation_pk = c_organisation_pk;

		commit;

		v_html := v_html||htm.jumpTo(g_package||'.'||c_procedure, 1, '...');

	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		return null;
end;
end editAlbums;


---------------------------------------------------------
-- name:        editAlbums
-- what:        edit (add, delete, +++) albums
-- author:      Frode Klevstul
-- start date:  04.07.2006
---------------------------------------------------------
procedure editPictures
(
	  p_album_pk					pic_album.pic_album_pk%type						default null
	, p_picture_pk					pic_picture.pic_picture_pk%type					default null
	, p_description					pic_picture.description%type					default null
	, p_bool_active					varchar2										default null
	, p_action						varchar2										default null
) is begin
	ext_lob.pri(pic_adm.editPictures(p_album_pk, p_picture_pk, p_description, p_bool_active, p_action));
end;

function editPictures
(
	  p_album_pk					pic_album.pic_album_pk%type						default null
	, p_picture_pk					pic_picture.pic_picture_pk%type					default null
	, p_description					pic_picture.description%type					default null
	, p_bool_active					varchar2										default null
	, p_action						varchar2										default null
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type					:= 'editPictures';

	c_id						constant ses_session.id%type 						:= ses_lib.getId;
	c_organisation_pk			constant org_organisation.org_organisation_pk%type	:= ses_lib.getOrgPk(c_id);
	c_directory_path			constant sys.all_directories.directory_path%type	:= org_lib.getDirectoryPath(c_organisation_pk, true);

	c_dir_img_org				constant sys_parameter.value%type 					:= sys_lib.getParameter('dir_img_org')||'_'||c_organisation_pk;
	c_max_pic_size				constant sys_parameter.value%type 					:= sys_lib.getParameter('max_pic_size');
	c_max_pic_width				constant sys_parameter.value%type 					:= sys_lib.getParameter('max_pic_width');
	c_max_pic_height			constant sys_parameter.value%type 					:= sys_lib.getParameter('max_pic_height');
	c_thumb_pic_width			constant sys_parameter.value%type 					:= sys_lib.getParameter('thumb_pic_width');
	c_thumb_pic_height			constant sys_parameter.value%type 					:= sys_lib.getParameter('thumb_pic_height');
	c_upl_pic_formats			constant sys_parameter.value%type 					:= sys_lib.getParameter('upl_pic_formats');

	c_text_emptyPath			constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'emptyPath');
	c_text_delete				constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'delete');
	c_text_doNotDelete			constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'doNotDelete');
	c_text_suppFileFormats		constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'suppFileFormats');
	c_text_albumIndex			constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'albumIndex');
	c_text_editPictures			constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'editPictures');
	c_text_updateText			constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'updateText');
	c_text_deactivate			constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'deactivate');
	c_text_activate				constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'activate');
	c_text_deletePic			constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'deletePic');
	c_text_addNewPicture		constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'addNewPicture');
	c_text_picture				constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'picture');
	c_text_upload				constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'upload');
	c_text_confirmDelete		constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'confirmDelete');
	c_text_confirmDeleteTxt		constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'confirmDeleteTxt');
	c_text_uploadFailed			constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'uploadFailed');

	v_picture_pk				pic_picture.pic_picture_pk%type;
	v_filename					varchar2(32);
	v_random					binary_integer										:= abs(dbms_random.random());
	v_i							number												:= 0;
	v_deActiveLink				varchar2(64);
	v_count						number;
	v_trg_filename				log_upload.trg_filename%type;
	v_description				pic_picture.description%type;

	cursor	cur_pic_picture is
	select	pic_picture_pk, description, path_filename, bool_active
	from	pic_picture
	where	pic_picture_pk in
	(
		select	pic_picture_fk_pk
		from	pic_album_picture
		where	pic_album_fk_pk = p_album_pk
	)
	order by date_insert asc;

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_album_pk='||p_album_pk||'��p_picture_pk='||p_picture_pk);
	ext_lob.ini(v_clob);

	-- ------------------------------------
	-- check that org_user is logged in
	-- ------------------------------------
	use_lib.loggedInCheck(g_package, c_procedure, null);

	-- ------------------------------------
	-- check that an album is chosen
	-- ------------------------------------
	if (p_album_pk is null) then
		v_html := v_html||htm.jumpTo(g_package||'.editAlbums', 3, '[NO_ALBUM_SELECTED]...');
		return v_html;
	end if;

	-- ---------------------------------
	-- check that org owns the album
	-- ---------------------------------
	if (p_album_pk is not null) then
		select	count(*)
		into	v_count
		from	pic_album
		where	pic_album_pk = p_album_pk
		and		org_organisation_fk = c_organisation_pk;
		if (v_count = 0) then
			v_html := v_html||htm.jumpTo('pag_pub.frontPage', 1, '...');
			return v_html;
		end if;
	end if;

	-- ---------------------------------
	-- check that org owns the picture
	-- ---------------------------------
	if (p_picture_pk is not null) then
		select	count(*)
		into	v_count
		from	pic_album, pic_album_picture
		where	pic_album_pk = pic_album_fk_pk
		and		org_organisation_fk = c_organisation_pk
		and		pic_picture_fk_pk = p_picture_pk;
		if (v_count = 0) then
			v_html := v_html||htm.jumpTo('pag_pub.frontPage', 1, '...');
			return v_html;
		end if;
	end if;

	-- ------------------------------------
	-- show form
	-- ------------------------------------
	if (p_action is null) then
		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundElements')||chr(13);
		v_html := v_html||'<tr><td class="surroundElements">'||chr(13);

		v_html := v_html||''||htm.bTable||chr(13);
		v_html := v_html||'<tr><td style="text-align: center;">'||htm.link(c_text_albumIndex, g_package||'.editAlbums', '_self', 'theButtonStyle')||'</td></tr>';
		v_html := v_html||''||htm.eTable||chr(13);

		v_html := v_html||'</td></tr><tr><td class="surroundElements">'||chr(13);

		v_html := v_html||'
			<script language="JavaScript" type="text/javascript">

				function submitAddPictureForm(){

					var	p_file_1	= document.getElementById(''p_file_1'');

					if (p_file_1.value.length == 0){
						alert("'||c_text_emptyPath||'");
						return;
					}

					document.upload_form.submit();
				}

			</script>
		';

		-- ----------------------
		-- add picture form
		-- ----------------------
		v_filename := c_organisation_pk||'_'||p_album_pk||'_'||v_random;
		v_html := v_html||''||htm.bForm('htm_lib.upload" enctype="multipart/form-data', 'upload_form')||chr(13);
		v_html := v_html||'<input type="hidden" name="p_type" value="image" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_url_success" value="'||g_package||'.'||c_procedure||'?p_action=insert&amp;p_album_pk='||p_album_pk||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_url_failure" value="'||g_package||'.'||c_procedure||'?p_action=failed&amp;p_album_pk='||p_album_pk||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_trg_directory" value="'||c_dir_img_org||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_max_file_size" value="'||c_max_pic_size||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_max_width" value="'||c_max_pic_width||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_max_height" value="'||c_max_pic_height||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_trg_filename" value="'||v_filename||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_watermark" value="1" />'||chr(13);

		v_html := v_html||''||htm.bBox(c_text_addNewPicture)||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);
		v_html := v_html||'<tr><td colspan="2">&nbsp;</td></tr>'||chr(13);
		v_html := v_html||'<tr><td colspan="2">'||c_text_suppFileFormats||' '||c_upl_pic_formats||'</td></tr>'||chr(13);
		v_html := v_html||'
			<tr>
				<td>'||c_text_picture||'</td>
				<td><input type="file" name="p_file_1" id="p_file_1" size="70"></td>
			</tr>
		'||chr(13);
		v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||htm.submitLink(c_text_upload, 'upload_form', 'submitAddPictureForm()')||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td colspan="2">&nbsp;</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);
		v_html := v_html||''||htm.eForm||chr(13);


		v_html := v_html||'</td></tr><tr><td class="surroundElements">'||chr(13);


		-- -------------------
		-- show pictures
		-- -------------------
		v_html := v_html||''||htm.bBox(c_text_editPictures||' : '||pic_lib.getAlbumText(p_album_pk, 'title'))||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);

		-- mini thumbs
		v_html := v_html||'<tr><td colspan="4">';
		ext_lob.add(v_clob, v_html);
		v_html := v_html||pic_lib.miniThumbs(c_organisation_pk, p_album_pk, null, false);
		v_html := v_html||'</td></tr>';
		v_html := v_html||'<tr><td colspan="4">&nbsp;</td></tr>';
		ext_lob.add(v_clob, v_html);

		for r_cursor in cur_pic_picture
		loop
			if (pic_lib.isActive(p_picture_pk => r_cursor.pic_picture_pk)) then
				v_deActiveLink := c_text_deactivate;
			else
				v_deActiveLink := c_text_activate;
			end if;

			if (v_i = 0) then
				v_html := v_html||'<tr>'||chr(13);
			elsif (mod(v_i, 2)=0) then
				v_html := v_html||'</tr><tr>'||chr(13);
			end if;

			v_html := v_html||'
				<td class="centerMiddle" width="200"><img class="picBorder" alt="'||r_cursor.description||'" title="'||r_cursor.description||'" src="'||c_directory_path||'/'||r_cursor.path_filename||'" width="250"></td>
				<td style="vertical-align: middle;">
					'||htm.bForm(g_package||'.'||c_procedure, 'update_pic_form_'||v_i)||'
						<input type="hidden" name="p_action" value="update_text">
						<input type="hidden" name="p_album_pk" value="'||p_album_pk||'">
						<input type="hidden" name="p_picture_pk" value="'||r_cursor.pic_picture_pk||'">
						<input type="text" name="p_description" value="'||r_cursor.description||'" maxlength="64" size="35"><br />
						'||htm.submitLink(c_text_updateText, 'update_pic_form_'||v_i)||'
					'||htm.eForm||'<br /><br /><br />
					'||htm.link(v_deActiveLink, g_package||'.'||c_procedure||'?p_action=de_activate&amp;p_picture_pk='||r_cursor.pic_picture_pk||'&amp;p_album_pk='||p_album_pk, '_self', 'theButtonStyle')||'<br /><br /><br /><br />
					'||htm.link(c_text_deletePic, g_package||'.'||c_procedure||'?p_action=delete&amp;p_picture_pk='||r_cursor.pic_picture_pk||'&amp;p_album_pk='||p_album_pk, '_self', 'theButtonStyle')||'
				</td>
				'||chr(13);
			v_i := v_i + 1;

			ext_lob.add(v_clob, v_html);
		end loop;

		if (v_i > 0) then
			v_html := v_html||'</tr>'||chr(13);
		end if;

		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);


		-- -------------
		-- end page
		-- -------------
		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

	-- ------------------------------------
	-- insert
	-- ------------------------------------
	elsif (p_action like 'insert') then

		v_picture_pk := sys_lib.getEmptyPk('pic_picture');

		insert into pic_picture
		(pic_picture_pk, path_filename, bool_active, date_insert, date_update)
		values
		(
			  v_picture_pk
			, (
				select	trg_filename as trg_filename
				from	log_upload
				where	log_upload_pk =
					(
						select	max(log_upload_pk)
						from	log_upload
						where	ses_session_id = c_id
					)
				)
			, 1
			, sysdate
			, sysdate
		);

		insert into pic_album_picture
		(pic_album_fk_pk, pic_picture_fk_pk)
		values
		(
			  p_album_pk
			, v_picture_pk
		);

		update	pic_album
		set		date_update = sysdate
		where	pic_album_pk = p_album_pk;

		update	org_organisation
		set		date_update = sysdate
		where	org_organisation_pk = c_organisation_pk;

		commit;

		v_html := v_html||htm.jumpTo(g_package||'.'||c_procedure||'?p_action=generate_thumb&amp;p_album_pk='||p_album_pk, 1, '...');

	-- ------------------------------------
	-- generate_thumb
	-- ------------------------------------
	elsif (p_action = 'generate_thumb') then

		-- ------------------------------------------------------------
		-- make a new insert in log_upload of the last file uploaded
		-- ------------------------------------------------------------
		select	trg_filename
		into	v_trg_filename
		from	log_upload
		where	log_upload_pk = 
		(
			select	max(log_upload_pk)
			from	log_upload
			where	ses_session_id = c_id
		);
		
		-- we want the thumb to end up in the same sub-directory as the main photo so we have to set
		-- DIR[dirno] so that sys_lib.manipulateImage will move the image into the right folder
		v_trg_filename	:= regexp_replace(v_trg_filename, '^([[:digit:]]{1,})([[:punct:]])(.*)', 'DIR\1_thumb_\3');

		insert into log_upload
		(log_upload_pk, content, src_filename, trg_filename, trg_directory, bool_written, ses_session_id, date_insert)
		values (
			  seq_log_upload.nextval
			, (select content from log_upload where log_upload_pk = (select max(log_upload_pk) from log_upload where ses_session_id = c_id))
			, (select src_filename from log_upload where log_upload_pk = (select max(log_upload_pk) from log_upload where ses_session_id = c_id))
			, v_trg_filename
			, (select trg_directory from log_upload where log_upload_pk = (select max(log_upload_pk) from log_upload where ses_session_id = c_id))
			, null
			, c_id
			, sysdate
			);
		commit;

		-- --------------------------------------
		-- call function to write file to OS
		-- --------------------------------------
		sys_lib.writeToFile(
			  'image'
			, to_number(c_thumb_pic_width)
			, to_number(c_thumb_pic_height)
			, 9999999
			, g_package||'.'||c_procedure||'?p_album_pk='||p_album_pk
			, g_package||'.'||c_procedure||'?p_album_pk='||p_album_pk||'&amp;p_action=failed_thumb'
			, 1			-- we want to crop the image if possible
		);


	-- ------------------------------------
	-- delete
	-- ------------------------------------
	elsif (p_action = 'delete' and p_album_pk is not null) then

		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundAdminBox')||'<tr><td>'||chr(13);

		v_html := v_html||''||htm.bBox(c_text_confirmDelete)||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);
		v_html := v_html||'<tr><td>'||c_text_confirmDeleteTxt||'<br /><br />'||pic_lib.showThumb(p_picture_pk, false)||'<br /><br /></td></tr>'||chr(13);
		v_html := v_html||'<tr><td>'||chr(13);
		v_html := v_html||''||htm.link(c_text_delete, g_package||'.'||c_procedure||'?p_action=delete_action&amp;p_picture_pk='||p_picture_pk||'&amp;p_album_pk='||p_album_pk, '_self', 'theButtonStyle')||chr(13);
		v_html := v_html||'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'||chr(13);
		v_html := v_html||''||htm.link(c_text_doNotDelete, owa_util.get_cgi_env('http_referer'), '_self', 'theButtonStyle')||chr(13);
		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);

		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

	-- ------------------------------------
	-- delete_action
	-- ------------------------------------
	elsif (p_action = 'delete_action' and p_album_pk is not null) then

		-- delete pictures connected to this album
		delete from pic_album_picture
		where pic_picture_fk_pk = p_picture_pk;

		delete from pic_picture
		where pic_picture_pk not in
		(
			select	pic_picture_fk_pk
			from	pic_album_picture
		);

		update	pic_album
		set		date_update = sysdate
		where	pic_album_pk = p_album_pk;

		update	org_organisation
		set		date_update = sysdate
		where	org_organisation_pk = c_organisation_pk;

		commit;

		v_html := v_html||htm.jumpTo(g_package||'.'||c_procedure||'?p_album_pk='||p_album_pk, 1, '...');

	-- ------------------------------------
	-- failed
	-- ------------------------------------
	elsif (p_action = 'failed') then
		v_html := v_html||htm.jumpTo(
					  g_package||'.'||c_procedure||'?p_album_pk='||p_album_pk
					, 10
					, c_text_uploadFailed
				);

	-- ------------------------------------
	-- failed_thumb
	-- ------------------------------------
	elsif (p_action = 'failed_thumb') then
		v_html := v_html||htm.jumpTo(
					  g_package||'.'||c_procedure||'?p_album_pk='||p_album_pk
					, 10
					, c_text_uploadFailed
				);

	-- ------------------------------------
	-- update_text
	-- ------------------------------------
	elsif (p_action like 'update_text' and p_picture_pk is not null and p_album_pk is not null) then

		v_description := htm_lib.HTMLEncode(p_description);
		
		update	pic_picture
		set		description = v_description
		where	pic_picture_pk = p_picture_pk;

		update	pic_album
		set		date_update = sysdate
		where	pic_album_pk = p_album_pk;

		update	org_organisation
		set		date_update = sysdate
		where	org_organisation_pk = c_organisation_pk;

		commit;		

		v_html := v_html||htm.jumpTo(g_package||'.'||c_procedure||'?p_album_pk='||p_album_pk, 1, '...');

	-- ------------------------------------
	-- de_activate
	-- ------------------------------------
	elsif (p_action = 'de_activate' and p_picture_pk is not null and p_album_pk is not null) then

		if (pic_lib.isActive(p_picture_pk => p_picture_pk)) then
			update	pic_picture
			set		  bool_active = 0
					, date_update = sysdate
			where	pic_picture_pk = p_picture_pk;

		else
			update	pic_picture
			set		  bool_active = 1
					, date_update = sysdate
			where	pic_picture_pk = p_picture_pk;
		end if;

		update	pic_album
		set		date_update = sysdate
		where	pic_album_pk = p_album_pk;

		update	org_organisation
		set		date_update = sysdate
		where	org_organisation_pk = c_organisation_pk;

		commit;

		v_html := v_html||htm.jumpTo(g_package||'.'||c_procedure||'?p_album_pk='||p_album_pk, 1, '...');

	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		return null;
end;
end editPictures;


-- ******************************************** --
end;
/
show errors;
