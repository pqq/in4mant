PROMPT *** con_adm ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package con_adm is
/* ******************************************************
*	Package:     pag_pub
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	051018 | Frode Klevstul
*			- Package created
****************************************************** */
	type parameter_arr 			is table of varchar2(4000) index by binary_integer;
	empty_array 				parameter_arr;

	procedure run;
	procedure admContent
	(
		  p_action				in varchar2										default null
		, p_mode				in varchar2										default null
		, p_con_type_pk			in con_type.con_type_pk%type					default null
		, p_content_pk			in con_content.con_content_pk%type				default null
		, p_date_start_date		in varchar2										default null
		, p_date_start_clock	in varchar2										default null
		, p_date_stop_date		in varchar2										default null
		, p_date_stop_clock		in varchar2										default null
		, p_date_publish_date	in varchar2										default null
		, p_date_publish_clock	in varchar2										default null
		, p_subject				in con_content.subject%type						default null
		, p_ingress				in con_content.ingress%type						default null
		, p_body				in con_content.body%type						default null
		, p_footer				in con_content.footer%type						default null
		, p_rating				in con_content.rating%type						default null
		, p_con_subtype_pk		in con_subtype.con_subtype_pk%type				default null
		, p_url_name_1			in con_url.name%type							default null
		, p_url_name_2			in con_url.name%type							default null
		, p_url_name_3			in con_url.name%type							default null
		, p_url_1				in con_url.url%type								default null
		, p_url_2				in con_url.url%type								default null
		, p_url_3				in con_url.url%type								default null
		, p_content_pk_1		in con_content.con_content_pk%type				default null
		, p_content_pk_2		in con_content.con_content_pk%type				default null
		, p_content_pk_3		in con_content.con_content_pk%type				default null
		, p_check_repeat		in varchar2										default null
		, p_check_subscription	in varchar2										default null
		, p_check_pic			in varchar2										default null
		, p_check_preview		in varchar2										default null
		, p_check_addNewContent	in varchar2										default null
	);
	function admContent
	(
		  p_action				in varchar2										default null
		, p_mode				in varchar2										default null
		, p_con_type_pk			in con_type.con_type_pk%type					default null
		, p_content_pk			in con_content.con_content_pk%type				default null
		, p_date_start_date		in varchar2										default null
		, p_date_start_clock	in varchar2										default null
		, p_date_stop_date		in varchar2										default null
		, p_date_stop_clock		in varchar2										default null
		, p_date_publish_date	in varchar2										default null
		, p_date_publish_clock	in varchar2										default null
		, p_subject				in con_content.subject%type						default null
		, p_ingress				in con_content.ingress%type						default null
		, p_body				in con_content.body%type						default null
		, p_footer				in con_content.footer%type						default null
		, p_rating				in con_content.rating%type						default null
		, p_con_subtype_pk		in con_subtype.con_subtype_pk%type				default null
		, p_url_name_1			in con_url.name%type							default null
		, p_url_name_2			in con_url.name%type							default null
		, p_url_name_3			in con_url.name%type							default null
		, p_url_1				in con_url.url%type								default null
		, p_url_2				in con_url.url%type								default null
		, p_url_3				in con_url.url%type								default null
		, p_content_pk_1		in con_content.con_content_pk%type				default null
		, p_content_pk_2		in con_content.con_content_pk%type				default null
		, p_content_pk_3		in con_content.con_content_pk%type				default null
		, p_check_repeat		in varchar2										default null
		, p_check_subscription	in varchar2										default null
		, p_check_pic			in varchar2										default null
		, p_check_preview		in varchar2										default null
		, p_check_addNewContent	in varchar2										default null
	) return clob;
	procedure conOverview
	(
		p_con_type_pk			con_type.con_type_pk%type				default null
	);
	function conOverview(
		p_con_type_pk			con_type.con_type_pk%type				default null
	) return clob;

end;
/
show errors;




-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body con_adm is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'con_adm';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  18.10.2005
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        admContent
-- what:        insert / update / delete content
-- author:      Frode Klevstul
-- start date:  09.01.2006
---------------------------------------------------------
procedure admContent
(
	  p_action				in varchar2										default null
	, p_mode				in varchar2										default null
	, p_con_type_pk			in con_type.con_type_pk%type					default null
	, p_content_pk			in con_content.con_content_pk%type				default null
	, p_date_start_date		in varchar2										default null
	, p_date_start_clock	in varchar2										default null
	, p_date_stop_date		in varchar2										default null
	, p_date_stop_clock		in varchar2										default null
	, p_date_publish_date	in varchar2										default null
	, p_date_publish_clock	in varchar2										default null
	, p_subject				in con_content.subject%type						default null
	, p_ingress				in con_content.ingress%type						default null
	, p_body				in con_content.body%type						default null
	, p_footer				in con_content.footer%type						default null
	, p_rating				in con_content.rating%type						default null
	, p_con_subtype_pk		in con_subtype.con_subtype_pk%type				default null
	, p_url_name_1			in con_url.name%type							default null
	, p_url_name_2			in con_url.name%type							default null
	, p_url_name_3			in con_url.name%type							default null
	, p_url_1				in con_url.url%type								default null
	, p_url_2				in con_url.url%type								default null
	, p_url_3				in con_url.url%type								default null
	, p_content_pk_1		in con_content.con_content_pk%type				default null
	, p_content_pk_2		in con_content.con_content_pk%type				default null
	, p_content_pk_3		in con_content.con_content_pk%type				default null
	, p_check_repeat		in varchar2										default null
	, p_check_subscription	in varchar2										default null
	, p_check_pic			in varchar2										default null
	, p_check_preview		in varchar2										default null
	, p_check_addNewContent	in varchar2										default null
) is begin
	ext_lob.pri(
		con_adm.admContent(
			  p_action
			, p_mode
			, p_con_type_pk
			, p_content_pk
			, p_date_start_date
			, p_date_start_clock
			, p_date_stop_date
			, p_date_stop_clock
			, p_date_publish_date
			, p_date_publish_clock
			, p_subject
			, p_ingress
			, p_body
			, p_footer
			, p_rating
			, p_con_subtype_pk
			, p_url_name_1
			, p_url_name_2
			, p_url_name_3
			, p_url_1
			, p_url_2
			, p_url_3
			, p_content_pk_1
			, p_content_pk_2
			, p_content_pk_3
			, p_check_repeat
			, p_check_subscription
			, p_check_pic
			, p_check_preview
			, p_check_addNewContent
		)
	);
 end;

function admContent
(
	  p_action				in varchar2										default null
	, p_mode				in varchar2										default null
	, p_con_type_pk			in con_type.con_type_pk%type					default null
	, p_content_pk			in con_content.con_content_pk%type				default null
	, p_date_start_date		in varchar2										default null
	, p_date_start_clock	in varchar2										default null
	, p_date_stop_date		in varchar2										default null
	, p_date_stop_clock		in varchar2										default null
	, p_date_publish_date	in varchar2										default null
	, p_date_publish_clock	in varchar2										default null
	, p_subject				in con_content.subject%type						default null
	, p_ingress				in con_content.ingress%type						default null
	, p_body				in con_content.body%type						default null
	, p_footer				in con_content.footer%type						default null
	, p_rating				in con_content.rating%type						default null
	, p_con_subtype_pk		in con_subtype.con_subtype_pk%type				default null
	, p_url_name_1			in con_url.name%type							default null
	, p_url_name_2			in con_url.name%type							default null
	, p_url_name_3			in con_url.name%type							default null
	, p_url_1				in con_url.url%type								default null
	, p_url_2				in con_url.url%type								default null
	, p_url_3				in con_url.url%type								default null
	, p_content_pk_1		in con_content.con_content_pk%type				default null
	, p_content_pk_2		in con_content.con_content_pk%type				default null
	, p_content_pk_3		in con_content.con_content_pk%type				default null
	, p_check_repeat		in varchar2										default null
	, p_check_subscription	in varchar2										default null
	, p_check_pic			in varchar2										default null
	, p_check_preview		in varchar2										default null
	, p_check_addNewContent	in varchar2										default null
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type		:= 'admContent';

	c_id						constant ses_session.id%type 						:= ses_lib.getId;
	c_organisation_pk			constant org_organisation.org_organisation_pk%type	:= ses_lib.getOrgPk(c_id);
	c_service_pk				constant ser_service.ser_service_pk%type			:= ses_lib.getSer(c_id);

	c_dir_img_org				constant sys_parameter.value%type 					:= sys_lib.getParameter('dir_img_org')||'_'||c_organisation_pk;
	c_con_pic					constant sys_parameter.value%type 					:= sys_lib.getParameter('con_pic');
	v_con_pic					sys_parameter.value%type;
	c_max_size					constant sys_parameter.value%type 					:= sys_lib.getParameter('max_cpho_size');
	c_max_width					constant sys_parameter.value%type 					:= sys_lib.getParameter('max_cpho_width');
	c_max_height				constant sys_parameter.value%type 					:= sys_lib.getParameter('max_cpho_height');
	c_dateFormat_hhmi			constant sys_parameter.value%type 					:= sys_lib.getParameter('dateFormat_hhmi');
	c_dateFormat_ymd			constant sys_parameter.value%type 					:= sys_lib.getParameter('dateFormat_ymd');
	c_directory_path			constant sys.all_directories.directory_path%type 	:= org_lib.getDirectoryPath(c_organisation_pk, true);
			
	e_stop						exception;
	e_return					exception;

	c_tex_addContent			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'addContent');
	c_tex_insertUpdateContent	constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'insertUpdateContent');
	c_tex_goBack				constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'goBack');
	c_tex_pleaseWait			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'pleaseWait');
	c_tex_addPicture			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'addPicture');
	c_tex_advancedMode			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'advancedMode');
	c_tex_simpleMode			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'simpleMode');
	c_tex_defaultValues			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'defaultValues');
	c_tex_startTime				constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'startTime');
	c_tex_subtype				constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'subtype');
	c_tex_setDefaultValues		constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'setDefaultValues');
	c_tex_dateStart				constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'dateStart');
	c_tex_dateStop				constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'dateStop');
	c_tex_datePublish			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'datePublish');
	c_tex_time					constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'time');
	c_tex_date					constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'date');
	c_tex_rating				constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'rating');
	c_tex_subject				constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'subject');
	c_tex_ingress				constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'ingress');
	c_tex_body					constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'body');
	c_tex_footer				constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'footer');
	c_tex_url					constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'url');
	c_tex_urlName				constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'urlName');
	c_tex_urlLink				constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'urlLink');
	c_tex_relateContent			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'relateContent');
	c_tex_changePicture			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'changePicture');
	c_tex_repeatEvent			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'repeatEvent');
	c_tex_addSubscription		constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'addSubscription');
	c_tex_preview				constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'preview');
	c_tex_clock					constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'clock');
	c_tex_publish				constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'publish');
	c_tex_update				constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'update');
	c_tex_pictureFile			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'pictureFile');
	c_tex_wrongDateStartClock	constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'wrongDateStartClock');
	c_tex_wrongDateStartDate	constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'wrongDateStartDate');
	c_tex_wrongDateStopClock	constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'wrongDateStopClock');
	c_tex_wrongDateStopDate		constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'wrongDateStopDate');
	c_tex_wrongDatePublishClock	constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'wrongDatePublishClock');
	c_tex_wrongDatePublishDate	constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'wrongDatePublishDate');
	c_tex_subjectIsNull			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'subjectIsNull');
	c_tex_bodyIsNull			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'bodyIsNull');
	c_tex_urlNameIsNull			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'urlNameIsNull');
	c_text_emptyPath			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'emptyPath');
	c_text_deleteImage			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'deleteImage');
	c_text_noPicChange			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'noPicChange');
	c_text_setDefaultValues		constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'setDefaultValues');
	c_text_delete				constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'delete');
	c_text_doNotDelete			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'doNotDelete');
	c_text_startDateLessThanSys	constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'startDateLessThanSys');
	c_text_startLessThanStop	constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'startLessThanStop');
	c_tex_addNewContent			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'addNewContent');
	c_tex_addConStay			constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'addConStay');
	c_tex_picUploadFailed		constant tex_text.string%type			:= tex_lib.get(g_package, c_procedure, 'picUploadFailed');

	cursor	cur_content
		(
			b_content_pk con_content.con_content_pk%type
		) is
	select	subject, ingress, body, footer, date_start, date_stop, date_publish, con_subtype_fk
	from	con_content
	where	con_content_pk = b_content_pk
	and		org_organisation_fk = c_organisation_pk;

	cursor	cur_con_type is
	select	con_type_pk, name, description
	from	con_type
	order by name;
	cursor	cur_con_url is
	select	url, name
	from	con_url
	where	con_content_fk = p_content_pk
	order by name;

	cursor	cur_con_relation is
	select	con_content_2_fk_pk
	from	con_relation
	where	con_content_1_fk_pk = p_content_pk;

	cursor	cur_log_upload
	(
		b_id	ses_session.id%type
	) is
	select	trg_filename
	from	log_upload
	where	log_upload_pk =
	(
		select	max(log_upload_pk)
		from	log_upload
		where	ses_session_id = b_id
	);

	v_content_pk				con_content.con_content_pk%type;
	v_update					boolean								:= false;
	v_type						con_type.name%type;
	v_con_type_pk				con_type.con_type_pk%type;
	v_con_subtype_pk			con_subtype.con_subtype_pk%type;
	v_con_subtype_pk_def		con_subtype.con_subtype_pk%type;
	v_rating_max				con_type.rating_max%type;
	v_date_insert				con_content.date_insert%type;
	v_count						number;
	v_count2					number;
	v_pk						number;
	v_tmp						varchar2(256);
	v_date_start_clock_def		varchar2(16);
	v_query_string				varchar2(256);
	v_date_start				con_content.date_start%type;
	v_date_stop					con_content.date_stop%type;
	v_date_publish				con_content.date_publish%type;
	v_arr_content_pk			parameter_arr;
	v_subject					varchar2(512);
	v_ingress					varchar2(1024);
	v_footer					varchar2(512);
	v_url_name_1				varchar2(128);
	v_url_name_2				varchar2(128);
	v_url_name_3				varchar2(128);
	v_url						con_url.url%type;
	v_sysdate					varchar2(16);
	v_mode						varchar2(16);

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_action='||p_action||'��p_con_type_pk='||p_con_type_pk||'��p_content_pk='||p_content_pk);
	ext_lob.ini(v_clob);

	use_lib.loggedInCheck(p_url => g_package||'.'||c_procedure||'?p_con_type_pk='||p_con_type_pk||'&p_action='||p_action||'&p_content_pk='||p_content_pk);

	-- ---
	-- delete dummy / tmp entry
	-- (in case it wasn't deleted earlier due to some error)
	-- ---
	delete from con_content
	where con_content_pk = -180876;
	commit;

	-- ---------------------------
	-- assign v_mode
	-- ---------------------------
	if (p_action = 'form' and p_content_pk is not null) then
		select	count(*)
		into	v_count
		from	con_content
		where	con_content_pk = p_content_pk
		and
		(
			footer is not null
			or ingress is not null
			or date_stop is not null
			or date_publish is not null
		);

		select	count(*) + v_count
		into	v_count
		from	con_relation
		where	con_content_1_fk_pk = p_content_pk;

	end if;

	-- start in advanced mode if content was made in that mode
	if (v_count > 0 and p_mode is null) then
		v_mode := 'advanced';
	else
		v_mode := p_mode;
	end if;

	-- ---------------------------------------------------------------
	-- SECURITY CHECK:
	-- check to make sure p_content_pk is related to
	-- the organisation in session
	-- ---------------------------------------------------------------

	-- If the content_pk doesn't exsist from before we'll not do the
	-- security check (cause then it'll fail/stop)
	select	count(*)
	into	v_count
	from	con_content
	where	con_content_pk = p_content_pk;

	if (v_count > 0) then
		select	count(*)
		into	v_count
		from	con_content
		where	con_content_pk = p_content_pk
		and		org_organisation_fk = c_organisation_pk;

		if (v_count = 0 and p_content_pk is not null) then
			if (c_organisation_pk is null) then
				v_html := v_html||htm.jumpTo('pag_pub.userLogin', 1, '...');
				return v_html;
			else
				pag_pub.errorPage(g_package, c_procedure, 'fake_content_pk:'||p_content_pk||':'||p_action);
			end if;

			raise e_stop;
		end if;
	end if;

	-- --------------------------------------------------------
	-- Build query string
	-- This string is used whenever we jump to another page
	-- --------------------------------------------------------
	v_query_string := 'p_check_repeat='||p_check_repeat||'&amp;p_check_subscription='||p_check_subscription||'&amp;p_check_preview='||p_check_preview||'&amp;p_check_addNewContent='||p_check_addNewContent||'&amp;p_mode='||v_mode;

	-- ---------------------------------------------------
	-- Are we doing an update or a new insert?
	-- If p_content_pk is null we're doing an update,
	-- we have to generate a new PK if we're going to
	-- show the form for insert (we need to know the
	-- PK before we do the actual insert)
	-- ---------------------------------------------------
	if (p_content_pk is null and p_action = 'form') then
		v_update := false;

		-- -----------------------------------------------------------
		-- if we don't know p_con_type_pk there is no point
		-- of generating a new PK since we need to know p_con_type_pk
		-- before we can proceed (this will stop later on if we don't
		-- have p_con_type_pk)
		-- -----------------------------------------------------------
 		if (p_con_type_pk is not null) then
			v_content_pk := sys_lib.getEmptyPk('con_content');

			-- ----------------------------------------------------------------
			-- do an insert in con_content so no other will get the same PK
			-- this entry will be deleted later on, in case we never continue
			-- ----------------------------------------------------------------
			insert into con_content
			(con_content_pk, footer, org_organisation_fk, date_insert)
			values (v_content_pk, '[tmp]', c_organisation_pk, sysdate);
			commit;
		end if;

		-- ----------------------------------------------------------------------
		-- since this is no update (it's a new insert) we have to know the
		-- p_con_type_pk (otherwise this can be found based on con_content_pk)
		-- ----------------------------------------------------------------------
		v_query_string := v_query_string||'&amp;p_con_type_pk='||p_con_type_pk;

	elsif (p_content_pk is not null) then

		v_update := true;

		v_content_pk := p_content_pk;

		v_query_string := v_query_string||'&amp;p_content_pk='||v_content_pk;

		-- we have to add the p_con_type_pk in case we want to continue publishing
		if (p_con_type_pk is not null) then
			v_query_string := v_query_string||'&amp;p_con_type_pk='||p_con_type_pk;
		end if;

	elsif (p_con_type_pk is not null) then

		v_query_string := v_query_string||'&amp;p_con_type_pk='||p_con_type_pk;

	end if;

	-- format of picture file (without filetype)
	v_con_pic := v_content_pk||''||c_con_pic;

	-- -----------------------------------------------------------------------
	--
	-- display update form
	--
	-- -----------------------------------------------------------------------
	if (p_action = 'form' and c_organisation_pk is not null) then

		-- --------------------------------------------------------------
		-- we have to know the name of the content type (con_type)
		-- to show a correct version of the form
		-- --------------------------------------------------------------
		-- 1: ask the user if we can't find out
		if (p_con_type_pk is null and p_content_pk is null) then

			v_html := v_html||'Chose content type:<br>';
			for r_cursor in cur_con_type
			loop
				v_html := v_html||'<a href="'||g_package||'.'||c_procedure||'?p_action=form&amp;p_con_type_pk='||r_cursor.con_type_pk||'">'||r_cursor.name||'</a> |';
			end loop;
			raise e_return;

		-- 2: find it based on con_content_pk (p_content_pk)
		elsif (p_con_type_pk is null and p_content_pk is not null) then

			select	name, con_type_pk
			into	v_type, v_con_type_pk
			from	con_type
			where	con_type_pk = (
				select	con_type_fk
				from	con_content
				where	con_content_pk = p_content_pk
			);

		-- 3: we know the pk for con_type (p_con_type_pk)
		elsif (p_con_type_pk is not null) then
			select	name
			into	v_type
			from	con_type
			where	con_type_pk = p_con_type_pk;

			v_con_type_pk := p_con_type_pk;
		end if;

		-- --------------------------------------------------
		-- get default values from browser cookie
		-- --------------------------------------------------
		v_tmp := htm_lib.getCookie('in4mant_defaultConValues');
		v_date_start_clock_def := substr(v_tmp, 1, instr(v_tmp, '-')-1);
		v_con_subtype_pk_def := substr(v_tmp, instr(v_tmp, '-')+1, length(v_tmp));

		-- ------------------------------------------------------------------
		--
		-- start HTML page
		--
		-- ------------------------------------------------------------------
		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundAdminBox')||'<tr><td>'||chr(13);

		-- -----------------
		-- get sysdate
		-- -----------------
		select	to_char(sysdate, 'YYMMDDHHMI')
		into	v_sysdate
		from	dual;

		-- ----------------------------------
		-- JavaScript functions needed
		-- ----------------------------------
		v_html := v_html||'
			<script language="JavaScript" type="text/javascript">

				function toggleVisibility(freezePModeSwap) {
				
					if (freezePModeSwap != ''yes'') {
						var p_mode 				= document.getElementById(''p_mode'');
	
						if (p_mode.value == ''simple'') {
							p_mode.value = ''advanced'';
						} else {
							p_mode.value = ''simple'';
						}
					}

					var swapTextToAdvanced	= document.getElementById(''swapTextToAdvanced'');
					var swapTextToSimple	= document.getElementById(''swapTextToSimple'');
					
					if (swapTextToAdvanced.style.display == ''none'') {
						swapTextToAdvanced.style.display	= ''block'';
						swapTextToSimple.style.display		= ''none'';
					} else {
						swapTextToAdvanced.style.display	= ''none'';
						swapTextToSimple.style.display		= ''block'';
					}

					if(document.getElementById(''defaultValues'')){
						var defaultValues = document.getElementById(''defaultValues'');
						
						if (defaultValues.style.display == ''none'') {
							defaultValues.style.display		= ''block'';
						} else {
							defaultValues.style.display		= ''none'';
						}
					}

					var publishDate = document.getElementById(''publishDate'');
					
					if (publishDate.style.display == ''none'') {
						publishDate.style.display		= ''block'';
					} else {
						publishDate.style.display		= ''none'';
					}

					var ingress = document.getElementById(''ingress'');
					
					if (ingress.style.display == ''none'') {
						ingress.style.display		= ''block'';
					} else {
						ingress.style.display		= ''none'';
					}

					var footer = document.getElementById(''footer'');
					
					if (footer.style.display == ''none'') {
						footer.style.display		= ''block'';
					} else {
						footer.style.display		= ''none'';
					}

					var urls = document.getElementById(''urls'');
					
					if (urls.style.display == ''none'') {
						urls.style.display		= ''block'';
					} else {
						urls.style.display		= ''none'';
					}

					var relatedContent = document.getElementById(''relatedContent'');
					
					if (relatedContent.style.display == ''none'') {
						relatedContent.style.display		= ''block'';
					} else {
						relatedContent.style.display		= ''none'';
					}

					var stopTime = document.getElementById(''stopTime'');
					
					if (stopTime.style.display == ''none'') {
						stopTime.style.display		= ''block'';
					} else {
						stopTime.style.display		= ''none'';
					}

				}

				function submitForm(){

					var	p_date_start_date;
					var	p_date_start_clock;
					var	p_date_stop_clock;
					var	p_date_stop_date;
					var	p_date_publish_clock;
					var	p_date_publish_date;
					var	p_subject;
					var	p_body;

					var v_start_date;
					var v_stop_date;

					// ---------------------------------
					// check that content is valid
					// ---------------------------------
					if (document.getElementById(''p_date_start_clock'')){

						p_date_start_clock		= document.getElementById(''p_date_start_clock'');

						if (p_date_start_clock.value.length == 0 || !verifyTime(p_date_start_clock.value)){
							alert("'||c_tex_wrongDateStartClock||': '||c_dateFormat_hhmi||'");
							return;
						}

						p_date_start_clock.value	= verifyTime(p_date_start_clock.value);
					}

					if (document.getElementById(''p_date_start_date'')){
						var	p_date_start_date		= document.getElementById(''p_date_start_date'');

						if (p_date_start_date.value.length == 0 || !verifyDate(p_date_start_date.value)){
							alert("'||c_tex_wrongDateStartDate||': '||c_dateFormat_ymd||'");
							return;
						}

						p_date_start_date.value		= verifyDate(p_date_start_date.value);
					}

					// check that: start date > sysdate
					if (p_date_start_clock != null && p_date_start_date != null){
						if (p_date_start_clock.value != '''' && p_date_start_date.value != '''') {
							v_start_date =
								  p_date_start_date.value.substring(6,8)
								+ p_date_start_date.value.substring(3,5)
								+ p_date_start_date.value.substring(0,2)
								+ p_date_start_clock.value.substring(0,2)
								+ p_date_start_clock.value.substring(3,5);

							if (v_start_date < '''||v_sysdate||'''){
								alert("'||c_text_startDateLessThanSys||'");
								return;
							}
						}
					}

					if (document.getElementById(''p_date_stop_clock'')){
						var	p_date_stop_clock		= document.getElementById(''p_date_stop_clock'');

						if (p_date_stop_clock.value.length != 0 && !verifyTime(p_date_stop_clock.value)){
							alert("'||c_tex_wrongDateStopClock||': '||c_dateFormat_hhmi||'");
							return;
						}

						p_date_stop_clock.value		= verifyTime(p_date_stop_clock.value);
					}

					if (document.getElementById(''p_date_stop_date'')){
						var	p_date_stop_date		= document.getElementById(''p_date_stop_date'');

						if (p_date_stop_date.value.length != 0 && !verifyDate(p_date_stop_date.value)){
							alert("'||c_tex_wrongDateStopDate||': '||c_dateFormat_ymd||'");
							return;
						}

						p_date_stop_date.value		= verifyDate(p_date_stop_date.value);
					}

					if (document.con_form.p_date_stop_clock && document.con_form.p_date_stop_date){
						// check that: stop date > start date
						if (p_date_stop_clock.value != '''' && p_date_stop_date.value != '''') {
							v_stop_date =
								  p_date_stop_date.value.substring(6,8)
								+ p_date_stop_date.value.substring(3,5)
								+ p_date_stop_date.value.substring(0,2)
								+ p_date_stop_clock.value.substring(0,2)
								+ p_date_stop_clock.value.substring(3,5);

							if (v_stop_date < v_start_date){
								alert("'||c_text_startLessThanStop||'");
								return;
							}
						}
					}

					if (document.getElementById(''p_date_publish_clock'')){
						var	p_date_publish_clock	= document.getElementById(''p_date_publish_clock'');

						if (p_date_publish_clock.value.length != 0 && !verifyTime(p_date_publish_clock.value)){
							alert("'||c_tex_wrongDatePublishClock||': '||c_dateFormat_hhmi||'");
							return;
						}

						p_date_publish_clock.value	= verifyTime(p_date_publish_clock.value);
					}

					if (document.getElementById(''p_date_publish_date'')){
						var	p_date_publish_date		= document.getElementById(''p_date_publish_date'');

						if (p_date_publish_date.value.length != 0 && !verifyDate(p_date_publish_date.value)){
							alert("'||c_tex_wrongDatePublishDate||': '||c_dateFormat_ymd||'");
							return;
						}

						p_date_publish_date.value	= verifyDate(p_date_publish_date.value);
					}

					if (document.getElementById(''p_subject'')){
						var	p_subject				= document.getElementById(''p_subject'');

						if (p_subject.value.length == 0){
							alert("'||c_tex_subjectIsNull||'");
							return;
						}
					}

					// ------------------------------------------------------
					// due to FCKeditor this is done differently
					// ------------------------------------------------------
					// Get the editor instance that we want to interact with.
					var oEditor = FCKeditorAPI.GetInstance(''p_body'') ;

					// Get the Editor Area DOM (Document object).
					var oDOM = oEditor.EditorDocument ;

					var iLength ;

					// The are two diffent ways to get the text (without HTML markups).
					// It is browser specific.
					if ( document.all )		// If Internet Explorer.
					{
						iLength = oDOM.body.innerText.length ;
					}
					else					// If Gecko.
					{
						var r = oDOM.createRange() ;
						r.selectNodeContents( oDOM.body ) ;
						iLength = r.toString().length ;
					}

					if (iLength == 0){
						alert("'||c_tex_bodyIsNull||'");
						return;
					}

					if (document.getElementById(''p_url_name_1'')){
						var	p_url_name_1	= document.getElementById(''p_url_name_1'');
						var	p_url_1			= document.getElementById(''p_url_1'');

						if (p_url_name_1.value.length == 0 && p_url_1.value.length > 0){
							alert("'||c_tex_urlNameIsNull||'");
							return;
						}
					}

					if (document.getElementById(''p_url_name_2'')){
						var	p_url_name_2	= document.getElementById(''p_url_name_2'');
						var	p_url_2			= document.getElementById(''p_url_2'');

						if (p_url_name_2.value.length == 0 && p_url_2.value.length > 0){
							alert("'||c_tex_urlNameIsNull||'");
							return;
						}
					}

					if (document.getElementById(''p_url_name_3'')){
						var	p_url_name_3	= document.getElementById(''p_url_name_3'');
						var	p_url_3			= document.getElementById(''p_url_3'');

						if (p_url_name_3.value.length == 0 && p_url_3.value.length > 0){
							alert("'||c_tex_urlNameIsNull||'");
							return;
						}
					}

					document.con_form.submit();
				}

			</script>

			<script type="text/javascript" src="/FCKeditor/fckeditor.js"></script>

			<script type="text/javascript">
			<!--
	      		window.onload = function()
	      		{
	        		var oFCKeditor = new FCKeditor( ''p_body'' ) ;
					oFCKeditor.BasePath = ''/FCKeditor/'' ;
					oFCKeditor.ToolbarSet = "i4_generalInfo" ;
					oFCKeditor.Width = 500 ;
					oFCKeditor.Height = 400 ;
					oFCKeditor.ReplaceTextarea() ;
				}
			//-->
			</script>
		';

		v_html := v_html||''||htm.bForm(g_package||'.'||c_procedure, 'con_form')||chr(13);
		-- -------------------------------------------------------------
		-- hidden values
		-- -------------------------------------------------------------
		v_html := v_html||'<input type="hidden" name="p_content_pk" id="p_content_pk" value="'||v_content_pk||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_action" id="p_action" value="insert_update" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_con_type_pk" id="p_con_type_pk" value="'||v_con_type_pk||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_mode" id="p_mode" value="'||v_mode||'" />'||chr(13);

		v_html := v_html||''||htm.bBox(c_tex_addContent)||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);

		-- add code to the clob
		ext_lob.add(v_clob, v_html);

		-- ----------------
		-- swap mode
		-- ----------------
		v_html := v_html||'
			<tr>
				<td colspan="3" style="text-align: right; padding-right: 20px">
					<div id="swapTextToAdvanced" style="display: block">
					<a class="theButtonStyle" href="javascript:toggleVisibility()">'||c_tex_advancedMode||'</a>
					</div>
					<div id="swapTextToSimple" style="display: none">
					<a class="theButtonStyle" href="javascript:toggleVisibility()">'||c_tex_simpleMode||'</a>
					</div>
				</td>
			</tr>'||chr(13);

		v_tmp := v_query_string;
		if (v_mode = 'advanced') then
			-- sometimes v_mode is null, that's why we have to do two replace
			v_tmp := replace(v_tmp, 'p_mode=advanced', 'p_mode=');
			v_tmp := replace(v_tmp, 'p_mode=', 'p_mode=simple');
		elsif (v_mode = 'simple' or v_mode is null) then
			v_tmp := replace(v_tmp, 'p_mode=simple', 'p_mode=');
			v_tmp := replace(v_tmp, 'p_mode=', 'p_mode=advanced');
		else
			v_html := v_html||'<tr><td colspan="3">error: unknown mode</td></tr>'||chr(13);
		end if;

		-- ----------------
		-- default values
		-- ----------------
		if (v_type like 'event%' or v_type like 'review%') then

			-- // toggleVisibility
			v_html := v_html||'<tr><td colspan="3"><table id="defaultValues" style="display: none; width: 100%">'||chr(13);
			v_html := v_html||'<tr><td><hr noshade width="500" size="1" /></td></tr>'||chr(13);
			v_html := v_html||'
				<tr>
					<td>
						'||c_tex_defaultValues||'<span style="padding-left: 10px;">&nbsp;</span>
			';
			if (v_type like 'event%') then
				v_html := v_html||' '||c_tex_startTime||': '||v_date_start_clock_def||', '||chr(13);
			end if;
			v_html := v_html||c_tex_subtype||': '||v_con_subtype_pk_def||chr(13);
			v_html := v_html||'
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						(<a href="'||g_package||'.'||c_procedure||'?p_action=setDefaultValues&amp;'||v_query_string||'">'||c_tex_setDefaultValues||'</a>)
					</td>
				</tr>';

			v_html := v_html||'<tr><td><hr noshade width="500" size="1" /></td></tr>'||chr(13);
			v_html := v_html||'</table></td></tr>';
		end if;

		v_html := v_html||'<tr><td colspan="3">&nbsp;</td></tr>'||chr(13);

		-- ----------------
		-- start/stop date
		-- ----------------
		if (v_type like 'event%') then
			v_html := v_html||'
				<tr>
					<td width="115">
						'||c_tex_dateStart||'
					</td>
					<td width="15%">
						'||c_tex_time||'<br>
						(<i>'||c_dateFormat_hhmi||'</i>)<br>
						<input type="text" name="p_date_start_clock" id="p_date_start_clock" size="5" maxlength="5" value="[r_cursor.date_start(clock)]" />
						'||htm.popUp(c_tex_clock, 'htm_lib.clock?p_form=con_form&amp;p_field=p_date_start_clock','350', '50')||'
					</td>
					<td>
						'||c_tex_date||'<br />(<i>'||c_dateFormat_ymd||'</i>)<br />
						<input type="text" name="p_date_start_date" id="p_date_start_date" size="8" maxlength="8" value="[r_cursor.date_start(date)]" />
					</td>
				</tr>';

			-- // toggleVisibility
			v_html := v_html||'<tr><td colspan="3"><table id="stopTime" style="display: none; width: 100%">'||chr(13);
			v_html := v_html||'
				<tr>
					<td width="110">
						'||c_tex_dateStop||'
					</td>
					<td width="15%">
						'||c_tex_time||'<br />(<i>'||c_dateFormat_hhmi||'</i>)<br />
						<input type="text" name="p_date_stop_clock" id="p_date_stop_clock" size="5" maxlength="5" value="[r_cursor.date_stop(clock)]" />
						'||htm.popUp(c_tex_clock, 'htm_lib.clock?p_form=con_form&amp;p_field=p_date_stop_clock','350', '50')||'
					</td>
					<td>
						'||c_tex_date||'<br />(<i>'||c_dateFormat_ymd||'</i>)<br />
						<input type="text" name="p_date_stop_date" id="p_date_stop_date" size="8" maxlength="8" value="[r_cursor.date_stop(date)]" />
					</td>
				</tr>';
			v_html := v_html||'</table></td></tr>';
		end if;

		-- ----------------
		-- publish date
		-- ----------------
		-- // toggleVisibility
		v_html := v_html||'<tr><td colspan="3"><table id="publishDate" style="display: none; width: 100%">'||chr(13);
		v_html := v_html||'
			<tr>
				<td width="110">
					'||c_tex_datePublish||'
				</td>
				<td width="15%">
					'||c_tex_time||'<br />(<i>'||c_dateFormat_hhmi||'</i>)<br />
					<input type="text" name="p_date_publish_clock" id="p_date_publish_clock" size="5" maxlength="5" value="[r_cursor.date_publish(clock)]" />
					'||htm.popUp(c_tex_clock, 'htm_lib.clock?p_form=con_form&amp;p_field=p_date_publish_clock','350', '50')||'
				</td>
				<td>
					'||c_tex_date||'<br />(<i>'||c_dateFormat_ymd||'</i>)<br /><input type="text" name="p_date_publish_date" id="p_date_publish_date" size="8" maxlength="8" value="[r_cursor.date_publish(date)]" />
				</td>
			</tr>';
		v_html := v_html||'</table></td></tr>';

		-- -----------
		-- subtype
		-- -----------
		if (v_type like 'event%' or v_type like 'review%') then
			if (p_content_pk is not null) then
				select	con_subtype_fk
				into	v_con_subtype_pk
				from	con_content
				where	con_content_pk = p_content_pk;
			else
				v_con_subtype_pk := v_con_subtype_pk_def;
			end if;
			v_html := v_html||'<tr><td>'||c_tex_subtype||'</td><td colspan="2">'||htm_lib.selectConSubtype(v_con_subtype_pk)||'</td></tr>'||chr(13);
		end if;

		-- -----------
		-- review
		-- -----------
		if (v_type like 'review%') then

			select	rating_max
			into	v_rating_max
			from	con_type
			where	con_type_pk = nvl(p_con_type_pk, (select con_type_fk from con_content where con_content_pk = p_content_pk));

			v_html := v_html||'<tr><td>'||c_tex_rating||'</td><td colspan="2">'||htm_lib.selectRating(v_rating_max)||'</td></tr>'||chr(13);
		end if;

		v_html := v_html||'<tr><td colspan="3">&nbsp;</td></tr>'||chr(13);

		-- ----------------------------------
		-- subject / ingress / body / footer
		-- ----------------------------------
		v_html := v_html||'<tr><td width="115">'||c_tex_subject||'</td><td colspan="2"><input type="text" name="p_subject" id="p_subject" size="73" maxlength="128" value="[r_cursor.subject]" /></td></tr>'||chr(13);
		-- // toggleVisibility
		v_html := v_html||'<tr><td colspan="3"><table id="ingress" style="display: none; width: 100%">'||chr(13);
		v_html := v_html||'<tr><td width="110">'||c_tex_ingress||'</td><td colspan="2"><textarea name="p_ingress" id="p_ingress" rows="4" cols="68">[r_cursor.ingress]</textarea></td></tr>'||chr(13);
		v_html := v_html||'</table></td></tr>';
		v_html := v_html||'<tr><td colspan="3">&nbsp;</td></tr>'||chr(13);
		v_html := v_html||'
			<tr>
				<td>'||c_tex_body||'</td>
				<td colspan="2"><textarea name="p_body" id="p_body">[r_cursor.body]</textarea></td>
			</tr>';

		-- // toggleVisibility
		v_html := v_html||'<tr><td colspan="3"><table id="footer" style="display: none; width: 100%">'||chr(13);
		v_html := v_html||'<tr><td width="110">'||c_tex_footer||'</td><td colspan="2"><input type="text" name="p_footer" id="p_footer" size="96" maxlength="128" value="[r_cursor.footer]" /></td></tr>'||chr(13);
		v_html := v_html||'</table></td></tr>';

		-- ------------------------------
		-- urls
		-- ------------------------------
		-- // toggleVisibility
		v_html := v_html||'<tr><td colspan="3"><table id="urls" style="display: none; width: 100%">'||chr(13);
		v_html := v_html||'<tr><td colspan="3">&nbsp;</td></tr>'||chr(13);
		v_html := v_html||'<tr><td width="110">'||c_tex_url||' 1</td><td colspan="2">'||c_tex_urlName||': <input type="text" name="p_url_name_1" id="p_url_name_1" size="15" maxlength="32" value="[r_cursor.url_name_1]" /> '||c_tex_urlLink||': <input type="text" name="p_url_1" id="p_url_1" size="40" maxlength="128" value="[r_cursor.url_1]" /></td></tr>'||chr(13);
		v_html := v_html||'<tr><td>'||c_tex_url||' 2</td><td colspan="2">'||c_tex_urlName||': <input type="text" name="p_url_name_2" id="p_url_name_2" size="15" maxlength="32" value="[r_cursor.url_name_2]" /> '||c_tex_urlLink||': <input type="text" name="p_url_2" id="p_url_2" size="40" maxlength="128" value="[r_cursor.url_2]" /></td></tr>'||chr(13);
		v_html := v_html||'<tr><td>'||c_tex_url||' 3</td><td colspan="2">'||c_tex_urlName||': <input type="text" name="p_url_name_3" id="p_url_name_3" size="15" maxlength="32" value="[r_cursor.url_name_3]" /> '||c_tex_urlLink||': <input type="text" name="p_url_3" id="p_url_3" size="40" maxlength="128" value="[r_cursor.url_3]" /></td></tr>'||chr(13);
		v_html := v_html||'</table></td></tr>';

		-- ------------------------------
		-- related content
		-- ------------------------------
		v_count := 1;
		v_arr_content_pk(1) := null;
		v_arr_content_pk(2) := null;
		v_arr_content_pk(3) := null;
		for r_cursor in cur_con_relation
		loop
			v_arr_content_pk(v_count) := r_cursor.con_content_2_fk_pk;
			v_count := v_count + 1;
		end loop;

		-- // toggleVisibility
		v_html := v_html||'<tr><td colspan="3"><table id="relatedContent" style="display: none; width: 100%">'||chr(13);
		v_html := v_html||'<tr><td colspan="3">&nbsp;</td></tr>'||chr(13);
		v_html := v_html||'<tr><td width="110">'||c_tex_relateContent||'</td><td colspan="2">'||htm_lib.selectContent(v_arr_content_pk(1), c_organisation_pk, 1)||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td>'||c_tex_relateContent||'</td><td colspan="2">'||htm_lib.selectContent(v_arr_content_pk(2), c_organisation_pk, 2)||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td>'||c_tex_relateContent||'</td><td colspan="2">'||htm_lib.selectContent(v_arr_content_pk(3), c_organisation_pk, 3)||'</td></tr>'||chr(13);
		v_html := v_html||'</table></td></tr>';

		-- ------------------------------
		-- set/assign value in case up update
		-- ------------------------------
		for r_cursor in cur_content(p_content_pk)
		loop
			v_html := replace(v_html, '[r_cursor.body]', htm_lib.HTMLFix(r_cursor.body));
			v_html := replace(v_html, '[r_cursor.subject]', r_cursor.subject);
			v_html := replace(v_html, '[r_cursor.ingress]', r_cursor.ingress);
			v_html := replace(v_html, '[r_cursor.footer]', r_cursor.footer);

			v_html := replace(v_html, '[r_cursor.date_start(clock)]', to_char(r_cursor.date_start, c_dateFormat_hhmi));
			v_html := replace(v_html, '[r_cursor.date_start(date)]', to_char(r_cursor.date_start, c_dateFormat_ymd));
			v_html := replace(v_html, '[r_cursor.date_stop(clock)]', to_char(r_cursor.date_stop, c_dateFormat_hhmi));
			v_html := replace(v_html, '[r_cursor.date_stop(date)]', to_char(r_cursor.date_stop, c_dateFormat_ymd));
			v_html := replace(v_html, '[r_cursor.date_publish(clock)]', to_char(r_cursor.date_publish, c_dateFormat_hhmi));
			v_html := replace(v_html, '[r_cursor.date_publish(date)]', to_char(r_cursor.date_publish, c_dateFormat_ymd));
		end loop;

		v_count := 1;
		for r_cursor in cur_con_url
		loop
			v_html := replace(v_html, '[r_cursor.url_'||v_count||']', r_cursor.url);
			v_html := replace(v_html, '[r_cursor.url_name_'||v_count||']', r_cursor.name);

			v_count := v_count + 1;
		end loop;

		-- ----------------------------------------------------------------------------
		-- we have to do some replacing if we didn't have three entries in con_url
		-- note: if we had 3 entries v_count will be one more = 4
		-- ----------------------------------------------------------------------------
		if (v_count < 4) then
			v_html := replace(v_html, '[r_cursor.url_1]', '');
			v_html := replace(v_html, '[r_cursor.url_name_1]','');
			v_html := replace(v_html, '[r_cursor.url_2]', '');
			v_html := replace(v_html, '[r_cursor.url_name_2]','');
			v_html := replace(v_html, '[r_cursor.url_3]', '');
			v_html := replace(v_html, '[r_cursor.url_name_3]','');
		end if;

		-- ---------------------------------------------------------------------
		-- if the cursor hasn't been accessed we have to remove the "tags"
		-- ---------------------------------------------------------------------
		if (not v_update) then
			v_html := replace(v_html, '[r_cursor.body]','');
			v_html := replace(v_html, '[r_cursor.subject]','');
			v_html := replace(v_html, '[r_cursor.ingress]','');
			v_html := replace(v_html, '[r_cursor.footer]','');
			v_html := replace(v_html, '[r_cursor.date_start(clock)]', v_date_start_clock_def);
			v_html := replace(v_html, '[r_cursor.date_start(date)]', '');
			v_html := replace(v_html, '[r_cursor.date_stop(clock)]','');
			v_html := replace(v_html, '[r_cursor.date_stop(date)]','');
			v_html := replace(v_html, '[r_cursor.date_publish(clock)]','');
			v_html := replace(v_html, '[r_cursor.date_publish(date)]','');
		end if;

		-- -----------------------
		-- checkboxes
		-- -----------------------
		v_html := v_html||'<tr><td colspan="3">&nbsp;</td></tr>';

		-- ----------------------
		-- checkbox for picture
		-- ----------------------
		select	count(*)
		into	v_count
		from	con_content_file
		where	con_content_fk_pk = p_content_pk;

		if (v_count > 0) then
			-- -------------------------------------
			-- get the latest picture related
			-- note: we only support ONE picture
			-- -------------------------------------
			select	path
			into	v_tmp
			from	con_file
			where	con_file_pk = (
				select	max(con_file_pk)
				from	con_file, con_content_file
				where	con_file_pk = con_file_fk_pk
				and		con_content_fk_pk = p_content_pk
				and		con_file_type_fk = (select con_file_type_pk from con_file_type where lower(name) = 'jpg')
			);

			v_html := v_html||'
				<tr>
					<td>
						'||c_tex_changePicture||'
					</td>
					<td colspan="2" style="padding-left: 20px;">
						<img width="150" src="'||c_directory_path||'/'||v_tmp||'" alt="" />
						<input type="radio" name="p_check_pic" id="p_check_pic1" value="1" /> '||c_tex_changePicture||'
						&nbsp;&nbsp;&nbsp;
						<input type="radio" name="p_check_pic" id="p_check_pic2" value="2" />'||c_text_deleteImage||'
						&nbsp;&nbsp;&nbsp;
						<input type="radio" name="p_check_pic" id="p_check_pic3" value="" />'||c_text_noPicChange||'
					</td>
				</tr>
			';

		else
			v_html := v_html||'
				<tr>
					<td>
						'||c_tex_addPicture||'
					</td>
					<td colspan="2">
						<input type="checkbox" name="p_check_pic" id="p_check_pic" value="1" />
					</td>
				</tr>
			';
		end if;

		-- ----------------------------------------
		-- checkbox for repeat and subscription
		-- ----------------------------------------
		-- // toggleVisibility
		--if (v_type like 'event%') then
		--	if (p_check_repeat = '1') then v_tmp := 'checked'; else v_tmp := ''; end if;
		--	v_html := v_html||'<tr><td colspan="3" style="padding-left: 20px;"><input type="checkbox" name="p_check_repeat" id="p_check_repeat" value="1" '||v_tmp||'> '||c_tex_repeatEvent||'</td></tr>';
		--	if (p_check_subscription = '1') then v_tmp := 'checked'; else v_tmp := ''; end if;
		--	v_html := v_html||'<tr><td colspan="3" style="padding-left: 20px;"><input type="checkbox" name="p_check_subscription" id="p_check_subscription" value="1" '||v_tmp||'> '||c_tex_addSubscription||'</td></tr>';
		--end if;

		-- ------------------------
		-- checkbox for preview
		-- ------------------------
		if (p_check_preview = '1') then v_tmp := 'checked'; else v_tmp := ''; end if;
		v_html := v_html||'
			<tr>
				<td>'||c_tex_preview||'</td>
				<td colspan="2">
					<input type="checkbox" name="p_check_preview" id="p_check_preview" value="1" '||v_tmp||' />
				</td>
			</tr>
		';

		-- ------------------------
		-- checkbox for add another/new content entry
		-- ------------------------
		if (p_check_addNewContent = '1') then v_tmp := 'checked'; else v_tmp := ''; end if;
		v_html := v_html||'
			<tr>
				<td>'||c_tex_addConStay||'</td>
				<td colspan="2">
					<input type="checkbox" name="p_check_addNewContent" id="p_check_addNewContent" value="1" '||v_tmp||' /> ('||c_tex_addNewContent||')
				</td>
			</tr>
		';

		-- -----------------------------------------
		-- toggleVisibility if in advanced mode
		-- -----------------------------------------
		if (v_mode = 'advanced') then
			v_html := v_html ||'
				<script type="text/javascript">
				<!--
		      		toggleVisibility(''yes'');
				//-->
				</script>
			'||chr(13);
		end if;

		-- ---------------------------------
		-- final buttons and end of page
		-- ---------------------------------
		v_html := v_html||'<tr><td colspan="3">&nbsp;</td></tr>'||chr(13);

		v_html := v_html||'<tr><td colspan="3" style="padding-left: 20px;">'||htm.submitLink(c_tex_insertUpdateContent, 'con_form', 'submitForm()')||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td colspan="3" style="padding-left: 20px;">'||htm.historyBack(c_tex_goBack)||'</td></tr>'||chr(13);

		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);
		v_html := v_html||''||htm.eForm||chr(13);

		v_html := v_html||'</td></tr>'||htm.eTable||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

	-- ---------------------------
	-- insert_update (insert and update)
	-- ---------------------------
	elsif (p_action = 'insert_update') then

		-- subject can't be null, and we have to know p_content_pk
		if (p_content_pk is not null and p_subject is not null) then

			-- check that there is an entry with the given PK
			select	count(*)
			into	v_count
			from	con_content
			where	con_content_pk = p_content_pk;

			-- check if this is a new entry (insert, not update)
			-- this is done to set proper status (published or preview)
			select	count(*)
			into	v_count2
			from	con_content
			where	con_content_pk = p_content_pk
			and		footer = '[tmp]';

			-- if we're doing an update we have to do different things to the db because
			-- we're actually doing a complete delete of the entry and then a new insert
			if (v_count > 0) then
				-- save insert_date so this will stay the same for the "new" entry
				select	date_insert
				into	v_date_insert
				from	con_content
				where	con_content_pk = p_content_pk;

				-- we need a dummy entry in con_content to avoid dataloss
				-- (we connect data we don't want to lose to this entry)
				insert into con_content
				(con_content_pk, date_insert)
				values (-180876, sysdate);

				-- update data we don't want to lose (due to cascading constraints)
				update	con_content_file
				set		con_content_fk_pk = -180876
				where	con_content_fk_pk = p_content_pk;

				update	con_ser
				set		con_content_fk_pk = -180876
				where	con_content_fk_pk = p_content_pk;

				update	con_comment
				set		con_content_fk = -180876
				where	con_content_fk = p_content_pk;

				update	con_vote
				set		con_content_fk = -180876
				where	con_content_fk = p_content_pk;

				update	con_relation
				set		con_content_2_fk_pk = -180876
				where	con_content_2_fk_pk = p_content_pk;

				-- delete the old occurence
				delete from	con_url
				where con_content_fk = p_content_pk;

				delete from con_relation
				where con_content_1_fk_pk = p_content_pk;

				delete from con_content
				where con_content_pk = p_content_pk
				and org_organisation_fk = c_organisation_pk;

				delete from con_ser
				where con_content_fk_pk = p_content_pk;

				-- this will delete tmp content where the user never continued filling out info
				delete from con_content
				where footer = '[tmp]'
				and org_organisation_fk = c_organisation_pk;

			end if;

			-- ------------
			-- fix dates
			-- ------------
			if (p_date_stop_date is not null and p_date_stop_clock is not null) then
				v_date_stop := to_date(p_date_stop_date||'-'||p_date_stop_clock, c_dateFormat_ymd||'-'||c_dateFormat_hhmi);
			else
				v_date_stop := null;
			end if;

			if (p_date_publish_date is not null and p_date_publish_clock is not null) then
				v_date_publish := to_date(p_date_publish_date||'-'||p_date_publish_clock, c_dateFormat_ymd||'-'||c_dateFormat_hhmi);
			else
				v_date_publish := null;
			end if;

			-- set start date to be equal publish date if start date is null
			if (p_date_start_date is not null and p_date_start_clock is not null) then
				v_date_start := to_date(p_date_start_date||'-'||p_date_start_clock, c_dateFormat_ymd||'-'||c_dateFormat_hhmi);
			elsif (v_date_publish is not null) then
				v_date_start := v_date_publish;
			end if;

			-- -----------------------
			-- insert new content
			-- -----------------------
			v_subject		:= htm_lib.HTMLEncode(p_subject);
			v_ingress		:= htm_lib.HTMLEncode(p_ingress);
			v_footer		:= htm_lib.HTMLEncode(p_footer);
			v_url_name_1	:= htm_lib.HTMLEncode(p_url_name_1);
			v_url_name_2	:= htm_lib.HTMLEncode(p_url_name_2);
			v_url_name_3	:= htm_lib.HTMLEncode(p_url_name_3);

			-- if this is a new insert we set status to preview
			if (v_count2 > 0 or p_check_preview = 1) then
				v_tmp := 'preview';
			-- else if update we use published (we don't go to preview mode, this to avoid loss)
			else
				v_tmp := 'published';
			end if;

			insert into con_content
			(con_content_pk, subject, ingress, body, footer, rating, org_organisation_fk, date_start, date_stop, date_publish, con_type_fk, con_subtype_fk, con_status_fk, date_insert, date_update)
			values(
				  p_content_pk
				, v_subject
				, v_ingress
				, p_body
				, v_footer
				, p_rating
				, c_organisation_pk
				, nvl(v_date_start, sysdate)										-- if no startdate is specified we use sysdate
				, v_date_stop
				, v_date_publish													-- when to publish the news
				, p_con_type_pk
				, p_con_subtype_pk
				, (select con_status_pk from con_status where name = v_tmp)
				, nvl(v_date_insert, sysdate)
				, sysdate
			);

			-- --------------------------------
			-- relate data to content again
			-- --------------------------------
			update	con_content_file
			set		con_content_fk_pk = p_content_pk
			where	con_content_fk_pk = -180876;

			update	con_ser
			set		con_content_fk_pk = p_content_pk
			where	con_content_fk_pk = -180876;

			update	con_comment
			set		con_content_fk = p_content_pk
			where	con_content_fk = -180876;

			update	con_vote
			set		con_content_fk = p_content_pk
			where	con_content_fk = -180876;

			update	con_relation
			set		con_content_2_fk_pk = -180876
			where	con_content_2_fk_pk = p_content_pk;


			-- ----------------------------------------------
			-- if service was null from before we set it
			-- to the service the user is using now
			-- ----------------------------------------------
			select	count(*)
			into	v_count
			from	con_ser
			where	con_content_fk_pk = p_content_pk;
			
			if (v_count = 0) then
				insert into con_ser
				(con_content_fk_pk, ser_service_fk_pk)
				values(p_content_pk, c_service_pk);
			end if;

			-- ----------------------
			-- delete dummy entry
			-- ----------------------
			delete from con_content
			where con_content_pk = -180876;

			-- -----------------
			-- insert URLs
			-- -----------------
			if (p_url_1 is not null) then
				v_pk := sys_lib.getEmptyPk('con_url');

				v_url := p_url_1;
				v_url := replace(v_url, 'http://', '');

				insert into con_url
				(con_url_pk, con_content_fk, url, name, date_insert)
				values(
					  v_pk
					, p_content_pk
					, v_url
					, v_url_name_1
					, sysdate
				);
			end if;

			if (p_url_2 is not null) then
				v_pk := sys_lib.getEmptyPk('con_url');

				v_url := p_url_2;
				v_url := replace(v_url, 'http://', '');

				insert into con_url
				(con_url_pk, con_content_fk, url, name, date_insert)
				values(
					  v_pk
					, p_content_pk
					, v_url
					, v_url_name_2
					, sysdate
				);
			end if;

			if (p_url_3 is not null) then
				v_pk := sys_lib.getEmptyPk('con_url');

				v_url := p_url_3;
				v_url := replace(v_url, 'http://', '');

				insert into con_url
				(con_url_pk, con_content_fk, url, name, date_insert)
				values(
					  v_pk
					, p_content_pk
					, v_url
					, v_url_name_3
					, sysdate
				);
			end if;

			-- ---------------------------
			-- insert related content
			-- ---------------------------
			if (p_content_pk_1 is not null) then
				insert into con_relation
				(con_content_1_fk_pk, con_content_2_fk_pk, date_insert)
				values(p_content_pk, p_content_pk_1, sysdate);
			end if;

			if (
				(p_content_pk_2 is not null and p_content_pk_1 is null)
				or
				(p_content_pk_2 is not null and p_content_pk_2 != p_content_pk_1)
			) then
				insert into con_relation
				(con_content_1_fk_pk, con_content_2_fk_pk, date_insert)
				values(p_content_pk, p_content_pk_2, sysdate);
			end if;

			if (
				(p_content_pk_3 is not null and p_content_pk_1 is null and p_content_pk_2 is null)
				or
				(p_content_pk_3 is not null and p_content_pk_2 is null and p_content_pk_3 != p_content_pk_1)
				or
				(p_content_pk_3 is not null and p_content_pk_1 is null and p_content_pk_3 != p_content_pk_2)
				or
				(p_content_pk_3 is not null and p_content_pk_3 != p_content_pk_1 and p_content_pk_3 != p_content_pk_2)
			) then
				insert into con_relation
				(con_content_1_fk_pk, con_content_2_fk_pk, date_insert)
				values(p_content_pk, p_content_pk_3, sysdate);
			end if;

			commit;
		end if;

		-- add picture
		if (p_check_pic = '1') then
			v_html := v_html||htm.jumpTo(g_package||'.'||c_procedure||'?p_action=add_picture&amp;'||v_query_string , 1, c_tex_pleaseWait);

		-- delete picture
		elsif (p_check_pic = '2') then
			v_html := v_html||htm.jumpTo(g_package||'.'||c_procedure||'?p_action=delete_picture&amp;'||v_query_string , 1, c_tex_pleaseWait);

		-- show preview
		elsif (p_check_preview = '1') then
			v_html := v_html||htm.jumpTo(g_package||'.'||c_procedure||'?p_action=preview&amp;'||v_query_string , 1, c_tex_pleaseWait);

		-- repeat event
		elsif (p_check_repeat = '1') then
			v_html := v_html||htm.jumpTo(g_package||'.'||c_procedure||'?p_action=repeat&amp;'||v_query_string , 1, c_tex_pleaseWait);

		-- publish
		else
			v_html := v_html||htm.jumpTo(g_package||'.'||c_procedure||'?p_action=publish&amp;'||v_query_string , 1, c_tex_pleaseWait);
		end if;

	-- ---------------------------
	-- add_picture
	-- ---------------------------
	elsif (p_action = 'add_picture') then

		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundAdminBox')||'<tr><td>'||chr(13);

		v_html := v_html||'
			<SCRIPT type="text/javascript" language="JavaScript">
				function checkFields_uploadForm(){
					var p_file_1	= document.getElementById(''p_file_1'');

					if(p_file_1.value.length == 0){
						alert('''||c_text_emptyPath||''');
						return false;
					} else {
						return true;
					}
				}
			</SCRIPT>
		';

		v_html := v_html||''||htm.bForm('htm_lib.upload" enctype="multipart/form-data', 'upload_form')||chr(13);
		v_html := v_html||''||htm.bBox(c_tex_addContent)||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);

		-- -------------------------------------------------------------
		-- hidden values that has to be sent to the upload module
		-- -------------------------------------------------------------
		v_html := v_html||'<input type="hidden" name="p_type" value="image" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_url_success" value="'||g_package||'.'||c_procedure||'?p_action=insert_picture&[parameters]" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_url_failure" value="'||g_package||'.'||c_procedure||'?p_action=failed_pic&[parameters]" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_trg_directory" value="'||c_dir_img_org||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_trg_filename" value="'||v_con_pic||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_max_file_size" value="'||c_max_size||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_max_width" value="'||c_max_width||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_max_height" value="'||c_max_height||'" />'||chr(13);

		-- -------------------------------------------------------------
		-- other hidden values
		-- -------------------------------------------------------------
		v_html := v_html||'<input type="hidden" name="p_content_pk" value="'||v_content_pk||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_check_repeat" value="'||p_check_repeat||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_check_subscription" value="'||p_check_subscription||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_check_preview" value="'||p_check_preview||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_check_addNewContent" value="'||p_check_addNewContent||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_con_type_pk" value="'||p_con_type_pk||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_mode" value="'||v_mode||'" />'||chr(13);

		-- -----------
		-- picture
		-- -----------
		v_html := v_html||'<tr><td>'||c_tex_pictureFile||'</td><td colspan="2"><input type="file" size="60" name="p_file_1" id="p_file_1" /></td></tr>'||chr(13);

		-- ------------------
		-- submit button
		-- ------------------
		v_html := v_html||'<tr><td colspan="3" style="padding-left: 20px;">'||htm.submitLink(c_tex_addPicture, 'upload_form', 'if (checkFields_uploadForm()) document.upload_form.submit()')||'</td></tr>'||chr(13);
		v_html := v_html||'<tr><td colspan="3" style="padding-left: 20px;">'||htm.historyBack(c_tex_goBack)||'</td></tr>'||chr(13);

		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);
		v_html := v_html||''||htm.eForm||chr(13);

		v_html := v_html||'</td></tr>'||htm.eTable||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

	-- ---------------------------
	-- preview
	-- ---------------------------
	elsif (p_action = 'preview') then

		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);

		v_html := v_html||''||htm.link(c_tex_publish, g_package||'.'||c_procedure||'?p_action=publish&amp;'||v_query_string, '_self', 'theButtonStyle')||chr(13);
		v_html := v_html||'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'||chr(13);
		v_html := v_html||''||htm.link(c_tex_update, g_package||'.'||c_procedure||'?p_action=form&amp;'||v_query_string, '_self', 'theButtonStyle')||chr(13);
		v_html := v_html||'<br><br><br>'||chr(13);

		v_html := v_html ||''||con_lib.viewCon(p_content_pk)||chr(13);

		v_html := v_html||'<br><br><br>'||chr(13);
		v_html := v_html||''||htm.link(c_tex_publish, g_package||'.'||c_procedure||'?p_action=publish&amp;'||v_query_string, '_self', 'theButtonStyle')||chr(13);
		v_html := v_html||'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'||chr(13);
		v_html := v_html||''||htm.link(c_tex_update, g_package||'.'||c_procedure||'?p_action=form&amp;'||v_query_string, '_self', 'theButtonStyle')||chr(13);

		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

	-- ---------------------------
	-- publish
	-- ---------------------------
	elsif (p_action = 'publish') then

		if (p_content_pk is not null) then
			update	con_content
			set		con_status_fk = (select con_status_pk from con_status where name = 'published')
				,	date_update = sysdate
			where	con_content_pk = p_content_pk
			and		org_organisation_fk = c_organisation_pk;

			commit;
		end if;

		-- repeat event
		if (p_check_repeat = '1') then
			v_html := v_html||htm.jumpTo(g_package||'.'||c_procedure||'?p_action=repeat&amp;'||v_query_string , 1, c_tex_pleaseWait);

		-- add new content
		elsif (p_check_addNewContent = '1') then
			v_html := v_html||htm.jumpTo(g_package||'.'||c_procedure||'?p_action=form&amp;p_con_type_pk='||p_con_type_pk , 1, c_tex_pleaseWait);

		-- back to admin page
		else
			v_html := v_html||htm.jumpTo('org_adm.viewOrg' , 1, c_tex_pleaseWait);
		end if;

	-- ---------------------------
	-- insert_picture
	-- ---------------------------
	elsif (p_action = 'insert_picture') then

		if (p_content_pk is not null) then
			-- -------------------------------------------
			-- delete all files related to con_content
			--
			-- NOTE: This code has to be changed if we later
			-- want to be able to relate more than one file!
			-- -------------------------------------------
  			delete from con_file
			where con_file_pk in (
				select	con_file_fk_pk
				from	con_content_file
				where	con_content_fk_pk = p_content_pk
			);
			
			-- ------------------------------------------------
			-- delete files that aren't related to any content
			-- ------------------------------------------------
			delete from con_file
			where con_file_pk not in (
				select con_file_fk_pk
				from con_content_file
			);

			-- get new pk
			v_pk := sys_lib.getEmptyPk('con_file');

			-- select name of file that just was uploaded
			for r_cursor in cur_log_upload(c_id)
			loop
				v_con_pic := r_cursor.trg_filename;
			end loop;

			insert into con_file
			(con_file_pk, name, path, con_file_type_fk, date_insert)
			values(
				  v_pk
				, 'content_photo'
				, v_con_pic
				, (select con_file_type_pk from con_file_type where lower(name) = 'jpg')
				, sysdate
			);

			insert into con_content_file
			(con_content_fk_pk, con_file_fk_pk)
			values(p_content_pk, v_pk);

			commit;
		end if;

		-- show preview
		if (p_check_preview = '1') then
			v_html := v_html||htm.jumpTo(g_package||'.'||c_procedure||'?p_action=preview&amp;'||v_query_string , 1, c_tex_pleaseWait);

		-- repeat event
		elsif (p_check_repeat = '1') then
			v_html := v_html||htm.jumpTo(g_package||'.'||c_procedure||'?p_action=repeat&amp;'||v_query_string , 1, c_tex_pleaseWait);

		-- publish
		else
			v_html := v_html||htm.jumpTo(g_package||'.'||c_procedure||'?p_action=publish&amp;'||v_query_string , 1, c_tex_pleaseWait);
		end if;

	-- ---------------------------
	-- failed_pic
	-- ---------------------------
	elsif (p_action = 'failed_pic') then

		v_html := v_html||htm.jumpTo(g_package||'.'||c_procedure||'?p_action=form&amp;'||v_query_string , 1, c_tex_picUploadFailed);

	-- ---------------------------
	-- delete_picture
	-- ---------------------------
	elsif (p_action = 'delete_picture') then

		if (p_content_pk is not null) then
			-- -------------------------------------------
			-- delete all files related to con_content
			--
			-- NOTE: This code has to be changed if we later
			-- want to be able to relate more than one file!
			-- -------------------------------------------
			delete from con_file
			where con_file_pk in (
				select	con_file_fk_pk
				from	con_content_file
				where	con_content_fk_pk = p_content_pk
			);

			commit;
		end if;

		-- show preview
		if (p_check_preview = '1') then
			v_html := v_html||htm.jumpTo(g_package||'.'||c_procedure||'?p_action=preview&amp;'||v_query_string , 1, c_tex_pleaseWait);

		-- repeat event
		elsif (p_check_repeat = '1') then
			v_html := v_html||htm.jumpTo(g_package||'.'||c_procedure||'?p_action=repeat&amp;'||v_query_string , 1, c_tex_pleaseWait);

		-- publish
		else
			v_html := v_html||htm.jumpTo(g_package||'.'||c_procedure||'?p_action=publish&amp;'||v_query_string , 1, c_tex_pleaseWait);
		end if;

	-- ---------------------------
	-- setDefaultValues
	-- ---------------------------
	elsif (p_action = 'setDefaultValues') then

		-- try to get values from the browser
		v_tmp := htm_lib.getCookie('in4mant_defaultConValues');

		v_date_start_clock_def := substr(v_tmp, 1, instr(v_tmp, '-')-1);
		v_con_subtype_pk_def := substr(v_tmp, instr(v_tmp, '-')+1, length(v_tmp));

		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);
		v_html := v_html||''||htm.bTable('surroundAdminBox')||'<tr><td>'||chr(13);

		v_html := v_html||'
			<SCRIPT type="text/javascript" language="JavaScript">
				function checkFields_updateForm(){
					var p_date_start_clock;

					if (document.updateForm.p_date_start_clock){
						p_date_start_clock		= document.getElementById(''p_date_start_clock'');

						if (p_date_start_clock.value.length == 0 || !verifyTime(p_date_start_clock.value)){
							alert("'||c_tex_wrongDateStartClock||': '||c_dateFormat_hhmi||'");
							return;
						}

						p_date_start_clock.value	= verifyTime(p_date_start_clock.value);
					}

					document.updateForm.submit()
				}
			</SCRIPT>
		';

		v_html := v_html||''||htm.bForm(g_package||'.'||c_procedure, 'updateForm')||chr(13);
		v_html := v_html||''||htm.bBox(c_text_setDefaultValues)||chr(13);
		v_html := v_html||''||htm.bTable||chr(13);

		v_html := v_html||'<input type="hidden" name="p_content_pk" value="'||v_content_pk||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_con_type_pk" value="'||p_con_type_pk||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_mode" value="'||v_mode||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_check_repeat" value="'||p_check_repeat||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_check_subscription" value="'||p_check_subscription||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_check_preview" value="'||p_check_preview||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_action" value="setDefaultValues_action" />'||chr(13);

		-- -----------
		-- start time
		-- -----------
		v_html := v_html||'<tr><td width="20%">'||c_tex_startTime||':</td><td>'||c_tex_clock||':<br />('||c_dateFormat_hhmi||')<br /><input type="text" name="p_date_start_clock" id="p_date_start_clock" size="5" maxlength="5" value="'||v_date_start_clock_def||'" /></td></tr>'||chr(13);

		-- -----------
		-- subtype
		-- -----------
		v_html := v_html||'<tr><td>'||c_tex_subtype||':</td><td>'||htm_lib.selectConSubtype(v_con_subtype_pk_def)||'</td></tr>'||chr(13);

		-- ------------------
		-- submit button
		-- ------------------
		v_html := v_html||'<tr><td colspan="2" style="padding-left: 20px;">'||htm.submitLink(c_text_setDefaultValues, 'form', 'checkFields_updateForm()')||'</td></tr>'||chr(13);

		v_html := v_html||''||htm.eTable||chr(13);
		v_html := v_html||''||htm.eBox||chr(13);
		v_html := v_html||''||htm.eForm||chr(13);

		v_html := v_html||'</td></tr>'||htm.eTable||chr(13);
		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

	-- ---------------------------
	-- setDefaultValues
	-- ---------------------------
	elsif (p_action = 'setDefaultValues_action') then

		-- set cookie
		htm_lib.setCookie('in4mant_defaultConValues', p_date_start_clock||'-'||p_con_subtype_pk);

		v_html := v_html||htm.jumpTo(g_package||'.'||c_procedure||'?p_action=form&amp;'||v_query_string , 1, c_tex_pleaseWait);

	-- ---------------------------
	-- repeat
	-- ---------------------------
	elsif (p_action = 'repeat') then


		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);

		v_html := v_html||''||htm.bForm(g_package||'.'||c_procedure)||chr(13);
		v_html := v_html||''||htm.bBox('[repeat_event]')||chr(13);

		-- -------------------------------------------------------------
		-- other hidden values
		-- -------------------------------------------------------------
		v_html := v_html||'<input type="hidden" name="p_content_pk" value="'||v_content_pk||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_check_repeat" value="'||p_check_repeat||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_check_subscription" value="'||p_check_subscription||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_check_preview" value="'||p_check_preview||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_mode" value="'||v_mode||'" />'||chr(13);
		v_html := v_html||'<input type="hidden" name="p_action" value="repeat_action" />'||chr(13);

		-- ------------------
		-- submit button
		-- ------------------
		v_html := v_html||'<tr><td colspan="3" style="padding-left: 20px;">'||htm.submitLink('[do something]', 'form')||'</td></tr>'||chr(13);

		v_html := v_html||''||htm.eBox||chr(13);
		v_html := v_html||''||htm.eForm||chr(13);

		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

	-- ---------------------------
	-- repeat_action
	-- ---------------------------
	elsif (p_action = 'repeat_action') then

		htp.p('repeat_action');

	-- ---------------------------
	-- delete
	-- ---------------------------
	elsif (p_action = 'delete') then

		v_html := v_html||''||htm.bPage(g_package, c_procedure, 'admin')||chr(13);
		v_html := v_html||'<center>'||chr(13);

		v_html := v_html||''||htm.link(c_text_delete, g_package||'.'||c_procedure||'?p_action=delete_action&amp;'||v_query_string, '_self', 'theButtonStyle')||chr(13);
		v_html := v_html||'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'||chr(13);
		v_html := v_html||''||htm.link(c_text_doNotDelete, owa_util.get_cgi_env('http_referer'), '_self', 'theButtonStyle')||chr(13);
		v_html := v_html||'<br /><br /><br />'||chr(13);

		v_html := v_html ||''||con_lib.viewCon(p_content_pk)||chr(13);

		v_html := v_html||'<br /><br /><br />'||chr(13);
		v_html := v_html||''||htm.link(c_text_delete, g_package||'.'||c_procedure||'?p_action=delete_action&amp;'||v_query_string, '_self', 'theButtonStyle')||chr(13);
		v_html := v_html||'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'||chr(13);
		v_html := v_html||''||htm.link(c_text_doNotDelete, owa_util.get_cgi_env('http_referer'), '_self', 'theButtonStyle')||chr(13);

		v_html := v_html||'</center>'||chr(13);
		v_html := v_html||''||htm.ePage||chr(13);

	-- ---------------------------
	-- delete_action
	-- ---------------------------
	elsif (p_action = 'delete_action') then

		update	con_content
		set		con_status_fk = (select con_status_pk from con_status where name = 'removed')
			,	date_update = sysdate
		where	con_content_pk = p_content_pk;

		v_html := v_html||htm.jumpTo('org_adm.viewOrg' , 1, c_tex_pleaseWait);

	-- ---------------------------
	-- login
	-- ---------------------------
	elsif (c_organisation_pk is null) then

		v_html := v_html||htm.jumpTo('pag_pub.userLogin', 1, '...');

	end if;

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
	when e_stop then
		return null;
	when e_return then
		return v_html;
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		return null;
end;
end admContent;


---------------------------------------------------------
-- name:        conOverview
-- what:        content admin overview
-- author:      Frode Klevstul
-- start date:  20.01.2006
---------------------------------------------------------
procedure conOverview(p_con_type_pk con_type.con_type_pk%type default null) is begin
	ext_lob.pri(con_adm.conOverview(p_con_type_pk));
end;

function conOverview
(
	p_con_type_pk			con_type.con_type_pk%type				default null
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type					:= 'conOverview';

	c_id						constant ses_session.id%type 						:= ses_lib.getId;
	c_organisation_pk			constant org_organisation.org_organisation_pk%type	:= ses_lib.getOrgPk(c_id);

	c_tex_addContent			constant tex_text.string%type						:= tex_lib.get(g_package, c_procedure, 'addContent');

	v_length					number;
	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_con_type_pk='||p_con_type_pk);
	ext_lob.ini(v_clob);

	use_lib.loggedInCheck(p_url => g_package||'.'||c_procedure||'?p_con_type_pk='||p_con_type_pk);

	v_html := v_html||''||htm.bPage(g_package, c_procedure, 'admin')||chr(13);
	v_html := v_html||'<center>'||chr(13);
	v_html := v_html||''||htm.bTable('surroundElements')||chr(13);
	v_html := v_html||'<tr><td style="text-align: center;">'||chr(13);

	v_html := v_html||''||htm.link(c_tex_addContent,'con_adm.admContent?p_action=form&amp;p_con_type_pk='||p_con_type_pk, '_self', 'theButtonStyle')||chr(13);
	v_html := v_html||'</td></tr><tr><td>'||chr(13);

	v_length := length(v_html);

	v_html := v_html||''||
	con_lib.listCon(
		  p_noFirst				=> 0																-- 0 here, in stead of null, will prevent the layout (bPage, ePage) to be printed
		, p_noOnOnePage			=> 30
		, p_noShowFrom			=> null
		, p_subjectMaxLength	=> 50
		, p_editMode			=> 2																-- when p_editMode = 2 the update and delete button will show
		, p_organisation_pk		=> c_organisation_pk
		, p_con_type_pk			=> p_con_type_pk
		, p_con_subtype_pk		=> null
		, p_date_start			=> null
		, p_date_stop			=> null
	);

	if ( length(v_html) > v_length + 1000 ) then
		v_html := v_html||'</td></tr><tr><td style="text-align: center;">'||htm.link(c_tex_addContent,'con_adm.admContent?p_action=form&amp;p_con_type_pk='||p_con_type_pk, '_self', 'theButtonStyle')||chr(13);
	end if;

	v_html := v_html||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||'</center>'||chr(13);
	v_html := v_html||''||htm.ePage||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		return null;
end;
end conOverview;



-- ******************************************** --
end;
/
show errors;
