PROMPT *** tes_tes ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package tes_tes is
/* ******************************************************
*	Package:     tes_tes
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	061020 | Frode Klevstul
*			- Package created
****************************************************** */
	procedure run;
	procedure javaTest
	(
		  p_number			in int						default 0
	);
	function javaTest
	(
		  p_number			in int						default 0
	) return clob;
	function theJava
	(
	numberIn in BINARY_INTEGER --NUMBER PLS_INTEGER
	) return number;

end;
/
show errors;





-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body tes_tes is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'tes_tes';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  23.07.2005
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        javaTest
-- what:        Test java code
-- author:      Frode Klevstul
-- start date:  20.10.2006
---------------------------------------------------------
procedure javaTest
(
	  p_number			in int						default 0
) is begin
    htp.p(tes_tes.javaTest(p_number));
end;

function javaTest
(
	  p_number			in int						default 0
) return clob
is
begin
declare
    c_procedure constant mod_procedure.name%type := 'javaTest';

    v_html						clob;

begin
    ses_lib.ini(g_package, c_procedure, 'p_number='||p_number);

	v_html := 'test : '||p_number||' + 1 = '||theJava(p_number);

    return v_html;

exception
    when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
        return null;
end;
end javaTest;



function theJava
(
numberIn in BINARY_INTEGER --NUMBER PLS_INTEGER
) return number
as language java
name 'javaTest.plusOne(int)return int';



-- ******************************************** --
end;
/
show errors;
