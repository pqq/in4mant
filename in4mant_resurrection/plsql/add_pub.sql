PROMPT *** add_pub ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package add_pub is
/* ******************************************************
*	Package:     add_pub
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	060809 | Frode Klevstul
*			- Package created
****************************************************** */
	type parameter_arr 			is table of varchar2(4000) index by binary_integer;
	empty		 				parameter_arr;

	procedure run;
	procedure viewMap
	(
		  p_address_pk			in parameter_arr								default empty
		, p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	);
	function viewMap
	(
		  p_address_pk			in parameter_arr								default empty
		, p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
	) return clob;

end;
/
show errors;





-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body add_pub is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'add_pub';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  09.08.2006
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);
	
	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        viewMap
-- what:        view map
-- author:      Frode Klevstul
-- start date:  09.08.2006
---------------------------------------------------------
procedure viewMap
(
	  p_address_pk			in parameter_arr								default empty
	, p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
) is begin
	ext_lob.pri(add_pub.viewMap(p_address_pk, p_organisation_pk));
end;

function viewMap
(
	  p_address_pk			in parameter_arr								default empty
	, p_organisation_pk		in org_organisation.org_organisation_pk%type	default null
) return clob
is
begin
declare
	c_procedure				constant mod_procedure.name%type				:= 'viewMap';

	c_tex_orgPage			constant tex_text.string%type					:= tex_lib.get(g_package, c_procedure, 'orgPage');

	cursor	cur_con_content is
	select	con_content_pk, subject, ingress, body, footer, org_organisation_fk, name, path_logo
	from	con_content, org_organisation
	where	org_organisation_pk = org_organisation_fk;

	v_html					clob;
	v_clob					clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_address_pk='||p_address_pk(1)||'��p_organisation_pk='||p_organisation_pk);
	ext_lob.ini(v_clob);

	v_html := v_html||''||htm.bPage(g_package, c_procedure)||chr(13);
	v_html := v_html||'<center>'||chr(13);
	v_html := v_html||''||htm.bTable('surroundElements')||chr(13);
	v_html := v_html||'<tr><td class="surroundElements">'||chr(13);
	
	if (p_organisation_pk is null) then
		v_html := v_html||''||add_lib.getMap(p_address_pk(1), 910, 600, null)||chr(13);
	else
		v_html := v_html||''||htm.bTable||chr(13);
		v_html := v_html||'<tr><td width="20%" class="surroundElements">'||chr(13);
		v_html := v_html||''||org_lib.contactInfo(p_organisation_pk)||chr(13);
		v_html := v_html||'</td><td style="text-align: center;">'||chr(13);
		v_html := v_html||''||add_lib.getMap(p_address_pk(1), 720, 600, org_lib.getOrgName(p_organisation_pk))||chr(13);
		v_html := v_html||'[<a href="org_pub.viewOrg?p_organisation_pk='||p_organisation_pk||'"> '||c_tex_orgPage||' </a>]'||chr(13);
		v_html := v_html||'</td></tr>'||chr(13);
		v_html := v_html||''||htm.eTable||chr(13);
	end if;

	v_html := v_html||'</td></tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);
	v_html := v_html||'</center>'||chr(13);
	v_html := v_html||''||htm.ePage||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		return null;
end;
end viewMap;


-- ******************************************** --
end;
/
show errors;
