PROMPT *** pic_pub ***
set define off
set serveroutput on;
alter session set plsql_warnings='disable:all';





-- ******************************************************
-- *                                         			*
-- *             		package head           			*
-- *                                          			*
-- ******************************************************
create or replace package pic_pub is
/* ******************************************************
*	Package:     pic_pub
*
*	- - - - Revision | Newest version at the top  - - - -
*
*	date:	060708 | Frode Klevstul
*			- Package created
****************************************************** */
	procedure run;
	procedure viewAlbum
	(
		  p_organisation_pk			org_organisation.org_organisation_pk%type			default null
		, p_album_pk				pic_album.pic_album_pk%type							default null

	);
	function viewAlbum
	(
		  p_organisation_pk			org_organisation.org_organisation_pk%type			default null
		, p_album_pk				pic_album.pic_album_pk%type							default null

	) return clob;
	procedure viewPicture
	(
		  p_organisation_pk			org_organisation.org_organisation_pk%type			default null
		, p_album_pk				pic_album.pic_album_pk%type							default null
		, p_picture_pk				pic_picture.pic_picture_pk%type						default null
		, p_slideshow				number												default null
	);
	function viewPicture
	(
		  p_organisation_pk			org_organisation.org_organisation_pk%type			default null
		, p_album_pk				pic_album.pic_album_pk%type							default null
		, p_picture_pk				pic_picture.pic_picture_pk%type						default null
		, p_slideshow				number												default null
	) return clob;
	procedure viewAlbums;
	function viewAlbums
		return clob;

end;
/
show errors;





-- ******************************************************
-- *                                         			*
-- *             		package body           			*
-- *                                          			*
-- ******************************************************
create or replace package body pic_pub is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
g_package	constant mod_package.name%type := 'pic_pub';


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- local procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- public procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        run
-- what:        startup procedure
-- author:      Frode Klevstul
-- start date:  08.07.2006
---------------------------------------------------------
procedure run
is
begin
declare
	c_procedure		constant mod_procedure.name%type := 'run';
begin
	ses_lib.ini(g_package, c_procedure, null);

	htp.p(g_package||'.'||c_procedure);
exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		null;
end;
end run;


---------------------------------------------------------
-- name:        viewAlbum
-- what:        view album
-- author:      Frode Klevstul
-- start date:  08.07.2006
---------------------------------------------------------
procedure viewAlbum
(
	  p_organisation_pk			org_organisation.org_organisation_pk%type			default null
	, p_album_pk				pic_album.pic_album_pk%type							default null
) is begin
	ext_lob.pri(pic_pub.viewAlbum(p_organisation_pk, p_album_pk));
end;

function viewAlbum
(
	  p_organisation_pk			org_organisation.org_organisation_pk%type			default null
	, p_album_pk				pic_album.pic_album_pk%type							default null
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type					:= 'viewAlbum';

	v_count						number;

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_organisation_pk='||p_organisation_pk||'��p_album_pk='||p_album_pk);
	ext_lob.ini(v_clob);

	-- ------------------------------
	-- check that PK's are valid
	-- ------------------------------
	if (p_album_pk is not null) then
		select	count(*)
		into	v_count
		from	pic_album
		where	pic_album_pk = p_album_pk
		and		org_organisation_fk = p_organisation_pk;
		if (v_count = 0) then
			v_html := v_html||htm.jumpTo('pag_pub.frontPage', 1, '...');
			return v_html;
		end if;
	end if;

	v_html := v_html||''||htm.bPage(g_package, c_procedure)||chr(13);
	v_html := v_html||'<center>'||chr(13);

	v_html := v_html||''||htm.bTable('surroundElements')||'
		<tr>
			<td width="16%" height="40%" class="surroundElements">
			'||org_lib.contactInfo(p_organisation_pk)||'
			</td>
			<td rowspan="2" class="surroundElements">'||chr(13);
	ext_lob.add(v_clob, v_html);
	v_html := v_html||''||pic_lib.pictureIndex(p_album_pk);
	v_html := v_html||'
			</td>
		</tr>
		<tr>
			<td class="surroundElements">
		';
	ext_lob.add(v_clob, v_html);
	v_html := v_html||''||pic_lib.albumIndex(p_organisation_pk, p_album_pk);
	v_html := v_html||'
			</td>
		</tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);

	v_html := v_html||'</center>'||chr(13);
	v_html := v_html||''||htm.ePage||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		return null;
end;
end viewAlbum;


---------------------------------------------------------
-- name:        viewPicture
-- what:        view picture
-- author:      Frode Klevstul
-- start date:  11.07.2006
---------------------------------------------------------
procedure viewPicture
(
	  p_organisation_pk			org_organisation.org_organisation_pk%type			default null
	, p_album_pk				pic_album.pic_album_pk%type							default null
	, p_picture_pk				pic_picture.pic_picture_pk%type						default null
	, p_slideshow				number												default null
) is begin
	ext_lob.pri(pic_pub.viewPicture(p_organisation_pk, p_album_pk, p_picture_pk, p_slideshow));
end;

function viewPicture
(
	  p_organisation_pk			org_organisation.org_organisation_pk%type			default null
	, p_album_pk				pic_album.pic_album_pk%type							default null
	, p_picture_pk				pic_picture.pic_picture_pk%type						default null
	, p_slideshow				number												default null
) return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type					:= 'viewPicture';

	v_count						number;

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, 'p_organisation_pk='||p_organisation_pk||'��p_album_pk='||p_album_pk||'��p_picture_pk='||p_picture_pk);
	ext_lob.ini(v_clob);

	-- ------------------------------
	-- check that PK's are valid
	-- ------------------------------
	select	count(*)
	into	v_count
	from	pic_album, pic_album_picture
	where	pic_album_pk = p_album_pk
	and		org_organisation_fk = p_organisation_pk
	and		pic_picture_fk_pk = p_picture_pk;
	if (v_count = 0) then
		v_html := v_html||htm.jumpTo('pag_pub.frontPage', 1, '...');
		return v_html;
	end if;

	v_html := v_html||''||htm.bPage(g_package, c_procedure)||chr(13);
	v_html := v_html||'<center>'||chr(13);

	v_html := v_html||''||htm.bTable('surroundElements')||'
		<tr>
			<td width="16%" height="40%" class="surroundElements">
			'||org_lib.contactInfo(p_organisation_pk)||'
			</td>
			<td rowspan="2" class="surroundElements">'||chr(13);
	ext_lob.add(v_clob, v_html);
	v_html := v_html||pic_lib.viewPicture(p_picture_pk, p_slideshow);
	v_html := v_html||'
			</td>
		</tr>
		<tr>
			<td class="surroundElements">
		';
	ext_lob.add(v_clob, v_html);
	v_html := v_html||pic_lib.albumIndex(p_organisation_pk, p_album_pk);
	v_html := v_html||'
			</td>
		</tr>'||chr(13);
	v_html := v_html||''||htm.eTable||chr(13);

	v_html := v_html||'</center>'||chr(13);
	v_html := v_html||''||htm.ePage||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		return null;
end;
end viewPicture;


---------------------------------------------------------
-- name:        viewAlbums
-- what:        view all albums
-- author:      Frode Klevstul
-- start date:  08.02.2007
---------------------------------------------------------
procedure viewAlbums is begin
	ext_lob.pri(pic_pub.viewAlbums);
end;

function viewAlbums
	return clob
is
begin
declare
	c_procedure					constant mod_procedure.name%type					:= 'viewAlbums';

	v_html						clob;
	v_clob						clob;

begin
	ses_lib.ini(g_package, c_procedure, null);
	ext_lob.ini(v_clob);


	v_html := v_html||''||htm.bPage(g_package, c_procedure)||chr(13);
	v_html := v_html||'<center>'||chr(13);
	v_html := v_html||''||htm.bTable('surroundElements')||'<tr><td>';

	v_html := v_html||pic_lib.latestPictures(true);

	v_html := v_html||'</td></tr>'||htm.eTable||chr(13);
	v_html := v_html||'</center>'||chr(13);
	v_html := v_html||''||htm.ePage||chr(13);

	ext_lob.add(v_clob, v_html);
	return v_clob;

exception
	when others then
		sys_lib.exceptionHandle(sqlcode, sqlerrm);
		return null;
end;
end viewAlbums;


-- ******************************************** --
end;
/
show errors;
