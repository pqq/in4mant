/*==============================================================*/
/* Database name:  I4_RESURRECTION                              */
/* DBMS name:      ORACLE Version 10g                           */
/* Created on:     19/10/2006 6:09:53 PM                        */
/*==============================================================*/


alter table ADD_ADDRESS
   drop constraint FK_ADD_ADDR_REFERENCE_GEO_GEOG;

alter table BIL_ITEM
   drop constraint FK_BIL_ITEM_REFERENCE_BIL_TRAD;

alter table BIL_ITEM
   drop constraint FK_BIL_ITEM_REFERENCE_BIL_ITEM;

alter table BIL_ITEM
   drop constraint FK_BIL_ITEM_REFERENCE_BIL_PAYM;

alter table BIL_ITEM
   drop constraint FK_BIL_ITEM_REFERENCE_USE_USER;

alter table COM_ORG
   drop constraint FK_COM_ORG_REFERENCE_COM_COMP;

alter table COM_ORG
   drop constraint FK_COM_ORG_REFERENCE_ORG_ORGA;

alter table CON_COMMENT
   drop constraint FK_CON_COMM_REFERENCE_CON_CONT;

alter table CON_COMMENT
   drop constraint FK_CON_COMM_REFERENCE_USE_USER;

alter table CON_CONTENT
   drop constraint FK_CON_CONT_REFERENCE_CON_TYPE;

alter table CON_CONTENT
   drop constraint FK_CON_CONT_REFERENCE_CON_STAT;

alter table CON_CONTENT
   drop constraint FK_CON_CONT_REFERENCE_ORG_ORGA;

alter table CON_CONTENT
   drop constraint FK_CON_CONT_REFERENCE_GEO_GEOG;

alter table CON_CONTENT
   drop constraint FK_CON_CONT_REFERENCE_CON_SUBT;

alter table CON_CONTENT_FILE
   drop constraint FK_CON_CONT_REFERENCE_CON_CONT;

alter table CON_CONTENT_FILE
   drop constraint FK_CON_CONT_REFERENCE_CON_FILE;

alter table CON_FILE
   drop constraint FK_CON_FILE_REFERENCE_CON_FILE;

alter table CON_RELATION
   drop constraint FK_CON_RELA_REFERENCE_CON_CON1;

alter table CON_RELATION
   drop constraint FK_CON_RELA_REFERENCE_CON_CON2;

alter table CON_SER
   drop constraint FK_CON_SER_REFERENCE_CON_CONT;

alter table CON_SER
   drop constraint FK_CON_SER_REFERENCE_SER_SERV;

alter table CON_URL
   drop constraint FK_CON_URL_REFERENCE_CON_CONT;

alter table CON_VOTE
   drop constraint FK_CON_VOTE_REFERENCE_CON_CONT;

alter table CON_VOTE
   drop constraint FK_CON_VOTE_REFERENCE_USE_USER;

alter table FOR_FORUM
   drop constraint FK_FOR_FORU_REFERENCE_FOR_FORU;

alter table FOR_FORUM
   drop constraint FK_FOR_FORU_REFERENCE_FOR_TYPE;

alter table FOR_TYPE
   drop constraint FK_FOR_TYPE_REFERENCE_SER_SERV;

alter table FOR_TYPE
   drop constraint FK_FOR_TYPE_REFERENCE_ORG_ORGA;

alter table FOR_USE
   drop constraint FK_FOR_USE_REFERENCE_FOR_FORU;

alter table FOR_USE
   drop constraint FK_FOR_USE_REFERENCE_USE_USER;

alter table GEO_GEOGRAPHY
   drop constraint FK_GEO_GEOG_REFERENCE_GEO_GEOG;

alter table GEO_ZIP
   drop constraint FK_GEO_ZIP_REFERENCE_GEO_GEOG;

alter table HIS_HISTORY
   drop constraint FK_HIS_HIST_REFERENCE_USE_USER;

alter table LIS_LIST
   drop constraint FK_LIS_LIST_REFERENCE_LIS_TYPE;

alter table LIS_ORG
   drop constraint FK_LIS_ORG_REFERENCE_LIS_LIST;

alter table LIS_ORG
   drop constraint FK_LIS_ORG_REFERENCE_ORG_ORGA;

alter table LOG_CON_EMAIL
   drop constraint FK_LOG_CON__REFERENCE_CON_CONT;

alter table LOG_LOGIN
   drop constraint FK_LOG_LOGI_REFERENCE_USE_USER;

alter table LOG_SUB
   drop constraint FK_LOG_SUB_REFERENCE_CON_CONT;

alter table LOG_SUB
   drop constraint FK_LOG_SUB_REFERENCE_USE_USER;

alter table MAR_STUNT
   drop constraint FK_MAR_STUN_REFERENCE_MAR_STAT;

alter table MAR_STUNT
   drop constraint FK_MAR_STUN_REFERENCE_MAR_TARG;

alter table MAR_STUNT
   drop constraint FK_MAR_STUN_REFERENCE_MAR_STUN;

alter table MAR_STUNT
   drop constraint FK_MAR_STUN_REFERENCE_USE_USER;

alter table MAR_TARGET
   drop constraint FK_MAR_TARG_REFERENCE_ADD_ADDR;

alter table MAR_TARGET
   drop constraint FK_MAR_TARG_REFERENCE_ORG_ORGA;

alter table MAR_TARGET
   drop constraint FK_MAR_TARG_REFERENCE_USE_USER;

alter table MEM_MOD
   drop constraint FK_MEM_MOD_REFERENCE_MOD_MODU;

alter table MEM_MOD
   drop constraint FK_MEM_MOD_REFERENCE_MEM_MEMB;

alter table MEM_ORG
   drop constraint FK_MEM_ORG_REFERENCE_ORG_ORGA;

alter table MEM_ORG
   drop constraint FK_MEM_ORG_REFERENCE_MEM_MEMB;

alter table MEN_MENU
   drop constraint FK_MEN_MENU_REFERENCE_MEN_TYPE;

alter table MEN_MENU
   drop constraint FK_MEN_MENU_REFERENCE_ORG_ORGA;

alter table MEN_OBJECT
   drop constraint FK_MEN_OBJE_REFERENCE_MEN_MENU;

alter table MEN_OBJECT
   drop constraint FK_MEN_OBJE_REFERENCE_MEN_OBJE;

alter table MEN_OBJECT
   drop constraint FK_MEN_OBJE_REFERENCE_MEN_SERV;

alter table MEN_OBJECT_SERVING
   drop constraint FK_MEN_OBJE_REFERENCE_MEN_ORG_;

alter table MEN_OBJECT_SERVING
   drop constraint FK_MEN_OBJE_REFERENCE_MEN_OBJ3;

alter table MEN_OBJECT_TYPE
   drop constraint FK_MEN_OBJE_REFERENCE_MEN_TYPE;

alter table MEN_ORG_SERVING
   drop constraint FK_MEN_ORG__REFERENCE_MEN_SERV;

alter table MEN_ORG_SERVING
   drop constraint FK_MEN_ORG__REFERENCE_ORG_ORGA;

alter table MEN_PRICE
   drop constraint FK_MEN_PRIC_REFERENCE_MEN_OBJ2;

alter table MOD_MODULE
   drop constraint FK_MOD_MODU_REFERENCE_MOD_PROC;

alter table MOD_MODULE
   drop constraint FK_MOD_MODU_REFERENCE_MOD_PACK;

alter table MOD_PERMISSION
   drop constraint FK_MOD_PERM_REFERENCE_MOD_MODU;

alter table MOD_PERMISSION
   drop constraint FK_MOD_PERM_REFERENCE_USE_USER;

alter table MOD_RESTRICTION
   drop constraint FK_MOD_REST_REFERENCE_MOD_MODU;

alter table MOD_RESTRICTION
   drop constraint FK_MOD_REST_REFERENCE_USE_TYPE;

alter table ORG_CHOSEN
   drop constraint FK_ORG_CHOS_REFERENCE_SER_SERV;

alter table ORG_CHOSEN
   drop constraint FK_ORG_CHOS_REFERENCE_ORG_ORGA;

alter table ORG_HOURS
   drop constraint FK_ORG_HOUR_REFERENCE_ORG_ORGA;

alter table ORG_ORGANISATION
   drop constraint FK_ORG_ORGA_REFERENCE_ORG_ORG1;

alter table ORG_ORGANISATION
   drop constraint FK_ORG_ORGA_REFERENCE_ORG_STAT;

alter table ORG_ORGANISATION
   drop constraint FK_ORG_ORGA_REFERENCE_ADD_ADDR;

alter table ORG_ORGANISATION_RIGHTS
   drop constraint FK_ORG_ORGA_REFERENCE_ORG_ORG2;

alter table ORG_ORGANISATION_RIGHTS
   drop constraint FK_ORG_ORGA_REFERENCE_ORG_RIGH;

alter table ORG_ORIGINAL
   drop constraint FK_ORG_ORIG_REFERENCE_ORG_ORGA;

alter table ORG_SER
   drop constraint FK_ORG_SER_REFERENCE_ORG_ORGA;

alter table ORG_SER
   drop constraint FK_ORG_SER_REFERENCE_SER_SERV;

alter table ORG_VALUE
   drop constraint FK_ORG_VALU_REFERENCE_ORG_VALU;

alter table ORG_VALUE
   drop constraint FK_ORG_VALU_REFERENCE_ORG_ORGA;

alter table PIC_ALBUM
   drop constraint FK_PIC_ALBU_REFERENCE_ORG_ORGA;

alter table PIC_ALBUM_PICTURE
   drop constraint FK_PIC_ALBU_REFERENCE_PIC_ALBU;

alter table PIC_ALBUM_PICTURE
   drop constraint FK_PIC_ALBU_REFERENCE_PIC_PICT;

alter table PIC_COMMENT
   drop constraint FK_PIC_COMM_REFERENCE_PIC_PICT;

alter table PIC_VOTE
   drop constraint FK_PIC_VOTE_REFERENCE_PIC_PICT;

alter table POLL_ANSWER
   drop constraint FK_POLL_ANS_REFERENCE_USE_USER;

alter table POLL_ANSWER
   drop constraint FK_POLL_ANS_REFERENCE_POL_ALTE;

alter table POL_ALTERNATIVE
   drop constraint FK_POL_ALTE_REFERENCE_POL_POLL;

alter table POL_POLL
   drop constraint FK_POL_POLL_REFERENCE_POL_TYPE;

alter table POL_TYPE
   drop constraint FK_POL_TYPE_REFERENCE_SER_SERV;

alter table QUO_QUOTE
   drop constraint FK_QUO_QUOT_REFERENCE_QUO_TYPE;

alter table QUO_TYPE_SER
   drop constraint FK_QUO_TYPE_REFERENCE_QUO_TYPE;

alter table QUO_TYPE_SER
   drop constraint FK_QUO_TYPE_REFERENCE_SER_SERV;

alter table REG_REGISTRATION
   drop constraint FK_REG_REGI_REFERENCE_CON_CONT;

alter table REG_USE
   drop constraint FK_REG_USE_REFERENCE_REG_REGI;

alter table REG_USE
   drop constraint FK_REG_USE_REFERENCE_USE_USER;

alter table REG_USE
   drop constraint FK_REG_USE_REFERENCE_REG_STAT;

alter table REP_PROJECT
   drop constraint FK_REP_PROJ_REFERENCE_USE_USER;

alter table REP_REPORT
   drop constraint FK_REP_REPO_REFERENCE_REP_PROJ;

alter table SES_INSTANCE
   drop constraint FK_SES_INST_REFERENCE_SES_SESS;

alter table SES_SESSION
   drop constraint FK_SES_SESS_REFERENCE_ORG_ORGA;

alter table SES_SESSION
   drop constraint FK_SES_SESS_REFERENCE_TEX_LANG;

alter table SES_SESSION
   drop constraint FK_SES_SESS_REFERENCE_SER_SERV;

alter table SES_SESSION
   drop constraint FK_SES_SESS_REFERENCE_USE_USER;

alter table SUB_SELECTION
   drop constraint FK_SUB_SELE_REFERENCE_SUB_SUBS;

alter table SUB_SUBSCRIPTION
   drop constraint FK_SUB_SUBS_REFERENCE_USE_USER;

alter table SYS_PARAMETER
   drop constraint FK_SYS_PARA_REFERENCE_SYS_PARA;

alter table SYS_PARAMETER
   drop constraint FK_SYS_PARA_REFERENCE_USE_USER;

alter table TEX_TEXT
   drop constraint FK_TEX_TEXT_REFERENCE_TEX_LANG;

alter table USE_DETAILS
   drop constraint FK_USE_DETA_REFERENCE_USE_USER;

alter table USE_DETAILS
   drop constraint FK_USE_DETA_REFERENCE_ADD_ADDR;

alter table USE_LOGIN_STATUS
   drop constraint FK_USE_LOGI_REFERENCE_USE_USE2;

alter table USE_ORG
   drop constraint FK_USE_ORG_REFERENCE_USE_USER;

alter table USE_USER
   drop constraint FK_USE_USER_REFERENCE_USE_TYPE;

alter table USE_USER
   drop constraint FK_USE_USER_REFERENCE_USE_STAT;

drop table ADD_ADDRESS cascade constraints;

drop table BIL_ITEM cascade constraints;

drop table BIL_ITEM_TYPE cascade constraints;

drop table BIL_PAYMENT_METHOD cascade constraints;

drop table BIL_TRADE_TYPE cascade constraints;

drop table COM_COMPARE cascade constraints;

drop table COM_ORG cascade constraints;

drop table CON_COMMENT cascade constraints;

drop table CON_CONTENT cascade constraints;

drop table CON_CONTENT_FILE cascade constraints;

drop table CON_FILE cascade constraints;

drop table CON_FILE_TYPE cascade constraints;

drop table CON_RELATION cascade constraints;

drop table CON_SER cascade constraints;

drop table CON_STATUS cascade constraints;

drop table CON_SUBTYPE cascade constraints;

drop table CON_TYPE cascade constraints;

drop table CON_URL cascade constraints;

drop table CON_VOTE cascade constraints;

drop table FOR_FORUM cascade constraints;

drop table FOR_TYPE cascade constraints;

drop table FOR_USE cascade constraints;

drop table GEO_GEOGRAPHY cascade constraints;

drop table GEO_ZIP cascade constraints;

drop table HIS_HISTORY cascade constraints;

drop table IMP_NIGHTCLUB cascade constraints;

drop table IMP_NIGHTCLUB_OWNER cascade constraints;

drop table LIS_LIST cascade constraints;

drop table LIS_ORG cascade constraints;

drop table LIS_TYPE cascade constraints;

drop table LOG_CON_EMAIL cascade constraints;

drop table LOG_DEBUG cascade constraints;

drop table LOG_ERROR cascade constraints;

drop table LOG_HITS cascade constraints;

drop table LOG_LOGIN cascade constraints;

drop table LOG_MOD cascade constraints;

drop table LOG_SUB cascade constraints;

drop table LOG_TEX cascade constraints;

drop table LOG_UPLOAD cascade constraints;

drop table MAR_STATUS cascade constraints;

drop table MAR_STUNT cascade constraints;

drop table MAR_STUNT_TYPE cascade constraints;

drop table MAR_TARGET cascade constraints;

drop table MEM_MEMBERSHIP cascade constraints;

drop table MEM_MOD cascade constraints;

drop table MEM_ORG cascade constraints;

drop table MEN_MENU cascade constraints;

drop table MEN_OBJECT cascade constraints;

drop table MEN_OBJECT_SERVING cascade constraints;

drop table MEN_OBJECT_TYPE cascade constraints;

drop table MEN_ORG_SERVING cascade constraints;

drop table MEN_PRICE cascade constraints;

drop table MEN_SERVING_PERIOD cascade constraints;

drop table MEN_SERVING_TYPE cascade constraints;

drop table MEN_TYPE cascade constraints;

drop table MOD_MODULE cascade constraints;

drop table MOD_PACKAGE cascade constraints;

drop table MOD_PERMISSION cascade constraints;

drop table MOD_PROCEDURE cascade constraints;

drop table MOD_RESTRICTION cascade constraints;

drop table ORG_CHOSEN cascade constraints;

drop table ORG_HOURS cascade constraints;

drop table ORG_ORGANISATION cascade constraints;

drop table ORG_ORGANISATION_RIGHTS cascade constraints;

drop table ORG_ORIGINAL cascade constraints;

drop table ORG_RIGHTS cascade constraints;

drop table ORG_SER cascade constraints;

drop table ORG_STATUS cascade constraints;

drop table ORG_VALUE cascade constraints;

drop table ORG_VALUE_TYPE cascade constraints;

drop table PIC_ALBUM cascade constraints;

drop table PIC_ALBUM_PICTURE cascade constraints;

drop table PIC_COMMENT cascade constraints;

drop table PIC_PICTURE cascade constraints;

drop table PIC_VOTE cascade constraints;

drop table POLL_ANSWER cascade constraints;

drop table POL_ALTERNATIVE cascade constraints;

drop table POL_POLL cascade constraints;

drop table POL_TYPE cascade constraints;

drop table QUO_QUOTE cascade constraints;

drop table QUO_TYPE cascade constraints;

drop table QUO_TYPE_SER cascade constraints;

drop table REG_REGISTRATION cascade constraints;

drop table REG_STATUS cascade constraints;

drop table REG_USE cascade constraints;

drop table REP_PROJECT cascade constraints;

drop table REP_REPORT cascade constraints;

drop table SER_SERVICE cascade constraints;

drop table SES_INSTANCE cascade constraints;

drop table SES_SESSION cascade constraints;

drop table SUB_SELECTION cascade constraints;

drop table SUB_SUBSCRIPTION cascade constraints;

drop table SYS_PARAMETER cascade constraints;

drop table SYS_PARAMETER_TYPE cascade constraints;

drop table TEX_LANGUAGE cascade constraints;

drop table TEX_TEXT cascade constraints;

drop table USE_DETAILS cascade constraints;

drop table USE_LOGIN_STATUS cascade constraints;

drop table USE_ORG cascade constraints;

drop table USE_STATUS cascade constraints;

drop table USE_TYPE cascade constraints;

drop table USE_USER cascade constraints;

drop sequence SEQ_HIS_HISTORY;

drop sequence SEQ_LOG_DEBUG;

drop sequence SEQ_LOG_ERROR;

drop sequence SEQ_LOG_HITS;

drop sequence SEQ_LOG_LOGIN;

drop sequence SEQ_LOG_MOD;

drop sequence SEQ_LOG_UPLOAD;

drop sequence SEQ_MOD_MODULE;

drop sequence SEQ_MOD_PACKAGE;

drop sequence SEQ_MOD_PROCEDURE;

drop sequence SEQ_SES_INSTANCE;

drop sequence SEQ_SES_SESSION;

create sequence SEQ_HIS_HISTORY
increment by 1
start with 1000;

create sequence SEQ_LOG_DEBUG
increment by 1
start with 1000;

create sequence SEQ_LOG_ERROR
increment by 1
start with 1000;

create sequence SEQ_LOG_HITS
increment by 1
start with 1000;

create sequence SEQ_LOG_LOGIN
increment by 1
start with 1000;

create sequence SEQ_LOG_MOD
increment by 1
start with 1000;

create sequence SEQ_LOG_UPLOAD
increment by 1
start with 1000;

create sequence SEQ_MOD_MODULE
increment by 1
start with 1000;

create sequence SEQ_MOD_PACKAGE
increment by 1
start with 1000;

create sequence SEQ_MOD_PROCEDURE
increment by 1
start with 1000;

create sequence SEQ_SES_INSTANCE
increment by 1
start with 1000;

create sequence SEQ_SES_SESSION
increment by 1
start with 1000;

/*==============================================================*/
/* Table: ADD_ADDRESS                                           */
/*==============================================================*/
create table ADD_ADDRESS  (
   ADD_ADDRESS_PK       NUMBER                          not null,
   NAME                 VARCHAR2(64),
   STREET               VARCHAR2(512),
   ZIP                  VARCHAR2(16),
   LANDLINE             VARCHAR2(32),
   MOBILE               VARCHAR2(32),
   FAX                  VARCHAR2(32),
   EMAIL                VARCHAR2(64),
   URL                  VARCHAR2(128),
   GEO_GEOGRAPHY_FK     NUMBER,
   LONGITUDE            NUMBER,
   LATITUDE             NUMBER,
   DATE_INSERT          DATE,
   DATE_UPDATE          DATE,
   constraint PK_ADD_ADDRESS primary key (ADD_ADDRESS_PK)
);

/*==============================================================*/
/* Table: BIL_ITEM                                              */
/*==============================================================*/
create table BIL_ITEM  (
   BIL_ITEM_PK          NUMBER                          not null,
   TITLE                VARCHAR2(64),
   BODY                 VARCHAR2(2048),
   PRICE                VARCHAR2(64),
   URL                  VARCHAR2(128),
   NO_ITEMS             NUMBER,
   BOOL_FREIGHT         NUMBER(1),
   BOOL_DISCOUNT_POSSIBLE NUMBER(1),
   PATH_PICTURE         VARCHAR2(64),
   BIL_TRADE_TYPE_FK    NUMBER,
   BIL_ITEM_TYPE_FK     NUMBER,
   BIL_PAYMENT_METHOD_FK NUMBER,
   ADD_ADDRESS_FK       NUMBER,
   USE_USER_FK          NUMBER,
   DATE_START           DATE,
   DATE_END             DATE,
   DATE_INSERT          DATE,
   DATE_UPDATE          DATE,
   constraint PK_BIL_ITEM primary key (BIL_ITEM_PK)
);

/*==============================================================*/
/* Table: BIL_ITEM_TYPE                                         */
/*==============================================================*/
create table BIL_ITEM_TYPE  (
   BIL_ITEM_TYPE_PK     NUMBER                          not null,
   NAME                 VARCHAR2(16),
   DESCRIPTION          VARCHAR2(64),
   constraint PK_BIL_ITEM_TYPE primary key (BIL_ITEM_TYPE_PK)
);

/*==============================================================*/
/* Table: BIL_PAYMENT_METHOD                                    */
/*==============================================================*/
create table BIL_PAYMENT_METHOD  (
   BIL_PAYMENT_METHOD_PK NUMBER                          not null,
   NAME                 VARCHAR2(16),
   DESCRIPTION          VARCHAR2(64),
   constraint PK_BIL_PAYMENT_METHOD primary key (BIL_PAYMENT_METHOD_PK)
);

/*==============================================================*/
/* Table: BIL_TRADE_TYPE                                        */
/*==============================================================*/
create table BIL_TRADE_TYPE  (
   BIL_TRADE_TYPE_PK    NUMBER                          not null,
   NAME                 VARCHAR2(16),
   DESCRIPTION          VARCHAR2(64),
   constraint PK_BIL_TRADE_TYPE primary key (BIL_TRADE_TYPE_PK)
);

/*==============================================================*/
/* Table: COM_COMPARE                                           */
/*==============================================================*/
create table COM_COMPARE  (
   COM_COMPARE_PK       NUMBER                          not null,
   NAME                 VARCHAR2(16),
   DESCRIPTION          VARCHAR2(64),
   constraint PK_COM_COMPARE primary key (COM_COMPARE_PK)
);

/*==============================================================*/
/* Table: COM_ORG                                               */
/*==============================================================*/
create table COM_ORG  (
   ORG_ORGANISATION_FK_PK NUMBER                          not null,
   COM_COMPARE_FK       NUMBER                          not null,
   PRICE                NUMBER,
   constraint PK_COM_ORG primary key (ORG_ORGANISATION_FK_PK, COM_COMPARE_FK)
);

/*==============================================================*/
/* Table: CON_COMMENT                                           */
/*==============================================================*/
create table CON_COMMENT  (
   CON_COMMENT_PK       NUMBER                          not null,
   CON_CONTENT_FK       NUMBER,
   USE_USER_FK          NUMBER,
   "COMMENT"            VARCHAR2(128),
   DATE_INSERT          DATE,
   DATE_UPDATE          DATE,
   constraint PK_CON_COMMENT primary key (CON_COMMENT_PK)
);

/*==============================================================*/
/* Table: CON_CONTENT                                           */
/*==============================================================*/
create table CON_CONTENT  (
   CON_CONTENT_PK       NUMBER                          not null,
   SUBJECT              VARCHAR2(128),
   INGRESS              VARCHAR2(512),
   BODY                 CLOB,
   FOOTER               VARCHAR2(128),
   RATING               NUMBER,
   ORG_ORGANISATION_FK  NUMBER,
   GEO_GEOGRAPHY_FK     NUMBER,
   DATE_START           DATE,
   DATE_STOP            DATE,
   DATE_PUBLISH         DATE,
   CON_TYPE_FK          NUMBER,
   CON_SUBTYPE_FK       NUMBER,
   CON_STATUS_FK        NUMBER,
   EXTERNAL_ID          VARCHAR2(32),
   DATE_INSERT          DATE,
   DATE_UPDATE          DATE,
   constraint PK_CON_CONTENT primary key (CON_CONTENT_PK)
);

/*==============================================================*/
/* Table: CON_CONTENT_FILE                                      */
/*==============================================================*/
create table CON_CONTENT_FILE  (
   CON_CONTENT_FK_PK    NUMBER                          not null,
   CON_FILE_FK_PK       NUMBER                          not null,
   constraint PK_CON_CONTENT_FILE primary key (CON_CONTENT_FK_PK, CON_FILE_FK_PK)
);

/*==============================================================*/
/* Table: CON_FILE                                              */
/*==============================================================*/
create table CON_FILE  (
   CON_FILE_PK          NUMBER                          not null,
   NAME                 VARCHAR2(16),
   PATH                 VARCHAR2(64),
   CON_FILE_TYPE_FK     NUMBER,
   DATE_INSERT          DATE,
   constraint PK_CON_FILE primary key (CON_FILE_PK)
);

/*==============================================================*/
/* Table: CON_FILE_TYPE                                         */
/*==============================================================*/
create table CON_FILE_TYPE  (
   CON_FILE_TYPE_PK     NUMBER                          not null,
   NAME                 VARCHAR2(16),
   DESCRIPTION          VARCHAR2(64),
   constraint PK_CON_FILE_TYPE primary key (CON_FILE_TYPE_PK)
);

/*==============================================================*/
/* Table: CON_RELATION                                          */
/*==============================================================*/
create table CON_RELATION  (
   CON_CONTENT_1_FK_PK  NUMBER                          not null,
   CON_CONTENT_2_FK_PK  NUMBER                          not null,
   DATE_INSERT          DATE,
   constraint PK_CON_RELATION primary key (CON_CONTENT_1_FK_PK, CON_CONTENT_2_FK_PK)
);

/*==============================================================*/
/* Table: CON_SER                                               */
/*==============================================================*/
create table CON_SER  (
   CON_CONTENT_FK_PK    NUMBER                          not null,
   SER_SERVICE_FK_PK    NUMBER                          not null,
   constraint PK_CON_SER primary key (CON_CONTENT_FK_PK, SER_SERVICE_FK_PK)
);

/*==============================================================*/
/* Table: CON_STATUS                                            */
/*==============================================================*/
create table CON_STATUS  (
   CON_STATUS_PK        NUMBER                          not null,
   NAME                 VARCHAR2(16),
   DESCRIPTION          VARCHAR2(64),
   constraint PK_CON_STATUS primary key (CON_STATUS_PK)
);

/*==============================================================*/
/* Table: CON_SUBTYPE                                           */
/*==============================================================*/
create table CON_SUBTYPE  (
   CON_SUBTYPE_PK       NUMBER                          not null,
   NAME                 VARCHAR2(16),
   DESCRIPTION          VARCHAR2(64),
   constraint PK_CON_SUBTYPE primary key (CON_SUBTYPE_PK)
);

/*==============================================================*/
/* Table: CON_TYPE                                              */
/*==============================================================*/
create table CON_TYPE  (
   CON_TYPE_PK          NUMBER                          not null,
   NAME                 VARCHAR2(16),
   DESCRIPTION          VARCHAR2(64),
   RATING_MAX           NUMBER,
   constraint PK_CON_TYPE primary key (CON_TYPE_PK)
);

/*==============================================================*/
/* Table: CON_URL                                               */
/*==============================================================*/
create table CON_URL  (
   CON_URL_PK           NUMBER                          not null,
   CON_CONTENT_FK       NUMBER,
   URL                  VARCHAR2(128),
   NAME                 VARCHAR2(32),
   DATE_INSERT          DATE,
   constraint PK_CON_URL primary key (CON_URL_PK)
);

/*==============================================================*/
/* Table: CON_VOTE                                              */
/*==============================================================*/
create table CON_VOTE  (
   CON_VOTE_PK          NUMBER                          not null,
   CON_CONTENT_FK       NUMBER,
   USE_USER_FK          NUMBER,
   RATING               NUMBER,
   DATE_INSERT          DATE,
   constraint PK_CON_VOTE primary key (CON_VOTE_PK)
);

/*==============================================================*/
/* Table: FOR_FORUM                                             */
/*==============================================================*/
create table FOR_FORUM  (
   FOR_FORUM_PK         NUMBER                          not null,
   TITLE                VARCHAR2(64),
   SUBTITLE             VARCHAR2(64),
   INGRESS              VARCHAR2(512),
   BODY                 VARCHAR2(4000),
   SER_SERVICE_FK       NUMBER,
   BOOL_ACTIVE          NUMBER(1),
   PARENT_FOR_FORUM_FK  NUMBER,
   FOR_TYPE_FK          NUMBER,
   DATE_INSERT          DATE,
   DATE_UPDATE          DATE,
   constraint PK_FOR_FORUM primary key (FOR_FORUM_PK)
);

/*==============================================================*/
/* Table: FOR_TYPE                                              */
/*==============================================================*/
create table FOR_TYPE  (
   FOR_TYPE_PK          NUMBER                          not null,
   NAME                 VARCHAR2(16),
   DESCRIPTION          VARCHAR2(64),
   SER_SERVICE_FK       NUMBER,
   ORG_ORGANISATION_FK  NUMBER,
   BOOL_ACTIVE          NUMBER(1),
   DATE_INSERT          DATE,
   DATE_UPDATE          DATE,
   constraint PK_FOR_TYPE primary key (FOR_TYPE_PK)
);

/*==============================================================*/
/* Table: FOR_USE                                               */
/*==============================================================*/
create table FOR_USE  (
   FOR_FORUM_FK_PK      NUMBER                          not null,
   USE_USER_FK_PK       NUMBER                          not null,
   constraint PK_FOR_USE primary key (FOR_FORUM_FK_PK, USE_USER_FK_PK)
);

/*==============================================================*/
/* Table: GEO_GEOGRAPHY                                         */
/*==============================================================*/
create table GEO_GEOGRAPHY  (
   GEO_GEOGRAPHY_PK     NUMBER                          not null,
   NAME                 VARCHAR2(64),
   PARENT_GEO_GEOGRAPHY_FK NUMBER,
   constraint PK_GEO_GEOGRAPHY primary key (GEO_GEOGRAPHY_PK)
);

/*==============================================================*/
/* Table: GEO_ZIP                                               */
/*==============================================================*/
create table GEO_ZIP  (
   GEO_GEOGRAPHY_FK_PK  NUMBER                          not null,
   ZIP_PK               VARCHAR2(16)                    not null,
   constraint PK_GEO_ZIP primary key (GEO_GEOGRAPHY_FK_PK, ZIP_PK)
);

/*==============================================================*/
/* Table: HIS_HISTORY                                           */
/*==============================================================*/
create table HIS_HISTORY  (
   HIS_HISTORY_PK       NUMBER                          not null,
   TABLE_NAME           VARCHAR2(128),
   COLUMN_NAME          VARCHAR2(128),
   OLD_VALUE            VARCHAR2(4000),
   USE_USER_FK          NUMBER,
   SES_SESSION_ID       NUMBER,
   DATE_INSERT          DATE,
   constraint PK_HIS_HISTORY primary key (HIS_HISTORY_PK)
);

/*==============================================================*/
/* Table: IMP_NIGHTCLUB                                         */
/*==============================================================*/
create table IMP_NIGHTCLUB  (
   IMP_NIGHTCLUB_PK     NUMBER                          not null,
   ORG_NUMBER           VARCHAR2(16),
   NAME                 VARCHAR2(64),
   ADDRESS_FULL         VARCHAR2(512),
   STREET               VARCHAR2(256),
   COUNCIL              VARCHAR2(64),
   ZIP                  VARCHAR2(16),
   LANDLINE             VARCHAR2(32),
   FAX                  VARCHAR2(32),
   EMAIL                VARCHAR2(64),
   URL                  VARCHAR2(128),
   BOOL_HAS_BEER        NUMBER(1),
   BOOL_HAS_WINE        NUMBER(1),
   BOOL_HAS_SPIRITS     NUMBER(1),
   SOURCE               VARCHAR2(32),
   MD5CHECKSUM          VARCHAR2(32),
   DATE_INSERT          DATE,
   DATE_IMPORT          DATE,
   constraint PK_IMP_NIGHTCLUB primary key (IMP_NIGHTCLUB_PK)
);

/*==============================================================*/
/* Table: IMP_NIGHTCLUB_OWNER                                   */
/*==============================================================*/
create table IMP_NIGHTCLUB_OWNER  (
   ORG_NUMBER           VARCHAR2(16)                    not null,
   NAME                 VARCHAR2(64),
   ADDRESS_FULL         VARCHAR2(512),
   STREET               VARCHAR2(256),
   COUNCIL              VARCHAR2(64),
   ZIP                  VARCHAR2(16),
   LANDLINE             VARCHAR2(32),
   FAX                  VARCHAR2(32),
   EMAIL                VARCHAR2(64),
   URL                  VARCHAR2(128),
   SOURCE               VARCHAR2(32),
   MD5CHECKSUM          VARCHAR2(32),
   DATE_INSERT          DATE,
   DATE_IMPORT          DATE,
   constraint PK_IMP_NIGHTCLUB_OWNER primary key (ORG_NUMBER)
);

/*==============================================================*/
/* Table: LIS_LIST                                              */
/*==============================================================*/
create table LIS_LIST  (
   LIS_LIST_PK          NUMBER                          not null,
   NAME                 VARCHAR2(16),
   DESCRIPTION          VARCHAR2(64),
   PARENT_LIS_LIST_FK   NUMBER,
   LIS_TYPE_FK          NUMBER,
   constraint PK_LIS_LIST primary key (LIS_LIST_PK)
);

alter table LIS_LIST
    add constraint UNIQUE_LIS_LIST_NAME UNIQUE (NAME);

/*==============================================================*/
/* Table: LIS_ORG                                               */
/*==============================================================*/
create table LIS_ORG  (
   LIS_LIST_FK_PK       NUMBER                          not null,
   ORG_ORGANISATION_FK_PK NUMBER                          not null,
   constraint PK_LIS_ORG primary key (LIS_LIST_FK_PK, ORG_ORGANISATION_FK_PK)
);

/*==============================================================*/
/* Table: LIS_TYPE                                              */
/*==============================================================*/
create table LIS_TYPE  (
   LIS_TYPE_PK          NUMBER                          not null,
   SER_SERVICE_FK       NUMBER,
   NAME                 VARCHAR2(16),
   DESCRIPTION          VARCHAR2(64),
   constraint PK_LIS_TYPE primary key (LIS_TYPE_PK)
);

/*==============================================================*/
/* Table: LOG_CON_EMAIL                                         */
/*==============================================================*/
create table LOG_CON_EMAIL  (
   LOG_CON_EMAIL_PK     NUMBER                          not null,
   VOLUME               NUMBER,
   CON_CONTENT_FK       NUMBER,
   DATE_INSERT          DATE,
   constraint PK_LOG_CON_EMAIL primary key (LOG_CON_EMAIL_PK)
);

/*==============================================================*/
/* Table: LOG_DEBUG                                             */
/*==============================================================*/
create table LOG_DEBUG  (
   LOG_DEBUG_PK         NUMBER                          not null,
   MESSAGE              VARCHAR2(1024),
   DATE_INSERT          DATE,
   constraint PK_LOG_DEBUG primary key (LOG_DEBUG_PK)
);

/*==============================================================*/
/* Table: LOG_ERROR                                             */
/*==============================================================*/
create table LOG_ERROR  (
   LOG_ERROR_PK         NUMBER                          not null,
   MESSAGE              VARCHAR2(1024),
   SES_SESSION_ID       NUMBER,
   SES_SESSION_ID_NEW   NUMBER,
   BOOL_CRITICAL        NUMBER(1),
   DATE_INSERT          DATE,
   constraint PK_LOG_ERROR primary key (LOG_ERROR_PK)
);

/*==============================================================*/
/* Table: LOG_HITS                                              */
/*==============================================================*/
create table LOG_HITS  (
   LOG_HITS_PK          NUMBER                          not null,
   HITS                 NUMBER,
   TABLE_NAME           VARCHAR2(128),
   FOREIGN_KEY_FK       NUMBER,
   constraint PK_LOG_HITS primary key (LOG_HITS_PK)
);

/*==============================================================*/
/* Table: LOG_LOGIN                                             */
/*==============================================================*/
create table LOG_LOGIN  (
   LOG_LOGIN_PK         NUMBER                          not null,
   USE_USER_FK          NUMBER,
   SES_SESSION_ID       NUMBER,
   USERNAME             VARCHAR2(32),
   PASSWORD             VARCHAR2(32),
   HOST                 VARCHAR2(64),
   IP                   VARCHAR2(32),
   BOOL_SUCCESS         NUMBER(1),
   DATE_INSERT          DATE,
   constraint PK_LOG_LOGIN primary key (LOG_LOGIN_PK)
);

/*==============================================================*/
/* Table: LOG_MOD                                               */
/*==============================================================*/
create table LOG_MOD  (
   LOG_MOD_PK           NUMBER                          not null,
   MOD_MODULE_NAME      VARCHAR2(128),
   DATE_INSERT          DATE,
   DATE_UPDATE          DATE,
   constraint PK_LOG_MOD primary key (LOG_MOD_PK)
);

/*==============================================================*/
/* Table: LOG_SUB                                               */
/*==============================================================*/
create table LOG_SUB  (
   LOG_SUB_PK           NUMBER                          not null,
   USE_USER_FK          NUMBER,
   CON_CONTENT_FK       NUMBER,
   NO_ENTRIES           NUMBER,
   DATE_INSERT          DATE,
   constraint PK_LOG_SUB primary key (LOG_SUB_PK)
);

/*==============================================================*/
/* Table: LOG_TEX                                               */
/*==============================================================*/
create table LOG_TEX  (
   TEX_TEXT_FK_PK       VARCHAR2(128)                   not null,
   TEX_LANGUAGE_FK_PK   VARCHAR2(3)                     not null,
   DATE_ACCESSED        DATE,
   constraint PK_LOG_TEX primary key (TEX_TEXT_FK_PK, TEX_LANGUAGE_FK_PK)
);

/*==============================================================*/
/* Table: LOG_UPLOAD                                            */
/*==============================================================*/
create table LOG_UPLOAD  (
   LOG_UPLOAD_PK        NUMBER                          not null,
   CONTENT              BLOB,
   SRC_FILENAME         VARCHAR2(128),
   TRG_FILENAME         VARCHAR2(32),
   TRG_DIRECTORY        VARCHAR2(32),
   BOOL_WRITTEN         NUMBER(1),
   MESSAGE              VARCHAR2(128),
   SES_SESSION_ID       NUMBER,
   DATE_INSERT          DATE,
   constraint PK_LOG_UPLOAD primary key (LOG_UPLOAD_PK)
);

/*==============================================================*/
/* Table: MAR_STATUS                                            */
/*==============================================================*/
create table MAR_STATUS  (
   MAR_STATUS_PK        NUMBER                          not null,
   NAME                 VARCHAR2(16),
   DESCRIPTION          VARCHAR2(64),
   constraint PK_MAR_STATUS primary key (MAR_STATUS_PK)
);

/*==============================================================*/
/* Table: MAR_STUNT                                             */
/*==============================================================*/
create table MAR_STUNT  (
   MAR_STUNT_PK         NUMBER                          not null,
   MAR_TARGET_FK        NUMBER,
   MESSAGE              VARCHAR2(4000),
   PATH_ATTACHMENT1     VARCHAR2(64),
   PATH_ATTACHMENT2     VARCHAR2(64),
   PATH_ATTACHMENT3     VARCHAR2(64),
   MAR_STUNT_TYPE_FK    NUMBER,
   MAR_STATUS_FK        NUMBER,
   BOOL_FEEDBACK        NUMBER(1),
   BOOL_IMMEDIATE_FEEDBACK NUMBER(1),
   INSERT_USE_USER_FK   NUMBER,
   DATE_INSERT          DATE,
   DATE_UPDATE          DATE,
   constraint PK_MAR_STUNT primary key (MAR_STUNT_PK)
);

/*==============================================================*/
/* Table: MAR_STUNT_TYPE                                        */
/*==============================================================*/
create table MAR_STUNT_TYPE  (
   MAR_STUNT_TYPE_PK    NUMBER                          not null,
   NAME                 VARCHAR2(16),
   DESCRIPTION          VARCHAR2(64),
   constraint PK_MAR_STUNT_TYPE primary key (MAR_STUNT_TYPE_PK)
);

/*==============================================================*/
/* Table: MAR_TARGET                                            */
/*==============================================================*/
create table MAR_TARGET  (
   MAR_TARGET_PK        NUMBER                          not null,
   ADD_ADDRESS_FK       NUMBER,
   ORG_ORGANISATION_FK  NUMBER,
   TITLE                VARCHAR2(64),
   DESCRIPTION          VARCHAR2(1024),
   INSERT_USE_USER_FK   NUMBER,
   DATE_INSERT          DATE,
   DATE_UPDATE          DATE,
   constraint PK_MAR_TARGET primary key (MAR_TARGET_PK)
);

/*==============================================================*/
/* Table: MEM_MEMBERSHIP                                        */
/*==============================================================*/
create table MEM_MEMBERSHIP  (
   MEM_MEMBERSHIP_PK    NUMBER                          not null,
   NAME                 VARCHAR2(16),
   DESCRIPTION          VARCHAR2(64),
   PRICE_DEFAULT        NUMBER,
   DURATION_DEFAULT     NUMBER,
   DATE_INSERT          DATE,
   DATE_UPDATE          DATE,
   constraint PK_MEM_MEMBERSHIP primary key (MEM_MEMBERSHIP_PK)
);

/*==============================================================*/
/* Table: MEM_MOD                                               */
/*==============================================================*/
create table MEM_MOD  (
   MEM_MEMBERSHIP_FK_PK NUMBER                          not null,
   MOD_MODULE_FK_PK     NUMBER                          not null,
   constraint PK_MEM_MOD primary key (MEM_MEMBERSHIP_FK_PK, MOD_MODULE_FK_PK)
);

/*==============================================================*/
/* Table: MEM_ORG                                               */
/*==============================================================*/
create table MEM_ORG  (
   MEM_MEMBERSHIP_FK_PK NUMBER                          not null,
   ORG_ORGANISATION_FK_PK NUMBER                          not null,
   DURATION             NUMBER,
   PRICE                NUMBER,
   DATE_START           DATE,
   DATE_INSERT          DATE,
   DATE_UPDATE          DATE,
   constraint PK_MEM_ORG primary key (MEM_MEMBERSHIP_FK_PK, ORG_ORGANISATION_FK_PK)
);

/*==============================================================*/
/* Table: MEN_MENU                                              */
/*==============================================================*/
create table MEN_MENU  (
   MEN_MENU_PK          NUMBER                          not null,
   ORG_ORGANISATION_FK  NUMBER,
   MENU_TYPE_FK         NUMBER,
   BOOL_ACTIVE          NUMBER(1),
   DATE_INSERT          DATE,
   DATE_UPDATE          DATE,
   constraint PK_MEN_MENU primary key (MEN_MENU_PK)
);

/*==============================================================*/
/* Table: MEN_OBJECT                                            */
/*==============================================================*/
create table MEN_OBJECT  (
   MEN_OBJECT_PK        NUMBER                          not null,
   MEN_MENU_FK          NUMBER,
   MEN_OBJECT_TYPE_FK   NUMBER,
   MEN_SERVING_TYPE_FK  NUMBER,
   TITLE                VARCHAR2(128),
   DESCRIPTION          VARCHAR2(2048),
   ORDER_NUMBER         NUMBER,
   PATH_PICTURE         VARCHAR2(64),
   BOOL_ACTIVE          NUMBER(1),
   DATE_INSERT          DATE,
   DATE_UPDATE          DATE,
   constraint PK_MEN_OBJECT primary key (MEN_OBJECT_PK)
);

/*==============================================================*/
/* Table: MEN_OBJECT_SERVING                                    */
/*==============================================================*/
create table MEN_OBJECT_SERVING  (
   MEN_OBJECT_FK_PK     NUMBER                          not null,
   MEN_ORG_SERVING_FK_PK NUMBER                          not null,
   constraint PK_MEN_OBJECT_SERVING primary key (MEN_OBJECT_FK_PK, MEN_ORG_SERVING_FK_PK)
);

/*==============================================================*/
/* Table: MEN_OBJECT_TYPE                                       */
/*==============================================================*/
create table MEN_OBJECT_TYPE  (
   MEN_OBJECT_TYPE_PK   NUMBER                          not null,
   NAME                 VARCHAR2(16),
   DESCRIPTION          VARCHAR2(64),
   MEN_TYPE_FK          NUMBER,
   constraint PK_MEN_OBJECT_TYPE primary key (MEN_OBJECT_TYPE_PK)
);

/*==============================================================*/
/* Table: MEN_ORG_SERVING                                       */
/*==============================================================*/
create table MEN_ORG_SERVING  (
   MEN_ORG_SERVING_PK   NUMBER                          not null,
   ORG_ORGANISATION_FK  NUMBER,
   MEN_SERVING_PERIOD_FK NUMBER,
   DATE_START           DATE,
   DATE_END             DATE,
   constraint PK_MEN_ORG_SERVING primary key (MEN_ORG_SERVING_PK)
);

/*==============================================================*/
/* Table: MEN_PRICE                                             */
/*==============================================================*/
create table MEN_PRICE  (
   MEN_PRICE_PK         NUMBER                          not null,
   MENU_OBJECT_FK       NUMBER,
   QUANTITY             VARCHAR2(16),
   PRICE                NUMBER,
   constraint PK_MEN_PRICE primary key (MEN_PRICE_PK)
);

/*==============================================================*/
/* Table: MEN_SERVING_PERIOD                                    */
/*==============================================================*/
create table MEN_SERVING_PERIOD  (
   MEN_SERVING_PERIOD_PK NUMBER                          not null,
   NAME                 VARCHAR2(16),
   DESCRIPTION          VARCHAR2(64),
   constraint PK_MEN_SERVING_PERIOD primary key (MEN_SERVING_PERIOD_PK)
);

/*==============================================================*/
/* Table: MEN_SERVING_TYPE                                      */
/*==============================================================*/
create table MEN_SERVING_TYPE  (
   MEN_SERVING_TYPE_PK  NUMBER                          not null,
   NAME                 VARCHAR2(16),
   DESCRIPTION          VARCHAR2(64),
   BOOL_ACTIVE          NUMBER(1),
   constraint PK_MEN_SERVING_TYPE primary key (MEN_SERVING_TYPE_PK)
);

/*==============================================================*/
/* Table: MEN_TYPE                                              */
/*==============================================================*/
create table MEN_TYPE  (
   MEN_TYPE_PK          NUMBER                          not null,
   NAME                 VARCHAR2(16),
   DESCRIPTION          VARCHAR2(64),
   BOOL_ACTIVE          NUMBER(1),
   constraint PK_MEN_TYPE primary key (MEN_TYPE_PK)
);

/*==============================================================*/
/* Table: MOD_MODULE                                            */
/*==============================================================*/
create table MOD_MODULE  (
   MOD_MODULE_PK        NUMBER                          not null,
   MOD_PACKAGE_FK       NUMBER,
   MOD_PROCEDURE_FK     NUMBER,
   NAME                 VARCHAR2(128),
   constraint PK_MOD_MODULE primary key (MOD_MODULE_PK)
);

/*==============================================================*/
/* Table: MOD_PACKAGE                                           */
/*==============================================================*/
create table MOD_PACKAGE  (
   MOD_PACKAGE_PK       NUMBER                          not null,
   NAME                 VARCHAR2(32),
   DESCRIPTION          VARCHAR2(64),
   DATE_INSERT          DATE,
   constraint PK_MOD_PACKAGE primary key (MOD_PACKAGE_PK)
);

/*==============================================================*/
/* Table: MOD_PERMISSION                                        */
/*==============================================================*/
create table MOD_PERMISSION  (
   MOD_MODULE_FK_PK     NUMBER                          not null,
   USE_USER_FK_PK       NUMBER                          not null,
   DATE_INSERT          DATE,
   constraint PK_MOD_PERMISSION primary key (MOD_MODULE_FK_PK, USE_USER_FK_PK)
);

/*==============================================================*/
/* Table: MOD_PROCEDURE                                         */
/*==============================================================*/
create table MOD_PROCEDURE  (
   MOD_PROCEDURE_PK     NUMBER                          not null,
   NAME                 VARCHAR2(32),
   DESCRIPTION          VARCHAR2(64),
   DATE_INSERT          DATE,
   constraint PK_MOD_PROCEDURE primary key (MOD_PROCEDURE_PK)
);

/*==============================================================*/
/* Table: MOD_RESTRICTION                                       */
/*==============================================================*/
create table MOD_RESTRICTION  (
   MOD_MODULE_FK_PK     NUMBER                          not null,
   USE_TYPE_FK_PK       NUMBER                          not null,
   DATE_INSERT          DATE,
   constraint PK_MOD_RESTRICTION primary key (MOD_MODULE_FK_PK)
);

/*==============================================================*/
/* Table: ORG_CHOSEN                                            */
/*==============================================================*/
create table ORG_CHOSEN  (
   ORG_ORGANISATION_FK_PK NUMBER                          not null,
   SER_SERVICE_FK_PK    NUMBER                          not null,
   DATE_START_PK        DATE                            not null,
   DATE_STOP            DATE,
   DATE_INSERT          DATE,
   constraint PK_ORG_CHOSEN primary key (SER_SERVICE_FK_PK, DATE_START_PK, ORG_ORGANISATION_FK_PK)
);

/*==============================================================*/
/* Table: ORG_HOURS                                             */
/*==============================================================*/
create table ORG_HOURS  (
   ORG_ORGANISATION_FK_PK NUMBER                          not null,
   MONDAY               VARCHAR2(128),
   TUESDAY              VARCHAR2(128),
   WEDNESDAY            VARCHAR2(128),
   THURSDAY             VARCHAR2(128),
   FRIDAY               VARCHAR2(128),
   SATURDAY             VARCHAR2(128),
   SUNDAY               VARCHAR2(128),
   constraint PK_ORG_HOURS primary key (ORG_ORGANISATION_FK_PK)
);

/*==============================================================*/
/* Table: ORG_ORGANISATION                                      */
/*==============================================================*/
create table ORG_ORGANISATION  (
   ORG_ORGANISATION_PK  NUMBER                          not null,
   NAME                 VARCHAR2(64),
   GENERAL_INFORMATION  VARCHAR2(4000),
   ORG_NUMBER           VARCHAR2(16),
   AGE_LIMIT            NUMBER,
   AGE_DESCRIPTION      VARCHAR2(128),
   ADD_ADDRESS_FK       NUMBER,
   PATH_MAINPHOTO       VARCHAR2(64),
   PATH_LOGO            VARCHAR2(64),
   BOOL_EVENT_BASED     NUMBER(1),
   BOOL_UMBRELLA_ORGANISATION NUMBER(1),
   BOOL_IMPLOCK         NUMBER(1),
   PARENT_ORG_ORGANISATION_FK NUMBER,
   ORG_STATUS_FK        NUMBER,
   SOURCE               VARCHAR2(32),
   DATE_STATUS_CHANGE   DATE,
   DATE_INSERT          DATE,
   DATE_UPDATE          DATE,
   constraint PK_ORG_ORGANISATION primary key (ORG_ORGANISATION_PK)
);

/*==============================================================*/
/* Table: ORG_ORGANISATION_RIGHTS                               */
/*==============================================================*/
create table ORG_ORGANISATION_RIGHTS  (
   ORG_ORGANISATION_FK_PK NUMBER                          not null,
   ORG_RIGHTS_FK_PK     NUMBER                          not null,
   constraint PK_ORG_ORGANISATION_RIGHTS primary key (ORG_ORGANISATION_FK_PK, ORG_RIGHTS_FK_PK)
);

/*==============================================================*/
/* Table: ORG_ORIGINAL                                          */
/*==============================================================*/
create table ORG_ORIGINAL  (
   ORG_ORGANISATION_FK_PK NUMBER                          not null,
   NAME                 VARCHAR2(64),
   ORG_NUMBER           VARCHAR2(16),
   constraint PK_ORG_ORIGINAL primary key (ORG_ORGANISATION_FK_PK)
);

/*==============================================================*/
/* Table: ORG_RIGHTS                                            */
/*==============================================================*/
create table ORG_RIGHTS  (
   ORG_RIGHTS_PK        NUMBER                          not null,
   NAME                 VARCHAR2(16),
   DESCRIPTION          VARCHAR2(64),
   constraint PK_ORG_RIGHTS primary key (ORG_RIGHTS_PK)
);

/*==============================================================*/
/* Table: ORG_SER                                               */
/*==============================================================*/
create table ORG_SER  (
   ORG_ORGANISATION_FK_PK NUMBER                          not null,
   SER_SERVICE_FK_PK    NUMBER                          not null,
   constraint PK_ORG_SER primary key (ORG_ORGANISATION_FK_PK, SER_SERVICE_FK_PK)
);

/*==============================================================*/
/* Table: ORG_STATUS                                            */
/*==============================================================*/
create table ORG_STATUS  (
   ORG_STATUS_PK        NUMBER                          not null,
   NAME                 VARCHAR2(16),
   DESCRIPTION          VARCHAR2(64),
   constraint PK_ORG_STATUS primary key (ORG_STATUS_PK)
);

/*==============================================================*/
/* Table: ORG_VALUE                                             */
/*==============================================================*/
create table ORG_VALUE  (
   ORG_ORGANISATION_FK_PK NUMBER                          not null,
   ORG_VALUE_TYPE_FK_PK NUMBER                          not null,
   NUM_VALUE            NUMBER,
   CHAR_VALUE           VARCHAR2(128),
   constraint PK_ORG_VALUE primary key (ORG_ORGANISATION_FK_PK, ORG_VALUE_TYPE_FK_PK)
);

/*==============================================================*/
/* Table: ORG_VALUE_TYPE                                        */
/*==============================================================*/
create table ORG_VALUE_TYPE  (
   ORG_VALUE_TYPE_PK    NUMBER                          not null,
   NAME                 VARCHAR2(16),
   UNIT                 VARCHAR2(8),
   DESCRIPTION          VARCHAR2(64),
   BOOL_ISNUMERIC       NUMBER(1),
   BOOL_ISCHARACTER     NUMBER(1),
   SER_SERVICE_FK       NUMBER,
   constraint PK_ORG_VALUE_TYPE primary key (ORG_VALUE_TYPE_PK)
);

/*==============================================================*/
/* Table: PIC_ALBUM                                             */
/*==============================================================*/
create table PIC_ALBUM  (
   PIC_ALBUM_PK         NUMBER                          not null,
   ORG_ORGANISATION_FK  NUMBER,
   TITLE                VARCHAR2(64),
   SUBTITLE             VARCHAR2(128),
   BOOL_ACTIVE          NUMBER(1),
   DATE_INSERT          DATE,
   DATE_UPDATE          DATE,
   constraint PK_PIC_ALBUM primary key (PIC_ALBUM_PK)
);

/*==============================================================*/
/* Table: PIC_ALBUM_PICTURE                                     */
/*==============================================================*/
create table PIC_ALBUM_PICTURE  (
   PIC_ALBUM_FK_PK      NUMBER                          not null,
   PIC_PICTURE_FK_PK    NUMBER                          not null,
   constraint PK_PIC_ALBUM_PICTURE primary key (PIC_ALBUM_FK_PK, PIC_PICTURE_FK_PK)
);

/*==============================================================*/
/* Table: PIC_COMMENT                                           */
/*==============================================================*/
create table PIC_COMMENT  (
   PIC_COMMENT_PK       NUMBER                          not null,
   PIC_PICTURE_FK       NUMBER,
   USE_USER_FK          NUMBER,
   "COMMENT"            VARCHAR2(128),
   DATE_INSERT          DATE,
   DATE_UPDATE          DATE,
   constraint PK_PIC_COMMENT primary key (PIC_COMMENT_PK)
);

/*==============================================================*/
/* Table: PIC_PICTURE                                           */
/*==============================================================*/
create table PIC_PICTURE  (
   PIC_PICTURE_PK       NUMBER                          not null,
   DESCRIPTION          VARCHAR2(64),
   PATH_FILENAME        VARCHAR2(64),
   BOOL_ACTIVE          NUMBER(1),
   DATE_INSERT          DATE,
   DATE_UPDATE          DATE,
   constraint PK_PIC_PICTURE primary key (PIC_PICTURE_PK)
);

/*==============================================================*/
/* Table: PIC_VOTE                                              */
/*==============================================================*/
create table PIC_VOTE  (
   PIC_VOTE_PK          NUMBER                          not null,
   PIC_PICTURE_FK       NUMBER,
   USE_USER_FK          NUMBER,
   RATING               NUMBER,
   DATE_INSERT          DATE,
   constraint PK_PIC_VOTE primary key (PIC_VOTE_PK)
);

/*==============================================================*/
/* Table: POLL_ANSWER                                           */
/*==============================================================*/
create table POLL_ANSWER  (
   POLL_ANSWER_PK       NUMBER                          not null,
   USE_USER_FK          NUMBER,
   POL_ALTERNATIVE_FK   NUMBER,
   DATE_INSERT          DATE,
   constraint PK_POLL_ANSWER primary key (POLL_ANSWER_PK)
);

/*==============================================================*/
/* Table: POL_ALTERNATIVE                                       */
/*==============================================================*/
create table POL_ALTERNATIVE  (
   POL_ALTERNATIVE_PK   NUMBER                          not null,
   POL_POLL_FK          NUMBER,
   TEXT                 VARCHAR2(64),
   ALTERNATIVE_NO       NUMBER,
   constraint PK_POL_ALTERNATIVE primary key (POL_ALTERNATIVE_PK)
);

/*==============================================================*/
/* Table: POL_POLL                                              */
/*==============================================================*/
create table POL_POLL  (
   POL_POLL_PK          NUMBER                          not null,
   QUESTION             VARCHAR2(128),
   POL_TYPE_FK          NUMBER,
   BOOL_ACTIVE          NUMBER(1),
   DATE_START           DATE,
   DATE_END             DATE,
   DATE_INSERT          DATE,
   DATE_UPDATE          DATE,
   constraint PK_POL_POLL primary key (POL_POLL_PK)
);

/*==============================================================*/
/* Table: POL_TYPE                                              */
/*==============================================================*/
create table POL_TYPE  (
   POL_TYPE_PK          NUMBER                          not null,
   NAME                 VARCHAR2(16),
   DESCRIPTION          VARCHAR2(64),
   SER_SERVICE_FK       NUMBER,
   constraint PK_POL_TYPE primary key (POL_TYPE_PK)
);

/*==============================================================*/
/* Table: QUO_QUOTE                                             */
/*==============================================================*/
create table QUO_QUOTE  (
   QUO_QUOTE_PK         NUMBER                          not null,
   QUOTE                VARCHAR2(1024),
   WHO                  VARCHAR2(128),
   WHEN                 VARCHAR2(128),
   "WHERE"              VARCHAR2(128),
   QUO_TYPE_FK          NUMBER,
   DATE_INSERT          DATE,
   DATE_UPDATE          DATE,
   constraint PK_QUO_QUOTE primary key (QUO_QUOTE_PK)
);

/*==============================================================*/
/* Table: QUO_TYPE                                              */
/*==============================================================*/
create table QUO_TYPE  (
   QUO_TYPE_PK          NUMBER                          not null,
   NAME                 VARCHAR2(16),
   DESCRIPTION          VARCHAR2(64),
   constraint PK_QUO_TYPE primary key (QUO_TYPE_PK)
);

/*==============================================================*/
/* Table: QUO_TYPE_SER                                          */
/*==============================================================*/
create table QUO_TYPE_SER  (
   QUO_TYPE_FK_PK       NUMBER                          not null,
   SER_SERVICE_FK_PK    NUMBER                          not null,
   constraint PK_QUO_TYPE_SER primary key (QUO_TYPE_FK_PK, SER_SERVICE_FK_PK)
);

/*==============================================================*/
/* Table: REG_REGISTRATION                                      */
/*==============================================================*/
create table REG_REGISTRATION  (
   CON_CONTENT_FK_PK    NUMBER                          not null,
   DATE_START           DATE,
   DATE_STOP            DATE,
   NO_SPOTS             NUMBER,
   BOOL_EMAIL_NOTIFICATION NUMBER(1),
   DATE_INSERT          DATE,
   DATE_UPDATE          DATE,
   constraint PK_REG_REGISTRATION primary key (CON_CONTENT_FK_PK)
);

/*==============================================================*/
/* Table: REG_STATUS                                            */
/*==============================================================*/
create table REG_STATUS  (
   REG_STATUS_PK        NUMBER                          not null,
   NAME                 VARCHAR2(16),
   DESCRIPTION          VARCHAR2(64),
   constraint PK_REG_STATUS primary key (REG_STATUS_PK)
);

/*==============================================================*/
/* Table: REG_USE                                               */
/*==============================================================*/
create table REG_USE  (
   CON_CONTENT_FK_PK    NUMBER                          not null,
   USE_USER_FK_PK       NUMBER                          not null,
   REG_STATUS_FK        NUMBER,
   DATE_INSERT          DATE,
   constraint PK_REG_USE primary key (CON_CONTENT_FK_PK, USE_USER_FK_PK)
);

/*==============================================================*/
/* Table: REP_PROJECT                                           */
/*==============================================================*/
create table REP_PROJECT  (
   REP_PROJECT_PK       NUMBER                          not null,
   NAME                 VARCHAR2(16),
   DESCRIPTION          VARCHAR2(64),
   BOOL_ACTIVE          NUMBER(1),
   BOOL_STATISTIC       NUMBER(1),
   DATE_INSERT          DATE,
   DATE_UPDATE          DATE,
   USE_USER_FK          NUMBER,
   constraint PK_REP_PROJECT primary key (REP_PROJECT_PK)
);

/*==============================================================*/
/* Table: REP_REPORT                                            */
/*==============================================================*/
create table REP_REPORT  (
   REP_REPORT_PK        NUMBER                          not null,
   REP_PROJECT_FK       NUMBER,
   USE_USER_FK          NUMBER,
   DURATION             NUMBER,
   DATE_REPORT          DATE,
   "COMMENT"            VARCHAR2(128),
   FEEDBACK             VARCHAR2(256),
   BOOL_APPROVED        NUMBER(1),
   BOOL_SUBMITTED       NUMBER(1),
   DATE_INSERT          DATE,
   DATE_UPDATE          DATE,
   constraint PK_REP_REPORT primary key (REP_REPORT_PK)
);

/*==============================================================*/
/* Table: SER_SERVICE                                           */
/*==============================================================*/
create table SER_SERVICE  (
   SER_SERVICE_PK       NUMBER                          not null,
   NAME                 VARCHAR2(16),
   DOMAIN               VARCHAR2(32),
   BOOL_ACTIVE          NUMBER(1),
   constraint PK_SER_SERVICE primary key (SER_SERVICE_PK)
);

/*==============================================================*/
/* Table: SES_INSTANCE                                          */
/*==============================================================*/
create table SES_INSTANCE  (
   SES_INSTANCE_PK      NUMBER                          not null,
   SES_SESSION_FK       NUMBER,
   MOD_MODULE_NAME      VARCHAR2(128),
   PARAMETERS           VARCHAR2(4000),
   DATE_INSERT          DATE,
   constraint PK_SES_INSTANCE primary key (SES_INSTANCE_PK)
);

/*==============================================================*/
/* Table: SES_SESSION                                           */
/*==============================================================*/
create table SES_SESSION  (
   SES_SESSION_PK       NUMBER                          not null,
   ID                   NUMBER,
   USE_USER_FK          NUMBER,
   ORG_ORGANISATION_FK  NUMBER,
   TEX_LANGUAGE_FK      VARCHAR2(3),
   SER_SERVICE_FK       NUMBER,
   IDLE_TIME            NUMBER,
   HOST                 VARCHAR2(64),
   IP                   VARCHAR2(32),
   DEBUG_MODE           NUMBER(1),
   DATE_INSERT          DATE,
   DATE_UPDATE          DATE,
   constraint PK_SES_SESSION primary key (SES_SESSION_PK)
);

alter table SES_SESSION
    add constraint UNIQUE_SES_SESSION_ID UNIQUE (ID);
/*==============================================================*/
/* Table: SUB_SELECTION                                         */
/*==============================================================*/
create table SUB_SELECTION  (
   SUB_SELECTION_PK     NUMBER                          not null,
   USE_USER_FK          NUMBER,
   QUERY                VARCHAR2(512),
   NO_ORG_LAST_RUN      NUMBER,
   DATE_LAST_RUN        DATE,
   DATE_INSERT          DATE,
   DATE_UPDATE          DATE,
   constraint PK_SUB_SELECTION primary key (SUB_SELECTION_PK)
);

/*==============================================================*/
/* Table: SUB_SUBSCRIPTION                                      */
/*==============================================================*/
create table SUB_SUBSCRIPTION  (
   USE_USER_FK_PK       NUMBER                          not null,
   EMAIL                VARCHAR2(128),
   SMS                  VARCHAR2(16),
   XML_URL              VARCHAR2(128),
   XML_EMAIL            VARCHAR2(128),
   DATE_DELIVERY_TIMEOFDAY DATE,
   BOOL_DELIVER_IMMEDIATE NUMBER(1),
   BOOL_DELIVER_MONDAY  NUMBER(1),
   BOOL_DELIVER_TUESDAY NUMBER(1),
   BOOL_DELIVER_WEDNESDAY NUMBER(1),
   BOOL_DELIVER_THURSDAY NUMBER(1),
   BOOL_DELIVER_FRIDAY  NUMBER(1),
   BOOL_DELIVER_SATURDAY NUMBER(1),
   BOOL_DELIVER_SUNDAY  NUMBER(1),
   DATE_INSERT          DATE,
   DATE_UPDATE          DATE,
   constraint PK_SUB_SUBSCRIPTION primary key (USE_USER_FK_PK)
);

/*==============================================================*/
/* Table: SYS_PARAMETER                                         */
/*==============================================================*/
create table SYS_PARAMETER  (
   SYS_PARAMETER_PK     NUMBER                          not null,
   NAME                 VARCHAR2(16),
   DESCRIPTION          VARCHAR2(64),
   VALUE                VARCHAR2(128),
   SYS_PARAMETER_TYPE_FK NUMBER,
   USE_USER_FK          NUMBER,
   DATE_INSERT          DATE,
   DATE_UPDATE          DATE,
   constraint PK_SYS_PARAMETER primary key (SYS_PARAMETER_PK)
);

/*==============================================================*/
/* Table: SYS_PARAMETER_TYPE                                    */
/*==============================================================*/
create table SYS_PARAMETER_TYPE  (
   SYS_PARAMETER_TYPE_PK NUMBER                          not null,
   NAME                 VARCHAR2(16),
   DESCRIPTION          VARCHAR2(64),
   constraint PK_SYS_PARAMETER_TYPE primary key (SYS_PARAMETER_TYPE_PK)
);

/*==============================================================*/
/* Table: TEX_LANGUAGE                                          */
/*==============================================================*/
create table TEX_LANGUAGE  (
   TEX_LANGUAGE_PK      VARCHAR2(3)                     not null,
   LANGUAGE_ENG         VARCHAR2(32),
   LANGUAGE_ORG         VARCHAR2(32),
   constraint PK_TEX_LANGUAGE primary key (TEX_LANGUAGE_PK)
);

/*==============================================================*/
/* Table: TEX_TEXT                                              */
/*==============================================================*/
create table TEX_TEXT  (
   TEX_TEXT_PK          VARCHAR2(128)                   not null,
   TEX_LANGUAGE_FK_PK   VARCHAR2(3)                     not null,
   STRING               VARCHAR2(4000),
   constraint PK_TEX_TEXT primary key (TEX_TEXT_PK, TEX_LANGUAGE_FK_PK)
);

/*==============================================================*/
/* Table: USE_DETAILS                                           */
/*==============================================================*/
create table USE_DETAILS  (
   USE_USER_FK_PK       NUMBER                          not null,
   FIRST_NAME           VARCHAR2(64),
   LAST_NAME            VARCHAR2(64),
   ADD_ADDRESS_FK       NUMBER,
   DATE_OF_BIRTH        DATE,
   SEX                  VARCHAR2(1),
   COMMENT_STATUS_CHANGE VARCHAR2(1024),
   DATE_STATUS_CHANGE   DATE,
   DATE_INSERT          DATE,
   DATE_UPDATE          DATE,
   constraint PK_USE_DETAILS primary key (USE_USER_FK_PK)
);

/*==============================================================*/
/* Table: USE_LOGIN_STATUS                                      */
/*==============================================================*/
create table USE_LOGIN_STATUS  (
   USE_USER_FK_PK       NUMBER                          not null,
   NO_SUCCESSFUL_LOGINS NUMBER,
   NO_FAILED_LOGINS     NUMBER,
   NO_FAILED_NOW        NUMBER,
   constraint PK_USE_LOGIN_STATUS primary key (USE_USER_FK_PK)
);

/*==============================================================*/
/* Table: USE_ORG                                               */
/*==============================================================*/
create table USE_ORG  (
   USE_USER_FK_PK       NUMBER                          not null,
   ORG_ORGANISATION_FK_PK NUMBER                          not null,
   constraint PK_USE_ORG primary key (USE_USER_FK_PK, ORG_ORGANISATION_FK_PK)
);

/*==============================================================*/
/* Table: USE_STATUS                                            */
/*==============================================================*/
create table USE_STATUS  (
   USE_STATUS_PK        NUMBER                          not null,
   NAME                 VARCHAR2(16),
   DESCRIPTION          VARCHAR2(64),
   constraint PK_USE_STATUS primary key (USE_STATUS_PK)
);

/*==============================================================*/
/* Table: USE_TYPE                                              */
/*==============================================================*/
create table USE_TYPE  (
   USE_TYPE_PK          NUMBER                          not null,
   NAME                 VARCHAR2(16),
   DESCRIPTION          VARCHAR2(64),
   IDLE_TIME            NUMBER,
   WRONG_LOGINS         NUMBER,
   WEIGHT               NUMBER,
   constraint PK_USE_TYPE primary key (USE_TYPE_PK)
);

/*==============================================================*/
/* Table: USE_USER                                              */
/*==============================================================*/
create table USE_USER  (
   USE_USER_PK          NUMBER                          not null,
   USERNAME             VARCHAR2(32),
   EMAIL                VARCHAR2(128),
   PASSWORD             VARCHAR2(32),
   USE_TYPE_FK          NUMBER,
   USE_STATUS_FK        NUMBER,
   constraint PK_USE_USER primary key (USE_USER_PK)
);

alter table USE_USER
    add constraint UNIQUE_USE_USER_EMAIL UNIQUE (EMAIL);

alter table USE_USER
    add constraint UNIQUE_USE_USER_USERNAME UNIQUE (USERNAME);

alter table ADD_ADDRESS
   add constraint FK_ADD_ADDR_REFERENCE_GEO_GEOG foreign key (GEO_GEOGRAPHY_FK)
      references GEO_GEOGRAPHY (GEO_GEOGRAPHY_PK)
      on delete cascade;

alter table BIL_ITEM
   add constraint FK_BIL_ITEM_REFERENCE_BIL_TRAD foreign key (BIL_TRADE_TYPE_FK)
      references BIL_TRADE_TYPE (BIL_TRADE_TYPE_PK)
      on delete cascade;

alter table BIL_ITEM
   add constraint FK_BIL_ITEM_REFERENCE_BIL_ITEM foreign key (BIL_ITEM_TYPE_FK)
      references BIL_ITEM_TYPE (BIL_ITEM_TYPE_PK)
      on delete cascade;

alter table BIL_ITEM
   add constraint FK_BIL_ITEM_REFERENCE_BIL_PAYM foreign key (BIL_PAYMENT_METHOD_FK)
      references BIL_PAYMENT_METHOD (BIL_PAYMENT_METHOD_PK)
      on delete cascade;

alter table BIL_ITEM
   add constraint FK_BIL_ITEM_REFERENCE_USE_USER foreign key (USE_USER_FK)
      references USE_USER (USE_USER_PK)
      on delete cascade;

alter table COM_ORG
   add constraint FK_COM_ORG_REFERENCE_COM_COMP foreign key (COM_COMPARE_FK)
      references COM_COMPARE (COM_COMPARE_PK)
      on delete cascade;

alter table COM_ORG
   add constraint FK_COM_ORG_REFERENCE_ORG_ORGA foreign key (ORG_ORGANISATION_FK_PK)
      references ORG_ORGANISATION (ORG_ORGANISATION_PK)
      on delete cascade;

alter table CON_COMMENT
   add constraint FK_CON_COMM_REFERENCE_CON_CONT foreign key (CON_CONTENT_FK)
      references CON_CONTENT (CON_CONTENT_PK)
      on delete cascade;

alter table CON_COMMENT
   add constraint FK_CON_COMM_REFERENCE_USE_USER foreign key (USE_USER_FK)
      references USE_USER (USE_USER_PK)
      on delete cascade;

alter table CON_CONTENT
   add constraint FK_CON_CONT_REFERENCE_CON_TYPE foreign key (CON_TYPE_FK)
      references CON_TYPE (CON_TYPE_PK)
      on delete cascade;

alter table CON_CONTENT
   add constraint FK_CON_CONT_REFERENCE_CON_STAT foreign key (CON_STATUS_FK)
      references CON_STATUS (CON_STATUS_PK)
      on delete cascade;

alter table CON_CONTENT
   add constraint FK_CON_CONT_REFERENCE_ORG_ORGA foreign key (ORG_ORGANISATION_FK)
      references ORG_ORGANISATION (ORG_ORGANISATION_PK)
      on delete cascade;

alter table CON_CONTENT
   add constraint FK_CON_CONT_REFERENCE_GEO_GEOG foreign key (GEO_GEOGRAPHY_FK)
      references GEO_GEOGRAPHY (GEO_GEOGRAPHY_PK)
      on delete cascade;

alter table CON_CONTENT
   add constraint FK_CON_CONT_REFERENCE_CON_SUBT foreign key (CON_SUBTYPE_FK)
      references CON_SUBTYPE (CON_SUBTYPE_PK)
      on delete cascade;

alter table CON_CONTENT_FILE
   add constraint FK_CON_CONT_REFERENCE_CON_CONT foreign key (CON_CONTENT_FK_PK)
      references CON_CONTENT (CON_CONTENT_PK)
      on delete cascade;

alter table CON_CONTENT_FILE
   add constraint FK_CON_CONT_REFERENCE_CON_FILE foreign key (CON_FILE_FK_PK)
      references CON_FILE (CON_FILE_PK)
      on delete cascade;

alter table CON_FILE
   add constraint FK_CON_FILE_REFERENCE_CON_FILE foreign key (CON_FILE_TYPE_FK)
      references CON_FILE_TYPE (CON_FILE_TYPE_PK)
      on delete cascade;

alter table CON_RELATION
   add constraint FK_CON_RELA_REFERENCE_CON_CON1 foreign key (CON_CONTENT_1_FK_PK)
      references CON_CONTENT (CON_CONTENT_PK)
      on delete cascade;

alter table CON_RELATION
   add constraint FK_CON_RELA_REFERENCE_CON_CON2 foreign key (CON_CONTENT_2_FK_PK)
      references CON_CONTENT (CON_CONTENT_PK)
      on delete cascade;

alter table CON_SER
   add constraint FK_CON_SER_REFERENCE_CON_CONT foreign key (CON_CONTENT_FK_PK)
      references CON_CONTENT (CON_CONTENT_PK)
      on delete cascade;

alter table CON_SER
   add constraint FK_CON_SER_REFERENCE_SER_SERV foreign key (SER_SERVICE_FK_PK)
      references SER_SERVICE (SER_SERVICE_PK)
      on delete cascade;

alter table CON_URL
   add constraint FK_CON_URL_REFERENCE_CON_CONT foreign key (CON_CONTENT_FK)
      references CON_CONTENT (CON_CONTENT_PK)
      on delete cascade;

alter table CON_VOTE
   add constraint FK_CON_VOTE_REFERENCE_CON_CONT foreign key (CON_CONTENT_FK)
      references CON_CONTENT (CON_CONTENT_PK)
      on delete cascade;

alter table CON_VOTE
   add constraint FK_CON_VOTE_REFERENCE_USE_USER foreign key (USE_USER_FK)
      references USE_USER (USE_USER_PK)
      on delete cascade;

alter table FOR_FORUM
   add constraint FK_FOR_FORU_REFERENCE_FOR_FORU foreign key (PARENT_FOR_FORUM_FK)
      references FOR_FORUM (FOR_FORUM_PK)
      on delete cascade;

alter table FOR_FORUM
   add constraint FK_FOR_FORU_REFERENCE_FOR_TYPE foreign key (FOR_TYPE_FK)
      references FOR_TYPE (FOR_TYPE_PK)
      on delete cascade;

alter table FOR_TYPE
   add constraint FK_FOR_TYPE_REFERENCE_SER_SERV foreign key (SER_SERVICE_FK)
      references SER_SERVICE (SER_SERVICE_PK)
      on delete cascade;

alter table FOR_TYPE
   add constraint FK_FOR_TYPE_REFERENCE_ORG_ORGA foreign key (ORG_ORGANISATION_FK)
      references ORG_ORGANISATION (ORG_ORGANISATION_PK)
      on delete cascade;

alter table FOR_USE
   add constraint FK_FOR_USE_REFERENCE_FOR_FORU foreign key (FOR_FORUM_FK_PK)
      references FOR_FORUM (FOR_FORUM_PK)
      on delete cascade;

alter table FOR_USE
   add constraint FK_FOR_USE_REFERENCE_USE_USER foreign key (USE_USER_FK_PK)
      references USE_USER (USE_USER_PK)
      on delete cascade;

alter table GEO_GEOGRAPHY
   add constraint FK_GEO_GEOG_REFERENCE_GEO_GEOG foreign key (PARENT_GEO_GEOGRAPHY_FK)
      references GEO_GEOGRAPHY (GEO_GEOGRAPHY_PK)
      on delete cascade;

alter table GEO_ZIP
   add constraint FK_GEO_ZIP_REFERENCE_GEO_GEOG foreign key (GEO_GEOGRAPHY_FK_PK)
      references GEO_GEOGRAPHY (GEO_GEOGRAPHY_PK)
      on delete cascade;

alter table HIS_HISTORY
   add constraint FK_HIS_HIST_REFERENCE_USE_USER foreign key (USE_USER_FK)
      references USE_USER (USE_USER_PK)
      on delete cascade;

alter table LIS_LIST
   add constraint FK_LIS_LIST_REFERENCE_LIS_TYPE foreign key (LIS_TYPE_FK)
      references LIS_TYPE (LIS_TYPE_PK)
      on delete cascade;

alter table LIS_ORG
   add constraint FK_LIS_ORG_REFERENCE_LIS_LIST foreign key (LIS_LIST_FK_PK)
      references LIS_LIST (LIS_LIST_PK)
      on delete cascade;

alter table LIS_ORG
   add constraint FK_LIS_ORG_REFERENCE_ORG_ORGA foreign key (ORG_ORGANISATION_FK_PK)
      references ORG_ORGANISATION (ORG_ORGANISATION_PK)
      on delete cascade;

alter table LOG_CON_EMAIL
   add constraint FK_LOG_CON__REFERENCE_CON_CONT foreign key (CON_CONTENT_FK)
      references CON_CONTENT (CON_CONTENT_PK)
      on delete cascade;

alter table LOG_LOGIN
   add constraint FK_LOG_LOGI_REFERENCE_USE_USER foreign key (USE_USER_FK)
      references USE_USER (USE_USER_PK)
      on delete cascade;

alter table LOG_SUB
   add constraint FK_LOG_SUB_REFERENCE_CON_CONT foreign key (CON_CONTENT_FK)
      references CON_CONTENT (CON_CONTENT_PK)
      on delete cascade;

alter table LOG_SUB
   add constraint FK_LOG_SUB_REFERENCE_USE_USER foreign key (USE_USER_FK)
      references USE_USER (USE_USER_PK)
      on delete cascade;

alter table MAR_STUNT
   add constraint FK_MAR_STUN_REFERENCE_MAR_STAT foreign key (MAR_STATUS_FK)
      references MAR_STATUS (MAR_STATUS_PK)
      on delete cascade;

alter table MAR_STUNT
   add constraint FK_MAR_STUN_REFERENCE_MAR_TARG foreign key (MAR_TARGET_FK)
      references MAR_TARGET (MAR_TARGET_PK)
      on delete cascade;

alter table MAR_STUNT
   add constraint FK_MAR_STUN_REFERENCE_MAR_STUN foreign key (MAR_STUNT_TYPE_FK)
      references MAR_STUNT_TYPE (MAR_STUNT_TYPE_PK)
      on delete cascade;

alter table MAR_STUNT
   add constraint FK_MAR_STUN_REFERENCE_USE_USER foreign key (INSERT_USE_USER_FK)
      references USE_USER (USE_USER_PK)
      on delete cascade;

alter table MAR_TARGET
   add constraint FK_MAR_TARG_REFERENCE_ADD_ADDR foreign key (ADD_ADDRESS_FK)
      references ADD_ADDRESS (ADD_ADDRESS_PK)
      on delete cascade;

alter table MAR_TARGET
   add constraint FK_MAR_TARG_REFERENCE_ORG_ORGA foreign key (ORG_ORGANISATION_FK)
      references ORG_ORGANISATION (ORG_ORGANISATION_PK)
      on delete cascade;

alter table MAR_TARGET
   add constraint FK_MAR_TARG_REFERENCE_USE_USER foreign key (INSERT_USE_USER_FK)
      references USE_USER (USE_USER_PK)
      on delete cascade;

alter table MEM_MOD
   add constraint FK_MEM_MOD_REFERENCE_MOD_MODU foreign key (MOD_MODULE_FK_PK)
      references MOD_MODULE (MOD_MODULE_PK)
      on delete cascade;

alter table MEM_MOD
   add constraint FK_MEM_MOD_REFERENCE_MEM_MEMB foreign key (MEM_MEMBERSHIP_FK_PK)
      references MEM_MEMBERSHIP (MEM_MEMBERSHIP_PK)
      on delete cascade;

alter table MEM_ORG
   add constraint FK_MEM_ORG_REFERENCE_ORG_ORGA foreign key (ORG_ORGANISATION_FK_PK)
      references ORG_ORGANISATION (ORG_ORGANISATION_PK)
      on delete cascade;

alter table MEM_ORG
   add constraint FK_MEM_ORG_REFERENCE_MEM_MEMB foreign key (MEM_MEMBERSHIP_FK_PK)
      references MEM_MEMBERSHIP (MEM_MEMBERSHIP_PK)
      on delete cascade;

alter table MEN_MENU
   add constraint FK_MEN_MENU_REFERENCE_MEN_TYPE foreign key (MENU_TYPE_FK)
      references MEN_TYPE (MEN_TYPE_PK)
      on delete cascade;

alter table MEN_MENU
   add constraint FK_MEN_MENU_REFERENCE_ORG_ORGA foreign key (ORG_ORGANISATION_FK)
      references ORG_ORGANISATION (ORG_ORGANISATION_PK)
      on delete cascade;

alter table MEN_OBJECT
   add constraint FK_MEN_OBJE_REFERENCE_MEN_MENU foreign key (MEN_MENU_FK)
      references MEN_MENU (MEN_MENU_PK)
      on delete cascade;

alter table MEN_OBJECT
   add constraint FK_MEN_OBJE_REFERENCE_MEN_OBJE foreign key (MEN_OBJECT_TYPE_FK)
      references MEN_OBJECT_TYPE (MEN_OBJECT_TYPE_PK)
      on delete cascade;

alter table MEN_OBJECT
   add constraint FK_MEN_OBJE_REFERENCE_MEN_SERV foreign key (MEN_SERVING_TYPE_FK)
      references MEN_SERVING_TYPE (MEN_SERVING_TYPE_PK)
      on delete cascade;

alter table MEN_OBJECT_SERVING
   add constraint FK_MEN_OBJE_REFERENCE_MEN_ORG_ foreign key (MEN_ORG_SERVING_FK_PK)
      references MEN_ORG_SERVING (MEN_ORG_SERVING_PK)
      on delete cascade;

alter table MEN_OBJECT_SERVING
   add constraint FK_MEN_OBJE_REFERENCE_MEN_OBJ3 foreign key (MEN_OBJECT_FK_PK)
      references MEN_OBJECT (MEN_OBJECT_PK)
      on delete cascade;

alter table MEN_OBJECT_TYPE
   add constraint FK_MEN_OBJE_REFERENCE_MEN_TYPE foreign key (MEN_TYPE_FK)
      references MEN_TYPE (MEN_TYPE_PK)
      on delete cascade;

alter table MEN_ORG_SERVING
   add constraint FK_MEN_ORG__REFERENCE_MEN_SERV foreign key (MEN_SERVING_PERIOD_FK)
      references MEN_SERVING_PERIOD (MEN_SERVING_PERIOD_PK)
      on delete cascade;

alter table MEN_ORG_SERVING
   add constraint FK_MEN_ORG__REFERENCE_ORG_ORGA foreign key (ORG_ORGANISATION_FK)
      references ORG_ORGANISATION (ORG_ORGANISATION_PK)
      on delete cascade;

alter table MEN_PRICE
   add constraint FK_MEN_PRIC_REFERENCE_MEN_OBJ2 foreign key (MENU_OBJECT_FK)
      references MEN_OBJECT (MEN_OBJECT_PK)
      on delete cascade;

alter table MOD_MODULE
   add constraint FK_MOD_MODU_REFERENCE_MOD_PROC foreign key (MOD_PROCEDURE_FK)
      references MOD_PROCEDURE (MOD_PROCEDURE_PK)
      on delete cascade;

alter table MOD_MODULE
   add constraint FK_MOD_MODU_REFERENCE_MOD_PACK foreign key (MOD_PACKAGE_FK)
      references MOD_PACKAGE (MOD_PACKAGE_PK)
      on delete cascade;

alter table MOD_PERMISSION
   add constraint FK_MOD_PERM_REFERENCE_MOD_MODU foreign key (MOD_MODULE_FK_PK)
      references MOD_MODULE (MOD_MODULE_PK)
      on delete cascade;

alter table MOD_PERMISSION
   add constraint FK_MOD_PERM_REFERENCE_USE_USER foreign key (USE_USER_FK_PK)
      references USE_USER (USE_USER_PK)
      on delete cascade;

alter table MOD_RESTRICTION
   add constraint FK_MOD_REST_REFERENCE_MOD_MODU foreign key (MOD_MODULE_FK_PK)
      references MOD_MODULE (MOD_MODULE_PK)
      on delete cascade;

alter table MOD_RESTRICTION
   add constraint FK_MOD_REST_REFERENCE_USE_TYPE foreign key (USE_TYPE_FK_PK)
      references USE_TYPE (USE_TYPE_PK)
      on delete cascade;

alter table ORG_CHOSEN
   add constraint FK_ORG_CHOS_REFERENCE_SER_SERV foreign key (SER_SERVICE_FK_PK)
      references SER_SERVICE (SER_SERVICE_PK)
      on delete cascade;

alter table ORG_CHOSEN
   add constraint FK_ORG_CHOS_REFERENCE_ORG_ORGA foreign key (ORG_ORGANISATION_FK_PK)
      references ORG_ORGANISATION (ORG_ORGANISATION_PK)
      on delete cascade;

alter table ORG_HOURS
   add constraint FK_ORG_HOUR_REFERENCE_ORG_ORGA foreign key (ORG_ORGANISATION_FK_PK)
      references ORG_ORGANISATION (ORG_ORGANISATION_PK)
      on delete cascade;

alter table ORG_ORGANISATION
   add constraint FK_ORG_ORGA_REFERENCE_ORG_ORG1 foreign key (PARENT_ORG_ORGANISATION_FK)
      references ORG_ORGANISATION (ORG_ORGANISATION_PK)
      on delete cascade;

alter table ORG_ORGANISATION
   add constraint FK_ORG_ORGA_REFERENCE_ORG_STAT foreign key (ORG_STATUS_FK)
      references ORG_STATUS (ORG_STATUS_PK)
      on delete cascade;

alter table ORG_ORGANISATION
   add constraint FK_ORG_ORGA_REFERENCE_ADD_ADDR foreign key (ADD_ADDRESS_FK)
      references ADD_ADDRESS (ADD_ADDRESS_PK)
      on delete cascade;

alter table ORG_ORGANISATION_RIGHTS
   add constraint FK_ORG_ORGA_REFERENCE_ORG_ORG2 foreign key (ORG_ORGANISATION_FK_PK)
      references ORG_ORGANISATION (ORG_ORGANISATION_PK)
      on delete cascade;

alter table ORG_ORGANISATION_RIGHTS
   add constraint FK_ORG_ORGA_REFERENCE_ORG_RIGH foreign key (ORG_RIGHTS_FK_PK)
      references ORG_RIGHTS (ORG_RIGHTS_PK)
      on delete cascade;

alter table ORG_ORIGINAL
   add constraint FK_ORG_ORIG_REFERENCE_ORG_ORGA foreign key (ORG_ORGANISATION_FK_PK)
      references ORG_ORGANISATION (ORG_ORGANISATION_PK)
      on delete cascade;

alter table ORG_SER
   add constraint FK_ORG_SER_REFERENCE_ORG_ORGA foreign key (ORG_ORGANISATION_FK_PK)
      references ORG_ORGANISATION (ORG_ORGANISATION_PK)
      on delete cascade;

alter table ORG_SER
   add constraint FK_ORG_SER_REFERENCE_SER_SERV foreign key (SER_SERVICE_FK_PK)
      references SER_SERVICE (SER_SERVICE_PK)
      on delete cascade;

alter table ORG_VALUE
   add constraint FK_ORG_VALU_REFERENCE_ORG_VALU foreign key (ORG_VALUE_TYPE_FK_PK)
      references ORG_VALUE_TYPE (ORG_VALUE_TYPE_PK)
      on delete cascade;

alter table ORG_VALUE
   add constraint FK_ORG_VALU_REFERENCE_ORG_ORGA foreign key (ORG_ORGANISATION_FK_PK)
      references ORG_ORGANISATION (ORG_ORGANISATION_PK)
      on delete cascade;

alter table PIC_ALBUM
   add constraint FK_PIC_ALBU_REFERENCE_ORG_ORGA foreign key (ORG_ORGANISATION_FK)
      references ORG_ORGANISATION (ORG_ORGANISATION_PK)
      on delete cascade;

alter table PIC_ALBUM_PICTURE
   add constraint FK_PIC_ALBU_REFERENCE_PIC_ALBU foreign key (PIC_ALBUM_FK_PK)
      references PIC_ALBUM (PIC_ALBUM_PK)
      on delete cascade;

alter table PIC_ALBUM_PICTURE
   add constraint FK_PIC_ALBU_REFERENCE_PIC_PICT foreign key (PIC_PICTURE_FK_PK)
      references PIC_PICTURE (PIC_PICTURE_PK)
      on delete cascade;

alter table PIC_COMMENT
   add constraint FK_PIC_COMM_REFERENCE_PIC_PICT foreign key (PIC_PICTURE_FK)
      references PIC_PICTURE (PIC_PICTURE_PK)
      on delete cascade;

alter table PIC_VOTE
   add constraint FK_PIC_VOTE_REFERENCE_PIC_PICT foreign key (PIC_PICTURE_FK)
      references PIC_PICTURE (PIC_PICTURE_PK)
      on delete cascade;

alter table POLL_ANSWER
   add constraint FK_POLL_ANS_REFERENCE_USE_USER foreign key (USE_USER_FK)
      references USE_USER (USE_USER_PK)
      on delete cascade;

alter table POLL_ANSWER
   add constraint FK_POLL_ANS_REFERENCE_POL_ALTE foreign key (POL_ALTERNATIVE_FK)
      references POL_ALTERNATIVE (POL_ALTERNATIVE_PK)
      on delete cascade;

alter table POL_ALTERNATIVE
   add constraint FK_POL_ALTE_REFERENCE_POL_POLL foreign key (POL_POLL_FK)
      references POL_POLL (POL_POLL_PK)
      on delete cascade;

alter table POL_POLL
   add constraint FK_POL_POLL_REFERENCE_POL_TYPE foreign key (POL_TYPE_FK)
      references POL_TYPE (POL_TYPE_PK)
      on delete cascade;

alter table POL_TYPE
   add constraint FK_POL_TYPE_REFERENCE_SER_SERV foreign key (SER_SERVICE_FK)
      references SER_SERVICE (SER_SERVICE_PK)
      on delete cascade;

alter table QUO_QUOTE
   add constraint FK_QUO_QUOT_REFERENCE_QUO_TYPE foreign key (QUO_TYPE_FK)
      references QUO_TYPE (QUO_TYPE_PK)
      on delete cascade;

alter table QUO_TYPE_SER
   add constraint FK_QUO_TYPE_REFERENCE_QUO_TYPE foreign key (QUO_TYPE_FK_PK)
      references QUO_TYPE (QUO_TYPE_PK)
      on delete cascade;

alter table QUO_TYPE_SER
   add constraint FK_QUO_TYPE_REFERENCE_SER_SERV foreign key (SER_SERVICE_FK_PK)
      references SER_SERVICE (SER_SERVICE_PK)
      on delete cascade;

alter table REG_REGISTRATION
   add constraint FK_REG_REGI_REFERENCE_CON_CONT foreign key (CON_CONTENT_FK_PK)
      references CON_CONTENT (CON_CONTENT_PK)
      on delete cascade;

alter table REG_USE
   add constraint FK_REG_USE_REFERENCE_REG_REGI foreign key (CON_CONTENT_FK_PK)
      references REG_REGISTRATION (CON_CONTENT_FK_PK)
      on delete cascade;

alter table REG_USE
   add constraint FK_REG_USE_REFERENCE_USE_USER foreign key (USE_USER_FK_PK)
      references USE_USER (USE_USER_PK)
      on delete cascade;

alter table REG_USE
   add constraint FK_REG_USE_REFERENCE_REG_STAT foreign key (REG_STATUS_FK)
      references REG_STATUS (REG_STATUS_PK)
      on delete cascade;

alter table REP_PROJECT
   add constraint FK_REP_PROJ_REFERENCE_USE_USER foreign key (USE_USER_FK)
      references USE_USER (USE_USER_PK)
      on delete cascade;

alter table REP_REPORT
   add constraint FK_REP_REPO_REFERENCE_REP_PROJ foreign key (REP_PROJECT_FK)
      references REP_PROJECT (REP_PROJECT_PK)
      on delete cascade;

alter table SES_INSTANCE
   add constraint FK_SES_INST_REFERENCE_SES_SESS foreign key (SES_SESSION_FK)
      references SES_SESSION (SES_SESSION_PK)
      on delete cascade;

alter table SES_SESSION
   add constraint FK_SES_SESS_REFERENCE_ORG_ORGA foreign key (ORG_ORGANISATION_FK)
      references ORG_ORGANISATION (ORG_ORGANISATION_PK)
      on delete cascade;

alter table SES_SESSION
   add constraint FK_SES_SESS_REFERENCE_TEX_LANG foreign key (TEX_LANGUAGE_FK)
      references TEX_LANGUAGE (TEX_LANGUAGE_PK)
      on delete cascade;

alter table SES_SESSION
   add constraint FK_SES_SESS_REFERENCE_SER_SERV foreign key (SER_SERVICE_FK)
      references SER_SERVICE (SER_SERVICE_PK)
      on delete cascade;

alter table SES_SESSION
   add constraint FK_SES_SESS_REFERENCE_USE_USER foreign key (USE_USER_FK)
      references USE_USER (USE_USER_PK)
      on delete cascade;

alter table SUB_SELECTION
   add constraint FK_SUB_SELE_REFERENCE_SUB_SUBS foreign key (USE_USER_FK)
      references SUB_SUBSCRIPTION (USE_USER_FK_PK)
      on delete cascade;

alter table SUB_SUBSCRIPTION
   add constraint FK_SUB_SUBS_REFERENCE_USE_USER foreign key (USE_USER_FK_PK)
      references USE_USER (USE_USER_PK)
      on delete cascade;

alter table SYS_PARAMETER
   add constraint FK_SYS_PARA_REFERENCE_SYS_PARA foreign key (SYS_PARAMETER_TYPE_FK)
      references SYS_PARAMETER_TYPE (SYS_PARAMETER_TYPE_PK)
      on delete cascade;

alter table SYS_PARAMETER
   add constraint FK_SYS_PARA_REFERENCE_USE_USER foreign key (USE_USER_FK)
      references USE_USER (USE_USER_PK)
      on delete cascade;

alter table TEX_TEXT
   add constraint FK_TEX_TEXT_REFERENCE_TEX_LANG foreign key (TEX_LANGUAGE_FK_PK)
      references TEX_LANGUAGE (TEX_LANGUAGE_PK)
      on delete cascade;

alter table USE_DETAILS
   add constraint FK_USE_DETA_REFERENCE_USE_USER foreign key (USE_USER_FK_PK)
      references USE_USER (USE_USER_PK)
      on delete cascade;

alter table USE_DETAILS
   add constraint FK_USE_DETA_REFERENCE_ADD_ADDR foreign key (ADD_ADDRESS_FK)
      references ADD_ADDRESS (ADD_ADDRESS_PK)
      on delete cascade;

alter table USE_LOGIN_STATUS
   add constraint FK_USE_LOGI_REFERENCE_USE_USE2 foreign key (USE_USER_FK_PK)
      references USE_USER (USE_USER_PK)
      on delete cascade;

alter table USE_ORG
   add constraint FK_USE_ORG_REFERENCE_USE_USER foreign key (USE_USER_FK_PK)
      references USE_USER (USE_USER_PK)
      on delete cascade;

alter table USE_USER
   add constraint FK_USE_USER_REFERENCE_USE_TYPE foreign key (USE_TYPE_FK)
      references USE_TYPE (USE_TYPE_PK)
      on delete cascade;

alter table USE_USER
   add constraint FK_USE_USER_REFERENCE_USE_STAT foreign key (USE_STATUS_FK)
      references USE_STATUS (USE_STATUS_PK)
      on delete cascade;



create or replace directory DIR_IMG_ORG as 'C:\Informant\www\img\org';
grant read, write on directory DIR_IMG_ORG to public;

grant create any directory to i4;

