truncate table ADD_ADDRESS;

truncate table BIL_ITEM;

truncate table BIL_ITEM_TYPE;

truncate table BIL_PAYMENT_METHOD;

truncate table BIL_TRADE_TYPE;

truncate table COM_COMPARE;

truncate table COM_ORG;

truncate table CON_COMMENT;

truncate table CON_CONTENT;

truncate table CON_CONTENT_FILE;

truncate table CON_FILE;

truncate table CON_FILE_TYPE;

truncate table CON_RELATION;

truncate table CON_SER;

truncate table CON_STATUS;

truncate table CON_SUBTYPE;

truncate table CON_TYPE;

truncate table CON_URL;

truncate table CON_VOTE;

truncate table FOR_FORUM;

truncate table FOR_TYPE;

truncate table FOR_USE;

truncate table GEO_GEOGRAPHY;

truncate table GEO_ZIP;

truncate table HIS_HISTORY;

truncate table IMP_NIGHTCLUB;

truncate table IMP_NIGHTCLUB_OWNER;

truncate table LIS_LIST;

truncate table LIS_ORG;

truncate table LIS_TYPE;

truncate table LOG_CON_EMAIL;

truncate table LOG_DEBUG;

truncate table LOG_ERROR;

truncate table LOG_HITS;

truncate table LOG_LOGIN;

truncate table LOG_MOD;

truncate table LOG_SUB;

truncate table LOG_TEX;

truncate table LOG_UPLOAD;

truncate table MAR_STATUS;

truncate table MAR_STUNT;

truncate table MAR_STUNT_TYPE;

truncate table MAR_TARGET;

truncate table MEM_MEMBERSHIP;

truncate table MEM_MOD;

truncate table MEM_ORG;

truncate table MEN_MENU;

truncate table MEN_OBJECT;

truncate table MEN_OBJECT_SERVING;

truncate table MEN_OBJECT_TYPE;

truncate table MEN_ORG_SERVING;

truncate table MEN_PRICE;

truncate table MEN_SERVING_PERIOD;

truncate table MEN_SERVING_TYPE;

truncate table MEN_TYPE;

truncate table MOD_MODULE;

truncate table MOD_PACKAGE;

truncate table MOD_PERMISSION;

truncate table MOD_PROCEDURE;

truncate table MOD_RESTRICTION;

truncate table ORG_CHOSEN;

truncate table ORG_HOURS;

truncate table ORG_ORGANISATION;

truncate table ORG_ORGANISATION_RIGHTS;

truncate table ORG_RIGHTS;

truncate table ORG_SER;

truncate table ORG_STATUS;

truncate table ORG_VALUE;

truncate table ORG_VALUE_TYPE;

truncate table PIC_ALBUM;

truncate table PIC_ALBUM_PICTURE;

truncate table PIC_COMMENT;

truncate table PIC_PICTURE;

truncate table PIC_VOTE;

truncate table POLL_ANSWER;

truncate table POL_ALTERNATIVE;

truncate table POL_POLL;

truncate table POL_TYPE;

truncate table QUO_QUOTE;

truncate table QUO_TYPE;

truncate table QUO_TYPE_SER;

truncate table REG_REGISTRATION;

truncate table REG_STATUS;

truncate table REG_USE;

truncate table REP_PROJECT;

truncate table REP_REPORT;

truncate table SER_SERVICE;

truncate table SES_INSTANCE;

truncate table SES_SESSION;

truncate table SUB_SELECTION;

truncate table SUB_SUBSCRIPTION;

truncate table SYS_PARAMETER;

truncate table SYS_PARAMETER_TYPE;

truncate table TEX_LANGUAGE;

truncate table TEX_TEXT;

truncate table USE_DETAILS;

truncate table USE_LOGIN_STATUS;

truncate table USE_ORG;

truncate table USE_STATUS;

truncate table USE_TYPE;

truncate table USE_USER;




PROMPT Remember the sequences!
