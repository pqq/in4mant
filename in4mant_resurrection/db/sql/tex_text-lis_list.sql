delete from tex_text where tex_text_pk like 'db.lis_list:%';

-- -----------------------
-- type of place
-- -----------------------
insert into tex_text values('db.lis_list:101_concert', 'nor', 'Konsert');
insert into tex_text values('db.lis_list:102_bar', 'nor', 'Bar');
insert into tex_text values('db.lis_list:103_discoteque', 'nor', 'Diskotek');
insert into tex_text values('db.lis_list:104_cafe', 'nor', 'Kaf�');
insert into tex_text values('db.lis_list:105_restaurant', 'nor', 'Restaurant');
insert into tex_text values('db.lis_list:106_scene', 'nor', 'Scene');
insert into tex_text values('db.lis_list:107_festival', 'nor', 'Festival');
insert into tex_text values('db.lis_list:108_pub', 'nor', 'Pub');
insert into tex_text values('db.lis_list:109_stripclub', 'nor', 'Strippeklubb');
insert into tex_text values('db.lis_list:110_nightclub', 'nor', 'Nattklubb');
insert into tex_text values('db.lis_list:111_society', 'nor', 'Forening');
insert into tex_text values('db.lis_list:112_plane', 'nor', 'Fly');
insert into tex_text values('db.lis_list:113_boat', 'nor', 'B�t');
insert into tex_text values('db.lis_list:114_cafebar', 'nor', 'Kaffebar');
insert into tex_text values('db.lis_list:115_takeaway', 'nor', 'Gatekj�kken');
insert into tex_text values('db.lis_list:116_hotel', 'nor', 'Hotell');


-- -----------------------
-- attractions
-- -----------------------
insert into tex_text values('db.lis_list:2', 'nor', '<b>Fasiliteter</b>');
insert into tex_text values('db.lis_list:201_internet', 'nor', 'Internett');
insert into tex_text values('db.lis_list:202_wardrobe', 'nor', 'Garderobe');
insert into tex_text values('db.lis_list:203_childfriend', 'nor', 'Barnevennlig');
insert into tex_text values('db.lis_list:204_dancefloor', 'nor', 'Dansegulv');
insert into tex_text values('db.lis_list:205_handicap', 'nor', 'Handikaptilpasset');
insert into tex_text values('db.lis_list:206_smoking', 'nor', 'R�ykeomr�de');
insert into tex_text values('db.lis_list:207_openair', 'nor', 'Uteservering');

insert into tex_text values('db.lis_list:3', 'nor', '<b>Klientell</b>');
insert into tex_text values('db.lis_list:301_mixed', 'nor', 'Blandet');
insert into tex_text values('db.lis_list:302_classy', 'nor', 'Classy');
insert into tex_text values('db.lis_list:303_gay', 'nor', 'Homofile');
insert into tex_text values('db.lis_list:304_musicians', 'nor', 'Musikere');
insert into tex_text values('db.lis_list:305_gothic', 'nor', 'Gotisk');
insert into tex_text values('db.lis_list:306_students', 'nor', 'Studenter');
insert into tex_text values('db.lis_list:307_turists', 'nor', 'Turister');
insert into tex_text values('db.lis_list:308_youth', 'nor', 'Ungdom');
insert into tex_text values('db.lis_list:309_adult', 'nor', 'Voksent');

insert into tex_text values('db.lis_list:4', 'nor', '<b>Betalingsmuligheter</b>');
insert into tex_text values('db.lis_list:401_amex', 'nor', 'American Express');
insert into tex_text values('db.lis_list:402_bank_accept', 'nor', 'Bank Accept');
insert into tex_text values('db.lis_list:403_diners', 'nor', 'Diners');
insert into tex_text values('db.lis_list:404_master_card', 'nor', 'Master Card');
insert into tex_text values('db.lis_list:405_visa', 'nor', 'VISA');
insert into tex_text values('db.lis_list:406_eurocard', 'nor', 'Eurocard');

insert into tex_text values('db.lis_list:5', 'nor', '<b>Underholdning</b>');
insert into tex_text values('db.lis_list:501_dj', 'nor', 'DJ');
insert into tex_text values('db.lis_list:502_karaoke', 'nor', 'Karaoke');
insert into tex_text values('db.lis_list:503_live_music', 'nor', 'Live musikk');
insert into tex_text values('db.lis_list:504_quiz', 'nor', 'Quiz');
insert into tex_text values('db.lis_list:505_standup', 'nor', 'Standup');
insert into tex_text values('db.lis_list:506_large_screen', 'nor', 'Storskjerm');

insert into tex_text values('db.lis_list:6', 'nor', '<b>Musikk</b>');
insert into tex_text values('db.lis_list:601_background', 'nor', 'Bakgrunnsmusikk');
insert into tex_text values('db.lis_list:602_mixed', 'nor', 'Blandet');
insert into tex_text values('db.lis_list:603_country', 'nor', 'Country');
insert into tex_text values('db.lis_list:604_danceband', 'nor', 'Danseband');
insert into tex_text values('db.lis_list:605_swing', 'nor', 'Swing');
insert into tex_text values('db.lis_list:606_disco', 'nor', 'Disco');
insert into tex_text values('db.lis_list:607_rnb', 'nor', 'RNB');
insert into tex_text values('db.lis_list:608_heavy_metal', 'nor', 'Heavy Metal');
insert into tex_text values('db.lis_list:609_drum_base', 'nor', 'Drum and Base');
insert into tex_text values('db.lis_list:610_hip_hop', 'nor', 'Hip Hop');
insert into tex_text values('db.lis_list:611_house_techno', 'nor', 'House / Techno');
insert into tex_text values('db.lis_list:612_indie', 'nor', 'Indie');
insert into tex_text values('db.lis_list:613_jazz_blues', 'nor', 'Jazz / Blues');
insert into tex_text values('db.lis_list:614_jukebox', 'nor', 'Jukebox');
insert into tex_text values('db.lis_list:615_classic', 'nor', 'Klassisk');
insert into tex_text values('db.lis_list:616_pop', 'nor', 'Pop');
insert into tex_text values('db.lis_list:617_oldies', 'nor', 'Oldies');
insert into tex_text values('db.lis_list:618_rock', 'nor', 'Rock');
insert into tex_text values('db.lis_list:619_punk_grunge', 'nor', 'Punk / Grunge');
insert into tex_text values('db.lis_list:620_rap', 'nor', 'Rap');
insert into tex_text values('db.lis_list:621_raggae', 'nor', 'Raggae');
insert into tex_text values('db.lis_list:622_salsa', 'nor', 'Salsa');
insert into tex_text values('db.lis_list:623_nomusic', 'nor', 'Ingen musikk');

insert into tex_text values('db.lis_list:7', 'nor', '<b>Spill</b>');
insert into tex_text values('db.lis_list:701_backgammon', 'nor', 'Backgammon');
insert into tex_text values('db.lis_list:702_pool', 'nor', 'Biljard');
insert into tex_text values('db.lis_list:703_dart', 'nor', 'Dart');
insert into tex_text values('db.lis_list:704_flipper', 'nor', 'Flipper');
insert into tex_text values('db.lis_list:705_cards', 'nor', 'Kort');
insert into tex_text values('db.lis_list:706_checkers', 'nor', 'Dam');
insert into tex_text values('db.lis_list:707_chess', 'nor', 'Sjakk');
insert into tex_text values('db.lis_list:708_slot_machine', 'nor', 'Spilleautomat');
insert into tex_text values('db.lis_list:709_game_machine', 'nor', 'Arcadespill');
insert into tex_text values('db.lis_list:710_football', 'nor', 'Fotballspill');
insert into tex_text values('db.lis_list:711_airhockey', 'nor', 'Air hockey');
insert into tex_text values('db.lis_list:712_bowling', 'nor', 'Bowling');

insert into tex_text values('db.lis_list:8', 'nor', '<b>Type servering</b>');
insert into tex_text values('db.lis_list:801_breakfast', 'nor', 'Frokost');
insert into tex_text values('db.lis_list:802_lunch', 'nor', 'Lunch');
insert into tex_text values('db.lis_list:803_brunch', 'nor', 'Brunch');
insert into tex_text values('db.lis_list:804_dinner', 'nor', 'Middag');
insert into tex_text values('db.lis_list:805_supper', 'nor', 'Kveldsmat');
insert into tex_text values('db.lis_list:806_nightfood', 'nor', 'Nattmat');

insert into tex_text values('db.lis_list:9', 'nor', '<b>Type mat</b>');
insert into tex_text values('db.lis_list:901_gourmet', 'nor', 'Gourmet');
insert into tex_text values('db.lis_list:902_fastfood', 'nor', 'Hurtigmat');
insert into tex_text values('db.lis_list:903_confectioner', 'nor', 'Konditorimat');
insert into tex_text values('db.lis_list:904_takeaway', 'nor', 'Takeaway');
insert into tex_text values('db.lis_list:905_xmasfood', 'nor', 'Julebord');
insert into tex_text values('db.lis_list:906_wildgamefood', 'nor', 'Vilt');

insert into tex_text values('db.lis_list:10', 'nor', '<b>Nasjonalitet</b>');
insert into tex_text values('db.lis_list:1001_american', 'nor', 'Amerikansk');
insert into tex_text values('db.lis_list:1002_arabian', 'nor', 'Arabisk');
insert into tex_text values('db.lis_list:1003_balkan', 'nor', 'Balkansk');
insert into tex_text values('db.lis_list:1004_brazilian', 'nor', 'Brasilsk');
insert into tex_text values('db.lis_list:1005_bulgarian', 'nor', 'Bulgarsk');
insert into tex_text values('db.lis_list:1006_danish', 'nor', 'Dansk');
insert into tex_text values('db.lis_list:1007_egyptian', 'nor', 'Egyptisk');
insert into tex_text values('db.lis_list:1008_english', 'nor', 'Engelsk');
insert into tex_text values('db.lis_list:1009_french', 'nor', 'Fransk');
insert into tex_text values('db.lis_list:1010_greek', 'nor', 'Gresk');
insert into tex_text values('db.lis_list:1011_indian', 'nor', 'Indisk');
insert into tex_text values('db.lis_list:1012_indonesian', 'nor', 'Indonesisk');
insert into tex_text values('db.lis_list:1013_iranian', 'nor', 'Iransk');
insert into tex_text values('db.lis_list:1014_irish', 'nor', 'Irsk');
insert into tex_text values('db.lis_list:1015_italian', 'nor', 'Italiensk');
insert into tex_text values('db.lis_list:1016_japanese', 'nor', 'Japansk');
insert into tex_text values('db.lis_list:1017_chinese', 'nor', 'Kinesisk');
insert into tex_text values('db.lis_list:1018_korean', 'nor', 'Koreansk');
insert into tex_text values('db.lis_list:1019_creole', 'nor', 'Kreolsk');
insert into tex_text values('db.lis_list:1020_cypriot', 'nor', 'Kypriotisk');
insert into tex_text values('db.lis_list:1021_libanese', 'nor', 'Libanesisk');
insert into tex_text values('db.lis_list:1022_moroccan', 'nor', 'Marokkansk');
insert into tex_text values('db.lis_list:1023_mexican', 'nor', 'Meksikansk');
insert into tex_text values('db.lis_list:1024_mongolian', 'nor', 'Mongolsk');
insert into tex_text values('db.lis_list:1025_dutch', 'nor', 'Nederlandsk');
insert into tex_text values('db.lis_list:1026_norwegian', 'nor', 'Norsk');
insert into tex_text values('db.lis_list:1027_pakistani', 'nor', 'Pakistansk');
insert into tex_text values('db.lis_list:1028_palestinian', 'nor', 'Palestinsk');
insert into tex_text values('db.lis_list:1029_persian', 'nor', 'Persisk');
insert into tex_text values('db.lis_list:1030_portuguese', 'nor', 'Protugisisk');
insert into tex_text values('db.lis_list:1031_russian', 'nor', 'Russisk');
insert into tex_text values('db.lis_list:1032_spanish', 'nor', 'Spansk');
insert into tex_text values('db.lis_list:1033_swiss', 'nor', 'Sveitsisk');
insert into tex_text values('db.lis_list:1034_swedish', 'nor', 'Svensk');
insert into tex_text values('db.lis_list:1035_thai', 'nor', 'Thailandsk');
insert into tex_text values('db.lis_list:1036_turkish', 'nor', 'Tyrkisk');
insert into tex_text values('db.lis_list:1037_german', 'nor', 'Tysk');
insert into tex_text values('db.lis_list:1038_hungarian', 'nor', 'Ungarsk');
insert into tex_text values('db.lis_list:1039_vietnamese', 'nor', 'Vietnamesisk');

insert into tex_text values('db.lis_list:11', 'nor', '<b>Kurs</b>');
insert into tex_text values('db.lis_list:1101_wine', 'nor', 'Vinkurs');
insert into tex_text values('db.lis_list:1102_food', 'nor', 'Matkurs');
insert into tex_text values('db.lis_list:1103_bartender', 'nor', 'Bartenderkurs');

insert into tex_text values('db.lis_list:12', 'nor', '<b>Utleie</b>');
insert into tex_text values('db.lis_list:1201_hire', 'nor', 'Har utleie');
