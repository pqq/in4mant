-- -------------------------------
-- disable all constraints
-- -------------------------------

set feedback off
set verify off
set echo off
prompt Finding constraints to disable...
set termout off
set pages 80
set heading off
set linesize 120
spool tmp_disable.sql
--select 'spool igen_disable.log;' from dual;
select 'ALTER TABLE '||substr(c.table_name,1,35)||
' DISABLE CONSTRAINT '||constraint_name||' ;'
from user_constraints c, user_tables u
where c.table_name = u.table_name;
select 'commit;' from dual;
spool off
set termout on
prompt Disabling constraints now...
set termout off
@tmp_disable.sql;
--!rm -i tmp_disable.sql;
exit
/

