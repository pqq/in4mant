-- -- 
-- THIS SQL SCRIPT IS TO TIDY UP THE DATABASE 
-- 
-- What:	The script will remove items not in use, clean log files etc 
-- Author:	Frode Klevstul (frode@klevstul.com) 
-- Started:	20th April 2006 
-- -- 



-- ---------------------------------------------------------------
PROMPT :::
PROMPT ::: Remove parent organisations that has not got any children :::
-- ---------------------------------------------------------------
delete from org_organisation
where	bool_umbrella_organisation = 1
and		org_organisation_pk not in 
(
	select	parent_org_organisation_fk
	from	org_organisation
	where	(bool_umbrella_organisation is null or bool_umbrella_organisation = 0)
);


-- ---------------------------------------------------------------
PROMPT :::
PROMPT ::: Remove addresses not being used :::
-- ---------------------------------------------------------------
delete from add_address
where add_address_pk not in
(
	select	add_address_fk
	from	bil_item
	where	add_address_fk is not null
union
	select	add_address_fk
	from	use_details
	where	add_address_fk is not null
union
	select	add_address_fk
	from	mar_target
	where	add_address_fk is not null
union
	select	add_address_fk
	from	org_organisation
	where	add_address_fk is not null
);

-- ---------------------------------------------------------------
PROMPT :::
PROMPT ::: Please commit or rollback the changes! :::
-- ---------------------------------------------------------------
-- commit;
