-- ---------------------------------
-- SOURCE DATA FOR IN4MANT DATABASE
-- ---------------------------------

-- ----
-- USE
-- ----
delete from use_type;
insert into use_type values(0, 'no_login', 'users not logged in', null, null, 0);
insert into use_type values(1, 'normal', 'users that register on Internet', 60, 10, 10);
insert into use_type values(2, 'organisation', 'a user related to an organisation', 30, 5, 20);
insert into use_type values(3, 'org_full', 'organisation with full access to all modules', 30, 5, 30);
insert into use_type values(4, 'admin_limited', 'an admin user with only limited access (must be specifed)', 30, 3, 100);
insert into use_type values(5, 'admin_normal', 'a normal administrator, with access to most of the modules', 30, 3, 150);
insert into use_type values(6, 'admin_super', 'a super administrator has access to everything', 30, 3, 200);

delete from use_status;
insert into use_status values(1, 'not_activated', 'a user that has registered but not activated the account');
insert into use_status values(2, 'activated', 'an activated user account');
insert into use_status values(3, 'suspended', 'an account suspended by an administrator');
insert into use_status values(4, 'wrong_login', 'account closed due to too many wrong logins');
insert into use_status values(5, 'closed', 'account closed by user');

delete from use_user;
insert into use_user values(1, 'fk', 'frode@klevstul.com', 'pwd', 6, 2);
insert into use_user values(2, 'em', 'espen@messel.no', 'pwd', 6, 2);
insert into use_user values(3, 'ar', 'andreas@moobi.com', 'pwd', 6, 2);
insert into use_user values(4, 'jr', 'joakim@bytur.no', 'pwd', 6, 2);
insert into use_user values(5, 'tm', 'tore@oxyride.com', 'pwd', 6, 2);

delete from use_details;
insert into use_details values(1, 'Frode', 'Klevstul', null, to_date('1976:08:18','YYYY:MM:DD'), 'M', sysdate, sysdate, null, null);
insert into use_details values(2, 'Espen', 'Messel', null, null, 'M', sysdate, sysdate, null, null);
insert into use_details values(3, 'Andreas', 'Ronnestad', null, to_date('1976:02:28','YYYY:MM:DD'), 'M', sysdate, sysdate, null, null);
insert into use_details values(4, 'Joakim', 'Ronning Hansen', null, null, 'M', sysdate, sysdate, null, null);

-- ----
-- ORG / SER
-- ----
delete from org_status;
insert into org_status values(1, 'open', 'open (existing) and active organisation');
insert into org_status values(2, 'member', 'organisation is a member');
insert into org_status values(3, 'closed', 'organisation is closed');
insert into org_status values(4, 'banned', 'organisation is banned');

delete from ser_service;
insert into ser_service values(1, 'uteliv', 'uteliv.no', 1);
insert into ser_service values(2, 'bytur', 'bytur.no', 0);

delete from org_rights;
insert into org_rights values(1, 'beer' , 'beer');
insert into org_rights values(2, 'wine' , 'wine');
insert into org_rights values(3, 'spirits' , 'spirits');
insert into org_rights values(4, 'strongbeer' , 'strongbeer');
insert into org_rights values(5, 'strongwine' , 'strongwine');

-- ----
-- ORG_VALUE_TYPE
-- ----
@org_value_type.sql

-- ----
-- GEO
-- ----
@geo_geography.sql

-- ----
-- TXT
-- ----
delete from tex_text;
delete from tex_language;

insert into tex_language values('nor', 'Norwegian', 'Norsk');
insert into tex_language values('eng', 'English', 'English');

@tex_text-lis_list.sql
@tex_text-thesis.sql
@tex_text.sql

-- ----
-- MEM
-- ----
delete from mem_membership;
insert into mem_membership values(1, 'basic', 'basic uteliv membership', 6900, 12, sysdate, sysdate);
insert into mem_membership values(2, 'plus', 'plus membership gives access to all modules', 9900, 12, sysdate, sysdate);

-- ----
-- CON
-- ----
delete from con_type;
insert into con_type values(1, 'event_org', 'an organisation event or happening', 5);
insert into con_type values(2, 'news_org', 'news from organisation', 5);
insert into con_type values(3, 'news_i4', 'news from in4mant', 5);
insert into con_type values(4, 'review_org', 'organisation reviews', 5);

delete from con_subtype;
insert into con_subtype values(1, 'party', 'party');
insert into con_subtype values(2, 'concert', 'concert');
insert into con_subtype values(3, 'scene', 'scene');
insert into con_subtype values(4, 'exhibition', 'exhibition');
insert into con_subtype values(5, 'festival', 'festival');
insert into con_subtype values(6, 'theatre', 'theatre');
insert into con_subtype values(7, 'movie', 'movie');
insert into con_subtype values(8, 'speech', 'speech');
insert into con_subtype values(9, 'other', 'other');

delete from con_status;
insert into con_status values(1, 'preview', 'content is for preview only');
insert into con_status values(2, 'published', 'content is published on the site');
insert into con_status values(3, 'draft', 'content wont show on site - is draft');
insert into con_status values(4, 'removed', 'content is removed from the site');

delete from con_file_type;
insert into con_file_type values(1, 'jpg', 'Picture jpg');
insert into con_file_type values(2, 'pdf', 'Acrobat PDF');
insert into con_file_type values(3, 'doc', 'Word document');

-- ----
-- LIS
-- ----
@lis.sql

-- ----
-- REG
-- ----
delete from reg_status;
insert into reg_status values(1, 'new', 'new registration not yet processed');
insert into reg_status values(2, 'accepted', 'accepted registration');
insert into reg_status values(3, 'rejected', 'rejected registration');

-- ----
-- SYS
-- ----
@sys.sql

-- ----
-- MAR
-- ----
delete from mar_status;
insert into mar_status values(1, 'no_response', 'no response from target');
insert into mar_status values(2, 'do_not_contact', 'do not contact this target again');
insert into mar_status values(3, 'not_interested', 'target not interested');
insert into mar_status values(4, 'low_interest', 'low interest');
insert into mar_status values(5, 'medium_interest', 'target shows medium interest');
insert into mar_status values(6, 'high_interest', 'target very interested');
insert into mar_status values(7, 'success', 'marketing stunt was a success');

delete from mar_stunt_type;
insert into mar_stunt_type values(1, 'email', 'email sent');
insert into mar_stunt_type values(2, 'post', 'mail sent in post');
insert into mar_stunt_type values(3, 'phonecall', 'phonecall made');
insert into mar_stunt_type values(4, 'meeting', 'meeting held');

-- ----
-- ADD
-- ----

-- ---
-- MOD (restrictions)
-- ---
@mod_restrictions.sql


commit;
