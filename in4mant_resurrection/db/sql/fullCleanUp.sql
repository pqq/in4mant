-- ---
-- FULL CLEAN UP IN DIFFERENT TABLES
-- ---
delete from mod_package;
delete from mod_procedure;
delete from mod_module;
delete from mod_restriction;

delete from log_error;
delete from log_mod;
delete from log_debug;

delete from ses_instance;
delete from ses_session;

-- this should not be neccesary, however it was at least once...
--delete from org_original where org_organisation_fk_pk not in (select org_organisation_pk from org_organisation)

commit;
