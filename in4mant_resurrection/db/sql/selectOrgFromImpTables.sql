select 
o.org_number,
o.name as eier, 
replace(o.street, chr(10), chr(44)) as street_eier, 
o.council, 
o.zip, 
o.landline, 
o.fax, 
o.email, 
o.url, 
n.name as utested, 
replace(n.address_full, chr(10), chr(44)) as address_full_utested, 
replace(n.street, chr(10), chr(44)) as street_utested, 
n.council, 
n.zip, 
n.landline, 
n.fax, 
n.email
from imp_nightclub_owner o, imp_nightclub n
where o.org_number = n.org_number;  