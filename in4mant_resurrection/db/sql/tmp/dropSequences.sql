DROP SEQUENCE SEQ_LOG_MOD;
DROP SEQUENCE SEQ_LOG_UPLOAD;
DROP SEQUENCE SEQ_MOD_MODULE;
DROP SEQUENCE SEQ_MOD_PACKAGE;
DROP SEQUENCE SEQ_MOD_PROCEDURE;
DROP SEQUENCE SEQ_ORG_ORGANISATION;
DROP SEQUENCE SEQ_SES_INSTANCE;
DROP SEQUENCE SEQ_SES_SESSION;
DROP SEQUENCE SEQ_USE_USER;
DROP SEQUENCE SEQ_HIS_HISTORY;
DROP SEQUENCE SEQ_LOG_DEBUG;
DROP SEQUENCE SEQ_ADD_ADDRESS;
DROP SEQUENCE SEQ_LOG_ERROR;
DROP SEQUENCE SEQ_LOG_HITS;
DROP SEQUENCE SEQ_LOG_LOGIN;

exit;