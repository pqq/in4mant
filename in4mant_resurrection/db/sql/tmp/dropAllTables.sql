
alter table ADD_ADDRESS
   drop constraint FK_ADD_ADDR_REFERENCE_GEO_GEOG;

alter table BIL_ITEM
   drop constraint FK_BIL_ITEM_REFERENCE_BIL_TRAD;

alter table BIL_ITEM
   drop constraint FK_BIL_ITEM_REFERENCE_BIL_ITEM;

alter table BIL_ITEM
   drop constraint FK_BIL_ITEM_REFERENCE_BIL_PAYM;

alter table BIL_ITEM
   drop constraint FK_BIL_ITEM_REFERENCE_USE_USER;

alter table COM_ORG
   drop constraint FK_COM_ORG_REFERENCE_COM_COMP;

alter table COM_ORG
   drop constraint FK_COM_ORG_REFERENCE_ORG_ORGA;

alter table CON_COMMENT
   drop constraint FK_CON_COMM_REFERENCE_CON_CONT;

alter table CON_COMMENT
   drop constraint FK_CON_COMM_REFERENCE_USE_USER;

alter table CON_CONTENT
   drop constraint FK_CON_CONT_REFERENCE_CON_TYPE;

alter table CON_CONTENT
   drop constraint FK_CON_CONT_REFERENCE_CON_STAT;

alter table CON_CONTENT
   drop constraint FK_CON_CONT_REFERENCE_ORG_ORGA;

alter table CON_CONTENT
   drop constraint FK_CON_CONT_REFERENCE_GEO_GEOG;

alter table CON_CONTENT
   drop constraint FK_CON_CONT_REFERENCE_CON_SUBT;

alter table CON_CONTENT_FILE
   drop constraint FK_CON_CONT_REFERENCE_CON_CONT;

alter table CON_CONTENT_FILE
   drop constraint FK_CON_CONT_REFERENCE_CON_FILE;

alter table CON_FILE
   drop constraint FK_CON_FILE_REFERENCE_CON_FILE;

alter table CON_RELATION
   drop constraint FK_CON_RELA_REFERENCE_CON_CON1;

alter table CON_RELATION
   drop constraint FK_CON_RELA_REFERENCE_CON_CON2;

alter table CON_SER
   drop constraint FK_CON_SER_REFERENCE_CON_CONT;

alter table CON_SER
   drop constraint FK_CON_SER_REFERENCE_SER_SERV;

alter table CON_URL
   drop constraint FK_CON_URL_REFERENCE_CON_CONT;

alter table CON_VOTE
   drop constraint FK_CON_VOTE_REFERENCE_CON_CONT;

alter table CON_VOTE
   drop constraint FK_CON_VOTE_REFERENCE_USE_USER;

alter table FOR_FORUM
   drop constraint FK_FOR_FORU_REFERENCE_FOR_FORU;

alter table FOR_FORUM
   drop constraint FK_FOR_FORU_REFERENCE_FOR_TYPE;

alter table FOR_TYPE
   drop constraint FK_FOR_TYPE_REFERENCE_SER_SERV;

alter table FOR_TYPE
   drop constraint FK_FOR_TYPE_REFERENCE_ORG_ORGA;

alter table FOR_USE
   drop constraint FK_FOR_USE_REFERENCE_FOR_FORU;

alter table FOR_USE
   drop constraint FK_FOR_USE_REFERENCE_USE_USER;

alter table GEO_GEOGRAPHY
   drop constraint FK_GEO_GEOG_REFERENCE_GEO_GEOG;

alter table GEO_ZIP
   drop constraint FK_GEO_ZIP_REFERENCE_GEO_GEOG;

alter table HIS_HISTORY
   drop constraint FK_HIS_HIST_REFERENCE_USE_USER;

alter table LIS_LIST
   drop constraint FK_LIS_LIST_REFERENCE_LIS_TYPE;

alter table LIS_ORG
   drop constraint FK_LIS_ORG_REFERENCE_LIS_LIST;

alter table LIS_ORG
   drop constraint FK_LIS_ORG_REFERENCE_ORG_ORGA;

alter table LOG_CON_EMAIL
   drop constraint FK_LOG_CON__REFERENCE_CON_CONT;

alter table LOG_LOGIN
   drop constraint FK_LOG_LOGI_REFERENCE_USE_USER;

alter table LOG_SUB
   drop constraint FK_LOG_SUB_REFERENCE_CON_CONT;

alter table LOG_SUB
   drop constraint FK_LOG_SUB_REFERENCE_USE_USER;

alter table MAR_STUNT
   drop constraint FK_MAR_STUN_REFERENCE_MAR_STAT;

alter table MAR_STUNT
   drop constraint FK_MAR_STUN_REFERENCE_MAR_TARG;

alter table MAR_STUNT
   drop constraint FK_MAR_STUN_REFERENCE_MAR_STUN;

alter table MAR_STUNT
   drop constraint FK_MAR_STUN_REFERENCE_USE_USER;

alter table MAR_TARGET
   drop constraint FK_MAR_TARG_REFERENCE_ADD_ADDR;

alter table MAR_TARGET
   drop constraint FK_MAR_TARG_REFERENCE_ORG_ORGA;

alter table MAR_TARGET
   drop constraint FK_MAR_TARG_REFERENCE_USE_USER;

alter table MEM_MOD
   drop constraint FK_MEM_MOD_REFERENCE_MOD_MODU;

alter table MEM_MOD
   drop constraint FK_MEM_MOD_REFERENCE_MEM_MEMB;

alter table MEM_ORG
   drop constraint FK_MEM_ORG_REFERENCE_ORG_ORGA;

alter table MEM_ORG
   drop constraint FK_MEM_ORG_REFERENCE_MEM_MEMB;

alter table MEN_MENU
   drop constraint FK_MEN_MENU_REFERENCE_MEN_TYPE;

alter table MEN_MENU
   drop constraint FK_MEN_MENU_REFERENCE_ORG_ORGA;

alter table MEN_OBJECT
   drop constraint FK_MEN_OBJE_REFERENCE_MEN_MENU;

alter table MEN_OBJECT
   drop constraint FK_MEN_OBJE_REFERENCE_MEN_OBJE;

alter table MEN_OBJECT
   drop constraint FK_MEN_OBJE_REFERENCE_MEN_SERV;

alter table MEN_OBJECT_SERVING
   drop constraint FK_MEN_OBJE_REFERENCE_MEN_ORG_;

alter table MEN_OBJECT_SERVING
   drop constraint FK_MEN_OBJE_REFERENCE_MEN_OBJ3;

alter table MEN_OBJECT_TYPE
   drop constraint FK_MEN_OBJE_REFERENCE_MEN_TYPE;

alter table MEN_ORG_SERVING
   drop constraint FK_MEN_ORG__REFERENCE_MEN_SERV;

alter table MEN_ORG_SERVING
   drop constraint FK_MEN_ORG__REFERENCE_ORG_ORGA;

alter table MEN_PRICE
   drop constraint FK_MEN_PRIC_REFERENCE_MEN_OBJ2;

alter table MOD_MODULE
   drop constraint FK_MOD_MODU_REFERENCE_MOD_PROC;

alter table MOD_MODULE
   drop constraint FK_MOD_MODU_REFERENCE_MOD_PACK;

alter table MOD_PERMISSION
   drop constraint FK_MOD_PERM_REFERENCE_MOD_MODU;

alter table MOD_PERMISSION
   drop constraint FK_MOD_PERM_REFERENCE_USE_USER;

alter table MOD_RESTRICTION
   drop constraint FK_MOD_REST_REFERENCE_MOD_MODU;

alter table MOD_RESTRICTION
   drop constraint FK_MOD_REST_REFERENCE_USE_TYPE;

alter table ORG_CHOSEN
   drop constraint FK_ORG_CHOS_REFERENCE_SER_SERV;

alter table ORG_CHOSEN
   drop constraint FK_ORG_CHOS_REFERENCE_ORG_ORGA;

alter table ORG_HOURS
   drop constraint FK_ORG_HOUR_REFERENCE_ORG_ORGA;

alter table ORG_ORGANISATION
   drop constraint FK_ORG_ORGA_REFERENCE_ORG_ORG1;

alter table ORG_ORGANISATION
   drop constraint FK_ORG_ORGA_REFERENCE_ORG_STAT;

alter table ORG_ORGANISATION
   drop constraint FK_ORG_ORGA_REFERENCE_ADD_ADDR;

alter table ORG_ORGANISATION_RIGHTS
   drop constraint FK_ORG_ORGA_REFERENCE_ORG_ORG2;

alter table ORG_ORGANISATION_RIGHTS
   drop constraint FK_ORG_ORGA_REFERENCE_ORG_RIGH;

alter table ORG_SER
   drop constraint FK_ORG_SER_REFERENCE_ORG_ORGA;

alter table ORG_SER
   drop constraint FK_ORG_SER_REFERENCE_SER_SERV;

alter table ORG_VALUE
   drop constraint FK_ORG_VALU_REFERENCE_ORG_VALU;

alter table ORG_VALUE
   drop constraint FK_ORG_VALU_REFERENCE_ORG_ORGA;

alter table PIC_ALBUM
   drop constraint FK_PIC_ALBU_REFERENCE_ORG_ORGA;

alter table PIC_ALBUM_PICTURE
   drop constraint FK_PIC_ALBU_REFERENCE_PIC_ALBU;

alter table PIC_ALBUM_PICTURE
   drop constraint FK_PIC_ALBU_REFERENCE_PIC_PICT;

alter table PIC_COMMENT
   drop constraint FK_PIC_COMM_REFERENCE_PIC_PICT;

alter table PIC_VOTE
   drop constraint FK_PIC_VOTE_REFERENCE_PIC_PICT;

alter table POLL_ANSWER
   drop constraint FK_POLL_ANS_REFERENCE_USE_USER;

alter table POLL_ANSWER
   drop constraint FK_POLL_ANS_REFERENCE_POL_ALTE;

alter table POL_ALTERNATIVE
   drop constraint FK_POL_ALTE_REFERENCE_POL_POLL;

alter table POL_POLL
   drop constraint FK_POL_POLL_REFERENCE_POL_TYPE;

alter table POL_TYPE
   drop constraint FK_POL_TYPE_REFERENCE_SER_SERV;

alter table QUO_QUOTE
   drop constraint FK_QUO_QUOT_REFERENCE_QUO_TYPE;

alter table QUO_TYPE_SER
   drop constraint FK_QUO_TYPE_REFERENCE_QUO_TYPE;

alter table QUO_TYPE_SER
   drop constraint FK_QUO_TYPE_REFERENCE_SER_SERV;

alter table REG_REGISTRATION
   drop constraint FK_REG_REGI_REFERENCE_CON_CONT;

alter table REG_USE
   drop constraint FK_REG_USE_REFERENCE_REG_REGI;

alter table REG_USE
   drop constraint FK_REG_USE_REFERENCE_USE_USER;

alter table REG_USE
   drop constraint FK_REG_USE_REFERENCE_REG_STAT;

alter table REP_PROJECT
   drop constraint FK_REP_PROJ_REFERENCE_USE_USER;

alter table REP_REPORT
   drop constraint FK_REP_REPO_REFERENCE_REP_PROJ;

alter table SES_INSTANCE
   drop constraint FK_SES_INST_REFERENCE_SES_SESS;

alter table SES_SESSION
   drop constraint FK_SES_SESS_REFERENCE_ORG_ORGA;

alter table SES_SESSION
   drop constraint FK_SES_SESS_REFERENCE_TEX_LANG;

alter table SES_SESSION
   drop constraint FK_SES_SESS_REFERENCE_SER_SERV;

alter table SES_SESSION
   drop constraint FK_SES_SESS_REFERENCE_USE_USER;

alter table SUB_SELECTION
   drop constraint FK_SUB_SELE_REFERENCE_SUB_SUBS;

alter table SUB_SUBSCRIPTION
   drop constraint FK_SUB_SUBS_REFERENCE_USE_USER;

alter table SYS_PARAMETER
   drop constraint FK_SYS_PARA_REFERENCE_SYS_PARA;

alter table SYS_PARAMETER
   drop constraint FK_SYS_PARA_REFERENCE_USE_USER;

alter table TEX_TEXT
   drop constraint FK_TEX_TEXT_REFERENCE_TEX_LANG;

alter table USE_DETAILS
   drop constraint FK_USE_DETA_REFERENCE_USE_USER;

alter table USE_DETAILS
   drop constraint FK_USE_DETA_REFERENCE_ADD_ADDR;

alter table USE_LOGIN_STATUS
   drop constraint FK_USE_LOGI_REFERENCE_USE_USE2;

alter table USE_ORG
   drop constraint FK_USE_ORG_REFERENCE_USE_USER;

alter table USE_USER
   drop constraint FK_USE_USER_REFERENCE_USE_TYPE;

alter table USE_USER
   drop constraint FK_USE_USER_REFERENCE_USE_STAT;

drop table ADD_ADDRESS cascade constraints;

drop table BIL_ITEM cascade constraints;

drop table BIL_ITEM_TYPE cascade constraints;

drop table BIL_PAYMENT_METHOD cascade constraints;

drop table BIL_TRADE_TYPE cascade constraints;

drop table COM_COMPARE cascade constraints;

drop table COM_ORG cascade constraints;

drop table CON_COMMENT cascade constraints;

drop table CON_CONTENT cascade constraints;

drop table CON_CONTENT_FILE cascade constraints;

drop table CON_FILE cascade constraints;

drop table CON_FILE_TYPE cascade constraints;

drop table CON_RELATION cascade constraints;

drop table CON_SER cascade constraints;

drop table CON_STATUS cascade constraints;

drop table CON_SUBTYPE cascade constraints;

drop table CON_TYPE cascade constraints;

drop table CON_URL cascade constraints;

drop table CON_VOTE cascade constraints;

drop table FOR_FORUM cascade constraints;

drop table FOR_TYPE cascade constraints;

drop table FOR_USE cascade constraints;

drop table GEO_GEOGRAPHY cascade constraints;

drop table GEO_ZIP cascade constraints;

drop table HIS_HISTORY cascade constraints;

drop table IMP_NIGHTCLUB cascade constraints;

drop table IMP_NIGHTCLUB_OWNER cascade constraints;

drop table LIS_LIST cascade constraints;

drop table LIS_ORG cascade constraints;

drop table LIS_TYPE cascade constraints;

drop table LOG_CON_EMAIL cascade constraints;

drop table LOG_DEBUG cascade constraints;

drop table LOG_ERROR cascade constraints;

drop table LOG_HITS cascade constraints;

drop table LOG_LOGIN cascade constraints;

drop table LOG_MOD cascade constraints;

drop table LOG_SUB cascade constraints;

drop table LOG_TEX cascade constraints;

drop table LOG_UPLOAD cascade constraints;

drop table MAR_STATUS cascade constraints;

drop table MAR_STUNT cascade constraints;

drop table MAR_STUNT_TYPE cascade constraints;

drop table MAR_TARGET cascade constraints;

drop table MEM_MEMBERSHIP cascade constraints;

drop table MEM_MOD cascade constraints;

drop table MEM_ORG cascade constraints;

drop table MEN_MENU cascade constraints;

drop table MEN_OBJECT cascade constraints;

drop table MEN_OBJECT_SERVING cascade constraints;

drop table MEN_OBJECT_TYPE cascade constraints;

drop table MEN_ORG_SERVING cascade constraints;

drop table MEN_PRICE cascade constraints;

drop table MEN_SERVING_PERIOD cascade constraints;

drop table MEN_SERVING_TYPE cascade constraints;

drop table MEN_TYPE cascade constraints;

drop table MOD_MODULE cascade constraints;

drop table MOD_PACKAGE cascade constraints;

drop table MOD_PERMISSION cascade constraints;

drop table MOD_PROCEDURE cascade constraints;

drop table MOD_RESTRICTION cascade constraints;

drop table ORG_CHOSEN cascade constraints;

drop table ORG_HOURS cascade constraints;

drop table ORG_ORGANISATION cascade constraints;

drop table ORG_ORGANISATION_RIGHTS cascade constraints;

drop table ORG_RIGHTS cascade constraints;

drop table ORG_SER cascade constraints;

drop table ORG_STATUS cascade constraints;

drop table ORG_VALUE cascade constraints;

drop table ORG_VALUE_TYPE cascade constraints;

drop table PIC_ALBUM cascade constraints;

drop table PIC_ALBUM_PICTURE cascade constraints;

drop table PIC_COMMENT cascade constraints;

drop table PIC_PICTURE cascade constraints;

drop table PIC_VOTE cascade constraints;

drop table POLL_ANSWER cascade constraints;

drop table POL_ALTERNATIVE cascade constraints;

drop table POL_POLL cascade constraints;

drop table POL_TYPE cascade constraints;

drop table QUO_QUOTE cascade constraints;

drop table QUO_TYPE cascade constraints;

drop table QUO_TYPE_SER cascade constraints;

drop table REG_REGISTRATION cascade constraints;

drop table REG_STATUS cascade constraints;

drop table REG_USE cascade constraints;

drop table REP_PROJECT cascade constraints;

drop table REP_REPORT cascade constraints;

drop table SER_SERVICE cascade constraints;

drop table SES_INSTANCE cascade constraints;

drop table SES_SESSION cascade constraints;

drop table SUB_SELECTION cascade constraints;

drop table SUB_SUBSCRIPTION cascade constraints;

drop table SYS_PARAMETER cascade constraints;

drop table SYS_PARAMETER_TYPE cascade constraints;

drop table TEX_LANGUAGE cascade constraints;

drop table TEX_TEXT cascade constraints;

drop table USE_DETAILS cascade constraints;

drop table USE_LOGIN_STATUS cascade constraints;

drop table USE_ORG cascade constraints;

drop table USE_STATUS cascade constraints;

drop table USE_TYPE cascade constraints;

drop table USE_USER cascade constraints;

drop sequence SEQ_ADD_ADDRESS;

drop sequence SEQ_HIS_HISTORY;

drop sequence SEQ_LOG_DEBUG;

drop sequence SEQ_LOG_ERROR;

drop sequence SEQ_LOG_HITS;

drop sequence SEQ_LOG_LOGIN;

drop sequence SEQ_LOG_MOD;

drop sequence SEQ_LOG_UPLOAD;

drop sequence SEQ_MOD_MODULE;

drop sequence SEQ_MOD_PACKAGE;

drop sequence SEQ_MOD_PROCEDURE;

drop sequence SEQ_ORG_ORGANISATION;

drop sequence SEQ_SES_INSTANCE;

drop sequence SEQ_SES_SESSION;

drop sequence SEQ_USE_USER;
