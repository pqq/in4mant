-- ---------------------------------------------------------------------------
-- get the IP address for those who are trying to fake an session ID 
-- ---------------------------------------------------------------------------
select *
from ses_session
where id in 
(
	select ses_session_id_new
	from log_error
	where message like '%fake_session_id%'
) 


-- ---------------------------------------------------------------------------
-- miscellaneous
-- ---------------------------------------------------------------------------
select * from ses_instance order by ses_instance_pk desc

select * from ses_session order by ses_session_pk desc

select * from log_error order by log_error_pk desc

select * from log_mod order by log_mod_pk desc
	   
select * from log_debug order by log_debug_pk desc

select * from mod_module

select * from sys_parameter 


-- ---------------------------------------------------------------------------
-- tmp
-- ---------------------------------------------------------------------------
