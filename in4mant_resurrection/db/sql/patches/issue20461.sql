alter table ORG_EXTRA
   drop constraint FK_ORG_EXTR_REFERENCE_ORG_ORGA;

drop table ORG_EXTRA cascade constraints;

/*==============================================================*/
/* Table: ORG_EXTRA                                             */
/*==============================================================*/
create table ORG_EXTRA  (
   ORG_ORGANISATION_FK_PK NUMBER                          not null,
   ORG_SUBDIR           NUMBER,
   constraint PK_ORG_EXTRA primary key (ORG_ORGANISATION_FK_PK)
);

alter table ORG_EXTRA
   add constraint FK_ORG_EXTR_REFERENCE_ORG_ORGA foreign key (ORG_ORGANISATION_FK_PK)
      references ORG_ORGANISATION (ORG_ORGANISATION_PK)
      on delete cascade;
