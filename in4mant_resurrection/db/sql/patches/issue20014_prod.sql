--  ********************************************************************** 
--  Note: This rebuild script is not meant to be used when a possibility * 
--        exists that someone might try to access the table while it is  * 
--        being rebuilt!                                                 * 
--                                                                       * 
--        Locks are released when the first DDL, COMMIT or ROLLBACK is   * 
--        performed, so adding a "Lock table" command at the top of this * 
--        script will not prevent others from accessing the table for    * 
--        the duration of the script.                                    * 
--                                                                       * 
--   One more important note:                                            * 
--        This script will cause the catalog in replicated environments  * 
--        to become out of sync.                                         * 
--  ********************************************************************** 

--  Table Rebuild script generated by TOAD  
--  
--  Original table: ADD_ADDRESS 
--  Backup of table: ADD_ADDRESS_X 
--  Date: 5/07/2006 6:35:04 PM 
--  
SET LINESIZE 200
--  
--  Make backup copy of original table 
ALTER TABLE I4.ADD_ADDRESS RENAME TO ADD_ADDRESS_X ; 
  
 
--  Drop FKey constraint from I4.MAR_TARGET 
ALTER TABLE I4.MAR_TARGET DROP CONSTRAINT FK_MAR_TARG_REFERENCE_ADD_ADDR ; 
  
--  Drop FKey constraint from I4.ORG_ORGANISATION 
ALTER TABLE I4.ORG_ORGANISATION DROP CONSTRAINT FK_ORG_ORGA_REFERENCE_ADD_ADDR ; 
  
--  Drop FKey constraint from I4.USE_DETAILS 
ALTER TABLE I4.USE_DETAILS DROP CONSTRAINT FK_USE_DETA_REFERENCE_ADD_ADDR ; 
  

-- Drop all user named constraints
ALTER TABLE I4.ADD_ADDRESS_X DROP CONSTRAINT FK_ADD_ADDR_REFERENCE_GEO_GEOG ;
ALTER TABLE I4.ADD_ADDRESS_X DROP CONSTRAINT PK_ADD_ADDRESS ;
DROP INDEX I4.PK_ADD_ADDRESS;

--  Recreate original table 
CREATE TABLE I4.ADD_ADDRESS
(
  ADD_ADDRESS_PK    NUMBER                      NOT NULL,
  NAME              VARCHAR2(64 BYTE),
  STREET            VARCHAR2(512 BYTE),
  ZIP               VARCHAR2(16 BYTE),
  LANDLINE          VARCHAR2(32 BYTE),
  MOBILE            VARCHAR2(32 BYTE),
  FAX               VARCHAR2(32 BYTE),
  EMAIL             VARCHAR2(64 BYTE),
  URL               VARCHAR2(128 BYTE),
  GEO_GEOGRAPHY_FK  NUMBER,
  LONGITUDE         NUMBER,
  LATITUDE          NUMBER,
  DATE_INSERT       DATE,
  DATE_UPDATE       DATE
)
TABLESPACE TS_IN4MANT
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;
 
--  Copy the data from the renamed table  
INSERT /*+ APPEND */
INTO I4.ADD_ADDRESS INS_TBL
(ADD_ADDRESS_PK, NAME, STREET, ZIP, 
LANDLINE, MOBILE, FAX, EMAIL, 
URL, GEO_GEOGRAPHY_FK, DATE_INSERT, DATE_UPDATE)
SELECT 
ADD_ADDRESS_PK, NAME, STREET, ZIP, 
LANDLINE, MOBILE, FAX, EMAIL, 
URL, GEO_GEOGRAPHY_FK, DATE_INSERT, DATE_UPDATE
FROM I4.ADD_ADDRESS_X SEL_TBL ; 
  
Commit ; 
  

-- Drop all other user named indexes 
-- (none) 




--  Recreate Indexes, Constraints, and Grants 

CREATE UNIQUE INDEX I4.PK_ADD_ADDRESS ON I4.ADD_ADDRESS
(ADD_ADDRESS_PK)
LOGGING
TABLESPACE TS_IN4MANT
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE I4.ADD_ADDRESS ADD (
  CONSTRAINT PK_ADD_ADDRESS
 PRIMARY KEY
 (ADD_ADDRESS_PK)
    USING INDEX 
    TABLESPACE TS_IN4MANT
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          320K
                MINEXTENTS       1
                MAXEXTENTS       2147483645
                PCTINCREASE      0
               ));


ALTER TABLE I4.ADD_ADDRESS ADD (
  CONSTRAINT FK_ADD_ADDR_REFERENCE_GEO_GEOG 
 FOREIGN KEY (GEO_GEOGRAPHY_FK) 
 REFERENCES I4.GEO_GEOGRAPHY (GEO_GEOGRAPHY_PK)
    ON DELETE CASCADE);
 
--  Recreate the FKeys that reference the NEW table 
 
ALTER TABLE I4.MAR_TARGET ADD 
 CONSTRAINT FK_MAR_TARG_REFERENCE_ADD_ADDR
 FOREIGN KEY (ADD_ADDRESS_FK) 
  REFERENCES I4.ADD_ADDRESS (ADD_ADDRESS_PK) 
  ON DELETE CASCADE ; 
  
ALTER TABLE I4.ORG_ORGANISATION ADD 
 CONSTRAINT FK_ORG_ORGA_REFERENCE_ADD_ADDR
 FOREIGN KEY (ADD_ADDRESS_FK) 
  REFERENCES I4.ADD_ADDRESS (ADD_ADDRESS_PK) 
  ON DELETE CASCADE ; 
  
ALTER TABLE I4.USE_DETAILS ADD 
 CONSTRAINT FK_USE_DETA_REFERENCE_ADD_ADDR
 FOREIGN KEY (ADD_ADDRESS_FK) 
  REFERENCES I4.ADD_ADDRESS (ADD_ADDRESS_PK) 
  ON DELETE CASCADE ; 
  
 
--  Recompile any dependent objects 
 
ALTER PACKAGE "I4"."GEO_LIB" COMPILE ; 
  
ALTER PACKAGE "I4"."IMP_I4" COMPILE ; 
  
ALTER PACKAGE "I4"."ORG_ADM" COMPILE ; 
  
ALTER PACKAGE "I4"."ORG_I4" COMPILE ; 
  
ALTER PACKAGE "I4"."ORG_LIB" COMPILE ; 
  
ALTER PACKAGE "I4"."USE_ADM" COMPILE ; 
  
ALTER PACKAGE "I4"."USE_I4" COMPILE ; 
  
ALTER PACKAGE  "I4"."CON_LIB" COMPILE PACKAGE ; 
  
ALTER PACKAGE  "I4"."GEO_LIB" COMPILE PACKAGE ; 
  
ALTER PACKAGE  "I4"."HTM_LIB" COMPILE PACKAGE ; 
  
ALTER PACKAGE  "I4"."IMP_I4" COMPILE PACKAGE ; 
  
ALTER PACKAGE  "I4"."ORG_ADM" COMPILE PACKAGE ; 
  
ALTER PACKAGE  "I4"."ORG_I4" COMPILE PACKAGE ; 
  
ALTER PACKAGE  "I4"."ORG_LIB" COMPILE PACKAGE ; 
  
ALTER PACKAGE  "I4"."TEX_I4" COMPILE PACKAGE ; 
  
ALTER PACKAGE  "I4"."USE_ADM" COMPILE PACKAGE ; 
  
ALTER PACKAGE  "I4"."USE_I4" COMPILE PACKAGE ; 
  
ALTER PACKAGE  "I4"."USE_PUB" COMPILE PACKAGE ; 
  
--  There are no triggers to rebuild.

