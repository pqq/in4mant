-- -----------------------------
-- --------- 01 ----------------
-- -----------------------------
delete from org_rights where org_rights_pk in (4,5);
insert into org_rights values(4, 'strongbeer' , 'strongbeer');
insert into org_rights values(5, 'strongwine' , 'strongwine');

delete from tex_text where tex_text_pk in ('db.org_rights:strongbeer', 'db.org_rights:strongwine');
insert into tex_text values('db.org_rights:strongbeer', 'nor', 'Sterk�l');
insert into tex_text values('db.org_rights:strongwine', 'nor', 'Sterkvin');

-- -----------------------------
-- --------- 02 ----------------
-- -----------------------------
@..\org_value_type.sql

delete from tex_text where tex_text_pk in (
	  'db.org_value_type:seats_indoor',
	  'db.org_value_type:seats_outdoor',
	  'db.org_value_type:beer_price',
	  'db.org_value_type:wine_price',
	  'db.org_value_type:capacity',
	  'db.org_value_type:floors',
	  'db.org_value_type:hire',
	  'db.org_value_type:no'
);

insert into tex_text values('db.org_value_type:seats_indoor', 'nor', 'Sitteplasser inne');
insert into tex_text values('db.org_value_type:seats_outdoor', 'nor', 'Sitteplasser ute');
insert into tex_text values('db.org_value_type:beer_price', 'nor', '�lpris');
insert into tex_text values('db.org_value_type:wine_price', 'nor', 'Vinpris');
insert into tex_text values('db.org_value_type:capacity', 'nor', 'Kapasitet');
insert into tex_text values('db.org_value_type:floors', 'nor', 'Etasjer');
insert into tex_text values('db.org_value_type:hire', 'nor', 'Utleie');
insert into tex_text values('db.org_value_type:no', 'nor', 'stk');

-- -----------------------------
-- --------- 03 ----------------
-- -----------------------------
@..\lis.sql
@..\tex_text-lis_list.sql

commit;