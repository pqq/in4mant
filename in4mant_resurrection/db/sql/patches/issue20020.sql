drop table LOG_UPLOAD cascade constraints;

/*==============================================================*/
/* Table: LOG_UPLOAD                                            */
/*==============================================================*/
create table LOG_UPLOAD  (
   LOG_UPLOAD_PK        NUMBER                          not null,
   CONTENT              BLOB,
   SRC_FILENAME         VARCHAR2(128),
   TRG_FILENAME         VARCHAR2(32),
   TRG_DIRECTORY        VARCHAR2(32),
   BOOL_WRITTEN         NUMBER(1),
   MESSAGE              VARCHAR2(128),
   SES_SESSION_ID       NUMBER,
   DATE_INSERT          DATE,
   constraint PK_LOG_UPLOAD primary key (LOG_UPLOAD_PK)
);

PROMPT Remember to commit the changes...
PROMPT Also run recompileAll.sql (if needed)
