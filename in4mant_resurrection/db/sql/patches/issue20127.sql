/* view all different sources in the db */
select distinct(source)
from org_organisation;

/* count */
select count(*)
from org_organisation
where parent_org_organisation_fk <> 900
and source = 'IMP:imp_i4';


/* *****************
   UPDATE
 * *****************/

/* update all imported entries with fictious parent */
update org_organisation
set source = 'imp_i4(old):excel:council'
where parent_org_organisation_fk = 900
and source = 'IMP:imp_i4';

/* update all imported entries real parents */
update org_organisation
set source = 'imp_i4(old):SHDIR'
where parent_org_organisation_fk <> 900
and source = 'IMP:imp_i4';


