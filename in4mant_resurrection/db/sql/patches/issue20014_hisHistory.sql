alter table HIS_HISTORY
   drop constraint FK_HIS_HIST_REFERENCE_USE_USER;

drop table HIS_HISTORY cascade constraints;

/*==============================================================*/
/* Table: HIS_HISTORY                                           */
/*==============================================================*/
create table HIS_HISTORY  (
   HIS_HISTORY_PK       NUMBER                          not null,
   TABLE_NAME           VARCHAR2(128),
   COLUMN_NAME          VARCHAR2(128),
   OLD_VALUE            VARCHAR2(4000),
   USE_USER_FK          NUMBER,
   SES_SESSION_ID       NUMBER,
   DATE_INSERT          DATE,
   constraint PK_HIS_HISTORY primary key (HIS_HISTORY_PK)
);

alter table HIS_HISTORY
   add constraint FK_HIS_HIST_REFERENCE_USE_USER foreign key (USE_USER_FK)
      references USE_USER (USE_USER_PK)
      on delete cascade;
