-- -------------------------------------------------------------------------------
-- create a fictitious parent organisation
-- -------------------------------------------------------------------------------
insert into org_organisation
(org_organisation_pk, name, org_number, bool_umbrella_organisation, source, date_insert, date_update)
values(900, '900 - Fictitious Parent', '900', 1, 'manual sql', sysdate, sysdate);


-- -------------------------------------------------------------------------------
-- update all organisations without a parent to be related to this new parent
-- -------------------------------------------------------------------------------
update	org_organisation
set		parent_org_organisation_fk = 900
where	org_organisation_pk in
(
	select	org_organisation_pk
	from	org_organisation
	where	(bool_umbrella_organisation is null or bool_umbrella_organisation = 0)
	and		parent_org_organisation_fk is null
);

commit;
