drop table IMP_NIGHTCLUB_OWNER cascade constraints;

/*==============================================================*/
/* Table: IMP_NIGHTCLUB_OWNER                                   */
/*==============================================================*/
create table IMP_NIGHTCLUB_OWNER  (
   ORG_NUMBER           VARCHAR2(16)                    not null,
   NAME                 VARCHAR2(64),
   ADDRESS_FULL         VARCHAR2(512),
   STREET               VARCHAR2(256),
   COUNCIL              VARCHAR2(64),
   ZIP                  VARCHAR2(16),
   LANDLINE             VARCHAR2(32),
   FAX                  VARCHAR2(32),
   EMAIL                VARCHAR2(64),
   URL                  VARCHAR2(128),
   SOURCE               VARCHAR2(32),
   MD5CHECKSUM          VARCHAR2(32),
   DATE_INSERT          DATE,
   DATE_IMPORT          DATE,
   constraint PK_IMP_NIGHTCLUB_OWNER primary key (ORG_NUMBER)
);





drop table IMP_NIGHTCLUB cascade constraints;

/*==============================================================*/
/* Table: IMP_NIGHTCLUB                                         */
/*==============================================================*/
create table IMP_NIGHTCLUB  (
   IMP_NIGHTCLUB_PK     NUMBER                          not null,
   ORG_NUMBER           VARCHAR2(16),
   NAME                 VARCHAR2(64),
   ADDRESS_FULL         VARCHAR2(512),
   STREET               VARCHAR2(256),
   COUNCIL              VARCHAR2(64),
   ZIP                  VARCHAR2(16),
   LANDLINE             VARCHAR2(32),
   FAX                  VARCHAR2(32),
   EMAIL                VARCHAR2(64),
   URL                  VARCHAR2(128),
   BOOL_HAS_BEER        NUMBER(1),
   BOOL_HAS_WINE        NUMBER(1),
   BOOL_HAS_SPIRITS     NUMBER(1),
   SOURCE               VARCHAR2(32),
   MD5CHECKSUM          VARCHAR2(32),
   DATE_INSERT          DATE,
   DATE_IMPORT          DATE,
   constraint PK_IMP_NIGHTCLUB primary key (IMP_NIGHTCLUB_PK)
);
