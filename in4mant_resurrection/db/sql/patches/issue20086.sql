alter table ORG_ORIGINAL
   drop constraint FK_ORG_ORIG_REFERENCE_ORG_ORGA;

drop table ORG_ORIGINAL cascade constraints;

/*==============================================================*/
/* Table: ORG_ORIGINAL                                          */
/*==============================================================*/
create table ORG_ORIGINAL  (
   ORG_ORGANISATION_FK_PK NUMBER                          not null,
   NAME                 VARCHAR2(64),
   ORG_NUMBER           VARCHAR2(16),
   constraint PK_ORG_ORIGINAL primary key (ORG_ORGANISATION_FK_PK)
);

alter table ORG_ORIGINAL
   add constraint FK_ORG_ORIG_REFERENCE_ORG_ORGA foreign key (ORG_ORGANISATION_FK_PK)
      references ORG_ORGANISATION (ORG_ORGANISATION_PK)
      on delete cascade;
