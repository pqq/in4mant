-- delete entries in case we run this script the second time
delete from org_original;
commit;


-- -------------------------------
-- set org_original
-- -------------------------------

set feedback off
set verify off
set echo off
prompt Creating an updating / insert script for org_original...
set termout off
set pages 80
set heading off
set linesize 120
spool tmp.sql
select 'set define off'||chr(13) from dual;
select 'insert into org_original (org_organisation_fk_pk) values ('||org_organisation_pk||');'||chr(13)
from org_organisation;
select 'update org_original 
set name='''||replace(name, '''', '''''')||''', org_number='''||org_number||''' 
where org_organisation_fk_pk='||org_organisation_pk||';'||chr(13)
from org_organisation;
select 'commit;' from dual;
spool off
set termout on
prompt Running the script now...
set termout off
@tmp;
exit
/
