set feedback off
set verify off
set echo off
prompt Creating an updating sql script for long / lat
set termout off
set pages 80
set heading off
set linesize 120
spool tmp.sql
select 'set define off'||chr(13) from dual;

select 'UPDATE add_address set latitude='||replace(a1.latitude,',','.')||', longitude='||replace(a1.longitude,',','.')||' where add_address_pk = '||a2.add_address_pk||';'
from add_address a1, add_address a2
where a1.street = a2.street
and a1.zip = a2.zip
and a1.zip is not null
and a1.street is not null
and a1.latitude is not null
and a2.latitude is null;

select 'commit;' from dual;
spool off
set termout on
prompt Running the script now...
set termout off
@tmp;
exit
/
