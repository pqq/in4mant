-- -----------------------------------------------------------------------
-- delete parent organisations that has not got any member children
-- -----------------------------------------------------------------------
delete
from org_organisation
where bool_umbrella_organisation = 1
and	org_organisation_pk not in
(
	select	parent_org_organisation_fk
	from	org_organisation
	where	(bool_umbrella_organisation is null or bool_umbrella_organisation = 0)
	and		org_status_fk = (select org_status_pk from org_status where name = 'member')
	and parent_org_organisation_fk is not null
);


-- -----------------------------------------------------------------------
-- delete child organisations that are not members
-- -----------------------------------------------------------------------
delete
from org_organisation
where (bool_umbrella_organisation is null or bool_umbrella_organisation = 0)
and	org_status_fk != (select org_status_pk from org_status where name = 'member');

commit;
