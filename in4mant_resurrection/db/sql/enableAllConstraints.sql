-- -------------------------------
-- enable all constraints
-- -------------------------------

set feedback off
set verify off
set echo off
prompt Finding constraints to enable...
set termout off
set pages 80
set heading off
set linesize 120
spool tmp_enable.sql
--select 'spool igen_enable.log;' from dual;
select 'ALTER TABLE '||substr(c.table_name,1,35)||
' ENABLE CONSTRAINT '||constraint_name||' ;'
from user_constraints c, user_tables u
where c.table_name = u.table_name;
select 'commit;' from dual;
spool off
set termout on
prompt Enabling constraints now...
set termout off
@tmp_enable;
--!rm -i tmp_enable.sql;
exit
/
