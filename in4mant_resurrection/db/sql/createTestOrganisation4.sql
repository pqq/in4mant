-- -----------
-- delete
-- -----------
delete from org_organisation
where org_organisation_pk = 10000004;

delete from add_address
where add_address_pk = 10000004;

delete from use_user
where use_user_pk = 10000004;

-- -----------
-- update
-- -----------
update use_user
set username = 'borge__1'
where lower(username) = 'borge';

-- -----------
-- insert
-- -----------
insert into add_address
(add_address_pk, street, zip, geo_geography_fk, date_insert, date_update)
values(10000004, 'Folke Bernadottes vei 7c', '0862', (select max(geo_geography_pk) from geo_geography where name = 'Oslo'), sysdate, sysdate);

insert into org_organisation
(org_organisation_pk, name, parent_org_organisation_fk, add_address_fk, date_insert, date_update, source, date_status_change, org_status_fk)
values (10000004, 'Borge Bar', 900, 10000004, sysdate, sysdate, 'createTestOrganisation3.sql', sysdate, (select org_status_pk from org_status where name = 'member'));

insert into org_original
(org_organisation_fk_pk, name)
values (10000004, 'Borge Bar');

insert into org_ser (org_organisation_fk_pk, ser_service_fk_pk)
values (10000004, (select ser_service_pk from ser_service where name = 'uteliv'));

insert into use_user
(use_user_pk, username, email, password, use_type_fk, use_status_fk)
values 
(
	  10000004
	, 'borge'
	, 'borge@uteliv.no'
	, 'borge'
	, (select use_type_pk from use_type where name = 'organisation')
	, (select use_status_pk from use_status where name = 'activated')
);

insert into use_org
(use_user_fk_pk, org_organisation_fk_pk)
values (10000004, 10000004);

commit;