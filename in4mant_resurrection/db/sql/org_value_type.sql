delete from org_value;
delete from org_value_type;

insert into org_value_type values(1, 'cover_charge', 'currency', 'cover charge', 1, 1, 1);
insert into org_value_type values(2, 'seats_indoor', 'no', 'number seats indoor', 1, 0, 1);
insert into org_value_type values(3, 'seats_outdoor', 'no', 'number seats outdoor', 1, 0, 1);
insert into org_value_type values(4, 'dance_floors', null, 'number dance floors', 1, 0, 1);
insert into org_value_type values(5, 'beer_price', 'currency', 'price of beer', 1, 1, 1);
insert into org_value_type values(6, 'wine_price', 'currency', 'price of wine', 1, 1, 1);
insert into org_value_type values(7, 'capacity', 'no', 'total capacity', 1, 0, 1);
insert into org_value_type values(8, 'floors', null, 'number of floors', 1, 0, 1);
insert into org_value_type values(9, 'hire', null, null, 0, 1, 1);