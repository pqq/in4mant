set hea off

-- -- 
-- THIS SQL SCRIPT IS TO CHECK THE DATABASE 
-- This should be runned next to tidyDatabase.sql to check if data is OK 
-- However errors found in this script are not fixed running tidyDatabase.sql 
-- 
-- Author:	Frode Klevstul (frode@klevstul.com) 
-- Started:	20th April 2006 
-- -- 


-- ---------------------------------------------------------------
PROMPT :::
PROMPT ::: Any children without parents? :::
-- ---------------------------------------------------------------
select	org_organisation_pk, name
from	org_organisation
where	(bool_umbrella_organisation is null or bool_umbrella_organisation = 0)
and		parent_org_organisation_fk is null;


-- ---------------------------------------------------------------
PROMPT :::
PROMPT ::: Any duplicate organisations? :::
-- ---------------------------------------------------------------
select o1.org_organisation_pk, o2.org_organisation_pk, o1.name
from  org_organisation o1
	, add_address a1
	, org_organisation o2
	, add_address a2
where	o1.add_address_fk = a1.ADD_ADDRESS_PK
and		o2.ADD_ADDRESS_FK = a2.ADD_ADDRESS_PK
and		o1.name = o2.name
and		o1.BOOL_UMBRELLA_ORGANISATION = o2.BOOL_UMBRELLA_ORGANISATION
and		o1.ORG_ORGANISATION_PK <> o2.ORG_ORGANISATION_PK
and		a1.ZIP = a2.zip;
