-- ------------------------
-- truncate all tables
-- ------------------------
@truncateAllTables.sql

-- ------------------------
-- disable triggers
-- ------------------------
@disableAllTriggers.sql
