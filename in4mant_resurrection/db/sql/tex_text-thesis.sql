insert into tex_text values('pag_pub.contactPage:contactTxt', 'nor', '
Hvis du skulle ha noen sp�rsm�l eller problemer med tjenesten, ber vi deg ta kontakt med oss p� epost adressen "<a href="mailto:post@informant.no">post@informant.no</a>.
<br><br>I dag har Uteliv.no kontor i Oslo. V�r bes�ksadresse og postadresse er:<pre>
Uteliv.no 	
Co/Informant DA
Sognsvannsveien 60
0372 Oslo
Norge

Organisasjonsnummer: 981 412 125</pre>
');


insert into tex_text values('pag_pub.advertisePage:advertiseTxt', 'nor', '
<pre>
Profilere utestedet p� Uteliv.no

Uteliv.no er et samlested for Norges utesteder. Ved � annonsere/profilere ditt utested p� Uteliv.no kan du 
selv bestemme hvilken informasjon som skal vises. Vi selger dermed synlighet og markedsf�ring for ditt 
utested. Utestedene som profilerer seg f�r tilgang til et administrasjonsverkt�y gjennom et unikt brukernavn 
og passord, noe som gj�r det b�de sikkert og enkelt � oppdatere all informasjon om utestedet.


P� Uteliv.no kan utesteder legge inn:
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
Kontaktinformasjon: adresse, telefon, faks, e-post, samt link til utestedets hjemmeside

Utestedspresentasjon: Her er det plass til en fyldig beskrivelse av deres utested.

Utestedslogo: Her legges utestedets logo ut.

�pningstider: Legg ut utestedets �pningstider.

Rettigheter og aldersgrense: Legg ut hvilke rettigheter ditt utested har, samt aldersgrense.

Type sted: Legg ut hva slags type sted ditt utested er, er det for eksempel konsertsted, bar, diskotek, kafe, 
restaurant, scene og/eller festival eller liknende.

Utvalgt bildet: Legg ut et bilde som representerer ditt utested p� Uteliv.no.

Attraksjoner og annen info: Mange utesteder har ulike attraksjoner de �nsker � fremheve Disse kan du 
profilere p� Uteliv.no. Dette kan v�re informasjon som; Cover, Antall sitteplasser, betalingsmuligheter, 
musikkstil, spill, type klientell, kurs etc.

Arrangement: Fortell hva som skjer p� ditt utested. Arrangementene legges inn p� en sv�rt enkel m�te og kan 
inneholde s� mye informasjon dere �nsker, Bilder kan selvf�lgelig ogs� legges ved.

Nyheter: Legg ut nyheter og pressemeldinger fra ditt utested. Nyhetene kan innholde blant annet tekst, 
linker og bilder.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

Prioritering: N�r utestedet velger � profilere seg hos Uteliv.no, vil utestedet bli presentert blant de 
f�rste utestedene n�r noen s�ker p� utesteder p� Uteliv.no. Dette medf�rer at en profilering vil v�re 
sv�rt effektiv for � gj�re utestedet kjent, og utestedet forsvinner ikke i mengden. 

Hvis dere �nsker � profilere ditt utested p� Uteliv.no, s� ta kontakt med oss p� tlf 454 80 808 eller 
send e-post til p� post@informant.no for � f� priser og tilbud.

Utvalgt utested: Videre kan utesteder velge � v�re "Utvalgt utested" p� forsiden av Uteliv.no. Prisene 
varierer ut fra varighet og sesong.

Annonsere p� Uteliv.no

Bannerannonsering: Hvis dere �nsker � ha bannere eller andre typer annonser p� Uteliv.no, s� ta kontakt 
med oss for � f� priser og oversikt over hvilke muligheter vi kan tilby.

<b>Kontakt:</b>
Telefon: 454 808080
Epost: <a href="mailto:post@informant.no">post@informant.no</a>

</pre>
');


insert into tex_text values('pag_pub.registerPage:registerTxt', 'nor', '
<pre>
For � registrere deres utested p� Uteliv.no ta kontakt med oss p�:
Telefon: 454 808080
Epost: <a href="mailto:post@informant.no">post@informant.no</a>
</pre>
');


insert into tex_text values('pag_pub.aboutPage:aboutTxt', 'nor', '
<pre>
Uteliv.no ble lansert 1 mars 2006. Dette er en katalogtjeneste som skal inneholde alle Norges utesteder.
Tjenesten skal gj�re at publikum vil f� den beste oversikten over alle utesteder, restauranter, puber, 
scener m.m. i Norge. Dette gj�r at vi vil bli den mest komplette og p�litelige utelivskatalogen p� 
Internett. Vi gir deg den st�rst sannsynlighet for � finne informasjon om det utestedet du s�ker.

For utesteder, restauranter, puber, barer, scener osv som velger � benytte seg av Uteliv.no, kan vi 
garantere stor synlighet p� internett. Uteliv.no er den eneste katalogtjeneste i dag som er s� h�y grad 
tilpasset utelivsbedrifter, man kan sv�rt enkelt oppdatere og legge ut den informasjonen man �nsker. 
Blant annet kan man legge ut: logo, kontaktinformasjon, rettigheter, �pningstider, bilder, generell 
informasjon, nyheter og kalender.

Vi selger synlighet og markedsf�ring.
Alle bedrifter trenger � v�re synlig for � f� nye kunder, og det er en kamp for � f� mest oppmerksomhet. 
Ved � profilere/annonsere p� uteliv.no kommer utestedet bedre frem i s�k. Dette gj�r at ditt utested 
skiller seg fra dine konkurrenter, noe som vil gi �kt oppmerksomhet, meget god markedsf�ring og �kt salg.

Kvalitet og seri�sitet
Uteliv.no fokuserer p� kvalitet og seri�sitet. Vi sender ikke faktura f�r vi har f�tt skriftlig 
bekreftelse p� din bestilling. Alle avtaler er ett�rige, uten automatisk fornyelse. Etter endt 
avtaleperiode vil alle v�re kunder bli kontaktet , og i samr�d med deg som kunde settes det opp en ny 
kontrakt for nye 12 m�neder som signeres og returneres til oss. 

Uteliv.no drives og eies av selskapet Informant DA.

Informant DA
Organisasjonsnummer: 981 412 125 
Adresse: Sognsvannsveien 60
0372 OSLO
Epost: post@informant.no
Telefon: 45 48 08 08
</pre>
');
