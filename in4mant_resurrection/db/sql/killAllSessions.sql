set feedback off
set verify off
set echo off
prompt Finding sessions to kill...
set termout off
set pages 80
set heading off
set linesize 120
spool tmp.sql

select 'alter system kill session '''||SESS.SID||', '||SESS.SERIAL#||''';' from v$session SESS where USERNAME='I4_MIRROR';
select 'commit;' from dual;

spool off

@tmp.sql;
exit
/