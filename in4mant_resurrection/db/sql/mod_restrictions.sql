delete from mod_restriction;

-- *** no_login ***
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'pag_pub.aboutPage')
 ,(select use_type_pk from use_type where name = 'no_login') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'pag_pub.contactPage')
 ,(select use_type_pk from use_type where name = 'no_login') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'pag_pub.advertisePage')
 ,(select use_type_pk from use_type where name = 'no_login') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'pag_pub.displayHiddenValues')
 ,(select use_type_pk from use_type where name = 'no_login') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'pag_pub.registerPage')
 ,(select use_type_pk from use_type where name = 'no_login') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'pag_pub.sendLinkPage')
 ,(select use_type_pk from use_type where name = 'no_login') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'use_pub.pleaseLogin')
 ,(select use_type_pk from use_type where name = 'no_login') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'org_pub.viewOrg')
 ,(select use_type_pk from use_type where name = 'no_login') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'pag_pub.frontPage')
 ,(select use_type_pk from use_type where name = 'no_login') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'use_pub.register')
 ,(select use_type_pk from use_type where name = 'no_login') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'ext_sea.generateList')
 ,(select use_type_pk from use_type where name = 'no_login') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'con_pub.viewCon')
 ,(select use_type_pk from use_type where name = 'no_login') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'pag_pub.userLogin')
 ,(select use_type_pk from use_type where name = 'no_login') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'con_xca.show')
 ,(select use_type_pk from use_type where name = 'no_login') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'pag_pub.errorPageSimple')
 ,(select use_type_pk from use_type where name = 'no_login') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'add_pub.viewMap')
 ,(select use_type_pk from use_type where name = 'no_login') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'pic_pub.viewPicture')
 ,(select use_type_pk from use_type where name = 'no_login') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'pic_pub.viewAlbum')
 ,(select use_type_pk from use_type where name = 'no_login') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'pag_pub.owaSqlError')
 ,(select use_type_pk from use_type where name = 'no_login') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'con_pub.listCon')
 ,(select use_type_pk from use_type where name = 'no_login') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'pag_pub.jobsPage')
 ,(select use_type_pk from use_type where name = 'no_login') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'pag_pub.partnerPage')
 ,(select use_type_pk from use_type where name = 'no_login') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'pic_pub.viewAlbums')
 ,(select use_type_pk from use_type where name = 'no_login') , sysdate );

-- *** normal ***
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'use_adm.updateUserInfo')
 ,(select use_type_pk from use_type where name = 'normal') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'use_pub.logout')
 ,(select use_type_pk from use_type where name = 'normal') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'log_pub.viewLogHits')
 ,(select use_type_pk from use_type where name = 'normal') , sysdate );


-- *** organisation ***
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'org_adm.updateOrgName')
 ,(select use_type_pk from use_type where name = 'organisation') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'org_adm.updatePicture')
 ,(select use_type_pk from use_type where name = 'organisation') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'org_adm.updateAddress')
 ,(select use_type_pk from use_type where name = 'organisation') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'org_adm.updateListOrg')
 ,(select use_type_pk from use_type where name = 'organisation') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'org_adm.updateOrgValues')
 ,(select use_type_pk from use_type where name = 'organisation') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'org_adm.viewOrg')
 ,(select use_type_pk from use_type where name = 'organisation') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'org_adm.updateGeneralInformation')
 ,(select use_type_pk from use_type where name = 'organisation') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'org_adm.updateDetailsInfo')
 ,(select use_type_pk from use_type where name = 'organisation') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'con_adm.admContent')
 ,(select use_type_pk from use_type where name = 'organisation') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'pic_adm.editAlbums')
 ,(select use_type_pk from use_type where name = 'organisation') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'pic_adm.editPictures')
 ,(select use_type_pk from use_type where name = 'organisation') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'con_adm.conOverview')
 ,(select use_type_pk from use_type where name = 'organisation') , sysdate );


-- *** admin_normal ***
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'i4_pub.overview')
 ,(select use_type_pk from use_type where name = 'admin_normal') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'org_i4.addOrg')
 ,(select use_type_pk from use_type where name = 'admin_normal') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'use_i4.addUser')
 ,(select use_type_pk from use_type where name = 'admin_normal') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'org_i4.updateOrgStatus')
 ,(select use_type_pk from use_type where name = 'admin_normal') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'org_i4.loginAsOrg')
 ,(select use_type_pk from use_type where name = 'admin_normal') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'org_i4.updateOrgService')
 ,(select use_type_pk from use_type where name = 'admin_normal') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'org_i4.admChosenOrg')
 ,(select use_type_pk from use_type where name = 'admin_normal') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'kAdmin.list')
 ,(select use_type_pk from use_type where name = 'admin_normal') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'kAdmin.insert')
 ,(select use_type_pk from use_type where name = 'admin_normal') , sysdate );
insert into mod_restriction values (
  (select mod_module_pk from mod_module where name = 'kAdmin.update')
 ,(select use_type_pk from use_type where name = 'admin_normal') , sysdate );


commit;
