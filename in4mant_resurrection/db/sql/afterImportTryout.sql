-- ------------------------
-- enable triggers
-- ------------------------
@enableAllTriggers.sql

-- --------------------------------------------
-- re-create sequences
-- --------------------------------------------
@prodExport\sequences.sql

-- --------------------------------------------
-- delete unneccesary entries in database
-- --------------------------------------------
@fullCleanUp.sql

-- --------------------------------------------
-- update database to use test values
-- --------------------------------------------
@updateTryoutDatabase.sql

-- --------------------------------------------
-- create test organisation
-- --------------------------------------------
@createTestOrganisation.sql

commit;

exit;