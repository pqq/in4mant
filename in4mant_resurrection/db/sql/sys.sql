delete from sys_parameter_type;
insert into sys_parameter_type values(1, 'number_display', 'parameters that controls number of items to display');
insert into sys_parameter_type values(2, 'layout', 'parameters that controls layout like colours etc');
insert into sys_parameter_type values(3, 'path_os', 'paths on the operating system');
insert into sys_parameter_type values(4, 'path_url', 'web paths');
insert into sys_parameter_type values(5, 'log_debug', 'paramteres that control logging and debug');
insert into sys_parameter_type values(6, 'session', 'paramteres that controls session logic');
insert into sys_parameter_type values(7, 'language_text', 'paramteres that controls the language and text on the site');
insert into sys_parameter_type values(8, 'navigation', 'paramteres that controls navigation on the site');
insert into sys_parameter_type values(9, 'time_date', 'paramteres that controls time and date settings');
insert into sys_parameter_type values(10, 'db_specific', 'db parameters');
insert into sys_parameter_type values(11, 'access_control', 'access control and security parameters');
insert into sys_parameter_type values(12, 'file_control', 'file control parameters');

delete from sys_parameter;
insert into sys_parameter values(1, 'debug', '"1" = debug on, "0" = debug off', '1', 5, 1, sysdate, sysdate);
insert into sys_parameter values(2, 'debug-mode', '"db" = in db, "web" = on web, "all" = all', 'db', 5, 1, sysdate, sysdate);
insert into sys_parameter values(3, 'log_mod', '"1" = log on, "0" = log off', '1', 5, 1, sysdate, sysdate);
insert into sys_parameter values(4, 'log_mod-mode', '"insert"=new insert each time, "update"=update date_update only', 'update', 5, 1, sysdate, sysdate);
insert into sys_parameter values(5, 'ses_instance', '"1" = instances on, "0" = instances off', '1', 6, 1, sysdate, sysdate);
insert into sys_parameter values(6, 'max_fake_ses_id', 'no fake session ids before we show error page', '3', 6, 1, sysdate, sysdate);
insert into sys_parameter values(7, 'language', 'default language on the site', 'nor', 7, 1, sysdate, sysdate);
insert into sys_parameter values(8, 'logout_package', 'package to jump to on logout', 'pag_pub.frontPage', 8, 1, sysdate, sysdate);
insert into sys_parameter values(9, 'login_package', 'package to jump to on login', 'pag_pub.frontPage', 8, 1, sysdate, sysdate);
insert into sys_parameter values(10, 'page_width', 'the width of all pages', '950', 2, 1, sysdate, sysdate);
insert into sys_parameter values(11, 'log_tex', '"1" = log on, "0" = log off', '1', 5, 1, sysdate, sysdate);
insert into sys_parameter values(12, 'img_org', 'relative path for organisation images', '/img/org', 3, 1, sysdate, sysdate);
insert into sys_parameter values(13, 'img_i4', 'relative path for in4mant graphics', '/img/i4', 3, 1, sysdate, sysdate);
insert into sys_parameter values(14, 'dateFormat_hhmi', 'time format for hours and minutes', 'hh24:mi', 9, 1, sysdate, sysdate);
insert into sys_parameter values(15, 'restriction-mode', 'strict-> default restricted, open-> default open', 'open', 11, 1, sysdate, sysdate);
insert into sys_parameter values(16, 'dir_img_org', 'name of dba_directory: path to org images', 'DIR_IMG_ORG', 10, 1, sysdate, sysdate);
insert into sys_parameter values(17, 'org_logo', 'format for org logo name (+ org_pk in front and filetype after)', '_logo', 3, 1, sysdate, sysdate);
insert into sys_parameter values(18, 'max_logo_size', 'max filesize in bytes for organisation logo', '15000', 2, 1, sysdate, sysdate);
insert into sys_parameter values(19, 'max_logo_width', 'max width in pixels for organisation logo', '120', 2, 1, sysdate, sysdate);
insert into sys_parameter values(20, 'max_logo_height', 'max height in pixels for organisation logo', '90', 2, 1, sysdate, sysdate);
insert into sys_parameter values(21, 'org_mpho', 'format for org mainphoto (+ org_pk in front and filetype after)', '_main', 3, 1, sysdate, sysdate);
insert into sys_parameter values(22, 'max_mpho_size', 'max filesize in bytes for organisation mainphoto', '30000', 2, 1, sysdate, sysdate);
insert into sys_parameter values(23, 'max_mpho_width', 'max width in pixels for organisation mainphoto', '200', 2, 1, sysdate, sysdate);
insert into sys_parameter values(24, 'max_mpho_height', 'max height in pixels for organisation mainphoto', '200', 2, 1, sysdate, sysdate);
insert into sys_parameter values(25, 'no_logo', 'filname for no logo picture', 'noLogo.jpg', 3, 1, sysdate, sysdate);
insert into sys_parameter values(26, 'no_photo', 'filname for no photo picture', 'noPhoto.jpg', 3, 1, sysdate, sysdate);
insert into sys_parameter values(27, 'service', 'default service on the site', 'uteliv', 8, 1, sysdate, sysdate);
insert into sys_parameter values(28, 'con_pic', 'format for content pic (+ con_pk in front and filetype after)', '_con', 3, 1, sysdate, sysdate);
insert into sys_parameter values(29, 'max_cpho_size', 'max filesize in bytes for content photo', '100000', 2, 1, sysdate, sysdate);
insert into sys_parameter values(30, 'max_cpho_width', 'max width in pixels for content photo', '700', 2, 1, sysdate, sysdate);
insert into sys_parameter values(31, 'max_cpho_height', 'max height in pixels for content photo', '350', 2, 1, sysdate, sysdate);
insert into sys_parameter values(32, 'dateFormat_ymd', 'time format for day, mon, year', 'dd.mm.yy', 9, 1, sysdate, sysdate);
insert into sys_parameter values(33, 'dateFormat_full', 'full date format', 'dd mon yy - hh24:mi', 9, 1, sysdate, sysdate);
insert into sys_parameter values(34, 'dateFormat_short', 'short version of full date format', 'dd.mm.yy hh24:mi', 9, 1, sysdate, sysdate);
insert into sys_parameter values(35, 'dir_scripts', 'relative path for scripts etc', '/scripts', 3, 1, sysdate, sysdate);
insert into sys_parameter values(36, 'admlogin_package', 'package to jump to on login for adm', 'org_adm.viewOrg', 8, 1, sysdate, sysdate);
insert into sys_parameter values(37, 'dir_org_imgbase', 'path to org image base dir for upload', 'C:\Informant\www\img\org', 3, 1, sysdate, sysdate);
insert into sys_parameter values(38, 'convert', 'full path to convert program', '/usr/bin/convert', 3, 1, sysdate, sysdate);
insert into sys_parameter values(39, 'dir_modOwa', 'relative path to mod_owa directory', '/owa', 4, 1, sysdate, sysdate);
insert into sys_parameter values(40, 'loginBar', '1=show login, 0=hide login', '1', 2, 1, sysdate, sysdate);
insert into sys_parameter values(41, 'i4admin_link', '1=show login, 0=hide login', '1', 2, 1, sysdate, sysdate);
insert into sys_parameter values(42, 'no_searchHits', 'Number of search hits to display on one page', '20', 1, 1, sysdate, sysdate);
insert into sys_parameter values(43, 'upl_pic_formats', 'Allowed formats for uplading photos', 'gif jpg jpeg bmp', 12, 1, sysdate, sysdate);
insert into sys_parameter values(44, 'max_pic_size', 'max filesize in bytes for PIC pictures', '300000', 2, 1, sysdate, sysdate);
insert into sys_parameter values(45, 'max_pic_width', 'max width in pixels for PIC pictures', '700', 2, 1, sysdate, sysdate);
insert into sys_parameter values(46, 'max_pic_height', 'max height in pixels for PIC pictures', '700', 2, 1, sysdate, sysdate);
insert into sys_parameter values(47, 'thumb_pic_height', 'max height for PIC thumbs', '200', 2, 1, sysdate, sysdate);
insert into sys_parameter values(48, 'thumb_pic_width', 'max width for PIC thumbs', '200', 2, 1, sysdate, sysdate);
insert into sys_parameter values(49, 'ext_geocode', 'url to external geocode script', '/cgi-external/geocode.cgi?[street]---[zip]---[country]---[address_pk]---[returnUrl]---[addLongLatUrl]', 4, 1, sysdate, sysdate);
insert into sys_parameter values(50, 'dir_i4_imgbase', 'full path to i4 images', '/srv/www/in4mant/uteliv.no/img_mirror/i4', 3, 1, sysdate, sysdate);
insert into sys_parameter values(51, 'watermark', 'watermark for images', 'pic_watermark.png', 2, 1, sysdate, sysdate);
