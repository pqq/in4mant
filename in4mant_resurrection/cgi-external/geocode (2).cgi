#!C:/perl/bin/Perl.exe
#!/usr/local/bin/perl
#
# -----------------------------------------------------
# Filename:		geocode.cgi
# Author:		Frode Klevstul (frode@klevstul.com)
# Started:		17.07.06
# -----------------------------------------------------





# -------------------
# use
# -------------------
use strict;
use LWP::UserAgent;
use HTTP::Request::Common qw(GET);
use HTTP::Response;





# -------------------
# global declarations
# -------------------
my $userAgent = LWP::UserAgent->new;
my $request;

# - - - - -
# Get the parameters that are sent with to this cgi script
# - - - - -
my $query_string = $ENV{QUERY_STRING};
my ($street, $zip, $country, $address_pk, $returnUrl, $addLongLatUrl) = split('---', $query_string);
my $searchString = $street ."+". $zip;

# - - - - -
# HTTP referer
#
# Note:		This will be the refering page to the target URL, should often be same as "$preURL"
#			if you want this to make sense.
# - - - - -
my $httpReferer = "http://kart.gulesider.no/kart/index.c";

# - - - - -
# Pre URL
#
# Note:		This URL will be accessed first. Usefull when for example voting, so it looks like
#			a user loads a page before he actually votes. This should be the same as the HTTP
#			referer page to make sense. If "Pre URL" is set, this must return success when trying
#			to load the object. If not the "Target URL" won't be loaded.
# - - - - -
my $preUrl = "http://kart.gulesider.no/kart/index.c";

# - - - - -
# URL to request
#
# Note: 	If you're using POST request method the parameters to send has to be set when
#			initiating the request object. If you're using GET you have to set them in the
#			url
# Example:	http://www.myUrl.com/something/vote.php?action=vote&choice=77 	(GET method URL)
#			http://www.myUrl.com/something/vote.php							(POST method URL)
# - - - - -
my $targetUrl = "http://kart.gulesider.no/kart/search.c?q=".$searchString;





# -------------------
# main
# -------------------
&printTop;
&sendRequest;





# -------------------
# sub procedures
# -------------------
sub printTop{
	print "Content-type: text/html\n\n";
}


sub sendRequest{
	my $latitude;
	my $longitude;
	my $result;
	my $htmlObject;
	my $nextUrl;

	my $n;
	my $s;
	my $e;
	my $w;

	# - - - - -
	# Make sure we have all values needed to run the search
	# - - - - -
	if ($street eq undef or ($zip eq undef or $country eq undef) or $returnUrl eq undef or $addLongLatUrl eq undef){
		$nextUrl = $returnUrl;
		#print '<h1>Not enough arguments!</h1>\n';
	}

	# - - - - -
	# Initialize user agent
	# - - - - -
	$userAgent->timeout(15);																			# timeout, in seconds
	$userAgent->agent('Mozilla/5.0');																	# identify user agent as Mozilla

	# - - - - -
	# Initiate request object and request the object(s)
	# - - - - -
	$request = HTTP::Request->new(GET => $targetUrl);													# GET request method
	$request->referer($httpReferer);																	# set HTTP referer
	$result = $userAgent->get($preUrl);																	# request object (pre-url)
	$result = $userAgent->request($request);															# request object (wanted page)

	if ($result->is_success){
		$htmlObject = $result->content();
	} else {
		$nextUrl = $returnUrl;
		#print "error: did not manage to load object!<br \>";
	}

	# for testing purposes only
	#$htmlObject = &testResponse;

	# ---
	# format of line with coordinates:
	# ---
	# <input type="hidden" name="w" value="-5.94442629373536"/>
	# <input type="hidden" name="e" value="41.7444262937354"/>
	# <input type="hidden" name="s" value="57.7999966756045"/>
	# <input type="hidden" name="n" value="71.3"/>
	# ---
	if ($htmlObject =~ /<input type="hidden" name="n" value="(\d+\.\d+)"\/>/){
		$n = $1;
	}
	if ($htmlObject =~ /<input type="hidden" name="s" value="(\d+\.\d+)"\/>/){
		$s = $1;
	}
	if ($htmlObject =~ /<input type="hidden" name="e" value="(\d+\.\d+)"\/>/){
		$e = $1;
	}
	if ($htmlObject =~ /<input type="hidden" name="w" value="(\d+\.\d+)"\/>/){
		$w = $1;
	}

	if ($n eq undef or $s eq undef or $e eq undef or $w eq undef){
		$latitude = 0;
		$longitude = 0;
		#print "error: latitude / longitude not found!<br \>";
		#print $htmlObject;
	}

	# calculate longitude and latitude
	if ($latitude eq undef) {
		$latitude = ($n + $s) / 2;
		$longitude = ($e + $w) / 2;
	}

	# build the URL we have to go to next to insert long. and lat. (using addLongLat and returnUrl)
	if ($nextUrl eq undef) {
		$nextUrl = $addLongLatUrl . '?p_address_pk='.$address_pk.'&p_longitude='.$longitude.'&p_latitude='.$latitude.'&p_returnUrl='.$returnUrl;
	}

	#print 'Latitude: '.$latitude.' <br>';
	#print 'Longitude: '.$longitude.' <br>';
	#print 'Return to: '.$returnUrl.' <br>';
	#print 'Add Long Lat: '.$addLongLatUrl.' <br>';
#	print 'Jump to: '.$nextUrl.' <br>';

	print qq(
		<META HTTP-EQUIV="Refresh" CONTENT="0; URL=$nextUrl" />
		<!-- redirect user to address above --><br />
		<center><i>&nbsp;...&nbsp;</i></center><br />
	);

}


sub testResponse{
	my $response =
	qq(
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- InstanceBegin template="/Templates/nor_gs.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<link rel="SHORTCUT ICON" href="/favicon-gs.ico"/>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Gule Sider&reg; - Kart</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<script type="text/javascript" src="../jscript/gsi.js"></script>
<link href="../css/stilsett.css" rel="stylesheet" type="text/css" />
<link href="../css/kart.css" rel="stylesheet" type="text/css" />
<link href="../css/kart-popups.css" rel="stylesheet" type="text/css" />
<link href="../css/map_common.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/map_common.js" type="text/javascript"></script>
<!-- InstanceEndEditable -->
</head>
<body onload="">
<div id="container">
<div id="topp">
<iframe src="/textAds.c?url=http%3a%2f%2ffindexa.adbureau.net%2fbserver%2fAAMALL%2fSITE%3dgsi%2fPAGEID%3d76915157%2fAREA%3dMIDDLE%2fAAMSZ%3d185X70%2fTAB%3dmap%2fDOMINANS%3dY%2fAAMB1%2fTKI_TEXT_POS%3d1%2fACC_RANDOM%3d9353612%2fAAMB2%2fTKI_TEXT_POS%3d2%2fACC_RANDOM%3d49257%2fAAMB3%2fTKI_TEXT_POS%3d3%2fACC_RANDOM%3d68544619%2fAAMB4%2fTKI_TEXT_POS%3d4%2fACC_RANDOM%3d54296886%2fAAMB5%2fTKI_TEXT_POS%3d5%2fACC_RANDOM%3d55710182" width="740" height="58" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" class="txtads"></iframe>
<div class="logo">
<a href="http://www.gulesider.no/">
<img src="../img/logo-gs.gif" alt="gulesider.no" width="200" height="60" border="0" />
</a>
</div>
</div>
<div id="hovedfaner">
<p>
<a target="_parent" href="https://login.gulesider.no/myPage.do">Logg inn</a>
| <a href="/kart/index.c?spraak=en&amp;id=a_171877">English</a>
| <a target="_parent" href="/kart/hjelp.html">Hjelp</a>
</p>
<ul>
<li class="">
<a target="_parent" href="http://www.gulesider.no/" title="Her s&oslash;ker du etter bedrifter" onclick="return setq(this);">Gule&nbsp;Sider&reg;</a>
</li>
<li class="">
<a target="_parent" href="http://www.gulesider.no/tk/index.c" title="Her s&oslash;ker du etter privatpersoner" onclick="return setq(this);">
Telefonkatalogen&#8482;</a>
</li>
<li class="">
<a target="_parent" href="http://www.gulesider.no/yelo/index.c" title="S&oslash;k p&aring; internett" onclick="return setq(this);">
Netts&oslash;k</a>
</li>
<li class="aktiv">
<a target="_parent" href="http://www.gulesider.no/kart/index.c" title="Her s&oslash;ker du etter adresser og lager kj&oslash;reruter fra ett punkt til ett annet" onclick="return setq(this);">
Kart</a>
</li>
<li>
<a target="_parent" href="http://www.gulesider.no/rubrikk/" title="Rubrikk"
onclick="return setq(this);">Rubrikk</a>
</li>
<li class="">
<a target="_parent" href="http://gulesider.dinpris.no/" title="DinPris">Shopping</a>
</li>
<li>
<a target="_parent" href="https://login.gulesider.no/sms.do" title="Her kan du som innlogget bruker sende 5 gratis SMS om dagen">Mobil</a>
</li>
<li class="">
<a target="_parent" href="https://login.gulesider.no/myPage.do" title="Her redigerer/registrerer du din profil">Min side</a>
</li>
<li class="annet">
<a target="_parent" href="../andre/index.html" title="Andre tjenester fra Gule Sider">Andre&nbsp;tjenester</a>
</li>
</ul>
</div>
<form name="searchForm" action="../kart/search.c" method="get" target="_parent">
	<div id="soek">
		<p class="sokefelt">
			<strong class="txtlede">Karts&oslash;k</strong>

			<input name="q" type="text" class="txtnpt" size="40" value="" />
			<input type="submit" class="knapp" value="S&oslash;k" />
		</p>
		<p class="tipsk">Eks: "akersbakken 12 oslo" eller "Frognerparken"</p>
	</div>
</form>
<div id="innhold" class="spalte-to">
<div id="kolonne-left">
<div id="KartRes">
    <h2><img src="../img/kart/vimpel2.gif" width="10" height="10" alt="Vimpel">Valgt sted
    </h2>
    <div class="innhold">
<p class="adr">
            Folke Bernadottes vei 7 C<br/>0862 Oslo
</p>
    </div>
</div>
<form action="../kart/index.c" method="get">
<div id="finn">
	<h2>Finn i kart</h2>
		<div class="innhold">
			<input name="nearest" type="text" class="txtnpt" value=""/>
			<input type="submit" class="knapp" value="S&oslash;k" />
			<input type="hidden" name="w" value="10.733561898117"/>

			<input type="hidden" name="e" value="10.743561901883"/>
			<input type="hidden" name="s" value="59.963022236981"/>
			<input type="hidden" name="n" value="59.966319562398134"/>
			<input type="hidden" name="id" value="a_171877"/>
			<input type="hidden" name="markX" value=""/>
			<input type="hidden" name="markY" value=""/>
			<input type="hidden" name="marklat" value=""/>
			<input type="hidden" name="marklon" value=""/>
		</div>
</div>
</form>
<script type="text/javascript">
    <!--
    function switchStartStop(form) {
        var tmp = form.q.value;
        form.q.value = form.stopQ.value;
        form.stopQ.value = tmp;

        tmp = form.startId.value;
        form.startId.value = form.stopId.value;
        form.stopId.value = tmp;
    }
    //-->
</script>
<form action="../kart/start.c" method="get" id="routeInputForm">
    <div id="kjorerute">
        <h2>Kj&oslash;rerute</h2>
        <div class="flagg"><span class="startflagg">Start</span></div>
        <div class="innhold">
                    <input type="text" name="q" class="txtnpt"/>
       	</div>
        <div class="eksempel">Eks: "akersbakken 12 oslo"</div>
        <div class="flagg"><span class="stoppflagg">Stopp</span></div>
        <div class="innhold">
			<input type="text" name="stopQ" class="txtnpt" value="Folke Bernadottes vei 7 C"/>
            <input type="hidden" name="originalText" value="Folke Bernadottes vei 7 C"/>
            <input type="hidden" name="startId" value=""/>
            <input type="hidden" name="stopId" value="a_171877">
        </div>
        <div class="eksempel">Eks: "frognerparken"</div>
		<p style="padding-bottom:10px;">
			<input type="submit" value="Beregn kj&oslash;rerute" class="knappkjo"/>
        	<input type="button" onclick="switchStartStop(MM_findObj('routeInputForm', document));" value="Snu kj&oslash;reretning" class="knappkjo"/>
		</p>
    </div>
</form>
</div>
<div id="kolonne-hoved">
			<div class="rammesvart">
				<div class="kartnav">
					<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td class="padding">
								<a href="javascript:mapZoomByFactor(0.5)" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Pluss','','../img/kart/zoombar_pluss_on.gif',1)"><img src="../img/kart/zoombar_pluss_off.gif" alt="Zoom inn" name="Pluss" width="18" height="18" border="0" id="Pluss" /></a>
								<a href="javascript:mapZoomByFactor(2.0)" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('minus','','../img/kart/zoombar_minus_on.gif',1)"><img src="../img/kart/zoombar_minus_off.gif" alt="Zoom out" name="minus" width="18" height="18" border="0" class="kartKnappMarg" id="minus" /></a>
								<a href="javascript:mapOverview()" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('norgeskart','','../img/kart/zoombar_norge_on.gif',1)"><img src="../img/kart/zoombar_norge_off.gif" alt="Hele Norge" name="norgeskart" width="18" height="18" border="0" class="kartKnappMarg" id="norgeskart" /></a>
							</td>
							<td class="buttwidht">
								<input type="radio" name="navType" id="zoomTool" accesskey="1" checked="checked" onclick="mapSelectZoomTool(true)"/>

							</td>
              				<td>
              					<label for="zoomTool">Kartutsnitt</label>
							</td>
							<td class="buttwidht">
								<input type="radio" name="navType" id="panTool" accesskey="2"  onclick="mapSelectPanTool(true)"/>
							</td>
							<td>

								<label for="panTool">Dra/Panorer</label>
							</td>
							<td class="buttwidht">
								<input type="radio" name="navType" id="markTool" accesskey="3" onclick="mapSelectMarkTool(false)"/>
							</td>
							<td>
								<label for="markTool">Marker i kart</label>
							</td>
						</tr>
					</table>
				</div>
<script type="text/javascript" src="/js/map_common.js"></script>
<script type="text/javascript">
    //<![CDATA[
    mapSetBaseUrl('/kart/index.c?ps=5&id=a_171877');
    mapSetBoundingBox(59.966319563019, 59.963022236981, 10.743561901883, 10.733561898117);
    mapSetBoundary(71.3, 57.8, 32.1, 3.7);
    mapSetImageSize(759, 500);
            mapSelectZoomTool(true);
    //]]>
</script>
<div id="map_container" style="width:759px;height:500px">
    <div id="zoom"></div>
    <img src="/kartshare/kart6/71AC5A6004D46C3A.gif" alt="" width="759" height="500" id="map_image" />
</div>
			</div>
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<table border="0" cellpadding="0" cellspacing="0" class="textknapp">
							<tr>
								<td><a href="javascript:mapOriginal()">Nullstill til opprinnelig karttreff</a></td>
							</tr>
						</table>
					</td>
					<td>
						<table border="0" cellpadding="0" cellspacing="0" class="textknapp">
							<tr>
								<td>
									<a href="javascript:;" onclick="MM_openBrWindow('/kart/sendMapMail.c?url=http%3a%2f%2fwww.gulesider.no%2fkart%2findex.c%3fw%3d10.733561898117%26ps%3d5%26n%3d59.966319563019%26s%3d59.963022236981%26e%3d10.743561901883%26id%3da_171877','kartEpost','scrollbars=no,width=330,height=550'); return false;">Send kart pr e-post</a>
								</td>
							</tr>
						</table>
					</td>
					<td>
						<table border="0" cellpadding="0" cellspacing="0" class="textknapp">
							<tr>
								<td>
									<a href="/kart/index.c?w=10.733561898117&v=print&ps=5&n=59.966319563019&s=59.963022236981&e=10.743561901883&id=a_171877">Utskriftsvennlig versjon</a>
								</td>
							</tr>
						</table>
					</td>
					<td>
						<table border="0" cellpadding="0" cellspacing="0" class="textknapp">
							<tr>
								<td>
									<a href="javascript:;" target="_blank" onclick="MM_openBrWindow('../kart/bigMap.c?w=10.733561898117&ps=5&n=59.966319563019&s=59.963022236981&e=10.743561901883&id=a_171877','_blank');return false">Stort kart</a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
<p><a href="http://info.gulesider.no/nor/omkart.html" class="nav" target="pop" onclick="MM_openBrWindow('http://info.gulesider.no/nor/omkart.html','pop','width=460,height=450')">Om kartene</a> | <a href="http://info.gulesider.no/nor/omsymbolene.html" class="nav" target="pop" onclick="MM_openBrWindow('http://info.gulesider.no/nor/omsymbolene.html','pop','width=460,height=300')">Om kartsymboler</a></p>
</div>
<div class="clear"></div>
</div>
<div id="fot-side">
<p>
<a href="../gs/disclaimer.html">&copy;</a>
<a href="http://www.findexa.no/" target="_blank">Findexa AS</a> - Telenors offisielle katalogutgiver |
<a href="../gs/vaerktoy.html">Verkt&oslash;ylinje</a> |
<a href="../gs/annonsere.html">Annonseinfo</a> |
<a target="_blank" href="http://www.findexa.no/karriere/">Ledige stillinger</a> |
<a href="http://tk.laboremus.ws/" target="_blank">Trygg Katalog</a> |
<a href="../gs/kontakt.html">Kontakt oss</a> - tlf 815 44 448
</p>
</div>
</div>
<script language="JavaScript" type="text/javascript">setFocus()</script>
<!-- Start TNSmetrix -->
<script type="text/javascript" src="/jscript/tmv11.js"></script>
<script type="text/javascript">
<!--
getTMqs('http', '', '', 'gulesider_no', 'no', 'ISO-8859-15');
//-->
</script>
<noscript><img src="http://statistik-gallup.net/V11***gulesider_no/no/ISO-8859-15//" alt=""></noscript>
<!-- End TNSMetrix -->
</body>
<!-- InstanceEnd -->
</html>
	);
	return $response;
}
