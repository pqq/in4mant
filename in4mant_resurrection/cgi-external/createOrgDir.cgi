#!/usr/bin/perl
#!C:/perl/bin/Perl.exe
#
# -----------------------------------------------------
# Filename:		createOrgDir.cgi
# Author:		Frode Klevstul (frode@klevstul.com)
# Started:		21.03.06
# -----------------------------------------------------


# -------------------
# use
# -------------------
use strict;


# -------------------
# global declarations
# -------------------
my $query_string = $ENV{QUERY_STRING};
my $referer = $ENV{HTTP_REFERER};
my ($dir, $url) = split('---', $query_string);
my $baseDir;
my $env;

# -------------------
# set correct base path
# -------------------
if ($referer =~ m/uteliv\.no/){
	$baseDir	= '/srv/www/in4mant/img/org';
	$env		= 'PROD';
} elsif ($url =~ m/tryout\//){
	$baseDir = 'C:\Informant\www\img_tryout\org';
	$env		= 'TRYOUT';
} elsif ($url =~ m/owa\//){
	$baseDir = 'C:\Informant\www\img\org';
	$env		= 'TEST';
} else {
	$baseDir = '/srv/www/in4mant/img/org';
	$env		= 'PROD';
}

# -------------------
# main
# -------------------
&createDir;


# -------------------
# sub procedures
# -------------------
sub createDir{
	my $i = 1000;
	my $file;
	my $input;
	my $filetype;

	print "Content-type: text/html\n\n"; 

	# check
	if ($dir eq undef || $url eq undef){
		print "You have to specify a directory and URL as arguments:<br><br>\n\nExample:<br>http://192.168.1.34/cgi-external/createOrgDir.cgi?[directory]---[url]<br>\n";
		die;
	}
	if ($dir !~ m/^\d+$/){
		die "Illegal directory specified: $dir";
	}

	# make directory
	mkdir $baseDir . '/' . $dir;
	if ($env eq 'PROD'){
		system("chmod a+w " . $baseDir . '/' . $dir);
	}

	# print info for debuging
	#print "$env: " . $env;
	#print "$referer: " . $referer;
	#print $baseDir . "/" . $dir;

	# print html code to jump to a page
	print qq(
		<html><head><title>.</title></head>
		<body>
		<META HTTP-EQUIV="Refresh" CONTENT="0; URL=$url">
		<!-- send user to "$url" --><br>
		</body>
	);

}
