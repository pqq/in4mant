#!/usr/bin/perl
#!C:/perl/bin/Perl.exe
#
# -----------------------------------------------------
# Filename:		deleteUnusedFiles.pl
# Author:		Frode Klevstul (frode@klevstul.com)
# Started:		02.08.06
# -----------------------------------------------------


# -------------------
# use
# -------------------
use strict;
use File::Copy;

# -------------------
# global declarations
# -------------------
#my $rootOrgDir			= 'C:\Informant\www\img_tryout\org';
#my $rootOrgDir			= '\\\\192.168.2.102\informant\www\img_tryout\org';
my $rootOrgDir			= '/srv/www/in4mant/img/org';
my $debug				= 0;
my $noFilesBeforeDel	= 100;
my $excludeList			= $rootOrgDir . '/excludeFiles.txt';
my @excludeFilesArray;
my $excludeFiles;
my $toBeDeletedDir		= $rootOrgDir . '/_toBeDeleted';

# -------------------
# main
# -------------------
&loadExcludeList;
if ($#excludeFilesArray > $noFilesBeforeDel) {
	&deleteTheFiles;
}


# -------------------
# sub procedures
# -------------------
sub loadExcludeList{
	my $line;
	my $fileAge = -M $excludeList;
	my $maxAge = .0005;

	# check that excludeList is newly updated
	if ($fileAge > $maxAge) {
		print "\"$excludeList\" is too old ($fileAge > $maxAge). Generate a new list using i4admin!\n";
		
		if ($fileAge > 1.5) {
			$fileAge = int($fileAge+.5);
			print "\"$excludeList\" is $fileAge days old.\n";
		}
		die;
	}

    open(FILE, "<$excludeList") || die "failed to open '$excludeList'";
	while ($line = <FILE>){
		#&debug($line);
		push(@excludeFilesArray, split('---', $line) );
		$excludeFiles .= $line;
	}
	close (FILE);
}


sub deleteTheFiles{
	my $action = $_[0];

	my $thisDir;																					# the name of the current directory when listing out files
	my $file;																						# used when listing out files
	my $thisFilePath;																				# "full" path to current file
	my @files;																						# file array (main)
	my @subFiles;																					# file array (sub)
	my @deleteFiles;																				# array of files to be deleted
	my $input;																						# user input
	my $filename;																					# the name of the file

	# open and read all files and directories
	opendir (D, "$rootOrgDir");
	my @files = grep /\w/, readdir(D);
	closedir(D);

	# go through and fetch all files in the sub directories
	foreach $file (sort @files){
		next unless (-d "$rootOrgDir/$file");
		next if ($toBeDeletedDir =~ m/$file/);
		#&debug("dir: " . $file);
		push(@subFiles, "dir: " . $file);
		opendir (D, "$rootOrgDir/$file");
		push(@subFiles, grep /\w/, readdir(D));
		closedir(D);
	}

	# list out all subfiles
	foreach $file (@subFiles){
		next if (-d "$rootOrgDir/$file");
		if ($file =~ m/^dir:\s(.*)$/){
			$thisDir = $1;
		} else {
			$thisFilePath = $thisDir . "/" . $file;
			#&debug($thisFilePath);
			
			if ($excludeFiles !~ m/$thisFilePath/){
				&debug("delete: " . $thisFilePath);
				if (!$action && !$debug){
					print "delete: " . $thisFilePath . "\n";
				}
				push(@deleteFiles, $rootOrgDir . "/" . $thisFilePath);
			} else {
				&debug("keep: " . $thisFilePath);
			}
		}
	}

	# confirm deletion
	if (!$action && $#deleteFiles > 0){
		print "Sure you want to delete all these files? 'y' to continue... \n";
		chop($input=<STDIN>);
		if ($input ne "y"){
			exit;
		} else {
			&deleteTheFiles(1);
		}
	} elsif ($action && $#deleteFiles > 0){

		foreach $file (@deleteFiles){
			$filename = $file;
			$filename =~ s/^.*\///;
			
			move($file, $toBeDeletedDir."/".$filename) or die "Cannot move file '$file' to '$toBeDeletedDir/$filename'";
			&debug("move($file, $toBeDeletedDir/$filename)");
		}
	} else {
		print "No files to be deleted... \n";
	}

}


sub debug{
	my $txt = $_[0];
	
	if ($debug == 1) {
		print "DEBUG: " . $txt . "\n";
	}
}