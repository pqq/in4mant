
/* Fylkes og kommunelister */
<!--
// Initialize class for Type and Style
var typeArrayInitialized = false;

function Type(id, type) {
	this.id = id;
	this.type = type;
}
function Style(id, id_type, style) {
	this.id = id;
	this.id_type = id_type;
	this.style = style;
}
function init(sel_type, sel_style) {
   if (!typeArrayInitialized) {
      document.start();
   }
   //alert('Sel type = ' + sel_type + 'sel style = ' + sel_style)
   var fylkeArr = document.getElementById("opt_id_type");
   var kommArr = document.getElementById("opt_id_style");
   //if (!fylkeArr) alert('Finner ikke array for fylke');
   //if (!kommArr) alert('Finner ikke array for kommune');
   /* document.advancedSearch.id_type.options[0] = new Option("Fylke"); */
   /* document.advancedSearch.id_style.options[0] = new Option("Kommune"); */
   fylkeArr.options[0] = new Option(" ");
   kommArr.options[0] = new Option(" ");
	for(i = 1; i <= TypeArray.length; i++) {
		/* document.advancedSearch.id_type.options[i] = new Option(TypeArray[i-1].type, TypeArray[i-1].id); */
        fylkeArr.options[i] = new Option(TypeArray[i-1].type, TypeArray[i-1].id);
		if(TypeArray[i-1].id == sel_type) {
         fylkeArr.options[i].selected = true;
        /* document.advancedSearch.id_type.options[i].selected = true; */
      }
	}
    showMunicipal(sel_style);
}
function showMunicipal(sel_style) {
    var fylkeArr = document.getElementById("opt_id_type");
    var kommArr = document.getElementById("opt_id_style");
	sel_type_index = fylkeArr.selectedIndex; /* document.advancedSearch.id_type.selectedIndex; */
	sel_type_value = fylkeArr[sel_type_index].value; /* document.advancedSearch.id_type[sel_type_index].value; */
	/* for(i = document.advancedSearch.id_style.length - 1; i > 0; i--) */
	for(i = kommArr.length - 1; i > 0; i--)
	   kommArr.options[i] = null;
	j=1;
	for(i = 1; i <= StyleArray.length; i++) {
		if(StyleArray[i-1].id_type == sel_type_value) {
		   kommArr.options[j] = new Option(StyleArray[i-1].style, StyleArray[i-1].id);
            if(StyleArray[i-1].id == sel_style) {
                kommArr.options[j].selected = true;
            }
            j++;
		}
	}
}

//window.onload = start;
