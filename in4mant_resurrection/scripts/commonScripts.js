// --------------------------------------------------------
// function that refresh (works within other frameset)
// --------------------------------------------------------
function refresh(frame_name, location){
	var object;

	if ( parent.length > 2 ){
		object = eval ("parent[0].parent." + frame_name);
	} else {
		object = eval ("top." + frame_name); 
	}

	object.location = location;
}

// --------------------------------------------------------
// function to open links with target = "_top" 
// (works within other frameset)
// --------------------------------------------------------
function OpenLink(location){
	if ( parent.length > 2 ){
		parent[0].parent.document.location=location;
	} else {
		this.location=location;
	}
}

// --------------------------------------------------------
// function to highlight background of an element
// --------------------------------------------------------
function highlight(which,color){
	if (document.all||document.getElementById){
		which.style.backgroundColor=color;
	}
}

// --------------------------------------------------------
// function to check if email is valid
// --------------------------------------------------------
function validEmail(emailAddress){
	var x = emailAddress;
	var filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return(filter.test(x));
}

// --------------------------------------------------------
// clock functions (http://www.proxy2.de/scripts.php)
// --------------------------------------------------------
function showFilled(Value) {
  return (Value > 9) ? "" + Value : "0" + Value;
}

function StartClock24() {
	TheTime = new Date;
	var clock = document.getElementById('clock');
	clock.innerHTML = showFilled(TheTime.getDate()) + "/" +
		showFilled(TheTime.getMonth()+1) + "-" + showFilled(TheTime.getFullYear()) 
		+ " - " + showFilled(TheTime.getHours()) + ":" +
		showFilled(TheTime.getMinutes()) + ":" +
		showFilled(TheTime.getSeconds());
 	setTimeout("StartClock24()",1000)
}


// --------------------------------------------------------
// verify time / date functions
// --------------------------------------------------------
function verifyTime(time) {

	// if empty it's OK
	if (time.length == 0){
		return '';
	}

 	// convert input to "."
 	time	= time.replace(':','.');
 	time	= time.replace(',','.');

	// in case "HHMM" format
	reg_hhmm = /^\d{4}/;
	if (time.match(reg_hhmm)){
		tmpHH = time.match(/^\d{2}/);
		tmpMM = time.match(/\d{2}$/);
		time = tmpHH +'.'+ tmpMM;
	}

 	// check if the time is correct
 	reg_time= /^\d{2}\.\d{2}/;
 	if (!(time.match(reg_time))){
 		alert("wrong time format");
		return false;
 	}

	// check if time input is "24:00", then change to "00:00"
	if (time.match(/24\.00/)){
		time = '00.00';
	}

 	hh = time.match(/^\d{2}/g);
 	if (hh > 24 || hh < 0) {
 		alert("error in hours");
		return false;
 	}

 	mm = time.match(/\d{2}$/g);
 	if (mm > 59 || mm < 0) {
 		alert("error in minutes");
		return false;
 	}

	return time;
}

function verifyDate(date) {

	// if empty it's OK
	if (date.length == 0){
		return '';
	}

 	// convert input to "."
 	date	= date.replace(':','.');
 	date	= date.replace(':','.');
 	date	= date.replace(',','.');
 	date	= date.replace(',','.');

	// in case "DDMMYY" format
	reg_hhmm = /^\d{6}/;
	if (date.match(reg_hhmm)){
		tmpDD = date.match(/^\d{2}/);
		tmpMM = date.substring(2,4);
		tmpYY = date.match(/\d{2}$/);
		date = tmpDD +'.'+ tmpMM +'.'+ tmpYY;
	}

 	// check if the date is correct
 	reg_date= /^\d{2}\.\d{2}\.\d{2}/;
 	if (!(date.match(reg_date))){
 		alert("wrong date format");
		return false;
 	}

 	dd = date.match(/^\d{2}/g);
 	if (dd > 31 || dd < 0) {
 		alert("error in days");
		return false;
 	}

 	mmyy = date.match(/\d{4}$/g);
 	if (mmyy > 1299 || mmyy < 0) {
 		alert("error in month");
		return false;
 	}

	return date;
}

// --------------------------------------------------------
// cookies functions
// --------------------------------------------------------
function createCookie(name,value,days)
{
	if (days)
	{
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name)
{
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++)
	{
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name)
{
	createCookie(name,"",-1);
}
