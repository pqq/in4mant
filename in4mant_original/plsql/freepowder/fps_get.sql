set define off
PROMPT *** package: fps_get ***

CREATE OR REPLACE PACKAGE fps_get IS

    FUNCTION value
    	(
			p_name                  in parameter.name%type default NULL
		) RETURN varchar2;

END;
/
show errors;


CREATE OR REPLACE PACKAGE BODY fps_get IS


---------------------------------------------------------
-- Name:        value
-- What:        returns a value
-- Author:      Frode Klevstul
-- Start date:  28.05.2002
---------------------------------------------------------
FUNCTION value
	(
		p_name  in parameter.name%type default NULL
	) RETURN varchar2
IS
BEGIN
DECLARE

        v_value         varchar2(500) default NULL;
        v_server_name   varchar2(500) default NULL;

BEGIN

	SELECT	value
	INTO	v_value
	FROM	parameter
	WHERE	name = p_name;

    RETURN v_value;

EXCEPTION
	WHEN NO_DATA_FOUND THEN
		v_value := 'No parameter with name = '''||p_name||'''';
		RETURN v_value;

END;
END value;



-- ++++++++++++++++++++++++++++++++++++++++++++++ --

END; -- ends package body
/
show errors;

