set define off
PROMPT *** package: fpi_help ***


CREATE OR REPLACE PACKAGE fpi_help IS

	PROCEDURE startup;
	PROCEDURE code_standard;
	PROCEDURE dictionary_views;

END;
/
show errors;


CREATE OR REPLACE PACKAGE BODY fpi_help IS


---------------------------------------------------------
-- Name:        startup
-- What:        starts up this package
-- Author:      Frode Klevstul
-- Start date:  19.04.2002
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

	htp.p('fpi_help');

END startup;


---------------------------------------------------------
-- Name:        code_standard
-- What:        defines the PL/SQL code standard we are using
-- Author:      Frode Klevstul
-- Start date:  18.04.2002
---------------------------------------------------------
PROCEDURE code_standard
IS
BEGIN

	htp.p('
<pre>

<h2>PL/SQL code standard for FreePowder.com</h2>
<b>---------------------------------------------</b>
<b>Author:</b>   Frode Klevstul (frode@klevstul.com)
<b>Revision:</b> 20020418
<b>---------------------------------------------</b>


---------------------------------------------

<h3>PACKAGE NAMES:</h3>
- The first three letters is describing who the package is ment for.
- The [name] should describe the meaning of the package.
- The [name] should only contain letters from [a-z]
- Only lowercase letters.
<b>
fpi_[name] - FreePowder Internal administrators (FP administrators)
fpa_[name] - Administrators                     (Other administrators, like photographers and magazines)
fpu_[name] - Users, normal end users
fps_[name] - FreePowder System packages         (packages doing system operations)
</b>
<b>Example:</b><i>
This package is named "fpi_help".
The "fpi" name is because this package is ment for "internal FreePowder administrators".
The package is named "help" because this package is ment to help us.
In other words, this is a help package for internal FreePowder administrators!
</i>

Give packages good describing names!

---------------------------------------------

<h3>PROCEDURE/FUNCTION NAMES:</h3>
- The name should describe the operation of the procedure or function.
- The name should contain letters from [a-z] and it might contain [_] (underscore)
- Only lowercase letters.
<b>
[name]
</b>
<b>Example:</b><i>
This procedure is named "code_standard" because this procedure writes out this 
text about the code standard on FreePowder.com.
</i>

---------------------------------------------

<h3>PARAMETER AND VARIABLE NAMES:</h3>
- The first letter tells us if it''s a variable or a parameter.
  Parameters starts with "p_", and variables starts with "v_".
- The name should describe what the param/variable is storing.
- The name should contain letters from [a-z] and it might contain [_] (underscore)
- Only lowercase letters.

<b>Example:</b><i>
If we are going to select the column "password" from a table, we stores this
data in a variable called "v_password". If we want to send this data to a function,
the parameter in that function is called "p_password".
</i>

---------------------------------------------

<h3>CURSOR NAMES:</h3>
- Starts with "c_".
- The name should contain letters from [a-z] and it might contain [_] (underscore)
- Only lowercase letters.
<b>
c_[cursorname]
</b>

<b>Example:</b><i>
CURSOR	c_select_menu_type IS
SELECT	mt.menu_type_pk, s.string
FROM	menu_type mt, menu m, string_group sg, groups g, strng s
WHERE	m.organization_fk = p_organization_pk
AND		m.menu_type_fk = mt.menu_type_pk
AND		mt.name_sg_fk = sg.string_group_pk
AND		g.string_group_fk = sg.string_group_pk
AND		g.string_fk = s.string_pk
AND		s.language_fk = v_language_pk
AND		m.activated = 1
ORDER BY s.string;
</i>

---------------------------------------------

<h3>PACKAGE DECLARATIONS:</h3>

<b>Example:</b><i>
set define off
PROMPT *** package: fpi_help ***

CREATE OR REPLACE PACKAGE fpi_help IS
....
END;
show errors;

CREATE OR REPLACE PACKAGE BODY fpi_help IS
....
-- ++++++++++++++++++++++++++++++++++++++++++++++ --
END; -- ends package body
show errors;
</i>

---------------------------------------------

<h3>PROCEDURE/FUNCTION DECLARATIONS:</h3>
- A procedure or a function starts with a comment field,
  who gives us some needful information.
- PL/SQL keywords are in UPPERcase

<b>Example:</b><i>
---------------------------------------------------------
-- Name:        code_standard
-- What:        defines the PL/SQL code standard we are using
-- Author:      Frode Klevstul
-- Start date:  18.04.2002
---------------------------------------------------------
PROCEDURE code_standard
IS
BEGIN
.... 
END code_standard;
</i>

---------------------------------------------

For more detailed information, look at the source of this package: <a href="fpi_admin.desc_package?p_name=fpi_help">source code</a>
and the <a href="fpi_admin.desc_package?p_name=fpi_admin">fpi_admin</a> package.

</pre>
	');

END code_standard;


---------------------------------------------------------
-- Name:        dictionary_views
-- What:        lists out dictinary views
-- Author:      Frode Klevstul
-- Start date:  17.04.2002
---------------------------------------------------------
PROCEDURE dictionary_views
IS
BEGIN

htp.p('

This page outlines the various standard dictionary views. 
Not all of them are available to all users. 
This was generated from a version 7.0.16.4.1 database. 
<br><br>
check also:
<br>
<a href="http://download-west.oracle.com/otndoc/oracle9i/901_doc/nav/catalog_views.htm">Oracle 9i</a><br>
<a href="http://tahiti.oracle.com/pls/tahiti/tahiti.catalog_views">8i (8.1.7)</a>
<br><br>

<DT>ALL_COL_COMMENTS<DD>
Comments on columns of accessible tables and views

<DT>ALL_COL_PRIVS<DD>
Grants on columns for which the user is the grantor, grantee, owner,
or an enabled role or PUBLIC is the grantee

<DT>ALL_COL_PRIVS_MADE<DD>
Grants on columns for which the user is owner or grantor

<DT>ALL_COL_PRIVS_RECD<DD>
Grants on columns for which the user, PUBLIC or enabled role is the grantee

<DT>ALL_CONSTRAINTS<DD>
Constraint definitions on accessible tables

<DT>ALL_CONS_COLUMNS<DD>
Information about accessible columns in constraint definitions

<DT>ALL_DB_LINKS<DD>
Database links accessible to the user

<DT>ALL_DEF_AUDIT_OPTS<DD>
Auditing options for newly created objects

<DT>ALL_DEPENDENCIES<DD>
Dependencies to and from objects accessible to the user

<DT>ALL_ERRORS<DD>
Current errors on stored objects that user is allowed to create

<DT>ALL_INDEXES<DD>
Descriptions of indexes on tables accessible to the user

<DT>ALL_IND_COLUMNS<DD>
COLUMNs comprising INDEXes on accessible TABLES

<DT>ALL_OBJECTS<DD>
Objects accessible to the user

<DT>ALL_SEQUENCES<DD>
Description of SEQUENCEs accessible to the user

<DT>ALL_SNAPSHOTS<DD>
Snapshots the user can look at

<DT>ALL_SOURCE<DD>
Current source on stored objects that user is allowed to create

<DT>ALL_SYNONYMS<DD>
All synonyms accessible to the user

<DT>ALL_TABLES<DD>
Description of tables accessible to the user

<DT>ALL_TAB_COLUMNS<DD>
Columns of all tables, views and clusters

<DT>ALL_TAB_COMMENTS<DD>
Comments on tables and views accessible to the user

<DT>ALL_TAB_PRIVS<DD>
Grants on objects for which the user is the grantor, grantee, owner,
or an enabled role or PUBLIC is the grantee

<DT>ALL_TAB_PRIVS_MADE<DD>
User''s grants and grants on user''s objects

<DT>ALL_TAB_PRIVS_RECD<DD>
Grants on objects for which the user, PUBLIC or enabled role is the grantee

<DT>ALL_TRIGGERS<DD>
Triggers accessible to the current user

<DT>ALL_TRIGGER_COLS<DD>
Column usage in user''s triggers or in triggers on user''s tables

<DT>ALL_USERS<DD>
Information about all users of the database

<DT>ALL_VIEWS<DD>
Text of views accessible to the user

<DT>AUDIT_ACTIONS<DD>
Description table for audit trail action type codes.  Maps action type numbers
 to action type names

<DT>CAT<DD>
Synonym for USER_CATALOG

<DT>CLU<DD>
Synonym for USER_CLUSTERS

<DT>COLS<DD>
Synonym for USER_TAB_COLUMNS

<DT>COLUMN_PRIVILEGES<DD>
Grants on columns for which the user is the grantor, grantee, owner, or
an enabled role or PUBLIC is the grantee

<DT>DBA_2PC_NEIGHBORS<DD>
information about incoming and outgoing connections for pending transactions

<DT>DBA_2PC_PENDING<DD>
info about distributed transactions awaiting recovery

<DT>DBA_ANALYZE_OBJECTS<DD>


<DT>DBA_AUDIT_EXISTS<DD>
Lists audit trail entries produced by AUDIT NOT EXISTS and AUDIT EXISTS

<DT>DBA_AUDIT_OBJECT<DD>
Audit trail records for statements concerning objects, specifically: table, cl
uster, view, index, sequence,  [public] database link, [public] synonym, proce
dure, trigger, rollback segment, tablespace, role, user

<DT>DBA_AUDIT_SESSION<DD>


<DT>DBA_AUDIT_STATEMENT<DD>
Audit trail records concerning  grant, revoke, audit, noaudit and alter system

<DT>DBA_AUDIT_TRAIL<DD>
All audit trail entries

<DT>DBA_CATALOG<DD>
All database Tables, Views, Synonyms, Sequences

<DT>DBA_CLUSTERS<DD>
Description of all clusters in the database

<DT>DBA_CLU_COLUMNS<DD>
Mapping of table columns to cluster columns

<DT>DBA_COL_COMMENTS<DD>
Comments on columns of all tables and views

<DT>DBA_COL_PRIVS<DD>
All grants on columns in the database

<DT>DBA_CONSTRAINTS<DD>
Constraint definitions on all tables

<DT>DBA_CONS_COLUMNS<DD>
Information about accessible columns in constraint definitions

<DT>DBA_DATA_FILES<DD>
Information about database files

<DT>DBA_DB_LINKS<DD>
All database links in the database

<DT>DBA_DEPENDENCIES<DD>
Dependencies to and from objects

<DT>DBA_ERRORS<DD>
Current errors on all stored objects in the database

<DT>DBA_EXP_FILES<DD>
Description of export files

<DT>DBA_EXP_OBJECTS<DD>
Objects that have been incrementally exported

<DT>DBA_EXP_VERSION<DD>
Version number of the last export session

<DT>DBA_EXTENTS<DD>
Extents comprising all segments in the database

<DT>DBA_FREE_SPACE<DD>
Free extents in all tablespaces

<DT>DBA_INDEXES<DD>
Description for all indexes in the database

<DT>DBA_IND_COLUMNS<DD>
COLUMNs comprising INDEXes on all TABLEs and CLUSTERs

<DT>DBA_OBJECTS<DD>
All objects in the database

<DT>DBA_OBJECT_SIZE<DD>
Sizes, in bytes, of various pl/sql objects

<DT>DBA_OBJ_AUDIT_OPTS<DD>
Auditing options for all tables and views

<DT>DBA_PRIV_AUDIT_OPTS<DD>
desc_packages current system privileges being audited across the system and by use
r

<DT>DBA_PROFILES<DD>
Display all profiles and their limits

<DT>DBA_ROLES<DD>
All Roles which exist in the database

<DT>DBA_ROLE_PRIVS<DD>
Roles granted to users and roles

<DT>DBA_ROLLBACK_SEGS<DD>
Description of rollback segments

<DT>DBA_SEGMENTS<DD>
Storage allocated for all database segments

<DT>DBA_SEQUENCES<DD>
Description of all SEQUENCEs in the database

<DT>DBA_SNAPSHOTS<DD>
All snapshots in the database

<DT>DBA_SNAPSHOT_LOGS<DD>
All snapshot logs in the database

<DT>DBA_SOURCE<DD>
Source of all stored objects in the database

<DT>DBA_STMT_AUDIT_OPTS<DD>
desc_packages current system auditing options across the system and by user

<DT>DBA_SYNONYMS<DD>
All synonyms in the database

<DT>DBA_SYS_PRIVS<DD>
System privileges granted to users and roles

<DT>DBA_TABLES<DD>
Description of all tables in the database

<DT>DBA_TABLESPACES<DD>
Description of all tablespaces

<DT>DBA_TAB_COLUMNS<DD>
Columns of all tables, views and clusters

<DT>DBA_TAB_COMMENTS<DD>
Comments on all tables and views in the database

<DT>DBA_TAB_PRIVS<DD>
All grants on objects in the database

<DT>DBA_TRIGGERS<DD>
All triggers in the database

<DT>DBA_TRIGGER_COLS<DD>
Column usage in all triggers

<DT>DBA_TS_QUOTAS<DD>
Tablespace quotas for all users

<DT>DBA_USERS<DD>
Information about all users of the database

<DT>DBA_VIEWS<DD>
Text of all views in the database

<DT>DICT<DD>
Synonym for DICTIONARY

<DT>DICTIONARY<DD>
Description of data dictionary tables and views

<DT>DICT_COLUMNS<DD>
Description of columns in data dictionary tables and views

<DT>DUAL<DD>


<DT>GLOBAL_NAME<DD>
global database name

<DT>IND<DD>
Synonym for USER_INDEXES

<DT>INDEX_HISTOGRAM<DD>
statistics on keys with repeat count

<DT>INDEX_STATS<DD>
statistics on the b-tree

<DT>OBJ<DD>
Synonym for USER_OBJECTS

<DT>RESOURCE_COST<DD>
Cost for each resource

<DT>ROLE_ROLE_PRIVS<DD>
Roles which are granted to roles

<DT>ROLE_SYS_PRIVS<DD>
System privileges granted to roles

<DT>ROLE_TAB_PRIVS<DD>
Table privileges granted to roles

<DT>SEQ<DD>
Synonym for USER_SEQUENCES

<DT>SESSION_PRIVS<DD>
Privileges which the user currently has set

<DT>SESSION_ROLES<DD>
Roles which the user currently has enabled.

<DT>SYN<DD>
Synonym for USER_SYNONYMS

<DT>TABLE_PRIVILEGES<DD>
Grants on objects for which the user is the grantor, grantee, owner,
or an enabled role or PUBLIC is the grantee

<DT>TABS<DD>
Synonym for USER_TABLES

<DT>USER_AUDIT_OBJECT<DD>
Audit trail records for statements concerning objects, specifically: table, cl
uster, view, index, sequence,  [public] database link, [public] synonym, proce
dure, trigger, rollback segment, tablespace, role, user

<DT>USER_AUDIT_SESSION<DD>


<DT>USER_AUDIT_STATEMENT<DD>
Audit trail records concerning  grant, revoke, audit, noaudit and alter system

<DT>USER_AUDIT_TRAIL<DD>
Audit trail entries relevant to the user

<DT>USER_CATALOG<DD>
Tables, Views, Synonyms and Sequences owned by the user

<DT>USER_CLUSTERS<DD>
Descriptions of user''s own clusters

<DT>USER_CLU_COLUMNS<DD>
Mapping of table columns to cluster columns

<DT>USER_COL_COMMENTS<DD>
Comments on columns of user''s tables and views

<DT>USER_COL_PRIVS<DD>
Grants on columns for which the user is the owner, grantor or grantee

<DT>USER_COL_PRIVS_MADE<DD>
All grants on columns of objects owned by the user

<DT>USER_COL_PRIVS_RECD<DD>
Grants on columns for which the user is the grantee

<DT>USER_CONSTRAINTS<DD>
Constraint definitions on user''s own tables

<DT>USER_CONS_COLUMNS<DD>
Information about accessible columns in constraint definitions

<DT>USER_DB_LINKS<DD>
Database links owned by the user

<DT>USER_DEPENDENCIES<DD>
Dependencies to and from a users objects

<DT>USER_ERRORS<DD>
Current errors on stored objects owned by the user

<DT>USER_EXTENTS<DD>
Extents comprising segments owned by the user

<DT>USER_FREE_SPACE<DD>
Free extents in tablespaces accessible to the user

<DT>USER_INDEXES<DD>
Description of the user''s own indexes

<DT>USER_IND_COLUMNS<DD>
COLUMNs comprising user''s INDEXes or on user''s TABLES

<DT>USER_OBJECTS<DD>
Objects owned by the user

<DT>USER_OBJECT_SIZE<DD>
Sizes, in bytes, of various pl/sql objects

<DT>USER_OBJ_AUDIT_OPTS<DD>
Auditing options for user''s own tables and views

<DT>USER_RESOURCE_LIMITS<DD>
Display resource limit of the user

<DT>USER_ROLE_PRIVS<DD>
Roles granted to current user

<DT>USER_SEGMENTS<DD>
Storage allocated for all database segments

<DT>USER_SEQUENCES<DD>
Description of the user''s own SEQUENCEs

<DT>USER_SNAPSHOTS<DD>
Snapshots the user can look at

<DT>USER_SNAPSHOT_LOGS<DD>
All snapshot logs owned by the user

<DT>USER_SOURCE<DD>
Source of stored objects accessible to the user

<DT>USER_SYNONYMS<DD>
The user''s private synonyms

<DT>USER_SYS_PRIVS<DD>
System privileges granted to current user

<DT>USER_TABLES<DD>
Description of the user''s own tables

<DT>USER_TABLESPACES<DD>
Description of accessible tablespaces

<DT>USER_TAB_COLUMNS<DD>
Columns of user''s tables, views and clusters

<DT>USER_TAB_COMMENTS<DD>
Comments on the tables and views owned by the user

<DT>USER_TAB_PRIVS<DD>
Grants on objects for which the user is the owner, grantor or grantee

<DT>USER_TAB_PRIVS_MADE<DD>
All grants on objects owned by the user

<DT>USER_TAB_PRIVS_RECD<DD>
Grants on objects for which the user is the grantee

<DT>USER_TRIGGERS<DD>
Triggers owned by the user

<DT>USER_TRIGGER_COLS<DD>
Column usage in user''s triggers

<DT>USER_TS_QUOTAS<DD>
Tablespace quotas for the user

<DT>USER_USERS<DD>
Information about the current user

<DT>USER_VIEWS<DD>
Text of views owned by the user

<DT>V$ACCESS<DD>
Synonym for V_$ACCESS

<DT>V$ARCHIVE<DD>
Synonym for V_$ARCHIVE

<DT>V$BACKUP<DD>
Synonym for V_$BACKUP

<DT>V$BGPROCESS<DD>
Synonym for V_$BGPROCESS

<DT>V$CIRCUIT<DD>
Synonym for V_$CIRCUIT

<DT>V$CONTROLFILE<DD>
Synonym for V_$CONTROLFILE

<DT>V$DATABASE<DD>
Synonym for V_$DATABASE

<DT>V$DATAFILE<DD>
Synonym for V_$DATAFILE

<DT>V$DBFILE<DD>
Synonym for V_$DBFILE

<DT>V$DBLINK<DD>
Synonym for V_$DBLINK

<DT>V$DB_OBJECT_CACHE<DD>
Synonym for V_$DB_OBJECT_CACHE

<DT>V$DISPATCHER<DD>
Synonym for V_$DISPATCHER

<DT>V$ENABLEDPRIVS<DD>
Synonym for V_$ENABLEDPRIVS

<DT>V$FILESTAT<DD>
Synonym for V_$FILESTAT

<DT>V$FIXED_TABLE<DD>
Synonym for V_$FIXED_TABLE

<DT>V$LATCH<DD>
Synonym for V_$LATCH

<DT>V$LATCHHOLDER<DD>
Synonym for V_$LATCHHOLDER

<DT>V$LATCHNAME<DD>
Synonym for V_$LATCHNAME

<DT>V$LIBRARYCACHE<DD>
Synonym for V_$LIBRARYCACHE

<DT>V$LICENSE<DD>
Synonym for V_$LICENSE

<DT>V$LOADCSTAT<DD>
Synonym for V_$LOADCSTAT

<DT>V$LOADTSTAT<DD>
Synonym for V_$LOADTSTAT

<DT>V$LOCK<DD>
Synonym for V_$LOCK

<DT>V$LOG<DD>
Synonym for V_$LOG

<DT>V$LOGFILE<DD>
Synonym for V_$LOGFILE

<DT>V$LOGHIST<DD>
Synonym for V_$LOGHIST

<DT>V$LOG_HISTORY<DD>
Synonym for V_$LOG_HISTORY

<DT>V$MLS_PARAMETERS<DD>
Synonym for V_$MLS_PARAMETERS

<DT>V$MTS<DD>
Synonym for V_$MTS

<DT>V$NLS_PARAMETERS<DD>
Synonym for V_$NLS_PARAMETERS

<DT>V$OPEN_CURSOR<DD>
Synonym for V_$OPEN_CURSOR

<DT>V$PARAMETER<DD>
Synonym for V_$PARAMETER

<DT>V$PROCESS<DD>
Synonym for V_$PROCESS

<DT>V$QUEUE<DD>
Synonym for V_$QUEUE

<DT>V$RECOVERY_LOG<DD>
Synonym for V_$RECOVERY_LOG

<DT>V$RECOVER_FILE<DD>
Synonym for V_$RECOVER_FILE

<DT>V$REQDIST<DD>
Synonym for V_$REQDIST

<DT>V$RESOURCE<DD>
Synonym for V_$RESOURCE

<DT>V$ROLLNAME<DD>
Synonym for V_$ROLLNAME

<DT>V$ROLLSTAT<DD>
Synonym for V_$ROLLSTAT

<DT>V$ROWCACHE<DD>
Synonym for V_$ROWCACHE

<DT>V$SESSION<DD>
Synonym for V_$SESSION

<DT>V$SESSION_CURSOR_CACHE<DD>
Synonym for V_$SESSION_CURSOR_CACHE

<DT>V$SESSION_EVENT<DD>
Synonym for V_$SESSION_EVENT

<DT>V$SESSION_WAIT<DD>
Synonym for V_$SESSION_WAIT

<DT>V$SESSTAT<DD>
Synonym for V_$SESSTAT

<DT>V$SGA<DD>
Synonym for V_$SGA

<DT>V$SGASTAT<DD>
Synonym for V_$SGASTAT

<DT>V$SHARED_SERVER<DD>
Synonym for V_$SHARED_SERVER

<DT>V$SQLAREA<DD>
Synonym for V_$SQLAREA

<DT>V$STATNAME<DD>
Synonym for V_$STATNAME

<DT>V$SYSSTAT<DD>
Synonym for V_$SYSSTAT

<DT>V$SYSTEM_CURSOR_CACHE<DD>
Synonym for V_$SYSTEM_CURSOR_CACHE

<DT>V$SYSTEM_EVENT<DD>
Synonym for V_$SYSTEM_EVENT

<DT>V$THREAD<DD>
Synonym for V_$THREAD

<DT>V$TIMER<DD>
Synonym for V_$TIMER

<DT>V$TRANSACTION<DD>
Synonym for V_$TRANSACTION

<DT>V$TYPE_SIZE<DD>
Synonym for V_$TYPE_SIZE

<DT>V$VERSION<DD>
Synonym for V_$VERSION

<DT>V$WAITSTAT<DD>
Synonym for V_$WAITSTAT

<DT>V$_LOCK<DD>
Synonym for V_$_LOCK

');

END dictionary_views;


-- ++++++++++++++++++++++++++++++++++++++++++++++ --

END; -- ends package body
/
show errors;

