set define off
PROMPT *** package: fps_htf ***


CREATE OR REPLACE PACKAGE fps_htf IS

	TYPE fp_arr 			is table of varchar2(4000) index by binary_integer;
	empty_array 			fp_arr;

	FUNCTION get_html
		(
			p_name			in html_code.name%type		default NULL,
			p_field_name	in fp_arr					default empty_array,
			p_field_data	in fp_arr					default empty_array
		) RETURN varchar2;
	FUNCTION b_box
		(
			p_title			in varchar2					default '&nbsp;',
			p_width			in varchar2					default '100%'
		) RETURN varchar2;
	FUNCTION e_box
		RETURN varchar2;
	FUNCTION b_table
		(
			p_width			in varchar2					default '100%',
			p_border		in varchar2					default '0'
		) RETURN varchar2;
	FUNCTION e_table
		RETURN varchar2;
	FUNCTION b_page
		(
			p_selected	in number						default NULL,
			p_width		in varchar2						default fps_get.value('page_width'),
			p_reload	in varchar2						default NULL,
			p_jump		in varchar2						default NULL
		) RETURN varchar2;
	FUNCTION e_page
		RETURN varchar2;

END;
/
show errors;


CREATE OR REPLACE PACKAGE BODY fps_htf IS


---------------------------------------------------------
-- Name:        get_html
-- What:        returns code from html_code table
-- Author:      Frode Klevstul
-- Start date:  21.05.2002
---------------------------------------------------------
FUNCTION get_html
	(
		p_name			in html_code.name%type		default NULL,
		p_field_name	in fp_arr					default empty_array,
		p_field_data	in fp_arr					default empty_array
	) RETURN varchar2
IS
BEGIN
DECLARE

	v_code		varchar2(5000)						default NULL;
	v_data		varchar2(5000)						default NULL;

BEGIN

	SELECT	value
	INTO	v_code
	FROM	html_code
	WHERE	name = p_name;

	if (p_field_name.last > p_field_name.first) then

		FOR i IN p_field_name.first..p_field_name.last LOOP
			owa_pattern.change(v_code, '\['||p_field_name(i)||'\]', p_field_data(i) , 'g');
		END LOOP;

	end if;

	return v_code;

EXCEPTION
	WHEN NO_DATA_FOUND THEN
		RETURN 'No entry in html_code with p_name = '''||p_name||'''';

END;
END get_html;




---------------------------------------------------------
-- Name:        b_box
-- What:        returns b_box code
-- Author:      Frode Klevstul
-- Start date:  21.05.2002
---------------------------------------------------------
FUNCTION b_box
	(
		p_title			in varchar2					default '&nbsp;',
		p_width			in varchar2					default '100%'
	) RETURN varchar2
IS
BEGIN
DECLARE

	v_code			varchar2(5000)			default NULL;
	v_field_name	fp_arr					default empty_array;
	v_field_data	fp_arr					default empty_array;

BEGIN

	v_field_name(1) := 'title';
	v_field_data(1) := p_title;

	v_field_name(2) := 'width';
	v_field_data(2) := p_width;

	v_field_name(3) := 'c_1';
	v_field_data(3) := '#ffffff';

	v_field_name(4) := 'c_2';
	v_field_data(4) := '#ffaa00';

	v_code := get_html('b_box', v_field_name, v_field_data);

	return v_code;

END;
END b_box;





---------------------------------------------------------
-- Name:		e_box
-- What:		returns e_box code
-- Author:		Frode Klevstul
-- Start date:	21.05.2002
---------------------------------------------------------
FUNCTION e_box
	RETURN varchar2
IS
BEGIN

	return get_html('e_box');

END e_box;






---------------------------------------------------------
-- Name:        b_table
-- What:        returns b_table code
-- Author:      Frode Klevstul
-- Start date:  28.05.2002
---------------------------------------------------------
FUNCTION b_table
	(
		p_width			in varchar2					default '100%',
		p_border		in varchar2					default '0'
	) RETURN varchar2
IS
BEGIN
DECLARE

	v_code			varchar2(5000)			default NULL;
	v_field_name	fp_arr		default empty_array;
	v_field_data	fp_arr		default empty_array;

BEGIN

	v_field_name(1) := 'width';
	v_field_data(1) := p_width;

	v_field_name(2) := 'border';
	v_field_data(2) := p_border;

	v_code := get_html('b_table', v_field_name, v_field_data);

	return v_code;

END;
END b_table;



---------------------------------------------------------
-- Name:		e_table
-- What:		returns e_table code
-- Author:		Frode Klevstul
-- Start date:	28.05.2002
---------------------------------------------------------
FUNCTION e_table
	RETURN varchar2
IS
BEGIN

	return get_html('e_table');

END e_table;




---------------------------------------------------------
-- Name:		b_page
-- What:		starts a page
-- Author:		Frode Klevstul
-- Start date:	28.05.2002
---------------------------------------------------------
FUNCTION b_page
	(
		p_selected	in number					default NULL,
		p_width		in varchar2					default fps_get.value('page_width'),
		p_reload	in varchar2					default NULL,
		p_jump		in varchar2					default NULL
	) RETURN varchar2
IS
BEGIN
DECLARE

	v_code			varchar2(5000)			default NULL;
	v_field_name	fp_arr					default empty_array;
	v_field_data	fp_arr					default empty_array;

v_tmp varchar2(4000) default NULL;
BEGIN

	v_field_name(1) := 'width';
--	v_field_data(1) := p_width;
v_tmp := p_width;
v_field_data(1) := v_tmp;
--	v_field_data(1) := '750';

	if ( p_reload is not null ) then
		v_field_name(2) := 'reload';
		v_field_data(2) := '<META HTTP-EQUIV="REFRESH" CONTENT="'||p_reload||'">';
	else
		v_field_name(2) := 'reload';
		v_field_data(2) := '';
	end if;

	if ( p_jump is not null ) then
		v_field_name(3) := 'jump';
		v_field_data(3) := '<META HTTP-EQUIV="REFRESH" Content="'||p_reload||';URL='||p_jump||'">';
	else
		v_field_name(3) := 'jump';
		v_field_data(3) := '';
	end if;

	v_field_name(4) := 'icon_dir';
	v_field_data(4) := fps_get.value('icon_dir');

	v_field_name(5) := 'css';
	v_field_data(5) := get_html('css');

	v_field_name(6) := 'javascript';
	v_field_data(6) := get_html('javascript');

	v_field_name(7) := 'c_bodybackground';
	v_field_data(7) := fps_get.value('c_bodybackground');

	v_field_name(8) := 'c_centerbackground';
	v_field_data(8) := fps_get.value('c_centerbackground');

	v_code := get_html('b_page', v_field_name, v_field_data);

-- hente ut html kode for meny her, og slenge p� slutten av v_code

	return v_code;

END;
END b_page;



---------------------------------------------------------
-- Name:		e_page
-- What:		ends b_page
-- Author:		Frode Klevstul
-- Start date:	28.05.2002
---------------------------------------------------------
FUNCTION e_page
	RETURN varchar2
IS
BEGIN

	return get_html('e_page');

END e_page;



-- ++++++++++++++++++++++++++++++++++++++++++++++ --

END; -- ends package body
/
show errors;

