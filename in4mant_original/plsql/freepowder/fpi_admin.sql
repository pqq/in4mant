set define off
PROMPT *** package: fpi_admin ***


CREATE OR REPLACE PACKAGE fpi_admin IS

	type columns_arr 		is table of varchar2(4000) index by binary_integer;
	empty_array 			columns_arr;

	procedure startup;
	procedure frames;
	procedure menu;
	procedure list_packages;
	procedure desc_package
		(
			p_name			in varchar2			default 'fpi_admin'
		);
	procedure list_tables;
	procedure list_columns
		(
			p_table_name	in varchar2			default null
		);
	procedure list_sequences;
	procedure admin_table
		(
			p_table_name	in varchar2				default null,
			p_action		in varchar2				default null,
			p_column_name	in columns_arr			default empty_array,
			p_column_data	in columns_arr			default empty_array,
			p_sql_stmt		in varchar2				default null
		);

END;
/
show errors;



CREATE OR REPLACE PACKAGE BODY fpi_admin IS

---------------------------------------------------------
-- Name:        startup
-- What:        starts up this package
-- Author:      Frode Klevstul
-- Start date:  17.04.2002
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

	frames;

END startup;


---------------------------------------------------------
-- Name:        frames
-- What:        shows this page in a framesset
-- Author:      Frode Klevstul
-- Start date:  18.04.2002
---------------------------------------------------------
PROCEDURE frames
IS
BEGIN

	htp.p('
		<html>
		<head>
			<title>FreePowder.com</title>
		</head>

		<frameset cols="150,*" border="0" frameborder="0">
			<frame name="left" src="fpi_admin.menu" scrolling="auto" noresize>
			<frame name="main" src="http://freepowder.com" scrolling="auto" noresize>
		</frameset>

		</html>
		');

END frames;


---------------------------------------------------------
-- Name:        menu
-- What:        writes out the menu
-- Author:      Frode Klevstul
-- Start date:  18.04.2002
---------------------------------------------------------
PROCEDURE menu
IS
BEGIN

	htp.p('
		<font size="1" face="arial,helvetica">

		<b>DOCUMENTATION:</b><br>
		<a href="fpi_help.code_standard" target="main">code_standard</a><br>
		<a href="fpi_help.dictionary_views" target="main">dictionary_views</a><br>

		<br><b>DB CONTENT:</b><br>
		<a href="fpi_admin.list_packages" target="main">packages</a><br>
		<a href="fpi_admin.list_sequences" target="main">sequences</a><br>
		<a href="fpi_admin.list_tables" target="main">tables</a><br>

		<br><b>ADMIN TABLES:</b><br>
		
		<b>SYSTEM:</b><br>
		<a href="fpi_admin.admin_table?p_table_name=parameter_type" target="main">parameter_type</a><br>
		<a href="fpi_admin.admin_table?p_table_name=parameter" target="main">parameter</a><br>
		<a href="fpi_admin.admin_table?p_table_name=html_type" target="main">html_type</a><br>
		<a href="fpi_admin.admin_table?p_table_name=html_code" target="main">html_code</a><br>
		<hr noshade>

		<b>MAIL_LIST:</b><br>
		<a href="fpi_admin.admin_table?p_table_name=mail_list" target="main">mail_list</a><br>
		<hr noshade>

		<b>NEWS:</b><br>
		<a href="fpi_admin.admin_table?p_table_name=news" target="main">news</a><br>
		<a href="fpi_admin.admin_table?p_table_name=note" target="main">note</a><br>
		<hr noshade>

		<b>LOCATION/GEOGRAPHY:</b><br>
		<a href="fpi_admin.admin_table?p_table_name=country" target="main">country</a><br>
		<a href="fpi_admin.admin_table?p_table_name=spot" target="main">spot</a><br>
		<a href="fpi_admin.admin_table?p_table_name=spot_activity" target="main">spot_activity</a><br>
		<a href="fpi_admin.admin_table?p_table_name=location" target="main">location</a><br>
		<hr noshade>

		<b>ACTIVITY/ELEMENT:</b><br>
		<a href="fpi_admin.admin_table?p_table_name=element" target="main">element</a><br>
		<a href="fpi_admin.admin_table?p_table_name=activity" target="main">activity</a><br>
		<a href="fpi_admin.admin_table?p_table_name=activity_l1" target="main">activity_l1</a><br>
		<a href="fpi_admin.admin_table?p_table_name=activity_l2" target="main">activity_l2</a><br>
		<a href="fpi_admin.admin_table?p_table_name=activity_l3" target="main">activity_l3</a><br>
		<a href="fpi_admin.admin_table?p_table_name=activity_element" target="main">activity_element</a><br>
		<hr noshade>

		<b>PROFILE MODULE:</b><br>
		<a href="fpi_admin.admin_table?p_table_name=profile" target="main">profile</a><br>
		<a href="fpi_admin.admin_table?p_table_name=profile_activity" target="main">profile_activity</a><br>
		<hr noshade>

<!--
		<b>company module:</b><br>
		<a href="fpi_admin.admin_table?p_table_name=company_type" target="main">company_type</a><br>
		<a href="fpi_admin.admin_table?p_table_name=medium_type" target="main">medium_type</a><br>
		<a href="fpi_admin.admin_table?p_table_name=medium" target="main">medium</a><br>

		<b>usr module:</b><br>
		<a href="fpi_admin.admin_table?p_table_name=user_type" target="main">user_type</a><br>
		<a href="fpi_admin.admin_table?p_table_name=module" target="main">module</a><br>

		<b>picture module:</b><br>
		<a href="fpi_admin.admin_table?p_table_name=format" target="main">format</a><br>
		<a href="fpi_admin.admin_table?p_table_name=format_delivery" target="main">format_delivery</a><br>


		<b>auction module:</b><br>
		<a href="fpi_admin.admin_table?p_table_name=auction_type" target="main">auction_type</a><br>
		<a href="fpi_admin.admin_table?p_table_name=payment_terms" target="main">payment_terms</a><br>
		<a href="fpi_admin.admin_table?p_table_name=delivery_terms" target="main">delivery_terms</a><br>
		<a href="fpi_admin.admin_table?p_table_name=currency" target="main">currency</a><br>

		<b>camera module:</b><br>
		<a href="fpi_admin.admin_table?p_table_name=lens" target="main">lens</a><br>
		<a href="fpi_admin.admin_table?p_table_name=shutter" target="main">shutter</a><br>
		<a href="fpi_admin.admin_table?p_table_name=focal_length" target="main">focal_length</a><br>
		<a href="fpi_admin.admin_table?p_table_name=camera_lens" target="main">camera_lens</a><br>
		<a href="fpi_admin.admin_table?p_table_name=filter" target="main">filter</a><br>
		<a href="fpi_admin.admin_table?p_table_name=camera" target="main">camera</a><br>
		<a href="fpi_admin.admin_table?p_table_name=film" target="main">film</a><br>
		<a href="fpi_admin.admin_table?p_table_name=manufacturer" target="main">manufacturer</a><br>
		<a href="fpi_admin.admin_table?p_table_name=film_type" target="main">film_type</a><br>
-->

		</font>
	');

END menu;



---------------------------------------------------------
-- Name:        list_packages
-- What:        lists out all packages for this db user
-- Author:      Frode Klevstul
-- Start date:  17.04.2002
---------------------------------------------------------
PROCEDURE list_packages
IS
BEGIN
DECLARE

	cursor	SELECT_ALL is
	select	distinct(NAME)
	from	USER_SOURCE
	order 	by NAME ASC;

	v_name	user_source.name%type		default null;

BEGIN

	htp.p('<table>');
	open select_all;
	loop
		fetch select_all into v_name;
		exit when select_all%NOTFOUND;
		htp.p('<tr><td><a href="fpi_admin.desc_package?p_name='||v_name||'">'||v_name||'</a></td></tr>');
	end loop;
	close select_all;
	htp.p('</table>');

END;
END list_packages;


---------------------------------------------------------
-- Name:        desc_package
-- What:        writes out the code for a specified package
-- Author:      Frode Klevstul
-- Start date:  17.04.2002
---------------------------------------------------------
PROCEDURE desc_package
	(
		p_name			in varchar2			default 'fpi_admin'
	)
IS
BEGIN
DECLARE

	cursor	select_all is
	select	line, text
	from	user_source
	where	name = upper(p_name);

	v_line	user_source.line%type		default null;
	v_text	user_source.text%type		default null;
	v_text2	user_source.text%type		default null;

BEGIN

	htp.p('<pre>');
	open select_all;
	loop
		fetch select_all into v_line, v_text;
		exit when select_all%NOTFOUND;
		owa_pattern.change(v_text, '<', '\&lt;', 'g');
		owa_pattern.change(v_text, '>', '\&gt;', 'g');
		owa_pattern.change(v_text, '\n', '', 'g');
		htp.p(v_line||':  '||v_text);
	end loop;
	close select_all;
	htp.p('</pre>');

END;
END desc_package;



---------------------------------------------------------
-- Name:        list_tables
-- What:        lists out all tables for this db user
-- Author:      Frode Klevstul
-- Start date:  17.04.2002
---------------------------------------------------------
PROCEDURE list_tables
IS
BEGIN
DECLARE

	cursor	select_all is
	select	distinct(table_name)
	from	user_tables
	order by table_name asc;

	v_table_name	user_tables.table_name%type		default null;

BEGIN

	htp.p('<table>');
	open select_all;
	loop
		fetch select_all into v_table_name;
		exit when select_all%NOTFOUND;
		htp.p('<tr><td><a href="fpi_admin.list_columns?p_table_name='||v_table_name||'">'||v_table_name||'</a></td></tr>');
	end loop;
	close select_all;
	htp.p('</table>');

END;
END list_tables;



---------------------------------------------------------
-- Name:        list_columns
-- What:        lists out all columns for a table
-- Author:      Frode Klevstul
-- Start date:  17.04.2002
---------------------------------------------------------
PROCEDURE list_columns
	(
		p_table_name	in varchar2			default NULL
	)
IS
BEGIN
DECLARE

	cursor	select_all is
	select	column_name, data_type, data_length
	from	user_tab_columns
	where	table_name = p_table_name
	order by column_id asc;

	v_column_name	user_tab_columns.column_name%type		default null;
	v_data_type		user_tab_columns.data_type%type			default null;
	v_data_length	user_tab_columns.data_length%type		default null;

BEGIN

	htp.p('<table>');
	open select_all;
	loop
		fetch select_all into v_column_name, v_data_type, v_data_length;
		exit when select_all%NOTFOUND;
		htp.p('<tr><td>'||v_column_name||'</td><td>&nbsp;&nbsp;&nbsp;'||v_data_type||'('||v_data_length||')</td></tr>');
	end loop;
	close select_all;
	htp.p('</table>');

END;
END list_columns;




---------------------------------------------------------
-- Name:        list_sequences
-- What:        lists out all tables for this db user
-- Author:      Frode Klevstul
-- Start date:  24.04.2002
---------------------------------------------------------
PROCEDURE list_sequences
IS
BEGIN
DECLARE

	cursor	select_all is
	select	sequence_name, min_value, increment_by, last_number
	from	user_sequences
	order by sequence_name asc;

	v_sequence_name	user_sequences.sequence_name%type	default null;
	v_min_value		user_sequences.min_value%type		default null;
	v_max_value		user_sequences.max_value%type		default null;
	v_increment_by	user_sequences.increment_by%type	default null;
	v_last_number	user_sequences.last_number%type		default null;

BEGIN

	htp.p('<table border="1">');
	htp.p('<tr><td><b>seqence name</b></td><td><b>min value</b></td><td><b>increment by</b></td><td><b>last_number</b></td></tr>');
	open select_all;
	loop
		fetch select_all into v_sequence_name, v_min_value, v_increment_by, v_last_number;
		exit when select_all%NOTFOUND;
		htp.p('<tr><td>'||v_sequence_name||'</td><td>'||v_min_value||'</td><td>'||v_increment_by||'</td><td>'||v_last_number||'</td></tr>');
	end loop;
	close select_all;
	htp.p('</table>');

END;
END list_sequences;




---------------------------------------------------------
-- Name:        admin_table
-- What:        insert, update, select from a table
-- Author:      Frode Klevstul
-- Start date:  19.04.2002
---------------------------------------------------------
PROCEDURE admin_table
	(
		p_table_name	in varchar2				default NULL,
		p_action		in varchar2				default NULL,
		p_column_name	in columns_arr			default empty_array,
		p_column_data	in columns_arr			default empty_array,
		p_sql_stmt		in varchar2				default NULL
	)
IS
BEGIN
DECLARE

	v_action		varchar2(20)							default 'list';

	cursor	select_columns is
	select	column_name, data_type, data_length
	from	user_tab_columns
	where	table_name = upper(p_table_name)
	order by column_id asc;

	v_column_name		user_tab_columns.column_name%type		default null;
	v_column_data		varchar2(4500)							default null;
	v_data_type			user_tab_columns.data_type%type			default null;
	v_data_length		user_tab_columns.data_length%type		default null;
	v_count				number									default null;
	v_parent_table		user_constraints.r_constraint_name%type	default null;
	v_submit			boolean									default true;
	v_sql_stmt			varchar2(32000)							default null;
	v_array				columns_arr								default empty_array;
	v_pk_array			columns_arr								default empty_array;
	v_pk				number									default null;
	v_i					number									default 0;
	v_name				varchar2(200)							default null;
	v_where_stmt		varchar2(200)							default null;
	v_delete_stmt		varchar2(500)							default null;
	v_delete_stmt_org	varchar2(500)							default null;
	v_pk_in_arr			integer									default null;
	v_text				varchar2(10000)							default null;
	v_tmp				varchar2(50)							default null;
	v_hidden_fields		varchar2(1000)							default null;

	type cur_typ is ref cursor;
	c_dyn_cursor	cur_typ;
	
BEGIN

	if (p_action is not null) then
		v_action := p_action;
	end if;

	htp.p('
		<table border="1" width="800">
		<tr><td colspan="3" align="center"><b>'||p_table_name||'</b></td></tr>
		<tr><td colspan="3" align="center">action = '||v_action||'</b></td></tr>
		<tr><td colspan="3">&nbsp;</td></tr>
		<tr><td colspan="3" align="center">
			<a href="fpi_admin.admin_table?p_table_name='||p_table_name||'&p_action=list">list</a> |
			<a href="fpi_admin.admin_table?p_table_name='||p_table_name||'&p_action=insert">insert</a>
		</td></tr>
	');

	-- ----------------------------------------
	-- insert/update: writes out insert form
	-- ----------------------------------------
	if( owa_pattern.match(v_action, '^insert$') or owa_pattern.match(v_action, '^update$') ) then

		htp.p('
			<form action="fpi_admin.admin_table" method="post">
			<input type="hidden" name="p_action" value="'||v_action||'_action">
			<input type="hidden" name="p_table_name" value="'||p_table_name||'">
		');
		open select_columns;
		loop
			fetch select_columns into v_column_name, v_data_type, v_data_length;
			exit when select_columns%NOTFOUND;
			
			-- ------------------------
			-- fremmedn�kkel
			-- ------------------------
			if ( owa_pattern.match(v_column_name, '_fk$', 'i') and owa_pattern.match(v_data_type, 'number', 'i') ) then

				htp.p('
					<input type="hidden" name="p_column_name" value="'||v_column_name||'">
					<tr>
					<td>'||v_column_name||':</td>
				');

				-- henter ut navnet p� "parent" tabellen
				v_tmp := v_column_name;
				owa_pattern.change(v_tmp, '_PK' , '', 'g');
				owa_pattern.change(v_tmp, '_FK' , '', 'g');

				select	r_constraint_name
				into	v_parent_table
				from	user_constraints
				where	table_name = upper(p_table_name)
				and		constraint_type = 'R'
				and		upper(r_constraint_name) like '%'||upper(v_tmp)||'%';				-- foreign keys

				owa_pattern.change(v_parent_table, 'PK_' , '', 'g');

				-- sjekker om "parent" tabellen har forekomster
				execute immediate 'select count(*) from '||v_parent_table into v_count;

				htp.p('<td>');

				if ( v_count=0 ) then
					htp.p('<font color="#ff0000">error: parent table '''||v_parent_table||''' is empty</font>');
					v_submit := false;
				else
						-- sjekker om foreldre tabellen har et felt "NAME", henter event. ut denn informasjonen
						select	count(*)
						into	v_count
						from	user_tab_columns
						where	table_name = upper(v_parent_table)
						and		column_name = 'NAME';

						if (v_count=1) then
							v_sql_stmt := 'select '||v_parent_table||'_pk, name'|| ' from '||v_parent_table;
						else
							v_sql_stmt := 'select '||v_parent_table||'_pk'|| ' from '||v_parent_table;
						end if;

						-- g�r igjennom foreldretabellens forekomster og skriver ut en "select-boks"
						htp.p('
							<select name="p_column_data">
							<option value="">null</option>
						');
						open c_dyn_cursor for v_sql_stmt;
						loop
							if (v_count = 0) then
								fetch c_dyn_cursor into v_array(1);
							else
								fetch c_dyn_cursor into v_array(1), v_array(2);
							end if;
							exit when c_dyn_cursor%NOTFOUND;

							if (p_action = 'update') then
								if (v_array(1) = p_column_data(select_columns%ROWCOUNT)) then
									if ( owa_pattern.match(v_column_name, '_pk_fk$', 'i') ) then
										-- sender med alle pk_fk n�kler som egne hidden variable, slik at vi vet hva de var n�r vi skal gj�re update (for � kunne bygge riktig WHERE statement)
										htp.p('<option value="'||v_array(1)||'" selected>');
										v_hidden_fields := v_hidden_fields ||' <input type="hidden" name="p_column_name" value="'||v_column_name||'"><input type="hidden" name="p_column_data" value="[PRIMARY_KEY:'||v_array(1)||']">';
									else
										htp.p('<option value="'||v_array(1)||'" selected>');
									end if;
								else
									htp.p('<option value="'||v_array(1)||'">');
								end if;
							else
								htp.p('<option value="'||v_array(1)||'">');							
							end if;

							for i in v_array.first..v_array.last loop
								htp.p(v_array(i)||': ');
							end loop;
							htp.p( '</option>' );
						end loop;
						close c_dyn_cursor;
						htp.p( '</select>' );
				end if;

				htp.p('</td><td>'||v_data_type||'('||v_data_length||')</td></tr>');

			-- ------------------------
			-- prim�rn�kkel
			-- ------------------------
			elsif ( owa_pattern.match(v_column_name, '_pk$', 'i') and owa_pattern.match(v_data_type, 'number', 'i') ) then

				htp.p('<input type="hidden" name="p_column_name" value="'||v_column_name||'">');

				select	count(*)
				into	v_count
				from	user_sequences
				where	sequence_name = upper(p_table_name) ||'_seq';
				
				if (v_count = 1) then
					htp.p('<tr><td>'||v_column_name||':</td>');
					if (p_action = 'update') then
						htp.p('
							<td>'||p_column_data(select_columns%ROWCOUNT)||'</td>
							<input type="hidden" name="p_column_data" value="[PRIMARY_KEY:'||p_column_data(select_columns%ROWCOUNT)||']">
						');
					else
						htp.p('
							<td>'||p_table_name||'_seq.nextval</td>
							<input type="hidden" name="p_column_data" value="[PRIMARY_KEY]">
						');
					end if;
					htp.p('<td>'||v_data_type||'('||v_data_length||')</td></tr>');
				else
					htp.p('
						<tr>
							<td>'||v_column_name||':</td>
							<td>
								<font color="#ff0000">'||p_table_name||'_seq missing</font>
								<a href="fpi_admin.admin_table?p_action=create_sequence&p_table_name='||p_table_name||'&p_sql_stmt=create sequence '||p_table_name||'_seq increment by 1 start with 1">create sequence SQL</a>
							</td>
							<td>'||v_data_type||'('||v_data_length||')</td>
						</tr>
					');
					v_submit := false;
				end if;

			-- ------------------------
			-- varchar2
			-- ------------------------
			elsif ( owa_pattern.match(v_data_type, 'varchar2', 'i') ) then

				htp.p('<input type="hidden" name="p_column_name" value="'||v_column_name||'">');

				if ( v_data_length > 200 ) then
					htp.p('
						<tr>
							<td>'||v_column_name||':</td>
							<td>
								<textarea name="p_column_data" cols="50" rows="10" wrap="off">');
					if (p_action = 'update') then
						v_text := p_column_data(select_columns%ROWCOUNT);
						owa_pattern.change(v_text, '<', '\&lt;', 'g');
						owa_pattern.change(v_text, '>', '\&gt;', 'g');
						owa_pattern.change(v_text, '"', '\&quot;', 'g');
						htp.p( v_text ||'</textarea>');
					else
						htp.p('</textarea>');
					end if;
					htp.p('
							</td>
							<td>'||v_data_type||'('||v_data_length||')</td>
						</tr>
					');
				else
					htp.p('
						<tr>
							<td>'||v_column_name||':</td>
							<td>
					');
					if (p_action = 'update') then
						htp.p('<input type="text" name="p_column_data" size="50" maxlength="'||v_data_length||'" value="'||p_column_data(select_columns%ROWCOUNT)||'">');
					else
						htp.p('<input type="text" name="p_column_data" size="50" maxlength="'||v_data_length||'">');
					end if;
					htp.p('
							</td>
							<td>'||v_data_type||'('||v_data_length||')</td>
						</tr>
					');
				end if;

			-- ------------------------
			-- date
			-- ------------------------
			elsif ( owa_pattern.match(v_data_type, 'date', 'i') ) then

				htp.p('
					<input type="hidden" name="p_column_name" value="'||v_column_name||'">
					<tr>
						<td>'||v_column_name||':</td>
						<td>
				');
				if (p_action = 'update') then
					htp.p('<input type="text" name="p_column_data" size="14" maxlength="14" value="'||to_char(SYSDATE,'DD/MM YY HH24:MI')||'">');
				else
					htp.p('<input type="text" name="p_column_data" size="14" maxlength="14" value="'||to_char(SYSDATE,'DD/MM YY HH24:MI')||'">');
				end if;
				htp.p('
						</td>
						<td>'||v_data_type||'('||v_data_length||')</td>
					</tr>
				');

			-- ------------------------
			-- number
			-- ------------------------
			elsif ( owa_pattern.match(v_data_type, 'number', 'i') ) then

				htp.p('
					<input type="hidden" name="p_column_name" value="'||v_column_name||'">
					<tr>
						<td>'||v_column_name||':</td>
						<td>
				');
				if (p_action = 'update') then
					htp.p('<input type="text" name="p_column_data" size="50" maxlength="'||v_data_length||'" value="'||p_column_data(select_columns%ROWCOUNT)||'">');
				else
					htp.p('<input type="text" name="p_column_data" size="50" maxlength="'||v_data_length||'">');
				end if;
				htp.p('
						</td>
						<td>'||v_data_type||'('||v_data_length||')</td>
					</tr>
				');

			else
				htp.p('<tr><td><font color="#ff0000">error: datatype not supported</font> '||v_column_name||'</td><td>&nbsp;&nbsp;&nbsp;'||v_data_type||'('||v_data_length||')</td><td>'||v_data_type||'('||v_data_length||')</td></tr>');
				v_submit := FALSE;
			end if;
	
		end loop;
		close select_columns;

		htp.p( v_hidden_fields );		-- skriver ut hidden felter fra event. pk_fk felter
		htp.p('
			<tr><td colspan="3">&nbsp;</td></tr>
			<tr><td align="right" colspan="3">
		');
		
		if (v_submit) then
			htp.p('<input type="submit" value="submit">');
		else
			htp.p('<font color="#ff0000">error</font>');
		end if;


	-- ----------------------------------------
	-- insert_action: legger inn i databasen
	-- ----------------------------------------
	elsif( owa_pattern.match(v_action, '^insert_action$') ) then

		-- henter ut og legger alle kolonnenes datatyper i et array
		open select_columns;
		loop
			fetch select_columns into v_column_name, v_data_type, v_data_length;
			exit when select_columns%NOTFOUND;
			v_array(select_columns%ROWCOUNT) := v_data_type;
		end loop;
		close select_columns;

		-- bygger opp SQL INSERT uttrykket
		v_sql_stmt := 'INSERT INTO '||p_table_name||' VALUES(';
		for i in p_column_data.first..p_column_data.last loop

			v_column_data := p_column_data(i);								-- legger inn i en variabel for � kunne endre innhold
			
			if ( owa_pattern.match(p_column_data(i),'\[PRIMARY\_KEY\]') ) then
				execute immediate 'select '||p_table_name||'_seq.nextval from dual' into v_pk;
				v_sql_stmt := v_sql_stmt ||''||v_pk||',';
			else
				if ( owa_pattern.match(v_array(i), 'VARCHAR2') ) then
					owa_pattern.change(v_column_data, '''', '''''');		-- dersom teksten inneholder ' tegn
					v_sql_stmt := v_sql_stmt ||''''|| v_column_data ||''',';
				elsif ( owa_pattern.match(v_array(i), 'NUMBER') ) then
					owa_pattern.change(v_column_data, ',', '\.');			-- dersom "teksten" inneholder ',' tegn
					if ( p_column_data(i) is null) then
						v_sql_stmt := v_sql_stmt ||'NULL,';
					else
						v_sql_stmt := v_sql_stmt ||''|| v_column_data ||',';
					end if;
				elsif ( owa_pattern.match(v_array(i), 'DATE') ) then
					v_name := p_column_data(i);
					owa_pattern.change(v_name, '\/', '');
					owa_pattern.change(v_name, ':', '');
					owa_pattern.change(v_name, ' ', '', 'g');
					if ( p_column_data(i) is null) then
						v_sql_stmt := v_sql_stmt ||'NULL,';
					else
						v_sql_stmt := v_sql_stmt ||' to_date('''|| v_name ||''',''DDMMYYHH24MI''),';
					end if;
				else
					raise_application_error(-20000, 'Datatype not supported in insert procedure');
				end if;
			end if;

		end loop;
		owa_pattern.change(v_sql_stmt, ',$', ')');

		execute immediate v_sql_stmt;

		owa_pattern.change(v_sql_stmt,'<','\&#060;', 'g');
		owa_pattern.change(v_sql_stmt,'>','\&#062;', 'g');
		owa_pattern.change(v_sql_stmt, '"', '\&quot;', 'g');
		
		htp.p('<tr><td>'||v_sql_stmt||'</td></tr>');

	-- ----------------------------------------
	-- list: lists out all entries
	-- ----------------------------------------
	elsif( owa_pattern.match(v_action, '^list$')) then

		open select_columns;
		loop
			fetch select_columns into v_column_name, v_data_type, v_data_length;
			exit when select_columns%NOTFOUND;
			v_array(select_columns%ROWCOUNT) := v_column_name;
		end loop;
		close select_columns;

		htp.p('<tr><td>'||v_sql_stmt||'</td></tr><tr><td><table border="1" width="100%">');

		-- bygger opp et select og et delete statement samt skriver ut heading
		htp.p('<tr>');
		v_delete_stmt_org := 'DELETE FROM '||p_table_name||' WHERE';
		v_sql_stmt := 'SELECT ';
		v_i := 1;	-- initierer hjelpetelleren til � v�re 1
		for i in v_array.first..v_array.last loop
			htp.p('<td><a href="fpi_admin.admin_table?p_action=list&p_table_name='||p_table_name||'&p_sql_stmt=ORDER BY '||v_array(i)||'"><b>'||v_array(i)||'</b></td>');
			v_sql_stmt := v_sql_stmt ||''||v_array(i)||',';
			if (owa_pattern.match(v_array(i), '_pk', 'i')) then
				v_delete_stmt_org := v_delete_stmt_org ||' '||v_array(i)||'���:'||v_i||' AND';
				v_pk_array(v_i) := i;	-- tar var p� hvor i arrayet pk'en er
				v_i := v_i + 1;
			end if;
		end loop;
		
		owa_pattern.change(v_sql_stmt, ',$', ' FROM '||p_table_name||' '||p_sql_stmt);	-- bytter ut siste ',' med slutten av sql koden pg "p_sql_stmt" for � f� med sortering...
		owa_pattern.change(v_delete_stmt_org, 'AND$', ''); 								-- fjerner siste 'AND', da det alltid vil v�re en AND for mye

		htp.p('<td>&nbsp;</td><td>&nbsp;</td></tr>');

		-- finner ut hvor mange elementer det finnes i arrayet
		v_i := v_array.last;

		open c_dyn_cursor for v_sql_stmt;
		loop
			-- det burde ha v�rt en bedre m�te � gj�re dette p�, men jeg har ikke funnet ut hvordan
			if (v_i = 1) then
				fetch c_dyn_cursor into v_array(1);
			elsif (v_i = 2) then
				fetch c_dyn_cursor into v_array(1),v_array(2);
			elsif (v_i = 3) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3);
			elsif (v_i = 4) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4);
			elsif (v_i = 5) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5);
			elsif (v_i = 6) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6);
			elsif (v_i = 7) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7);
			elsif (v_i = 8) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8);
			elsif (v_i = 9) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9);
			elsif (v_i = 10) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10);
			elsif (v_i = 11) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10),v_array(11);
			elsif (v_i = 12) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10),v_array(11),v_array(12);
			elsif (v_i = 13) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10),v_array(11),v_array(12),v_array(13);
			elsif (v_i = 14) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10),v_array(11),v_array(12),v_array(13),v_array(14);
			elsif (v_i = 15) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10),v_array(11),v_array(12),v_array(13),v_array(14),v_array(15);
			elsif (v_i = 16) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10),v_array(11),v_array(12),v_array(13),v_array(14),v_array(15),v_array(16);
			elsif (v_i = 17) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10),v_array(11),v_array(12),v_array(13),v_array(14),v_array(15),v_array(16),v_array(17);
			elsif (v_i = 18) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10),v_array(11),v_array(12),v_array(13),v_array(14),v_array(15),v_array(16),v_array(17),v_array(18);
			elsif (v_i = 19) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10),v_array(11),v_array(12),v_array(13),v_array(14),v_array(15),v_array(16),v_array(17),v_array(18),v_array(19);
			elsif (v_i = 20) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10),v_array(11),v_array(12),v_array(13),v_array(14),v_array(15),v_array(16),v_array(17),v_array(18),v_array(19),v_array(20);
			elsif (v_i = 21) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10),v_array(11),v_array(12),v_array(13),v_array(14),v_array(15),v_array(16),v_array(17),v_array(18),v_array(19),v_array(20),v_array(21);
			elsif (v_i = 22) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10),v_array(11),v_array(12),v_array(13),v_array(14),v_array(15),v_array(16),v_array(17),v_array(18),v_array(19),v_array(20),v_array(21),v_array(22);
			elsif (v_i = 23) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10),v_array(11),v_array(12),v_array(13),v_array(14),v_array(15),v_array(16),v_array(17),v_array(18),v_array(19),v_array(20),v_array(21),v_array(22),v_array(23);
			elsif (v_i = 24) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10),v_array(11),v_array(12),v_array(13),v_array(14),v_array(15),v_array(16),v_array(17),v_array(18),v_array(19),v_array(20),v_array(21),v_array(22),v_array(23),v_array(24);
			elsif (v_i = 25) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10),v_array(11),v_array(12),v_array(13),v_array(14),v_array(15),v_array(16),v_array(17),v_array(18),v_array(19),v_array(20),v_array(21),v_array(22),v_array(23),v_array(24),v_array(25);
			else
				raise_application_error(-20000, 'more colums selected than fetched');
			end if;

			exit when c_dyn_cursor%NOTFOUND;

			htp.p('
				<tr>
				<form action="fpi_admin.admin_table" method="post">
			');
			for i in v_array.first..v_array.last loop
				owa_pattern.change(v_array(i),'<','\&#060;', 'g');
				owa_pattern.change(v_array(i),'>','\&#062;', 'g');
				owa_pattern.change(v_array(i), '"', '\&quot;', 'g');
				htp.p( v_text );
				htp.p('<td valign="top">'||v_array(i)||'<input type="hidden" name="p_column_data" value="'||v_array(i)||'"></td>');
			end loop;

			-- g�r igjennom DELETE statementet og bytter ut ':1', ':2' etc med riktige verdier.
			v_delete_stmt := v_delete_stmt_org;
			for i in v_pk_array.first..v_pk_array.last loop
				owa_pattern.change(v_delete_stmt, ':'||i , v_array(v_pk_array(i)));
			end loop;

			htp.p( '
				<td valign="top">
					<nobr>
					<input type="hidden" name="p_table_name" value="'||p_table_name||'">
					<input type="hidden" name="p_action" value="update">
					<input type="submit" value="update">
					</form>
					</nobr>
				</td>
				<td valign="top">
					<a href="fpi_admin.admin_table?p_action=delete&p_table_name='||p_table_name||'&p_sql_stmt='||v_delete_stmt||'">delete</a>
				</td>
			');

		end loop;
		close c_dyn_cursor;

	-- ----------------------------------------
	-- delete: confirm deleting an entry
	-- ----------------------------------------
	elsif( owa_pattern.match(v_action, '^delete$')) then

		v_sql_stmt := p_sql_stmt;
		owa_pattern.change(v_sql_stmt,'���','=', 'g');

		htp.p('<tr><td>'||v_sql_stmt||'?</td></tr>');

		owa_pattern.change(v_sql_stmt,'DELETE','SELECT *','i');
		htp.p('
			<tr><td><br>Are you sure you want run this delete statement?<br>You will delete ALL data referenced to this database entry (CASCADE delete).<br><br></td></tr>
			<tr><td>
				<a href="javascript:history.back(-1)">don''t delete</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="fpi_admin.admin_table?p_action=delete_action&p_table_name='||p_table_name||'&p_sql_stmt='||p_sql_stmt||'">delete</a>
			</td></tr>
			<tr><td><br>If you''re not 100% sure, run this SQL statement to see what you are deleting:<br><i>'||v_sql_stmt||';</i><br><br></td></tr>
		');

	-- ----------------------------------------
	-- delete: delete an entry
	-- ----------------------------------------
	elsif( owa_pattern.match(v_action, '^delete_action$')) then
		
		v_sql_stmt := p_sql_stmt;
		owa_pattern.change(v_sql_stmt,'���','=', 'g');

		htp.p('<tr><td>'||v_sql_stmt||'</td></tr>');

		execute immediate v_sql_stmt;

	-- ----------------------------------------
	-- create_sequence: SQL to create an sequence
	-- ----------------------------------------
	elsif( owa_pattern.match(v_action, '^create_sequence$')) then

		htp.p('<tr><td><i>'|| p_sql_stmt ||';</i></td></tr>');

	-- ----------------------------------------
	-- update_action: updates entry
	-- ----------------------------------------
	elsif( owa_pattern.match(v_action, '^update_action$')) then

		-- henter ut og legger alle kolonnenes datatyper i et array
		open select_columns;
		loop
			fetch select_columns into v_column_name, v_data_type, v_data_length;
			exit when select_columns%NOTFOUND;
			v_array(select_columns%ROWCOUNT) := v_data_type;
		end loop;
		close select_columns;

		v_sql_stmt := 'UPDATE '||p_table_name||' SET ';

		for i in p_column_data.first..p_column_data.last loop
			v_column_data := p_column_data(i);

			if ( owa_pattern.match(p_column_data(i),'\[PRIMARY\_KEY:') ) then
				v_name := p_column_data(i);
				owa_pattern.change(v_name, '\[PRIMARY\_KEY:', '');
				owa_pattern.change(v_name, '\]', '');
				v_where_stmt := v_where_stmt ||' '|| p_column_name(i) ||'='|| v_name ||' AND';
			else
				if ( owa_pattern.match(v_array(i), 'VARCHAR2') ) then
					owa_pattern.change(v_column_data, '''', '''''');	-- dersom teksten inneholder ''' tegn
					v_sql_stmt := v_sql_stmt ||' '||p_column_name(i)||'='''|| v_column_data ||''',';
				elsif ( owa_pattern.match(v_array(i), 'NUMBER') ) then
					owa_pattern.change(v_column_data, ',', '\.');	-- dersom "teksten" inneholder ',' tegn
					if ( p_column_data(i) is null) then
						v_sql_stmt := v_sql_stmt ||' '||p_column_name(i)||'=NULL,';
					else
						v_sql_stmt := v_sql_stmt ||' '||p_column_name(i)||'='|| v_column_data ||',';
					end if;
				elsif ( owa_pattern.match(v_array(i), 'DATE') ) then
					owa_pattern.change(v_column_data, '\/', '');
					owa_pattern.change(v_column_data, ':', '');
					owa_pattern.change(v_column_data, ' ', '', 'g');
					if ( p_column_data(i) is null) then
						v_sql_stmt := v_sql_stmt ||' '||p_column_name(i)||'=NULL,';
					else
						v_sql_stmt := v_sql_stmt ||' '||p_column_name(i)||'=to_date('''|| v_column_data ||''',''DDMMYYHH24MI''),';
					end if;
				end if;
			end if;

		end loop;

		owa_pattern.change(v_where_stmt,'AND$','');
		owa_pattern.change(v_sql_stmt,',$',' WHERE '||v_where_stmt);

		execute immediate v_sql_stmt;

		owa_pattern.change(v_sql_stmt,'<','\&#060;', 'g');
		owa_pattern.change(v_sql_stmt,'>','\&#062;', 'g');
		owa_pattern.change(v_sql_stmt, '"', '\&quot;', 'g');

		htp.p('<tr><td><i>'|| v_sql_stmt ||';</i></td></tr>');

	end if;

	htp.p('</table></td></tr></form></table>');


	-- ---------------------
	-- exception handeling
	-- ---------------------
	exception
		when no_data_found then 
			htp.p('<font color="#ff0000">error: NO_DATA_FOUND exception</font>');

--		when others then
--			raise_application_error(-20000, v_sql_stmt);

END;
END admin_table;





-- ++++++++++++++++++++++++++++++++++++++++++++++ --

END; -- ends package body
/
show errors;

