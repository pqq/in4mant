set define off
PROMPT *** package: fpu_news ***


CREATE OR REPLACE PACKAGE fpu_news IS

	PROCEDURE startup;
	PROCEDURE list_notes
		(
			p_note_pk		in note.note_pk%type	default NULL
		);
	PROCEDURE list_news
		(
			p_news_pk		in news.news_pk%type	default NULL
		);

END;
/
show errors;


CREATE OR REPLACE PACKAGE BODY fpu_news IS


---------------------------------------------------------
-- Name:        startup
-- What:        starts up this package
-- Author:      Frode Klevstul
-- Start date:  21.05.2002
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

	htp.p('startup');

END startup;



---------------------------------------------------------
-- Name:        list_notes
-- What:        write out the notes
-- Author:      Frode Klevstul
-- Start date:  21.05.2002
---------------------------------------------------------
PROCEDURE list_notes
	(
		p_note_pk		in note.note_pk%type	default NULL
	)
IS
BEGIN
DECLARE

	CURSOR	select_all IS
	SELECT	note_pk, heading, ingress, author
	FROM	note
	WHERE	accepted = 1
	ORDER BY order_by asc;

	CURSOR	select_one IS
	SELECT	heading, ingress, body, author
	FROM	note
	WHERE	accepted = 1
	AND		note_pk = p_note_pk
	ORDER BY order_by asc;

	v_heading		note.heading%type	default NULL;
	v_ingress		note.ingress%type	default NULL;
	v_body			note.body%type		default NULL;
	v_author		note.author%type	default NULL;
	v_note_pk		note.note_pk%type	default NULL;
	v_count			number				default 0;

BEGIN

	-- ----------------------------
	-- lister ut alle notatene
	-- ----------------------------
	if (p_note_pk is null) then

		fps_htp.b_box('Notes', '100%');
		fps_htp.b_table;
	
		open select_all;
		loop
			fetch select_all into v_note_pk, v_heading, v_ingress, v_author;
			exit when select_all%NOTFOUND;

			SELECT	count(*)
			INTO	v_count
			FROM	note
			WHERE	note_pk = v_note_pk
			AND		body IS NOT NULL;
	
			htp.p('
				<tr><td colspan="2"><h3>'||v_heading||'</h3></td></tr>
				<tr><td colspan="2">'||v_ingress||'</td></tr>
				<tr>
					<td><i>'||v_author||'</i></td>
			');

			if (v_count > 0) then
				htp.p('<td align="right" CLASS="link"><a href="fpu_news.list_notes?p_note_pk='||v_note_pk||'">read more</a></td>');
			else
				htp.p('<td>&nbsp;</td>');			
			end if;
			
			htp.p('
				</tr>
				<tr><td colspan="2"><hr noshade size="1" align="center" width="98%"></td></tr>
			');
	
		end loop;
		close select_all;
	
		fps_htp.e_table;
		fps_htp.e_box;

	-- -----------------------------------
	-- lister ut all info om ett notat
	-- -----------------------------------	
	else

		fps_htp.b_page;
		fps_htp.b_box('Notes', '100%');
		fps_htp.b_table;
	
		open select_one;
		loop
			fetch select_one into v_heading, v_ingress, v_body, v_author;
			exit when select_one%NOTFOUND;
	
			htp.p('
				<tr><td><h1>'||v_heading||'</h1></td></tr>
				<tr><td><h2>'||v_ingress||'</h2></td></tr>
				<tr><td>'||v_body||'</td></tr>
				<tr><td>&nbsp;</td></tr>
				<tr><td><i>'||v_author||'</i></td></tr>
			');
	
		end loop;
		close select_one;
	
		fps_htp.e_table;
		fps_htp.e_box;
		fps_htp.e_page;

	end if;


END;
END list_notes;



---------------------------------------------------------
-- Name:        list_news
-- What:        write out the news
-- Author:      Frode Klevstul
-- Start date:  17.06.2002
---------------------------------------------------------
PROCEDURE list_news
	(
		p_news_pk		in news.news_pk%type	default NULL
	)
IS
BEGIN
DECLARE

	CURSOR	select_all IS
	SELECT	news_pk, heading, publish_date
	FROM	news
	WHERE	accepted = 1
	ORDER BY publish_date desc;

	CURSOR	select_one IS
	SELECT	heading, ingress, body, author, publish_date
	FROM	news
	WHERE	accepted = 1
	AND		news_pk = p_news_pk
	ORDER BY publish_date desc;

	v_heading		news.heading%type		default NULL;
	v_ingress		news.ingress%type		default NULL;
	v_body			news.body%type			default NULL;
	v_author		news.author%type		default NULL;
	v_news_pk		news.news_pk%type		default NULL;
	v_publish_date	news.publish_date%type	default NULL;

BEGIN

	-- ----------------------------
	-- lister ut alle notatene
	-- ----------------------------
	if (p_news_pk is null) then

		fps_htp.b_box('News', '100%');
		fps_htp.b_table;
	
		open select_all;
		loop
			fetch select_all into v_news_pk, v_heading, v_publish_date;
			exit when select_all%NOTFOUND;

			htp.p('<tr><td width="70"><b>'||v_publish_date||':</b></td><td><a href="fpu_news.list_news?p_news_pk='||v_news_pk||'">'||v_heading||'</a></td></tr>');

		end loop;
		close select_all;
	
		fps_htp.e_table;
		fps_htp.e_box;

	-- -----------------------------------
	-- lister ut all info om ett nyhetsoppslag
	-- -----------------------------------	
	else

		fps_htp.b_page;
		fps_htp.b_box('News', '100%');
		fps_htp.b_table;
	
		open select_one;
		loop
			fetch select_one into v_heading, v_ingress, v_body, v_author, v_publish_date;
			exit when select_one%NOTFOUND;
	
			htp.p('
				<tr><td>'||v_publish_date||' (CET)</td></tr>
				<tr><td><h1>'||v_heading||'</h1></td></tr>
				<tr><td><h2>'||v_ingress||'</h2></td></tr>
				<tr><td>'||v_body||'</td></tr>
				<tr><td>&nbsp;</td></tr>
				<tr><td><i>'||v_author||'</i></td></tr>
			');
	
		end loop;
		close select_one;
	
		fps_htp.e_table;
		fps_htp.e_box;
		fps_htp.e_page;

	end if;


END;
END list_news;

-- ++++++++++++++++++++++++++++++++++++++++++++++ --

END; -- ends package body
/
show errors;

