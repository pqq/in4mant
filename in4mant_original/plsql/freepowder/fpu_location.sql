set define off
PROMPT *** package: fpu_location ***


CREATE OR REPLACE PACKAGE fpu_location IS

	PROCEDURE startup;
	PROCEDURE locations
		(
			p_location_pk	in location.location_pk%type	default NULL
		);

END;
/
show errors;


CREATE OR REPLACE PACKAGE BODY fpu_location IS


---------------------------------------------------------
-- Name:        startup
-- What:        starts up this package
-- Author:      Frode Klevstul
-- Start date:  17.06.2002
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

	htp.p('startup');

END startup;




---------------------------------------------------------
-- Name:        locations
-- What:        write out the notes
-- Author:      Frode Klevstul
-- Start date:  21.05.2002
---------------------------------------------------------
PROCEDURE locations
	(
		p_location_pk	in location.location_pk%type	default NULL
	)
IS
BEGIN
DECLARE

	-- lister ut "location of the month"
	CURSOR	select_lofm IS
	SELECT	l.location_pk, l.title, s.longitude, s.latitude
	FROM	location l, geography g, country c, spot s
	WHERE	l.geography_fk = g.geography_pk
	AND		g.country_fk = c.country_pk
	AND		g.spot_fk = s.spot_pk
	AND		l.accepted = 1
	AND		l.publish_date = 
		(
			SELECT	MAX(publish_date) 
			FROM	location
		);

	-- lister ut "location of the month"
	CURSOR	select_location IS
	SELECT	l.location_pk, l.title, s.longitude, s.latitude
	FROM	location l, geography g, country c, spot s
	WHERE	l.geography_fk = g.geography_pk
	AND		g.country_fk = c.country_pk
	AND		g.spot_fk = s.spot_pk
	AND		l.accepted = 1
	AND		l.location_pk = p_location_pk;

	v_location_pk	location.location_pk%type	default NULL;
	v_title			location.title%type			default NULL;
	v_longitude		spot.longitude%type			default NULL;
	v_latitude		spot.latitude%type			default NULL;

BEGIN

	-- ----------------------------
	-- lister ut "location of the month" (lotm)
	-- ----------------------------
	if (p_location_pk is null) then

		fps_htp.b_box('Location of the month', '100%');
		fps_htp.b_table;
	
		open select_lofm;
		loop
			fetch select_lofm into v_location_pk, v_title, v_longitude, v_latitude;
			exit when select_lofm%NOTFOUND;
	
			htp.p('
				<tr>
					<td><b>'||v_longitude||' - '||v_latitude||'</b>:</td>
					<td><a href="fpu_location.locations?p_location_pk='||v_location_pk||'">'||v_title||'</td>
				</tr>
			');
	
		end loop;
		close select_lofm;
	
		fps_htp.e_table;
		fps_htp.e_box;
	
	-- ----------------------------
	-- lister ut info om en lokasjon
	-- ----------------------------
	else

		fps_htp.b_page;
		fps_htp.b_box('Location of the month', '100%');
		fps_htp.b_table;
	
		open select_location;
		loop
			fetch select_location into v_location_pk, v_title, v_longitude, v_latitude;
			exit when select_location%NOTFOUND;
	
			htp.p('
				<tr>
					<td>Longitude: '||v_longitude||' Latitude: '||v_latitude||'</td>
				</tr>
				<tr>
					<td>'||v_title||'</td>
				</tr>
			');
	
		end loop;
		close select_location;
	
		fps_htp.e_table;
		fps_htp.e_box;
		fps_htp.e_page;
	
	end if;

END;
END locations;


-- ++++++++++++++++++++++++++++++++++++++++++++++ --

END; -- ends package body
/
show errors;

