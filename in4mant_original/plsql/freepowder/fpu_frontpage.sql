set define off
PROMPT *** package: fpu_frontpage ***


CREATE OR REPLACE PACKAGE fpu_frontpage IS

	PROCEDURE startup;
	PROCEDURE frontpage;

END;
/
show errors;


CREATE OR REPLACE PACKAGE BODY fpu_frontpage IS


---------------------------------------------------------
-- Name:        startup
-- What:        starts up this package
-- Author:      Frode Klevstul
-- Start date:  17.06.2002
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

	frontpage;

END startup;



---------------------------------------------------------
-- Name:        frontpage
-- What:        write out the notes
-- Author:      Frode Klevstul
-- Start date:  17.06.2002
---------------------------------------------------------
PROCEDURE frontpage
IS
BEGIN

	fps_htp.b_page;

	fps_htp.b_table;

	htp.p('<tr><td valign="top">');
	fpu_news.list_news;
	htp.p('</td><td valign="top">');
	fpu_news.list_notes;
	htp.p('</td></tr>');

	htp.p('<tr><td valign="top">');
	fpu_location.locations;
	htp.p('</td><td valign="top">');
	htp.p('profile of the month here...');
	htp.p('</td></tr>');


	fps_htp.e_table;

	fps_htp.e_page;

END frontpage;


-- ++++++++++++++++++++++++++++++++++++++++++++++ --

END; -- ends package body
/
show errors;

