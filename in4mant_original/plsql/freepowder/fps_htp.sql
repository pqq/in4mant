set define off
PROMPT *** package: fps_htp ***


CREATE OR REPLACE PACKAGE fps_htp IS

	PROCEDURE b_box
		(
			p_title			in varchar2					default '&nbsp;',
			p_width			in varchar2					default '100%'
		);
	PROCEDURE e_box;
	PROCEDURE b_table
		(
			p_width			in varchar2					default '100%',
			p_border		in varchar2					default '0'
		);
	PROCEDURE e_table;
	PROCEDURE b_page
		(
			p_selected	in number						default NULL,
			p_width		in varchar2						default fps_get.value('page_width'),
			p_reload	in varchar2						default NULL,
			p_jump		in varchar2						default NULL
		);
	PROCEDURE e_page;

END;
/
show errors;



CREATE OR REPLACE PACKAGE BODY fps_htp IS



---------------------------------------------------------
-- Name:        b_box
-- What:        returns b_box code
-- Author:      Frode Klevstul
-- Start date:  17.06.2002
---------------------------------------------------------
PROCEDURE b_box
	(
		p_title			in varchar2					default '&nbsp;',
		p_width			in varchar2					default '100%'
	)
IS
BEGIN
	
	htp.p( fps_htf.b_box(p_title, p_width) );

END b_box;



---------------------------------------------------------
-- Name:		e_box
-- What:		returns e_box code
-- Author:		Frode Klevstul
-- Start date:	17.06.2002
---------------------------------------------------------
PROCEDURE e_box
IS
BEGIN

	htp.p( fps_htf.e_box );

END e_box;



---------------------------------------------------------
-- Name:        b_table
-- What:        returns b_table code
-- Author:      Frode Klevstul
-- Start date:  17.06.2002
---------------------------------------------------------
PROCEDURE b_table
	(
		p_width			in varchar2					default '100%',
		p_border		in varchar2					default '0'
	)
IS
BEGIN

	htp.p( fps_htf.b_table(p_width, p_border) );

END b_table;



---------------------------------------------------------
-- Name:		e_table
-- What:		returns e_table code
-- Author:		Frode Klevstul
-- Start date:	17.06.2002
---------------------------------------------------------
PROCEDURE e_table
IS
BEGIN

	htp.p( fps_htf.e_table );

END e_table;



---------------------------------------------------------
-- Name:		b_page
-- What:		starts a page
-- Author:		Frode Klevstul
-- Start date:	17.06.2002
---------------------------------------------------------
PROCEDURE b_page
	(
		p_selected	in number					default NULL,
		p_width		in varchar2					default fps_get.value('page_width'),
		p_reload	in varchar2					default NULL,
		p_jump		in varchar2					default NULL
	)
IS
BEGIN

	htp.p( fps_htf.b_page(p_selected, p_width, p_reload, p_jump) );

END b_page;



---------------------------------------------------------
-- Name:		e_page
-- What:		ends b_page
-- Author:		Frode Klevstul
-- Start date:	17.06.2002
---------------------------------------------------------
PROCEDURE e_page
IS
BEGIN

	htp.p( fps_htf.e_page );

END e_page;


-- ++++++++++++++++++++++++++++++++++++++++++++++ --

END; -- ends package body
/
show errors;

