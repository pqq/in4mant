set define off
PROMPT *** package: drink_guiden ***

CREATE OR REPLACE PACKAGE drink_guiden IS

empty_array owa_util.ident_arr;

PROCEDURE startup;
PROCEDURE show_drink (     p_drink_pk           in drink.drink_pk%TYPE            DEFAULT NULL );
PROCEDURE random_drink;

FUNCTION select_ingredient (     p_ingredient_fk           in ingredient.ingredient_pk%TYPE            DEFAULT NULL )
                           return varchar2;
PROCEDURE browse (  p_command         IN varchar2             DEFAULT 'name',
                    p_pk              IN NUMBER               DEFAULT NULL
                    );
PROCEDURE search (  p_string          IN varchar2             DEFAULT NULL,
                    p_command         IN varchar2             DEFAULT NULL,
                    p_param           IN varchar2             DEFAULT NULL,
                    p_ingredient_fk   IN owa_util.ident_arr   DEFAULT empty_array
                    );
PROCEDURE search_form (  p_string          IN varchar2             DEFAULT NULL,
                         p_ingredient_fk   IN owa_util.ident_arr   DEFAULT empty_array
                    );
PROCEDURE browse_menu;
END;
/
CREATE OR REPLACE PACKAGE BODY drink_guiden IS

--------------------------------------------------------------------
-- Setter globale variable
--------------------------------------------------------------------
--v_measurement_array     owa_util.ident_arr      DEFAULT empty_array;
/*
v_measurement_array(375):='3/8';
v_measurement_array(125):='1/8';
v_measurement_array(80):='4/5';
v_measurement_array(75):='3/4';
v_measurement_array(67):='2/3';
v_measurement_array(60):='3/5';
v_measurement_array(50):='1/2';
v_measurement_array(40):='2/5';
v_measurement_array(33):='1/3';
v_measurement_array(25):='1/4';
v_measurement_array(20):='1/5';
v_measurement_array(8):='4/5';
v_measurement_array(6):='3/5';
v_measurement_array(5):='1/2';
v_measurement_array(4):='2/5';
v_measurement_array(2):='1/5';
*/
--------------------------------------------------------------------

---------------------------------------------------------------------
---------------------------------------------------------------------
-- Name: startup
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 15.07.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE startup
IS
BEGIN
IF (    login.timeout('drink_guiden.startup') > 0 AND
        login.right('drink_guiden') > 0 ) THEN
        html.b_adm_page;
        html.b_box;
        htp.p('<a href="drink_guiden.browse">browse drink by name</a><br>');
        htp.p('<a href="drink_guiden.browse?p_command=glass">browse drink by glass</a><br>');
        htp.p('<a href="drink_guiden.browse?p_command=category">browse drink by category</a><br>');
        htp.p('<a href="drink_guiden.browse?p_command=ingredient">browse drink by ingredient</a><br>');
        htp.p('<a href="drink_guiden.browse?p_command=ing_type">browse drink by ingredient type</a><br>');
        html.e_box;
        html.e_page_2;
END IF;
END startup;

---------------------------------------------------------------------
-- Name: show_drink
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 02.03.2002
-- Chng:
---------------------------------------------------------------------

PROCEDURE show_drink (     p_drink_pk           in drink.drink_pk%TYPE            DEFAULT NULL )
IS
BEGIN
DECLARE

v_name_sg_fk            drink.name_sg_fk%TYPE                   DEFAULT NULL;
v_glass_sg_fk           glass.name_sg_fk%TYPE                   DEFAULT NULL;
v_category_sg_fk        category.name_sg_fk%TYPE                DEFAULT NULL;
v_info_sg_fk            drink.info_sg_fk%TYPE                   DEFAULT NULL;
v_ingredient_sg_fk      ingredient.name_sg_fk%TYPE              DEFAULT NULL;
v_order_in_drink        ing_in_drink.order_in_drink%TYPE        DEFAULT NULL;
v_amount                ing_in_drink.amount%TYPE                DEFAULT NULL;
v_amount2               VARCHAR2(16)                            DEFAULT NULL;
v_decimal               NUMBER                                  DEFAULT NULL;
v_number                NUMBER                                  DEFAULT NULL;
v_measurement_sg_fk     ing_in_drink.measurement_sg_fk%TYPE     DEFAULT NULL;
v_source                drink.source%TYPE                       DEFAULT NULL;
v_measurement_array     owa_util.ident_arr                      DEFAULT empty_array;

CURSOR get_drink IS
select d.name_sg_fk, d.glass_sg_fk, d.category_sg_fk, d.info_sg_fk,
       i.name_sg_fk, iid.order_in_drink, iid.amount, d.source, iid.measurement_sg_fk
from   drink d, ing_in_drink iid, ingredient i
where  d.drink_pk = iid.drink_fk
and    iid.ingredient_fk = i.ingredient_pk
and    d.drink_pk = p_drink_pk
order by iid.order_in_drink
;

BEGIN
/*
FOR v_number IN 1 .. 100
LOOP
  v_measurement_array(v_number):=NULL;
END LOOP;
*/
v_measurement_array(375):='3/8';
v_measurement_array(125):='1/8';
v_measurement_array(80):='4/5';
v_measurement_array(75):='3/4';
v_measurement_array(67):='2/3';
v_measurement_array(60):='3/5';
v_measurement_array(50):='1/2';
v_measurement_array(40):='2/5';
v_measurement_array(33):='1/3';
v_measurement_array(25):='1/4';
v_measurement_array(20):='1/5';
v_measurement_array(8):='4/5';
v_measurement_array(6):='3/5';
v_measurement_array(5):='1/2';
v_measurement_array(4):='2/5';
v_measurement_array(2):='1/5';

html.b_page(NULL, NULL, NULL, NULL, 1);
html.home_menu;
htp.p('<table border="0" cellspacing="0" cellpadding="10" width="100%">');
htp.p('<tr><td valign="top" width="30%" align="left">');
drink_guiden.browse_menu;
htp.p('<br><br>');
drink_guiden.search_form;
htp.p('</td><td valign="top" width="490">');
html.b_box('','100%','drink_guiden.show_drink');
html.b_table;

-- Legger inn i statistikk tabellen
stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,31,get.serv);

open get_drink;
LOOP
        fetch get_drink INTO v_name_sg_fk, v_glass_sg_fk, v_category_sg_fk,
                v_info_sg_fk, v_ingredient_sg_fk, v_order_in_drink, v_amount, v_source, v_measurement_sg_fk;
        exit when get_drink%NOTFOUND;
        IF ( v_source IS NULL ) THEN
           v_source := get.txt('unknown');
        END IF;

        IF ( get_drink%ROWCOUNT = 1 ) THEN
           htp.p('<tr><td colspan="2"><span class="boxheading">'||get.text( v_name_sg_fk )||'</span><br></td></tr>
           <tr><td width="20%">'||get.txt('category')||':</td><td width="80%">'||get.text( v_category_sg_fk )||'</td></tr>
           <tr><td>'||get.txt('glass')||':</td><td>'||get.text( v_glass_sg_fk )||'<br></td></tr>
           <tr><td colspan="2">'||get.txt('ingredients')||':</td></tr>
           <tr><td>&nbsp;</td><td align="left">');
        END IF;
        IF ( instr(v_amount,',') > 0 ) THEN
           v_number  := substr(v_amount,1,(instr(v_amount,',')-1));
           v_decimal := substr(v_amount,(instr(v_amount,',')+1),length(v_amount));
           IF ( v_number > 0 ) THEN
              v_amount2 := to_char(v_number)||'&nbsp;'||v_measurement_array(v_decimal);
           ELSE
              v_amount2 := v_measurement_array(v_decimal);
           END IF;
        ELSIF ( v_amount < 1 ) THEN
           v_amount2 := '0'||to_char(v_amount);
        ELSE
           v_amount2 := to_char(v_amount);
        END IF;
        htp.p(v_amount2||' '||get.text(v_measurement_sg_fk)||' '||get.text(v_ingredient_sg_fk)||'<br>');

END LOOP;
htp.p('&nbsp;<br></td></tr>');

htp.p('<tr><td colspan="2">'||get.txt('Mixing instructions')||':</td></tr>
<tr><td colspan="2">'||get.text(v_info_sg_fk)||'</td></tr>
<tr><td colspan="2"><br><b><i>'||get.txt('source')||':</i></b>&nbsp;<i>'||v_source||'</i></td></tr>');
close get_drink;
html.e_table;
html.e_box;
htp.p('</td></tr>');
html.e_table;
html.e_page;

END;
END show_drink;

---------------------------------------------------------------------
-- Name: random_drink
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 02.03.2002
-- Chng:
---------------------------------------------------------------------

PROCEDURE random_drink
IS
BEGIN
DECLARE
v_drink_pk              drink.drink_pk%TYPE                     DEFAULT NULL;
v_drink_pk_prev         drink.drink_pk%TYPE                     DEFAULT NULL;
v_name_sg_fk            drink.name_sg_fk%TYPE                   DEFAULT NULL;
v_glass_sg_fk           glass.name_sg_fk%TYPE                   DEFAULT NULL;
v_category_sg_fk        category.name_sg_fk%TYPE                DEFAULT NULL;
v_info_sg_fk            drink.info_sg_fk%TYPE                   DEFAULT NULL;
v_ingredient_sg_fk      ingredient.name_sg_fk%TYPE              DEFAULT NULL;
v_order_in_drink        ing_in_drink.order_in_drink%TYPE        DEFAULT NULL;
v_amount                ing_in_drink.amount%TYPE                DEFAULT NULL;
v_measurement_sg_fk     ing_in_drink.measurement_sg_fk%TYPE     DEFAULT NULL;
v_source                drink.source%TYPE                       DEFAULT NULL;

CURSOR get_drink IS
select d.drink_pk, d.name_sg_fk, d.glass_sg_fk, d.category_sg_fk, d.info_sg_fk,
       i.name_sg_fk, iid.order_in_drink, iid.amount, d.source, iid.measurement_sg_fk
from   drink d, ing_in_drink iid, ingredient i
where  d.drink_pk = iid.drink_fk
and    iid.ingredient_fk = i.ingredient_pk
and    d.drink_pk >= ( select to_number(value) from parameter where name = 'random_drink' )
order by d.drink_pk, iid.order_in_drink
;

BEGIN

html.b_box_2(get.txt('bartender'),'100%','drink_guiden.show_drink','drink_guiden.browse');
html.b_table;

open get_drink;
LOOP
        fetch get_drink INTO v_drink_pk, v_name_sg_fk, v_glass_sg_fk, v_category_sg_fk,
                v_info_sg_fk, v_ingredient_sg_fk, v_order_in_drink, v_amount, v_source, v_measurement_sg_fk;
        exit when get_drink%NOTFOUND;
        IF ( v_drink_pk_prev IS NOT NULL AND v_drink_pk <> v_drink_pk_prev ) THEN
           update parameter
           set value = to_char(v_drink_pk)
           where name = 'random_drink';
           commit;
           exit;
        ELSE
           v_drink_pk_prev := v_drink_pk;
        END IF;

        IF ( v_source IS NULL ) THEN
           v_source := get.txt('unknown');
        END IF;
        IF ( get_drink%ROWCOUNT = 1 ) THEN
           htp.p('<tr><td colspan="2"><a href="drink_guiden.show_drink?p_drink_pk='||v_drink_pk||'"><span class="boldheading">'||get.text( v_name_sg_fk )||'</span></a></td></tr>
           <tr><td colspan="2">'||get.txt('ingredients')||':</td></tr>
           <tr><td>&nbsp;</td><td>');
--         html.b_table(NULL,0);
        END IF;
        IF ( v_amount < 1 ) THEN
--              htp.p('<tr><td align="right">0'||v_amount||' '||get.text(v_measurement_sg_fk)||'</td><td align="left">'||get.text(v_ingredient_sg_fk)||'</td></tr>');
                htp.p('0'||v_amount||' '||get.text(v_measurement_sg_fk)||' '||get.text(v_ingredient_sg_fk)||'<br>');
        ELSE
--              htp.p('<tr><td align="right">'||v_amount||' '||get.text(v_measurement_sg_fk)||'</td><td align="left">'||get.text(v_ingredient_sg_fk)||'</td></tr>');
                htp.p(v_amount||' '||get.text(v_measurement_sg_fk)||' '||get.text(v_ingredient_sg_fk)||'<br>');
        END IF;


END LOOP;

IF ( v_drink_pk = v_drink_pk_prev ) THEN
   update parameter
   set value = '1'
   where name = 'random_drink';
   commit;
END IF;

close get_drink;
htp.p('</td></tr>
<tr><td colspan=2><br>'); html.button_link_2(get.txt('more_drinks'), 'drink_guiden.browse', NULL);
htp.p('</td></tr>');

html.e_table;
html.e_box_2;

END;
END random_drink;


---------------------------------------------------------------------
-- Name: select_ingredient
-- Type: function
-- What:
-- Made: Espen Messel
-- Date: 02.03.2002
-- Chng:
---------------------------------------------------------------------

FUNCTION select_ingredient (     p_ingredient_fk           in ingredient.ingredient_pk%TYPE            DEFAULT NULL )
          return varchar2
IS
BEGIN
DECLARE

CURSOR  get_ingredient IS
SELECT  s.string, gl.name_sg_fk, gl.ingredient_pk
FROM    strng s, groups g, ingredient gl
WHERE   g.string_fk = s.string_pk
AND     g.string_group_fk = gl.name_sg_fk
AND     s.language_fk = get.lan
ORDER BY string;


v_string                strng.string%TYPE               DEFAULT NULL;
v_ingredient_sg_fk      ingredient.name_sg_fk%TYPE      DEFAULT NULL;
v_ingredient_pk         ingredient.ingredient_pk%TYPE   DEFAULT NULL;
v_code                  varchar2(30000)                 default null;

BEGIN

        v_code := '<select name="p_ingredient_fk">
                   <option value="">'||get.txt('no_ingredient')||'</option>
';

        OPEN get_ingredient;
        LOOP
                FETCH get_ingredient INTO v_string, v_ingredient_sg_fk, v_ingredient_pk;
                EXIT WHEN get_ingredient%NOTFOUND;
                IF ( v_ingredient_pk = p_ingredient_fk ) THEN
                        v_code := v_code || '<option value="'|| v_ingredient_pk ||'" selected>'|| v_string ||'</option>
';
                ELSE
                        v_code := v_code || '<option value="'|| v_ingredient_pk ||'">'|| v_string ||'</option>
';
                END IF;
        END LOOP;
        CLOSE get_ingredient;

        v_code := v_code || '</select>';
        return v_code;
END;
END select_ingredient;

--------------------------------------------------------------------
-- Name: browse
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 01.03.2002
-- Chng:
---------------------------------------------------------------------
PROCEDURE browse (  p_command         IN varchar2             DEFAULT 'name',
                    p_pk              IN NUMBER               DEFAULT NULL
                    )
IS
BEGIN
DECLARE

v_string_group_pk       string_group.string_group_pk%TYPE       DEFAULT NULL;
v_name                  strng.string%TYPE                       DEFAULT NULL;
v_sql                   varchar2(1024)                          DEFAULT NULL;

TYPE cur_typ IS REF CURSOR;
c_dyn_cursor    cur_typ;

BEGIN

if ( p_pk is null ) then
   if ( p_command = 'glass' ) then
      v_sql := '
                SELECT distinct(STRING),glass_sg_fk
                FROM DRINK d, GROUPS g, strng s
                WHERE d.glass_sg_fk = g.string_group_fk
                ';
   elsif ( p_command = 'category' ) then
      v_sql := '
                SELECT distinct(STRING),category_sg_fk
                FROM DRINK d, GROUPS g, strng s
                WHERE d.category_sg_fk = g.string_group_fk
                ';
   elsif ( p_command = 'name' ) then
      v_sql := '
                SELECT distinct(STRING),drink_pk
                FROM DRINK d, GROUPS g, strng s
                WHERE d.name_sg_fk = g.string_group_fk
                ';
   elsif ( p_command = 'ingredient' ) then
      v_sql := '
                SELECT distinct(STRING),ingredient_pk
                FROM ingredient i, GROUPS g, strng s
                WHERE i.name_sg_fk = g.string_group_fk
                ';
   elsif ( p_command = 'ing_type' ) then
      v_sql := '
                SELECT distinct(STRING),ing_type_sg_fk
                FROM ingredient i, GROUPS g, strng s
                WHERE i.ing_type_sg_fk = g.string_group_fk
                ';
   end if;
   v_sql := v_sql||'AND g.string_fk=s.string_pk
                AND s.language_fk = 1
                ORDER BY s.string
                ';
else
   v_sql := '
             SELECT distinct(STRING),drink_pk
             FROM DRINK d, GROUPS g, strng s
             WHERE d.name_sg_fk = g.string_group_fk
             AND g.string_fk=s.string_pk
             AND s.language_fk = 1
             ';
   if ( p_command = 'glass' ) then
      v_sql := v_sql||'AND d.glass_sg_fk = '||p_pk||'
                ORDER BY s.string
                ';
   elsif ( p_command = 'category' ) then
      v_sql := v_sql||'AND d.category_sg_fk = '||p_pk||'
                ORDER BY s.string
                ';
   elsif ( p_command = 'ingredient' ) then
      v_sql := v_sql||'AND d.drink_pk IN (
                SELECT drink_fk
                FROM   ing_in_drink
                WHERE  ingredient_fk = '||p_pk||')
                ORDER BY s.string
                ';
   elsif ( p_command = 'ing_type' ) then
      v_sql := v_sql||'AND d.drink_pk IN (
                SELECT iid.drink_fk
                FROM   ing_in_drink iid, ingredient i
                WHERE  iid.ingredient_fk = i.ingredient_pk
                AND    i.ing_type_sg_fk = '||p_pk||')
                ORDER BY s.string
                ';
   end if;
end if;

-- Legger inn i statistikk tabellen
stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,31,get.serv);

html.b_page(NULL, NULL, NULL, NULL, 1);
html.home_menu;
htp.p('<table border="0" cellspacing="0" cellpadding="10" width="100%">');
htp.p('<tr><td valign="top" width="30%" align="left">');
drink_guiden.browse_menu;
htp.p('<br><br>');
drink_guiden.search_form;
htp.p('</td><td valign="top" width="490">');

html.b_box(get.txt('browse_by_'||p_command),'100%','drink_guiden.show_drink');
html.b_table;
htp.p('<tr><td>');
        open c_dyn_cursor for v_sql;
        loop
                fetch c_dyn_cursor into v_name, v_string_group_pk;
                exit when c_dyn_cursor%NOTFOUND;

                IF ( MOD(c_dyn_cursor%ROWCOUNT,2) != 0 ) THEN
                   htp.p('<tr'||system.bgcolor((c_dyn_cursor%ROWCOUNT+1)/2)||'>');
                END IF;
                IF ( p_pk is null AND p_command != 'name' ) then
                   htp.p('<td><a href="drink_guiden.browse?p_command='||p_command||'&p_pk='||v_string_group_pk||'">'||v_name||'</a></td>');
                ELSE
                   htp.p('<td><a href="drink_guiden.show_drink?p_drink_pk='||v_string_group_pk||'">'||v_name||'</a></td>');
                END IF;
                IF ( MOD(c_dyn_cursor%ROWCOUNT,2) = 0 ) THEN
                   htp.p('</tr>');
                END IF;
        END LOOP;
        IF ( MOD(c_dyn_cursor%ROWCOUNT,2) != 0 ) THEN
           htp.p('<td>&nbsp;</td></tr>');
        END IF;
        CLOSE c_dyn_cursor;
htp.p('</td></tr>');
html.e_table;
html.e_box;
htp.p('</td></tr>');
html.e_table;
html.e_page;
END;
END browse;


---------------------------------------------------------------------
-- Name: search
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 01.03.2002
-- Chng:
---------------------------------------------------------------------
PROCEDURE search (  p_string          IN varchar2             DEFAULT NULL,
                    p_command         IN varchar2             DEFAULT NULL,
                    p_param           IN varchar2             DEFAULT NULL,
                    p_ingredient_fk   IN owa_util.ident_arr   DEFAULT empty_array
                    )
IS
BEGIN
DECLARE

v_select_ingredient     LONG                                    DEFAULT select_ingredient;
v_string_group_pk       string_group.string_group_pk%TYPE       DEFAULT NULL;
v_name                  strng.string%TYPE                       DEFAULT NULL;
v_sql                   varchar2(1024)                          DEFAULT NULL;

TYPE cur_typ IS REF CURSOR;
c_dyn_cursor    cur_typ;

BEGIN
-- Legger inn i statistikk tabellen
stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,31,get.serv);

html.b_page(NULL, NULL, NULL, NULL, 1);
html.home_menu;
htp.p('<table border="0" cellspacing="0" cellpadding="10" width="100%">');
htp.p('<tr><td valign="top" width="30%" align="left">');
drink_guiden.browse_menu;
htp.p('<br><br>');
drink_guiden.search_form(p_string);
htp.p('</td><td valign="top" width="490">');
html.b_box(get.txt('drink_search'),'100%','drink_guiden.search');
html.b_table;

IF ( p_command = 'search' AND p_string IS NOT NULL ) THEN
      v_sql := '
                SELECT distinct(STRING),drink_pk
                  FROM DRINK d, GROUPS g, strng s
                  WHERE upper(string) LIKE ''%''||upper('''||p_string||''')||''%''
                  AND d.name_sg_fk = g.string_group_fk
                  AND g.string_fk=s.string_pk
                  AND s.language_fk = 1
                  ORDER BY s.string
                  ';
ELSIF ( p_command = 'bartender' AND p_ingredient_fk(1) IS NOT NULL ) THEN
      v_sql := '
                SELECT distinct(STRING),drink_pk
                  FROM DRINK d, GROUPS g, strng s
                  WHERE d.drink_pk in (
                  SELECT drink_fk FROM ing_in_drink
                    WHERE ingredient_fk = '|| p_ingredient_fk(1);
      IF ( p_ingredient_fk(2) IS NOT NULL ) then
         v_sql := v_sql || ' ' || p_param  ||'
                  SELECT drink_fk FROM ing_in_drink
                    WHERE ingredient_fk = '|| p_ingredient_fk(2);
      END IF;
      v_sql := v_sql || ')
                  AND d.name_sg_fk = g.string_group_fk
                  AND g.string_fk=s.string_pk
                  AND s.language_fk = 1
                  ORDER BY s.string
                  ';
END IF;

IF ( v_sql IS NOT NULL ) THEN
   OPEN c_dyn_cursor for v_sql;
   LOOP
      fetch c_dyn_cursor into v_name, v_string_group_pk;
      exit when c_dyn_cursor%NOTFOUND;
      IF ( MOD(c_dyn_cursor%ROWCOUNT,2) != 0 ) THEN
         htp.p('<tr'||system.bgcolor((c_dyn_cursor%ROWCOUNT+1)/2)||'>');
      END IF;
      htp.p('<td><a href="drink_guiden.show_drink?p_drink_pk='||v_string_group_pk||'">'||v_name||'</a></td>');
      IF ( MOD(c_dyn_cursor%ROWCOUNT,2) = 0 ) THEN
         htp.p('</tr>');
      END IF;
   END LOOP;
   IF ( MOD(c_dyn_cursor%ROWCOUNT,2) != 0 ) THEN
      htp.p('<td>&nbsp;</td></tr>');
   END IF;
   CLOSE c_dyn_cursor;
ELSIF ( p_command = 'search' ) THEN
   htp.p('<tr><td>Du m� skrive inn noe i s�kefeltet!</td></tr>');
ELSIF ( p_command = 'bartender' ) THEN
   htp.p('<tr><td>Du m� velge minst en ingrediens!</td></tr>');
END IF;

html.e_table;
html.e_box;
htp.p('</td></tr>');
html.e_table;
html.e_page;
END;
END search;

---------------------------------------------------------------------
-- Name: search_form
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 01.03.2002
-- Chng:
---------------------------------------------------------------------
PROCEDURE search_form (  p_string          IN varchar2             DEFAULT NULL,
                         p_ingredient_fk   IN owa_util.ident_arr   DEFAULT empty_array
                    )
IS
BEGIN
DECLARE

v_select_ingredient     LONG                                    DEFAULT select_ingredient;

BEGIN

html.b_box_2(get.txt('search_form_drink'),'100%','drink_guiden.show_drink');
html.b_table;
html.b_form('drink_guiden.search','search');
htp.p('<tr><td>'||get.txt('find_drink_by_name')||'<br>
<input TYPE="text" name="p_string" width="20" value="'||p_string||'"><br>
<input TYPE="hidden" name="p_command" value="search">
<input TYPE="submit" value="'||get.txt('find_drink')||'"></td></tr>');
html.e_form;
html.b_form('drink_guiden.search','bartender');
htp.p('<tr><td>'||get.txt('drinks_with_ingredient')||'<br>');

htp.p(v_select_ingredient||'<br>');
htp.p(v_select_ingredient||'<br>');

htp.p('<input TYPE="hidden" name="p_command" value="bartender">
<input TYPE="radio" name="p_param" value="INTERSECT">'||get.txt('and')||'<br>
<input TYPE="radio" name="p_param" value="UNION" checked>'||get.txt('or')||'
<br>
<input TYPE="submit" value="'||get.txt('find_drink')||'"></td></tr>');
html.e_form;
html.e_table;
html.e_box_2;

END;
END search_form;

---------------------------------------------------------------------
-- Name: browse_menu
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 01.03.2002
-- Chng:
---------------------------------------------------------------------
PROCEDURE browse_menu
IS
BEGIN
html.b_box_2(get.txt('browse_drink'),'100%','drink_guiden.show_drink');
html.b_table;
htp.p('
<tr><td><a href="drink_guiden.browse?p_command=name">'||get.txt('browse_by_name')||'</a></td></tr>
<tr><td><a href="drink_guiden.browse?p_command=glass">'||get.txt('browse_by_glass')||'</a></td></tr>
<tr><td><a href="drink_guiden.browse?p_command=category">'||get.txt('browse_by_category')||'</a></td></tr>
<tr><td><a href="drink_guiden.browse?p_command=ingredient">'||get.txt('browse_by_ingredient')||'</a></td></tr>
<tr><td><a href="drink_guiden.browse?p_command=ing_type">'||get.txt('browse_by_ing_type')||'</a></td></tr>');
html.e_table;
html.e_box_2;
END browse_menu;

---------------------------------------------------------------------
---------------------------------------------------------------------
END;
/
show errors;
