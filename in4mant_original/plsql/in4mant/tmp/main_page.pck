CREATE OR REPLACE PACKAGE main_page IS

        PROCEDURE startup
                (
                        p_service_pk    IN service.service_pk%TYPE      DEFAULT NULL,
                        p_language_pk   IN la.language_pk%TYPE          DEFAULT NULL
                );
        PROCEDURE nor_student
                (
                        p_service_pk    IN service.service_pk%TYPE      DEFAULT NULL,
                        p_language_pk   IN la.language_pk%TYPE          DEFAULT NULL
                );
        PROCEDURE nor_studnews
                (
                        p_service_pk    IN service.service_pk%TYPE      DEFAULT NULL,
                        p_language_pk   IN la.language_pk%TYPE          DEFAULT NULL
                );
	PROCEDURE nor_nightlife
	        (
	                p_service_pk    IN service.service_pk%TYPE      DEFAULT NULL,
	                p_language_pk   IN la.language_pk%TYPE          DEFAULT NULL
	        );
	PROCEDURE nor_finance
	        (
	                p_service_pk    IN service.service_pk%TYPE      DEFAULT NULL,
	                p_language_pk   IN la.language_pk%TYPE          DEFAULT NULL
	        );
        PROCEDURE all_services;
	PROCEDURE admin_service
	        (
	                p_service_pk    IN service.service_pk%TYPE      DEFAULT NULL,
	                p_language_pk   IN la.language_pk%TYPE          DEFAULT NULL
	        );
	PROCEDURE test_service
	        (
	                p_service_pk    IN service.service_pk%TYPE      DEFAULT NULL,
	                p_language_pk   IN la.language_pk%TYPE          DEFAULT NULL
	        );

	PROCEDURE www_leftside;

END;
/
CREATE OR REPLACE PACKAGE BODY main_page IS


---------------------------------------------------------
-- Name:        startup
-- Type:        procedure
-- What:        start procedure, to run the package
-- Author:      Frode Klevstul
-- Start date:  17.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE startup
        (
                p_service_pk    IN service.service_pk%TYPE      DEFAULT NULL,
                p_language_pk   IN la.language_pk%TYPE          DEFAULT NULL
        )
IS
BEGIN
DECLARE

        v_service_pk    service.service_pk%TYPE DEFAULT NULL;
        v_language_pk   la.language_pk%TYPE     DEFAULT NULL;

BEGIN

        IF ( p_service_pk IS NOT NULL AND p_language_pk IS NOT NULL ) THEN
                v_service_pk := p_service_pk;
                v_language_pk := p_language_pk;
        ELSE
                v_service_pk := get.serv;
                v_language_pk := get.lan;
        END IF;

        stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,21,v_service_pk);
        IF ( v_service_pk = 1 ) THEN -- student
                nor_student(v_service_pk, v_language_pk);
        ELSIF (v_service_pk = 5) THEN -- studnyhet
                nor_studnews(v_service_pk, v_language_pk);
        ELSIF (v_service_pk = 3) THEN -- uteliv
                nor_nightlife(v_service_pk, v_language_pk);
        ELSIF (v_service_pk = 2) THEN -- finans
                nor_finance(v_service_pk, v_language_pk);
        ELSIF (v_service_pk = 0) THEN -- www
                all_services;
        ELSIF (v_service_pk = 6) THEN -- admin
                admin_service(v_service_pk, v_language_pk);
        ELSIF (v_service_pk = 7) THEN -- test
                test_service(v_service_pk, v_language_pk);
        ELSE
                all_services;
        END IF;

END;
END startup;



---------------------------------------------------------
-- Name:        nor_student
-- Type:        procedure
-- What:        lists out all elements for the front page (service: nor student service)
-- Author:      Frode Klevstul
-- Start date:  17.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE nor_student
                (
                        p_service_pk    IN service.service_pk%TYPE      DEFAULT NULL,
                        p_language_pk   IN la.language_pk%TYPE          DEFAULT NULL
                )
IS
BEGIN

        html.b_page(NULL, NULL, NULL, 1, 1);
        html.home_menu;
        htp.p('<table cellspacing="0" cellpadding="10">');
        htp.p('<tr><td valign="top" width="10%" align="left">');

        -- ----------------
        -- venstre stolpe
        -- ----------------
	htp.p('<br>');

        login.login_form(NULL, 'main_page', '100%');

	htp.p('<br><br>');

        fora_util.all_fora(p_service_pk,p_language_pk);

	htp.p('<br><br>');

        poll_util.show_poll(p_service_pk,p_language_pk);

	htp.p('<br><br>');

        main.different;

	htp.p('<br><br>');

        main.advertising;

	htp.p('<br><br>');

        htp.p('</td><td valign="top" align="right">');

	-- ----------------
        -- h�yre stolpe
	-- ----------------


	htp.p('<br>');

        main.service_info(p_service_pk);

	htp.p('<br><br><br><br>');

        doc_util.doc_serv(p_service_pk,p_language_pk);

	htp.p('<br><br><br><br>');

        cal_util.cal_serv(p_service_pk,p_language_pk);

	htp.p('<br><br><br><br>');

        photo.user_last_comment;

	htp.p('<br><br><br><br>');

        photo.user_list_albums(NULL, 1);

	htp.p('<br><br><br><br>');

        photo.user_last_pictures;

	htp.p('<br><br><br><br>');

        main.the_line;

	htp.p('<br><br><br><br>');

        tip.someone;

	htp.p('<br><br><br><br>');

	-- -------
	-- bunn
	-- -------
        htp.p('</td></tr></table>');
	html.e_page;


END nor_student;





---------------------------------------------------------
-- Name:        nor_studnews
-- Type:        procedure
-- What:        lists out all elements for the front page (service: nor student-news service)
-- Author:      Frode Klevstul
-- Start date:  29.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE nor_studnews
        (
                p_service_pk    IN service.service_pk%TYPE      DEFAULT NULL,
                p_language_pk   IN la.language_pk%TYPE          DEFAULT NULL
        )
IS
BEGIN

        html.b_page(NULL, NULL, NULL, 1, 1);
        html.home_menu;
        htp.p('<table cellspacing="0" cellpadding="10">');
        htp.p('<tr><td valign="top" width="10%" align="left" bgcolor="'|| get.value( 'c_'|| get.serv_name ||'_bbm' ) ||'">');

        -- venstre stolpe
        html.b_table;
        htp.p('<tr><td valign="top">');
        login.login_form(NULL, 'main_page', '100%');
        htp.p('<br></td></tr>');
        htp.p('<tr><td valign="top">');
        fora_util.all_fora(p_service_pk,p_language_pk);
        htp.p('	<br></td></tr>
         	<tr><td valign="top">
        ');
        poll_util.show_poll(p_service_pk,p_language_pk);
        htp.p('<br></td></tr>');
        htp.p('<tr><td valign="top">');
        main.different;
        htp.p('</td></tr>
        <tr><td valign="top" align="center">');
        main.advertising;
        htp.p('</td></tr>');
        html.e_table;

        htp.p('</td><td valign="top" align="right">');

        -- h�yre stolpe
        html.b_table('100%', 0);
        htp.p('<tr><td valign="top">');
        main.service_info(p_service_pk);
        htp.p('<br><br></td></tr>
        <tr><td valign="top">');
        doc_util.doc_serv(p_service_pk,p_language_pk);
        htp.p('<br><br></td></tr>
        <tr><td valign="top">');
        cal_util.cal_serv(p_service_pk,p_language_pk);
        htp.p('<br><br></td></tr>');
        htp.p('<tr><td>');
        tip.someone;
        htp.p('</td></tr>');
        html.e_table;

	-- bunn
        htp.p('</td></tr></table>');
	html.e_page;


END nor_studnews;



---------------------------------------------------------
-- Name:        nor_nightlife
-- Type:        procedure
-- What:        lists out all elements for the front page (service: nor lightlife service)
-- Author:      Frode Klevstul
-- Start date:  23.11.2000
-- Desc:
---------------------------------------------------------
PROCEDURE nor_nightlife
        (
                p_service_pk    IN service.service_pk%TYPE      DEFAULT NULL,
                p_language_pk   IN la.language_pk%TYPE          DEFAULT NULL
        )
IS
BEGIN

        html.b_page(NULL, NULL, NULL, 1, 1);
        html.home_menu;
        htp.p('<table cellspacing="0" cellpadding="10">');
        htp.p('<tr><td valign="top" width="10%" align="left">');

	-- ---------------
        -- venstre stolpe
        -- ---------------
	htp.p('<br>');

        login.login_form(NULL, 'main_page', '100%');

	htp.p('<br><br>');

        fora_util.all_fora(p_service_pk,p_language_pk);

	htp.p('<br><br>');

        poll_util.show_poll(p_service_pk,p_language_pk);

	htp.p('<br><br>');

        main.different;

	htp.p('<br><br>');

        main.advertising;

	htp.p('<br><br>');

        htp.p('</td><td valign="top" align="right">');

	-- -----------------
        -- h�yre stolpe
        -- -----------------
	htp.p('<br>');

        main.service_info(p_service_pk);

	htp.p('<br><br><br><br>');

        doc_util.doc_serv(p_service_pk,p_language_pk);

	htp.p('<br><br><br><br>');

        cal_util.cal_serv(p_service_pk,p_language_pk);

	htp.p('<br><br><br><br>');

        photo.user_last_comment;

	htp.p('<br><br><br><br>');

        photo.user_list_albums(NULL, 1);

	htp.p('<br><br><br><br>');

        photo.user_last_pictures;

	htp.p('<br><br><br><br>');

        main.the_line;

	htp.p('<br><br><br><br>');

        tip.someone;

	htp.p('<br><br><br><br>');

	-- ----------
	-- bunn
	-- ----------
        htp.p('</td></tr></table>');
	html.e_page;

END nor_nightlife;


---------------------------------------------------------
-- Name:        nor_finance
-- Type:        procedure
-- What:        lists out all elements for the front page (service: nor finance service)
-- Author:      Frode Klevstul
-- Start date:  02.12.2000
-- Desc:
---------------------------------------------------------
PROCEDURE nor_finance
        (
                p_service_pk    IN service.service_pk%TYPE      DEFAULT NULL,
                p_language_pk   IN la.language_pk%TYPE          DEFAULT NULL
        )
IS
BEGIN

        html.b_page(NULL, NULL, NULL, 1, 1);
        htp.p(' <table cellspacing="0" cellpadding="10">');
        htp.p('<tr><td valign="top" width="10%" align="left" bgcolor="#ffffff">');

        -- venstre stolpe
        html.b_table;
        htp.p('<tr><td valign="top">');
        login.login_form(NULL, 'main_page');
        htp.p('<br></td></tr>');
        htp.p('<tr><td valign="top">');
        fora_util.all_fora(p_service_pk,p_language_pk);
        htp.p('	<br></td></tr>
         	<tr><td valign="top">
        ');
        poll_util.show_poll(p_service_pk,p_language_pk);
        htp.p('<br></td></tr>');
        htp.p('<tr><td valign="top">');
        main.different;
        htp.p('</td></tr>
        <tr><td valign="top" align="center">');
        main.advertising;
        htp.p('</td></tr>');
        html.e_table;
        htp.p('</td><td valign="top" align="right">');

        -- h�yre stolpe
        html.b_table('100%', 0);
        htp.p('<tr><td valign="top">');
        main.service_info(p_service_pk);
        htp.p('<br></td></tr>
        <tr><td valign="top">');
        doc_util.doc_serv(p_service_pk,p_language_pk);
        htp.p('<br></td></tr>
        <tr><td valign="top">');
        cal_util.cal_serv(p_service_pk,p_language_pk);
        htp.p('<br></td></tr>');
        htp.p('<tr><td valign="top">');
        photo.user_last_comment;
        htp.p('	<br></td></tr>
           	<tr><td valign="top">
        ');
        photo.user_list_albums(NULL, 1);
        htp.p('	<br></td></tr>
           	<tr><td valign="top">
        ');
        photo.user_last_pictures;
        htp.p('	</td></tr>
           	<tr><td valign="top">
        ');
        main.the_line;
        htp.p('</td></tr>');
        html.e_table;

	-- bunn
        htp.p('</td></tr>
        <tr><td colspan="2">');
        tip.someone;
        htp.p('</td></tr>');
        html.e_table;


        html.e_page;

END nor_finance;


---------------------------------------------------------
-- Name:        all_services
-- Type:        procedure
-- What:        writes out front page for www domain
-- Author:      Frode Klevstul
-- Start date:  02.11.2000
-- Desc:
---------------------------------------------------------
PROCEDURE all_services
IS
BEGIN

	htp.p('
		<HTML><HEAD><TITLE>in4mant.com</TITLE></HEAD>
		<frameset cols="*,750,*" border="0" frameborder="0">
			<FRAME name="" src="main.plain" scrolling="no" noresize>
			<FRAMESET rows=140,350,* frameborder="no">
				<FRAME name="" src="html.top?p_type=3" scrolling="no" noresize marginwidth="0">
				<FRAMESET rows=*,40>
					<FRAMESET cols=250,*>
						<FRAME name="" src="main_page.www_leftside" scrolling="no" noresize>
						<FRAME name="" src="main.plain" scrolling="no" noresize>
					</FRAMESET>
					<FRAME name="" src="html.bottom" scrolling="no" noresize>
				</FRAMESET>
				<FRAME name="" src="main.plain" scrolling="no" noresize>
			</FRAMESET>
			<FRAME name="" src="main.plain" scrolling="no" noresize>
		</frameset>
		</HTML>
	');

END all_services;






---------------------------------------------------------
-- Name:        admin_service
-- Type:        procedure
-- What:        lists out all elements for the front page (service: admin service)
-- Author:      Frode Klevstul
-- Start date:  16.01.2000
-- Desc:
---------------------------------------------------------
PROCEDURE admin_service
        (
                p_service_pk    IN service.service_pk%TYPE      DEFAULT NULL,
                p_language_pk   IN la.language_pk%TYPE          DEFAULT NULL
        )
IS
BEGIN

        html.b_page(NULL, NULL, NULL, 2, 1);
        htp.p('<table cellspacing="0" cellpadding="10">');
        htp.p('<tr><td valign="top" width="10%" align="left" bgcolor="'|| get.value( 'c_'|| get.serv_name ||'_bbm' ) ||'">');

        -- venstre stolpe
        html.b_table;
        htp.p('<tr><td valign="top">');
        login.login_form(NULL, 'main_page', '100%');
        htp.p('<br></td></tr>');
        htp.p('<tr><td valign="top">');
        fora_util.all_fora(p_service_pk,p_language_pk);
        htp.p('	<br></td></tr>
         	<tr><td valign="top">
        ');
        poll_util.show_poll(p_service_pk,p_language_pk);
        htp.p('<br></td></tr>');
        html.e_table;

        htp.p('</td><td valign="top" align="right">');

        -- h�yre stolpe
        html.b_table('100%', 0);
        htp.p('<tr><td valign="top">');
        main.service_info(p_service_pk);
        htp.p('<br><br></td></tr>');
        html.e_table;

	-- bunn
        htp.p('</td></tr></table>');
	html.e_page;

END admin_service;




---------------------------------------------------------
-- Name:        test_service
-- Type:        procedure
-- What:        lists out all elements for the front page (service: test service)
-- Author:      Frode Klevstul
-- Start date:  28.01.2001
-- Desc:
---------------------------------------------------------
PROCEDURE test_service
                (
                        p_service_pk    IN service.service_pk%TYPE      DEFAULT NULL,
                        p_language_pk   IN la.language_pk%TYPE          DEFAULT NULL
                )
IS
BEGIN

        html.b_page(NULL, NULL, NULL, 1, 1);
        html.home_menu;
        htp.p('<table cellspacing="0" cellpadding="10">');
        htp.p('<tr><td valign="top" width="10%" align="left">');

        -- venstre stolpe
--        html.b_table;
--        htp.p('<tr><td valign="top">');
htp.p('<br>');
        login.login_form(NULL, 'main_page', '100%');
htp.p('<br><br>');
--        htp.p('<br></td></tr>');
--        htp.p('<tr><td valign="top">');
        fora_util.all_fora(p_service_pk,p_language_pk);
htp.p('<br><br>');
--        htp.p('	<br></td></tr>
--         	<tr><td valign="top">
--        ');
        poll_util.show_poll(p_service_pk,p_language_pk);
htp.p('<br><br>');
--        htp.p('<br></td></tr>');
--        htp.p('<tr><td valign="top">');
        main.different;
htp.p('<br><br>');
--        htp.p('</td></tr>
--        <tr><td valign="top" align="center">');
        main.advertising;
htp.p('<br><br>');
--        htp.p('</td></tr>');
--        html.e_table;

        htp.p('</td><td valign="top" align="right">');

        -- h�yre stolpe
--        html.b_table('100%', 0);
--        htp.p('<tr><td valign="top">');
--        main.service_info(p_service_pk);
htp.p('<br>');
--        htp.p('<br><br></td></tr>
--        <tr><td valign="top">');
        doc_util.doc_serv(p_service_pk,p_language_pk);
htp.p('<br><br><br><br>');
--        htp.p('<br><br></td></tr>
--        <tr><td valign="top">');
        cal_util.cal_serv(p_service_pk,p_language_pk);
htp.p('<br><br><br><br>');
--        htp.p('<br><br></td></tr>
--        <tr><td valign="top">');
        photo.user_last_comment;
htp.p('<br><br><br><br>');
--        htp.p('	<br><br></td></tr>
--           	<tr><td valign="top">
--        ');
        photo.user_list_albums(NULL, 1);
htp.p('<br><br><br><br>');
--        htp.p('	<br><br></td></tr>
--           	<tr><td valign="top">
--        ');
        photo.user_last_pictures;
htp.p('<br><br><br><br>');
--        htp.p('	<br><br></td></tr>
--           	<tr><td valign="top">
--        ');
        main.the_line;
htp.p('<br><br><br><br>');
--        htp.p('<br><br></td></tr><tr><td>');
        tip.someone;
htp.p('<br><br><br><br>');
--        htp.p('</td></tr>');
--        html.e_table;
	-- bunn
        htp.p('</td></tr></table>');
	html.e_page;


END test_service;




---------------------------------------------------------
-- Name:        www_leftside
-- Type:        procedure
-- What:        the content on the left side of www/front page
-- Author:      Frode Klevstul
-- Start date:  14.03.2001
-- Desc:
---------------------------------------------------------
PROCEDURE www_leftside
IS
BEGIN

	html.b_page_3(NULL, NULL, get.value('c_'|| get.serv_name ||'_lpb'));

	htp.p('

		<SCRIPT language=JavaScript>
		<!-- Begin
		function changeText(service){
			function makearray(x) {
				this.length = x
				for (var i=1; i<= x; i += 1){
					this[i] = 0;
				}
			}

			the_text = new makearray(6);

			the_text[0] = "'|| get.txt('welcome_to_in4mant_com_text') ||'";
			the_text[1] = "'|| get.txt('nor_student_in4mant_desc') ||'";
			the_text[2] = "'|| get.txt('nor_nightlife_in4mant_desc') ||'";

			the_text[100] = "'|| get.txt('welcome_to_in4mant_com') ||'";
			the_text[101] = "'|| get.txt('student.in4mant.com') ||'";
			the_text[102] = "'|| get.txt('uteliv.in4mant.com') ||'";

			window.parent.frames[3].document.open();
			window.parent.frames[3].document.write("<body bgcolor='|| get.value('c_'|| get.serv_name ||'_bbs' ) ||'>");
			window.parent.frames[3].document.write("<font face=Arial,Helvetica,San-Serif size=-2>");
			window.parent.frames[3].document.write("<p><font size=+1><b>" + the_text[service+100] + "</b></font></p>");
			window.parent.frames[3].document.write(the_text[service]);
			window.parent.frames[3].document.write("</font>");
			window.parent.frames[3].document.write("</body>");
			window.parent.frames[3].document.close();
			return;
		}
		// End -->
		</SCRIPT>

		<body onload="javascript:changeText(0)">
		<table border="0" width="225">
		<tr><td align="center">
		<br><br><br>
		<A onmouseover="changeText(1); return true" onmouseout="changeText(0); return true" href="http://'|| get.domain(1, get.lan) ||'/cgi-bin/main_page.startup" target="_top">'|| get.txt('nor_student_button') ||'</a>
		<br>
		<A onmouseover="changeText(2); return true" onmouseout="changeText(0); return true" href="http://'|| get.domain(3, get.lan) ||'/cgi-bin/main_page.startup" target="_top">'|| get.txt('nor_nightlife_button') ||'</a>
		</td></tr>
		</table>
	');

	html.e_page_3;

END www_leftside;


-- ++++++++++++++++++++++++++++++++++++++++++++++ --


END; -- slutter pakke kroppen
/
