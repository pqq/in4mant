CREATE OR REPLACE PACKAGE doc_util IS

PROCEDURE startup;
PROCEDURE test(     p_document_pk           IN document.document_pk%TYPE                    DEFAULT NULL,
                    p_ad                    IN VARCHAR2                                     DEFAULT NULL
                               ) ;

PROCEDURE doc_archive   (       p_organization_pk       IN organization.organization_pk%TYPE            DEFAULT NULL,
                                p_language_pk           IN document.language_fk%TYPE                    DEFAULT NULL,
                                p_service_pk            IN service.service_pk%TYPE                      DEFAULT NULL,
                                p_new_date              IN VARCHAR2                                     DEFAULT to_char(SYSDATE,get.txt('date_long')),
                                p_old_date              IN VARCHAR2                                     DEFAULT NULL,
                                p_count                 IN NUMBER                                       DEFAULT 1,
                                p_my_archive            IN NUMBER                                       DEFAULT NULL,
                                p_document_pk           IN document.document_pk%TYPE                    DEFAULT NULL
                        );
PROCEDURE show_document (       p_document_pk           IN document.document_pk%TYPE                    DEFAULT NULL,
                                p_search                IN VARCHAR2                                     DEFAULT NULL,
                                p_command               IN VARCHAR2                                     DEFAULT NULL );
PROCEDURE doc_serv(     p_service_pk            IN service.service_pk%TYPE      DEFAULT NULL,
                        p_language_pk           IN document.language_fk%TYPE    DEFAULT NULL );

FUNCTION return_document (     p_document_pk           IN document.document_pk%TYPE                    DEFAULT NULL,
                               p_ad                    IN VARCHAR2                                     DEFAULT NULL
                               ) RETURN long;
PROCEDURE show_mail_document (       p_document_pk   IN document.document_pk%TYPE    DEFAULT NULL );
END;
/
CREATE OR REPLACE PACKAGE BODY doc_util IS
---------------------------------------------------------------------
---------------------------------------------------------------------
-- Name: startup
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 15.07.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE startup
IS
BEGIN
        doc_archive;
END startup;

PROCEDURE test(     p_document_pk           IN document.document_pk%TYPE                    DEFAULT NULL,
                    p_ad                    IN VARCHAR2                                     DEFAULT NULL
              )
IS
BEGIN
        htp.p( return_document(p_document_pk, p_ad ) );
END test;

---------------------------------------------------------------------
-- Name: doc_archive
-- Type: procedure
-- What: Genererer dokumentarkiv
-- Made: Espen Messel
-- Date: 15.07.2000
-- Chng:
--      26.08.00        -       Frode Klevstul  - la til MY_ARCHIVE
---------------------------------------------------------------------

PROCEDURE doc_archive   (       p_organization_pk       IN organization.organization_pk%TYPE            DEFAULT NULL,
                                p_language_pk           IN document.language_fk%TYPE                    DEFAULT NULL,
                                p_service_pk            IN service.service_pk%TYPE                      DEFAULT NULL,
                                p_new_date              IN VARCHAR2                                     DEFAULT to_char(SYSDATE,get.txt('date_long')),
                                p_old_date              IN VARCHAR2                                     DEFAULT NULL,
                                p_count                 IN NUMBER                                       DEFAULT 1,
                                p_my_archive            IN NUMBER                                       DEFAULT NULL,
                                p_document_pk           IN document.document_pk%TYPE                    DEFAULT NULL
                        )
IS
BEGIN
DECLARE

   v_service_pk            service.service_pk%TYPE                 DEFAULT NULL;
   v_publish_date          document.publish_date%TYPE              DEFAULT NULL;
   v_publish_date2         document.publish_date%TYPE              DEFAULT NULL;
   v_heading               document.heading%TYPE                   DEFAULT NULL;
   v_ingress               document.ingress%TYPE                   DEFAULT NULL;
   v_file_path             document.file_path%TYPE                 DEFAULT NULL;
   v_document_name         document.document_name%TYPE             DEFAULT NULL;
   v_document_pk           document.document_pk%TYPE               DEFAULT NULL;
   v_name                  organization.name%TYPE                  DEFAULT NULL;
   v_count                 NUMBER                                  DEFAULT NULL;
   v_count2                NUMBER                                  DEFAULT NULL;
   v_count3                NUMBER                                  DEFAULT NULL;
   v_doc_arc_nr            NUMBER                                  DEFAULT NULL;
   v_organization_pk       organization.organization_pk%TYPE       DEFAULT NULL;
   v_language_pk           document.language_fk%TYPE               DEFAULT NULL;
   v_user_pk               usr.user_pk%TYPE                        DEFAULT NULL;
   v_org_menu              VARCHAR2(1000)                          DEFAULT NULL;
v_geography_pk		   organization.geography_fk%TYPE	   DEFAULT NULL;

        CURSOR  document_archive IS
        SELECT  d.publish_date, d.heading, d.ingress, d.file_path,
                d.document_name, d.document_pk, o.name, o.organization_pk
        FROM    document d, document_type dt, organization o
        WHERE   d.document_type_fk=dt.document_type_pk
        AND     d.language_fk=v_language_pk
        AND     d.organization_fk = p_organization_pk
        AND     d.organization_fk = o.organization_pk
        AND     d.publish_date < to_date(trans(p_new_date),get.txt('date_long'))
        AND     d.publish_date < SYSDATE
        AND     d.accepted > 0
        AND     dt.service_fk = v_service_pk
        OR      d.document_type_fk=dt.document_type_pk
        AND     d.language_fk=v_language_pk
        AND     d.organization_fk = p_organization_pk
        AND     d.organization_fk = o.organization_pk
        AND     d.publish_date = to_date(trans(p_new_date),get.txt('date_long'))
        AND     d.publish_date < SYSDATE
        AND     d.accepted > 0
        AND     dt.service_fk=v_service_pk
        AND     d.document_pk > p_document_pk
        ORDER BY d.publish_date DESC, d.document_pk
        ;

        CURSOR  document_archive_all IS
        SELECT  d.publish_date, d.heading, d.ingress, d.file_path,
                d.document_name, d.document_pk, o.name, o.organization_pk, o.geography_fk
        FROM    document d, document_type dt, organization o
        WHERE   d.document_type_fk=dt.document_type_pk
        AND     d.language_fk=v_language_pk
        AND     d.organization_fk = o.organization_pk
        AND     d.publish_date < to_date(trans(p_new_date),get.txt('date_long'))
        AND     d.publish_date < SYSDATE
        AND     d.accepted > 0
        AND     dt.service_fk=v_service_pk
        OR      d.document_type_fk=dt.document_type_pk
        AND     d.language_fk=v_language_pk
        AND     d.organization_fk = o.organization_pk
        AND     d.publish_date = to_date(trans(p_new_date),get.txt('date_long'))
        AND     d.publish_date < SYSDATE
        AND     d.accepted > 0
        AND     dt.service_fk=v_service_pk
        AND     d.document_pk > p_document_pk
        ORDER BY d.publish_date DESC, d.document_pk
        ;

        CURSOR  my_document_archive(v_user_pk in usr.user_pk%type) IS
        SELECT  d.publish_date, d.heading, d.ingress, d.file_path,
                d.document_name, d.document_pk, o.name, o.organization_pk, o.geography_fk
        FROM    document d, document_type dt, organization o
        WHERE   d.document_type_fk=dt.document_type_pk
        AND     d.organization_fk = o.organization_pk
        AND     d.publish_date < to_date(trans(p_new_date),get.txt('date_long'))
        AND     d.publish_date < SYSDATE
        AND     d.accepted > 0
        AND     dt.service_fk=v_service_pk
        AND     (o.organization_pk, d.language_fk) IN
                (
                        SELECT  DISTINCT organization_fk, lu.language_fk
                        FROM    list_org lo, list l, list_user lu, list_type lt
                        WHERE   lo.list_fk = l.list_pk
                        AND     lt.list_type_pk = l.list_type_fk
                        AND     lu.user_fk = v_user_pk
                        AND
                        (
                                lu.list_fk = l.list_pk
                        OR      lu.list_fk = l.level0
                        OR      lu.list_fk = l.level1
                        OR      lu.list_fk = l.level2
                        OR      lu.list_fk = l.level3
                        OR      lu.list_fk = l.level4
                        OR      lu.list_fk = l.level5
                        )
                )
        OR      d.document_type_fk=dt.document_type_pk
        AND     d.organization_fk = o.organization_pk
        AND     d.publish_date = to_date(trans(p_new_date),get.txt('date_long'))
        AND     d.publish_date < SYSDATE
        AND     d.accepted > 0
        AND     dt.service_fk=v_service_pk
        AND     d.document_pk > p_document_pk
        AND     (o.organization_pk, d.language_fk) IN
                (
                        SELECT  DISTINCT organization_fk, lu.language_fk
                        FROM    list_org lo, list l, list_user lu, list_type lt
                        WHERE   lo.list_fk = l.list_pk
                        AND     lt.list_type_pk = l.list_type_fk
                        AND     lu.user_fk = v_user_pk
                        AND
                        (
                                lu.list_fk = l.list_pk
                        OR      lu.list_fk = l.level0
                        OR      lu.list_fk = l.level1
                        OR      lu.list_fk = l.level2
                        OR      lu.list_fk = l.level3
                        OR      lu.list_fk = l.level4
                        OR      lu.list_fk = l.level5
                        )
                )
        ORDER BY d.publish_date DESC, d.document_pk
        ;


BEGIN
IF ( p_service_pk IS NULL ) THEN
   v_service_pk := get.serv;
ELSE
   v_service_pk := p_service_pk;
END IF;

IF ( p_organization_pk IS NOT NULL ) THEN
      html.b_page(NULL,NULL,NULL,NULL,3);
      html.org_menu(p_organization_pk);
      -- Legger inn i statistikk tabellen
      stat.reg(p_organization_pk,NULL,NULL,NULL,NULL,NULL,19,v_service_pk);
ELSIF (p_my_archive IS NOT NULL) THEN
      html.b_page;
      html.my_menu;
      -- Legger inn i statistikk tabellen
      stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,28,v_service_pk);
ELSE
      html.b_page(NULL, NULL, NULL, NULL, 1);
      html.home_menu;
      -- Legger inn i statistikk tabellen
      stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,21,v_service_pk);
END IF;


   IF ( p_language_pk IS NULL ) THEN
      v_language_pk := get.lan;
   ELSE
      v_language_pk := p_language_pk;
   END IF;

   html.b_box( get.txt('document_archive'), '100%', 'org_page.doc' );
   html.b_table('100%');

   v_doc_arc_nr := get.value('doc_arc_nr');


   IF ( p_organization_pk IS NULL AND p_language_pk IS NULL AND p_my_archive IS NULL) THEN
	        html.b_form('doc_util.doc_archive');
                htp.p('<tr><td>');
                html.select_service;
                htp.p('</td><td>');
                html.select_lang;
                htp.p('</td><td>');
                html.submit_link( get.txt('Show_archive') );
                htp.p('</td></tr>');
                html.e_form;
        ELSE
                IF ( p_organization_pk IS NOT NULL ) THEN

                        SELECT  count(*)
                        INTO    v_count
                        FROM    document d, document_type dt, organization o
                        WHERE   d.document_type_fk=dt.document_type_pk
                        AND     d.language_fk=v_language_pk
                        AND     d.organization_fk = p_organization_pk
                        AND     d.organization_fk = o.organization_pk
                        AND     d.publish_date < SYSDATE
                        AND     d.accepted > 0
                        AND     dt.service_fk=v_service_pk
                        ;

                        htp.p('<tr><td align="left"><b>'||get.txt('date')||':</b></td><td colspan="2"><b>'||get.txt('subject')||':</b></td></tr>');
                        OPEN document_archive;
                        WHILE ( document_archive%ROWCOUNT < v_doc_arc_nr )
                        LOOP
                                FETCH document_archive INTO v_publish_date, v_heading, v_ingress,
                                        v_file_path, v_document_name, v_document_pk,
                                        v_name, v_organization_pk;
                                exit when document_archive%NOTFOUND;
                                IF ( document_archive%ROWCOUNT = 1 ) THEN
                                        v_publish_date2 := v_publish_date;
                                END IF;
                                htp.p('<tr><td width="20%" align="left">'|| to_char(v_publish_date,get.txt('date_long')) ||':</td>
                                <td width="80%" align="left" colspan="2"><a href="doc_util.show_document?p_document_pk='||v_document_pk||'">'|| v_heading ||'</a></td></tr>
                                ');
        --                      <td width="20%" align="left" valign="top"><a href="org_page.main?p_organization_pk='||v_organization_pk||'">'|| v_name ||'</a></td></tr>
        --                      <tr><td colspan="3">'|| v_ingress ||'</td></tr>
                        END LOOP;
                        CLOSE document_archive;

                ELSIF(p_my_archive IS NULL) then

                        SELECT  count(*)
                        INTO    v_count
                        FROM    document d, document_type dt, organization o
                        WHERE   d.document_type_fk=dt.document_type_pk
                        AND     d.language_fk=v_language_pk
                        AND     d.organization_fk = o.organization_pk
                        AND     d.publish_date < SYSDATE
                        AND     d.accepted > 0
                        AND     dt.service_fk=v_service_pk
                        ;
                        htp.p('<tr><td align="left"><b>'||get.txt('date')||':</b></td><td><b>'||get.txt('subject')||':</b></td><td><b>'||get.txt('source')||':</b></td><td><b>'||get.txt('geography_location')||':</b></td></tr>');
                        OPEN document_archive_all;
                        WHILE ( document_archive_all%ROWCOUNT < v_doc_arc_nr )
                        LOOP
                                FETCH document_archive_all INTO v_publish_date, v_heading, v_ingress,
                                        v_file_path, v_document_name, v_document_pk,
                                        v_name, v_organization_pk, v_geography_pk;
                                exit when document_archive_all%NOTFOUND;
                                IF ( document_archive_all%ROWCOUNT = 1 ) THEN
                                        v_publish_date2 := v_publish_date;
                                END IF;
                                htp.p('<tr><td width="15%" align="left">'|| to_char(v_publish_date,get.txt('date_long')) ||':</td>
                                <td width="55%" align="left"><a href="doc_util.show_document?p_document_pk='||v_document_pk||'">'|| v_heading ||'</a></td>
                                <td width="15%" align="left" valign="top"><a href="org_page.main?p_organization_pk='||v_organization_pk||'">'|| v_name ||'</a></td>
                        <td width="15%" align="left">'|| get.locn(v_geography_pk) ||'</a></td></tr>
                                ');
                                --<tr><td colspan="3">'|| v_ingress ||'</td></tr>
                        END LOOP;
                        CLOSE document_archive_all;
                ELSE
                        v_user_pk := get.uid;

                        SELECT  count(*)
                        INTO    v_count
                        FROM    document d, document_type dt, organization o
                        WHERE   d.document_type_fk=dt.document_type_pk
                        AND     d.language_fk=v_language_pk
                        AND     d.organization_fk = o.organization_pk
                        AND     d.publish_date < SYSDATE
                        AND     d.accepted > 0
                        AND     dt.service_fk=v_service_pk
                        AND     (o.organization_pk, d.language_fk) IN
                                (
                                        SELECT  DISTINCT organization_fk, lu.language_fk
                                        FROM    list_org lo, list l, list_user lu, list_type lt
                                        WHERE   lo.list_fk = l.list_pk
                                        AND     lt.list_type_pk = l.list_type_fk
                                        AND     lu.user_fk = v_user_pk
                                        AND
                                        (
                                                lu.list_fk = l.list_pk
                                        OR      lu.list_fk = l.level0
                                        OR      lu.list_fk = l.level1
                                        OR      lu.list_fk = l.level2
                                        OR      lu.list_fk = l.level3
                                        OR      lu.list_fk = l.level4
                                        OR      lu.list_fk = l.level5
                                        )
                                )
                        ;
                        htp.p('<tr><td align="left"><b>'||get.txt('date')||':</b></td><td><b>'||get.txt('subject')||':</b></td><td><b>'||get.txt('source')||':</b></td><td><b>'||get.txt('geography_location')||':</b></td></tr>');
                        OPEN my_document_archive(v_user_pk);
                        WHILE ( my_document_archive%ROWCOUNT < v_doc_arc_nr )
                        LOOP
                                FETCH my_document_archive INTO v_publish_date, v_heading, v_ingress,
                                        v_file_path, v_document_name, v_document_pk,
                                        v_name, v_organization_pk, v_geography_pk;
                                exit when my_document_archive%NOTFOUND;
                                IF ( my_document_archive%ROWCOUNT = 1 ) THEN
                                        v_publish_date2 := v_publish_date;
                                END IF;
                                htp.p('<tr><td width="15%" align="left">'|| to_char(v_publish_date,get.txt('date_long')) ||':</td>
                                <td width="55%" align="left"><a href="doc_util.show_document?p_document_pk='||v_document_pk||'">'|| v_heading ||'</a></td>
                                <td width="15%" align="left" valign="top"><a href="org_page.main?p_organization_pk='||v_organization_pk||'">'|| v_name ||'</a></td>
				<td width="15%" align="left">'|| get.locn(v_geography_pk) ||'</a></td></tr>
                                ');
                        END LOOP;
                        CLOSE my_document_archive;
                END IF;

                htp.p('<tr><td colspan="3">&nbsp;</td></tr>');
                IF ( p_old_date IS NOT NULL ) THEN
                        v_count3 := p_count-v_doc_arc_nr;
                        html.b_form('doc_util.doc_archive','prev');
                        htp.p('<input type="hidden" name="p_service_pk" value="'|| v_service_pk ||'">
                        <input type="hidden" name="p_my_archive" value="'|| p_my_archive ||'">
                        <input type="hidden" name="p_language_pk" value="'|| v_language_pk ||'">
                        <input type="hidden" name="p_organization_pk" value="'|| p_organization_pk ||'">
                        <input type="hidden" name="p_count" value="'|| v_count3 ||'">
                        <input type="hidden" name="p_new_date" value="'|| SUBSTR(trans(p_old_date),LENGTH(trans(p_old_date))-LENGTH(to_char(SYSDATE,get.txt('date_long')))+1,LENGTH(to_char(SYSDATE,get.txt('date_long')))+1) ||'">
                        <input type="hidden" name="p_old_date" value="'|| SUBSTR(trans(p_old_date),0,LENGTH(trans(p_old_date))-(LENGTH(to_char(SYSDATE,get.txt('date_long')))+2)) ||'">');
                        htp.p('<tr><td align="center" valign="bottom">');
                        html.submit_link( get.txt('previous')||' '||v_doc_arc_nr,'prev' );
                        htp.p('&nbsp;</td>');
                        html.e_form;
                ELSE
                        htp.p('<tr><td align="center" valign="bottom">&nbsp;</td>');
                END IF;
                v_count2 := p_count+v_doc_arc_nr-1;
                IF ( v_count2 > v_count ) THEN
                        htp.p('<td valign="center" align="center">'|| p_count ||'-'|| v_count ||'/'|| v_count ||'</td>');
                ELSE
                        htp.p('<td valign="center" align="center">'|| p_count ||'-'|| v_count2 ||'/'|| v_count ||'</td>');
                END IF;
                IF ( v_count2 < v_count ) THEN
                        v_count3 := p_count+v_doc_arc_nr;
                        IF ( v_count - v_count2 < v_doc_arc_nr ) THEN
                                v_doc_arc_nr := v_count - v_count2;
                        END IF;
                        html.b_form('doc_util.doc_archive','next');
                        htp.p('<input type="hidden" name="p_service_pk" value="'|| v_service_pk ||'">
                        <input type="hidden" name="p_my_archive" value="'|| p_my_archive ||'">
                        <input type="hidden" name="p_language_pk" value="'|| v_language_pk ||'">
                        <input type="hidden" name="p_document_pk" value="'|| v_document_pk ||'">
                        <input type="hidden" name="p_organization_pk" value="'|| p_organization_pk ||'">
                        <input type="hidden" name="p_count" value="'|| v_count3 ||'">
                        <input type="hidden" name="p_new_date" value="'|| to_char( v_publish_date,get.txt('date_long')) ||'">
                        <input type="hidden" name="p_old_date" value="'|| trans(p_old_date) ||';;'|| trans(p_new_date) ||'">');
                        htp.p('<td align="center" valign="top">&nbsp;');
                        html.submit_link( get.txt('next')||' '||v_doc_arc_nr,'next');
                        htp.p('</td></tr>');
                        html.e_form;
                ELSE
                        htp.p('<td align="center" valign="top">&nbsp;</td></tr>');
                END IF;
                IF ( p_organization_pk IS NOT NULL ) THEN
                        IF ( v_name IS NULL ) THEN
                                SELECT  name
                                INTO    v_name
                                FROM    organization
                                WHERE   organization_pk = p_organization_pk;
                        END IF;
                        htp.p('
        <tr><td colspan="3">&nbsp;</td></tr>
        <tr><td align="center" colspan="3">
        <a href="org_page.main?p_organization_pk='|| p_organization_pk ||'">['||v_name||']</a>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="/">['||get.domain||']</a>
        </td></tr>
        <tr><td colspan="3">&nbsp;</td></tr>
        ');
                END IF;
        END IF;
        html.e_table;
        html.e_box;
        html.e_page;

END;
END doc_archive;


---------------------------------------------------------------------
-- Name: show_document
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE show_document (       p_document_pk   IN document.document_pk%TYPE    DEFAULT NULL,
                                p_search        IN VARCHAR2                     DEFAULT NULL,
                                p_command       IN VARCHAR2                     DEFAULT NULL )
IS
BEGIN
DECLARE

v_heading               document.heading%TYPE                   DEFAULT NULL;
v_ingress               document.ingress%TYPE                   DEFAULT NULL;
v_main1                 document.main1%TYPE                     DEFAULT NULL;
v_main2                 document.main2%TYPE                     DEFAULT NULL;
v_main3                 document.main3%TYPE                     DEFAULT NULL;
v_main4                 document.main4%TYPE                     DEFAULT NULL;
v_main5                 document.main5%TYPE                     DEFAULT NULL;
v_main6                 document.main6%TYPE                     DEFAULT NULL;
v_main7                 document.main7%TYPE                     DEFAULT NULL;
v_main8                 document.main8%TYPE                     DEFAULT NULL;
v_main9                 document.main9%TYPE                     DEFAULT NULL;
v_main10                document.main10%TYPE                    DEFAULT NULL;
v_footer                document.footer%TYPE                    DEFAULT NULL;
v_publish_date          document.publish_date%TYPE              DEFAULT NULL;
v_logo                  organization.logo%TYPE                  DEFAULT NULL;
v_organization_pk       organization.organization_pk%TYPE       DEFAULT NULL;
v_name                  organization.name%TYPE                  DEFAULT NULL;
v_language_pk           document.language_fk%TYPE               DEFAULT NULL;
v_path                  picture.path%TYPE                       DEFAULT NULL;
v_path2                 VARCHAR2(100)                           DEFAULT NULL;
v_document_pk           document.document_pk%TYPE               DEFAULT NULL;
v_heading2              document.heading%TYPE                   DEFAULT NULL;

CURSOR  pic_in_doc IS
SELECT  p.path
FROM    picture_in_document pid, picture p
WHERE   pid.document_fk = p_document_pk
AND     pid.picture_fk=p.picture_pk
;

CURSOR  rel_doc IS
SELECT  d.document_pk, d.heading, d.publish_date
FROM    document d, relation_d2d r
WHERE   r.document_fk_1 = p_document_pk
AND     r.document_fk_2 = d.document_pk
AND     d.organization_fk = v_organization_pk
;

CURSOR  get_doc IS
SELECT  d.heading, d.ingress,
        d.main1, d.main2, d.main3, d.main4, d.main5, d.main6, d.main7,
        d.main8, d.main9, d.main10, d.footer, o.logo, d.publish_date,
        o.organization_pk, o.name, d.language_fk
FROM    document d, organization o
WHERE   d.document_pk = p_document_pk
AND     d.accepted > 0
AND     d.organization_fk = o.organization_pk
AND     o.accepted = 1
;

BEGIN

OPEN    get_doc;
FETCH   get_doc
INTO    v_heading, v_ingress,
        v_main1, v_main2, v_main3, v_main4, v_main5 ,v_main6, v_main7,
        v_main8, v_main9, v_main10, v_footer, v_logo,v_publish_date,
        v_organization_pk, v_name, v_language_pk;

IF ( get_doc%ROWCOUNT = 0 ) THEN
        v_organization_pk := get.oid;
        SELECT d.heading, d.ingress,
        d.main1, d.main2, d.main3, d.main4, d.main5, d.main6, d.main7,
        d.main8, d.main9, d.main10, d.footer, o.logo, d.publish_date,
        o.organization_pk, o.name, d.language_fk
        INTO
        v_heading, v_ingress,
        v_main1, v_main2, v_main3, v_main4, v_main5 ,v_main6, v_main7,
        v_main8, v_main9, v_main10, v_footer, v_logo,v_publish_date,
        v_organization_pk, v_name, v_language_pk
        FROM    document d, organization o
        WHERE   d.document_pk = p_document_pk
        AND     d.accepted > 0
        AND     d.organization_fk = o.organization_pk
        AND     d.organization_fk = v_organization_pk
        ;
END IF;
CLOSE   get_doc;

-- Legger inn i statistikk tabellen
stat.reg(v_organization_pk ,p_document_pk, NULL, NULL, NULL, NULL,19);

IF ( p_command IS NULL ) THEN
	v_path2 := owa_util.get_cgi_env('HTTP_REFERER');

	IF ( owa_pattern.match( v_path2, 'p_organization_pk=\d*', 'i') ) THEN
	        owa_pattern.change(v_path2, '^.*p_organization_pk=', '');
	        owa_pattern.change(v_path2, '&p_language_pk=\d*', '');

		html.b_page(NULL, NULL, NULL, NULL, 3);
		if ( v_organization_pk is not null ) THEN
		        html.org_menu( v_organization_pk );
		else
			html.org_menu(to_number(v_path));
		END IF;
	ELSE
		html.b_page(NULL, NULL, NULL, NULL, 1);
		html.home_menu;
	END IF;

ELSE
        html.b_page_2('600', NULL, NULL, NULL, 'JavaScript:self.print();');
	html.empty_menu;
END IF;


OPEN pic_in_doc;
FETCH pic_in_doc INTO v_path;
CLOSE pic_in_doc;

html.b_box('','100%','doc_util.show_document','doc_util.doc_archive?p_organization_pk='||v_organization_pk||'&p_language_pk='||v_language_pk);
html.b_table;
htp.p('<tr><td colspan="2">&nbsp;</td></tr>');

IF ( p_search IS NOT NULL ) THEN
        htp.p('<tr><td colspan="2"><a
        href="search.doc?p_search='||REPLACE(trans(p_search),' ','+')||'">'||get.txt('back_to_search')||'</a></td></tr>
        <tr><td colspan="2">&nbsp;</td></tr>');
END IF;

IF( v_logo IS NULL ) THEN
    v_logo := 'no_logo.gif';
END IF;

htp.p('
<tr><td colspan="2"><b>'||get.txt('date')||':</b>&nbsp;&nbsp;'|| to_char(v_publish_date,get.txt('date_long')) ||'&nbsp;(CET)<br></td></tr>
<tr><td colspan="2"><h2><b>'|| html.rem_tag(v_heading) ||'</b></h2></td></tr>
<tr><td><b>'|| html.rem_tag(v_ingress) ||'</b></td>
<td rowspan="6" valign="top"><a href="org_page.main?p_organization_pk='|| v_organization_pk ||'">
<img src="'||get.value('org_logo_dir')||'/'|| v_logo ||'" alt="'||get.txt('back')||'"  border="0"></a></td>
</tr>
<tr><td>&nbsp;</td></tr>
<tr><td valign="top">');
IF ( v_path IS NOT NULL ) THEN
       htp.p('<DIV ALIGN="right"><img align="right" src="'|| get.value('photo_archive') ||'/'|| v_path ||'.jpg"></DIV>');
END IF;

htp.p('<DIV VALIGN="TOP" ALIGN="left">
'|| html.rem_tag(v_main1) ||''|| html.rem_tag(v_main2) ||'
'|| html.rem_tag(v_main3) ||''|| html.rem_tag(v_main4) ||'
'|| html.rem_tag(v_main5) ||''|| html.rem_tag(v_main6) ||'
'|| html.rem_tag(v_main7) ||''|| html.rem_tag(v_main8) ||'
'|| html.rem_tag(v_main9) ||''|| html.rem_tag(v_main10) ||'
</DIV>
</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td><i>'|| html.rem_tag(v_footer) ||'</i></td></tr>
<tr><td>&nbsp;</td></tr>');

IF ( p_command IS NULL ) THEN
        htp.p('<tr><td colspan="2">');
	html.b_table;
	htp.p('<tr><td align="left" width="10%">'||
        html.popup( get.txt('print_page'),
        'doc_util.show_document?p_document_pk='||p_document_pk||'&p_command=print','620', '700', '1' )
        ||'</td><td align="left" width="90%"><b>'||get.txt('written_by')||':</b>&nbsp;&nbsp;<a href="org_page.main?p_organization_pk='|| v_organization_pk ||'">'||v_name||'</a>
<br>&nbsp;<br><b>'||get.txt('date')||':</b>&nbsp;&nbsp;'|| to_char(v_publish_date,get.txt('date_long')) ||'&nbsp;(CET)
</td></tr>');
	html.e_table;
	htp.p('</td></tr>');
END IF;

htp.p('
<tr><td align="center" colspan="2"><br>
<a href="org_page.main?p_organization_pk='|| v_organization_pk ||'">['||v_name||']</a>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href="/">['||get.domain||']</a>
</td></tr>
<tr><td>&nbsp;</td></tr>');
html.e_table;
html.e_box;
--<tr><td>');
OPEN rel_doc;
LOOP
        FETCH rel_doc INTO v_document_pk, v_heading2, v_publish_date;
        EXIT WHEN rel_doc%NOTFOUND;
        IF ( rel_doc%ROWCOUNT = 1 ) THEN
                html.b_box(get.txt('related_docs'));
                html.b_table('100%');
        END IF;
        htp.p('<tr><td width="20%">'||to_char(v_publish_date,get.txt('date_long'))||'</td>
              <td><a href="doc_util.show_document?p_document_pk='||v_document_pk||'">'||v_heading2||'</a></td></tr>');
END LOOP;
IF ( rel_doc%ROWCOUNT > 0 ) THEN
        html.e_table;
        html.e_box;
END IF;
CLOSE rel_doc;

--htp.p('</td></tr>
--<tr><td>&nbsp;</td></tr>');
--html.e_table;
--html.e_box;
IF ( p_command IS NULL ) THEN
        tip.someone;
        html.e_page;
ELSE
        html.e_page_2;
END IF;

EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
        html.jump_to('/');
END;
END show_document;

---------------------------------------------------------------------
-- Name: doc_serv
-- Type: procedure
-- What: Genererer opp hovedsiden for admin av en organisasjon
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE doc_serv(     p_service_pk            IN service.service_pk%TYPE      DEFAULT NULL,
                        p_language_pk           IN document.language_fk%TYPE    DEFAULT NULL )
IS
BEGIN
DECLARE

v_document_pk           document.document_pk%TYPE               DEFAULT NULL;
v_publish_date          document.publish_date%TYPE              DEFAULT NULL;
v_heading               document.heading%TYPE                   DEFAULT NULL;
v_ingress               document.ingress%TYPE                   DEFAULT NULL;
v_organization_pk       organization.organization_pk%TYPE       DEFAULT NULL;
v_name                  organization.name%TYPE                  DEFAULT NULL;
v_geography_pk          organization.geography_fk%TYPE          DEFAULT NULL;
v_service_pk            service.service_pk%TYPE                 DEFAULT NULL;
v_language_pk           document.language_fk%TYPE               DEFAULT NULL;
v_doc_nr                NUMBER                                  DEFAULT NULL;

CURSOR  get_doc IS
SELECT  d.document_pk, d.publish_date, d.heading, d.ingress,
        o.name, o.organization_pk, o.geography_fk
FROM    document d, organization o, org_service os
WHERE   d.organization_fk=o.organization_pk
AND     d.organization_fk=os.organization_fk
AND     d.language_fk=v_language_pk
AND     o.accepted = 1
AND     d.accepted > 0
AND     d.publish_date < SYSDATE
AND     os.service_fk=v_service_pk
ORDER BY d.publish_date DESC;

BEGIN
   v_doc_nr := get.value('doc_nr');
   IF ( p_service_pk IS NULL ) THEN
      v_service_pk := get.serv;
   ELSE
      v_service_pk := p_service_pk;
   END IF;
   IF ( p_language_pk IS NULL ) THEN
      v_language_pk := get.lan;
   ELSE
      v_language_pk := p_language_pk;
   END IF;
   html.b_box(get.txt('latest_news'),'100%','org_page.doc','doc_util.doc_archive?p_language_pk='||p_language_pk);
   html.b_table;
   htp.p('<tr><td align="left"><b>'||get.txt('date')||':</b></td><td><b>'||get.txt('subject')||':</b></td><td><b>'||get.txt('source')||':</b></td><td><b>'||get.txt('geography_location')||':</b></td></tr>');
   OPEN get_doc;
   LOOP
      fetch get_doc into v_document_pk, v_publish_date, v_heading, v_ingress, v_name, v_organization_pk, v_geography_pk;
      exit when get_doc%NOTFOUND;
      htp.p('<tr><td valign="top" nowrap>'||to_char(v_publish_date,get.txt('date_short'))||'</td>
            <td valign="top" align="left" width="90%"><b><a href="doc_util.show_document?p_document_pk='||v_document_pk||'">
        '||html.rem_tag(v_heading)||'</a></b></td>
              <td nowrap valign="top"><a href="org_page.main?p_organization_pk='||v_organization_pk||'">'||v_name||'</a></td>
	      <td>'||get.locn(v_geography_pk)||'</td></tr>');
      IF ( v_service_pk = 5 ) THEN
         htp.p('<tr><td>&nbsp;</td><td valign="top">'||v_ingress||'</td><td>&nbsp;</td><td>&nbsp;</td></tr>');
      END IF;
      exit when get_doc%ROWCOUNT = v_doc_nr;
   END LOOP;
   close get_doc;
   html.e_table;
   html.e_box;
END;
END doc_serv;

---------------------------------------------------------------------
-- Name: return_document
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

FUNCTION return_document (     p_document_pk           IN document.document_pk%TYPE                    DEFAULT NULL,
                               p_ad                    IN VARCHAR2                                     DEFAULT NULL
                               ) RETURN long
IS
BEGIN
DECLARE

v_heading               document.heading%TYPE                   DEFAULT NULL;
v_ingress               document.ingress%TYPE                   DEFAULT NULL;
v_main1                 document.main1%TYPE                     DEFAULT NULL;
v_main2                 document.main2%TYPE                     DEFAULT NULL;
v_main3                 document.main3%TYPE                     DEFAULT NULL;
v_main4                 document.main4%TYPE                     DEFAULT NULL;
v_main5                 document.main5%TYPE                     DEFAULT NULL;
v_main6                 document.main6%TYPE                     DEFAULT NULL;
v_main7                 document.main7%TYPE                     DEFAULT NULL;
v_main8                 document.main8%TYPE                     DEFAULT NULL;
v_main9                 document.main9%TYPE                     DEFAULT NULL;
v_main10                document.main10%TYPE                    DEFAULT NULL;
v_footer                document.footer%TYPE                    DEFAULT NULL;
v_publish_date          document.publish_date%TYPE              DEFAULT NULL;
v_logo                  organization.logo%TYPE                  DEFAULT NULL;
v_organization_pk       organization.organization_pk%TYPE       DEFAULT NULL;
v_name                  organization.name%TYPE                  DEFAULT NULL;
v_language_pk           document.language_fk%TYPE               DEFAULT NULL;
v_path                  picture.path%TYPE                       DEFAULT NULL;
v_path2                 VARCHAR2(100)                           DEFAULT NULL;
v_document_pk           document.document_pk%TYPE               DEFAULT NULL;
v_heading2              document.heading%TYPE                   DEFAULT NULL;
v_html                  long                                    DEFAULT NULL;
v_code                  VARCHAR2(4000)                          DEFAULT NULL;
v_code2                 VARCHAR2(4000)                          DEFAULT NULL;
v_service_pk            service.service_pk%TYPE                 DEFAULT NULL;
v_service_name          VARCHAR2(100)                           DEFAULT NULL;
v_domain                service_domain.domain%TYPE              DEFAULT NULL;

CURSOR  pic_in_doc IS
SELECT  p.path
FROM    picture_in_document pid, picture p
WHERE   pid.document_fk = p_document_pk
AND     pid.picture_fk=p.picture_pk
;

CURSOR  rel_doc IS
SELECT  d.document_pk, d.heading, d.publish_date
FROM    document d, relation_d2d r
WHERE   r.document_fk_1 = p_document_pk
AND     r.document_fk_2 = d.document_pk
AND     d.organization_fk = v_organization_pk
;

CURSOR  get_doc IS
SELECT  d.heading, d.ingress,
        d.main1, d.main2, d.main3, d.main4, d.main5, d.main6, d.main7,
        d.main8, d.main9, d.main10, d.footer, o.logo, d.publish_date,
        o.organization_pk, o.name, d.language_fk, os.service_fk
FROM    document d, organization o, org_service os
WHERE   d.document_pk = p_document_pk
AND     d.accepted > 0
AND     d.organization_fk = o.organization_pk
AND     o.accepted = 1
AND     d.organization_fk = os.organization_fk
;

BEGIN

OPEN    get_doc;
FETCH   get_doc
INTO    v_heading, v_ingress,
        v_main1, v_main2, v_main3, v_main4, v_main5 ,v_main6, v_main7,
        v_main8, v_main9, v_main10, v_footer, v_logo,v_publish_date,
        v_organization_pk, v_name, v_language_pk, v_service_pk;
CLOSE   get_doc;

SELECT  domain INTO v_domain
  FROM    service_domain
  WHERE   service_fk = v_service_pk
  AND     language_fk = v_language_pk;
SELECT  description INTO v_service_name
  FROM    service
  WHERE   service_pk = v_service_pk;

v_code := html.getcode('b_page');

owa_pattern.change(v_code, '\n', '���', 'g');
owa_pattern.change(v_code, '<!-- banner_start -->.*<!-- banner_end -->', trans(p_ad), 'g');
owa_pattern.change(v_code, '���', '\n', 'g');
v_code := REPLACE (v_code, '[reload]', '[reload]<BASE href="http://'|| v_domain ||'/cgi-bin/">');
v_code := REPLACE (v_code, '[reload]', '');
v_code := REPLACE (v_code, '[jump]', '');
v_code := REPLACE (v_code, '[icons_dir]', get.value('icons_dir') );
v_code := REPLACE (v_code, '[width]', get.value('width') );
v_code := REPLACE (v_code, '[css]',  html.getcode('css') );
v_code := REPLACE (v_code, '[javascript]',  html.getcode('javascript') );
v_code := REPLACE (v_code, '[c_service_bbm]' , get.value( 'c_'|| v_service_name ||'_bbm' ) );
v_code := REPLACE (v_code, '[c_service_bbs]' , get.value( 'c_'|| v_service_name ||'_bbs' ) );
v_code := REPLACE (v_code, '[c_service_mmb]' , get.value( 'c_'|| v_service_name ||'_mmb' ) );
v_code := REPLACE (v_code, '[ad_button]', get.txt('ad_button', v_language_pk) );
v_html := v_code;

v_code :=  html.getcode('main_menu');
v_code := REPLACE (v_code, '[home_button]', get.txt('home_button', v_language_pk) );
v_code := REPLACE (v_code, '[my_page_button]', get.txt('my_page_button', v_language_pk) );
v_code := REPLACE (v_code, '[organizations_button]', get.txt('organizations_button2', v_language_pk) );
v_code := REPLACE (v_code, '[c_service_mmb]' , get.value( 'c_'|| v_service_name ||'_mmb' ) );
v_html := v_html ||''|| v_code;

v_code := html.getcode('home_menu');
v_code := REPLACE (v_code, '[doc_archive_button]', get.txt('doc_archive_button', v_language_pk) );
v_code := REPLACE (v_code, '[cal_archive_button]', get.txt('cal_archive_button', v_language_pk) );
v_code := REPLACE (v_code, '[last_comments_button]', get.txt('last_comments_button', v_language_pk) );
v_code := REPLACE (v_code, '[last_albums_button]', get.txt('last_albums_button', v_language_pk) );
v_code := REPLACE (v_code, '[last_pictures_button]', get.txt('last_pictures_button', v_language_pk) );

v_code2 := html.getcode('i4_menu');
v_code2 := REPLACE (v_code2, '[login_status_button]', get.txt('login_button', v_language_pk) );
v_code2 := REPLACE (v_code2, '[login_status_url]', 'portfolio.startup' );
v_code2 := REPLACE (v_code2, '[search_button]', get.txt('search_button', v_language_pk) );
v_code2 := REPLACE (v_code2, '[contact_us_button]', get.txt('contact_us_button', v_language_pk) );
v_code2 := REPLACE (v_code2, '[about_button]', get.txt('about_button', v_language_pk) );
v_code2 := REPLACE (v_code2, '[help_button]', get.txt('help_button', v_language_pk) );

v_code := REPLACE (v_code, '[i4_menu]' , v_code2 );
v_code := REPLACE (v_code, '[p_language_pk]' , v_language_pk );
v_html := v_html ||''|| v_code;

OPEN pic_in_doc;
FETCH pic_in_doc INTO v_path;
CLOSE pic_in_doc;

v_code := html.getcode('b_box');
v_code := REPLACE (v_code, '[title]', '' );
v_code := REPLACE (v_code, '[width]', '100%' );
v_code := REPLACE (v_code, '<!-- ht -->' , html.popup( get.txt('?', v_language_pk), 'help.module?p_package=doc_util.show_document' , 400, 500, 0, 1) );
v_code := REPLACE (v_code, '<!-- archive -->','<a href="doc_util.doc_archive?p_organization_pk='||v_organization_pk||'&p_language_pk='||v_language_pk||'">'||get.txt('archive', v_language_pk)||'</a>' );
v_html := v_html ||''|| v_code;

v_code :=  html.getcode('b_table');
v_code := REPLACE (v_code, '[width]', '' );
v_code := REPLACE (v_code, '[border]', 0 );
v_html := v_html ||''|| v_code;

IF( v_logo IS NULL ) THEN
    v_logo := 'no_logo.gif';
END IF;

v_html := v_html ||'<tr><td colspan="2">&nbsp;</td></tr>
<tr><td colspan="2"><b>'||get.txt('date', v_language_pk)||':</b>&nbsp;&nbsp;'|| to_char(v_publish_date,get.txt('date_long', v_language_pk)) ||'&nbsp;(CET)<br></td></tr>
<tr><td colspan="2"><h2><b>'|| html.rem_tag(v_heading) ||'</b></h2></td></tr>
<tr><td><b>'|| html.rem_tag(v_ingress) ||'</b></td>
<td rowspan="6" valign="top"><a href="org_page.main?p_organization_pk='|| v_organization_pk ||'">
<img src="'||get.value('org_logo_dir')||'/'|| v_logo ||'" alt="'||get.txt('back', v_language_pk)||'"  border="0"></a></td>
</tr>
<tr><td>&nbsp;</td></tr>
<tr><td valign="top">';
IF ( v_path IS NOT NULL ) THEN
       v_html := v_html ||'<DIV ALIGN="right"><img align="right" src="'|| get.value('photo_archive') ||'/'|| v_path ||'.jpg"></DIV>';
END IF;

v_html := v_html ||'
<DIV VALIGN="TOP" ALIGN="left">
'|| html.rem_tag(v_main1) ||'
'|| html.rem_tag(v_main2) ||'
'|| html.rem_tag(v_main3) ||'
'|| html.rem_tag(v_main4) ||'
'|| html.rem_tag(v_main5) ||'
';

v_html := v_html ||'
'|| html.rem_tag(v_main6) ||'
'|| html.rem_tag(v_main7) ||'
'|| html.rem_tag(v_main8) ||'
'|| html.rem_tag(v_main9) ||'
'|| html.rem_tag(v_main10) ||'
</DIV>
</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td><i>'|| html.rem_tag(v_footer) ||'</i></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td colspan="2">';


v_code :=  html.getcode('b_table');
v_code := REPLACE (v_code, '[width]', '' );
v_code := REPLACE (v_code, '[border]', 0 );
v_html := v_html ||''|| v_code;

v_html := v_html ||'<tr><td align="left" width="10%">'||
html.popup( get.txt('print_page', v_language_pk),
'doc_util.show_document?p_document_pk='||p_document_pk||'&p_command=print','620', '700', '1' )
||'</td><td align="left" width="90%"><b>'||get.txt('written_by', v_language_pk)||':</b>&nbsp;&nbsp;<a href="org_page.main?p_organization_pk='|| v_organization_pk ||'">'||v_name||'</a>
<br>&nbsp;<br><b>'||get.txt('date', v_language_pk)||':</b>&nbsp;&nbsp;'|| to_char(v_publish_date,get.txt('date_long', v_language_pk)) ||'&nbsp;(CET)
</td></tr>
</table>
</td></tr>

<tr><td align="center" colspan="2"><br>
<a href="org_page.main?p_organization_pk='|| v_organization_pk ||'">['||v_name||']</a>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href="/">['||v_service_name||']</a>
</td></tr>
<tr><td>&nbsp;</td></tr>
</table>'|| html.getcode('e_box');

v_code := html.getcode('e_page');
v_code := REPLACE (v_code, '[icons_dir]', get.value('icons_dir') );
v_code := REPLACE (v_code, '[c_service_bbb]' , get.value( 'c_'|| v_service_name ||'_bbb' ) );
v_html := v_html ||''|| v_code;



RETURN v_html;

END;
END return_document;



PROCEDURE show_mail_document (       p_document_pk   IN document.document_pk%TYPE    DEFAULT NULL )
IS
BEGIN
	htp.p(doc_util.return_document(p_document_pk, '<img src="/img/banners/blank_earth.gif">'));
END show_mail_document;


---------------------------------------------------------------------
---------------------------------------------------------------------
END;
/
