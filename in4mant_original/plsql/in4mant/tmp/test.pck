CREATE OR REPLACE PACKAGE test IS

	PROCEDURE test;
	PROCEDURE write_calendar_files;
	FUNCTION return_html_calendar
		(
			p_calendar_pk		IN calendar.calendar_pk%TYPE            DEFAULT NULL
		) RETURN long;

END;
/
CREATE OR REPLACE PACKAGE BODY test IS


---------------------------------------------------------
-- Name:        test
-- Type:        procedure
-- What:        writes out the hiscore list for a game
-- Author:      Frode Klevstul
-- Start date:  02.03.2001
-- Desc:
---------------------------------------------------------
PROCEDURE test
IS
BEGIN
DECLARE
	v_date	date default NULL;
BEGIN

if ( to_number(to_char(sysdate, 'HH24MI'))>1200 and to_number(to_char(sysdate, 'HH24MI'))<1700 ) then

	owa_util.print_cgi_env;
else
	htp.p('else');
end if;


END;
END test;











---------------------------------------------------------
-- Name:        write_calendar_files
-- Type:        procedure
-- What:        a proc that goes through the database and
--              writes calendar "email" files to the OS.
--		This should be executed every day at 12:00
-- Author:      Frode Klevstul
-- Start date:  22.03.2001
-- Desc:
---------------------------------------------------------
PROCEDURE write_calendar_files
IS
BEGIN
DECLARE

	v_date		date	default to_date( (to_char(sysdate, 'YYYY DD/MM')||' 12:00'), 'YYYY DD/MM HH24:MI');

        -- Cursor that selects all documents to be sendt out with email.
        CURSOR  select_calendar IS
        SELECT  calendar_pk, organization_fk, language_fk, event_date, heading, message
        FROM    calendar c, organization o
        WHERE   c.organization_fk = o.organization_pk
        AND     o.accepted = 1
        AND     c.event_date between v_date and v_date + 1.75
        AND     c.accepted IS NULL;

	v_calendar_pk		calendar.calendar_pk%type		default NULL;
        v_organization_pk       organization.organization_pk%type       default NULL;
        v_language_pk           la.language_pk%type                     default NULL;
	v_event_date		calendar.event_date%type		default NULL;
	v_heading		calendar.heading%type			default NULL;
	v_message		calendar.message%type			default NULL;

        -- Cursor that selects out users related to an organization.
        CURSOR  select_users
                (
                        v_organization_pk       in organization.organization_pk%type,
                        v_language_pk           in la.language_pk%type
                ) IS
        SELECT  DISTINCT(lu.user_fk), ud.email
        FROM    list_org lo, list l, list_user lu, list_type lt, user_details ud
        WHERE   lu.list_fk = l.list_pk
        AND     lt.list_type_pk = l.list_type_fk
        AND     lu.language_fk = v_language_pk
        AND     lo.organization_fk = v_organization_pk
        AND     lu.user_fk = ud.user_fk
        AND
                (
                        lo.list_fk = l.list_pk
                OR      lo.list_fk = l.level0
                OR      lo.list_fk = l.level1
                OR      lo.list_fk = l.level2
                OR      lo.list_fk = l.level3
                OR      lo.list_fk = l.level4
                OR      lo.list_fk = l.level5
                );

        v_user_pk               usr.user_pk%type                        default NULL;
        v_email                 user_details.email%type                 default NULL;

        v_name                  organization.name%type                  default NULL;

        v_file                  utl_file.file_type                      default NULL;
        v_email_dir             varchar2(100)                           default NULL;
        v_number                number                                  default NULL;
        v_message_txt           varchar2(4000)                          default NULL;
        v_msg_footer            varchar2(4000)                          default NULL;
        v_check                 number                                  default NULL;
	v_publish_info		varchar2(1000)				default NULL;
	v_et_text		varchar2(4000)				default NULL;
	v_et_html		varchar2(4000)				default NULL;

        v_email_text_pk         email_text.email_text_pk%type           default NULL;
        v_name_sg_fk            email_text.name_sg_fk%type              default NULL;
        v_mail_log_pk           mail_log.mail_log_pk%type               default NULL;
        v_no_users              number                                  default NULL;

	v_html_part		varchar2(32767)				default NULL;
	v_string_length 	number 					default NULL;
	v_tmp1			number					default NULL;

/* ---- deklarasjoner til bruk i html versjonen av mailen ---- */


/* ----------- */



BEGIN


	v_email_dir     := get.value('email_dir');

        -- -----------------------------------------------------------------------
        -- henter ut dokumenter som er publisert og hvor det skal sendes ut mail
        -- -----------------------------------------------------------------------
        OPEN select_calendar;
        LOOP
                FETCH select_calendar INTO v_calendar_pk, v_organization_pk, v_language_pk, v_event_date, v_heading, v_message;
                EXIT WHEN select_calendar%NOTFOUND;

                v_message_txt   := get.txt('email_update_heading', v_language_pk);
                v_msg_footer    := get.txt('email_update_footer', v_language_pk);
                v_name          := get.oname(v_organization_pk);
                v_no_users      := 0; -- antall mottagere av dette dokumentet

		v_message_txt := REPLACE (v_message_txt, '[subject]' , v_name ||' - '|| SUBSTR(v_heading, 0, 20) ||'  ...' );
		-- m� st� helt f�rst p� linja
v_publish_info := get.txt('publish_date', v_language_pk) ||': '|| to_char(v_event_date, get.txt('date_long', v_language_pk)) ||'
'|| get.txt('source', v_language_pk)||': '|| v_name ||'
'|| get.txt('url_to_news', v_language_pk)||': '|| get.olink(v_organization_pk, 'cal_util.show_calendar?p_calendar_pk='||v_calendar_pk, v_language_pk);

                SELECT  email_script_seq.NEXTVAL
                INTO    v_number
                FROM    dual;

                -- ------------------------------------------------
                -- skriver selve mailen (innholdet) *** START ***
                -- ------------------------------------------------

                -- legger eventuelt til reklametekst i mail
                v_email_text_pk := get.et(v_organization_pk, 'CB');
                if (v_email_text_pk IS NOT NULL) then
                        SELECT  name_sg_fk INTO v_name_sg_fk
                        FROM    email_text
                        WHERE   email_text_pk = v_email_text_pk;

                        v_et_text := get.text(v_name_sg_fk, v_language_pk);
                        v_et_html := v_et_text;

			-- splitter opp teksten i en TEXT del og en HTML del
			owa_pattern.change(v_et_text, '\n', '���', 'g');
			owa_pattern.change(v_et_text, '</txt-message>.*$', NULL, 'g');
			owa_pattern.change(v_et_text, '^<txt-message>', NULL);
			owa_pattern.change(v_et_text, '���', '\n', 'g');

			owa_pattern.change(v_et_html, '\n', '���', 'g');
			owa_pattern.change(v_et_html, '^.*<html-message>', NULL, 'g');
			owa_pattern.change(v_et_html, '</html-message>', NULL);
			owa_pattern.change(v_et_html, '���', '\n', 'g');
                end if;

                v_file := utl_file.fopen( v_email_dir, v_number||'_message.txt' ,'a');

                utl_file.put_line(v_file, substr(v_message_txt,1,999));
                utl_file.put(v_file, substr(v_message_txt,1000,1000));
                utl_file.put(v_file, substr(v_message_txt,2000,1000));
                utl_file.put(v_file, substr(v_message_txt,3000,1000));

                -- -----------------------------
		-- skriver mailen som ren tekst
                -- -----------------------------
		utl_file.put(v_file, '<txt-message>');
		utl_file.new_line(v_file, 1);
                utl_file.put(v_file, v_publish_info );
		utl_file.new_line(v_file, 1);
                utl_file.put(v_file, v_heading );
                utl_file.new_line(v_file, 2);

                utl_file.put(v_file, substr(v_message,1,999));
                utl_file.put(v_file, substr(v_message,1000,1000));
                utl_file.put(v_file, substr(v_message,2000,1000));
                utl_file.put(v_file, substr(v_message,3000,1000));

                utl_file.new_line(v_file, 10);
                utl_file.put(v_file, substr(v_et_text,1,999));
                utl_file.put(v_file, substr(v_et_text,1000,1000));
                utl_file.put(v_file, substr(v_et_text,2000,1000));
                utl_file.put(v_file, substr(v_et_text,3000,1000));

                utl_file.new_line(v_file, 1);
		utl_file.put_line(v_file, '</txt-message>');

-- ------------
-- Dette (html versjon) skal implementeres senere ....
                -- -------------------------------------------------------------
		-- skriver mailen som html
                -- -------------------------------------------------------------
		utl_file.put_line(v_file, '<html-message>');

                utl_file.new_line(v_file, 1);
		utl_file.put_line(v_file, '</html-message>');


                utl_file.fclose(v_file);
                -- ---------------------------------------------------------
                -- /slutt p� � skrive innholdet av mailen *** SLUTT ***
                -- ---------------------------------------------------------

                -- ----------------------------------------------------------------------------------
                -- skriver mottager fil ********* START **********
                -- ----------------------------------------------------------------------------------
                v_file := utl_file.fopen( v_email_dir, v_number||'_email-list.txt', 'a');

                -- ----------------------------------------------------------
                -- henter ut brukere tilknyttet en organisasjon p� ett spr�k
                -- ----------------------------------------------------------
                OPEN select_users(v_organization_pk, v_language_pk);
                LOOP
                        FETCH select_users INTO v_user_pk, v_email;
                        EXIT WHEN select_users%NOTFOUND;

			utl_file.put_line(v_file, trans(v_email));
			v_no_users := v_no_users + 1; -- antall email mottagere

                END LOOP;
                CLOSE select_users;

                utl_file.fclose(v_file);

                -- -------------------------------------------------------------------------
                -- /slutt p� � skrive mottager fil ********* END **********
                -- -------------------------------------------------------------------------

                -- -----------------------------------------------
                -- oppdaterer dokument status (til "mail utsendt")
                -- -----------------------------------------------
                UPDATE  calendar
                SET     accepted = 2
                WHERE   calendar_pk = v_calendar_pk;
                commit;

                -- ------------------------------------
                -- legg inn informasjon i mail_log
                -- ------------------------------------
                SELECT  mail_log_seq.NEXTVAL
                INTO    v_mail_log_pk
                FROM    dual;

                INSERT INTO mail_log
                (mail_log_pk, volume, sendt_date, organization_fk, calendar_fk, email_text_fk)
                VALUES (v_mail_log_pk, v_no_users, sysdate, v_organization_pk, v_calendar_pk, v_email_text_pk);
                commit;

        END LOOP;
        CLOSE select_calendar;



EXCEPTION
	WHEN UTL_FILE.INVALID_PATH THEN
	BEGIN
		v_file := utl_file.fopen( v_email_dir, v_number||'_INVALID_PATH.txt', 'a');
		utl_file.put_line(v_file, 'UTL_FILE EXCEPTION! calendar_pk: '||v_calendar_pk);
		utl_file.fclose(v_file);
	END;

	WHEN UTL_FILE.INVALID_MODE THEN
	BEGIN
		v_file := utl_file.fopen( v_email_dir, v_number||'_INVALID_MODE.txt', 'a');
		utl_file.put_line(v_file, 'UTL_FILE EXCEPTION! calendar_pk: '||v_calendar_pk);
		utl_file.fclose(v_file);
	END;

	WHEN UTL_FILE.INVALID_FILEHANDLE THEN
	BEGIN
		v_file := utl_file.fopen( v_email_dir, v_number||'_INVALID_FILEHANDLE.txt', 'a');
		utl_file.put_line(v_file, 'UTL_FILE EXCEPTION! calendar_pk: '||v_calendar_pk);
		utl_file.fclose(v_file);
	END;

	WHEN UTL_FILE.INVALID_OPERATION THEN
	BEGIN
		v_file := utl_file.fopen( v_email_dir, v_number||'_INVALID_OPERATION.txt', 'a');
		utl_file.put_line(v_file, 'UTL_FILE EXCEPTION! calendar_pk: '||v_calendar_pk);
		utl_file.fclose(v_file);
	END;

	WHEN UTL_FILE.READ_ERROR THEN
	BEGIN
		v_file := utl_file.fopen( v_email_dir, v_number||'_READ_ERROR.txt', 'a');
		utl_file.put_line(v_file, 'UTL_FILE EXCEPTION! calendar_pk: '||v_calendar_pk);
		utl_file.fclose(v_file);
	END;

	WHEN UTL_FILE.INTERNAL_ERROR THEN
	BEGIN
		v_file := utl_file.fopen( v_email_dir, v_number||'_INTERNAL_ERROR.txt', 'a');
		utl_file.put_line(v_file, 'UTL_FILE EXCEPTION! calendar_pk: '||v_calendar_pk);
		utl_file.fclose(v_file);
	END;

	WHEN UTL_FILE.WRITE_ERROR THEN
	BEGIN
		v_file := utl_file.fopen( v_email_dir, v_number||'_WRITE_ERROR.txt', 'a');
		utl_file.put_line(v_file, 'UTL_FILE EXCEPTION! calendar_pk: '||v_calendar_pk);
		utl_file.fclose(v_file);
	END;

	WHEN UTL_FILE.INVALID_MAXLINESIZE THEN
	BEGIN
		v_file := utl_file.fopen( v_email_dir, v_number||'_INVALID_MAXLINESIZE.txt', 'a');
		utl_file.put_line(v_file, 'UTL_FILE EXCEPTION! calendar_pk: '||v_calendar_pk);
		utl_file.fclose(v_file);
	END;

	WHEN OTHERS THEN
	BEGIN
		v_file := utl_file.fopen( v_email_dir, v_number||'_OTHER.txt', 'a');
		utl_file.put_line(v_file, 'UTL_FILE EXCEPTION! calendar_pk: '||v_calendar_pk);
		utl_file.fclose(v_file);
	END;

END;
END write_calendar_files;







---------------------------------------------------------------------
-- Name: return_html_calendar
-- Type: procedure
-- What: Returnerer en html side av kalenderen for bruk i mailutsendelse
-- Made: Frode Klevstul
-- Date: 23.03 2001
-- Chng:
---------------------------------------------------------------------

FUNCTION return_html_calendar
	(
		p_calendar_pk		IN calendar.calendar_pk%TYPE            DEFAULT NULL
	) RETURN long
IS
BEGIN
DECLARE

	v_calendar_pk           calendar.calendar_pk%TYPE               DEFAULT NULL;
	v_heading               calendar.heading%TYPE                   DEFAULT NULL;
	v_event_date            calendar.event_date%TYPE                DEFAULT NULL;
	v_end_date              calendar.end_date%TYPE                	DEFAULT NULL;
	v_message               calendar.message%TYPE                   DEFAULT NULL;
	v_language_pk           calendar.language_fk%TYPE               DEFAULT NULL;
	v_organization_pk       calendar.organization_fk%TYPE           DEFAULT NULL;
	v_name                  organization.name%TYPE                  DEFAULT NULL;
	v_logo                  organization.logo%TYPE                  DEFAULT NULL;
	v_path                  VARCHAR2(100)                           DEFAULT NULL;
	v_registration_pk       registration.registration_pk%type       DEFAULT NULL;

	v_html                  varchar2(32767)		                DEFAULT NULL;
	v_code                  VARCHAR2(10000)                         DEFAULT NULL;
	v_code2                 VARCHAR2(4000)                          DEFAULT NULL;


BEGIN

	-- -----------------------------------------------------
	-- henter ut informasjon om hendelsen fra databasen
	-- -----------------------------------------------------
	SELECT  c.calendar_pk, c.event_date, c.end_date, c.heading, c.message,
	        c.language_fk, o.organization_pk, o.name, o.logo
	INTO    v_calendar_pk, v_event_date, v_end_date, v_heading, v_message,
	        v_language_pk, v_organization_pk, v_name, v_logo
	FROM    calendar c, organization o
	WHERE   c.calendar_pk = p_calendar_pk
	AND     c.organization_fk = o.organization_pk
	AND     o.accepted=1;

/*
	html.b_page(NULL, NULL, NULL, NULL, 3);
        html.org_menu( v_organization_pk );
*/
/*
		-- -----------
		-- b_page
		-- -----------
		v_code := html.getcode('b_page');

		owa_pattern.change(v_code, '\n', '���', 'g');
		owa_pattern.change(v_code, '<!-- banner_start -->.*<!-- banner_end -->', v_et_html, 'g');
		owa_pattern.change(v_code, '���', '\n', 'g');
		v_code := REPLACE (v_code, '[reload]', '[reload]<BASE href="http://'|| v_domain ||'/cgi-bin/">');
		v_code := REPLACE (v_code, '[reload]', '');
		v_code := REPLACE (v_code, '[jump]', '');
		v_code := REPLACE (v_code, '[icons_dir]', get.value('icons_dir') );
		v_code := REPLACE (v_code, '[width]', get.value('width') );
		v_code := REPLACE (v_code, '[css]',  html.getcode('css') );
		v_code := REPLACE (v_code, '[javascript]',  html.getcode('javascript') );
		v_code := REPLACE (v_code, '[c_service_bbm]' , get.value( 'c_'|| v_service_name ||'_bbm' ) );
		v_code := REPLACE (v_code, '[c_service_bbs]' , get.value( 'c_'|| v_service_name ||'_bbs' ) );
		v_code := REPLACE (v_code, '[c_service_mmb]' , get.value( 'c_'|| v_service_name ||'_mmb' ) );
		v_code := REPLACE (v_code, '[ad_button]', get.txt('ad_button', v_language_pk) );

		v_html := v_code;

		-- ------------
		-- main_menu
		-- ------------
		v_code :=  html.getcode('main_menu');
		v_code := REPLACE (v_code, '[home_button]', get.txt('home_button', v_language_pk) );
		v_code := REPLACE (v_code, '[my_page_button]', get.txt('my_page_button', v_language_pk) );
		v_code := REPLACE (v_code, '[organizations_button]', get.txt('organizations_button2', v_language_pk) );
		v_code := REPLACE (v_code, '[c_service_mmb]' , get.value( 'c_'|| v_service_name ||'_mmb' ) );

		v_html := v_html ||''|| v_code;

		-- ------------
		-- home_menu & i4_menu
		-- ------------

		v_code := html.getcode('home_menu');
		v_code := REPLACE (v_code, '[doc_archive_button]', get.txt('doc_archive_button', v_language_pk) );
		v_code := REPLACE (v_code, '[cal_archive_button]', get.txt('cal_archive_button', v_language_pk) );
		v_code := REPLACE (v_code, '[last_comments_button]', get.txt('last_comments_button', v_language_pk) );
		v_code := REPLACE (v_code, '[last_albums_button]', get.txt('last_albums_button', v_language_pk) );
		v_code := REPLACE (v_code, '[last_pictures_button]', get.txt('last_pictures_button', v_language_pk) );

		v_code2 := html.getcode('i4_menu');
		v_code2 := REPLACE (v_code2, '[login_status_button]', get.txt('login_button', v_language_pk) );
		v_code2 := REPLACE (v_code2, '[login_status_url]', 'portfolio.startup' );
		v_code2 := REPLACE (v_code2, '[search_button]', get.txt('search_button', v_language_pk) );
		v_code2 := REPLACE (v_code2, '[contact_us_button]', get.txt('contact_us_button', v_language_pk) );
		v_code2 := REPLACE (v_code2, '[about_button]', get.txt('about_button', v_language_pk) );
		v_code2 := REPLACE (v_code2, '[help_button]', get.txt('help_button', v_language_pk) );

		v_code := REPLACE (v_code, '[i4_menu]' , v_code2 );
		v_code := REPLACE (v_code, '[p_language_pk]' , v_language_pk );

		v_html := v_html ||''|| v_code;
*/

	html.b_box('','100%','cal_util.show_document','cal_util.cal_archive?p_organization_pk='||v_organization_pk||'&p_language_pk='||v_language_pk);
	html.b_table;
	htp.p('<tr><td colspan="2">&nbsp;</td></tr>');

	IF( v_logo IS NULL ) THEN
	    v_logo := 'no_logo.gif';
	END IF;

	htp.p(' <tr><td colspan="2"><b>'||get.txt('date', v_language_pk)||':</b>&nbsp;&nbsp;'|| to_char(v_event_date,get.txt('date_long', v_language_pk)) );

	if (v_end_date IS NOT NULL and v_end_date > v_event_date) then
		htp.p(' - '||to_char(v_end_date,get.txt('date_long', v_language_pk)) );
	end if;

	htp.p('
		&nbsp;(CET)<br></td></tr>
		<tr><td colspan="2"><h2><b>'|| html.rem_tag(v_heading) ||'</b></h2></td></tr>
		<tr><td width="90%"><b>'|| html.rem_tag(v_message) ||'</b></td>
		<td rowspan="2" valign="top"><a href="org_page.main?p_organization_pk='|| v_organization_pk ||'">
		<img src="'||get.value('org_logo_dir')||'/'|| v_logo ||'" alt="'||get.txt('back', v_language_pk)||'"  border="0"></a></td>
		</tr>
		<tr><td>&nbsp;</td></tr>
	');


        htp.p('<tr><td colspan="2">');
	html.b_table;
	htp.p('
		<tr><td align="left" width="10%">'||
	        html.popup( get.txt('print_page', v_language_pk),
	        'cal_util.show_calendar?p_calendar_pk='||p_calendar_pk||'&p_command=print','620', '700', '1' )
	        ||'</td><td align="left" width="90%"><b>'||get.txt('written_by', v_language_pk)||':</b>&nbsp;&nbsp;<a href="org_page.main?p_organization_pk='|| v_organization_pk ||'">'||v_name||'</a>
		<br>&nbsp;<br><b>'||get.txt('date', v_language_pk)||':</b>&nbsp;&nbsp;'|| to_char(v_event_date,get.txt('date_long', v_language_pk)) ||'&nbsp;(CET)
		</td></tr>
	');
	html.e_table;
	htp.p('</td></tr>');


	-- sjekker om brukeren og kalenderen har en p�melding tilknyttet
	v_registration_pk := get.reg_event(p_calendar_pk);
	IF ( v_registration_pk IS NOT NULL) THEN
	        IF ( get.has_reg(p_calendar_pk) ) THEN
	                htp.p( '<tr><td align="right" colspan="2">'|| get.txt('entered_button', v_language_pk) ||'</td></tr>');
	        ELSE
	                htp.p('<tr><td align="right" colspan="2"><a href="register.enter_for?p_registration_pk='||v_registration_pk||'">'|| get.txt('enter_for_button', v_language_pk) ||'</a></td></tr>');
	        END IF;
	END IF;

	htp.p('
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr><td align="center" colspan="2">
		<a href="org_page.main?p_organization_pk='|| v_organization_pk ||'">['||v_name||']</a>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="/">['||get.serv_name||']</a>
		</td></tr>
		<tr><td colspan="2">&nbsp;</td></tr>
	');

	html.e_table;
	html.e_box;

        tip.someone;
        html.e_page;


        RETURN v_html;

EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
        html.jump_to('/');
END;
END return_html_calendar;







-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/
