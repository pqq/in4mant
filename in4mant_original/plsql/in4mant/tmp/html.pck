CREATE OR REPLACE PACKAGE html IS

        PROCEDURE startup;
        PROCEDURE write
                (
                        p_name  in html_code.name%type default NULL
                );
        FUNCTION getcode (
                        p_name  in html_code.name%type default NULL
        ) RETURN varchar2;


        PROCEDURE b_form
                (
                        p_action        in varchar2     default NULL,
                        p_name          in varchar2     default 'form',
                        p_target        in varchar2     default NULL
                );
        PROCEDURE e_form;
        PROCEDURE b_table
                (
                        p_width         in varchar2     default '100%',
                        p_border        in varchar2     default '0'
                );
        PROCEDURE e_table;
        PROCEDURE b_box
                (
                        p_title         in varchar2     default 'insert title',
                        p_width         in varchar2     default '100%',
                        p_help_package  in varchar2     default NULL,           -- pakken som hjelpeteksten skal skrives ut for
                        p_archive       in varchar2     default NULL
                );
        PROCEDURE e_box;
        PROCEDURE b_box_2
                (
                        p_title         in varchar2     default 'insert title',
                        p_width         in varchar2     default '100%',
                        p_help_package  in varchar2     default NULL,           -- pakken som hjelpeteksten skal skrives ut for
                        p_archive       in varchar2     default NULL,
			p_type		in number	default NULL
                );
        PROCEDURE e_box_2;
        PROCEDURE b_adm_page
                (
                        p_title         in varchar2     default 'insert title',
                        p_width         in number       default get.value('width'),
                        p_reload        IN NUMBER       DEFAULT NULL
                );
        PROCEDURE b_page
                (
                        p_width         in number       default get.value('width'),
                        p_reload        IN NUMBER       DEFAULT NULL,
                        p_jump          IN VARCHAR2     DEFAULT NULL,
                        p_type          IN NUMBER       DEFAULT NULL,
                        p_selected	IN NUMBER	DEFAULT NULL
                );
        PROCEDURE b_page_2
                (
                        p_width         in number       default get.value('width'),
                        p_reload        IN NUMBER       DEFAULT NULL,
                        p_jump          IN VARCHAR2     DEFAULT NULL,
                        p_type          IN NUMBER       DEFAULT NULL,
                        p_onload        IN VARCHAR2     DEFAULT NULL
                );
        PROCEDURE b_page_3
                (
	                p_width         in number       default get.value('width'),
	                p_reload        IN NUMBER       DEFAULT NULL,
	                p_bgcolor	in varchar2	default NULL
                );
        PROCEDURE e_page;
        PROCEDURE e_page_2;
        PROCEDURE e_page_3;
        PROCEDURE e_adm_page;
        PROCEDURE main_title
                (
                        p_title         in varchar2     default 'insert title'
                );
        PROCEDURE button_link
                (
                        p_text          in varchar2     default NULL,
                        p_href          in varchar2     default NULL,
                        p_target        in varchar2     default '_self'
                );
        PROCEDURE button_link_2
                (
                        p_text          in varchar2     default NULL,
                        p_href          in varchar2     default NULL,
                        p_target        in varchar2     default '_self'
                );
        PROCEDURE submit_link
                (
                        p_text          in varchar2     default NULL,
                        p_form          in varchar2     default 'form',
                        p_name          in varchar2     default NULL
                );
        PROCEDURE select_lang
                (
                        p_language_pk           in la.language_pk%type          default NULL,
                        p_service_pk            in service.service_pk%type      default 0
                );
        PROCEDURE select_service
                (
                        p_service_pk            in service.service_pk%type              default NULL,
                        p_special               in number                               default NULL
                );
        PROCEDURE select_user
		(
			p_user_pk	in usr.user_pk%type	default NULL
		);
        FUNCTION rem_tag
                (
                        p_input                 in varchar2             default NULL
                ) RETURN varchar2;
        PROCEDURE jump_to
                (
                        p_url           in varchar2             default NULL,
                        p_seconds       IN NUMBER               DEFAULT 0,
                        p_message       IN VARCHAR2             DEFAULT NULL
                );
        FUNCTION text
                (
                        p_name          in varchar2             default NULL,
                        p_size          in number               default 30,
                        p_maxlength     in number               default 30,
                        p_value         in varchar2             default NULL
                ) RETURN varchar2;
        FUNCTION popup
                (
                        p_name          in varchar2     default NULL,
                        p_href          in varchar2     default NULL,
                        p_width         in varchar2     default 100,
                        p_height        in varchar2     default 100,
                        p_toolbar       in varchar2     default 0,
                        p_scrollbars	in varchar2	default 0
                ) RETURN varchar2;
        PROCEDURE b_select_jump;
        PROCEDURE e_select_jump;
        PROCEDURE self_close;
        PROCEDURE close
                (
                        p_name          in varchar2     default 'send_name'
                );
        PROCEDURE back
                (
                        p_name          in varchar2     default 'send_name'
                );
        PROCEDURE reset_form
                (
                        p_name                  in varchar2     default 'send_name',
                        p_form_name             in varchar2     default 'form'
                );
        PROCEDURE org_menu
                (
                        p_organization_pk       in organization.organization_pk%type    default NULL
                );
        PROCEDURE main_menu
        	(
			p_selected	in number	default NULL
        	);
        PROCEDURE org_adm_menu
		(
			p_selected	in number	default NULL
		);
        PROCEDURE my_menu;
        PROCEDURE search_menu;
	PROCEDURE home_menu;
	FUNCTION i4_menu
		RETURN varchar2;
	PROCEDURE empty_menu;
	PROCEDURE www_menu;
	PROCEDURE date_field
		(
			p_field_name	in varchar2	default NULL,
			p_field_text	in varchar2	default NULL,
			p_field_value	in date		default NULL,
			p_start_date	in date		default sysdate,
			p_command	in varchar2	default NULL
		);
	PROCEDURE bottom;
	PROCEDURE top
		(
	                p_width         	in number       			DEFAULT get.value('width'),
	                p_reload        	IN NUMBER       			DEFAULT NULL,
	                p_jump          	IN VARCHAR2     			DEFAULT NULL,
	                p_type          	IN NUMBER       			DEFAULT NULL,
	                p_selected		IN NUMBER				DEFAULT NULL,
	                p_organization_pk	IN organization.organization_pk%type	DEFAULT NULL
		);

END;
/
CREATE OR REPLACE PACKAGE BODY html IS

-- **************************************************
-- "PUBLIC METODER" (KALLES DIREKTE FRA WEB)
-- **************************************************

---------------------------------------------------------
-- Name:        startup
-- Type:        procedure
-- What:        start prosedyren, for � kj�re pakken
-- Author:      Frode Klevstul
-- Start date:  30.05.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter] [description]
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

        htp.p('
        <pre>
        HTML package:

        <b>list of procedures:</b>
        name:           in_param:               description:
        ----------------------------------------------------------------------------------------------
        get             p_name                  writes out entry in html_code table where name = p_name

        <b>list of functions:</b>
        name:   in_param:       return:         description:
        ----------------------------------------------------

        </pre>
        ');

END startup;

-- **************************************************
-- "PRIVATE METODER" (KALLES IKKE DIREKTE FRA WEB)
-- **************************************************



---------------------------------------------------------
-- Name:        write
-- Type:        procedure
-- What:        writes out html code from html_code table
-- Author:      Frode Klevstul
-- Start date:  30.05.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
-- beskrivelse av inn parametere:
--
-- [parameter]  [description]
---------------------------------------------------------
PROCEDURE write
        (
                p_name  in html_code.name%type default NULL
        )
IS
        v_code  html_code.code%type default NULL;
BEGIN

        -- henter koden fra databasen
        SELECT code INTO v_code
        FROM html_code
        WHERE name = p_name;

        -- skriver ut til skjerm
        htp.p( v_code );


EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
        htp.p( get.msg(1, 'there is no entry in html_code table with name = <b>'||p_name||'</b>') );
END write;


---------------------------------------------------------
-- Name:        getcode
-- Type:        function
-- What:        returns html code from html_code table
-- Author:      Frode Klevstul
-- Start date:  18.05.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
-- beskrivelse av inn parametere:
--
-- [parameter]  [description]
---------------------------------------------------------
FUNCTION getcode (
                p_name  in html_code.name%type default NULL
        ) RETURN varchar2
IS
        v_code  html_code.code%type default NULL;
BEGIN

        SELECT code INTO v_code
        FROM html_code
        WHERE name = p_name;

        RETURN v_code;

EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
        v_code := get.msg(1, 'there is no entry in html_code table with name = <b>'||p_name||'</b>');
        RETURN v_code;
END getcode;


-- **************************************
-- prosedyrer som erstatter HTML tagger
-- **************************************

---------------------------------------------------------
-- Name:        b_form
-- HTML:        <FORM>
-- Author:      Frode Klevstul, 30.05.2000
---------------------------------------------------------
PROCEDURE b_form
        (
                p_action        in varchar2     default NULL,
                p_name          in varchar2     default 'form',
                p_target        in varchar2     default NULL
        )
IS
        v_code  varchar2(4000)          default NULL;
BEGIN

        v_code := getcode('b_form');
        v_code := REPLACE (v_code, '[target]', p_target );
        v_code := REPLACE (v_code, '[action]', p_action );
        v_code := REPLACE (v_code, '[name]', p_name );
        htp.p( v_code );

END b_form;

---------------------------------------------------------
-- Name:        e_form
-- HTML:        </FORM>
-- Author:      Frode Klevstul, 30.05.2000
---------------------------------------------------------
PROCEDURE e_form
IS
BEGIN

        htp.p('</form>');

END e_form;

---------------------------------------------------------
-- Name:        b_table
-- HTML:        <TABLE>
-- Author:      Frode Klevstul, 30.05.2000
---------------------------------------------------------
PROCEDURE b_table
        (
                p_width         in varchar2     default '100%',
                p_border        in varchar2     default '0'
        )
IS
        v_code  varchar2(4000)          default NULL;
BEGIN

        v_code := getcode('b_table');
        v_code := REPLACE (v_code, '[width]', p_width );
        v_code := REPLACE (v_code, '[border]', p_border );
        htp.p( v_code );

END b_table;

---------------------------------------------------------
-- Name:        e_table
-- HTML:        </TABLE>
-- Author:      Frode Klevstul, 30.05.2000
---------------------------------------------------------
PROCEDURE e_table
IS
BEGIN

        htp.p('</table>');

END e_table;



---------------------------------------------------------
-- Name:        b_box
-- HTML:
-- Author:      Frode Klevstul, 30.05.2000
---------------------------------------------------------
PROCEDURE b_box
        (
                p_title         in varchar2     default 'insert title',
                p_width         in varchar2     default '100%',
                p_help_package  in varchar2     default NULL,           -- pakken som hjelpeteksten skal skrives ut for
                p_archive       in varchar2     default NULL
        )
IS
        v_code          varchar2(4000)          default NULL;
	v_service_name  VARCHAR2(50)            DEFAULT NULL;
BEGIN

	v_service_name := get.serv_name;

        v_code := getcode('b_box');
        v_code := REPLACE (v_code, '[title]', p_title );
        v_code := REPLACE (v_code, '[width]', p_width );
        v_code := REPLACE (v_code, '[c_service_mmb]' , get.value( 'c_'|| v_service_name ||'_mmb' ) );

        if (p_help_package IS NOT NULL) then
                v_code := REPLACE (v_code, '<!-- ht -->' , html.popup( get.txt('?'), 'help.module?p_package='|| p_help_package , 400, 500, 0, 1) );
        end if;
        if (p_archive IS NOT NULL) then
                v_code := REPLACE (v_code, '<!-- archive -->','<a href="'||p_archive||'">'||get.txt('archive')||'</a>' );
        end if;

        htp.p( v_code );

END b_box;



---------------------------------------------------------
-- Name:        e_box
-- HTML:
-- Author:      Frode Klevstul, 30.05.2000
---------------------------------------------------------
PROCEDURE e_box
IS
BEGIN

        write('e_box');

END e_box;



---------------------------------------------------------
-- Name:        b_box_2
-- HTML:
-- Author:      Frode Klevstul, 28.12.2000
---------------------------------------------------------
PROCEDURE b_box_2
        (
                p_title         in varchar2     default 'insert title',
                p_width         in varchar2     default '100%',
                p_help_package  in varchar2     default NULL,           -- pakken som hjelpeteksten skal skrives ut for
                p_archive       in varchar2     default NULL,
		p_type		in number	default NULL
        )
IS
        v_code          varchar2(4000)          default NULL;
        v_service_name  VARCHAR2(50)            DEFAULT NULL;
BEGIN

        v_service_name := get.serv_name;
        v_code := getcode('b_box_2');
        v_code := REPLACE (v_code, '[title]', p_title );
        v_code := REPLACE (v_code, '[width]', p_width );
        v_code := REPLACE (v_code, '[c_service_bbs]' , get.value( 'c_'|| v_service_name ||'_bbs' ) );
        v_code := REPLACE (v_code, '[c_service_mmb]' , get.value( 'c_'|| v_service_name ||'_mmb' ) );

	if ( p_type = 2 ) then -- dersom man skal bruke denne boksen med c_service_mms som bakgrunnsfarge s� byttes de runde hj�rnene
		v_code := REPLACE (v_code, 'ul.gif', 'ul2.gif');
		v_code := REPLACE (v_code, 'ur.gif', 'ur2.gif');
	end if;

        if (p_help_package IS NOT NULL) then
                v_code := REPLACE (v_code, '<!-- ht -->' , html.popup( get.txt('?2'), 'help.module?p_package='|| p_help_package , 400, 500, 0, 1) );
        end if;

        if (p_archive IS NOT NULL) then
                v_code := REPLACE (v_code, '<!-- archive -->','<a href="'||p_archive||'">'||get.txt('archive2')||'</a>' );
        end if;

        htp.p( v_code );

END b_box_2;


---------------------------------------------------------
-- Name:        e_box_2
-- HTML:
-- Author:      Frode Klevstul, 28.12.2000
---------------------------------------------------------
PROCEDURE e_box_2
IS
BEGIN

        write('e_box_2');

END e_box_2;


---------------------------------------------------------
-- Name:        b_adm_page
-- HTML:
-- Author:      Frode Klevstul, 30.05.2000
---------------------------------------------------------
PROCEDURE b_adm_page
        (
                p_title         in varchar2     default 'insert title',
                p_width         in number       default get.value('width'),
                p_reload        IN NUMBER       DEFAULT NULL
        )
IS
BEGIN
DECLARE
        v_code  varchar2(4000)          default NULL;
BEGIN

        b_page_2(p_width,p_reload);
        write('adm_menu');
        main_title(p_title);

END;
END b_adm_page;



---------------------------------------------------------
-- Name:        b_page
-- HTML:
-- Author:      Frode Klevstul, 07.07.2000
---------------------------------------------------------
PROCEDURE b_page
        (
                p_width         in number       default get.value('width'),
                p_reload        IN NUMBER       DEFAULT NULL,
                p_jump          IN VARCHAR2     DEFAULT NULL,
                p_type          IN NUMBER       DEFAULT NULL,
                p_selected	IN NUMBER	DEFAULT NULL
        )
IS
BEGIN
DECLARE
        v_code          varchar2(5000)                  default NULL;
        v_tmp           varchar2(200)                   default NULL;
        v_service_name  service.description%TYPE        default NULL;
        v_service_pk    service.service_pk%TYPE         DEFAULT NULL;
        v_width         varchar2(10)                    DEFAULT NULL;
	v_path		varchar2(100)			default NULL;

BEGIN

        IF (p_width IS NULL) THEN
                v_width := get.value('width');
        ELSE
                v_width := p_width;
        END IF;

        v_service_name 	:= get.serv_name;
        v_service_pk	:= get.serv;

        v_code := getcode('b_page');
        IF ( p_reload IS NOT NULL ) THEN
                v_code := REPLACE (v_code, '[reload]', '<META HTTP-EQUIV="REFRESH" CONTENT="'||p_reload||'">' );
        ELSE
                v_code := REPLACE (v_code, '[reload]', '');
        END IF;
        IF ( p_jump IS NOT NULL ) THEN
                v_code := REPLACE (v_code, '[jump]', '<META HTTP-EQUIV="REFRESH" Content="'||p_reload||';URL='||p_jump||'">' );
        ELSE
                v_code := REPLACE (v_code, '[jump]', '');
        END IF;

        -- legger inn banner utifra tjeneste man er inne p�
        IF (v_service_pk = 1) THEN
        	v_code := REPLACE (v_code, '[banner_link]', html.getcode('banner_student') );
        ELSIF (v_service_pk = 5) THEN
        	v_code := REPLACE (v_code, '[banner_link]', html.getcode('banner_studnyhet') );
        ELSIF (v_service_pk = 3) THEN
        	v_code := REPLACE (v_code, '[banner_link]', html.getcode('banner_uteliv') );
	ELSE
        	v_code := REPLACE (v_code, '[banner_link]', html.getcode('banner_general') );
        END IF;

        v_code := REPLACE (v_code, '[icons_dir]', get.value('icons_dir') );
        v_code := REPLACE (v_code, '[width]', v_width );
        v_code := REPLACE (v_code, '[css]', getcode('css') );
        v_code := REPLACE (v_code, '[javascript]', html.getcode('javascript') );
        v_code := REPLACE (v_code, '[ad_button]', get.txt('ad_button') );
        v_code := REPLACE (v_code, '[c_service_bbm]' , get.value( 'c_'|| v_service_name ||'_bbm' ) );
        v_code := REPLACE (v_code, '[c_service_mmb]' , get.value( 'c_'|| v_service_name ||'_mmb' ) );
        v_code := REPLACE (v_code, '[c_service_bbs]' , get.value( 'c_'|| v_service_name ||'_bbs' ) );

        htp.p(v_code);


	-- man kan sende med p_type for � spare tid, hvis ikke hentes HTTP_REFERER
        if (p_type = 1) then
                html.main_menu(p_selected);
        elsif (p_type = 2) then
                html.org_adm_menu(p_selected);
	elsif (p_type = 3) then
		html.www_menu;
	elsif (p_type IS NULL) then
		v_path := owa_util.get_cgi_env('SERVER_NAME');

		if ( owa_pattern.match( v_path, 'www.in4mant.com', 'i') or owa_pattern.match( v_path, '^in4mant.com', 'i') ) then
			html.www_menu;
		elsif ( owa_pattern.match( v_path, 'admin.in4mant.com', 'i') ) then
	                html.org_adm_menu;
		elsif ( owa_pattern.match( v_path, '.in4mant.com', 'i') ) then
	                html.main_menu(p_selected);
		end if;

        end if;


END;
END b_page;


---------------------------------------------------------
-- Name:        b_page_2
-- HTML:
-- Author:      Frode Klevstul, 07.07.2000
---------------------------------------------------------
PROCEDURE b_page_2
        (
                p_width         in number       default get.value('width'),
                p_reload        IN NUMBER       DEFAULT NULL,
                p_jump          IN VARCHAR2     DEFAULT NULL,
                p_type          IN NUMBER       DEFAULT NULL,
                p_onload        IN VARCHAR2     DEFAULT NULL
        )
IS
BEGIN
DECLARE
        v_code  varchar2(5000)          default NULL;
BEGIN

        v_code := getcode('b_page_2');
        IF ( p_reload IS NOT NULL ) THEN
                v_code := REPLACE (v_code, '[reload]', '<META HTTP-EQUIV="REFRESH" CONTENT="'||p_reload||'">' );
        ELSE
                v_code := REPLACE (v_code, '[reload]', '');
        END IF;
        IF ( p_jump IS NOT NULL ) THEN
                v_code := REPLACE (v_code, '[jump]', '<META HTTP-EQUIV="REFRESH" Content="'||p_reload||';URL='||p_jump||'">' );
        ELSE
                v_code := REPLACE (v_code, '[jump]', '');
        END IF;
        IF ( p_onload IS NOT NULL ) THEN
                v_code := REPLACE (v_code, '[onload]', ' onload="'||p_onload||'"' );
        ELSE
                v_code := REPLACE (v_code, '[onload]', '');
        END IF;
        v_code := REPLACE (v_code, '[width]', p_width );
        v_code := REPLACE (v_code, '[css]', getcode('css') );
        v_code := REPLACE (v_code, '[javascript]', html.getcode('javascript') );
        v_code := REPLACE (v_code, '[c_service_bbm]' , get.value( 'c_'|| get.serv_name ||'_bbm' ) );
        v_code := REPLACE (v_code, '[c_service_bbs]' , get.value( 'c_'|| get.serv_name ||'_bbs' ) );
        htp.p(v_code);

END;
END b_page_2;

---------------------------------------------------------
-- Name:        b_page_3
-- HTML:
-- Author:      Frode Klevstul, 07.07.2000
---------------------------------------------------------
PROCEDURE b_page_3
        (
                p_width         in number       default get.value('width'),
                p_reload        IN NUMBER       DEFAULT NULL,
                p_bgcolor	in varchar2	default NULL
        )
IS
BEGIN
DECLARE
        v_code  varchar2(5000)          default NULL;
BEGIN

        v_code := getcode('b_page_3');
        IF ( p_reload IS NOT NULL ) THEN
                v_code := REPLACE (v_code, '[reload]', '<META HTTP-EQUIV="REFRESH" CONTENT="'||p_reload||'">' );
        ELSE
                v_code := REPLACE (v_code, '[reload]', '');
        END IF;
        v_code := REPLACE (v_code, '[width]', p_width );
        v_code := REPLACE (v_code, '[css]', getcode('css') );
        v_code := REPLACE (v_code, '[javascript]', html.getcode('javascript') );

        if (p_bgcolor IS NULL) then
	       v_code := REPLACE (v_code, '[c_service_bbs]' , get.value( 'c_'|| get.serv_name ||'_bbs' ) );
	else
	       v_code := REPLACE (v_code, '[c_service_bbs]' , p_bgcolor );
	end if;

        htp.p(v_code);

END;
END b_page_3;

---------------------------------------------------------
-- Name:        e_page
-- HTML:
-- Author:      Frode Klevstul, 30.05.2000
---------------------------------------------------------
PROCEDURE e_page
IS
        v_code  varchar2(4000)          default NULL;

BEGIN

        v_code := getcode('e_page');
        v_code := REPLACE (v_code, '[icons_dir]', get.value('icons_dir') );
        v_code := REPLACE (v_code, '[c_service_mmb]' , get.value( 'c_'|| get.serv_name ||'_mmb' ) );
        htp.p( v_code );

END e_page;


---------------------------------------------------------
-- Name:        e_page_2
-- HTML:
-- Author:      Frode Klevstul, 22.08.2000
---------------------------------------------------------
PROCEDURE e_page_2
IS
        v_code  varchar2(4000)          default NULL;

BEGIN

        write('e_page_2');

END e_page_2;



---------------------------------------------------------
-- Name:        e_page_3
-- HTML:
-- Author:      Frode Klevstul, 22.08.2000
---------------------------------------------------------
PROCEDURE e_page_3
IS
        v_code  varchar2(4000)          default NULL;

BEGIN

        write('e_page_3');

END e_page_3;



---------------------------------------------------------
-- Name:        e_adm_page
-- HTML:
-- Author:      Frode Klevstul, 07.07.2000
---------------------------------------------------------
PROCEDURE e_adm_page
IS
BEGIN
DECLARE
        v_code  varchar2(4000)          default NULL;
BEGIN

        htp.p('<br><br><br><br>');
        back('<--');
        v_code := getcode('e_adm_page');
        htp.p(v_code);

END;
END e_adm_page;



---------------------------------------------------------
-- Name:        main_title
-- HTML:
-- Author:      Frode Klevstul, 30.05.2000
---------------------------------------------------------
PROCEDURE main_title
        (
                p_title         in varchar2     default 'insert title'
        )
IS
        v_code  varchar2(4000)          default NULL;
BEGIN

        v_code := getcode('main_title');
        v_code := REPLACE (v_code, '[title]', p_title );
        v_code := REPLACE (v_code, '[c_service_mmb]' , get.value( 'c_'|| get.serv_name ||'_mmb' ) );
        htp.p( v_code );

END main_title;



---------------------------------------------------------
-- Name:        button_link
-- HTML:
-- Author:      Frode Klevstul, 08.06.2000
---------------------------------------------------------
PROCEDURE button_link
        (
                p_text          in varchar2     default NULL,
                p_href          in varchar2     default NULL,
                p_target        in varchar2     default '_self'
        )
IS
        v_code  html_code.code%type             default NULL;
BEGIN

        v_code := getcode('button_link');
        v_code := REPLACE (v_code, '[href]', p_href );
        v_code := REPLACE (v_code, '[text]', p_text );
        v_code := REPLACE (v_code, '[target]', p_target );
        htp.p( v_code );

END button_link;



---------------------------------------------------------
-- Name:        button_link_2
-- HTML:
-- Author:      Frode Klevstul, 16.08.2000
---------------------------------------------------------
PROCEDURE button_link_2
        (
                p_text          in varchar2     default NULL,
                p_href          in varchar2     default NULL,
                p_target        in varchar2     default '_self'
        )
IS
        v_code  html_code.code%type             default NULL;
BEGIN

	if (p_target = '_top') then
		v_code := '<a href="JavaScript:OpenLink('''|| p_href ||''')">'|| p_text ||'</a>';
	else
	        v_code := getcode('button_link_2');
	        v_code := REPLACE (v_code, '[href]', p_href );
	        v_code := REPLACE (v_code, '[text]', p_text );
	        v_code := REPLACE (v_code, '[target]', p_target );
	end if;

        htp.p( v_code );

END button_link_2;

---------------------------------------------------------
-- Name:        submit_link
-- HTML:
-- Author:      Frode Klevstul, 08.06.2000
---------------------------------------------------------
PROCEDURE submit_link
        (
                p_text          in varchar2     default NULL,
                p_form          in varchar2     default 'form',
                p_name          in varchar2     default NULL
        )
IS
        v_code  html_code.code%type             default NULL;
BEGIN

        if ( p_name is not null ) then
                v_code := getcode(p_name);
        else
                v_code := getcode('submit_link');
        end if;
        v_code := REPLACE (v_code, '[form]', p_form );
        v_code := REPLACE (v_code, '[text]', p_text );
        v_code := REPLACE (v_code, '[name]', '' );
        htp.p( v_code );

END submit_link;






---------------------------------------------------------
-- Name:        select_lang
-- Type:        procedure
-- What:        writes out the html code for a select box
--              with all languages in the database.
-- Author:      Frode Klevstul
-- Start date:  27.05.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter] [description]
---------------------------------------------------------
PROCEDURE select_lang
                (
                        p_language_pk           in la.language_pk%type          default NULL,
                        p_service_pk            in service.service_pk%type      default 0       -- '0' er all_services
                )
IS
BEGIN
DECLARE

        CURSOR  all_languages_special IS
        SELECT  language_pk, language_name
        FROM    la, service_on_lang
        WHERE   language_fk = language_pk
        AND     service_fk = 0
        ORDER BY language_name;


        CURSOR  all_languages IS
        SELECT  language_pk, language_name
        FROM    la, service_on_lang
        WHERE   language_fk = language_pk
        AND     service_fk = p_service_pk
        ORDER BY language_name;

        v_language_pk           la.language_pk%type             default NULL;
        v_language_name         la.language_name%type           default NULL;

BEGIN

        htp.p('<select name="p_language_pk">');

        -- dersom det sendes med -1000 inn, s� listes det ut ALLE spr�k (ingen begrensninger)
        if (p_service_pk = -1000) then
                -- forutsetter at det finnes et spr�k "0" (hvis ikke stopper programmet/f�r feil)
                SELECT  language_name INTO v_language_name
                FROM    la
                WHERE   language_pk = 0;
                htp.p('<option value="0">'|| v_language_name ||'</option>');

                open all_languages_special;
                while (1>0)     -- alltid sant -> g�r igjennom alle forekomstene
                loop
                        fetch all_languages_special into v_language_pk, v_language_name;
                        exit when all_languages_special%NOTFOUND;
                        if (v_language_pk = p_language_pk) then
                                htp.p('<option value="'|| v_language_pk ||'" selected>'|| v_language_name ||'</option>');
                        else
                                htp.p('<option value="'|| v_language_pk ||'">'|| v_language_name ||'</option>');
                        end if;
                end loop;
                close all_languages_special;
        else
                open all_languages;
                while (1>0)     -- alltid sant -> g�r igjennom alle forekomstene
                loop
                        fetch all_languages into v_language_pk, v_language_name;
                        exit when all_languages%NOTFOUND;
                        if (v_language_pk = p_language_pk) then
                                htp.p('<option value="'|| v_language_pk ||'" selected>'|| v_language_name ||'</option>');
                        else
                                htp.p('<option value="'|| v_language_pk ||'">'|| v_language_name ||'</option>');
                        end if;
                end loop;
                close all_languages;
        end if;

        htp.p('</select>');

END;
END select_lang;





---------------------------------------------------------
-- Name:        select_service
-- Type:        procedure
-- What:        writes out the html code for a select box
--              with all service in the database.
-- Author:      Frode Klevstul
-- Start date:  09.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE select_service
                (
                        p_service_pk            in service.service_pk%type              default NULL,
                        p_special               in number                               default NULL
                )
IS
BEGIN
DECLARE

        CURSOR  all_services IS
        SELECT  service_pk, name_sg_fk
        FROM    service
        WHERE   service_pk <> 0
        AND     activated > 0;

        CURSOR  all_services_special IS
        SELECT  service_pk, name_sg_fk
        FROM    service;

        v_service_pk            service.service_pk%type                 default NULL;
        v_name_sg_fk            service.name_sg_fk%type                 default NULL;

BEGIN

        htp.p('<select name="p_service_pk">');
        IF ( p_special = 0 ) THEN
           htp.p('<option value="0">'|| get.txt('no_service') ||'</option>');
        END IF;
        if (p_special = -1000) then
                open all_services_special;
                loop
                        fetch all_services_special into v_service_pk, v_name_sg_fk;
                        exit when all_services_special%NOTFOUND;
                        if (v_service_pk = p_service_pk) then
                                htp.p('<option value="'|| v_service_pk ||'" selected>'|| get.text(v_name_sg_fk) ||'</option>');
                        else
                                htp.p('<option value="'|| v_service_pk ||'">'|| get.text(v_name_sg_fk) ||'</option>');
                        end if;
                end loop;
                close all_services_special;
        else
                open all_services;
                loop
                        fetch all_services into v_service_pk, v_name_sg_fk;
                        exit when all_services%NOTFOUND;
                        if (v_service_pk = p_service_pk) then
                                htp.p('<option value="'|| v_service_pk ||'" selected>'|| get.text(v_name_sg_fk) ||'</option>');
                        else
                                htp.p('<option value="'|| v_service_pk ||'">'|| get.text(v_name_sg_fk) ||'</option>');
                        end if;
                end loop;
                close all_services;
        end if;

        htp.p('</select>');

END;
END select_service;





---------------------------------------------------------
-- Name:        select_user
-- Type:        procedure
-- What:        writes out the html code for a select box
--              with all users in the database.
-- Author:      Frode Klevstul
-- Start date:  21.06.2000
-- Desc:
---------------------------------------------------------
PROCEDURE select_user
	(
		p_user_pk	in usr.user_pk%type	default NULL
	)
IS
BEGIN
DECLARE

        CURSOR  select_all IS
        SELECT  user_pk, login_name
        FROM    usr
        WHERE   user_type_fk = -3
        OR      user_type_fk = -4
        ORDER BY login_name;

        v_user_pk               usr.user_pk%type                default NULL;
        v_login_name            usr.login_name%type             default NULL;

BEGIN

        htp.p('<select name="p_user_pk">');

        open select_all;
        while (1>0)     -- alltid sant -> g�r igjennom alle forekomstene
        loop
                fetch select_all into v_user_pk, v_login_name;
                exit when select_all%NOTFOUND;

                if (p_user_pk = v_user_pk) then
                	htp.p('<option value="'|| v_user_pk ||'" selected>'|| v_login_name ||'</option>');
                else
                	htp.p('<option value="'|| v_user_pk ||'">'|| v_login_name ||'</option>');
                end if;
        end loop;
        close select_all;

        htp.p('</select>');

END;
END select_user;




---------------------------------------------------------
-- Name:        rem_tag
-- Type:        function
-- What:        for use when to write out html code/tags etc.
--              as plain text. "remove tag"
-- Author:      Frode Klevstul
-- Start date:  08.06.2000
-- Desc:        NB! Strengen som returneres er lengre enn den som tas
--              imot. Dette kan f�re til feil dersom strengen mottas i en
--              variabel som er deklart som %type av en datatype.
--              eks. kolonne%type = varchar2(5). Strengen er '<1>',
--              returstreng blir da '&lt;1&gt;' som er 9 tegn.
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter] [description]
---------------------------------------------------------
FUNCTION rem_tag
                (
                        p_input                 in varchar2             default NULL
                ) RETURN varchar2
IS
BEGIN
DECLARE

        v_output        varchar2(32000)  default NULL;

BEGIN

        v_output := p_input;

        v_output := REPLACE (v_output, '<', '&lt;');
        v_output := REPLACE (v_output, '>', '&gt;');
        v_output := REPLACE (v_output, '"', '&quot;' );
        v_output := REPLACE (v_output, '+', '&#043;' );
        v_output := REPLACE (v_output, '�', '&quot;' );

-- erstatter 'return' med '<br>' (vet ikke hvilket tegn som brukes for return, '\n' funker ikke)
v_output := REPLACE (v_output, '
', '<br>
');

        return(v_output);


END;
END rem_tag;




---------------------------------------------------------
-- Name:        jump_to
-- Type:        procedure
-- What:        makes a location jump to given url
-- Author:      Frode Klevstul
-- Start date:  11.06.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter] [description]
---------------------------------------------------------
PROCEDURE jump_to
        (
                p_url           in varchar2             default NULL,
                p_seconds       IN NUMBER               DEFAULT 0,
                p_message       IN VARCHAR2             DEFAULT NULL
        )
IS
BEGIN

        htp.p('
                <html>
                <head>
                <META HTTP-EQUIV="Refresh" CONTENT="'||p_seconds||'; URL='||trans(p_url)||'">

                <title>in4mant.com</title>
                </head>

                <body>
                <!-- send user to "p_url" -->
                <font color="red">'||p_message||'</font>
                </body>
                </html>
        ');

END jump_to;




---------------------------------------------------------
-- Name:        text
-- Type:        function
-- What:        writes out '<input type="text">' tag
-- Author:      Frode Klevstul
-- Start date:  15.06.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter] [description]
---------------------------------------------------------
FUNCTION text(
        p_name          in varchar2             default NULL,
        p_size          in number               default 30,
        p_maxlength     in number               default 30,
        p_value         in varchar2             default NULL
) RETURN varchar2
IS
BEGIN

        RETURN '<input type="text" name="'||p_name||'" size="'||p_size||'" maxlength="'||p_maxlength||'" maxlen="'||p_maxlength||'" value="'||p_value||'">';

END text;




---------------------------------------------------------------------
-- Name: popup
-- Type: function
-- What: generates a link to a popup window
-- Made: Frode Klevstul
-- Date: 20.06.2000
-- Chng:
---------------------------------------------------------------------
FUNCTION popup
        (
                p_name          in varchar2     default NULL,
                p_href          in varchar2     default NULL,
                p_width         in varchar2     default 100,
                p_height        in varchar2     default 100,
                p_toolbar       in varchar2     default 0,
		p_scrollbars	in varchar2	default 0
        ) RETURN varchar2
IS
BEGIN
DECLARE
        v_code  html_code.code%type             default NULL;
BEGIN

        v_code := getcode('popup');
        v_code := REPLACE (v_code, '[href]', p_href );
        v_code := REPLACE (v_code, '[name]', p_name );
        v_code := REPLACE (v_code, '[width]', p_width );
        v_code := REPLACE (v_code, '[height]', p_height );
	v_code := REPLACE (v_code, '[toolbar]', p_toolbar );
	v_code := REPLACE (v_code, '[scrollbars]', p_scrollbars );
	v_code := REPLACE (v_code, '[window]', 'win_'||to_char(sysdate, 'SSSSS') );

        IF ( p_toolbar > 0 and p_scrollbars > 0) THEN
                v_code := REPLACE (v_code, 'openWin', 'openWin2');
	ELSIF (p_scrollbars > 0) THEN
                v_code := REPLACE (v_code, 'openWin', 'openWin3');
	ELSIF (p_toolbar > 0) THEN
                v_code := REPLACE (v_code, 'openWin', 'openWin4');
	END IF;

        RETURN v_code;

END;
END popup;


---------------------------------------------------------------------
-- Name: b_select_jump
-- Type: procedure
-- What: writes out javascript code for <select> jump
-- Made: Frode Klevstul
-- Date: 07.07.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE b_select_jump
IS
BEGIN

        htp.p('
                <script language="Javascript">
                <!--
                function select_jump(form) {
                 var dest=form.dest.options[form.dest.selectedIndex].value;

                if ( dest != '''') {
                        window.location = dest;
                 }

                }
                //-->
                </script>

                <form>
                <select name="dest" onchange="select_jump(this.form)">
        ');

END b_select_jump;

---------------------------------------------------------------------
-- Name: e_select_jump
-- Type: procedure
-- What: end's html code for select jump
-- Made: Frode Klevstul
-- Date: 07.07.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE e_select_jump
IS
BEGIN

        htp.p('
                </select></form>
        ');

END e_select_jump;



---------------------------------------------------------------------
-- Name: self_close
-- Type: procedure
-- What: closes a window
-- Made: Frode Klevstul
-- Date: 08.07.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE self_close
IS
BEGIN
        htp.p('
                <SCRIPT language=JavaScript>
                        self.close();
                </SCRIPT>
        ');

END self_close;


---------------------------------------------------------------------
-- Name: close
-- Type: procedure
-- What: writes out a link to close a window
-- Made: Frode Klevstul
-- Date: 11.07.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE close
        (
                p_name          in varchar2     default 'send_name'
        )
IS
BEGIN
DECLARE
        v_code  html_code.code%type             default NULL;
BEGIN

        v_code := getcode('close');
        v_code := REPLACE (v_code, '[name]', p_name );
        htp.p( v_code );

END;
END close;


---------------------------------------------------------------------
-- Name: back
-- Type: procedure
-- What: writes out a code for history back
-- Made: Frode Klevstul
-- Date: 15.07.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE back
        (
                p_name          in varchar2     default 'send_name'
        )
IS
BEGIN
DECLARE
        v_code  html_code.code%type             default NULL;
BEGIN

        v_code := getcode('history_back');
        v_code := REPLACE (v_code, '[name]', p_name );
        htp.p( v_code );

END;
END back;


---------------------------------------------------------------------
-- Name: reset_form
-- Type: procedure
-- What: writes out a code for resetting a form
-- Made: Frode Klevstul
-- Date: 19.07.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE reset_form
        (
                p_name                  in varchar2     default 'send_name',
                p_form_name             in varchar2     default 'form'
        )
IS
BEGIN
DECLARE
        v_code  html_code.code%type             default NULL;
BEGIN

        v_code := getcode('reset_form');
        v_code := REPLACE (v_code, '[name]', p_name );
        v_code := REPLACE (v_code, '[form_name]', p_form_name );
        htp.p( v_code );

END;
END reset_form;




---------------------------------------------------------------------
-- Name: org_menu
-- Type: procedure
-- What: writes out an menu for an organization
-- Made: Frode Klevstul
-- Date: 15.08.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE org_menu
        (
                p_organization_pk       in organization.organization_pk%type    default NULL
        )
IS
BEGIN
DECLARE
        v_code  html_code.code%type             default NULL;
BEGIN

        v_code := getcode('org_menu');
        v_code := REPLACE (v_code, '[olink]', get.olink(p_organization_pk) );
        v_code := REPLACE (v_code, '[org_pk]', p_organization_pk );
        v_code := REPLACE (v_code, '[lan_pk]', get.lan );

        v_code := REPLACE (v_code, '[org_page_button]', get.txt('org_page_button') );
        v_code := REPLACE (v_code, '[org_news_button]', get.txt('org_news_button') );
        v_code := REPLACE (v_code, '[org_calendar_button]', get.txt('org_calendar_button') );
        v_code := REPLACE (v_code, '[org_fora_button]', get.txt('org_fora_button') );
        v_code := REPLACE (v_code, '[org_album_button]' , get.txt('org_album_button') );
        v_code := REPLACE (v_code, '[i4_menu]' , html.i4_menu );
        htp.p( v_code );

END;
END org_menu;



---------------------------------------------------------------------
-- Name: main_menu
-- Type: procedure
-- What: writes out the main menu
-- Made: Frode Klevstul
-- Date: 15.08.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE main_menu(
		p_selected	in number	default NULL
	)
IS
BEGIN
DECLARE
        v_code  html_code.code%type             default NULL;
BEGIN

        v_code := getcode('main_menu');

        if (p_selected = 1) then
	       v_code := REPLACE (v_code, '[home_button]', get.txt('home_button2') );
	else
	       v_code := REPLACE (v_code, '[home_button]', get.txt('home_button') );
	end if;

	if (p_selected = 2) then
	        v_code := REPLACE (v_code, '[my_page_button]', get.txt('my_page_button2') );
	else
	        v_code := REPLACE (v_code, '[my_page_button]', get.txt('my_page_button') );
	end if;

	if (p_selected = 3) then
	        v_code := REPLACE (v_code, '[organizations_button]', get.txt('organizations_button2') );
	else
	        v_code := REPLACE (v_code, '[organizations_button]', get.txt('organizations_button') );
	end if;

        v_code := REPLACE (v_code, '[c_service_mmb]' , get.value( 'c_'|| get.serv_name ||'_mmb' ) );
        htp.p( v_code );

END;
END main_menu;



---------------------------------------------------------------------
-- Name: org_adm_menu
-- Type: procedure
-- What: writes out the main menu
-- Made: Frode Klevstul
-- Date: 15.08.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE org_adm_menu
	(
		p_selected	in number	default NULL
	)
IS
BEGIN
DECLARE
        v_code  		html_code.code%type             	default NULL;
        v_organization_pk	organization.organization_pk%type	default NULL;

BEGIN

	v_organization_pk := get.oid;

        v_code := getcode('org_adm_menu');
        v_code := REPLACE (v_code, '[c_service_mmb]' , get.value( 'c_'|| get.serv_name ||'_mmb' ) );

	if (p_selected = 1) then
	        v_code := REPLACE (v_code, '[org_adm_home_button]', get.txt('org_adm_home_button2') );
	else
	        v_code := REPLACE (v_code, '[org_adm_home_button]', get.txt('org_adm_home_button') );
	end if;

	if (p_selected = 2) then
	        v_code := REPLACE (v_code, '[org_adm_tools_button]', get.txt('org_adm_tools_button2') );
	else
	        v_code := REPLACE (v_code, '[org_adm_tools_button]', get.txt('org_adm_tools_button') );
	end if;


	-- dersom brukeren er logget inn viser vi logout button, hvis ikke vises login button
        if ( login.timeout(NULL, 1) > 0) then
	        v_code := REPLACE (v_code, '[login_status_button]', get.txt('logout_button') );
	        v_code := REPLACE (v_code, '[login_status_url]', 'login.logout' );

	        v_code := REPLACE (v_code, '[org_home]', '<a href="[olink]org_page.main?p_organization_pk=[org_pk]" target="new_window">[org_page_button]</a>');
	        v_code := REPLACE (v_code, '[olink]', get.olink(v_organization_pk) );
	        v_code := REPLACE (v_code, '[org_pk]', v_organization_pk );
		v_code := REPLACE (v_code, '[org_page_button]', get.txt('org_page_button') );
	else
	        v_code := REPLACE (v_code, '[login_status_button]', get.txt('login_button') );
	        v_code := REPLACE (v_code, '[login_status_url]', 'admin.startup' );

	        v_code := REPLACE (v_code, '[org_home]', NULL);
	end if;


        v_code := REPLACE (v_code, '[contact_us_button]', get.txt('contact_us_button') );
        v_code := REPLACE (v_code, '[about_button]', get.txt('about_button') );
        v_code := REPLACE (v_code, '[help_button]', get.txt('help_button') );
        htp.p( v_code );

END;
END org_adm_menu;

---------------------------------------------------------------------
-- Name: my_menu
-- Type: procedure
-- What: writes out the my menu
-- Made: Frode Klevstul
-- Date: 20.08.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE my_menu
IS
BEGIN
DECLARE
        v_code  html_code.code%type             default NULL;
BEGIN

        v_code := getcode('my_menu');
        v_code := REPLACE (v_code, '[my_result_page_button]', get.txt('my_result_page_button') );
        v_code := REPLACE (v_code, '[my_subscribtion_button]', get.txt('my_subscribtion_button') );
        v_code := REPLACE (v_code, '[my_registrations_button]', get.txt('my_registrations_button') );
        v_code := REPLACE (v_code, '[my_information_button]', get.txt('my_information_button') );
        v_code := REPLACE (v_code, '[i4_menu]' , html.i4_menu );
        htp.p( v_code );

END;
END my_menu;



---------------------------------------------------------------------
-- Name: search_menu
-- Type: procedure
-- What: writes out search menu
-- Made: Frode Klevstul
-- Date: 22.08.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE search_menu
IS
BEGIN
DECLARE
        v_code  html_code.code%type             default NULL;
BEGIN

        v_code := getcode('search_menu');
        v_code := REPLACE (v_code, '[org_search_button]', get.txt('org_search_button') );
        v_code := REPLACE (v_code, '[doc_search_button]', get.txt('doc_search_button') );
        v_code := REPLACE (v_code, '[cal_search_button]', get.txt('cal_search_button') );
        v_code := REPLACE (v_code, '[fora_search_button]', get.txt('fora_search_button') );
        v_code := REPLACE (v_code, '[album_search_button]', get.txt('album_search_button') );
        v_code := REPLACE (v_code, '[bboard_search_button]', get.txt('bboard_search_button') );
        v_code := REPLACE (v_code, '[i4_menu]' , html.i4_menu );
        htp.p( v_code );

END;
END search_menu;




---------------------------------------------------------------------
-- Name: home_menu
-- Type: procedure
-- What: writes out home menu
-- Made: Frode Klevstul
-- Date: 03.01.2001
-- Chng:
---------------------------------------------------------------------
PROCEDURE home_menu
IS
BEGIN
DECLARE
        v_code  	html_code.code%type             default NULL;

BEGIN

        v_code := getcode('home_menu');
	v_code := REPLACE (v_code, '[doc_archive_button]', get.txt('doc_archive_button') );
	v_code := REPLACE (v_code, '[cal_archive_button]', get.txt('cal_archive_button') );
	v_code := REPLACE (v_code, '[last_comments_button]', get.txt('last_comments_button') );
	v_code := REPLACE (v_code, '[last_albums_button]', get.txt('last_albums_button') );
	v_code := REPLACE (v_code, '[last_pictures_button]', get.txt('last_pictures_button') );
        v_code := REPLACE (v_code, '[i4_menu]' , html.i4_menu );
	v_code := REPLACE (v_code, '[p_language_pk]' , get.lan );
        htp.p( v_code );

END;
END home_menu;




---------------------------------------------------------------------
-- Name: i4_menu
-- Type: procedure
-- What: writes out search button, contact us, help and info about in4mant icons
-- Made: Frode Klevstul
-- Date: 03.01.2001
---------------------------------------------------------------------
FUNCTION i4_menu
	RETURN varchar2
IS
BEGIN
DECLARE
        v_code  html_code.code%type             default NULL;

BEGIN

        v_code := getcode('i4_menu');

	-- dersom brukeren er logget inn viser vi logout button, hvis ikke vises login button
        if ( login.timeout(NULL, 1) > 0) then
	        v_code := REPLACE (v_code, '[login_status_button]', get.txt('logout_button') );
	        v_code := REPLACE (v_code, '[login_status_url]', 'login.logout' );
	else
	        v_code := REPLACE (v_code, '[login_status_button]', get.txt('login_button') );
	        v_code := REPLACE (v_code, '[login_status_url]', 'portfolio.startup' );
	end if;

	v_code := REPLACE (v_code, '[search_button]', get.txt('search_button') );
	v_code := REPLACE (v_code, '[contact_us_button]', get.txt('contact_us_button') );
	v_code := REPLACE (v_code, '[about_button]', get.txt('about_button') );
	v_code := REPLACE (v_code, '[help_button]', get.txt('help_button') );
        RETURN v_code;

END;
END i4_menu;





---------------------------------------------------------------------
-- Name: empty_menu
-- Type: procedure
-- What: writes out home menu
-- Made: Frode Klevstul
-- Date: 03.01.2001
-- Chng:
---------------------------------------------------------------------
PROCEDURE empty_menu
IS
BEGIN
DECLARE
        v_code  	html_code.code%type             default NULL;

BEGIN

        v_code := getcode('empty_menu');
        v_code := REPLACE (v_code, '[i4_menu]' , html.i4_menu );
	v_code := REPLACE (v_code, '[p_language_pk]' , get.lan );
        htp.p( v_code );

END;
END empty_menu;




---------------------------------------------------------------------
-- Name: www_menu
-- Type: procedure
-- What: writes out the menu on the front page
-- Made: Frode Klevstul
-- Date: 14.01.2001
-- Chng:
---------------------------------------------------------------------
PROCEDURE www_menu
IS
BEGIN
DECLARE
        v_code  	html_code.code%type             default NULL;

BEGIN

        v_code := getcode('www_menu');
        v_code := REPLACE (v_code, '[c_service_mmb]' , get.value( 'c_'|| get.serv_name ||'_mmb' ) );
	v_code := REPLACE (v_code, '[contact_us_button]', get.txt('contact_us_button') );
	v_code := REPLACE (v_code, '[about_button]', get.txt('about_button') );
	v_code := REPLACE (v_code, '[help_button]', get.txt('help_button') );
        htp.p( v_code );

END;
END www_menu;




---------------------------------------------------------------------
-- Name: date_field
-- Type: procedure
-- What: writes out a date field
-- Made: Frode Klevstul
-- Date: 20.02.2001
-- Chng:
---------------------------------------------------------------------
PROCEDURE date_field
	(
		p_field_name	in varchar2	default NULL,
		p_field_text	in varchar2	default NULL,
		p_field_value	in date		default NULL,
		p_start_date	in date		default sysdate,
		p_command	in varchar2	default NULL
	)
IS
BEGIN
DECLARE
        v_code  	html_code.code%type             default NULL;

BEGIN

        v_code := getcode('date_field');
	v_code := REPLACE (v_code, '[field_name]', p_field_name );
	v_code := REPLACE (v_code, '[field_value]', to_char(p_field_value, get.txt('date_long')) );
	v_code := REPLACE (v_code, '[start_date]', to_char(p_start_date, get.txt('date_full')) );
	v_code := REPLACE (v_code, '[change_link]', html.popup( get.txt(p_field_text),'date_time.startup?p_command='||p_command||'&p_variable='||p_field_name||'&p_start_date='|| to_char(p_start_date,get.txt('date_full')),'300', '200') );
	v_code := REPLACE (v_code, '[command]', p_command );
        htp.p( v_code );

END;
END date_field;




---------------------------------------------------------------------
-- Name: bottom
-- Type: procedure
-- What: writes out the bottom
-- Made: Frode Klevstul
-- Date: 09.03.2001
-- Chng:
---------------------------------------------------------------------
PROCEDURE bottom
IS
BEGIN
DECLARE
        v_code  	html_code.code%type             default NULL;
        v_service_name  service.description%TYPE        default NULL;

BEGIN

        v_service_name 	:= get.serv_name;
        v_code := getcode('bottom');

        v_code := REPLACE (v_code, '[icons_dir]', get.value('icons_dir') );
        v_code := REPLACE (v_code, '[width]', get.value('width') );
        v_code := REPLACE (v_code, '[css]', getcode('css') );
        v_code := REPLACE (v_code, '[c_service_mmb]' , get.value( 'c_'|| v_service_name ||'_mmb' ) );

        htp.p( v_code );

END;
END bottom;







---------------------------------------------------------
-- Name:        top
-- Type:        procedure
-- What:        writes out an html top (in frames)
-- Author:      Frode Klevstul
-- Start date:  14.03.2001
-- Desc:
---------------------------------------------------------
PROCEDURE top
	(
                p_width         	in number       			DEFAULT get.value('width'),
                p_reload        	IN NUMBER       			DEFAULT NULL,
                p_jump          	IN VARCHAR2     			DEFAULT NULL,
                p_type          	IN NUMBER       			DEFAULT NULL,
                p_selected		IN NUMBER				DEFAULT NULL,
                p_organization_pk	IN organization.organization_pk%type	DEFAULT NULL
	)
IS
BEGIN

	html.b_page(p_width, p_reload, p_jump, p_type, p_selected);


	IF ( p_organization_pk IS NOT NULL ) THEN
		html.org_menu(p_organization_pk);
	END IF;

	IF ( p_type <> 3 ) THEN
		html.empty_menu;
	END IF;

	htp.p('<br><br><br><br><br><br><br><br>');

	html.e_page;


END top;



-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/
