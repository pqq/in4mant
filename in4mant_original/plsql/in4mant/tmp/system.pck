CREATE OR REPLACE PACKAGE system IS

        PROCEDURE set_cookie
                (
                        p_name          in varchar2     default NULL,
                        p_value         in number       default NULL
                );
        FUNCTION get_cookie
                (
                        p_name          in varchar2     default NULL
                ) RETURN number;
        PROCEDURE remove_cookie
                (
                        p_name          in varchar2     default NULL
                );
        PROCEDURE write_mail_files;
	PROCEDURE write_calendar_files;
	PROCEDURE write_calendar_info_file;
        PROCEDURE clean_db;
        PROCEDURE log_sg
                (
                        p_string_group_pk       in string_group.string_group_pk%type    default NULL
                );

END;
/
CREATE OR REPLACE PACKAGE BODY system IS

---------------------------------------------------------
-- Name:        set_cookie
-- Type:        procedure
-- What:        sets a cookie
-- Author:      Frode Klevstul
-- Start date:  11.06.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
-- beskrivelse av inn parametere:
--
-- [parameter]  [description]
---------------------------------------------------------
PROCEDURE set_cookie
        (
                p_name          in varchar2     default NULL,
                p_value         in number       default NULL
        )
IS
BEGIN

        owa_util.mime_header('text/html', FALSE);

        -- owa_cookie.send(name, value, expires, path, domain, secure)

        -- OAS: hadde med '/' n�r vi kj�rte p� OAS:
--        owa_cookie.send(p_name, p_value, NULL, '/');
        -- Apache: p� Apache m� ikke '/' taes med
--        owa_cookie.send(p_name, p_value, NULL);

        -- cookien setter p� to forskjellige m�ter da ikke den siste m�ten
        -- (det at cookien gjelder for flere domener) fungerer overalt

        owa_cookie.send(p_name, p_value, NULL );
        owa_cookie.send(p_name, p_value, NULL, NULL, '.in4mant.com');

        owa_util.http_header_close;

END set_cookie;


---------------------------------------------------------
-- Name:        get_cookie
-- Type:        function
-- What:        returns the value of a cookie
-- Author:      Frode Klevstul
-- Start date:  11.06.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
-- beskrivelse av inn parametere:
--
-- [parameter]  [description]
---------------------------------------------------------
FUNCTION get_cookie
        (
                p_name          in varchar2     default NULL
        ) RETURN number
IS
        v_cookie        owa_cookie.cookie;     -- variablen som vi legger 'cookien' i
        v_value         number                  default NULL;

BEGIN
        v_cookie        := owa_cookie.get(p_name);      -- henter ut cookien
        v_value         := v_cookie.vals(1);            -- henter ut verdien til cookien
        RETURN v_value;

EXCEPTION
        WHEN NO_DATA_FOUND     -- dersom cookien ikke finnes
        THEN
                RETURN NULL;

END get_cookie;




---------------------------------------------------------
-- Name:        remove_cookie
-- Type:        procedure
-- What:        sets a cookie
-- Author:      Frode Klevstul
-- Start date:  11.06.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
-- beskrivelse av inn parametere:
--
-- [parameter]  [description]
---------------------------------------------------------
PROCEDURE remove_cookie
        (
                p_name          in varchar2     default NULL
        )
IS
BEGIN

DECLARE
        v_cookie owa_cookie.cookie;     -- cookien som vi henter
        v_value number;                 -- sesjonsnummeret vi henter fra cookien

BEGIN

        v_cookie := owa_cookie.get(p_name);     -- henter ut cookien
        v_value := (v_cookie.vals(1));          -- henter ut cookiens verdi

        owa_util.mime_header('text/html', FALSE);
        owa_cookie.remove(p_name,v_value);   -- fjerner cookien
        owa_util.http_header_close;

END;
END remove_cookie;



---------------------------------------------------------
-- Name:        write_mail_files
-- Type:        procedure
-- What:        a proc that goes through the database and
--              writes "email" files to the OS
-- Author:      Frode Klevstul
-- Start date:  13.09.2000
-- Desc:        Does not write out mail for entries that are
--              older than 30 days.
--		Get UTL_FILE.WRITE_ERROR if we send more than 1023 char
--		into put_line
---------------------------------------------------------
PROCEDURE write_mail_files
IS
BEGIN
DECLARE

        -- Cursor that selects all documents to be sendt out with email.
        CURSOR  select_documents IS
        SELECT  document_pk, organization_fk, language_fk, heading, ingress, main1, main2, main3, main4, main5, main6, main7, main8, main9, main10, footer, publish_date
        FROM    document d, organization o
        WHERE   d.organization_fk = o.organization_pk
        AND     o.accepted = 1
        AND     d.publish_date < SYSDATE
        AND     d.publish_date > (SYSDATE - 30)
        AND     d.accepted = 1;


        -- Cursor that selects out users related to an organization.
        CURSOR  select_users
                (
                        v_organization_pk       in organization.organization_pk%type,
                        v_language_pk           in la.language_pk%type
                ) IS
        SELECT  DISTINCT(lu.user_fk), ud.email
        FROM    list_org lo, list l, list_user lu, list_type lt, user_details ud
        WHERE   lo.list_fk = l.list_pk
        AND     lt.list_type_pk = l.list_type_fk
        AND     lu.language_fk = v_language_pk
        AND     lo.organization_fk = v_organization_pk
        AND     lu.user_fk = ud.user_fk
        AND
                (
                        lu.list_fk = l.list_pk
                OR      lu.list_fk = l.level0
                OR      lu.list_fk = l.level1
                OR      lu.list_fk = l.level2
                OR      lu.list_fk = l.level3
                OR      lu.list_fk = l.level4
                OR      lu.list_fk = l.level5
                );

        v_document_pk           document.document_pk%type               default NULL;
        v_organization_pk       organization.organization_pk%type       default NULL;
        v_user_pk               usr.user_pk%type                        default NULL;
        v_language_pk           la.language_pk%type                     default NULL;
        v_email                 user_details.email%type                 default NULL;
        v_name                  organization.name%type                  default NULL;

        v_heading               document.heading%type                   default NULL;
        v_ingress               document.ingress%type                   default NULL;
        v_main1                 document.main1%type                     default NULL;
        v_main2                 document.main2%type                     default NULL;
        v_main3                 document.main3%type                     default NULL;
        v_main4                 document.main4%type                     default NULL;
        v_main5                 document.main5%type                     default NULL;
        v_main6                 document.main6%type                     default NULL;
        v_main7                 document.main7%type                     default NULL;
        v_main8                 document.main8%type                     default NULL;
        v_main9                 document.main9%type                     default NULL;
        v_main10                document.main10%type                    default NULL;
        v_footer                document.footer%type                    default NULL;
        v_publish_date          document.publish_date%type              default NULL;

        v_file                  utl_file.file_type                      default NULL;
        v_email_dir             varchar2(100)                           default NULL;
        v_number                number                                  default NULL;
        v_message               varchar2(4000)                          default NULL;
        v_msg_footer            varchar2(4000)                          default NULL;
        v_check                 number                                  default NULL;
	v_publish_info		varchar2(1000)				default NULL;
	v_et_text		varchar2(4000)				default NULL;
	v_et_html		varchar2(4000)				default NULL;

        v_email_text_pk         email_text.email_text_pk%type           default NULL;
        v_name_sg_fk            email_text.name_sg_fk%type              default NULL;
        v_mail_log_pk           mail_log.mail_log_pk%type               default NULL;
        v_no_users              number                                  default NULL;

	v_html_part		varchar2(32767)				default NULL;
	v_string_length 	number 					default NULL;
	v_tmp1			number					default NULL;

/* ---- deklarasjoner til bruk i html versjonen av mailen ---- */

	v_logo                  organization.logo%TYPE                  DEFAULT NULL;
	v_path                  picture.path%TYPE                       DEFAULT NULL;
	v_path2                 VARCHAR2(100)                           DEFAULT NULL;
	v_heading2              document.heading%TYPE                   DEFAULT NULL;
	v_html                  varchar2(32767)		                DEFAULT NULL;
	v_code                  VARCHAR2(10000)                         DEFAULT NULL;
	v_code2                 VARCHAR2(4000)                          DEFAULT NULL;
	v_service_pk            service.service_pk%TYPE                 DEFAULT NULL;
	v_service_name          VARCHAR2(100)                           DEFAULT NULL;
	v_domain                service_domain.domain%TYPE              DEFAULT NULL;

	CURSOR  pic_in_doc IS
	SELECT  p.path
	FROM    picture_in_document pid, picture p
	WHERE   pid.document_fk = v_document_pk
	AND     pid.picture_fk=p.picture_pk
	;

	CURSOR  rel_doc IS
	SELECT  d.document_pk, d.heading, d.publish_date
	FROM    document d, relation_d2d r
	WHERE   r.document_fk_1 = v_document_pk
	AND     r.document_fk_2 = d.document_pk
	AND     d.organization_fk = v_organization_pk
	;

	CURSOR  get_doc IS
	SELECT  d.heading, d.ingress,
	        d.main1, d.main2, d.main3, d.main4, d.main5, d.main6, d.main7,
	        d.main8, d.main9, d.main10, d.footer, o.logo, d.publish_date,
	        o.organization_pk, o.name, d.language_fk, os.service_fk
	FROM    document d, organization o, org_service os
	WHERE   d.document_pk = v_document_pk
	AND     d.accepted > 0
	AND     d.organization_fk = o.organization_pk
	AND     o.accepted = 1
	AND     d.organization_fk = os.organization_fk
	;

/* ----------- */



BEGIN

-- ---------------
/* NB! */
/* kan (b�r) skrives om slik at man bruker samme cursor for � hente ut info om dokumenter */
-- ---------------


	v_email_dir     := get.value('email_dir');

        -- -----------------------------------------------------------------------
        -- henter ut dokumenter som er publisert og hvor det skal sendes ut mail
        -- -----------------------------------------------------------------------
        OPEN select_documents;
        LOOP
                FETCH select_documents INTO v_document_pk, v_organization_pk, v_language_pk, v_heading, v_ingress, v_main1, v_main2, v_main3, v_main4, v_main5, v_main6, v_main7, v_main8, v_main9, v_main10, v_footer, v_publish_date;
                EXIT WHEN select_documents%NOTFOUND;

                v_message       := get.txt('email_update_heading', v_language_pk);
                v_msg_footer    := get.txt('email_update_footer', v_language_pk);
                v_name          := get.oname(v_organization_pk);
                v_no_users      := 0; -- antall mottagere av dette dokumentet

		v_message := REPLACE (v_message, '[subject]' , v_name ||' - '|| SUBSTR(v_heading, 0, 20) ||'  ...' );
		-- m� st� helt f�rst p� linja
v_publish_info := get.txt('publish_date', v_language_pk) ||': '|| to_char(v_publish_date, get.txt('date_long', v_language_pk)) ||'
'|| get.txt('source', v_language_pk)||': '|| v_name ||'
'|| get.txt('url_to_news', v_language_pk)||': '|| get.olink(v_organization_pk, 'doc_util.show_document?p_document_pk='||v_document_pk, v_language_pk);

                SELECT  email_script_seq.NEXTVAL
                INTO    v_number
                FROM    dual;

                -- ------------------------------------------------
                -- skriver selve mailen (innholdet) *** START ***
                -- ------------------------------------------------

                -- legger eventuelt til reklametekst i mail
                v_email_text_pk := get.et(v_organization_pk, 'CB');
                if (v_email_text_pk IS NOT NULL) then
                        SELECT  name_sg_fk INTO v_name_sg_fk
                        FROM    email_text
                        WHERE   email_text_pk = v_email_text_pk;

                        v_et_text := get.text(v_name_sg_fk, v_language_pk);
                        v_et_html := v_et_text;

			-- splitter opp teksten i en TEXT del og en HTML del
			owa_pattern.change(v_et_text, '\n', '���', 'g');
			owa_pattern.change(v_et_text, '</txt-message>.*$', NULL, 'g');
			owa_pattern.change(v_et_text, '^<txt-message>', NULL);
			owa_pattern.change(v_et_text, '���', '\n', 'g');

			owa_pattern.change(v_et_html, '\n', '���', 'g');
			owa_pattern.change(v_et_html, '^.*<html-message>', NULL, 'g');
			owa_pattern.change(v_et_html, '</html-message>', NULL);
			owa_pattern.change(v_et_html, '���', '\n', 'g');
                end if;

                v_file := utl_file.fopen( v_email_dir, v_number||'_message.txt' ,'a');

                utl_file.put_line(v_file, substr(v_message,1,999));
                utl_file.put(v_file, substr(v_message,1000,1000));
                utl_file.put(v_file, substr(v_message,2000,1000));
                utl_file.put(v_file, substr(v_message,3000,1000));

                -- -----------------------------
		-- skriver mailen som ren tekst
                -- -----------------------------
		utl_file.put(v_file, '<txt-message>');
		utl_file.new_line(v_file, 1);
                utl_file.put(v_file, v_publish_info );
		utl_file.new_line(v_file, 3);
                utl_file.put(v_file, v_heading );
                utl_file.new_line(v_file, 2);
                utl_file.put(v_file, v_ingress );
                utl_file.new_line(v_file, 2);

                utl_file.put(v_file, substr(v_main1,1,999));
                utl_file.put(v_file, substr(v_main1,1000,1000));
                utl_file.put(v_file, substr(v_main1,2000,1000));
                utl_file.put(v_file, substr(v_main1,3000,1000));

                utl_file.put(v_file, substr(v_main2,1,999));
                utl_file.put(v_file, substr(v_main2,1000,1000));
                utl_file.put(v_file, substr(v_main2,2000,1000));
                utl_file.put(v_file, substr(v_main2,3000,1000));

                utl_file.put(v_file, substr(v_main3,1,999));
                utl_file.put(v_file, substr(v_main3,1000,1000));
                utl_file.put(v_file, substr(v_main3,2000,1000));
                utl_file.put(v_file, substr(v_main3,3000,1000));

                utl_file.put(v_file, substr(v_main4,1,999));
                utl_file.put(v_file, substr(v_main4,1000,1000));
                utl_file.put(v_file, substr(v_main4,2000,1000));
                utl_file.put(v_file, substr(v_main4,3000,1000));

                utl_file.put(v_file, substr(v_main5,1,999));
                utl_file.put(v_file, substr(v_main5,1000,1000));
                utl_file.put(v_file, substr(v_main5,2000,1000));
                utl_file.put(v_file, substr(v_main5,3000,1000));

                utl_file.put(v_file, substr(v_main6,1,999));
                utl_file.put(v_file, substr(v_main6,1000,1000));
                utl_file.put(v_file, substr(v_main6,2000,1000));
                utl_file.put(v_file, substr(v_main6,3000,1000));

                utl_file.put(v_file, substr(v_main7,1,999));
                utl_file.put(v_file, substr(v_main7,1000,1000));
                utl_file.put(v_file, substr(v_main7,2000,1000));
                utl_file.put(v_file, substr(v_main7,3000,1000));

                utl_file.put(v_file, substr(v_main8,1,999));
                utl_file.put(v_file, substr(v_main8,1000,1000));
                utl_file.put(v_file, substr(v_main8,2000,1000));
                utl_file.put(v_file, substr(v_main8,3000,1000));

                utl_file.put(v_file, substr(v_main9,1,999));
                utl_file.put(v_file, substr(v_main9,1000,1000));
                utl_file.put(v_file, substr(v_main9,2000,1000));
                utl_file.put(v_file, substr(v_main9,3000,1000));

                utl_file.put(v_file, substr(v_main10,1,999));
                utl_file.put(v_file, substr(v_main10,1000,1000));
                utl_file.put(v_file, substr(v_main10,2000,1000));
                utl_file.put(v_file, substr(v_main10,3000,1000));

                utl_file.new_line(v_file, 1);
                utl_file.put(v_file, v_footer );
                utl_file.new_line(v_file, 3);
                utl_file.put(v_file, substr(v_msg_footer,1,999));
                utl_file.put(v_file, substr(v_msg_footer,1000,1000));
                utl_file.put(v_file, substr(v_msg_footer,2000,1000));
                utl_file.put(v_file, substr(v_msg_footer,3000,1000));
                utl_file.new_line(v_file, 2);
                utl_file.put(v_file, substr(v_et_text,1,999));
                utl_file.put(v_file, substr(v_et_text,1000,1000));
                utl_file.put(v_file, substr(v_et_text,2000,1000));
                utl_file.put(v_file, substr(v_et_text,3000,1000));

                utl_file.new_line(v_file, 1);
		utl_file.put_line(v_file, '</txt-message>');

                -- -------------------------------------------------------------
		-- skriver mailen som html
                -- -------------------------------------------------------------

		utl_file.put_line(v_file, '<html-message>');

		OPEN    get_doc;
		FETCH   get_doc
		INTO    v_heading, v_ingress,
		        v_main1, v_main2, v_main3, v_main4, v_main5 ,v_main6, v_main7,
		        v_main8, v_main9, v_main10, v_footer, v_logo,v_publish_date,
		        v_organization_pk, v_name, v_language_pk, v_service_pk;
		CLOSE   get_doc;

		SELECT  domain INTO v_domain
		FROM    service_domain
		WHERE   service_fk = v_service_pk
		AND     language_fk = v_language_pk;

		SELECT  description INTO v_service_name
		FROM    service
		WHERE   service_pk = v_service_pk;

		-- -----------
		-- b_page
		-- -----------
		v_code := html.getcode('b_page');

		owa_pattern.change(v_code, '\n', '���', 'g');
		owa_pattern.change(v_code, '<!-- banner_start -->.*<!-- banner_end -->', v_et_html, 'g');
		owa_pattern.change(v_code, '���', '\n', 'g');
		v_code := REPLACE (v_code, '[reload]', '[reload]<BASE href="http://'|| v_domain ||'/cgi-bin/">');
		v_code := REPLACE (v_code, '[reload]', '');
		v_code := REPLACE (v_code, '[jump]', '');
		v_code := REPLACE (v_code, '[icons_dir]', get.value('icons_dir') );
		v_code := REPLACE (v_code, '[width]', get.value('width') );
		v_code := REPLACE (v_code, '[css]',  html.getcode('css') );
		v_code := REPLACE (v_code, '[javascript]',  html.getcode('javascript') );
		v_code := REPLACE (v_code, '[c_service_bbm]' , get.value( 'c_'|| v_service_name ||'_bbm' ) );
		v_code := REPLACE (v_code, '[c_service_bbs]' , get.value( 'c_'|| v_service_name ||'_bbs' ) );
		v_code := REPLACE (v_code, '[c_service_mmb]' , get.value( 'c_'|| v_service_name ||'_mmb' ) );
		v_code := REPLACE (v_code, '[ad_button]', get.txt('ad_button', v_language_pk) );

		v_html := v_code;

		-- ------------
		-- main_menu
		-- ------------
		v_code :=  html.getcode('main_menu');
		v_code := REPLACE (v_code, '[home_button]', get.txt('home_button', v_language_pk) );
		v_code := REPLACE (v_code, '[my_page_button]', get.txt('my_page_button', v_language_pk) );
		v_code := REPLACE (v_code, '[organizations_button]', get.txt('organizations_button2', v_language_pk) );
		v_code := REPLACE (v_code, '[c_service_mmb]' , get.value( 'c_'|| v_service_name ||'_mmb' ) );

		v_html := v_html ||''|| v_code;

		-- ------------
		-- home_menu & i4_menu
		-- ------------

		v_code := html.getcode('home_menu');
		v_code := REPLACE (v_code, '[doc_archive_button]', get.txt('doc_archive_button', v_language_pk) );
		v_code := REPLACE (v_code, '[cal_archive_button]', get.txt('cal_archive_button', v_language_pk) );
		v_code := REPLACE (v_code, '[last_comments_button]', get.txt('last_comments_button', v_language_pk) );
		v_code := REPLACE (v_code, '[last_albums_button]', get.txt('last_albums_button', v_language_pk) );
		v_code := REPLACE (v_code, '[last_pictures_button]', get.txt('last_pictures_button', v_language_pk) );

		v_code2 := html.getcode('i4_menu');
		v_code2 := REPLACE (v_code2, '[login_status_button]', get.txt('login_button', v_language_pk) );
		v_code2 := REPLACE (v_code2, '[login_status_url]', 'portfolio.startup' );
		v_code2 := REPLACE (v_code2, '[search_button]', get.txt('search_button', v_language_pk) );
		v_code2 := REPLACE (v_code2, '[contact_us_button]', get.txt('contact_us_button', v_language_pk) );
		v_code2 := REPLACE (v_code2, '[about_button]', get.txt('about_button', v_language_pk) );
		v_code2 := REPLACE (v_code2, '[help_button]', get.txt('help_button', v_language_pk) );

		v_code := REPLACE (v_code, '[i4_menu]' , v_code2 );
		v_code := REPLACE (v_code, '[p_language_pk]' , v_language_pk );

		v_html := v_html ||''|| v_code;


		-- -----------
		-- b_box, b_table ...
		-- -----------

		OPEN pic_in_doc;
		FETCH pic_in_doc INTO v_path;
		CLOSE pic_in_doc;

		v_code := html.getcode('b_box');
		v_code := REPLACE (v_code, '[title]', '' );
		v_code := REPLACE (v_code, '[width]', '100%' );
		v_code := REPLACE (v_code, '<!-- ht -->' , html.popup( get.txt('?', v_language_pk), 'help.module?p_package=doc_util.show_document' , 400, 500, 0, 1) );
		v_code := REPLACE (v_code, '<!-- archive -->','<a href="doc_util.doc_archive?p_organization_pk='||v_organization_pk||'&p_language_pk='||v_language_pk||'">'||get.txt('archive', v_language_pk)||'</a>' );
		v_html := v_html ||''|| v_code;

		v_code :=  html.getcode('b_table');
		v_code := REPLACE (v_code, '[width]', '' );
		v_code := REPLACE (v_code, '[border]', 0 );
		v_html := v_html ||''|| v_code;

		IF( v_logo IS NULL ) THEN
		    v_logo := 'no_logo.gif';
		END IF;

		v_html := v_html ||'<tr><td colspan="2">&nbsp;</td></tr>
		<tr><td colspan="2"><b>'||get.txt('date', v_language_pk)||':</b>&nbsp;&nbsp;'|| to_char(v_publish_date,get.txt('date_long', v_language_pk)) ||'&nbsp;(CET)<br></td></tr>
		<tr><td colspan="2"><h2><b>'|| html.rem_tag(v_heading) ||'</b></h2></td></tr>
		<tr><td><b>'|| html.rem_tag(v_ingress) ||'</b></td>
		<td rowspan="6" valign="top"><a href="org_page.main?p_organization_pk='|| v_organization_pk ||'">
		<img src="'||get.value('org_logo_dir')||'/'|| v_logo ||'" alt="'||get.txt('back', v_language_pk)||'"  border="0"></a></td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td valign="top">';
		IF ( v_path IS NOT NULL ) THEN
		       v_html := v_html ||'<DIV ALIGN="right"><img align="right" src="'|| get.value('photo_archive') ||'/'|| v_path ||'.jpg"></DIV>';
		END IF;



		-- -------------------------------------------
		-- skriver f�rste del av meldingen til fil
		-- -------------------------------------------
		v_string_length := LENGTH(v_html);
		v_tmp1 := 1;
		WHILE (v_tmp1 < v_string_length) LOOP
			v_html_part := substr(v_html, v_tmp1, 1000);
			utl_file.put(v_file, v_html_part );
			v_tmp1 := v_tmp1 + 1000;
		END LOOP;


		-- -----------------------------------------------
		-- skriver inn maks 5*4000=20000 tegn av gangen
		-- -----------------------------------------------
		v_html := '<DIV VALIGN="TOP" ALIGN="left">'|| html.rem_tag(v_main1) ||''|| html.rem_tag(v_main2) ||''|| html.rem_tag(v_main3) ||''|| html.rem_tag(v_main4) ||''|| html.rem_tag(v_main5);

		utl_file.new_line(v_file, 1);

		v_string_length := LENGTH(v_html);
		v_tmp1 := 1;
		WHILE (v_tmp1 < v_string_length) LOOP
			v_html_part := substr(v_html, v_tmp1, 1000);
			utl_file.put(v_file, v_html_part );
			v_tmp1 := v_tmp1 + 1000;
		END LOOP;

		-- ------------------------------------
		-- skriver inn siste del av meldingen
		-- ------------------------------------

		v_html := html.rem_tag(v_main6) ||''|| html.rem_tag(v_main7) ||''|| html.rem_tag(v_main8) ||''|| html.rem_tag(v_main9) ||''|| html.rem_tag(v_main10) ||'
		</DIV>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td><i>'|| html.rem_tag(v_footer) ||'</i></td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td colspan="2">';

		v_string_length := LENGTH(v_html);
		v_tmp1 := 1;
		WHILE (v_tmp1 < v_string_length) LOOP
			v_html_part := substr(v_html, v_tmp1, 1000);
			utl_file.put(v_file, v_html_part );
			v_tmp1 := v_tmp1 + 1000;
		END LOOP;

		utl_file.new_line(v_file, 1);

		-- ------------------------------------------------
		-- skriver ut avsluttende del av html versjonen
		-- ------------------------------------------------
		v_code :=  html.getcode('b_table');
		v_code := REPLACE (v_code, '[width]', '' );
		v_code := REPLACE (v_code, '[border]', 0 );
		v_html := v_html ||''|| v_code;

		v_html := v_html ||'<tr><td align="left" width="10%">'||
		html.popup( get.txt('print_page', v_language_pk),
		'doc_util.show_document?p_document_pk='||v_document_pk||'&p_command=print','620', '700', '1' )
		||'</td><td align="left" width="90%"><b>'||get.txt('written_by', v_language_pk)||':</b>&nbsp;&nbsp;<a href="org_page.main?p_organization_pk='|| v_organization_pk ||'">'||v_name||'</a>
		<br>&nbsp;<br><b>'||get.txt('date', v_language_pk)||':</b>&nbsp;&nbsp;'|| to_char(v_publish_date,get.txt('date_long', v_language_pk)) ||'&nbsp;(CET)
		</td></tr>
		</table>
		</td></tr>

		<tr><td align="center" colspan="2"><br>
		<a href="org_page.main?p_organization_pk='|| v_organization_pk ||'">['||v_name||']</a>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="/">['||v_service_name||']</a>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		</table>'|| html.getcode('e_box');

		v_code := html.getcode('e_page');
		v_code := REPLACE (v_code, '[icons_dir]', get.value('icons_dir') );
		v_code := REPLACE (v_code, '[c_service_bbb]' , get.value( 'c_'|| v_service_name ||'_bbb' ) );
		v_html := v_html ||''|| v_code;

		v_string_length := LENGTH(v_html);
		v_tmp1 := 1;
		WHILE (v_tmp1 < v_string_length) LOOP
			v_html_part := substr(v_html, v_tmp1, 1000);
			utl_file.put(v_file, v_html_part );
			v_tmp1 := v_tmp1 + 1000;
		END LOOP;

                utl_file.new_line(v_file, 1);
		utl_file.put_line(v_file, '</html-message>');


                utl_file.fclose(v_file);
                -- ---------------------------------------------------------
                -- /slutt p� � skrive innholdet av mailen *** SLUTT ***
                -- ---------------------------------------------------------

                -- ----------------------------------------------------------------------------------
                -- skriver mottager fil ********* START **********
                -- ----------------------------------------------------------------------------------
                v_file := utl_file.fopen( v_email_dir, v_number||'_email-list.txt', 'a');

                -- ----------------------------------------------------------
                -- henter ut brukere tilknyttet en organisasjon p� ett spr�k
                -- ----------------------------------------------------------
                OPEN select_users(v_organization_pk, v_language_pk);
                LOOP
                        FETCH select_users INTO v_user_pk, v_email;
                        EXIT WHEN select_users%NOTFOUND;

			utl_file.put_line(v_file, trans(v_email));
			v_no_users := v_no_users + 1; -- antall email mottagere

                END LOOP;
                CLOSE select_users;

                utl_file.fclose(v_file);

                -- -------------------------------------------------------------------------
                -- /slutt p� � skrive mottager fil ********* END **********
                -- -------------------------------------------------------------------------


		-- -------------------------------------------------------------------------
		-- skriver info.txt fil (for generering av inkluderte sider)
		-- -------------------------------------------------------------------------
                v_file := utl_file.fopen( v_email_dir, v_number||'_info.txt', 'a');
		utl_file.put_line(v_file, 'ORGANIZATION_PK:'||v_organization_pk);
		utl_file.put_line(v_file, 'LANGUAGE_PK:'||v_language_pk);
		utl_file.put_line(v_file, 'DOCUMENT_PK:'||v_document_pk);
                utl_file.fclose(v_file);



                -- -----------------------------------------------
                -- oppdaterer dokument status (til "mail utsendt")
                -- -----------------------------------------------
                UPDATE  document
                SET     accepted = 2
                WHERE   document_pk = v_document_pk;
                commit;

                -- ------------------------------------
                -- legg inn informasjon i mail_log
                -- ------------------------------------
                SELECT  mail_log_seq.NEXTVAL
                INTO    v_mail_log_pk
                FROM    dual;

                INSERT INTO mail_log
                (mail_log_pk, volume, sendt_date, organization_fk, document_fk, email_text_fk)
                VALUES (v_mail_log_pk, v_no_users, sysdate, v_organization_pk, v_document_pk, v_email_text_pk);
                commit;

        END LOOP;
        CLOSE select_documents;


	-- ----------------------
	-- hver dag klokken 1200 sender vi ut kalenderhendelser p� mail
	-- ----------------------
	if ( to_number(to_char(sysdate, 'HH24MI'))>1200 and to_number(to_char(sysdate, 'HH24MI'))<1730 ) then
		system.write_calendar_files;
	end if;


	-- ----------------------------------------------------------
	-- vi skriver en info fil for bruk med integrerte sider
	-- ----------------------------------------------------------
	write_calendar_info_file;



EXCEPTION
	WHEN UTL_FILE.INVALID_PATH THEN
	BEGIN
		v_file := utl_file.fopen( v_email_dir, v_number||'_INVALID_PATH.txt', 'a');
		utl_file.put_line(v_file, 'UTL_FILE EXCEPTION! document_pk: '||v_document_pk);
		utl_file.fclose(v_file);
	END;

	WHEN UTL_FILE.INVALID_MODE THEN
	BEGIN
		v_file := utl_file.fopen( v_email_dir, v_number||'_INVALID_MODE.txt', 'a');
		utl_file.put_line(v_file, 'UTL_FILE EXCEPTION! document_pk: '||v_document_pk);
		utl_file.fclose(v_file);
	END;

	WHEN UTL_FILE.INVALID_FILEHANDLE THEN
	BEGIN
		v_file := utl_file.fopen( v_email_dir, v_number||'_INVALID_FILEHANDLE.txt', 'a');
		utl_file.put_line(v_file, 'UTL_FILE EXCEPTION! document_pk: '||v_document_pk);
		utl_file.fclose(v_file);
	END;

	WHEN UTL_FILE.INVALID_OPERATION THEN
	BEGIN
		v_file := utl_file.fopen( v_email_dir, v_number||'_INVALID_OPERATION.txt', 'a');
		utl_file.put_line(v_file, 'UTL_FILE EXCEPTION! document_pk: '||v_document_pk);
		utl_file.fclose(v_file);
	END;

	WHEN UTL_FILE.READ_ERROR THEN
	BEGIN
		v_file := utl_file.fopen( v_email_dir, v_number||'_READ_ERROR.txt', 'a');
		utl_file.put_line(v_file, 'UTL_FILE EXCEPTION! document_pk: '||v_document_pk);
		utl_file.fclose(v_file);
	END;

	WHEN UTL_FILE.INTERNAL_ERROR THEN
	BEGIN
		v_file := utl_file.fopen( v_email_dir, v_number||'_INTERNAL_ERROR.txt', 'a');
		utl_file.put_line(v_file, 'UTL_FILE EXCEPTION! document_pk: '||v_document_pk);
		utl_file.fclose(v_file);
	END;

	WHEN UTL_FILE.WRITE_ERROR THEN
	BEGIN
		v_file := utl_file.fopen( v_email_dir, v_number||'_WRITE_ERROR.txt', 'a');
		utl_file.put_line(v_file, 'UTL_FILE EXCEPTION! document_pk: '||v_document_pk);
		utl_file.fclose(v_file);
	END;

	WHEN UTL_FILE.INVALID_MAXLINESIZE THEN
	BEGIN
		v_file := utl_file.fopen( v_email_dir, v_number||'_INVALID_MAXLINESIZE.txt', 'a');
		utl_file.put_line(v_file, 'UTL_FILE EXCEPTION! document_pk: '||v_document_pk);
		utl_file.fclose(v_file);
	END;

	WHEN OTHERS THEN
	BEGIN
		v_file := utl_file.fopen( v_email_dir, v_number||'_OTHER.txt', 'a');
		utl_file.put_line(v_file, 'UTL_FILE EXCEPTION! document_pk: '||v_document_pk);
		utl_file.fclose(v_file);
	END;

END;
END write_mail_files;






---------------------------------------------------------
-- Name:        write_calendar_files
-- Type:        procedure
-- What:        a proc that goes through the database and
--              writes calendar "email" files to the OS.
--		This should be executed every day at 12:00
-- Author:      Frode Klevstul
-- Start date:  22.03.2001
-- Desc:
---------------------------------------------------------
PROCEDURE write_calendar_files
IS
BEGIN
DECLARE

	v_date		date	default to_date( (to_char(sysdate, 'YYYY DD/MM')||' 12:00'), 'YYYY DD/MM HH24:MI');

        -- Cursor that selects all documents to be sendt out with email.
        CURSOR  select_calendar IS
        SELECT  calendar_pk, organization_fk, language_fk, event_date, heading, message
        FROM    calendar c, organization o
        WHERE   c.organization_fk = o.organization_pk
        AND     o.accepted = 1
        AND     c.event_date between v_date and v_date + 1.75
        AND     c.accepted = 1;

	v_calendar_pk		calendar.calendar_pk%type		default NULL;
        v_organization_pk       organization.organization_pk%type       default NULL;
        v_language_pk           la.language_pk%type                     default NULL;
	v_event_date		calendar.event_date%type		default NULL;
	v_heading		calendar.heading%type			default NULL;
	v_message		calendar.message%type			default NULL;

        -- Cursor that selects out users related to an organization.
        CURSOR  select_users
                (
                        v_organization_pk       in organization.organization_pk%type,
                        v_language_pk           in la.language_pk%type
                ) IS
        SELECT  DISTINCT(lu.user_fk), ud.email
        FROM    list_org lo, list l, list_user lu, list_type lt, user_details ud
        WHERE   lo.list_fk = l.list_pk
        AND     lt.list_type_pk = l.list_type_fk
        AND     lu.language_fk = v_language_pk
        AND     lo.organization_fk = v_organization_pk
        AND     lu.user_fk = ud.user_fk
        AND
                (
                        lu.list_fk = l.list_pk
                OR      lu.list_fk = l.level0
                OR      lu.list_fk = l.level1
                OR      lu.list_fk = l.level2
                OR      lu.list_fk = l.level3
                OR      lu.list_fk = l.level4
                OR      lu.list_fk = l.level5
                );

        v_user_pk               usr.user_pk%type                        default NULL;
        v_email                 user_details.email%type                 default NULL;

        v_name                  organization.name%type                  default NULL;

        v_file                  utl_file.file_type                      default NULL;
        v_email_dir             varchar2(100)                           default NULL;
        v_number                number                                  default NULL;
        v_message_txt           varchar2(4000)                          default NULL;
        v_msg_footer            varchar2(4000)                          default NULL;
        v_check                 number                                  default NULL;
	v_publish_info		varchar2(1000)				default NULL;
	v_et_text		varchar2(4000)				default NULL;
	v_et_html		varchar2(4000)				default NULL;

        v_email_text_pk         email_text.email_text_pk%type           default NULL;
        v_name_sg_fk            email_text.name_sg_fk%type              default NULL;
        v_mail_log_pk           mail_log.mail_log_pk%type               default NULL;
        v_no_users              number                                  default NULL;

	v_html_part		varchar2(32767)				default NULL;
	v_string_length 	number 					default NULL;
	v_tmp1			number					default NULL;

/* ---- deklarasjoner til bruk i html versjonen av mailen ---- */


/* ----------- */



BEGIN


	v_email_dir     := get.value('email_dir');

        -- -----------------------------------------------------------------------
        -- henter ut dokumenter som er publisert og hvor det skal sendes ut mail
        -- -----------------------------------------------------------------------
        OPEN select_calendar;
        LOOP
                FETCH select_calendar INTO v_calendar_pk, v_organization_pk, v_language_pk, v_event_date, v_heading, v_message;
                EXIT WHEN select_calendar%NOTFOUND;

                v_message_txt   := get.txt('email_update_heading', v_language_pk);
                v_msg_footer    := get.txt('email_update_footer', v_language_pk);
                v_name          := get.oname(v_organization_pk);
                v_no_users      := 0; -- antall mottagere av dette dokumentet

		v_message_txt := REPLACE (v_message_txt, '[subject]' , v_name ||' - '|| SUBSTR(v_heading, 0, 20) ||'  ...' );
		-- m� st� helt f�rst p� linja
v_publish_info := get.txt('event_date', v_language_pk) ||': '|| to_char(v_event_date, get.txt('date_long', v_language_pk)) ||'
'|| get.txt('source', v_language_pk)||': '|| v_name ||'
'|| get.txt('url_to_news', v_language_pk)||': '|| get.olink(v_organization_pk, 'cal_util.show_calendar?p_calendar_pk='||v_calendar_pk, v_language_pk);

                SELECT  email_script_seq.NEXTVAL
                INTO    v_number
                FROM    dual;

                -- ------------------------------------------------
                -- skriver selve mailen (innholdet) *** START ***
                -- ------------------------------------------------

                -- legger eventuelt til reklametekst i mail
                v_email_text_pk := get.et(v_organization_pk, 'CB');
                if (v_email_text_pk IS NOT NULL) then
                        SELECT  name_sg_fk INTO v_name_sg_fk
                        FROM    email_text
                        WHERE   email_text_pk = v_email_text_pk;

                        v_et_text := get.text(v_name_sg_fk, v_language_pk);
                        v_et_html := v_et_text;

			-- splitter opp teksten i en TEXT del og en HTML del
			owa_pattern.change(v_et_text, '\n', '���', 'g');
			owa_pattern.change(v_et_text, '</txt-message>.*$', NULL, 'g');
			owa_pattern.change(v_et_text, '^<txt-message>', NULL);
			owa_pattern.change(v_et_text, '���', '\n', 'g');

			owa_pattern.change(v_et_html, '\n', '���', 'g');
			owa_pattern.change(v_et_html, '^.*<html-message>', NULL, 'g');
			owa_pattern.change(v_et_html, '</html-message>', NULL);
			owa_pattern.change(v_et_html, '���', '\n', 'g');
                end if;

                v_file := utl_file.fopen( v_email_dir, v_number||'_message.txt' ,'a');

                utl_file.put_line(v_file, substr(v_message_txt,1,999));
                utl_file.put(v_file, substr(v_message_txt,1000,1000));
                utl_file.put(v_file, substr(v_message_txt,2000,1000));
                utl_file.put(v_file, substr(v_message_txt,3000,1000));

                -- -----------------------------
		-- skriver mailen som ren tekst
                -- -----------------------------
		utl_file.put(v_file, '<txt-message>');
		utl_file.new_line(v_file, 1);
                utl_file.put(v_file, v_publish_info );
		utl_file.new_line(v_file, 3);
                utl_file.put(v_file, v_heading );
                utl_file.new_line(v_file, 2);

                utl_file.put(v_file, substr(v_message,1,999));
                utl_file.put(v_file, substr(v_message,1000,1000));
                utl_file.put(v_file, substr(v_message,2000,1000));
                utl_file.put(v_file, substr(v_message,3000,1000));

                utl_file.new_line(v_file, 10);
                utl_file.put(v_file, substr(v_et_text,1,999));
                utl_file.put(v_file, substr(v_et_text,1000,1000));
                utl_file.put(v_file, substr(v_et_text,2000,1000));
                utl_file.put(v_file, substr(v_et_text,3000,1000));

                utl_file.new_line(v_file, 1);
		utl_file.put_line(v_file, '</txt-message>');

-- ------------
-- Dette (html versjon) skal implementeres senere ....
                -- -------------------------------------------------------------
		-- skriver mailen som html
                -- -------------------------------------------------------------
		utl_file.put_line(v_file, '<html-message>');

		utl_file.put_line(v_file, '<br>');
                utl_file.put(v_file, v_publish_info );
		utl_file.put_line(v_file, '<br><br><br><h2>');
                utl_file.put(v_file, v_heading );
		utl_file.put_line(v_file, '</h2><br><br>');

                utl_file.put(v_file, substr(v_message,1,999));
                utl_file.put(v_file, substr(v_message,1000,1000));
                utl_file.put(v_file, substr(v_message,2000,1000));
                utl_file.put(v_file, substr(v_message,3000,1000));

                utl_file.put_line(v_file, '<br><br><br><br><br>');
                utl_file.put(v_file, substr(v_et_text,1,999));
                utl_file.put(v_file, substr(v_et_text,1000,1000));
                utl_file.put(v_file, substr(v_et_text,2000,1000));
                utl_file.put(v_file, substr(v_et_text,3000,1000));

                utl_file.new_line(v_file, 1);

		utl_file.put_line(v_file, '</html-message>');


                utl_file.fclose(v_file);
                -- ---------------------------------------------------------
                -- /slutt p� � skrive innholdet av mailen *** SLUTT ***
                -- ---------------------------------------------------------

                -- ----------------------------------------------------------------------------------
                -- skriver mottager fil ********* START **********
                -- ----------------------------------------------------------------------------------
                v_file := utl_file.fopen( v_email_dir, v_number||'_email-list.txt', 'a');

                -- ----------------------------------------------------------
                -- henter ut brukere tilknyttet en organisasjon p� ett spr�k
                -- ----------------------------------------------------------
                OPEN select_users(v_organization_pk, v_language_pk);
                LOOP
                        FETCH select_users INTO v_user_pk, v_email;
                        EXIT WHEN select_users%NOTFOUND;

			utl_file.put_line(v_file, trans(v_email));
			v_no_users := v_no_users + 1; -- antall email mottagere

                END LOOP;
                CLOSE select_users;

                utl_file.fclose(v_file);

                -- -------------------------------------------------------------------------
                -- /slutt p� � skrive mottager fil ********* END **********
                -- -------------------------------------------------------------------------



                -- -----------------------------------------------
                -- oppdaterer dokument status (til "mail utsendt")
                -- -----------------------------------------------
                UPDATE  calendar
                SET     accepted = 2
                WHERE   calendar_pk = v_calendar_pk;
                commit;

                -- ------------------------------------
                -- legg inn informasjon i mail_log
                -- ------------------------------------
                SELECT  mail_log_seq.NEXTVAL
                INTO    v_mail_log_pk
                FROM    dual;

                INSERT INTO mail_log
                (mail_log_pk, volume, sendt_date, organization_fk, calendar_fk, email_text_fk)
                VALUES (v_mail_log_pk, v_no_users, sysdate, v_organization_pk, v_calendar_pk, v_email_text_pk);
                commit;

        END LOOP;
        CLOSE select_calendar;



EXCEPTION
	WHEN UTL_FILE.INVALID_PATH THEN
	BEGIN
		v_file := utl_file.fopen( v_email_dir, v_number||'_INVALID_PATH.txt', 'a');
		utl_file.put_line(v_file, 'UTL_FILE EXCEPTION! calendar_pk: '||v_calendar_pk);
		utl_file.fclose(v_file);
	END;

	WHEN UTL_FILE.INVALID_MODE THEN
	BEGIN
		v_file := utl_file.fopen( v_email_dir, v_number||'_INVALID_MODE.txt', 'a');
		utl_file.put_line(v_file, 'UTL_FILE EXCEPTION! calendar_pk: '||v_calendar_pk);
		utl_file.fclose(v_file);
	END;

	WHEN UTL_FILE.INVALID_FILEHANDLE THEN
	BEGIN
		v_file := utl_file.fopen( v_email_dir, v_number||'_INVALID_FILEHANDLE.txt', 'a');
		utl_file.put_line(v_file, 'UTL_FILE EXCEPTION! calendar_pk: '||v_calendar_pk);
		utl_file.fclose(v_file);
	END;

	WHEN UTL_FILE.INVALID_OPERATION THEN
	BEGIN
		v_file := utl_file.fopen( v_email_dir, v_number||'_INVALID_OPERATION.txt', 'a');
		utl_file.put_line(v_file, 'UTL_FILE EXCEPTION! calendar_pk: '||v_calendar_pk);
		utl_file.fclose(v_file);
	END;

	WHEN UTL_FILE.READ_ERROR THEN
	BEGIN
		v_file := utl_file.fopen( v_email_dir, v_number||'_READ_ERROR.txt', 'a');
		utl_file.put_line(v_file, 'UTL_FILE EXCEPTION! calendar_pk: '||v_calendar_pk);
		utl_file.fclose(v_file);
	END;

	WHEN UTL_FILE.INTERNAL_ERROR THEN
	BEGIN
		v_file := utl_file.fopen( v_email_dir, v_number||'_INTERNAL_ERROR.txt', 'a');
		utl_file.put_line(v_file, 'UTL_FILE EXCEPTION! calendar_pk: '||v_calendar_pk);
		utl_file.fclose(v_file);
	END;

	WHEN UTL_FILE.WRITE_ERROR THEN
	BEGIN
		v_file := utl_file.fopen( v_email_dir, v_number||'_WRITE_ERROR.txt', 'a');
		utl_file.put_line(v_file, 'UTL_FILE EXCEPTION! calendar_pk: '||v_calendar_pk);
		utl_file.fclose(v_file);
	END;

	WHEN UTL_FILE.INVALID_MAXLINESIZE THEN
	BEGIN
		v_file := utl_file.fopen( v_email_dir, v_number||'_INVALID_MAXLINESIZE.txt', 'a');
		utl_file.put_line(v_file, 'UTL_FILE EXCEPTION! calendar_pk: '||v_calendar_pk);
		utl_file.fclose(v_file);
	END;

	WHEN OTHERS THEN
	BEGIN
		v_file := utl_file.fopen( v_email_dir, v_number||'_OTHER.txt', 'a');
		utl_file.put_line(v_file, 'UTL_FILE EXCEPTION! calendar_pk: '||v_calendar_pk);
		utl_file.fclose(v_file);
	END;

END;
END write_calendar_files;






---------------------------------------------------------
-- Name:        write_calendar_info_file
-- Type:        procedure
-- What:        a proc that goes through the database and
--              writes calendar "info" files to the OS.
-- Author:      Frode Klevstul
-- Start date:  09.04.2001
-- Desc:
---------------------------------------------------------
PROCEDURE write_calendar_info_file
IS
BEGIN
DECLARE

        CURSOR  select_calendar IS
        SELECT  calendar_pk, organization_fk, language_fk
        FROM    calendar c, organization o
        WHERE   c.organization_fk = o.organization_pk
        AND     o.accepted = 1
        AND     c.accepted IS NULL;

	v_calendar_pk		calendar.calendar_pk%type		default NULL;
        v_organization_pk       organization.organization_pk%type       default NULL;
        v_language_pk           la.language_pk%type                     default NULL;

        v_file                  utl_file.file_type                      default NULL;
        v_email_dir             varchar2(100)                           default NULL;
        v_number                number                                  default NULL;

BEGIN

	v_email_dir     := get.value('email_dir');

        OPEN select_calendar;
        LOOP
                FETCH select_calendar INTO v_calendar_pk, v_organization_pk, v_language_pk;
                EXIT WHEN select_calendar%NOTFOUND;

                SELECT  email_script_seq.NEXTVAL
                INTO    v_number
                FROM    dual;

		-- -------------------------------------------------------------------------
		-- skriver info.txt fil (for generering av inkluderte sider)
		-- -------------------------------------------------------------------------
                v_file := utl_file.fopen( v_email_dir, v_number||'_info.txt', 'a');
		utl_file.put_line(v_file, 'ORGANIZATION_PK:'||v_organization_pk);
		utl_file.put_line(v_file, 'LANGUAGE_PK:'||v_language_pk);
		utl_file.put_line(v_file, 'CALENDAR_PK:'||v_calendar_pk);
                utl_file.fclose(v_file);

                -- -----------------------------------------------
                -- oppdaterer dokument status (til "info fil skrevet")
                -- -----------------------------------------------
                UPDATE  calendar
                SET     accepted = 1
                WHERE   calendar_pk = v_calendar_pk;
                commit;

        END LOOP;
        CLOSE select_calendar;


END;
END write_calendar_info_file;





---------------------------------------------------------
-- Name:        clean_db
-- Type:        procedure
-- What:        a proc that goes through the database and
--              writes old data (30 days) to files on the OS,
--              and delete from the db.
-- Author:      Frode Klevstul
-- Start date:  17.09.2000
-- Desc:
---------------------------------------------------------
PROCEDURE clean_db
IS
BEGIN
DECLARE

        CURSOR  select_sessn IS
        SELECT  sessn_pk, session_id, user_fk, login_time, last_action, logout_time
        FROM    sessn
        WHERE   login_time < (SYSDATE - 30)
        ORDER BY sessn_pk;

        CURSOR  select_user_stat IS
        SELECT  user_stat_pk, user_fk, ip_address, host_name, login_time
        FROM    user_stat
        WHERE   login_time < (SYSDATE - 30)
        ORDER BY user_stat_pk;

        CURSOR  select_pictures IS
        SELECT  picture_pk
        FROM    picture
        WHERE   picture_pk NOT IN
        (
                SELECT  picture_fk
                FROM    picture_in_album
        )
        OR      picture_pk IN
        (
                SELECT picture_fk
                FROM picture_in_album, album
                WHERE album_fk = album_pk
                AND organization_fk IS NULL
        );

        CURSOR  select_item IS
        SELECT  item_pk
        FROM    item
        WHERE   end_date < (sysdate-60);

        CURSOR  select_orguser IS
        SELECT  user_pk
        FROM    usr u, user_details ud
        WHERE   ud.user_fk = u.user_pk
        AND     ud.register_date < (sysdate-2)
        AND     u.user_type_fk = -2
        AND     user_fk NOT IN
        (
                SELECT  user_fk
                FROM    client_user
        );


        v_sessn_pk      sessn.sessn_pk%type             default NULL;
        v_session_id    sessn.session_id%type           default NULL;
        v_user_fk       sessn.user_fk%type              default NULL;
        v_login_time    sessn.login_time%type           default NULL;
        v_last_action   sessn.last_action%type          default NULL;
        v_logout_time   sessn.logout_time%type          default NULL;

        v_user_stat_pk  user_stat.user_stat_pk%type     default NULL;
        v_ip_address    user_stat.ip_address%type       default NULL;
        v_host_name     user_stat.host_name%type        default NULL;

        v_picture_pk    picture.picture_pk%type         default NULL;

        v_item_pk       item.item_pk%type               default NULL;

        v_user_pk       usr.user_pk%type                default NULL;

        v_date_format   varchar2(30)                    default NULL;

        -- variabler for bruk i forbindelse med UTL_FILE
        v_file                  utl_file.file_type                      default NULL;
        v_clean_db_dir          varchar2(100)                           default NULL;
        v_check                 number                                  default NULL;

        -- antall slettede forekomster
        v_no_sess_del           number                                  default NULL;
        v_no_ustat_del          number                                  default NULL;
        v_no_list_del           number                                  default NULL;
        v_no_pic_del            number                                  default NULL;
        v_no_item_del           number                                  default NULL;
        v_no_orguser_del        number                                  default NULL;

BEGIN

        v_clean_db_dir  := get.value('clean_db_dir');
        v_date_format   := get.value('log_date');


        -- ---------------------------------
        -- rensker opp i SESSN tabellen
        -- ---------------------------------
        v_file := utl_file.fopen( v_clean_db_dir, 'sessn.txt', 'a');
        OPEN select_sessn;
        LOOP
                FETCH select_sessn INTO v_sessn_pk, v_session_id, v_user_fk, v_login_time, v_last_action, v_logout_time;
                IF (select_sessn%NOTFOUND) THEN
                        v_no_sess_del := select_sessn%ROWCOUNT;
                END IF;
                EXIT WHEN select_sessn%NOTFOUND;

                utl_file.put_line(v_file, v_sessn_pk||'���'||v_session_id||'���'||v_user_fk||'���'||to_char(v_login_time, v_date_format)||'���'||to_char(v_last_action, v_date_format)||'���'||v_logout_time);

                -- delete entry from table
                DELETE FROM sessn
                WHERE sessn_pk = v_sessn_pk;
                commit;

        END LOOP;
        CLOSE select_sessn;
        utl_file.fclose(v_file);


        -- ---------------------------------
        -- rensker opp i USER_STAT tabellen
        -- ---------------------------------
        v_file := utl_file.fopen( v_clean_db_dir, 'user_stat.txt', 'a');
        OPEN select_user_stat;
        LOOP
                FETCH select_user_stat INTO v_user_stat_pk, v_user_fk, v_ip_address, v_host_name, v_login_time;
                IF (select_user_stat%NOTFOUND) THEN
                        v_no_ustat_del := select_user_stat%ROWCOUNT;
                END IF;
                EXIT WHEN select_user_stat%NOTFOUND;

                utl_file.put_line(v_file, v_user_stat_pk||'���'||v_user_fk||'���'||v_ip_address||'���'||v_host_name||'���'||to_char(v_login_time, v_date_format));

                -- delete entry from table
                DELETE FROM user_stat
                WHERE user_stat_pk = v_user_stat_pk;
                commit;

        END LOOP;
        CLOSE select_user_stat;
        utl_file.fclose(v_file);

        -- ---------------------------------------------------------------
        -- renske opp i "LIST" tabellen
        -- slette personlige lister til brukere som ikke lengre finnes
        -- ---------------------------------------------------------------
        SELECT count(*) INTO v_no_list_del
        FROM list
        WHERE list_pk IN
                (
                        SELECT  list_pk
                        FROM    list l, list_type lt
                        WHERE   l.list_type_fk = lt.list_type_pk
                        AND     lt.description LIKE 'my_personal_list%'
                        AND     list_pk NOT IN
                                (
                                        SELECT  list_pk
                                        FROM    list_user lu, list l, list_type lt
                                        WHERE   lu.list_fk = l.list_pk
                                        AND     l.list_type_fk = lt.list_type_pk
                                        AND     lt.description LIKE 'my_personal_list%'
                                )
                );

        if ( v_no_list_del > 0 ) then
                DELETE FROM list
                WHERE list_pk IN
                        (
                                SELECT  list_pk
                                FROM    list l, list_type lt
                                WHERE   l.list_type_fk = lt.list_type_pk
                                AND     lt.description LIKE 'my_personal_list%'
                                AND     list_pk NOT IN
                                        (
                                                SELECT  list_pk
                                                FROM    list_user lu, list l, list_type lt
                                                WHERE   lu.list_fk = l.list_pk
                                                AND     l.list_type_fk = lt.list_type_pk
                                                AND     lt.description LIKE 'my_personal_list%'
                                        )
                        );
        end if;


        -- --------------------------------------------------------------
        -- slett bilder fra OS og db dersom de ikke ligger i et album, eller
        -- dersom det finnes et album som ikke har en organisasjon tilknyttet
        -- --------------------------------------------------------------
        SELECT  count(*) INTO v_no_pic_del
        FROM    picture
        WHERE   picture_pk NOT IN
        (
                SELECT  picture_fk
                FROM    picture_in_album
        )
        OR      picture_pk IN
        (
                SELECT picture_fk
                FROM picture_in_album, album
                WHERE album_fk = album_pk
                AND organization_fk IS NULL
        );

        if (v_no_pic_del > 0) then

                open select_pictures;
                loop
                        fetch select_pictures into v_picture_pk;
                        exit when select_pictures%NOTFOUND;

                        DELETE FROM picture
                        WHERE picture_pk = v_picture_pk;
                        commit;

                        photo.create_delete_file(v_picture_pk);

                end loop;
                close select_pictures;

        end if;


        -- --------------------------------------------------------------------
        -- sletter bilder fra OS dersom bildet ikke ligger i picture tabellen
        -- --------------------------------------------------------------------
        -- lage et perl script som skriver alle filnavn til en fil
        -- bruke utl_file.get_line for � lese alle filnavn
        -- sjekke i pic tabellen etterhvert som man g�r igjennom filnavnene


        -- --------------------------------------------------------------------
        -- sletter "items" fra bboard hvor de har g�tt ut for mer enn 60 dager siden
        -- --------------------------------------------------------------------
        SELECT  count(*) INTO v_no_item_del
        FROM    item
        WHERE   end_date < (sysdate-60);

        if (v_no_item_del > 0) then

                open select_item;
                loop
                        fetch select_item into v_item_pk;
                        exit when select_item%NOTFOUND;

                        DELETE FROM item
                        WHERE item_pk = v_item_pk;
                        commit;

                end loop;
                close select_item;

        end if;

        -- ----------------------------------------------------------
        -- sletter org. brukere som ikke har f�tt tilknyttet en org.
        -- ----------------------------------------------------------
        SELECT  count(*)
        INTO    v_no_orguser_del
        FROM    usr u, user_details ud
        WHERE   ud.user_fk = u.user_pk
        AND     ud.register_date < (sysdate-2)
        AND     u.user_type_fk = -2
        AND     user_fk NOT IN
        (
                SELECT  user_fk
                FROM    client_user
        );

        if (v_no_orguser_del > 0) then

                open select_orguser;
                loop
                        fetch select_orguser into v_user_pk;
                        exit when select_orguser%NOTFOUND;

                        DELETE FROM usr
                        WHERE user_pk = v_user_pk;
                        commit;

                end loop;
                close select_orguser;

        end if;

        -- ---------------------
        -- SKRIVER LOGG FIL
        -- ---------------------
        v_file := utl_file.fopen( v_clean_db_dir, 'logg.txt', 'a');
        utl_file.put_line(v_file, to_char(sysdate, v_date_format)||' ��� no sessn del: '||v_no_sess_del||' ��� no user_stat del: '||v_no_ustat_del||' ��� no list del: '||v_no_list_del||' ��� no pic del: '||v_no_pic_del||' ��� no item del: '||v_no_item_del||' ��� no orguser del: '||v_no_orguser_del);
        utl_file.fclose(v_file);


END;
END clean_db;





---------------------------------------------------------
-- Name:        log_sg
-- Type:        procedure
-- What:        a procedure that log string_group data
-- Author:      Frode Klevstul
-- Start date:  15.11.2000
-- Desc:
---------------------------------------------------------
PROCEDURE log_sg
        (
                p_string_group_pk       in string_group.string_group_pk%type    default NULL
        )
IS
BEGIN
DECLARE

        v_check         number          default NULL;

BEGIN


        -- NB: for � spare tid inneholder denne proceduren INGEN test for � finne ut
        -- om forekomsten "p_string_group_pk" eksisterer. Dette kan gj�res da loggen ikke
        -- kalles f�r dette er testet i "get.txt" og "get.text" (kalles etter et select kall med EXEPTION)


   UPDATE  sg_log
     SET     last_used = sysdate
     WHERE   string_group_fk = p_string_group_pk;

   IF ( SQL%NOTFOUND ) THEN
      INSERT INTO sg_log
        (string_group_fk, last_used)
        VALUES(p_string_group_pk, sysdate);
   END IF;
   commit;

END;
END log_sg;





-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/
