CREATE OR REPLACE PACKAGE bboard_adm IS

	PROCEDURE startup;
	PROCEDURE list_item_type;
	PROCEDURE insert_item_type
			(
				p_item_type_pk		in item_type.item_type_pk%type	 	default NULL,
				p_name			in string_group.name%type	 	default NULL,
				p_description		in item_type.description%type	 	default NULL
	                );
	PROCEDURE delete_item_type
			(
				p_item_type_pk 		in item_type.item_type_pk%type		default NULL,
				p_confirm		in varchar2				default NULL
	                );
	PROCEDURE update_item_type
			(
				p_item_type_pk 		in item_type.item_type_pk%type	default NULL
	                );
	PROCEDURE item_type_service
			(
				p_item_type_fk 		in item_type_on_service.item_type_fk%type	default NULL
			);
	PROCEDURE add_service
			(
				p_item_type_fk		in item_type_on_service.item_type_fk%type 		default NULL,
				p_service_pk 		in item_type_on_service.service_fk%type 		default NULL
	                );
	PROCEDURE remove_service
			(
				p_item_type_fk		in item_type_on_service.item_type_fk%type 	default NULL,
				p_service_fk 		in item_type_on_service.service_fk%type 	default NULL
	                );
	PROCEDURE list_trade_type;
	PROCEDURE insert_trade_type
			(
				p_trade_type_pk		in trade_type.trade_type_pk%type	 default NULL,
				p_name			in string_group.name%type	 	default NULL,
				p_description		in trade_type.description%type	 	default NULL
	                );
	PROCEDURE delete_trade_type
			(
				p_trade_type_pk 	in trade_type.trade_type_pk%type	default NULL,
				p_confirm		in varchar2				default NULL
	                );
	PROCEDURE update_trade_type
			(
				p_trade_type_pk 	in trade_type.trade_type_pk%type	default NULL
	                );
	PROCEDURE list_payment_method;
	PROCEDURE insert_payment_method
			(
				p_payment_method_pk	in payment_method.payment_method_pk%type	default NULL,
				p_name			in string_group.name%type	 		default NULL,
				p_description		in payment_method.description%type	 	default NULL
			);
	PROCEDURE delete_payment_method
			(
				p_payment_method_pk 	in payment_method.payment_method_pk%type	default NULL,
				p_confirm		in varchar2					default NULL
	                );
	PROCEDURE update_payment_method
			(
				p_payment_method_pk 	in payment_method.payment_method_pk%type	default NULL
	                );

END;
/
CREATE OR REPLACE PACKAGE BODY bboard_adm IS


---------------------------------------------------------
-- Name: 	startup
-- Type: 	procedure
-- What: 	start procedure, to run the package
-- Author:  	Frode Klevstul
-- Start date: 	25.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

	list_item_type;

END startup;


/* ----------------------------------------- */
/* TABELLER FOR ADMINISTRASJON AV ITEM_TYPE */
/* ----------------------------------------- */



---------------------------------------------------------
-- Name: 	list_item_type
-- Type: 	procedure
-- What: 	procedure to list all item types
-- Author:  	Frode Klevstul
-- Start date: 	05.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE list_item_type
IS
BEGIN
DECLARE

	CURSOR 	select_all IS
	SELECT 	item_type_pk, name_sg_fk, description
	FROM	item_type;

	v_item_type_pk		item_type.item_type_pk%type	default NULL;
	v_name_sg_fk		item_type.name_sg_fk%type	default NULL;
	v_description		item_type.description%type	default NULL;


BEGIN
if ( login.timeout('bboard_adm.startup')>0 AND login.right('bboard_adm')>0 ) then

	html.b_adm_page('bboard');
	html.b_box('administrate "item_type"');
	html.b_table;

	htp.p('<tr><td><b>pk:</b></td><td><b>name:</b></td><td><b>description:</b></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>');

	open select_all;
	loop
		fetch select_all into v_item_type_pk, v_name_sg_fk, v_description;
		exit when select_all%NOTFOUND;

		htp.p('<tr><td valign="top">'|| v_item_type_pk ||'</td><td valign="top"><a href="multilang.list_all?p_name='||get.sg_name(v_name_sg_fk)||'">'|| get.sg_name(v_name_sg_fk) ||'</a></td><td valign="top">'|| v_description ||'</td><td align="right">');
		html.button_link('add/rem service','bboard_adm.item_type_service?p_item_type_fk='||v_item_type_pk);
		htp.p('</td><td align="center">');
		html.button_link('update', 'bboard_adm.update_item_type?p_item_type_pk='||v_item_type_pk);
		htp.p('</td><td>');
		html.button_link('delete', 'bboard_adm.delete_item_type?p_item_type_pk='||v_item_type_pk);
		htp.p('</td></tr>');

	end loop;
	close select_all;
	html.e_table;


	/* kode til insert form */
	html.b_form('bboard_adm.insert_item_type');
	html.b_table('60%');
	htp.p('
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr bgcolor="#eeeeee"><td colspan="2" align="center"><b>add new entry in "item_type"</b></td></tr>
		<tr><td>name:</td><td><input type="Text" name="p_name" size="40" maxlength="200"></td></tr>
		<tr><td>description:</td><td><input type="Text" name="p_description" size="50" maxlength="150"></td></tr>
		<tr><td colspan="2" align="right">'); html.submit_link('insert'); htp.p('
		</td></tr>
	');
	html.e_table;
	html.e_form;



	html.e_box;
	html.e_adm_page;

end if;
END;
END list_item_type;




---------------------------------------------------------
-- Name: 	insert_line_type
-- Type: 	procedure
-- What: 	inserts the entry into the database
-- Author:  	Frode Klevstul
-- Start date: 	05.10.2000
---------------------------------------------------------
PROCEDURE insert_item_type
		(
			p_item_type_pk		in item_type.item_type_pk%type	 	default NULL,
			p_name			in string_group.name%type	 	default NULL,
			p_description		in item_type.description%type	 	default NULL
                )
IS
BEGIN
DECLARE

	v_item_type_pk		item_type.item_type_pk%type		default NULL;
	v_name			string_group.name%type			default NULL;
	v_description		item_type.description%type		default NULL;
	v_string_group_pk	string_group.string_group_pk%type	default NULL;

	v_check			number					default NULL;

BEGIN
if ( login.timeout('bboard_adm.startup')>0 AND login.right('bboard_adm')>0 ) then


	-- sjekker om det allerede finnes en forekomst med dette navnet
	if ( p_item_type_pk IS NOT NULL ) then
		SELECT 	count(*) INTO v_check
		FROM 	string_group, item_type
		WHERE 	name = trans(p_name)
		AND	name_sg_fk = string_group_pk
		AND 	item_type_pk <> p_item_type_pk;
	else
		SELECT 	count(*) INTO v_check
		FROM 	string_group
		WHERE 	name = trans(p_name);
	end if;

	if (v_check > 0) then
		get.error_page(1, 'there already exists an entry with name = <b>'||p_name||'</b>');
		return;
	else
	begin
		v_description := p_description;
		v_description := html.rem_tag(v_description);

		-- henter ut neste nummer i sekvensen til tabellen
		if (p_item_type_pk is NULL AND p_name IS NOT NULL) then
			SELECT	item_type_seq.NEXTVAL
			INTO	v_item_type_pk
			FROM	dual;

			SELECT 	string_group_seq.NEXTVAL
			INTO	v_string_group_pk
			FROM 	dual;

			-- legger inn i databasen
			INSERT INTO string_group
			(string_group_pk, name, description)
			VALUES (v_string_group_pk, trans(p_name), '[entry inserted by bboard_adm package]');
			commit;

			INSERT INTO item_type
			(item_type_pk, name_sg_fk, description)
			VALUES (v_item_type_pk, v_string_group_pk, trans(p_description));
			commit;
		elsif (p_name IS NOT NULL) then
		begin
			UPDATE	string_group
			SET	name = trans(p_name)
			WHERE	string_group_pk =
				(
					SELECT	name_sg_fk
					FROM	item_type
					WHERE	item_type_pk = p_item_type_pk
				);
			commit;

			UPDATE	item_type
			SET	description = trans(p_description)
			WHERE	item_type_pk = p_item_type_pk;
			commit;

		end;
		end if;
	end;
	end if;

	html.jump_to('bboard_adm.list_item_type');


end if;
END;
END insert_item_type;



---------------------------------------------------------
-- Name: 	delete_item_type
-- Type: 	procedure
-- What: 	deletes an entry in the database
-- Author:  	Frode Klevstul
-- Start date: 	06.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE delete_item_type
		(
			p_item_type_pk 		in item_type.item_type_pk%type		default NULL,
			p_confirm		in varchar2				default NULL
                )
IS
BEGIN
DECLARE

	v_name_sg_fk	item_type.name_sg_fk%type	default NULL;

BEGIN
if ( login.timeout('bboard_adm.startup')>0 AND login.right('bboard_adm')>0 ) then

	if (p_confirm IS NULL) then

		SELECT	name_sg_fk INTO v_name_sg_fk
		FROM	item_type
		WHERE	item_type_pk = p_item_type_pk;

		html.b_adm_page('bboard_adm');
		html.b_box('delete item_type');
		html.b_form('bboard_adm.delete_item_type');
		html.b_table;

		htp.p('
			<input type="hidden" name="p_item_type_pk" value="'|| p_item_type_pk ||'">
			<tr><td colspan="2">Are you sure you want to delete the entry <b>'|| get.sg_name(v_name_sg_fk) ||'</b> with pk <b>'||p_item_type_pk||'</b>?</td></tr>
			<tr><td><input type="submit" name="p_confirm" value="yes"></td><td align="right"><input type="submit" name="p_confirm" value="no"></td></tr>
			</form>
		');

		html.e_table;
		html.e_form;
		html.e_box;
		html.e_adm_page;

	else

		if (p_confirm = 'yes' AND p_item_type_pk IS NOT NULL) then
			DELETE FROM item_type
			WHERE item_type_pk = p_item_type_pk;
			commit;
		end if;

		html.jump_to('bboard_adm.list_item_type');

	end if;

end if;
END;
END delete_item_type;





---------------------------------------------------------
-- Name: 	update_item_type
-- Type: 	procedure
-- What: 	updates an entry
-- Author:  	Frode Klevstul
-- Start date: 	06.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE update_item_type
		(
			p_item_type_pk 		in item_type.item_type_pk%type	default NULL
                )
IS
BEGIN
DECLARE

	v_name_sg_fk	string_group.string_group_pk%type	default NULL;
	v_description	line_type.description%type		default NULL;

BEGIN
if ( login.timeout('bboard_adm.startup')>0 AND login.right('bboard_adm')>0 ) then

	SELECT 	name_sg_fk, description INTO v_name_sg_fk, v_description
	FROM 	item_type
	WHERE 	item_type_pk = p_item_type_pk;

	v_description := html.rem_tag(v_description);

	html.b_adm_page('bboard');
	html.b_box('update item_type');
	html.b_form('bboard_adm.insert_item_type');
	html.b_table;

	htp.p('
		<tr><td colspan="2">&nbsp;</td></tr>
		<input type="hidden" name="p_item_type_pk" value="'||p_item_type_pk||'">
		<tr bgcolor="#eeeeee"><td colspan="2" align="center"><b>update entry in "item_type"</b></td></tr>
		<tr><td>name:</td><td><input type="Text" name="p_name" size="30" maxlength="50" value="'||get.sg_name(v_name_sg_fk)||'"></td></tr>
		<tr><td>description:</td><td><input type="Text" name="p_description" size="50" maxlength="50" value="'||trans(v_description)||'"></td></tr>
		<tr><td colspan="2" align="right">'); html.submit_link('update'); htp.p('
		</td></tr>
	');


	html.e_table;
	html.e_form;
	html.e_box;
	html.e_adm_page;


end if;
END;
END update_item_type;




---------------------------------------------------------
-- Name: 	item_type_service
-- Type: 	procedure
-- What: 	adm of item_type on service
-- Author:  	Frode Klevstul
-- Start date: 	06.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE item_type_service
	(
		p_item_type_fk 		in item_type_on_service.item_type_fk%type	default NULL
	)
IS
BEGIN
DECLARE

	CURSOR 	select_all IS
	SELECT 	service_fk, name_sg_fk
	FROM 	service, item_type_on_service
	WHERE	service_pk = service_fk
	AND	item_type_fk = p_item_type_fk;

	v_service_fk		service.service_pk%type		default NULL;
	v_s_name_sg_fk		service.name_sg_fk%type		default NULL;
	v_it_name_sg_fk		item_type.name_sg_fk%type	default NULL;


BEGIN
if ( login.timeout('bboard_adm.startup')>0 AND login.right('bboard_adm')>0 ) then

	SELECT 	name_sg_fk INTO v_it_name_sg_fk
	FROM 	item_type
	WHERE 	item_type_pk = p_item_type_fk;

	html.b_adm_page('bboard_adm');
	html.b_box('services connected to item type "'|| get.sg_name(v_it_name_sg_fk) ||'"');
	html.b_table('60%');
	htp.p('<tr bgcolor="#eeeeee"><td colspan="2">The item type "'|| get.sg_name(v_it_name_sg_fk) ||'" is on following services:</td></tr>');


	open select_all;
	loop
		fetch select_all into v_service_fk, v_s_name_sg_fk;
		exit when select_all%NOTFOUND;
		htp.p('<tr><td valign="top">'|| get.text(v_s_name_sg_fk) ||'</td><td>'); html.button_link('remove', 'bboard_adm.remove_service?p_item_type_fk='||p_item_type_fk||'&p_service_fk='||v_service_fk); htp.p('</td></tr>');
	end loop;
	close select_all;
	html.e_table;


	-- form for � legge til service
	html.b_form('bboard_adm.add_service');
	htp.p('<input type="hidden" name="p_item_type_fk" value="'|| p_item_type_fk ||'">');

	html.b_table('60%');
	htp.p('
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr bgcolor="#eeeeee"><td colspan="2">add service to item type "'|| get.sg_name(v_it_name_sg_fk) ||'":</td></tr>
		<tr><td>service:</td><td>
	');

		html.select_service(NULL, -1000);

	htp.p('
		</td></tr>
		<tr><td colspan="2" align="right">'); html.submit_link('add service'); htp.p('</td></tr>
	');

	html.e_table;
	html.e_form;
	html.e_box;
	html.e_adm_page;


end if;
END;
END item_type_service;




---------------------------------------------------------
-- Name: 	add_service
-- Type: 	procedure
-- What: 	adds service to a item type
-- Author:  	Frode Klevstul
-- Start date: 	06.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE add_service
		(
			p_item_type_fk		in item_type_on_service.item_type_fk%type 		default NULL,
			p_service_pk 		in item_type_on_service.service_fk%type 		default NULL
                )
IS
BEGIN
DECLARE

	v_check			number					default 0;

BEGIN
if ( login.timeout('bboard_adm.startup')>0 AND login.right('bboard_adm')>0 ) then

	SELECT 	count(*) INTO v_check
	FROM 	item_type_on_service
	WHERE	service_fk = p_service_pk
	AND	item_type_fk = p_item_type_fk;


	if ( v_check = 0 ) then
		INSERT INTO item_type_on_service
		(item_type_fk, service_fk)
		VALUES (p_item_type_fk, p_service_pk);
		commit;
	end if;


	html.jump_to('bboard_adm.item_type_service?p_item_type_fk='||p_item_type_fk);


end if;
END;
END add_service;





---------------------------------------------------------
-- Name: 	remove_service
-- Type: 	procedure
-- What: 	remove service from an item type
-- Author:  	Frode Klevstul
-- Start date: 	06.10.2000
---------------------------------------------------------
PROCEDURE remove_service
		(
			p_item_type_fk		in item_type_on_service.item_type_fk%type 	default NULL,
			p_service_fk 		in item_type_on_service.service_fk%type 	default NULL
                )
IS
BEGIN
if ( login.timeout('bboard_adm.startup')>0 AND login.right('bboard_adm')>0 ) then

	DELETE FROM item_type_on_service
	WHERE	item_type_fk = p_item_type_fk
	AND	service_fk = p_service_fk;
	commit;

	html.jump_to('bboard_adm.item_type_service?p_item_type_fk='||p_item_type_fk);

end if;
END remove_service;


/* ----------------------------------------- */
/* TABELLER FOR ADMINISTRASJON AV TRADE_TYPE */
/* ----------------------------------------- */



---------------------------------------------------------
-- Name: 	list_trade_type
-- Type: 	procedure
-- What: 	procedure to list all trade types
-- Author:  	Frode Klevstul
-- Start date: 	08.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE list_trade_type
IS
BEGIN
DECLARE

	CURSOR 	select_all IS
	SELECT 	trade_type_pk, name_sg_fk, description
	FROM	trade_type;

	v_trade_type_pk		trade_type.trade_type_pk%type	default NULL;
	v_name_sg_fk		trade_type.name_sg_fk%type	default NULL;
	v_description		trade_type.description%type	default NULL;


BEGIN
if ( login.timeout('bboard_adm.startup')>0 AND login.right('bboard_adm')>0 ) then

	html.b_adm_page('bboard');
	html.b_box('administrate "trade_type"');
	html.b_table;

	htp.p('<tr><td><b>pk:</b></td><td><b>name:</b></td><td><b>description:</b></td><td>&nbsp;</td><td>&nbsp;</td></tr>');

	open select_all;
	loop
		fetch select_all into v_trade_type_pk, v_name_sg_fk, v_description;
		exit when select_all%NOTFOUND;

		htp.p('<tr><td valign="top">'|| v_trade_type_pk ||'</td><td valign="top"><a href="multilang.list_all?p_name='||get.sg_name(v_name_sg_fk)||'">'|| get.sg_name(v_name_sg_fk) ||'</a></td><td valign="top">'|| v_description ||'</td>');
		htp.p('<td align="center">');
		html.button_link('update', 'bboard_adm.update_trade_type?p_trade_type_pk='||v_trade_type_pk);
		htp.p('</td><td>');
		html.button_link('delete', 'bboard_adm.delete_trade_type?p_trade_type_pk='||v_trade_type_pk);
		htp.p('</td></tr>');

	end loop;
	close select_all;
	html.e_table;


	/* kode til insert form */
	html.b_form('bboard_adm.insert_trade_type');
	html.b_table('60%');
	htp.p('
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr bgcolor="#eeeeee"><td colspan="2" align="center"><b>add new entry in "trade_type"</b></td></tr>
		<tr><td>name:</td><td><input type="Text" name="p_name" size="40" maxlength="200"></td></tr>
		<tr><td>description:</td><td><input type="Text" name="p_description" size="50" maxlength="150"></td></tr>
		<tr><td colspan="2" align="right">'); html.submit_link('insert'); htp.p('
		</td></tr>
	');
	html.e_table;
	html.e_form;



	html.e_box;
	html.e_adm_page;

end if;
END;
END list_trade_type;




---------------------------------------------------------
-- Name: 	insert_trade_type
-- Type: 	procedure
-- What: 	inserts the entry into the database
-- Author:  	Frode Klevstul
-- Start date: 	08.10.2000
---------------------------------------------------------
PROCEDURE insert_trade_type
		(
			p_trade_type_pk		in trade_type.trade_type_pk%type	 default NULL,
			p_name			in string_group.name%type	 	default NULL,
			p_description		in trade_type.description%type	 	default NULL
                )
IS
BEGIN
DECLARE

	v_trade_type_pk		trade_type.trade_type_pk%type		default NULL;
	v_name			string_group.name%type			default NULL;
	v_description		trade_type.description%type		default NULL;

	v_string_group_pk	string_group.string_group_pk%type	default NULL;
	v_check			number					default NULL;

BEGIN
if ( login.timeout('bboard_adm.startup')>0 AND login.right('bboard_adm')>0 ) then


	-- sjekker om det allerede finnes en forekomst med dette navnet
	if ( p_trade_type_pk IS NOT NULL ) then
		SELECT 	count(*) INTO v_check
		FROM 	string_group, trade_type
		WHERE 	name = trans(p_name)
		AND	name_sg_fk = string_group_pk
		AND 	trade_type_pk <> p_trade_type_pk;
	else
		SELECT 	count(*) INTO v_check
		FROM 	string_group
		WHERE 	name = trans(p_name);
	end if;

	if (v_check > 0) then
		get.error_page(1, 'there already exists an entry with name = <b>'||p_name||'</b>');
		return;
	else
	begin
		v_description := p_description;
		v_description := html.rem_tag(v_description);

		-- henter ut neste nummer i sekvensen til tabellen
		if (p_trade_type_pk is NULL AND p_name IS NOT NULL) then
			SELECT	trade_type_seq.NEXTVAL
			INTO	v_trade_type_pk
			FROM	dual;

			SELECT 	string_group_seq.NEXTVAL
			INTO	v_string_group_pk
			FROM 	dual;

			-- legger inn i databasen
			INSERT INTO string_group
			(string_group_pk, name, description)
			VALUES (v_string_group_pk, trans(p_name), '[entry inserted by bboard_adm package]');
			commit;

			INSERT INTO trade_type
			(trade_type_pk, name_sg_fk, description)
			VALUES (v_trade_type_pk, v_string_group_pk, trans(p_description));
			commit;
		elsif (p_name IS NOT NULL) then
		begin
			UPDATE	string_group
			SET	name = trans(p_name)
			WHERE	string_group_pk =
				(
					SELECT	name_sg_fk
					FROM	trade_type
					WHERE	trade_type_pk = p_trade_type_pk
				);
			commit;

			UPDATE	trade_type
			SET	description = trans(p_description)
			WHERE	trade_type_pk = p_trade_type_pk;
			commit;

		end;
		end if;
	end;
	end if;

	html.jump_to('bboard_adm.list_trade_type');


end if;
END;
END insert_trade_type;



---------------------------------------------------------
-- Name: 	delete_trade_type
-- Type: 	procedure
-- What: 	deletes an entry in the database
-- Author:  	Frode Klevstul
-- Start date: 	08.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE delete_trade_type
		(
			p_trade_type_pk 	in trade_type.trade_type_pk%type	default NULL,
			p_confirm		in varchar2				default NULL
                )
IS
BEGIN
DECLARE

	v_name_sg_fk	trade_type.name_sg_fk%type	default NULL;

BEGIN
if ( login.timeout('bboard_adm.startup')>0 AND login.right('bboard_adm')>0 ) then

	if (p_confirm IS NULL) then

		SELECT	name_sg_fk INTO v_name_sg_fk
		FROM	trade_type
		WHERE	trade_type_pk = p_trade_type_pk;

		html.b_adm_page('bboard_adm');
		html.b_box('delete trade_type');
		html.b_form('bboard_adm.delete_trade_type');
		html.b_table;

		htp.p('
			<input type="hidden" name="p_trade_type_pk" value="'|| p_trade_type_pk ||'">
			<tr><td colspan="2">Are you sure you want to delete the entry <b>'|| get.sg_name(v_name_sg_fk) ||'</b> with pk <b>'||p_trade_type_pk||'</b>?</td></tr>
			<tr><td><input type="submit" name="p_confirm" value="yes"></td><td align="right"><input type="submit" name="p_confirm" value="no"></td></tr>
			</form>
		');

		html.e_table;
		html.e_form;
		html.e_box;
		html.e_adm_page;

	else

		if (p_confirm = 'yes' AND p_trade_type_pk IS NOT NULL) then
			DELETE FROM trade_type
			WHERE trade_type_pk = p_trade_type_pk;
			commit;
		end if;

		html.jump_to('bboard_adm.list_trade_type');

	end if;

end if;
END;
END delete_trade_type;





---------------------------------------------------------
-- Name: 	update_trade_type
-- Type: 	procedure
-- What: 	updates an entry
-- Author:  	Frode Klevstul
-- Start date: 	08.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE update_trade_type
		(
			p_trade_type_pk 	in trade_type.trade_type_pk%type	default NULL
                )
IS
BEGIN
DECLARE

	v_name_sg_fk	string_group.string_group_pk%type	default NULL;
	v_description	trade_type.description%type		default NULL;

BEGIN
if ( login.timeout('bboard_adm.startup')>0 AND login.right('bboard_adm')>0 ) then

	SELECT 	name_sg_fk, description INTO v_name_sg_fk, v_description
	FROM 	trade_type
	WHERE 	trade_type_pk = p_trade_type_pk;

	v_description := html.rem_tag(v_description);

	html.b_adm_page('bboard');
	html.b_box('update trade_type');
	html.b_form('bboard_adm.insert_trade_type');
	html.b_table;

	htp.p('
		<tr><td colspan="2">&nbsp;</td></tr>
		<input type="hidden" name="p_trade_type_pk" value="'||p_trade_type_pk||'">
		<tr bgcolor="#eeeeee"><td colspan="2" align="center"><b>update entry in "trade_type"</b></td></tr>
		<tr><td>name:</td><td><input type="Text" name="p_name" size="30" maxlength="50" value="'||get.sg_name(v_name_sg_fk)||'"></td></tr>
		<tr><td>description:</td><td><input type="Text" name="p_description" size="50" maxlength="50" value="'||trans(v_description)||'"></td></tr>
		<tr><td colspan="2" align="right">'); html.submit_link('update'); htp.p('
		</td></tr>
	');


	html.e_table;
	html.e_form;
	html.e_box;
	html.e_adm_page;


end if;
END;
END update_trade_type;


/* --------------------------------------------- */
/* TABELLER FOR ADMINISTRASJON AV PAYMENT_METHOD */
/* --------------------------------------------- */


---------------------------------------------------------
-- Name: 	list_payment_method
-- Type: 	procedure
-- What: 	procedure to list all payment methods
-- Author:  	Frode Klevstul
-- Start date: 	08.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE list_payment_method
IS
BEGIN
DECLARE

	CURSOR 	select_all IS
	SELECT 	payment_method_pk, name_sg_fk, description
	FROM	payment_method;

	v_payment_method_pk	payment_method.payment_method_pk%type	default NULL;
	v_name_sg_fk		payment_method.name_sg_fk%type		default NULL;
	v_description		payment_method.description%type		default NULL;

BEGIN
if ( login.timeout('bboard_adm.startup')>0 AND login.right('bboard_adm')>0 ) then

	html.b_adm_page('bboard');
	html.b_box('administrate "payment_method"');
	html.b_table;

	htp.p('<tr><td><b>pk:</b></td><td><b>name:</b></td><td><b>description:</b></td><td>&nbsp;</td><td>&nbsp;</td></tr>');

	open select_all;
	loop
		fetch select_all into v_payment_method_pk, v_name_sg_fk, v_description;
		exit when select_all%NOTFOUND;

		htp.p('<tr><td valign="top">'|| v_payment_method_pk ||'</td><td valign="top"><a href="multilang.list_all?p_name='||get.sg_name(v_name_sg_fk)||'">'|| get.sg_name(v_name_sg_fk) ||'</a></td><td valign="top">'|| v_description ||'</td>');
		htp.p('<td align="center">');
		html.button_link('update', 'bboard_adm.update_payment_method?p_payment_method_pk='||v_payment_method_pk);
		htp.p('</td><td>');
		html.button_link('delete', 'bboard_adm.delete_payment_method?p_payment_method_pk='||v_payment_method_pk);
		htp.p('</td></tr>');

	end loop;
	close select_all;
	html.e_table;


	/* kode til insert form */
	html.b_form('bboard_adm.insert_payment_method');
	html.b_table('60%');
	htp.p('
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr bgcolor="#eeeeee"><td colspan="2" align="center"><b>add new entry in "payment_method"</b></td></tr>
		<tr><td>name:</td><td><input type="Text" name="p_name" size="40" maxlength="200"></td></tr>
		<tr><td>description:</td><td><input type="Text" name="p_description" size="50" maxlength="150"></td></tr>
		<tr><td colspan="2" align="right">'); html.submit_link('insert'); htp.p('
		</td></tr>
	');
	html.e_table;
	html.e_form;



	html.e_box;
	html.e_adm_page;

end if;
END;
END list_payment_method;




---------------------------------------------------------
-- Name: 	insert_payment_method
-- Type: 	procedure
-- What: 	inserts the entry into the database
-- Author:  	Frode Klevstul
-- Start date: 	08.10.2000
---------------------------------------------------------
PROCEDURE insert_payment_method
		(
			p_payment_method_pk	in payment_method.payment_method_pk%type	default NULL,
			p_name			in string_group.name%type	 		default NULL,
			p_description		in payment_method.description%type	 	default NULL
                )
IS
BEGIN
DECLARE

	v_payment_method_pk	payment_method.payment_method_pk%type		default NULL;
	v_name			string_group.name%type				default NULL;
	v_description		payment_method.description%type			default NULL;

	v_string_group_pk	string_group.string_group_pk%type	default NULL;
	v_check			number					default NULL;

BEGIN
if ( login.timeout('bboard_adm.startup')>0 AND login.right('bboard_adm')>0 ) then


	-- sjekker om det allerede finnes en forekomst med dette navnet
	if ( p_payment_method_pk IS NOT NULL ) then
		SELECT 	count(*) INTO v_check
		FROM 	string_group, payment_method
		WHERE 	name = trans(p_name)
		AND	name_sg_fk = string_group_pk
		AND 	payment_method_pk <> p_payment_method_pk;
	else
		SELECT 	count(*) INTO v_check
		FROM 	string_group
		WHERE 	name = trans(p_name);
	end if;

	if (v_check > 0) then
		get.error_page(1, 'there already exists an entry with name = <b>'||p_name||'</b>');
		return;
	else
	begin
		v_description := p_description;
		v_description := html.rem_tag(v_description);

		-- henter ut neste nummer i sekvensen til tabellen
		if (p_payment_method_pk is NULL AND p_name IS NOT NULL) then
			SELECT	payment_method_seq.NEXTVAL
			INTO	v_payment_method_pk
			FROM	dual;

			SELECT 	string_group_seq.NEXTVAL
			INTO	v_string_group_pk
			FROM 	dual;

			-- legger inn i databasen
			INSERT INTO string_group
			(string_group_pk, name, description)
			VALUES (v_string_group_pk, trans(p_name), '[entry inserted by bboard_adm package]');
			commit;

			INSERT INTO payment_method
			(payment_method_pk, name_sg_fk, description)
			VALUES (v_payment_method_pk, v_string_group_pk, trans(p_description));
			commit;
		elsif (p_name IS NOT NULL) then
		begin
			UPDATE	string_group
			SET	name = trans(p_name)
			WHERE	string_group_pk =
				(
					SELECT	name_sg_fk
					FROM	payment_method
					WHERE	payment_method_pk = p_payment_method_pk
				);
			commit;

			UPDATE	payment_method
			SET	description = trans(p_description)
			WHERE	payment_method_pk = p_payment_method_pk;
			commit;

		end;
		end if;
	end;
	end if;

	html.jump_to('bboard_adm.list_payment_method');


end if;
END;
END insert_payment_method;




---------------------------------------------------------
-- Name: 	delete_payment_method
-- Type: 	procedure
-- What: 	deletes an entry in the database
-- Author:  	Frode Klevstul
-- Start date: 	08.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE delete_payment_method
		(
			p_payment_method_pk 	in payment_method.payment_method_pk%type	default NULL,
			p_confirm		in varchar2					default NULL
                )
IS
BEGIN
DECLARE

	v_name_sg_fk	payment_method.name_sg_fk%type	default NULL;

BEGIN
if ( login.timeout('bboard_adm.startup')>0 AND login.right('bboard_adm')>0 ) then

	if (p_confirm IS NULL) then

		SELECT	name_sg_fk INTO v_name_sg_fk
		FROM	payment_method
		WHERE	payment_method_pk = p_payment_method_pk;

		html.b_adm_page('bboard_adm');
		html.b_box('delete payment_method');
		html.b_form('bboard_adm.delete_payment_method');
		html.b_table;

		htp.p('
			<input type="hidden" name="p_payment_method_pk" value="'|| p_payment_method_pk ||'">
			<tr><td colspan="2">Are you sure you want to delete the entry <b>'|| get.sg_name(v_name_sg_fk) ||'</b> with pk <b>'||p_payment_method_pk||'</b>?</td></tr>
			<tr><td><input type="submit" name="p_confirm" value="yes"></td><td align="right"><input type="submit" name="p_confirm" value="no"></td></tr>
			</form>
		');

		html.e_table;
		html.e_form;
		html.e_box;
		html.e_adm_page;

	else

		if (p_confirm = 'yes' AND p_payment_method_pk IS NOT NULL) then
			DELETE FROM payment_method
			WHERE payment_method_pk = p_payment_method_pk;
			commit;
		end if;

		html.jump_to('bboard_adm.list_payment_method');

	end if;

end if;
END;
END delete_payment_method;




---------------------------------------------------------
-- Name: 	update_payment_method
-- Type: 	procedure
-- What: 	updates an entry
-- Author:  	Frode Klevstul
-- Start date: 	08.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE update_payment_method
		(
			p_payment_method_pk 	in payment_method.payment_method_pk%type	default NULL
                )
IS
BEGIN
DECLARE

	v_name_sg_fk	string_group.string_group_pk%type	default NULL;
	v_description	payment_method.description%type		default NULL;

BEGIN
if ( login.timeout('bboard_adm.startup')>0 AND login.right('bboard_adm')>0 ) then

	SELECT 	name_sg_fk, description INTO v_name_sg_fk, v_description
	FROM 	payment_method
	WHERE 	payment_method_pk = p_payment_method_pk;

	v_description := html.rem_tag(v_description);

	html.b_adm_page('bboard');
	html.b_box('update payment_method');
	html.b_form('bboard_adm.insert_payment_method');
	html.b_table;

	htp.p('
		<tr><td colspan="2">&nbsp;</td></tr>
		<input type="hidden" name="p_payment_method_pk" value="'||p_payment_method_pk||'">
		<tr bgcolor="#eeeeee"><td colspan="2" align="center"><b>update entry in "payment_method"</b></td></tr>
		<tr><td>name:</td><td><input type="Text" name="p_name" size="30" maxlength="50" value="'||get.sg_name(v_name_sg_fk)||'"></td></tr>
		<tr><td>description:</td><td><input type="Text" name="p_description" size="50" maxlength="50" value="'||trans(v_description)||'"></td></tr>
		<tr><td colspan="2" align="right">'); html.submit_link('update'); htp.p('
		</td></tr>
	');


	html.e_table;
	html.e_form;
	html.e_box;
	html.e_adm_page;


end if;
END;
END update_payment_method;



-- ++++++++++++++++++++++++++++++++++++++++++++++ --

END; -- slutter pakke kroppen
/
