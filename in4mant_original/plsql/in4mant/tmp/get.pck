CREATE OR REPLACE PACKAGE get IS

        FUNCTION sess
                RETURN number;
        FUNCTION uid
                RETURN number;
        FUNCTION txt
                (
                        p_name                  in string_group.name%type       default NULL,
                        p_language_pk           in la.language_pk%type          default NULL
                )  RETURN varchar2;
        FUNCTION lan
                RETURN number;
        FUNCTION value (
                        p_name                  in parameter.name%type default NULL
                ) RETURN varchar2;
        FUNCTION oid
                RETURN number;
        FUNCTION oln
        (
                p_organization_pk           in organization.organization_pk%type        default NULL
        ) RETURN VARCHAR2;
        FUNCTION locn
                (
                        p_geography_pk          in geography.geography_pk%type          default NULL
                )RETURN VARCHAR2;
        FUNCTION listn (
                        p_list_pk               IN list.list_pk%TYPE                    default NULL
                )RETURN VARCHAR2;
        FUNCTION text
                (
                        p_string_group_pk       in string_group.string_group_pk%type    default NULL,
                        p_language_pk           in la.language_pk%type                  default NULL
                )  RETURN varchar2;
        FUNCTION sg_name
                (
                        p_string_group_pk       in string_group.string_group_pk%type default NULL
                )  RETURN varchar2;
        FUNCTION oname
                (
                        p_organization_pk       in organization.organization_pk%type    default NULL
                )  RETURN varchar2;
        FUNCTION msg (
                        p_msg_id        in NUMBER default NULL,
                        p_msg_text      in VARCHAR2 default NULL
        ) RETURN varchar2;
        PROCEDURE error_page
                (
                        p_msg_id        in NUMBER default NULL,
                        p_msg_text      in VARCHAR2 default NULL,
                        p_heading       in VARCHAR2     default NULL,
                        p_url           in varchar2     default NULL
                );
        FUNCTION ulpk
                RETURN number;
        FUNCTION uln
                RETURN varchar2;
        FUNCTION serv
                RETURN number;
        FUNCTION serv_name
        	(
        		p_service_pk		in service.service_pk%type	default NULL
        	)
                RETURN varchar2;
        FUNCTION oserv
                (
                        p_organization_fk       in org_service.organization_fk%type     default NULL
                )
                RETURN number;
        FUNCTION u_type
                (
                        p_user_pk       in usr.user_pk%type     default NULL
                )
                RETURN number;
        FUNCTION u_status
                (
                        p_user_pk       in usr.user_pk%type     default NULL
                )
                RETURN number;
        FUNCTION uname
                (
                        p_user_pk       in usr.user_pk%type     default NULL
                )
                RETURN varchar2;
        FUNCTION login_name
                (
                        p_user_pk       in usr.user_pk%type     default NULL
                )
                RETURN varchar2;
        FUNCTION lname
                RETURN varchar2;
        FUNCTION ap_text
                (
                        p_pk                    in number                       default NULL,
                        p_column                in varchar2                     default NULL
                )
                RETURN varchar2;
        FUNCTION random
                (
                        p_low           in number       default NULL,
                        p_high          in number       default NULL
                )
                RETURN number;
        FUNCTION no_org_list
                (
                        p_list_pk       in list.list_pk%type    default NULL
                ) RETURN number;
        FUNCTION bb_text
                (
                        p_pk            in number               default NULL,
                        p_table         in varchar2             default NULL
                ) RETURN varchar2;
        FUNCTION reg_event
                (
                        p_calendar_pk   in calendar.calendar_pk%type    default NULL
                ) RETURN number;
        FUNCTION has_reg
                (
                        p_calendar_pk   in calendar.calendar_pk%type    default NULL,
                        p_user_pk       in usr.user_pk%type             default NULL
                ) RETURN boolean;
        FUNCTION email
                (
                        p_user_pk       in usr.user_pk%type             default NULL
                ) RETURN varchar2;
        FUNCTION oemail
                (
                        p_organization_pk       in organization.organization_pk%type            default NULL
                ) RETURN varchar2;
        FUNCTION olink
                (
                        p_organization_pk       in organization.organization_pk%type            default NULL,
                        p_link                  in varchar2                                     default NULL,
                        p_language_pk           in la.language_pk%type                          default NULL
                ) RETURN varchar2;
        FUNCTION domain
                (
                        p_service_pk    in service.service_pk%type              default NULL,
                        p_language_pk   in la.language_pk%type                  default NULL
                ) RETURN varchar2;
        FUNCTION et
                (
                        p_organization_pk       in organization.organization_pk%type    default NULL,
                        p_type                  in varchar2                             default NULL
                ) RETURN number;

END;
/
CREATE OR REPLACE PACKAGE BODY get IS


---------------------------------------------------------
-- Name:        session_id
-- Type:        function
-- What:        returns the users session number
-- Author:      Frode Klevstul
-- Start date:  13.06.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
-- beskrivelse av inn parametere:
--
-- [parameter]  [description]
---------------------------------------------------------
FUNCTION sess
        RETURN number
IS
BEGIN
DECLARE
        v_session_id    sessn.session_id%type   default NULL;
BEGIN

        return system.get_cookie('in4mant_session');

END;
END sess;




---------------------------------------------------------
-- Name:        uid
-- Type:        function
-- What:        returns the user id (usr.user_pk)
-- Author:      Frode Klevstul
-- Start date:  13.06.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter] [description]
---------------------------------------------------------
FUNCTION uid
        RETURN NUMBER
IS
BEGIN
DECLARE
        v_user_pk       usr.user_pk%type                default NULL;

BEGIN

        SELECT  user_fk INTO v_user_pk
        FROM    sessn
        WHERE   session_id = system.get_cookie('in4mant_session');

        return v_user_pk;


EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
                RETURN NULL;
END;
END uid;



---------------------------------------------------------
-- Name:        txt
-- Type:        function
-- What:        returns a text string on the right language
-- Author:      Frode Klevstul
-- Start date:  01.06.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
-- beskrivelse av inn parametere:
--
-- [parameter]  [description]
---------------------------------------------------------
FUNCTION txt
        (
                p_name          in string_group.name%type       default NULL,
                p_language_pk   in la.language_pk%type          default NULL

        )  RETURN varchar2
IS
        v_string                strng.string%type                       default NULL;
        v_language_pk           la.language_pk%type                     default NULL;

        -- verdier som brukes til logg
        v_string_group_pk       string_group.string_group_pk%type       default NULL;
        v_check                 number                                  default NULL;

BEGIN

        if (p_language_pk IS NULL) then
                v_language_pk := get.lan;
        else
                v_language_pk := p_language_pk;
        end if;

        -- henter koden fra databasen
        SELECT  string, string_group_pk INTO v_string, v_string_group_pk
        FROM    strng s, groups g, string_group sg
        WHERE   string_fk = string_pk
        AND     string_group_fk = string_group_pk
        AND     name = p_name
        AND     language_fk = v_language_pk;


        -- brukes for � logge hvilke verdier i multilang modulen som brukes
        -- kan kommenteres bort for � gj�re tjenesten raskere. Man kan da ogs�
        -- droppe � hente ut string_group_pk ovenfor
-- F�r feil i arkivet dersom denne kj�res:
--	system.log_sg(v_string_group_pk);
/*
ORA-14551: kan ikke utf�re en DML-operasjon i en sp�rring ORA-06512: ved "IN4MANT_ADM.SYSTEM",
line 689 ORA-06512: ved "IN4MANT_ADM.GET", line 127 ORA-06512: ved line 1 ORA-06512: ved "IN4MANT_ADM.DOC_UTIL",
line 67 ORA-06512: ved "IN4MANT_ADM.DOC_UTIL", line 229 ORA-06512: ved line 2
*/

        RETURN v_string;


EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
                v_string := get.msg(4, p_name);
                RETURN v_string;

END txt;




---------------------------------------------------------
-- Name:        lan
-- Type:        function
-- What:        returns the language the user has choosen
-- Author:      Frode Klevstul
-- Start date:  01.06.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
-- beskrivelse av inn parametere:
--
-- [parameter]  [description]
---------------------------------------------------------
FUNCTION lan
        RETURN number
IS
        v_language_pk   la.language_pk%type             default NULL;
        v_check         number                          default NULL;

BEGIN
        v_language_pk   := system.get_cookie('in4mant_lang');

        if ( v_language_pk IS NULL ) then

                SELECT  count(*) into v_check
                FROM    la
                WHERE   language_code = get.value('default_lang');

                if (v_check = 0) then
                        htp.p( get.msg(1, 'The default language with code '''|| get.value('default_lang') ||''' don''t exist ') );
                else
                        SELECT  language_pk INTO v_language_pk
                        FROM    la
                        WHERE   language_code = get.value('default_lang');
                end if;
        end if;

        RETURN v_language_pk;

END lan;




---------------------------------------------------------
-- Name:        param
-- Type:        function
-- What:        returns a parameter from the parameter table
-- Author:      Frode Klevstul
-- Start date:  19.05.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
-- beskrivelse av inn parametere:
--
-- [parameter]  [description]
---------------------------------------------------------
FUNCTION value (
                p_name  in parameter.name%type default NULL
        ) RETURN varchar2
IS
        v_value         varchar2(500) default NULL; -- hvis NO_DATA -> returnerer en streng som er lengre enn
                                                    -- parameter.value%type, derfor settes denne til en konstant
        v_server_name   varchar2(500) default NULL;
BEGIN

        SELECT value INTO v_value
        FROM parameter
        WHERE name = p_name;

        RETURN v_value;

EXCEPTION
WHEN NO_DATA_FOUND
THEN
--      v_server_name := owa_util.get_cgi_env('SERVER_NAME');
--      owa_util.print_cgi_env;
--      IF ( v_server_name = 'student.in4mant.com') THEN
--              v_server_name := 'norsk_student_tjeneste';
--              SELECT value INTO v_value
--              FROM parameter
--              WHERE name = REPLACE(p_name,'__', '_'||v_server_name||'_');
--              v_value := v_server_name;
--      ELSE
                v_value := get.msg(1, 'there is no entry in parameter table with name = <b>'||p_name||'</b>');
--      END IF;
        RETURN v_value;
END value;




---------------------------------------------------------
-- Name:        oid
-- Type:        function
-- What:        returns the organization id
-- Author:      Frode Klevstul
-- Start date:  15.06.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter] [description]
---------------------------------------------------------
FUNCTION oid
        RETURN NUMBER
IS
BEGIN
DECLARE
        v_organization_fk       client_user.organization_fk%type        default NULL;
        v_user_pk               usr.user_pk%type                        default NULL;
BEGIN

        v_user_pk := uid;

        SELECT  organization_fk INTO v_organization_fk
        FROM    client_user
        WHERE   user_fk = v_user_pk;

        RETURN v_organization_fk;

EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
        RETURN NULL;

END;
END oid;



---------------------------------------------------------
-- Name:        text
-- Type:        function
-- What:        returns an text-string on right language
-- Author:      Frode Klevstul
-- Start date:  16.06.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter] [description]
---------------------------------------------------------
FUNCTION text
        (
                p_string_group_pk       in string_group.string_group_pk%type    default NULL,
                p_language_pk           in la.language_pk%type                  default NULL

        )  RETURN varchar2
IS
BEGIN
DECLARE
        v_string        strng.string%type               default NULL;
        v_language_pk   la.language_pk%type     default NULL;

BEGIN

        if (p_language_pk IS NULL) then
                v_language_pk := get.lan;
        else
                v_language_pk := p_language_pk;
        end if;

        SELECT string INTO v_string
        FROM string_group, groups, strng
        WHERE string_group_fk = string_group_pk
        AND string_fk = string_pk
        AND language_fk = v_language_pk
        AND string_group_pk = p_string_group_pk;

        -- logger hvilke verdier som brukes. Fjernes for � spare tid
--	system.log_sg(p_string_group_pk);

        RETURN v_string;

EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
                v_string := get.msg(4, 'pk '|| p_string_group_pk || ' dont exists in string_group on lang '|| v_language_pk);
                RETURN v_string;
END;
END text;



---------------------------------------------------------
-- Name:        sg_name
-- Type:        function
-- What:        returns the name in string_group
-- Author:      Frode Klevstul
-- Start date:  12.07.2000
-- Desc:
---------------------------------------------------------
FUNCTION sg_name
        (
                p_string_group_pk       in string_group.string_group_pk%type default NULL
        )  RETURN varchar2
IS
BEGIN
DECLARE
        v_name  string_group.name%type          default NULL;

BEGIN

        SELECT name INTO v_name
        FROM string_group
        WHERE string_group_pk = p_string_group_pk;

        RETURN v_name;

EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
                v_name := get.msg(4, 'pk '|| p_string_group_pk || ' don''t exist');
                RETURN v_name;
END;
END sg_name;


---------------------------------------------------------
-- Name:        oname
-- Type:        function
-- What:        returns the name of the organization
-- Author:      Frode Klevstul
-- Start date:  20.06.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter] [description]
---------------------------------------------------------
FUNCTION oname
        (
                p_organization_pk       in organization.organization_pk%type    default NULL
        )  RETURN varchar2
IS
BEGIN
DECLARE

        v_organization_pk       organization.organization_pk%type       default NULL;
        v_name                  organization.name%type                  default NULL;

BEGIN

        if (p_organization_pk IS NULL) then
                v_organization_pk := get.oid;
        else
                v_organization_pk := p_organization_pk;
        end if;

        SELECT name INTO v_name
        FROM organization
        WHERE organization_pk = v_organization_pk;

        RETURN v_name;

EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
                v_name := get.msg(1, 'pk '|| p_organization_pk || ' dont exist, or dont have a name');
                RETURN v_name;
END;
END oname;



---------------------------------------------------------
-- Name:        msg
-- Type:        function
-- What:        returns a message (error msg)
-- Author:      Frode Klevstul
-- Start date:  07.07.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
-- beskrivelse av inn parametere:
--
-- [parameter]  [description]
---------------------------------------------------------
FUNCTION msg (
                        p_msg_id        in NUMBER default NULL,
                        p_msg_text      in VARCHAR2 default NULL
        ) RETURN varchar2
IS
        v_return_string         varchar2(500) default NULL;
BEGIN

        -- -----------------------
        -- forklaring av msg_id:
        -- -----------------------
        -- 1:   Skriv ut feilmelding (Error) med r�d skrivft p� skjermen.
        --      Brukes til Alvorlige feil, der det ikke er naturlig � fortsette videre.
        -- 2:   Skriver ut en "Notice" i samme layout som ovenfor
        -- 3:   Skriver ut feilen som sort tekst.
        -- 4:   Brukes dersom man ikke finner strengen i "multi language" modulen

        if (p_msg_id = 1) then
                v_return_string := '<b><font color="#000000" face="arial, helvetica" size="1">ERROR::</b></font><br><font color="#ff0000" face="arial, helvetica" size="1">' || p_msg_text || '</font>';
        elsif (p_msg_id = 2) then
                v_return_string := '<b><font color="#000000">NOTICE::</b><br><font color="#ff0000">' || p_msg_text || '</font>';
        elsif (p_msg_id = 3) then
                v_return_string := p_msg_text;
        elsif (p_msg_id = 4) then
                v_return_string := '['|| p_msg_text ||']';
        else
                v_return_string := 'MSG ID NOT SUPPORTED IN get.msg';
        end if;

        return v_return_string;

END msg;


---------------------------------------------------------
-- Name:        error_page
-- Type:        procedure
-- What:        writes out an error page with an error, and jumps back
-- Author:      Frode Klevstul
-- Start date:  01.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE error_page
        (
                p_msg_id        in number       default NULL,
                p_msg_text      in varchar2     default NULL,
                p_heading       in varchar2     default NULL,
                p_url           in varchar2     default NULL
        )
IS
BEGIN
DECLARE
        v_url           varchar2(350)           default NULL;
	v_reload_time	number			default NULL;

BEGIN

	v_reload_time := 12;

        if (p_url IS NULL) then
                v_url   := owa_util.get_cgi_env('HTTP_REFERER');
        else
                v_url   := p_url;
        end if;

        if (p_heading = '2') then
                html.b_page_2(NULL, v_reload_time, v_url);
        elsif (p_heading IS NULL OR p_heading = 'main_page') then
                html.b_page(NULL, v_reload_time, v_url);
        end if;

        html.b_box( get.txt('error') );

        -- -----------------------
        -- forklaring av msg_id:
        -- -----------------------
        -- 1:   Skriv ut feilmelding (Error) med r�d skrivft p� skjermen.
        --      Brukes til Alvorlige feil, der det ikke er naturlig � fortsette videre.
        -- 2:   Skriver ut en "Notice" i samme layout som ovenfor
        -- 3:   Skriver ut feilen som sort tekst.

        if (p_msg_id = 1) then
                htp.p( '<b><font color="#000000" face="arial, helvetica" size="1">ERROR::</b></font><br><font color="#ff0000" face="arial, helvetica" size="1">' || p_msg_text || '</font>' );
        elsif (p_msg_id = 2) then
                htp.p('<b><font color="#000000">NOTICE::</b><br><font color="#ff0000">' || p_msg_text || '</font>');
        elsif (p_msg_id = 3) then
                htp.p( p_msg_text );
        else
                htp.p('p_msg_id is not supported in get.error_page');
        end if;

        htp.p('<br><br><a href="'||v_url||'">'|| get.txt('back') ||'</a>');

        html.e_box;

        if (p_heading = '2') then
                html.e_page_2;
        elsif (p_heading IS NULL OR p_heading = 'main_page') then
                html.e_page;
        end if;

END;
END error_page;


---------------------------------------------------------
-- Name:        ulpk
-- Type:        function
-- What:        returns "user location pk"
-- Author:      Frode Klevstul
-- Start date:  13.10.2000
-- Desc:
---------------------------------------------------------
FUNCTION ulpk
        RETURN number
IS
        v_geography_pk  geography.geography_pk%type             default NULL;
        v_user_pk       usr.user_pk%type                        default NULL;

BEGIN

        v_user_pk := get.uid;

        SELECT  geography_location_fk INTO v_geography_pk
        FROM    user_details
        WHERE   user_fk = v_user_pk;

        RETURN v_geography_pk;

EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
                RETURN NULL;

END ulpk;


---------------------------------------------------------
-- Name:        uln
-- Type:        function
-- What:        returns the name of the location the user has choosen (User Location Name)
-- Author:      Frode Klevstul
-- Start date:  08.07.2000
-- Desc:
---------------------------------------------------------
FUNCTION uln
        RETURN varchar2
IS
        v_name                          varchar2(200)                           default NULL;
        v_name_sg_fk                    geography.name_sg_fk%type               default NULL;
        v_geography_location_fk         user_details.geography_location_fk%type default NULL;
        v_user_pk                       usr.user_pk%type                        default NULL;

BEGIN

        v_user_pk := get.uid;

        SELECT  geography_location_fk INTO v_geography_location_fk
        FROM    user_details
        WHERE   user_fk = v_user_pk;

        if (v_geography_location_fk IS NULL) then
                v_name := get.txt('no_location_set');
        else
                SELECT  name_sg_fk INTO v_name_sg_fk
                FROM    geography
                WHERE   geography_pk = v_geography_location_fk;

                v_name := get.text(v_name_sg_fk);
        end if;

        RETURN v_name;

END uln;

---------------------------------------------------------
-- Name:        oln
-- Type:        function
-- What:        returns the name of the location for
--              the organization (Organization Location Name)
-- Author:      Frode Klevstul
-- Start date:  08.07.2000
-- Desc:
---------------------------------------------------------
FUNCTION oln
        (
                p_organization_pk           in organization.organization_pk%type        default NULL
        )
        RETURN varchar2
IS
        v_geography_pk                  geography.geography_pk%type             default NULL;

BEGIN

        SELECT  geography_fk INTO v_geography_pk
        FROM    organization
        WHERE   organization_pk = p_organization_pk;

        RETURN get.locn(v_geography_pk);

EXCEPTION
        WHEN NO_DATA_FOUND THEN
                RETURN NULL;
END oln;


---------------------------------------------------------
-- Name:        locn
-- Type:        function
-- What:        returns the name of an location
-- Author:      Frode Klevstul
-- Start date:  14.10.2000
-- Desc:
---------------------------------------------------------
FUNCTION locn
        (
                p_geography_pk          in geography.geography_pk%type          default NULL
        )
        RETURN varchar2
IS
        v_name                          varchar2(200)                           default NULL;
        v_name_sg_fk                    geography.name_sg_fk%type               default NULL;

BEGIN


        SELECT  name_sg_fk INTO v_name_sg_fk
        FROM    geography
        WHERE   geography_pk = p_geography_pk;

        v_name := get.text(v_name_sg_fk);

        RETURN v_name;

EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
                if (p_geography_pk IS NOT NULL) then
                        RETURN 'wrong geography_pk ('||p_geography_pk||')';
                else
                        RETURN NULL;
                end if;
END locn;

---------------------------------------------------------
-- Name:        listn
-- Type:        function
-- What:        returns the name of an list
-- Author:      Espen Messel
-- Start date:  15.10.2000
-- Desc:
---------------------------------------------------------
FUNCTION listn
        (
                p_list_pk               in list.list_pk%type            default NULL
        )
        RETURN varchar2
IS
        v_name                          varchar2(200)                           default NULL;
        v_name_sg_fk                    geography.name_sg_fk%type               default NULL;

BEGIN


        SELECT  name_sg_fk INTO v_name_sg_fk
        FROM    list
        WHERE   list_pk = p_list_pk;

        v_name := get.text(v_name_sg_fk);

        RETURN v_name;

EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
                if (p_list_pk IS NOT NULL) then
                        RETURN 'wrong list_pk ('||p_list_pk||')';
                else
                        RETURN NULL;
                end if;
END listn;


---------------------------------------------------------
-- Name:        serv
-- Type:        function
-- What:        returns the service_pk the user has choosen
-- Author:      Frode Klevstul
-- Start date:  01.06.2000
---------------------------------------------------------
FUNCTION serv
        RETURN number
IS
        v_service_pk    service.service_pk%type         default NULL;
        v_server_name   VARCHAR2(30)                    DEFAULT NULL;
        v_check         number                          default NULL;

BEGIN
        v_server_name := owa_util.get_cgi_env('SERVER_NAME');

        SELECT  count( DISTINCT(service_fk) )
        INTO    v_check
        FROM    service_domain
        WHERE   domain = v_server_name;

        if (v_check = 1) then
                SELECT  DISTINCT(service_fk)
                INTO    v_service_pk
                FROM    service_domain
                WHERE   domain = v_server_name;
        else
                SELECT  service_pk
                INTO    v_service_pk
                FROM    service
                WHERE   description = get.value('default_service');
        end if;

        RETURN v_service_pk;

END serv;


---------------------------------------------------------
-- Name:        serv_name
-- Type:        function
-- What:        returns the name of the service the user has choosen
-- Author:      Frode Klevstul
-- Start date:  09.07.2000
---------------------------------------------------------
FUNCTION serv_name
	(
		p_service_pk		in service.service_pk%type	default NULL
	)
        RETURN varchar2
IS
        v_service_pk    service.service_pk%type         default NULL;
        v_description   service.description%type        default NULL;

BEGIN

	IF (p_service_pk IS NULL) THEN
	        v_service_pk    := get.serv;
	ELSE
	        v_service_pk    := p_service_pk;
	END IF;

        IF ( v_service_pk IS NULL ) THEN
                v_description := get.value('default_service');
        ELSE
                SELECT  description INTO v_description
                FROM    service
                WHERE   service_pk = v_service_pk;
        END IF;

        RETURN v_description;

END serv_name;



---------------------------------------------------------
-- Name:        oserv
-- Type:        function
-- What:        returns the service_pk the org is on
-- Author:      Frode Klevstul
-- Start date:  19.07.2000
---------------------------------------------------------
FUNCTION oserv
        (
                p_organization_fk       in org_service.organization_fk%type     default NULL
        )
        RETURN number
IS
        v_service_fk    org_service.service_fk%type             default NULL;

BEGIN
        SELECT  service_fk INTO v_service_fk
        FROM    org_service
        WHERE   organization_fk = p_organization_fk;

        RETURN v_service_fk;

END oserv;




---------------------------------------------------------
-- Name:        u_type
-- Type:        function
-- What:        returns the user_type number
-- Author:      Frode Klevstul
-- Start date:  26.07.2000
---------------------------------------------------------
FUNCTION u_type
        (
                p_user_pk       in usr.user_pk%type     default NULL
        )
        RETURN number
IS
        v_user_pk       usr.user_pk%type                default NULL;
        v_user_type_fk  usr.user_type_fk%type           default NULL;

BEGIN

        if (p_user_pk IS NULL) then
                v_user_pk := get.uid;
        else
                v_user_pk := p_user_pk;
        end if;

        SELECT  user_type_fk INTO v_user_type_fk
        FROM    usr
        WHERE   user_pk = v_user_pk;

        RETURN v_user_type_fk;

EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
                RETURN NULL;

END u_type;



---------------------------------------------------------
-- Name:        u_status
-- Type:        function
-- What:        returns the user_status number
-- Author:      Frode Klevstul
-- Start date:  30.09.2000
---------------------------------------------------------
FUNCTION u_status
        (
                p_user_pk       in usr.user_pk%type     default NULL
        )
        RETURN number
IS
        v_user_pk               usr.user_pk%type                default NULL;
        v_user_status_fk        usr.user_status_fk%type         default NULL;

BEGIN

        if (p_user_pk IS NULL) then
                v_user_pk := get.uid;
        else
                v_user_pk := p_user_pk;
        end if;

        SELECT  user_status_fk INTO v_user_status_fk
        FROM    usr
        WHERE   user_pk = v_user_pk;

        RETURN v_user_status_fk;

EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
                RETURN NULL;

END u_status;



---------------------------------------------------------
-- Name:        uname
-- Type:        function
-- What:        returns the users first and last name
-- Author:      Frode Klevstul
-- Start date:  27.07.2000
---------------------------------------------------------
FUNCTION uname
        (
                p_user_pk       in usr.user_pk%type     default NULL
        )
        RETURN varchar2
IS

        v_first_name    user_details.first_name%type    default NULL;
        v_last_name     user_details.last_name%type     default NULL;
        v_user_pk       usr.user_pk%type                default NULL;

BEGIN

        if ( p_user_pk IS NULL ) then
                v_user_pk := get.uid;
        else
                v_user_pk := p_user_pk;
        end if;

        SELECT  first_name, last_name INTO v_first_name, v_last_name
        FROM    user_details
        WHERE   user_fk = v_user_pk;

        if (v_first_name is null and v_last_name is null) then
                RETURN get.login_name(v_user_pk);
        else
                RETURN v_first_name ||' '|| v_last_name;
        end if;

EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
                RETURN get.txt('no_name');

END uname;



---------------------------------------------------------
-- Name:        login_name
-- Type:        function
-- What:        returns the users login name
-- Author:      Frode Klevstul
-- Start date:  27.07.2000
---------------------------------------------------------
FUNCTION login_name
        (
                p_user_pk       in usr.user_pk%type     default NULL
        )
        RETURN varchar2
IS

        v_login_name    usr.login_name%type             default NULL;
        v_user_pk       usr.user_pk%type                default NULL;

BEGIN

        if ( p_user_pk IS NULL ) then
                v_user_pk := get.uid;
        else
                v_user_pk := p_user_pk;
        end if;

        SELECT  login_name INTO v_login_name
        FROM    usr
        WHERE   user_pk = v_user_pk;

        RETURN v_login_name;

EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
                RETURN NULL;

END login_name;


---------------------------------------------------------
-- Name:        lname
-- Type:        function
-- What:        returns the name of the language
-- Author:      Frode Klevstul
-- Start date:  01.08.2000
---------------------------------------------------------
FUNCTION lname
        RETURN varchar2
IS

        v_language_name         la.language_name%type   default NULL;

BEGIN

        SELECT  language_name INTO v_language_name
        FROM    la
        WHERE   language_pk = get.lan;

        RETURN v_language_name;

EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
                RETURN NULL;

END lname;




---------------------------------------------------------
-- Name:        ap_text
-- Type:        function
-- What:        returns text from album or picture
-- Author:      Frode Klevstul
-- Start date:  04.08.2000
---------------------------------------------------------
FUNCTION ap_text
        (
                p_pk                    in number                       default NULL,
                p_column                in varchar2                     default NULL
        )
        RETURN varchar2
IS

        v_check                 number                          default NULL;
        v_language_pk           la.language_pk%type             default NULL;

        v_title                 album_text.title%type     	default NULL;
        v_description           album_text.description%type     default NULL;
        v_when_period           album_text.when_period%type     default NULL;
        v_what                  picture_text.what%type          default NULL;

BEGIN

        v_language_pk := get.lan;

        if ( p_column = 'title' ) then
                SELECT  title INTO v_title
                FROM    album_text
                WHERE   album_fk = p_pk
                AND     language_fk = v_language_pk;

                v_title := html.rem_tag(v_title);
                if (v_title IS NULL) then
                        v_title := '-';
                end if;
                RETURN v_title;
        elsif ( p_column = 'description' ) then
                SELECT  description INTO v_description
                FROM    album_text
                WHERE   album_fk = p_pk
                AND     language_fk = v_language_pk;

                v_description := html.rem_tag(v_description);
                RETURN v_description;
        elsif ( p_column = 'when_period' ) then
                SELECT  when_period INTO v_when_period
                FROM    album_text
                WHERE   album_fk = p_pk
                AND     language_fk = v_language_pk;

                v_when_period := html.rem_tag(v_when_period);
                if (v_when_period IS NULL) then
                        v_when_period := '-';
                end if;
                RETURN v_when_period;
        elsif ( p_column = 'what' ) then
                SELECT  what INTO v_what
                FROM    picture_text
                WHERE   picture_fk = p_pk
                AND     language_fk = v_language_pk;

                v_what := html.rem_tag(v_what);
                RETURN v_what;
        else
                RETURN NULL;
        end if;

EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
                RETURN '-';

END ap_text;




---------------------------------------------------------
-- Name:        random
-- Type:        function
-- What:        returns a "random" number
-- Author:      Frode Klevstul
-- Start date:  23.08.2000
-- Desc:
---------------------------------------------------------
FUNCTION random
        (
                p_low           in number       default NULL,
                p_high          in number       default NULL
        )
        RETURN number
IS
BEGIN
DECLARE

        v_tmp_n1                number                  default NULL;
        v_tmp_c1                varchar2(10)            default NULL;
        v_tmp_n2                number                  default NULL;
        v_tmp_c2                varchar2(10)            default NULL;
        v_tmp_n3                number                  default NULL;
        v_tmp_c3                varchar2(10)            default NULL;

        v_return_value          number                  default NULL;
BEGIN

        -- dersom p_low er h�yere en p_high slutter vi
        if ( (p_low > p_high) OR (p_low IS NULL) OR (p_high IS NULL) ) then
                htp.p( get.msg(1, 'p_low higher than p_high') );
                v_return_value := 0;
                RETURN v_return_value;
        -- sjekker at p_low og p_high er innenfor omr�de denne prosedyren returnerer
        elsif ( (p_low < 0) OR (p_high > 999) ) then
                htp.p( get.msg(1, 'values out of range') );
                v_return_value := 0;
                RETURN v_return_value;
        end if;

        v_return_value := p_high + 1;

                -- HH24:MI:SS
                -- henter ut f�rste siffer (siste sifferet i SS (sekunder))
                v_tmp_c1        := to_char (sysdate, 'SS');
                v_tmp_c1        := SUBSTR(v_tmp_c1, 2, 1);
                v_tmp_n1        := to_number(v_tmp_c1);

                -- henter ut andre siffer (siste siffer i min. + f�rste i sek.)
                v_tmp_c2        := to_char (sysdate, 'SS');
                v_tmp_c2        := SUBSTR(v_tmp_c2, 1, 1);
                v_tmp_n2        := to_number(v_tmp_c2);

                v_tmp_c2        := to_char (sysdate, 'MI');
                v_tmp_c2        := SUBSTR(v_tmp_c2, 2, 1);
                v_tmp_n2        := to_number(v_tmp_c2) + v_tmp_n2;

                if (v_tmp_n2 > 9) then
                        v_tmp_n2        := to_number(SUBSTR(to_char(v_tmp_n2), 2, 1));
                end if;

                -- henter ut tredje siffer (f�rste siffer i sek. + siste siffer i sek.)
                v_tmp_c3        := to_char (sysdate, 'SS');
                v_tmp_c3        := SUBSTR(v_tmp_c3, 1, 1);
                v_tmp_n3        := to_number(v_tmp_c3);

                v_tmp_c3        := to_char (sysdate, 'SS');
                v_tmp_c3        := SUBSTR(v_tmp_c3, 2, 1);
                v_tmp_n3        := to_number(v_tmp_c3) + v_tmp_n3;

                if (v_tmp_n3 > 9) then
                        v_tmp_n3        := to_number(SUBSTR(to_char(v_tmp_n3), 2, 1));
                end if;


                if (p_high > 99) then
                        v_return_value := v_tmp_n1 ||''||v_tmp_n2 ||''||v_tmp_n3;
                elsif (p_high > 9) then
                        v_return_value := v_tmp_n1 ||''||v_tmp_n2;
                else
                        v_return_value := v_tmp_n1;
                end if;

                while (v_return_value > p_high) loop
                        v_return_value := v_return_value - p_high;
                end loop;

                while (v_return_value < p_low) loop
                        if ( (v_return_value + p_low) < p_high ) then
                                v_return_value := v_return_value + p_low;
                        elsif ( (v_return_value + 10) < p_high ) then
                                v_return_value := v_return_value + 10;
                        elsif ( (v_return_value + 5) < p_high ) then
                                v_return_value := v_return_value + 5;
                        else
                                v_return_value := v_return_value + 1;
                        end if;
                end loop;

        RETURN v_return_value;

END;
END random;



---------------------------------------------------------
-- Name:        no_org_list
-- Type:        function
-- What:        returns the number of org on a list
-- Author:      Frode Klevstul
-- Start date:  14.09.2000
-- Desc:
---------------------------------------------------------
FUNCTION no_org_list
        (
                p_list_pk       in list.list_pk%type    default NULL
        ) RETURN number
IS
BEGIN
DECLARE
        v_list_pk       list.list_pk%type       default NULL;
        v_no_org        number                  default NULL;
BEGIN

        v_list_pk := p_list_pk;

        SELECT  count( DISTINCT(organization_pk) ) INTO v_no_org
        FROM    list_org lo, list l, organization o
        WHERE   lo.list_fk = l.list_pk
        AND     (
                level0 = v_list_pk OR
                level1 = v_list_pk OR
                level2 = v_list_pk OR
                level3 = v_list_pk OR
                level4 = v_list_pk OR
                level5 = v_list_pk
                )
        AND     o.organization_pk = lo.organization_fk
        AND     o.accepted > 0;

        RETURN v_no_org;

END;
END no_org_list;



---------------------------------------------------------
-- Name:        bb_type_name
-- Type:        function
-- What:        returns a name of a type in bulletin board module
-- Author:      Frode Klevstul
-- Start date:  11.10.2000
-- Desc:
---------------------------------------------------------
FUNCTION bb_text
        (
                p_pk            in number               default NULL,
                p_table         in varchar2             default NULL
        ) RETURN varchar2
IS
BEGIN
DECLARE

        v_name_sg_fk            string_group.string_group_pk%type       default NULL;
        v_string                strng.string%type                       default NULL;

BEGIN

        if (p_table = 'trade_type') then
                SELECT  name_sg_fk INTO v_name_sg_fk
                FROM    trade_type
                WHERE   trade_type_pk = p_pk;
        elsif (p_table = 'item_type') then
                SELECT  name_sg_fk INTO v_name_sg_fk
                FROM    item_type
                WHERE   item_type_pk = p_pk;
        elsif (p_table = 'payment_method') then
                SELECT  name_sg_fk INTO v_name_sg_fk
                FROM    payment_method
                WHERE   payment_method_pk = p_pk;
        end if;


        if (v_name_sg_fk IS NOT NULL) then
                v_string := get.text(v_name_sg_fk);

                RETURN v_string;
        else
                RETURN NULL;
        end if;

EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
                RETURN 'not a valid pk';
END;
END bb_text;




---------------------------------------------------------
-- Name:        reg_event
-- Type:        function
-- What:        returns the reg id for an calendar event
-- Author:      Frode Klevstul
-- Start date:  24.10.2000
-- Desc:
---------------------------------------------------------
FUNCTION reg_event
        (
                p_calendar_pk   in calendar.calendar_pk%type    default NULL
        ) RETURN number
IS
BEGIN
DECLARE

        v_registration_pk       registration.registration_pk%type       default NULL;

BEGIN

        SELECT  registration_pk
        INTO    v_registration_pk
        FROM    registration
        WHERE   calendar_fk = p_calendar_pk
        AND     start_date < sysdate
        AND     end_date > sysdate;

        RETURN v_registration_pk;

EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
                RETURN NULL;
END;
END reg_event;





---------------------------------------------------------
-- Name:        has_reg
-- Type:        function
-- What:        true if user has reg the calendar event
-- Author:      Frode Klevstul
-- Start date:  24.10.2000
-- Desc:
---------------------------------------------------------
FUNCTION has_reg
        (
                p_calendar_pk   in calendar.calendar_pk%type    default NULL,
                p_user_pk       in usr.user_pk%type             default NULL
        ) RETURN boolean
IS
BEGIN
DECLARE

        v_check                 number                                  default NULL;
        v_user_pk               usr.user_pk%type                        default NULL;

BEGIN

        if (p_user_pk IS NULL) then
                v_user_pk := get.uid;
        else
                v_user_pk := p_user_pk;
        end if;


        SELECT  count(*)
        INTO    v_check
        FROM    calendar, registration, user_reg
        WHERE   calendar_pk = calendar_fk
        AND     registration_pk = registration_fk
        AND     user_fk = v_user_pk
        AND     calendar_pk = p_calendar_pk;

        if (v_check = 1) then
                RETURN true;
        else
                RETURN false;
        end if;


EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
                RETURN false;
END;
END has_reg;




---------------------------------------------------------
-- Name:        email
-- Type:        function
-- What:        returns an users email address
-- Author:      Frode Klevstul
-- Start date:  24.10.2000
-- Desc:
---------------------------------------------------------
FUNCTION email
        (
                p_user_pk       in usr.user_pk%type             default NULL
        ) RETURN varchar2
IS
BEGIN
DECLARE

        v_user_pk               usr.user_pk%type                        default NULL;
        v_email                 user_details.email%type                 default NULL;

BEGIN

        if (p_user_pk IS NULL) then
                v_user_pk := get.uid;
        else
                v_user_pk := p_user_pk;
        end if;

        SELECT  email
        INTO    v_email
        FROM    user_details
        WHERE   user_fk = v_user_pk;

        RETURN v_email;

EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
                RETURN NULL;
END;
END email;





---------------------------------------------------------
-- Name:        oemail
-- Type:        function
-- What:        returns an organizations email address
-- Author:      Frode Klevstul
-- Start date:  28.10.2000
-- Desc:
---------------------------------------------------------
FUNCTION oemail
        (
                p_organization_pk       in organization.organization_pk%type            default NULL
        ) RETURN varchar2
IS
BEGIN
DECLARE

        v_organization_pk       organization.organization_pk%type       default NULL;
        v_email                 user_details.email%type                 default NULL;

BEGIN

        if (p_organization_pk IS NULL) then
                v_organization_pk := get.oid;
        else
                v_organization_pk := p_organization_pk;
        end if;

        SELECT  email
        INTO    v_email
        FROM    organization
        WHERE   organization_pk = v_organization_pk;

        RETURN v_email;

EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
                RETURN NULL;
END;
END oemail;




---------------------------------------------------------
-- Name:        olink
-- Type:        function
-- What:        returns a link to an organizations page
-- Author:      Frode Klevstul
-- Start date:  30.10.2000
-- Desc:
---------------------------------------------------------
FUNCTION olink
        (
                p_organization_pk       in organization.organization_pk%type            default NULL,
                p_link                  in varchar2                                     default NULL,
                p_language_pk           in la.language_pk%type                          default NULL
        ) RETURN varchar2
IS
BEGIN
DECLARE

        v_organization_pk       organization.organization_pk%type       default NULL;
        v_link                  varchar2(250)                           default NULL;
        v_oserv                 number                                  default NULL;
        v_language_pk           la.language_pk%type                     default NULL;

BEGIN

        if (p_organization_pk IS NULL) then
                v_organization_pk := get.oid;
        else
                v_organization_pk := p_organization_pk;
        end if;

        if (p_language_pk IS NULL) then
                v_language_pk := get.lan;
        else
                v_language_pk := p_language_pk;
        end if;


        v_link  := p_link;
        v_oserv := get.oserv(v_organization_pk);

        -- dersom org er med p� en annen tjeneste enn det som vi er p� s� m� vi bytte til riktig domene
        -- vi bytter ikke dersom linken som sendes er fra "user_sub" pakken (hvor vi melder p� organisasjoner)
        if not ( owa_pattern.match(p_link, 'user_sub.')) then
                v_link := 'http://'|| get.domain(v_oserv, v_language_pk) ||'/cgi-bin/'||v_link;
        end if;

        RETURN v_link;

EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
                RETURN NULL;
END;
END olink;




---------------------------------------------------------
-- Name:        domain
-- Type:        function
-- What:        returns the domain for a service
-- Author:      Frode Klevstul
-- Start date:  30.10.2000
-- Desc:
---------------------------------------------------------
FUNCTION domain
        (
                p_service_pk    in service.service_pk%type              default NULL,
                p_language_pk   in la.language_pk%type                  default NULL
        ) RETURN varchar2
IS
BEGIN
DECLARE

        v_service_pk            service.service_pk%type                 default NULL;
        v_language_pk           la.language_pk%type                     default NULL;
        v_domain                service_domain.domain%type              default NULL;
        v_check                 number                                  default NULL;

BEGIN

        if (p_service_pk IS NULL) then
                v_service_pk := get.serv;
        else
                v_service_pk := p_service_pk;
        end if;

        if (p_language_pk IS NULL) then
                v_language_pk := get.lan;
        else
                v_language_pk := p_language_pk;
        end if;

        SELECT  count(*)
        INTO    v_check
        FROM    service_domain
        WHERE   service_fk = v_service_pk
        AND     language_fk = v_language_pk;

        if (v_check = 1) then
                SELECT  domain
                INTO    v_domain
                FROM    service_domain
                WHERE   service_fk = v_service_pk
                AND     language_fk = v_language_pk;
        else
                v_domain := NULL;
        end if;

        RETURN v_domain;

EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
                RETURN NULL;
END;
END domain;



---------------------------------------------------------
-- Name:        et
-- Type:        function
-- What:        returns the email_text_pk for an organization
-- Author:      Frode Klevstul
-- Start date:  13.11.2000
-- Desc:
---------------------------------------------------------
FUNCTION et
        (
                p_organization_pk       in organization.organization_pk%type    default NULL,
                p_type                  in varchar2                             default NULL
        ) RETURN number
IS
BEGIN
DECLARE

        CURSOR  select_et_type IS
        SELECT  et_type_pk, name
        FROM    et_type;

        CURSOR  select_org IS
        SELECT  email_text_pk
        FROM    email_text et, et_relation etr
        WHERE   et.email_text_pk = etr.email_text_fk
        AND     organization_fk = p_organization_pk
	AND	(start_date > sysdate OR start_date IS NULL)
	AND	(end_date < sysdate OR end_date IS NULL)
	AND	(volume IS NULL OR volume > (
			SELECT	sum(volume)
			FROM	mail_log
			WHERE	email_text_fk = (
			        SELECT  email_text_pk
			        FROM    email_text et, et_relation etr
			        WHERE   et.email_text_pk = etr.email_text_fk
			        AND     organization_fk = p_organization_pk
				AND	(start_date > sysdate OR start_date IS NULL)
				AND	(end_date < sysdate OR end_date IS NULL)
				)
			)
		);

        CURSOR  select_serv IS
	SELECT  email_text_pk
	FROM    email_text et, et_relation etr
	WHERE   et.email_text_pk = etr.email_text_fk
	AND     service_fk = get.oserv(p_organization_pk)
	AND	(start_date > sysdate OR start_date IS NULL)
	AND	(end_date < sysdate OR end_date IS NULL)
	AND	(volume IS NULL OR volume > (
			SELECT	sum(volume)
			FROM	mail_log
			WHERE	email_text_fk = (
				SELECT	email_text_pk
				FROM    email_text et, et_relation etr
				WHERE   et.email_text_pk = etr.email_text_fk
				AND     service_fk = get.oserv(p_organization_pk)
				AND	(start_date > sysdate OR start_date IS NULL)
				AND	(end_date < sysdate OR end_date IS NULL)
				)
			)
		);


        CURSOR  select_list IS
        SELECT  email_text_pk
        FROM    email_text et, et_relation etr, list_org lo, list l
        WHERE   et.email_text_pk = etr.email_text_fk
        AND     etr.list_fk = l.list_pk
        AND     lo.organization_fk = p_organization_pk
        AND     (
                        lo.list_fk = l.list_pk
                OR      lo.list_fk = l.level0
                OR      lo.list_fk = l.level1
                OR      lo.list_fk = l.level2
                OR      lo.list_fk = l.level3
                OR      lo.list_fk = l.level4
                OR      lo.list_fk = l.level5
        )
	AND	(start_date > sysdate OR start_date IS NULL)
	AND	(end_date < sysdate OR end_date IS NULL)
	AND	(volume IS NULL OR volume > (
			SELECT	sum(volume)
			FROM	mail_log
			WHERE	email_text_fk = (
			        SELECT  email_text_pk
			        FROM    email_text et, et_relation etr, list_org lo, list l
			        WHERE   et.email_text_pk = etr.email_text_fk
			        AND     etr.list_fk = l.list_pk
			        AND     lo.organization_fk = p_organization_pk
			        AND     (
			                        lo.list_fk = l.list_pk
			                OR      lo.list_fk = l.level0
			                OR      lo.list_fk = l.level1
			                OR      lo.list_fk = l.level2
			                OR      lo.list_fk = l.level3
			                OR      lo.list_fk = l.level4
			                OR      lo.list_fk = l.level5
			        )
				AND	(start_date > sysdate OR start_date IS NULL)
				AND	(end_date < sysdate OR end_date IS NULL)
				)
			)
		);

        CURSOR  select_geo IS
        SELECT  email_text_pk
        FROM    email_text et, et_relation etr, organization o, geography g
        WHERE   et.email_text_pk = etr.email_text_fk
        AND     o.organization_pk = p_organization_pk
        AND     o.geography_fk = g.geography_pk
        AND     (
                        etr.geography_fk = g.geography_pk
                OR      etr.geography_fk = g.level0
                OR      etr.geography_fk = g.level1
                OR      etr.geography_fk = g.level2
                OR      etr.geography_fk = g.level3
                OR      etr.geography_fk = g.level4
                OR      etr.geography_fk = g.level5
        )
	AND	(start_date > sysdate OR start_date IS NULL)
	AND	(end_date < sysdate OR end_date IS NULL)
	AND	(volume IS NULL OR volume > (
			SELECT	sum(volume)
			FROM	mail_log
			WHERE	email_text_fk = (
			        SELECT  email_text_pk
			        FROM    email_text et, et_relation etr, organization o, geography g
			        WHERE   et.email_text_pk = etr.email_text_fk
			        AND     o.organization_pk = p_organization_pk
			        AND     o.geography_fk = g.geography_pk
			        AND     (
			                        etr.geography_fk = g.geography_pk
			                OR      etr.geography_fk = g.level0
			                OR      etr.geography_fk = g.level1
			                OR      etr.geography_fk = g.level2
			                OR      etr.geography_fk = g.level3
			                OR      etr.geography_fk = g.level4
			                OR      etr.geography_fk = g.level5
			        )
				AND	(start_date > sysdate OR start_date IS NULL)
				AND	(end_date < sysdate OR end_date IS NULL)
				)
			)
		);

        v_et_type_pk            et_type.et_type_pk%type         default NULL;
        v_name                  et_type.name%type               default NULL;

        v_check_org             number                          default NULL;
        v_check_serv            number                          default NULL;
        v_check_list            number                          default NULL;
        v_check_geo             number                          default NULL;

        v_email_text_pk         number                          default NULL;
        v_check                 number                          default NULL;

BEGIN
        -- *********************************************************************************
        -- NB: st�tter ikke flere tekster som har overlappende relasjoner/like relasjoner
        -- prosedyren returnerer pk'en til den f�rste eller siste riktige teksten den finner
        -- *********************************************************************************

        -- --------------------------------------------------------------------------
        -- sjekker om org. betaler for abonnementet. Sender ikke ut reklame dersom
        -- prisen er forskjellig fra 0
        -- --------------------------------------------------------------------------
        SELECT  count(*)
        INTO    v_check
        FROM    organization o, membership m
        WHERE   o.membership_fk = m.membership_pk
        AND     o.organization_pk = p_organization_pk
        AND     m.price = 0;

        -- dersom org. betaler s� skal den ikke ha reklame
        if (v_check = 0) then
                RETURN NULL;
        end if;

        -- -------------------------------------------------------------------
        -- forskjellige sjekker som brukes for � finne ut hvilke relasjoner
        -- som finnes fra "email_text" modulen til gitt organisasjon
        -- -------------------------------------------------------------------
        -- relasjon til org
        SELECT  count(*)
        INTO    v_check_org
        FROM    email_text et, et_relation etr
        WHERE   et.email_text_pk = etr.email_text_fk
        AND     organization_fk = p_organization_pk
	AND	(start_date > sysdate OR start_date IS NULL)
	AND	(end_date < sysdate OR end_date IS NULL)
	AND	(volume IS NULL OR volume > (
			SELECT	sum(volume)
			FROM	mail_log
			WHERE	email_text_fk = (
			        SELECT  email_text_pk
			        FROM    email_text et, et_relation etr
			        WHERE   et.email_text_pk = etr.email_text_fk
			        AND     organization_fk = p_organization_pk
				AND	(start_date > sysdate OR start_date IS NULL)
				AND	(end_date < sysdate OR end_date IS NULL)
				)
			)
		);

        -- relasjon til tjeneste
        SELECT  count(*)
        INTO    v_check_serv
        FROM    email_text et, et_relation etr
        WHERE   et.email_text_pk = etr.email_text_fk
        AND     service_fk = get.oserv(p_organization_pk)
	AND	(start_date > sysdate OR start_date IS NULL)
	AND	(end_date < sysdate OR end_date IS NULL)
	AND	(volume IS NULL OR volume > (
			SELECT	sum(volume)
			FROM	mail_log
			WHERE	email_text_fk = (
				SELECT	email_text_pk
				FROM    email_text et, et_relation etr
				WHERE   et.email_text_pk = etr.email_text_fk
				AND     service_fk = get.oserv(p_organization_pk)
				AND	(start_date > sysdate OR start_date IS NULL)
				AND	(end_date < sysdate OR end_date IS NULL)
				)
			)
		);

        -- relasjon til liste
        SELECT  count(*)
        INTO    v_check_list
        FROM    email_text et, et_relation etr, list_org lo, list l
        WHERE   et.email_text_pk = etr.email_text_fk
        AND     etr.list_fk = l.list_pk
        AND     lo.organization_fk = p_organization_pk
        AND     (
                        lo.list_fk = l.list_pk
                OR      lo.list_fk = l.level0
                OR      lo.list_fk = l.level1
                OR      lo.list_fk = l.level2
                OR      lo.list_fk = l.level3
                OR      lo.list_fk = l.level4
                OR      lo.list_fk = l.level5
        )
	AND	(start_date > sysdate OR start_date IS NULL)
	AND	(end_date < sysdate OR end_date IS NULL)
	AND	(volume IS NULL OR volume > (
			SELECT	sum(volume)
			FROM	mail_log
			WHERE	email_text_fk = (
			        SELECT  email_text_pk
			        FROM    email_text et, et_relation etr, list_org lo, list l
			        WHERE   et.email_text_pk = etr.email_text_fk
			        AND     etr.list_fk = l.list_pk
			        AND     lo.organization_fk = p_organization_pk
			        AND     (
			                        lo.list_fk = l.list_pk
			                OR      lo.list_fk = l.level0
			                OR      lo.list_fk = l.level1
			                OR      lo.list_fk = l.level2
			                OR      lo.list_fk = l.level3
			                OR      lo.list_fk = l.level4
			                OR      lo.list_fk = l.level5
			        )
				AND	(start_date > sysdate OR start_date IS NULL)
				AND	(end_date < sysdate OR end_date IS NULL)
				)
			)
		);

        -- relasjon til geo
        SELECT  count(*)
        INTO    v_check_geo
        FROM    email_text et, et_relation etr, organization o, geography g
        WHERE   et.email_text_pk = etr.email_text_fk
        AND     o.organization_pk = p_organization_pk
        AND     o.geography_fk = g.geography_pk
        AND     (
                        etr.geography_fk = g.geography_pk
                OR      etr.geography_fk = g.level0
                OR      etr.geography_fk = g.level1
                OR      etr.geography_fk = g.level2
                OR      etr.geography_fk = g.level3
                OR      etr.geography_fk = g.level4
                OR      etr.geography_fk = g.level5
        )
	AND	(start_date > sysdate OR start_date IS NULL)
	AND	(end_date < sysdate OR end_date IS NULL)
	AND	(volume IS NULL OR volume > (
			SELECT	sum(volume)
			FROM	mail_log
			WHERE	email_text_fk = (
			        SELECT  email_text_pk
			        FROM    email_text et, et_relation etr, organization o, geography g
			        WHERE   et.email_text_pk = etr.email_text_fk
			        AND     o.organization_pk = p_organization_pk
			        AND     o.geography_fk = g.geography_pk
			        AND     (
			                        etr.geography_fk = g.geography_pk
			                OR      etr.geography_fk = g.level0
			                OR      etr.geography_fk = g.level1
			                OR      etr.geography_fk = g.level2
			                OR      etr.geography_fk = g.level3
			                OR      etr.geography_fk = g.level4
			                OR      etr.geography_fk = g.level5
			        )
				AND	(start_date > sysdate OR start_date IS NULL)
				AND	(end_date < sysdate OR end_date IS NULL)
				)
			)
		);


	-- ---------------------------------------------------------
        -- g�r igjennom et_type, sjekker om p_type = et_type.name
	-- ---------------------------------------------------------
        OPEN select_et_type;
        LOOP
                FETCH select_et_type INTO v_et_type_pk, v_name;
                EXIT WHEN select_et_type%NOTFOUND;

                if (v_name = p_type) then
                        -- organisasjon er �verst i hierarkiet (overstyrer andre relasjoner)
                        if (v_check_org <> 0) then
                                OPEN select_org;
                                LOOP
                                        FETCH select_org INTO v_email_text_pk;
                                        EXIT WHEN select_org%NOTFOUND;
                                        RETURN v_email_text_pk;
                                END LOOP;
                                CLOSE select_org;
                        elsif (v_check_serv <> 0 OR v_check_list <> 0 OR v_check_geo <> 0) then
                                -- -----------
                                -- 1 relasjon
                                -- -----------
                                -- dersom det kun finnes en relasjon til "service"
                                if (v_check_serv <> 0 AND v_check_list = 0 AND v_check_geo = 0) then
                                        OPEN select_serv;
                                        LOOP
                                                FETCH select_serv INTO v_email_text_pk;
                                                EXIT WHEN select_serv%NOTFOUND;
                                                RETURN v_email_text_pk;
                                        END LOOP;
                                        CLOSE select_serv;
                                -- dersom det kun finnes en relasjon til "list"
                                elsif (v_check_serv = 0 AND v_check_list <> 0 AND v_check_geo = 0) then
                                        OPEN select_list;
                                        LOOP
                                                FETCH select_list INTO v_email_text_pk;
                                                EXIT WHEN select_list%NOTFOUND;
                                                RETURN v_email_text_pk;
                                        END LOOP;
                                        CLOSE select_list;
                                -- dersom det kun finnes en relasjon til "geography"
                                elsif (v_check_serv = 0 AND v_check_list = 0 AND v_check_geo <> 0) then
                                        OPEN select_geo;
                                        LOOP
                                                FETCH select_geo INTO v_email_text_pk;
                                                EXIT WHEN select_geo%NOTFOUND;
                                                RETURN v_email_text_pk;
                                        END LOOP;
                                        CLOSE select_geo;
                                -- -------------
                                -- 2 relasjoner
                                -- -------------
                                -- relasjon til "service" og "list"
                                elsif (v_check_serv <> 0 AND v_check_list <> 0 AND v_check_geo = 0) then
                                        OPEN select_serv;
                                        LOOP
                                                FETCH select_serv INTO v_email_text_pk;
                                                EXIT WHEN select_serv%NOTFOUND;
                                        END LOOP;
                                        CLOSE select_serv;

                                        -- sjekker i mail_log tabellen om denne teksten er sendt ut forrige gang
                                        -- organisasjonen sendte ut email
                                        SELECT  count(*)
                                        INTO    v_check
                                        FROM    mail_log
                                        WHERE   email_text_fk = v_email_text_pk
                                        AND     mail_log_pk IN
                                                (
                                                        SELECT  MAX(mail_log_pk)
                                                        FROM    mail_log
                                                        WHERE   organization_fk = p_organization_pk
                                                );


                                        if (v_check = 0) then -- sender ut serv
                                                RETURN v_email_text_pk;
                                        else -- serv ble sendt forrige gang, sender ut list n�
                                                OPEN select_list;
                                                LOOP
                                                        FETCH select_list INTO v_email_text_pk;
                                                        EXIT WHEN select_list%NOTFOUND;
                                                        RETURN v_email_text_pk;
                                                END LOOP;
                                                CLOSE select_list;
                                        end if;
                                -- relasjon til "service" og "geo"
                                elsif (v_check_serv <> 0 AND v_check_list = 0 AND v_check_geo <> 0) then
                                        OPEN select_serv;
                                        LOOP
                                                FETCH select_serv INTO v_email_text_pk;
                                                EXIT WHEN select_serv%NOTFOUND;
                                        END LOOP;
                                        CLOSE select_serv;

                                        -- sjekker i mail_log tabellen om denne teksten er sendt ut forrige gang
                                        -- organisasjonen sendte ut email
                                        SELECT  count(*)
                                        INTO    v_check
                                        FROM    mail_log
                                        WHERE   email_text_fk = v_email_text_pk
                                        AND     mail_log_pk IN
                                                (
                                                        SELECT  MAX(mail_log_pk)
                                                        FROM    mail_log
                                                        WHERE   organization_fk = p_organization_pk
                                                );


                                        if (v_check = 0) then -- sender ut serv
                                                RETURN v_email_text_pk;
                                        else -- serv ble sendt forrige gang, sender ut list n�
                                                OPEN select_geo;
                                                LOOP
                                                        FETCH select_geo INTO v_email_text_pk;
                                                        EXIT WHEN select_geo%NOTFOUND;
                                                        RETURN v_email_text_pk;
                                                END LOOP;
                                                CLOSE select_geo;
                                        end if;
                                -- relasjon til "list" og "geo"
                                elsif (v_check_serv = 0 AND v_check_list <> 0 AND v_check_geo <> 0) then
                                        OPEN select_list;
                                        LOOP
                                                FETCH select_list INTO v_email_text_pk;
                                                EXIT WHEN select_list%NOTFOUND;
                                        END LOOP;
                                        CLOSE select_list;

                                        -- sjekker i mail_log tabellen om denne teksten er sendt ut forrige gang
                                        -- organisasjonen sendte ut email
                                        SELECT  count(*)
                                        INTO    v_check
                                        FROM    mail_log
                                        WHERE   email_text_fk = v_email_text_pk
                                        AND     mail_log_pk IN
                                                (
                                                        SELECT  MAX(mail_log_pk)
                                                        FROM    mail_log
                                                        WHERE   organization_fk = p_organization_pk
                                                );


                                        if (v_check = 0) then -- sender ut serv
                                                RETURN v_email_text_pk;
                                        else -- serv ble sendt forrige gang, sender ut list n�
                                                OPEN select_geo;
                                                LOOP
                                                        FETCH select_geo INTO v_email_text_pk;
                                                        EXIT WHEN select_geo%NOTFOUND;
                                                        RETURN v_email_text_pk;
                                                END LOOP;
                                                CLOSE select_geo;
                                        end if;
                                -- --------------
                                -- 3 relasjoner
                                -- --------------
                                elsif (v_check_serv <> 0 AND v_check_list <> 0 AND v_check_geo <> 0) then
                                        -- ---------------------------------------------------------------
                                        -- sjekker om serv ble sendt sist, hvis ikke s� returneres denne
                                        -- ---------------------------------------------------------------
                                        OPEN select_serv;
                                        LOOP
                                                FETCH select_serv INTO v_email_text_pk;
                                                EXIT WHEN select_serv%NOTFOUND;
                                        END LOOP;
                                        CLOSE select_serv;

                                        SELECT  count(*)
                                        INTO    v_check
                                        FROM    mail_log
                                        WHERE   email_text_fk = v_email_text_pk
                                        AND     mail_log_pk IN
                                                (
                                                        SELECT  MAX(mail_log_pk)
                                                        FROM    mail_log
                                                        WHERE   organization_fk = p_organization_pk
                                                );

                                        if (v_check = 0) then -- sender ut serv
                                                RETURN v_email_text_pk;
                                        end if;

                                        -- ---------------------------------------------------------------
                                        -- sjekker om list ble sendt sist, hvis ikke s� returneres denne
                                        -- ---------------------------------------------------------------
                                        OPEN select_list;
                                        LOOP
                                                FETCH select_list INTO v_email_text_pk;
                                                EXIT WHEN select_list%NOTFOUND;
                                        END LOOP;
                                        CLOSE select_list;

                                        SELECT  count(*)
                                        INTO    v_check
                                        FROM    mail_log
                                        WHERE   email_text_fk = v_email_text_pk
                                        AND     mail_log_pk IN
                                                (
                                                        SELECT  MAX(mail_log_pk)
                                                        FROM    mail_log
                                                        WHERE   organization_fk = p_organization_pk
                                                );

                                        if (v_check = 0) then -- sender ut list
                                                RETURN v_email_text_pk;
                                        end if;

                                        -- ---------------------------------------------------------------
                                        -- sjekker om geo ble sendt sist, hvis ikke s� returneres denne
                                        -- ---------------------------------------------------------------
                                        OPEN select_geo;
                                        LOOP
                                                FETCH select_geo INTO v_email_text_pk;
                                                EXIT WHEN select_geo%NOTFOUND;
                                        END LOOP;
                                        CLOSE select_geo;

                                        SELECT  count(*)
                                        INTO    v_check
                                        FROM    mail_log
                                        WHERE   email_text_fk = v_email_text_pk
                                        AND     mail_log_pk IN
                                                (
                                                        SELECT  MAX(mail_log_pk)
                                                        FROM    mail_log
                                                        WHERE   organization_fk = p_organization_pk
                                                );

                                        if (v_check = 0) then -- sender ut geo
                                                RETURN v_email_text_pk;
                                        end if;

                                        -- dersom ingen sjekker ovenfor sl�r til er det noe feil
                                        -- sender ut NULL selv om det strengt tatt ikke burde v�re n�dvendig
                                        RETURN NULL;

                                end if;
                        else
                                RETURN NULL;
                        end if;
                else
                        RETURN NULL;
                end if;


        END LOOP;
        CLOSE select_et_type;


END;
END et;


-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/
