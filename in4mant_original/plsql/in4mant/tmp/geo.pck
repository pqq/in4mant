CREATE OR REPLACE PACKAGE geo IS

	PROCEDURE startup;
	PROCEDURE list_all;
	PROCEDURE html_top;
	PROCEDURE insert_level0
		(
			p_geography_pk		in geography.geography_pk%type		default NULL,
			p_name 			in string_group.name%type		default NULL
                );
	PROCEDURE update_entry
		(
			p_geography_pk 		in geography.geography_pk%type	default NULL
                );
	PROCEDURE delete_entry
		(
			p_geography_pk 		in geography.geography_pk%type	default NULL
                );
	PROCEDURE delete_entry_2
		(
			p_geography_pk 		in geography.geography_pk%type		default NULL,
			p_confirm		in varchar2				default NULL
                );
	PROCEDURE list_level1
		(
			p_level0		in geography.level0%type		default NULL
                );
	PROCEDURE insert_level1
		(
			p_name 			in string_group.name%type		default NULL,
			p_level0		in geography.level0%type		default NULL
                );
	PROCEDURE list_level2
		(
			p_level0		in geography.level0%type		default NULL,
			p_level1		in geography.level1%type		default NULL
                );
	PROCEDURE insert_level2
		(
			p_name 			in string_group.name%type		default NULL,
			p_level0		in geography.level0%type		default NULL,
			p_level1		in geography.level1%type		default NULL
                );
	PROCEDURE list_level3
		(
			p_level0		in geography.level0%type		default NULL,
			p_level1		in geography.level1%type		default NULL,
			p_level2		in geography.level2%type		default NULL
                );
	PROCEDURE insert_level3
		(
			p_name 			in string_group.name%type		default NULL,
			p_level0		in geography.level0%type		default NULL,
			p_level1		in geography.level1%type		default NULL,
			p_level2		in geography.level2%type		default NULL
                );
	PROCEDURE list_level4
		(
			p_level0		in geography.level0%type		default NULL,
			p_level1		in geography.level1%type		default NULL,
			p_level2		in geography.level2%type		default NULL,
			p_level3		in geography.level3%type		default NULL
                );
	PROCEDURE insert_level4
		(
			p_name 			in string_group.name%type		default NULL,
			p_level0		in geography.level0%type		default NULL,
			p_level1		in geography.level1%type		default NULL,
			p_level2		in geography.level2%type		default NULL,
			p_level3		in geography.level3%type		default NULL
                );
	PROCEDURE list_level5
		(
			p_level0		in geography.level0%type		default NULL,
			p_level1		in geography.level1%type		default NULL,
			p_level2		in geography.level2%type		default NULL,
			p_level3		in geography.level3%type		default NULL,
			p_level4		in geography.level4%type		default NULL
                );
	PROCEDURE insert_level5
		(
			p_name 			in string_group.name%type		default NULL,
			p_level0		in geography.level0%type		default NULL,
			p_level1		in geography.level1%type		default NULL,
			p_level2		in geography.level2%type		default NULL,
			p_level3		in geography.level3%type		default NULL,
			p_level4		in geography.level4%type		default NULL
                );
	PROCEDURE add_countrylang
		(
			p_geography_pk		in geography.geography_pk%type 		default NULL,
			p_language_pk 		in la.language_pk%type 			default NULL
                );
	PROCEDURE remove_lang
		(
			p_geography_pk		in geography.geography_pk%type 		default NULL,
			p_language_pk 		in la.language_pk%type 			default NULL
                );

END;
/
CREATE OR REPLACE PACKAGE BODY geo IS

-- **************************************************
-- "PUBLIC METODER" (KALLES DIREKTE FRA WEB)
-- **************************************************

---------------------------------------------------------
-- Name: 	startup
-- Type: 	procedure
-- What: 	start prosedyren, for � kj�re pakken
-- Author:  	Frode Klevstul
-- Start date: 	25.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

if ( login.timeout('geo.startup')>0 AND login.right('geo')>0 ) then
	list_all;
end if;

END startup;

-- **************************************************
-- "PRIVATE METODER" (KALLES IKKE DIREKTE FRA WEB)
-- **************************************************

---------------------------------------------------------
-- Name: 	list_all (list_level0)
-- Type: 	procedure
-- What: 	lister ut alle forekomster
-- Author:  	Frode Klevstul
-- Start date: 	28.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
-- beskrivelse av inn parametere:
--
-- [parameter]	[description]
---------------------------------------------------------
PROCEDURE list_all
IS
BEGIN
DECLARE

	CURSOR 	select_all IS
	SELECT 	g.geography_pk, sg.name, g.level0, g.name_sg_fk
	FROM 	geography g, string_group sg
	WHERE	g.name_sg_fk = sg.string_group_pk
	AND	g.level1 IS NULL
	ORDER BY sg.name;

	v_geography_pk	geography.geography_pk%type 	default NULL;
	v_name		string_group.name%type 		default NULL;
	v_level0	geography.level0%type 		default NULL;
	v_name_sg_fk	geography.name_sg_fk%type	default NULL;

BEGIN

if ( login.timeout('geo.startup')>0 AND login.right('geo')>0 ) then

	html_top;
	htp.p('
		<table border="0" width="100%"><tr><td colspan="6">&nbsp;</td></tr>
		<tr bgcolor="#eeeeee"><td width="5%">pk:</td><td>continent (sg_pk): </td><td>level code:</td><td align="center">&nbsp;</td><td align="center">&nbsp;</td></tr>
	');
	-- g�r igjennom "cursoren"...
	open select_all;
	while (1>0)	-- alltid sant -> g�r igjennom alle forekomstene
	loop
		fetch select_all into v_geography_pk, v_name, v_level0, v_name_sg_fk;
		exit when select_all%NOTFOUND;
		htp.p('<tr><td width="5%"><font color="#aaaaaa">'|| v_geography_pk ||'</font></td><td><b><a href="geo.list_level1?p_level0='||v_level0||'">'|| v_name ||'</a></b>&nbsp;&nbsp;&nbsp;<font color="#000000">('|| v_name_sg_fk ||')</font></td><td><font color="#aaaaaa">'|| v_level0 ||'</font></td><td bgcolor="#000000" align="center"><a href="geo.update_entry?p_geography_pk='||v_geography_pk||'"><font color="#ffffff">UPDATE</font></a></td><td bgcolor="#000000" align="center"><a href="geo.delete_entry?p_geography_pk='|| v_geography_pk ||'"><font color="#ffffff">DELETE</font></a></td></tr>');
	end loop;
	close select_all;

	htp.p('</table>');


	/* kode for � legge til ny forekomst i geography */

	htp.p('
		<form name="form" action="geo.insert_level0" method="post">
		<table border="0">
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr bgcolor="#eeeeee"><td colspan="2" align="center"><b>add new continent</b></td></tr>
		<tr><td>name (in string_group):</td><td><input type="Text" name="p_name" size="20" maxlength="50"></td></tr>
		<tr><td colspan="2" align="right"><input type="Submit" value="insert"></td></tr>
		</table>
		</form>
	');


	html.write('e_page');
end if;


END;
END list_all;

---------------------------------------------------------
-- Name: 	insert_level0
-- Type: 	procedure
-- What: 	inserts the entry into the database
-- Author:  	Frode Klevstul
-- Start date: 	29.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE insert_level0
		(
			p_geography_pk		in geography.geography_pk%type		default NULL,
			p_name 			in string_group.name%type		default NULL
                )
IS
BEGIN
DECLARE

	v_geography_pk 		geography.geography_pk%type		default NULL;
	v_name			string_group.description%type 		default NULL;
	v_string_group_pk	string_group.string_group_pk%type 	default NULL;

	v_check 		number					default 0;
	v_message		varchar2(300)				default NULL;


	v_level0	geography.level0%type		default NULL;
	v_level1	geography.level1%type		default NULL;
	v_level2	geography.level2%type		default NULL;
	v_level3	geography.level3%type		default NULL;
	v_level4	geography.level4%type		default NULL;
	v_level5	geography.level5%type		default NULL;

BEGIN

if ( login.timeout('geo.startup')>0 AND login.right('geo')>0 ) then

	-- sjekker om det allerede finnes en forekomst med dette navnet
	if ( p_geography_pk IS NOT NULL ) then
		SELECT 	count(*) INTO v_check
		FROM 	string_group sg, geography g
		WHERE 	sg.name = trans(p_name)
		AND	g.name_sg_fk = sg.string_group_pk
		AND 	geography_pk <> p_geography_pk;
	else
		SELECT count(*) INTO v_check
		FROM string_group
		WHERE name = trans(p_name);
	end if;

	if (v_check > 0) then
		v_message := get.msg(1, 'there already exists an entry with name = <b>'||trans(p_name)||'</b>');
		htp.p(v_message);
		html.write('e_page'); -- skriver ut slutten p� html siden
	else
	begin

		if (p_geography_pk is NULL AND p_name IS NOT NULL) then
			-- legger inn i databasen

			SELECT string_group_seq.NEXTVAL
			INTO v_string_group_pk
			FROM dual;

			INSERT INTO string_group
			(string_group_pk, name, description)
			VALUES (v_string_group_pk, trans(p_name), '[entry inserted by the GEO package]');
			commit;

			SELECT geography_seq.NEXTVAL
			INTO v_geography_pk
			FROM dual;

			INSERT INTO geography
			(geography_pk, name_sg_fk, level0)
			VALUES (v_geography_pk, v_string_group_pk, v_geography_pk);
			commit;

			list_all;


		elsif (p_name IS NOT NULL) then
		begin
			UPDATE string_group
			SET name = trans(p_name)
			WHERE string_group_pk = (
				SELECT	name_sg_fk
				FROM	geography
				WHERE 	geography_pk = p_geography_pk
			);
			commit;

			-- ordner slik at vi blir v�rende p� riktig sted i hierarkiet
			SELECT 	level0, level1, level2, level3, level4, level5
			INTO	v_level0, v_level1, v_level2, v_level3, v_level4, v_level5
			FROM 	geography
			WHERE 	geography_pk = p_geography_pk;

			if ( v_level1 IS NULL ) then
				list_all;
			elsif ( v_level2 IS NULL ) then
				list_level1(v_level0);
			elsif ( v_level3 IS NULL ) then
				list_level2(v_level0, v_level1);
			elsif ( v_level4 IS NULL ) then
				list_level3(v_level0, v_level1, v_level2);
			elsif ( v_level5 IS NULL ) then
				list_level4(v_level0, v_level1, v_level2, v_level3);
			else
				list_level5(v_level0, v_level1, v_level2, v_level3, v_level4);
			end if;


		end;
		end if;
	end;
	end if;

end if;

END;
END insert_level0;



---------------------------------------------------------
-- Name: 	list_level1
-- Type: 	procedure
-- What: 	lister ut alle forekomster
-- Author:  	Frode Klevstul
-- Start date: 	29.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
-- beskrivelse av inn parametere:
--
-- [parameter]	[description]
---------------------------------------------------------
PROCEDURE list_level1
		(
			p_level0		in geography.level0%type		default NULL
                )
IS
BEGIN
DECLARE

	CURSOR 	select_all IS
	SELECT 	g.geography_pk, sg.name, g.level0, g.level1, g.name_sg_fk
	FROM 	geography g, string_group sg
	WHERE	g.name_sg_fk = sg.string_group_pk
	AND	g.level0 = p_level0
	AND	g.level2 IS NULL
	AND 	g.level1 IS NOT NULL
	ORDER BY sg.name;

	v_geography_pk	geography.geography_pk%type 	default NULL;
	v_name		string_group.name%type 		default NULL;
	v_level0	geography.level0%type 		default NULL;
	v_level1	geography.level1%type 		default NULL;
	v_name_sg_fk	geography.name_sg_fk%type	default NULL;
	v_continent	string_group.name%type 		default NULL;

BEGIN

if ( login.timeout('geo.startup')>0 AND login.right('geo')>0 ) then

	SELECT 	name INTO v_continent
	FROM 	geography, string_group
	WHERE 	name_sg_fk = string_group_pk
	AND	level0 = p_level0
	AND 	level1 IS NULL;

	html_top;
	htp.p('
		<table border="0" width="100%"><tr><td colspan="6" align="center"><b>'|| v_continent ||' ('|| p_level0 ||')</b></td></tr>
		<tr bgcolor="#eeeeee"><td width="5%">pk:</td><td>country (sg_pk): </td><td>level code:</td><td align="center">&nbsp;</td><td align="center">&nbsp;</td></tr>
	');
	-- g�r igjennom "cursoren"...
	open select_all;
	while (1>0)	-- alltid sant -> g�r igjennom alle forekomstene
	loop
		fetch select_all into v_geography_pk, v_name, v_level0, v_level1, v_name_sg_fk;
		exit when select_all%NOTFOUND;
		htp.p('<tr><td width="5%"><font color="#aaaaaa">'|| v_geography_pk ||'</font></td><td><b><a href="geo.list_level2?p_level0='||v_level0||'&p_level1='|| v_level1 ||' ">'|| v_name ||'</a></b>&nbsp;&nbsp;&nbsp;<font color="#aaaaaa">('|| v_name_sg_fk ||')</font></td><td><font color="#aaaaaa">'|| v_level0 ||' - '|| v_level1 ||'</font></td><td bgcolor="#000000" align="center"><a href="geo.update_entry?p_geography_pk='||v_geography_pk||'"><font color="#ffffff">UPDATE</font></a></td><td bgcolor="#000000" align="center"><a href="geo.delete_entry?p_geography_pk='|| v_geography_pk ||'"><font color="#ffffff">DELETE</font></a></td></tr>');
	end loop;
	close select_all;

	htp.p('</table>');


	/* kode for � legge til ny forekomst i geography */

	htp.p('
		<form name="form" action="geo.insert_level1" method="post">
		<input type="hidden" name="p_level0" value="'|| p_level0 ||'">
		<table border="0">
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr bgcolor="#eeeeee"><td colspan="2" align="center"><b>add new country</b></td></tr>
		<tr><td>name (in string_group):</td><td><input type="Text" name="p_name" size="20" maxlength="50"></td></tr>
		<tr><td colspan="2" align="right"><input type="Submit" value="insert"></td></tr>
		</table>
		</form>
	');

	html.write('e_page');
end if;

END;
END list_level1;


---------------------------------------------------------
-- Name: 	insert_level1
-- Type: 	procedure
-- What: 	inserts the entry into the database
-- Author:  	Frode Klevstul
-- Start date: 	29.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE insert_level1
		(
			p_name 			in string_group.name%type		default NULL,
			p_level0		in geography.level0%type		default NULL
                )
IS
BEGIN
DECLARE

	v_geography_pk 		geography.geography_pk%type		default NULL;
	v_name			string_group.description%type 		default NULL;
	v_string_group_pk	string_group.string_group_pk%type 	default NULL;

	v_check 		number					default 0;
	v_message		varchar2(300)				default NULL;

BEGIN

if ( login.timeout('geo.startup')>0 AND login.right('geo')>0 ) then

	-- sjekker om det allerede finnes en forekomst med dette navnet
	SELECT count(*) INTO v_check
	FROM string_group
	WHERE name = trans(p_name);

	if (v_check > 0) then
		v_message := get.msg(1, 'there already exists an entry with name = <b>'||trans(p_name)||'</b>');
		htp.p(v_message);
		html.write('e_page'); -- skriver ut slutten p� html siden
	else
	begin

		if (p_name IS NOT NULL) then
			-- legger inn i databasen

			SELECT string_group_seq.NEXTVAL
			INTO v_string_group_pk
			FROM dual;

			INSERT INTO string_group
			(string_group_pk, name, description)
			VALUES (v_string_group_pk, trans(p_name), '[entry inserted by the GEO package]');
			commit;

			SELECT geography_seq.NEXTVAL
			INTO v_geography_pk
			FROM dual;

			INSERT INTO geography
			(geography_pk, name_sg_fk, level0, level1)
			VALUES (v_geography_pk, v_string_group_pk, p_level0, v_geography_pk);
			commit;
		end if;
	end;
	end if;
	list_level1(p_level0);
end if;

END;
END insert_level1;



---------------------------------------------------------
-- Name: 	list_level2
-- Type: 	procedure
-- What: 	lister ut alle forekomster
-- Author:  	Frode Klevstul
-- Start date: 	29.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
-- beskrivelse av inn parametere:
--
-- [parameter]	[description]
---------------------------------------------------------
PROCEDURE list_level2
		(
			p_level0		in geography.level0%type		default NULL,
			p_level1		in geography.level1%type		default NULL
                )
IS
BEGIN
DECLARE

	CURSOR 	select_all IS
	SELECT 	g.geography_pk, sg.name, g.level0, g.level1, g.level2, g.name_sg_fk
	FROM 	geography g, string_group sg
	WHERE	g.name_sg_fk = sg.string_group_pk
	AND	g.level0 = p_level0
	AND	g.level1 = p_level1
	AND	g.level2 IS NOT NULL
	AND 	g.level3 IS NULL
	ORDER BY sg.name;

	CURSOR 	select_lang IS
	SELECT 	language_name, language_pk
	FROM 	la, country_language
	WHERE 	language_fk = language_pk
	AND 	geography_fk = (
		SELECT 	geography_pk
		FROM 	geography
		WHERE	level0 = p_level0
		AND	level1 = p_level1
		AND	level2 IS NULL
		);

	v_geography_pk	geography.geography_pk%type 	default NULL;
	v_name		string_group.name%type 		default NULL;
	v_level0	geography.level0%type 		default NULL;
	v_level1	geography.level1%type 		default NULL;
	v_level2	geography.level2%type 		default NULL;
	v_name_sg_fk	geography.name_sg_fk%type	default NULL;
	v_continent	string_group.name%type 		default NULL;
	v_country	string_group.name%type 		default NULL;

	v_language_name		la.language_name%type		default NULL;
	v_language_pk		la.language_pk%type		default NULL;

BEGIN

if ( login.timeout('geo.startup')>0 AND login.right('geo')>0 ) then

	SELECT 	name INTO v_continent
	FROM 	geography, string_group
	WHERE 	name_sg_fk = string_group_pk
	AND	level0 = p_level0
	AND 	level1 IS NULL;

	SELECT 	name INTO v_country
	FROM 	geography, string_group
	WHERE 	name_sg_fk = string_group_pk
	AND	level0 = p_level0
	AND	level1 = p_level1
	AND 	level2 IS NULL;



	html_top;
	htp.p('
		<table border="0" width="100%"><tr><td colspan="6" align="center"><b><a href="geo.list_level1?p_level0='||p_level0||'">'|| v_continent ||'</a> - '|| v_country ||' ('|| p_level0 ||' - '|| p_level1 ||')</b></td></tr>
		<tr bgcolor="#eeeeee"><td width="5%">pk:</td><td>county (sg_pk): </td><td>level code:</td><td align="center">&nbsp;</td><td align="center">&nbsp;</td></tr>
	');
	-- g�r igjennom "cursoren"...
	open select_all;
	while (1>0)	-- alltid sant -> g�r igjennom alle forekomstene
	loop
		fetch select_all into v_geography_pk, v_name, v_level0, v_level1, v_level2, v_name_sg_fk;
		exit when select_all%NOTFOUND;
		htp.p('<tr><td width="5%"><font color="#aaaaaa">'|| v_geography_pk ||'</font></td><td><b><a href="geo.list_level3?p_level0='||v_level0||'&p_level1='||v_level1||'&p_level2='||v_level2||'">'|| v_name ||'</a></b>&nbsp;&nbsp;&nbsp;<font color="#aaaaaa">('|| v_name_sg_fk ||')</font></td><td><font color="#aaaaaa">'|| v_level0 ||' - '|| v_level1 ||' - '|| v_level2 ||'</font></td><td bgcolor="#000000" align="center"><a href="geo.update_entry?p_geography_pk='||v_geography_pk||'"><font color="#ffffff">UPDATE</font></a></td><td bgcolor="#000000" align="center"><a href="geo.delete_entry?p_geography_pk='|| v_geography_pk ||'"><font color="#ffffff">DELETE</font></a></td></tr>');
	end loop;
	close select_all;

	htp.p('</table>');


	/* kode for � legge til ny forekomst i geography */

	htp.p('
		<form name="form" action="geo.insert_level2" method="post">
		<input type="hidden" name="p_level0" value="'|| p_level0 ||'">
		<input type="hidden" name="p_level1" value="'|| p_level1 ||'">
		<table border="0">
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr bgcolor="#eeeeee"><td colspan="2" align="center"><b>add new county</b></td></tr>
		<tr><td>name (in string_group):</td><td><input type="Text" name="p_name" size="20" maxlength="50"></td></tr>
		<tr><td colspan="2" align="right"><input type="Submit" value="insert"></td></tr>
		</table>
		</form>
	');



	/* kode for � vise tilknyttede spr�k */


	/* kode for � koble til spr�k */
	SELECT 	geography_pk INTO v_geography_pk
	FROM 	geography
	WHERE	level0 = p_level0
	AND	level1 = p_level1
	AND	level2 IS NULL;


	htp.p('
		<table border="0" width="40%">
		<tr bgcolor="#eeeeee"><td colspan="2">In '||v_country||' they speak:</td></tr>
	');
	-- g�r igjennom "cursoren"...
	open select_lang;
	while (1>0)	-- alltid sant -> g�r igjennom alle forekomstene
	loop
		fetch select_lang into v_language_name, v_language_pk;
		exit when select_lang%NOTFOUND;
		htp.p('<tr><td>'||v_language_name||'</td><td bgcolor="#000000" width="10%"><a href="geo.remove_lang?p_geography_pk='||v_geography_pk||'&p_language_pk='||v_language_pk||'"><font color="#ffffff">REMOVE</font></a></td></tr>');
	end loop;
	close select_lang;

	htp.p('</table>');


	htp.p('
		<form name="form" action="geo.add_countrylang" method="post">
		<input type="hidden" name="p_geography_pk" value="'|| v_geography_pk ||'">
		<table border="0">
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr bgcolor="#eeeeee"><td colspan="2" align="center"><b>add language spoken in '||v_country||'</b></td></tr>
		<tr><td>language:</td><td>
	');

		html.select_lang;

	htp.p('
		</td></tr>
		<tr><td colspan="2" align="right"><input type="Submit" value="add language"></td></tr>
		</table>
		</form>
	');


	html.write('e_page');
end if;

END;
END list_level2;



---------------------------------------------------------
-- Name: 	insert_level2
-- Type: 	procedure
-- What: 	inserts the entry into the database
-- Author:  	Frode Klevstul
-- Start date: 	29.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE insert_level2
		(
			p_name 			in string_group.name%type		default NULL,
			p_level0		in geography.level0%type		default NULL,
			p_level1		in geography.level1%type		default NULL
                )
IS
BEGIN
DECLARE

	v_geography_pk 		geography.geography_pk%type		default NULL;
	v_name			string_group.description%type 		default NULL;
	v_string_group_pk	string_group.string_group_pk%type 	default NULL;

	v_check 		number					default 0;
	v_message		varchar2(300)				default NULL;

BEGIN

if ( login.timeout('geo.startup')>0 AND login.right('geo')>0 ) then

	-- sjekker om det allerede finnes en forekomst med dette navnet
	SELECT count(*) INTO v_check
	FROM string_group
	WHERE name = trans(p_name);

	if (v_check > 0) then
		v_message := get.msg(1, 'there already exists an entry with name = <b>'||trans(p_name)||'</b>');
		htp.p(v_message);
		html.write('e_page'); -- skriver ut slutten p� html siden
	else
	begin

		if (p_name IS NOT NULL) then
			-- legger inn i databasen

			SELECT string_group_seq.NEXTVAL
			INTO v_string_group_pk
			FROM dual;

			INSERT INTO string_group
			(string_group_pk, name, description)
			VALUES (v_string_group_pk, trans(p_name), '[entry inserted by the GEO package]');
			commit;

			SELECT geography_seq.NEXTVAL
			INTO v_geography_pk
			FROM dual;

			INSERT INTO geography
			(geography_pk, name_sg_fk, level0, level1, level2)
			VALUES (v_geography_pk, v_string_group_pk, p_level0, p_level1, v_geography_pk);
			commit;
		end if;
	end;
	end if;
	list_level2(p_level0, p_level1);
end if;

END;
END insert_level2;



---------------------------------------------------------
-- Name: 	list_level3
-- Type: 	procedure
-- What: 	lister ut alle forekomster
-- Author:  	Frode Klevstul
-- Start date: 	29.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
-- beskrivelse av inn parametere:
--
-- [parameter]	[description]
---------------------------------------------------------
PROCEDURE list_level3
		(
			p_level0		in geography.level0%type		default NULL,
			p_level1		in geography.level1%type		default NULL,
			p_level2		in geography.level2%type		default NULL
                )
IS
BEGIN
DECLARE

	CURSOR 	select_all IS
	SELECT 	g.geography_pk, sg.name, g.level0, g.level1, g.level2, g.level3, g.name_sg_fk
	FROM 	geography g, string_group sg
	WHERE	g.name_sg_fk = sg.string_group_pk
	AND	g.level0 = p_level0
	AND	g.level1 = p_level1
	AND	g.level2 = p_level2
	AND	g.level3 IS NOT NULL
	AND 	g.level4 IS NULL
	ORDER BY sg.name;

	v_geography_pk	geography.geography_pk%type 	default NULL;
	v_name		string_group.name%type 		default NULL;
	v_level0	geography.level0%type 		default NULL;
	v_level1	geography.level1%type 		default NULL;
	v_level2	geography.level2%type 		default NULL;
	v_level3	geography.level3%type 		default NULL;
	v_name_sg_fk	geography.name_sg_fk%type	default NULL;
	v_continent	string_group.name%type 		default NULL;
	v_country	string_group.name%type 		default NULL;
	v_county	string_group.name%type 		default NULL;

BEGIN


if ( login.timeout('geo.startup')>0 AND login.right('geo')>0 ) then

	SELECT 	name INTO v_continent
	FROM 	geography, string_group
	WHERE 	name_sg_fk = string_group_pk
	AND	level0 = p_level0
	AND 	level1 IS NULL;

	SELECT 	name INTO v_country
	FROM 	geography, string_group
	WHERE 	name_sg_fk = string_group_pk
	AND	level0 = p_level0
	AND	level1 = p_level1
	AND 	level2 IS NULL;

	SELECT 	name INTO v_county
	FROM 	geography, string_group
	WHERE 	name_sg_fk = string_group_pk
	AND	level0 = p_level0
	AND	level1 = p_level1
	AND	level2 = p_level2
	AND 	level3 IS NULL;


	html_top;
	htp.p('
		<table border="0" width="100%"><tr><td colspan="6" align="center"><b><a href="geo.list_level1?p_level0='||p_level0||'">'|| v_continent ||'</a> - <a href="geo.list_level2?p_level0='||p_level0||'&p_level1='||p_level1||'">'|| v_country ||'</a> - '|| v_county ||' ('|| p_level0 ||' - '|| p_level1 ||' - '|| p_level2 ||')</b></td></tr>
		<tr bgcolor="#eeeeee"><td width="5%">pk:</td><td>city (sg_pk): </td><td>level code:</td><td align="center">&nbsp;</td><td align="center">&nbsp;</td></tr>
	');
	-- g�r igjennom "cursoren"...
	open select_all;
	while (1>0)	-- alltid sant -> g�r igjennom alle forekomstene
	loop
		fetch select_all into v_geography_pk, v_name, v_level0, v_level1, v_level2, v_level3, v_name_sg_fk;
		exit when select_all%NOTFOUND;
		htp.p('<tr><td width="5%"><font color="#aaaaaa">'|| v_geography_pk ||'</font></td><td><b><a href="geo.list_level4?p_level0='||v_level0||'&p_level1='||v_level1||'&p_level2='||v_level2||'&p_level3='||v_level3||'">'|| v_name ||'</a></b>&nbsp;&nbsp;&nbsp;<font color="#aaaaaa">('|| v_name_sg_fk ||')</font></td><td><font color="#aaaaaa">'|| v_level0 ||' - '|| v_level1 ||' - '|| v_level2 ||' - '|| v_level3 ||'</font></td><td bgcolor="#000000" align="center"><a href="geo.update_entry?p_geography_pk='||v_geography_pk||'"><font color="#ffffff">UPDATE</font></a></td><td bgcolor="#000000" align="center"><a href="geo.delete_entry?p_geography_pk='|| v_geography_pk ||'"><font color="#ffffff">DELETE</font></a></td></tr>');
	end loop;
	close select_all;

	htp.p('</table>');


	/* kode for � legge til ny forekomst i geography */

	htp.p('
		<form name="form" action="geo.insert_level3" method="post">
		<input type="hidden" name="p_level0" value="'|| p_level0 ||'">
		<input type="hidden" name="p_level1" value="'|| p_level1 ||'">
		<input type="hidden" name="p_level2" value="'|| p_level2 ||'">
		<table border="0">
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr bgcolor="#eeeeee"><td colspan="2" align="center"><b>add new city</b></td></tr>
		<tr><td>name (in string_group):</td><td><input type="Text" name="p_name" size="20" maxlength="50"></td></tr>
		<tr><td colspan="2" align="right"><input type="Submit" value="insert"></td></tr>
		</table>
		</form>
	');

	html.write('e_page');

end if;

END;
END list_level3;




---------------------------------------------------------
-- Name: 	insert_level3
-- Type: 	procedure
-- What: 	inserts the entry into the database
-- Author:  	Frode Klevstul
-- Start date: 	29.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE insert_level3
		(
			p_name 			in string_group.name%type		default NULL,
			p_level0		in geography.level0%type		default NULL,
			p_level1		in geography.level1%type		default NULL,
			p_level2		in geography.level2%type		default NULL
                )
IS
BEGIN
DECLARE

	v_geography_pk 		geography.geography_pk%type		default NULL;
	v_name			string_group.description%type 		default NULL;
	v_string_group_pk	string_group.string_group_pk%type 	default NULL;

	v_check 		number					default 0;
	v_message		varchar2(300)				default NULL;

BEGIN

if ( login.timeout('geo.startup')>0 AND login.right('geo')>0 ) then

	-- sjekker om det allerede finnes en forekomst med dette navnet
	SELECT count(*) INTO v_check
	FROM string_group
	WHERE name = trans(p_name);

	if (v_check > 0) then
		v_message := get.msg(1, 'there already exists an entry with name = <b>'||trans(p_name)||'</b>');
		htp.p(v_message);
		html.write('e_page'); -- skriver ut slutten p� html siden
	else
	begin

		if (p_name IS NOT NULL) then
			-- legger inn i databasen

			SELECT string_group_seq.NEXTVAL
			INTO v_string_group_pk
			FROM dual;

			INSERT INTO string_group
			(string_group_pk, name, description)
			VALUES (v_string_group_pk, trans(p_name), '[entry inserted by the GEO package]');
			commit;

			SELECT geography_seq.NEXTVAL
			INTO v_geography_pk
			FROM dual;

			INSERT INTO geography
			(geography_pk, name_sg_fk, level0, level1, level2, level3)
			VALUES (v_geography_pk, v_string_group_pk, p_level0, p_level1, p_level2, v_geography_pk);
			commit;
		end if;
	end;
	end if;
	list_level3(p_level0, p_level1, p_level2);

end if;

END;
END insert_level3;



---------------------------------------------------------
-- Name: 	list_level4
-- Type: 	procedure
-- What: 	lister ut alle forekomster
-- Author:  	Frode Klevstul
-- Start date: 	29.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
-- beskrivelse av inn parametere:
--
-- [parameter]	[description]
---------------------------------------------------------
PROCEDURE list_level4
		(
			p_level0		in geography.level0%type		default NULL,
			p_level1		in geography.level1%type		default NULL,
			p_level2		in geography.level2%type		default NULL,
			p_level3		in geography.level3%type		default NULL
                )
IS
BEGIN
DECLARE

	CURSOR 	select_all IS
	SELECT 	g.geography_pk, sg.name, g.level0, g.level1, g.level2, g.level3, g.level4, g.name_sg_fk
	FROM 	geography g, string_group sg
	WHERE	g.name_sg_fk = sg.string_group_pk
	AND	g.level0 = p_level0
	AND	g.level1 = p_level1
	AND	g.level2 = p_level2
	AND	g.level3 = p_level3
	AND	g.level4 IS NOT NULL
	AND 	g.level5 IS NULL
	ORDER BY sg.name;

	v_geography_pk	geography.geography_pk%type 	default NULL;
	v_name		string_group.name%type 		default NULL;
	v_level0	geography.level0%type 		default NULL;
	v_level1	geography.level1%type 		default NULL;
	v_level2	geography.level2%type 		default NULL;
	v_level3	geography.level3%type 		default NULL;
	v_level4	geography.level4%type 		default NULL;
	v_name_sg_fk	geography.name_sg_fk%type	default NULL;
	v_continent	string_group.name%type 		default NULL;
	v_country	string_group.name%type 		default NULL;
	v_county	string_group.name%type 		default NULL;
	v_city		string_group.name%type 		default NULL;

BEGIN

if ( login.timeout('geo.startup')>0 AND login.right('geo')>0 ) then

	SELECT 	name INTO v_continent
	FROM 	geography, string_group
	WHERE 	name_sg_fk = string_group_pk
	AND	level0 = p_level0
	AND 	level1 IS NULL;

	SELECT 	name INTO v_country
	FROM 	geography, string_group
	WHERE 	name_sg_fk = string_group_pk
	AND	level0 = p_level0
	AND	level1 = p_level1
	AND 	level2 IS NULL;

	SELECT 	name INTO v_county
	FROM 	geography, string_group
	WHERE 	name_sg_fk = string_group_pk
	AND	level0 = p_level0
	AND	level1 = p_level1
	AND	level2 = p_level2
	AND 	level3 IS NULL;

	SELECT 	name INTO v_city
	FROM 	geography, string_group
	WHERE 	name_sg_fk = string_group_pk
	AND	level0 = p_level0
	AND	level1 = p_level1
	AND	level2 = p_level2
	AND	level3 = p_level3
	AND 	level4 IS NULL;


	html_top;
	htp.p('
		<table border="0" width="100%"><tr><td colspan="6" align="center"><b><a href="geo.list_level1?p_level0='||p_level0||'">'|| v_continent ||'</a> - <a href="geo.list_level2?p_level0='||p_level0||'&p_level1='||p_level1||'">'|| v_country ||'</a> - <a href="geo.list_level3?p_level0='||p_level0||'&p_level1='||p_level1||'&p_level2='||p_level2||'"> '|| v_county ||'</a> - '|| v_city ||' ('|| p_level0 ||' - '|| p_level1 ||' - '|| p_level2 ||' - '|| p_level3 ||')</b></td></tr>
		<tr bgcolor="#eeeeee"><td width="5%">pk:</td><td>district (sg_pk): </td><td>level code:</td><td align="center">&nbsp;</td><td align="center">&nbsp;</td></tr>
	');
	-- g�r igjennom "cursoren"...
	open select_all;
	while (1>0)	-- alltid sant -> g�r igjennom alle forekomstene
	loop
		fetch select_all into v_geography_pk, v_name, v_level0, v_level1, v_level2, v_level3, v_level4, v_name_sg_fk;
		exit when select_all%NOTFOUND;
		htp.p('<tr><td width="5%"><font color="#aaaaaa">'|| v_geography_pk ||'</font></td><td><b><a href="geo.list_level5?p_level0='||v_level0||'&p_level1='||v_level1||'&p_level2='||v_level2||'&p_level3='||v_level3||'&p_level4='||v_level4||'">'|| v_name ||'</a></b>&nbsp;&nbsp;&nbsp;<font color="#aaaaaa">('|| v_name_sg_fk ||')</font></td><td><font color="#aaaaaa">'|| v_level0 ||' - '|| v_level1 ||' - '|| v_level2 ||' - '|| v_level3 ||' - '|| v_level4 ||'</font></td><td bgcolor="#000000" align="center"><a href="geo.update_entry?p_geography_pk='||v_geography_pk||'"><font color="#ffffff">UPDATE</font></a></td><td bgcolor="#000000" align="center"><a href="geo.delete_entry?p_geography_pk='|| v_geography_pk ||'"><font color="#ffffff">DELETE</font></a></td></tr>');
	end loop;
	close select_all;

	htp.p('</table>');


	/* kode for � legge til ny forekomst i geography */

	htp.p('
		<form name="form" action="geo.insert_level4" method="post">
		<input type="hidden" name="p_level0" value="'|| p_level0 ||'">
		<input type="hidden" name="p_level1" value="'|| p_level1 ||'">
		<input type="hidden" name="p_level2" value="'|| p_level2 ||'">
		<input type="hidden" name="p_level3" value="'|| p_level3 ||'">
		<table border="0">
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr bgcolor="#eeeeee"><td colspan="2" align="center"><b>add new district</b></td></tr>
		<tr><td>name (in string_group):</td><td><input type="Text" name="p_name" size="20" maxlength="50"></td></tr>
		<tr><td colspan="2" align="right"><input type="Submit" value="insert"></td></tr>
		</table>
		</form>
	');

	html.write('e_page');
end if;

END;
END list_level4;




---------------------------------------------------------
-- Name: 	insert_level4
-- Type: 	procedure
-- What: 	inserts the entry into the database
-- Author:  	Frode Klevstul
-- Start date: 	29.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE insert_level4
		(
			p_name 			in string_group.name%type		default NULL,
			p_level0		in geography.level0%type		default NULL,
			p_level1		in geography.level1%type		default NULL,
			p_level2		in geography.level2%type		default NULL,
			p_level3		in geography.level3%type		default NULL
                )
IS
BEGIN
DECLARE

	v_geography_pk 		geography.geography_pk%type		default NULL;
	v_name			string_group.description%type 		default NULL;
	v_string_group_pk	string_group.string_group_pk%type 	default NULL;

	v_check 		number					default 0;
	v_message		varchar2(300)				default NULL;

BEGIN

if ( login.timeout('geo.startup')>0 AND login.right('geo')>0 ) then

	-- sjekker om det allerede finnes en forekomst med dette navnet
	SELECT count(*) INTO v_check
	FROM string_group
	WHERE name = trans(p_name);

	if (v_check > 0) then
		v_message := get.msg(1, 'there already exists an entry with name = <b>'||trans(p_name)||'</b>');
		htp.p(v_message);
		html.write('e_page'); -- skriver ut slutten p� html siden
	else
	begin

		if (p_name IS NOT NULL) then
			-- legger inn i databasen

			SELECT string_group_seq.NEXTVAL
			INTO v_string_group_pk
			FROM dual;

			INSERT INTO string_group
			(string_group_pk, name, description)
			VALUES (v_string_group_pk, trans(p_name), '[entry inserted by the GEO package]');
			commit;

			SELECT geography_seq.NEXTVAL
			INTO v_geography_pk
			FROM dual;

			INSERT INTO geography
			(geography_pk, name_sg_fk, level0, level1, level2, level3, level4)
			VALUES (v_geography_pk, v_string_group_pk, p_level0, p_level1, p_level2, p_level3, v_geography_pk);
			commit;
		end if;
	end;
	end if;
	list_level4(p_level0, p_level1, p_level2, p_level3);

end if;

END;
END insert_level4;




---------------------------------------------------------
-- Name: 	list_level5
-- Type: 	procedure
-- What: 	lister ut alle forekomster
-- Author:  	Frode Klevstul
-- Start date: 	29.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
-- beskrivelse av inn parametere:
--
-- [parameter]	[description]
---------------------------------------------------------
PROCEDURE list_level5
		(
			p_level0		in geography.level0%type		default NULL,
			p_level1		in geography.level1%type		default NULL,
			p_level2		in geography.level2%type		default NULL,
			p_level3		in geography.level3%type		default NULL,
			p_level4		in geography.level4%type		default NULL
                )
IS
BEGIN
DECLARE

	CURSOR 	select_all IS
	SELECT 	g.geography_pk, sg.name, g.level0, g.level1, g.level2, g.level3, g.level4, g.level5, g.name_sg_fk
	FROM 	geography g, string_group sg
	WHERE	g.name_sg_fk = sg.string_group_pk
	AND	g.level0 = p_level0
	AND	g.level1 = p_level1
	AND	g.level2 = p_level2
	AND	g.level3 = p_level3
	AND	g.level4 = p_level4
	AND	g.level5 IS NOT NULL
	ORDER BY sg.name;

	v_geography_pk	geography.geography_pk%type 	default NULL;
	v_name		string_group.name%type 		default NULL;
	v_level0	geography.level0%type 		default NULL;
	v_level1	geography.level1%type 		default NULL;
	v_level2	geography.level2%type 		default NULL;
	v_level3	geography.level3%type 		default NULL;
	v_level4	geography.level4%type 		default NULL;
	v_level5	geography.level5%type 		default NULL;
	v_name_sg_fk	geography.name_sg_fk%type	default NULL;
	v_continent	string_group.name%type 		default NULL;
	v_country	string_group.name%type 		default NULL;
	v_county	string_group.name%type 		default NULL;
	v_city		string_group.name%type 		default NULL;
	v_district	string_group.name%type 		default NULL;

BEGIN

if ( login.timeout('geo.startup')>0 AND login.right('geo')>0 ) then

	SELECT 	name INTO v_continent
	FROM 	geography, string_group
	WHERE 	name_sg_fk = string_group_pk
	AND	level0 = p_level0
	AND 	level1 IS NULL;

	SELECT 	name INTO v_country
	FROM 	geography, string_group
	WHERE 	name_sg_fk = string_group_pk
	AND	level0 = p_level0
	AND	level1 = p_level1
	AND 	level2 IS NULL;

	SELECT 	name INTO v_county
	FROM 	geography, string_group
	WHERE 	name_sg_fk = string_group_pk
	AND	level0 = p_level0
	AND	level1 = p_level1
	AND	level2 = p_level2
	AND 	level3 IS NULL;

	SELECT 	name INTO v_city
	FROM 	geography, string_group
	WHERE 	name_sg_fk = string_group_pk
	AND	level0 = p_level0
	AND	level1 = p_level1
	AND	level2 = p_level2
	AND	level3 = p_level3
	AND 	level4 IS NULL;

	SELECT 	name INTO v_district
	FROM 	geography, string_group
	WHERE 	name_sg_fk = string_group_pk
	AND	level0 = p_level0
	AND	level1 = p_level1
	AND	level2 = p_level2
	AND	level3 = p_level3
	AND	level4 = p_level4
	AND 	level5 IS NULL;

	html_top;
	htp.p('
		<table border="0" width="100%"><tr><td colspan="6" align="center"><b><a href="geo.list_level1?p_level0='||p_level0||'">'|| v_continent ||'</a> - <a href="geo.list_level2?p_level0='||p_level0||'&p_level1='||p_level1||'">'|| v_country ||'</a> - <a href="geo.list_level3?p_level0='||p_level0||'&p_level1='||p_level1||'&p_level2='||p_level2||'"> '|| v_county ||'</a> - <a href="geo.list_level4?p_level0='||p_level0||'&p_level1='||p_level1||'&p_level2='||p_level2||'&p_level3='||p_level3||'">'|| v_city ||'</a> - '|| v_district ||' ('|| p_level0 ||' - '|| p_level1 ||' - '|| p_level2 ||' - '|| p_level3 ||' - '|| p_level4 ||')</b></td></tr>
		<tr bgcolor="#eeeeee"><td width="5%">pk:</td><td>street (sg_pk): </td><td>level code:</td><td align="center">&nbsp;</td><td align="center">&nbsp;</td></tr>
	');
	-- g�r igjennom "cursoren"...
	open select_all;
	while (1>0)	-- alltid sant -> g�r igjennom alle forekomstene
	loop
		fetch select_all into v_geography_pk, v_name, v_level0, v_level1, v_level2, v_level3, v_level4, v_level5, v_name_sg_fk;
		exit when select_all%NOTFOUND;
		htp.p('<tr><td width="5%"><font color="#aaaaaa">'|| v_geography_pk ||'</font></td><td><b>'|| v_name ||'</b>&nbsp;&nbsp;&nbsp;<font color="#aaaaaa">('|| v_name_sg_fk ||')</font></td><td><font color="#aaaaaa">'|| v_level0 ||' - '|| v_level1 ||' - '|| v_level2 ||' - '|| v_level3 ||' - '|| v_level4 ||' - '|| v_level5 ||'</font></td><td bgcolor="#000000" align="center"><a href="geo.update_entry?p_geography_pk='||v_geography_pk||'"><font color="#ffffff">UPDATE</font></a></td><td bgcolor="#000000" align="center"><a href="geo.delete_entry?p_geography_pk='|| v_geography_pk ||'"><font color="#ffffff">DELETE</font></a></td></tr>');
	end loop;
	close select_all;

	htp.p('</table>');


	/* kode for � legge til ny forekomst i geography */

	htp.p('
		<form name="form" action="geo.insert_level5" method="post">
		<input type="hidden" name="p_level0" value="'|| p_level0 ||'">
		<input type="hidden" name="p_level1" value="'|| p_level1 ||'">
		<input type="hidden" name="p_level2" value="'|| p_level2 ||'">
		<input type="hidden" name="p_level3" value="'|| p_level3 ||'">
		<input type="hidden" name="p_level4" value="'|| p_level4 ||'">
		<table border="0">
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr bgcolor="#eeeeee"><td colspan="2" align="center"><b>add new street</b></td></tr>
		<tr><td>name (in string_group):</td><td><input type="Text" name="p_name" size="20" maxlength="50"></td></tr>
		<tr><td colspan="2" align="right"><input type="Submit" value="insert"></td></tr>
		</table>
		</form>
	');

	html.write('e_page');

end if;

END;
END list_level5;



---------------------------------------------------------
-- Name: 	insert_level5
-- Type: 	procedure
-- What: 	inserts the entry into the database
-- Author:  	Frode Klevstul
-- Start date: 	29.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE insert_level5
		(
			p_name 			in string_group.name%type		default NULL,
			p_level0		in geography.level0%type		default NULL,
			p_level1		in geography.level1%type		default NULL,
			p_level2		in geography.level2%type		default NULL,
			p_level3		in geography.level3%type		default NULL,
			p_level4		in geography.level4%type		default NULL
                )
IS
BEGIN
DECLARE

	v_geography_pk 		geography.geography_pk%type		default NULL;
	v_name			string_group.description%type 		default NULL;
	v_string_group_pk	string_group.string_group_pk%type 	default NULL;

	v_check 		number					default 0;
	v_message		varchar2(300)				default NULL;

BEGIN


if ( login.timeout('geo.startup')>0 AND login.right('geo')>0 ) then

	-- sjekker om det allerede finnes en forekomst med dette navnet
	SELECT count(*) INTO v_check
	FROM string_group
	WHERE name = trans(p_name);

	if (v_check > 0) then
		v_message := get.msg(1, 'there already exists an entry with name = <b>'||trans(p_name)||'</b>');
		htp.p(v_message);
		html.write('e_page'); -- skriver ut slutten p� html siden
	else
	begin

		if (p_name IS NOT NULL) then
			-- legger inn i databasen

			SELECT string_group_seq.NEXTVAL
			INTO v_string_group_pk
			FROM dual;

			INSERT INTO string_group
			(string_group_pk, name, description)
			VALUES (v_string_group_pk, trans(p_name), '[entry inserted by the GEO package]');
			commit;

			SELECT geography_seq.NEXTVAL
			INTO v_geography_pk
			FROM dual;

			INSERT INTO geography
			(geography_pk, name_sg_fk, level0, level1, level2, level3, level4, level5)
			VALUES (v_geography_pk, v_string_group_pk, p_level0, p_level1, p_level2, p_level3, p_level4, v_geography_pk);
			commit;
		end if;
	end;
	end if;
	list_level5(p_level0, p_level1, p_level2, p_level3, p_level4);

end if;

END;
END insert_level5;




---------------------------------------------------------
-- Name: 	html_top
-- Type: 	procedure
-- What: 	skriver ut html_koden p� toppen (linker)
-- Author:  	Frode Klevstul
-- Start date: 	28.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
-- beskrivelse av inn parametere:
--
-- [parameter]	[description]
-- p_link	gir oss nr p� linken vi er p� n�
--		1 = insert_html
--		2 = edit_html
---------------------------------------------------------
PROCEDURE html_top
IS
BEGIN

	html.b_adm_page; 	-- skriver ut starten p� html siden
	htp.p('<table width="100%"><tr><td align="center"><b>geography</b></td></tr></table>');

	htp.p('
		<table border="0" width="100%">
		<tr bgcolor="#cccccc"><td bgcolor="#aaaaaa" align="center" width="100%">&nbsp;</td></tr>
		</table>
	');


END html_top;


---------------------------------------------------------
-- Name: 	update_entry
-- Type: 	procedure
-- What: 	updates an entry
-- Author:  	Frode Klevstul
-- Start date: 	29.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE update_entry
		(
			p_geography_pk 		in geography.geography_pk%type	default NULL
                )
IS
BEGIN
DECLARE

	v_name			string_group.name%type		default NULL;

BEGIN

if ( login.timeout('geo.startup')>0 AND login.right('geo')>0 ) then

	SELECT 	name INTO v_name
	FROM 	geography, string_group
	WHERE 	geography_pk = p_geography_pk
	AND	name_sg_fk = string_group_pk;

	html_top;
	htp.p('
		<form name="form" action="geo.insert_level0" method="post">
		<input type="hidden" name="p_geography_pk" value="'|| p_geography_pk ||'">
		<table border="0">
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr bgcolor="#eeeeee"><td colspan="2" align="center"><b>update entry in "geography" table</b></td></tr>
		<tr><td>name (in string_group):</td><td><input type="Text" name="p_name" size="20" maxlength="50" value="'|| v_name ||'"></td></tr>
		<tr><td colspan="2" align="right"><input type="Submit" value="update"></td></tr>
		</table>
		</form>
	');
	html.write('e_page');

end if;

END;
END update_entry;



---------------------------------------------------------
-- Name: 	delete_entry
-- Type: 	procedure
-- What:
-- Author:  	Frode Klevstul
-- Start date: 	27.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE delete_entry
		(
			p_geography_pk 		in geography.geography_pk%type	default NULL
                )
IS
BEGIN
DECLARE

	v_name		string_group.name%type	default NULL;

BEGIN

if ( login.timeout('geo.startup')>0 AND login.right('geo')>0 ) then

	SELECT name INTO v_name
	FROM string_group
	WHERE string_group_pk = (
		SELECT name_sg_fk
		FROM geography
		WHERE geography_pk = p_geography_pk
		);

	html_top;
	htp.p('
		<form action="geo.delete_entry_2" method="post">
		<input type="hidden" name="p_geography_pk" value="'|| p_geography_pk ||'">
		<table>
		<tr><td colspan="2">Are you sure you want to delete the entry <b>'|| v_name ||'</b> with pk <b>'|| p_geography_pk ||'</b>?</td></tr>
		<tr><td><input type="submit" name="p_confirm" value="yes"></td><td align="right"><input type="submit" name="p_confirm" value="no"></td></tr>
		</table>
		</form>
	');
	html.write('e_page');

end if;

END;
END delete_entry;


---------------------------------------------------------
-- Name: 	delete_entry_2
-- Type: 	procedure
-- What:
-- Author:  	Frode Klevstul
-- Start date: 	29.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE delete_entry_2
		(
			p_geography_pk 		in geography.geography_pk%type		default NULL,
			p_confirm		in varchar2				default NULL
                )
IS
BEGIN
DECLARE

	v_level0	geography.level0%type		default NULL;
	v_level1	geography.level1%type		default NULL;
	v_level2	geography.level2%type		default NULL;
	v_level3	geography.level3%type		default NULL;
	v_level4	geography.level4%type		default NULL;
	v_level5	geography.level5%type		default NULL;

BEGIN

if ( login.timeout('geo.startup')>0 AND login.right('geo')>0 ) then

	if (p_confirm = 'yes') then


		SELECT 	level0, level1, level2, level3, level4, level5
		INTO	v_level0, v_level1, v_level2, v_level3, v_level4, v_level5
		FROM 	geography
		WHERE 	geography_pk = p_geography_pk;

		if ( v_level1 IS NULL ) then
			DELETE FROM geography
			WHERE level0 = v_level0;
			commit;
			list_all;
		elsif ( v_level2 IS NULL ) then
			DELETE FROM geography
			WHERE level1 = v_level1;
			commit;
			list_level1(v_level0);
		elsif ( v_level3 IS NULL ) then
			DELETE FROM geography
			WHERE level2 = v_level2;
			commit;
			list_level2(v_level0, v_level1);
		elsif ( v_level4 IS NULL ) then
			DELETE FROM geography
			WHERE level3 = v_level3;
			commit;
			list_level3(v_level0, v_level1, v_level2);
		elsif ( v_level5 IS NULL ) then
			DELETE FROM geography
			WHERE level4 = v_level4;
			commit;
			list_level4(v_level0, v_level1, v_level2, v_level3);
		else
			DELETE FROM geography
			WHERE geography_pk = p_geography_pk;
			commit;
			list_level5(v_level0, v_level1, v_level2, v_level3, v_level4);
		end if;

	end if;

end if;

END;
END delete_entry_2;


---------------------------------------------------------
-- Name: 	add_countrylang
-- Type: 	procedure
-- What: 	inserts the string
-- Author:  	Frode Klevstul
-- Start date: 	30.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE add_countrylang
		(
			p_geography_pk		in geography.geography_pk%type 		default NULL,
			p_language_pk 		in la.language_pk%type	 		default NULL
                )
IS
BEGIN
DECLARE

	v_check		number				default 0;
	v_level0	geography.level0%type		default NULL;
	v_level1	geography.level1%type		default NULL;

BEGIN

if ( login.timeout('geo.startup')>0 AND login.right('geo')>0 ) then

	SELECT 	count(*) INTO v_check
	FROM 	country_language
	WHERE	geography_fk = p_geography_pk
	AND	language_fk = p_language_pk;

	if ( v_check = 0 ) then
		INSERT INTO country_language
		(geography_fk, language_fk)
		VALUES(p_geography_pk, p_language_pk);
		commit;
	end if;


	SELECT 	level0, level1
	INTO	v_level0, v_level1
	FROM 	geography
	WHERE 	geography_pk = p_geography_pk;

	list_level2(v_level0, v_level1);

end if;

END;
END add_countrylang;


---------------------------------------------------------
-- Name: 	remove_lang
-- Type: 	procedure
-- What: 	inserts the string
-- Author:  	Frode Klevstul
-- Start date: 	30.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE remove_lang
		(
			p_geography_pk		in geography.geography_pk%type 		default NULL,
			p_language_pk 		in la.language_pk%type	 		default NULL
                )
IS
BEGIN
DECLARE

	v_level0	geography.level0%type		default NULL;
	v_level1	geography.level1%type		default NULL;

BEGIN

if ( login.timeout('geo.startup')>0 AND login.right('geo')>0 ) then

	DELETE FROM country_language
	WHERE	geography_fk = p_geography_pk
	AND	language_fk = p_language_pk;
	commit;

	SELECT 	level0, level1
	INTO	v_level0, v_level1
	FROM 	geography
	WHERE 	geography_pk = p_geography_pk;

	list_level2(v_level0, v_level1);

end if;

END;
END remove_lang;


-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/
