CREATE OR REPLACE PACKAGE link_list IS

        PROCEDURE startup;
	PROCEDURE admin_links
		(
			p_type		in varchar2		default NULL,
			p_web_link_pk	in number		default NULL,
			p_url		in varchar2		default NULL,
			p_title		in varchar2		default NULL,
			p_description	in varchar2		default NULL
		);
	FUNCTION link_text
	        (
	                p_pk                    in number                       default NULL,
	                p_column                in varchar2                     default NULL
	        )
	        RETURN varchar2;
	PROCEDURE show_links
		(
			p_organization_pk	in organization.organization_pk%type	default NULL,
			p_type			in varchar2				default NULL
		);

END;
/
CREATE OR REPLACE PACKAGE BODY link_list IS



---------------------------------------------------------
-- Name:        startup
-- Type:        procedure
-- What:        start procedure
-- Author:      Frode Klevstul
-- Start date:  08.02.2001
-- Desc:
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

	link_list.admin_links;

END startup;




---------------------------------------------------------
-- Name:        admin_links
-- Type:        procedure
-- What:        start procedure
-- Author:      Frode Klevstul
-- Start date:  08.02.2001
-- Desc:
---------------------------------------------------------
PROCEDURE admin_links
	(
		p_type		in varchar2		default NULL,
		p_web_link_pk	in number		default NULL,
		p_url		in varchar2		default NULL,
		p_title		in varchar2		default NULL,
		p_description	in varchar2		default NULL
	)
IS
BEGIN
DECLARE

        CURSOR  select_all
                (
                        v_organization_pk       in organization.organization_pk%type
                ) IS
        SELECT  web_link_pk, url
        FROM    web_link
        WHERE   organization_fk = v_organization_pk
        ORDER BY inserted_date desc;

	v_web_link_pk		web_link.web_link_pk%type		default NULL;
	v_url			web_link.url%type			default NULL;

        v_organization_pk       organization.organization_pk%type       default NULL;
        v_language_pk           la.language_pk%type                     default NULL;

	v_title			web_link_text.title%type		default NULL;
	v_description		web_link_text.description%type		default NULL;

	v_check			number					default NULL;

BEGIN
if ( login.timeout('link_list.admin_links')>0 ) then

        v_organization_pk       := get.oid;
        v_language_pk           := get.lan;

	if ( get.u_type <> -2 ) then
		get.error_page(3, get.txt('not_a_client_user'), NULL, 'main_page.startup');
		return;
	end if;

	-- ----------------------------------------------------
	-- p_type IS NULL: liste ut alle linker
	-- ----------------------------------------------------
	if (p_type IS NULL) then

	        html.b_page(NULL, NULL, NULL, 2);
	        html.b_box( get.txt('administrate_links_for') ||' '||get.oname(v_organization_pk) , '100%', 'link_list.admin_links');
	        html.b_table;

	        htp.p('<tr><td colspan="9">'||get.txt('admin_links_desc')||'</td></tr>');
	        htp.p('<tr><td colspan="9"><hr noshade></td></tr>');
	        htp.p('<tr><td><b>'||get.txt('title')||':</b></td><td><b>'|| get.txt('description') ||':</b></td><td>&nbsp;</td><td>&nbsp;</td></tr>');


	        open select_all(v_organization_pk);
	        loop
	                fetch select_all into v_web_link_pk, v_url;
	                exit when select_all%NOTFOUND;


	                v_title         := link_list.link_text(v_web_link_pk, 'title');
	                v_description   := link_list.link_text(v_web_link_pk, 'description');

	                htp.p('<tr>
	                        <td nowrap valign="top"><a href="'|| v_url ||'" target="new_window">'|| v_title ||'</a>&nbsp;</td>
	                        <td nowrap valign="top">'|| v_description ||'</td>
			');
	                htp.p('</td><td>');
	                html.button_link( get.txt('update_link') ,'link_list.admin_links?p_web_link_pk='||v_web_link_pk||'&p_type=update');
	                htp.p('</td><td>');
	                html.button_link( get.txt('delete_link') ,'link_list.admin_links?p_web_link_pk='||v_web_link_pk||'&p_type=delete');
	                htp.p('</td></tr>');
	                htp.p('<tr><td colspan="9"><hr noshade width="100%"></td></tr>');
	        end loop;
	        close select_all;

	        html.e_table;
	        html.e_box;

		htp.p('<br><br><br>');

		-- ---
		-- kode til insert form
	        -- ---

	        html.b_box( get.txt('add_new_link'), '100%', 'link_list.admin_links(insert)');
	        html.b_form('link_list.admin_links');
	        html.b_table;
	        htp.p('<tr><td colspan="2"><br>');
	        htp.p('</td></tr>');
	        htp.p('
	                <input type="hidden" name="p_type" value="insert">
	                <tr><td>'|| get.txt('title') ||':</td><td><input type="Text" name="p_title" size="50" maxlength="50"></td></tr>
	                <tr><td>'|| get.txt('description') ||':</td><td><input type="Text" name="p_description" size="60" maxlength="200"></td></tr>
	                <tr><td>'|| get.txt('web_url') ||':</td><td><input type="Text" name="p_url" size="70" maxlength="200"></td></tr>
	                <tr><td colspan="2" align="right">'); html.submit_link( get.txt('add_link') ); htp.p('
	                </td></tr>
	        ');

	        html.e_table;
	        html.e_form;
	        html.e_box;

	        html.e_page;

	-- ----------------------------------------------------------------
	-- p_type = 'insert': legger inn ny forekomst i databasen
	-- ----------------------------------------------------------------
	elsif (p_type = 'insert' and p_title IS NOT NULL and p_url IS NOT NULL) then

                SELECT web_link_seq.NEXTVAL
                INTO v_web_link_pk
                FROM dual;

		v_url := trans(p_url);
		-- ^ 		: Matches newline or the beginning of the target
		-- \w?		: Matches word characters (alphanumeric) [0-9, a-z, A-Z or _] 0 or 1 time
		if (NOT owa_pattern.match(v_url, '^http\w?://')) then
			v_url := 'http://' || v_url;
		end if;

		INSERT INTO web_link
		(web_link_pk, organization_fk, inserted_date, url)
		VALUES (v_web_link_pk, v_organization_pk, sysdate, v_url);
		commit;

		INSERT INTO web_link_text
		(web_link_fk, language_fk, title, description)
		VALUES (v_web_link_pk, v_language_pk, trans(p_title), trans(p_description));
		commit;

		html.jump_to('link_list.admin_links');

	-- ----------------------------------------------------------------
	-- p_type = 'update': oppdaterer verdier
	-- ----------------------------------------------------------------
	elsif (p_type = 'update') then

		-- en sjekk for � hindre at man oppdaterer feil linker
		SELECT	count(*)
		INTO	v_check
		FROM	web_link
		WHERE	web_link_pk = p_web_link_pk
		AND	organization_fk = v_organization_pk;

		if (v_check = 1) then
			-- ---
			-- oppdaterer i databasen
			-- ---
			if (p_title IS NOT NULL and p_url IS NOT NULL) then

				v_url := trans(p_url);
				if (NOT owa_pattern.match(v_url, '^http\w?://')) then
					v_url := 'http://' || v_url;
				end if;

				UPDATE	web_link
				SET	url = v_url
				WHERE	web_link_pk = p_web_link_pk;
				commit;

				UPDATE	web_link_text
				SET	title = trans(p_title),
					description = trans(p_description)
				WHERE	web_link_fk = p_web_link_pk
				AND	language_fk = v_language_pk;
				commit;

				html.jump_to('link_list.admin_links');
			-- ---
			-- skriver ut html form
			-- ---
			else

			        html.b_page(NULL, NULL, NULL, 2);
			        html.b_box( get.txt('update_link'), '100%', 'link_list.admin_links(update)');
			        html.b_form('link_list.admin_links');
			        html.b_table;

			        SELECT  web_link_pk, url
			        INTO	v_web_link_pk, v_url
			        FROM    web_link
			        WHERE   web_link_pk = p_web_link_pk;

			        htp.p('
			                <input type="hidden" name="p_type" value="update">
			                <input type="hidden" name="p_web_link_pk" value="'|| p_web_link_pk ||'">
			                <tr><td>'|| get.txt('title') ||':</td><td><input type="Text" name="p_title" size="50" maxlength="50" value="'|| link_list.link_text(v_web_link_pk, 'title') ||'"></td></tr>
			                <tr><td>'|| get.txt('description') ||':</td><td><input type="Text" name="p_description" size="60" maxlength="200" value="'|| link_list.link_text(v_web_link_pk, 'description') ||'"></td></tr>
			                <tr><td>'|| get.txt('web_url') ||':</td><td><input type="Text" name="p_url" size="70" maxlength="200" value="'||v_url||'"></td></tr>
			                <tr><td colspan="2" align="right">'); html.submit_link( get.txt('update_link') ); htp.p('
			                </td></tr>
			        ');

				html.e_table;
				html.e_form;
				html.e_box;
				html.e_page;

			end if;
		else
			html.jump_to('link_list.admin_links');
		end if;

	-- ----------------------------------------------------------------
	-- p_type = 'delete': sletter verdier fra databasen
	-- ----------------------------------------------------------------
	elsif ( owa_pattern.match(p_type,'delete') ) then

		-- en sjekk for � hindre at man sletter feil linker
		SELECT	count(*)
		INTO	v_check
		FROM	web_link
		WHERE	web_link_pk = p_web_link_pk
		AND	organization_fk = v_organization_pk;

		if (v_check = 1) then
			-- ---
			-- sletter i databasen
			-- ---
			if ( owa_pattern.match(p_type,'delete_action') ) then

				DELETE FROM web_link
				WHERE web_link_pk = p_web_link_pk;
				commit;

				html.jump_to('link_list.admin_links');
			-- ---
			-- skriver ut html form (confirm delete)
			-- ---
			else

			        html.b_page(NULL, NULL, NULL, 2);

			        html.b_box( get.txt('delete_link'), '100%', 'link_list.admin_links(delete)');
			        html.b_form('link_list.admin_links');
			        html.b_table;

			        htp.p('
			                <input type="hidden" name="p_type" value="delete_action">
			                <input type="hidden" name="p_web_link_pk" value="'|| p_web_link_pk ||'">
			                <tr><td>'|| get.txt('are_you_sure_to_delete_the_link') ||' '''|| link_list.link_text(p_web_link_pk, 'title') ||'''?</td></tr>
			                <tr><td>'); html.submit_link( get.txt('delete_link') ); htp.p('</td></tr>
			                <tr><td>'); html.back( get.txt('dont_delete_link') ); htp.p('</td></tr>
			        ');

				html.e_table;
				html.e_form;
				html.e_box;
				html.e_page;

			end if;
		end if;


	else
		html.jump_to('link_list.admin_links');
	end if;

end if;
END;
END admin_links;




---------------------------------------------------------
-- Name:        link_text
-- Type:        function
-- What:        returns text from web_link_text
-- Author:      Frode Klevstul
-- Start date:  08.02 2000
---------------------------------------------------------
FUNCTION link_text
        (
                p_pk                    in number                       default NULL,
                p_column                in varchar2                     default NULL
        )
        RETURN varchar2
IS

        v_language_pk           la.language_pk%type             default NULL;
        v_title                 web_link_text.title%type     	default NULL;
        v_description           web_link_text.description%type	default NULL;

BEGIN

        v_language_pk := get.lan;

        if ( p_column = 'title' ) then
                SELECT  title INTO v_title
                FROM    web_link_text
                WHERE   web_link_fk = p_pk
                AND     language_fk = v_language_pk;

                v_title := html.rem_tag(v_title);
                if (v_title IS NULL) then
                        v_title := '-';
                end if;
                RETURN v_title;
        elsif ( p_column = 'description' ) then
                SELECT  description INTO v_description
                FROM    web_link_text
                WHERE   web_link_fk = p_pk
                AND     language_fk = v_language_pk;

                v_description := html.rem_tag(v_description);
                RETURN v_description;
        else
                RETURN NULL;
        end if;

EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
                RETURN '-';

END link_text;



---------------------------------------------------------
-- Name:        show_links
-- Type:        procedure
-- What:        shows the links related to an organization
-- Author:      Frode Klevstul
-- Start date:  11.02.2001
-- Desc:
---------------------------------------------------------
PROCEDURE show_links
	(
		p_organization_pk	in organization.organization_pk%type	default NULL,
		p_type			in varchar2				default NULL
	)
IS
BEGIN
DECLARE

	CURSOR	show_links_last(
		v_organization_pk in organization.organization_pk%type,
		v_language_pk la.language_pk%type
	) IS
	SELECT	url, title
	FROM	web_link, web_link_text
	WHERE	web_link_fk = web_link_pk
	AND	language_fk = v_language_pk
	AND	organization_fk = v_organization_pk
	ORDER BY inserted_date DESC;

	CURSOR	show_links_all(
		v_organization_pk in organization.organization_pk%type,
		v_language_pk la.language_pk%type
	) IS
	SELECT	url, title, description
	FROM	web_link, web_link_text
	WHERE	web_link_fk = web_link_pk
	AND	language_fk = v_language_pk
	AND	organization_fk = v_organization_pk
	ORDER BY title ASC;

	v_language_pk		la.language_pk%type		default NULL;
	v_no			parameter.value%type		default NULL;

	v_url			web_link.url%type		default NULL;
	v_title			web_link_text.title%type	default NULL;
	v_description		web_link_text.description%type	default NULL;

BEGIN

	v_language_pk 	:= get.lan;
	v_no		:= get.value('no_links');

	if (p_type IS NULL) then
		html.b_box_2(get.txt('show_last_links'), '100%', 'link_list.show_links(last)', 'link_list.show_links?p_organization_pk='||p_organization_pk||'&p_type=all');
		html.b_table;
		open show_links_last(p_organization_pk, v_language_pk);
		while (show_links_last%ROWCOUNT < v_no) loop
			fetch show_links_last into v_url, v_title;
			exit when show_links_last%NOTFOUND;

			htp.p('<tr><td><a href="'||v_url||'" target="new_window">'||v_title||'</a></td></tr>');
		end loop;
		close show_links_last;
		html.e_table;
		html.e_box_2;
	elsif (p_type = 'all') then
	        html.b_page(NULL,NULL,NULL,NULL,3);
	        html.org_menu(p_organization_pk);
		html.b_box(get.txt('show_all_links'), '100%', 'link_list.show_links(all)');
		html.b_table;
		htp.p('<tr><td width="20%"><b>'|| get.txt('title') ||'</b></td><td><b>'|| get.txt('description') ||'</b></td></tr>');
		open show_links_all(p_organization_pk, v_language_pk);
		loop
			fetch show_links_all into v_url, v_title, v_description;
			exit when show_links_all%NOTFOUND;

			htp.p('<tr><td><a href="'||v_url||'" target="new_window">'||v_title||'</a></td><td>'||v_description||'</td></tr>');
		end loop;
		close show_links_all;
		html.e_table;
		html.e_box;
		html.e_page;
	end if;


END;
END show_links;




-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/
