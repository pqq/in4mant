CREATE OR REPLACE PACKAGE htmf IS

        FUNCTION getcode
        	(
			p_name  in html_code.name%type default NULL
		) RETURN varchar2;

END;
/
CREATE OR REPLACE PACKAGE BODY htmf IS



---------------------------------------------------------
-- Name:        getcode
-- Type:        function
-- What:        returns html code from html_code table
-- Author:      Frode Klevstul
-- Start date:  23.03.2001
-- Desc:
---------------------------------------------------------


/* ----------------------- !!!!!!!!!!!! -------------------- */
/* NB: Denne funksjonen b�r legges over i get.sql (get.code) */
/* --------------------------------------------------------- */

FUNCTION getcode
	(
		p_name  in html_code.name%type default NULL
        ) RETURN varchar2
IS
BEGIN
DECLARE
	v_code  html_code.code%type default NULL;
BEGIN

        SELECT code INTO v_code
        FROM html_code
        WHERE name = p_name;

        RETURN v_code;

EXCEPTION
        WHEN NO_DATA_FOUND THEN
		v_code := get.msg(1, 'there is no entry in html_code table with name = <b>'||p_name||'</b>');
		RETURN v_code;
END;
END getcode;





-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/
