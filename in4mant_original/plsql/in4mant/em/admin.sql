set define off
PROMPT *** package: admin ***

CREATE OR REPLACE PACKAGE admin IS
PROCEDURE startup;
PROCEDURE menu;
PROCEDURE edit_org_info (       p_command               IN varchar2                             DEFAULT NULL,
                                p_name                  IN organization.name%TYPE               DEFAULT NULL,
                                p_org_number            IN organization.org_number%TYPE         DEFAULT NULL,
                                p_address               IN organization.address%TYPE            DEFAULT NULL,
                                p_url                   IN organization.url%TYPE                DEFAULT NULL,
                                p_email                 IN organization.email%TYPE              DEFAULT NULL,
                                p_phone                 IN organization.phone%TYPE              DEFAULT NULL,
                                p_fax                   IN organization.fax%TYPE                DEFAULT NULL,
                                p_service_pk            IN service.service_pk%TYPE              DEFAULT NULL,
                                p_geography_pk          IN organization.geography_fk%TYPE       DEFAULT NULL,
                                p_geo_name              IN organization.name%TYPE               DEFAULT NULL,
                                p_membership_pk         IN organization.membership_fk%TYPE      DEFAULT NULL,
                                p_number                IN NUMBER                               DEFAULT NULL,
                                p_info                  IN VARCHAR2                             DEFAULT NULL
                                );
PROCEDURE publish_document (    p_heading               IN document.heading%TYPE                DEFAULT NULL,
                                p_ingress               IN LONG                                 DEFAULT NULL,
                                p_main                  IN LONG                                 DEFAULT NULL,
                                p_footer                IN LONG                                 DEFAULT NULL,
                                p_command               IN VARCHAR2                             DEFAULT NULL,
                                p_document_pk           IN document.document_pk%TYPE            DEFAULT NULL,
                                p_document_type_pk      IN document_type.document_type_pk%TYPE  DEFAULT NULL,
                                p_language_pk           IN la.language_pk%TYPE                  DEFAULT NULL,
                                p_publish_date          IN VARCHAR2                             DEFAULT NULL,
                                p_publish_date2         IN VARCHAR2                             DEFAULT NULL,
                                p_picture_name          IN VARCHAR2                             DEFAULT NULL,
                                p_picture_pk            IN picture.picture_pk%TYPE              DEFAULT NULL,
                                p_name                  IN VARCHAR2                             DEFAULT NULL,
                                p_error                 IN VARCHAR2                             DEFAULT NULL
                                );
PROCEDURE select_doc_type(      p_document_type_pk      IN document_type.document_type_pk%TYPE  DEFAULT NULL,
                                p_service_pk            IN service.service_pk%TYPE              DEFAULT NULL );
PROCEDURE select_lang_org (     p_organization_pk       IN organization.organization_pk%TYPE    DEFAULT NULL,
                                p_language_pk           IN la.language_pk%TYPE                  DEFAULT NULL
                                );
PROCEDURE select_info_name(     p_info_name_fk          IN info_name.info_name_pk%TYPE          DEFAULT NULL);
PROCEDURE publish_calendar (    p_command               IN VARCHAR2                             DEFAULT NULL,
                                p_heading               IN VARCHAR2                             DEFAULT NULL,
                                p_message               IN LONG                                 DEFAULT NULL,
                                p_event_date            IN VARCHAR2                             DEFAULT NULL,
                                p_end_date              IN VARCHAR2                             DEFAULT NULL,
                                p_name                  IN VARCHAR2                             DEFAULT NULL,
                                p_language_pk           IN calendar.language_fk%TYPE            DEFAULT NULL,
                                p_calendar_pk           IN calendar.calendar_pk%TYPE            DEFAULT NULL,
                                p_picture_name          IN VARCHAR2                             DEFAULT NULL,
                                p_picture_pk            IN picture.picture_pk%TYPE              DEFAULT NULL,
                                p_error                 IN VARCHAR2                             DEFAULT NULL
                                );
PROCEDURE publish_org_info (    p_command               IN VARCHAR2                             DEFAULT NULL,
                                p_info_name_pk          IN org_info.info_name_fk%TYPE           DEFAULT NULL,
                                p_value                 IN LONG                                 DEFAULT NULL,
                                p_org_info_pk           IN org_info.org_info_pk%TYPE            DEFAULT NULL,
                                p_name                  IN VARCHAR2                             DEFAULT NULL
                                );
PROCEDURE photo          (      p_organization_pk       IN organization.organization_pk%TYPE    DEFAULT NULL,
                                p_album_pk              IN album.album_pk%TYPE                  DEFAULT NULL,
                                p_picture_pk            IN picture.picture_pk%TYPE              DEFAULT NULL
                                );
PROCEDURE related_docs (       p_organization_pk       IN organization.organization_pk%TYPE    DEFAULT NULL,
                               p_language_pk           IN document.language_fk%TYPE            DEFAULT NULL,
                               p_service_pk            IN service.service_pk%TYPE              DEFAULT NULL,
                               p_document_pk           IN document.document_pk%TYPE            DEFAULT NULL,
                               p_new_date              IN VARCHAR2                             DEFAULT to_char(SYSDATE,get.txt('date_long')),
                               p_old_date              IN VARCHAR2                             DEFAULT NULL,
                               p_quit                  IN VARCHAR2                             DEFAULT NULL,
                               p_count                 IN NUMBER                               DEFAULT 1
                                );
PROCEDURE related_docs2 (       p_organization_pk       IN organization.organization_pk%TYPE    DEFAULT NULL,
                               p_language_pk           IN document.language_fk%TYPE            DEFAULT NULL,
                               p_service_pk            IN service.service_pk%TYPE              DEFAULT NULL,
                               p_document_pk           IN document.document_pk%TYPE            DEFAULT NULL,
                               p_new_date              IN VARCHAR2                             DEFAULT to_char(SYSDATE,get.txt('date_long')),
                               p_old_date              IN VARCHAR2                             DEFAULT NULL,
                               p_quit                  IN VARCHAR2                             DEFAULT NULL,
                               p_count                 IN NUMBER                               DEFAULT 1,
                               p_rel_doc_pk            IN owa_util.ident_arr,
                               p_del_doc_pk            IN owa_util.ident_arr
                                );
PROCEDURE tip_user (            p_email_address         IN VARCHAR2                             DEFAULT NULL,
                                p_organization_pk       IN VARCHAR2                             DEFAULT NULL,
                                p_command               IN VARCHAR2                             DEFAULT NULL
                );
PROCEDURE edit_fora (           p_command               IN varchar2                             DEFAULT NULL,
                                p_name                  IN fora_type.name%TYPE                  DEFAULT NULL,
                                p_description           IN fora_type.description%TYPE           DEFAULT NULL,
                                p_language_pk           IN fora_type.language_fk%TYPE           DEFAULT NULL,
                                p_fora_type_pk          IN fora_type.fora_type_pk%TYPE          DEFAULT NULL,
                                p_activated             IN fora_type.activated%TYPE             DEFAULT NULL
                         );
PROCEDURE select_membership (     p_membership_pk      IN membership.membership_pk%TYPE  DEFAULT NULL);
PROCEDURE select_membership2 (     p_membership_pk      IN membership.membership_pk%TYPE  DEFAULT NULL);

END;
/
show errors;




CREATE OR REPLACE PACKAGE BODY admin IS

---------------------------------------------------------------------
---------------------------------------------------------------------
-- Name: startup
-- Type: procedure
-- What: Genererer opp hovedsiden for admin av en organisasjon
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE startup
IS
BEGIN
        menu;
END startup;

---------------------------------------------------------------------
-- Name: menu
-- Type: procedure
-- What: Genererer opp hovedsiden for admin av en organisasjon
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE menu
IS
BEGIN
DECLARE

v_organization_pk       organization.organization_pk%TYPE       DEFAULT NULL;
v_name                  organization.name%TYPE                  DEFAULT NULL;
v_logo                  organization.logo%TYPE                  DEFAULT NULL;
v_accepted              organization.accepted%TYPE              DEFAULT NULL;

BEGIN

IF (login.timeout('admin.startup') > 0 ) THEN

        v_organization_pk := get.oid;

        IF ( v_organization_pk IS NULL ) THEN
                html.jump_to('http://bytur.no/cgi-bin/main_page.startup',2,'Not a client user!');
        ELSE

        select name, logo, accepted
        into
        v_name, v_logo, v_accepted
        from organization
        where organization_pk = v_organization_pk;

        IF ( v_logo IS NULL ) THEN
                v_logo := 'no_logo.gif';
        END IF;
        html.b_page(NULL, NULL, NULL, 2, 2);
        html.b_table;
        htp.p('
<tr>
<td align="right" colspan="2"><img src="'||get.value('org_logo_dir')||'/'||
v_logo ||'"></td></tr>
</tr>
<tr>
<td align="center" colspan="2"><font size="4" face="Times, Arial, Helvetica"
color="red"><b>'||get.txt('mem_area')||' - '|| v_name ||'</b></font></td>
</tr>
');

IF  ( v_accepted IS NULL OR v_accepted = 0 ) THEN
        htp.p('
        <tr><td colspan="2"><HR size="2" noshade="noshade"></td></tr>
        <tr><td colspan="2"><font color="red">'|| get.txt('org_not_accepted') ||'</font></td></tr>
        ');
END IF;

htp.p('
<tr><td colspan="2"><HR size="2" noshade="noshade"><br></td></tr>
<tr>
<td colspan="2">
'||REPLACE(get.txt('welcome_admin'), '[org_name]', v_name)||'
</td>
</tr>');
        htp.p('<tr><td colspan="2">&nbsp;</td></tr>');
        html.e_table;
        htp.p('</td></tr><tr><td colspan="2">');
        html.b_box(get.txt('choose_menu'),'100%','admin.startup');
        html.b_table;

htp.p('<tr><td colspan="3"><hr noshade></td></tr>');
htp.p('<tr><td colspan="3"><b>'|| get.txt('org_menu_info') ||'</b></td></tr>');
htp.p('<tr><td colspan="3"><hr noshade></td></tr>');
htp.p('
        <tr>
        <td height="100" valign="top">1.</td>
        <td valign="top"><a href="admin.edit_org_info">'||get.txt('edit_org_info')||'</a></td>
        <td valign="top">'||get.txt('edit_org_info_desc') ||'</td>
        </tr>

        <tr>
        <td height="100" valign="top">2.</td>
        <td valign="top"><a href="admin.publish_org_info">'||get.txt('edit_org_rel_info')||'</a></td>
        <td valign="top">'||get.txt('edit_org_rel_info_desc') ||'</td>
        </tr>

        <tr>
        <td height="100" valign="top">3.</td>
        <td valign="top"><a href="users.show_details">'||get.txt('username_info')||'</a></td>
        <td valign="top">'||get.txt('username_info_desc') ||'</td>
        </tr>

        <tr>
        <td height="100" valign="top">4.</td>
        <td valign="top"><a href="upload.org_logo">'||get.txt('upload_logo')||'</a></td>
        <td valign="top">'||get.txt('upload_logo_desc') ||'</td>
        </tr>

        <tr>
        <td height="100" valign="top">5.</td>
        <td valign="top"><a href="org_list.show_lists">'||get.txt('change_lists')||'</a></td>
        <td valign="top">'||get.txt('change_lists_desc') ||'</td>
        </tr>
');
htp.p('<tr><td colspan="3"><hr noshade></td></tr>');
htp.p('<tr><td colspan="3"><b>'|| get.txt('org_menu_tools') ||'</b></td></tr>');
htp.p('<tr><td colspan="3"><hr noshade></td></tr>');
htp.p('
        <tr>
        <td height="100" valign="top">1.</td>
        <td valign="top"><a href="admin.publish_calendar">'||get.txt('publish_edit_calendar')||'</a></td>
        <td valign="top">'||get.txt('publish_edit_calendar_desc') ||'</td>
        </tr>

        <tr>
        <td height="100" valign="top">2.</td>
        <td valign="top"><a href="photo.startup">'||get.txt('admin_album')||'</a></td>
        <td valign="top">'||get.txt('admin_album_desc') ||'</td>
        </tr>

        <tr>
        <td height="100" valign="top">3.</td>
        <td valign="top"><a href="admin.publish_document">'||get.txt('publish_document')||'</a></td>
        <td valign="top">'||get.txt('publish_document_desc') ||'</td>
        </tr>');

       IF ( get.oserv(v_organization_pk)= 9 OR
         get.oserv(v_organization_pk)= 3 OR
         get.oserv(v_organization_pk)= 7 ) then
            htp.p('
                   <tr>
                   <td height="100" valign="top">4.</td>
                   <td valign="top"><a href="admin.publish_document?p_command=list">'||get.txt('delete_document')||'</a></td>
                   <td valign="top">'||get.txt('delete_document_desc') ||'</td>
                   </tr>
                 ');
       END IF;
       htp.p('
        <tr>
        <td height="100" valign="top">5.</td>
        <td valign="top"><a href="admin.related_docs">'||get.txt('related_documents')||'</a></td>
        <td valign="top">'||get.txt('related_documents_desc') ||'</td>
        </tr>

        <tr>
        <td height="100" valign="top">6.</td>
        <td valign="top"><a href="register.startup">'||get.txt('adm_registrations')||'</a></td>
        <td valign="top">'||get.txt('adm_registrations_desc') ||'</td>
        </tr>

        <tr>
        <td height="100" valign="top">7.</td>
        <td valign="top"><a href="admin.edit_fora">'||get.txt('edit_fora')||'</a></td>
        <td valign="top">'||get.txt('edit_fora_desc') ||'</td>
        </tr>
        </tr>

        <tr>
        <td height="100" valign="top">8.</td>
        <td valign="top"><a href="link_list.admin_links">'||get.txt('admin_link_list')||'</a></td>
        <td valign="top">'||get.txt('admin_link_list_desc') ||'</td>
        </tr>
');

        -- 7 = test.in4mant.com, 3 = uteliv.in4mant.com
        if ( get.oserv(v_organization_pk)= 7 or get.oserv(v_organization_pk)= 3 ) then
            htp.p('
                   <tr>
                   <td height="100" valign="top">9.</td>
                   <td valign="top"><a href="menu_org.startup">'||get.txt('admin_org_menu')||'</a></td>
                   <td valign="top">'||get.txt('admin_org_menu_desc') ||'</td>
                   </tr>
                   </tr>
                 ');
       END IF;

htp.p('<tr><td colspan="3"><hr noshade></td></tr>');
htp.p('<tr><td colspan="3"><b>'|| get.txt('org_menu_other') ||'</b></td></tr>');
htp.p('<tr><td colspan="3"><hr noshade></td></tr>');
htp.p('
        <tr>
        <td height="100" valign="top">1.</td>
        <td valign="top"><a href="admin.tip_user">'||get.txt('tip_org')||'</a></td>
        <td valign="top">'||get.txt('tip_org_desc') ||'</td>
        </tr>

        <tr>
        <td height="100" valign="top">2.</td>
        <td valign="top"><a href="stat.res_page">'||get.txt('statistics')||'</a></td>
        <td valign="top">'||get.txt('statistics_desc') ||'</td>
        </tr>
');

        html.e_table;
        html.e_box;
        html.e_page;
        END IF;
END IF;

EXCEPTION
WHEN NO_DATA_FOUND
THEN
htp.p('No data found! admin.sql -> startup');
END;
END menu;

---------------------------------------------------------------------
-- Name: edit_org_info
-- Type: procedure
-- What: Genererer opp hovedsiden for admin av en organisasjon
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE edit_org_info (       p_command               IN varchar2                             DEFAULT NULL,
                                p_name                  IN organization.name%TYPE               DEFAULT NULL,
                                p_org_number            IN organization.org_number%TYPE         DEFAULT NULL,
                                p_address               IN organization.address%TYPE            DEFAULT NULL,
                                p_url                   IN organization.url%TYPE                DEFAULT NULL,
                                p_email                 IN organization.email%TYPE              DEFAULT NULL,
                                p_phone                 IN organization.phone%TYPE              DEFAULT NULL,
                                p_fax                   IN organization.fax%TYPE                DEFAULT NULL,
                                p_service_pk            IN service.service_pk%TYPE              DEFAULT NULL,
                                p_geography_pk          IN organization.geography_fk%TYPE       DEFAULT NULL,
                                p_geo_name              IN organization.name%TYPE               DEFAULT NULL,
                                p_membership_pk         IN organization.membership_fk%TYPE      DEFAULT NULL,
                                p_number                IN NUMBER                               DEFAULT NULL,
                                p_info                  IN VARCHAR2                             DEFAULT NULL
                         )
IS
BEGIN
DECLARE

v_user_pk               usr.user_pk%TYPE                        DEFAULT NULL;
v_organization_pk       organization.organization_pk%TYPE       DEFAULT NULL;
v_name                  organization.name%TYPE                  DEFAULT NULL;
v_org_number            organization.org_number%TYPE            DEFAULT NULL;
v_address               organization.address%TYPE               DEFAULT NULL;
v_url                   organization.url%TYPE                   DEFAULT NULL;
v_email                 organization.email%TYPE                 DEFAULT NULL;
v_phone                 organization.phone%TYPE                 DEFAULT NULL;
v_fax                   organization.fax%TYPE                   DEFAULT NULL;
v_logo                  organization.logo%TYPE                  DEFAULT NULL;
v_accepted              organization.accepted%TYPE              DEFAULT NULL;
v_name_sg_fk            service.name_sg_fk%TYPE                 DEFAULT NULL;
v_name_sg_fk2           membership.name_sg_fk%TYPE              DEFAULT NULL;
v_count                 NUMBER                                  DEFAULT NULL;
v_count2                NUMBER                                  DEFAULT NULL;
v_geography_pk          organization.geography_fk%TYPE          DEFAULT NULL;
v_file                  utl_file.file_type                      DEFAULT NULL;
v_number                NUMBER                                  DEFAULT NULL;
v_membership_pk         organization.membership_fk%TYPE         DEFAULT NULL;

BEGIN

IF (login.timeout('admin.edit_org_info') > 0 ) THEN

        v_organization_pk := get.oid;

        IF ( v_organization_pk > 0 ) THEN
                SELECT count(*)
                INTO   v_count
                FROM   org_service
                WHERE  organization_fk = v_organization_pk
                ;
        END IF;
        IF ( v_organization_pk IS NULL ) THEN
                SELECT organization_seq.nextval
                INTO v_organization_pk
                FROM dual;
                INSERT INTO organization ( organization_pk )
                VALUES ( v_organization_pk );
                v_user_pk := get.uid;
                COMMIT;
                INSERT INTO client_user ( user_fk, organization_fk )
                VALUES ( v_user_pk, v_organization_pk );
                COMMIT;

                INSERT INTO fora_type
                ( fora_type_pk, name, description, language_fk, organization_fk, activated )
                VALUES
                ( fora_type_seq.NEXTVAL, get.txt('fora_name_1'), get.txt('fora_description_1'), get.lan, v_organization_pk, 1 );
                commit;

                INSERT INTO fora_type
                ( fora_type_pk, name, description, language_fk, organization_fk, activated )
                VALUES
                ( fora_type_seq.NEXTVAL, get.txt('fora_name_2'), get.txt('fora_description_2'), get.lan, v_organization_pk, 1 );
                commit;

                INSERT INTO org_service ( organization_fk, service_fk )
                VALUES ( v_organization_pk, get.serv )
                ;
                COMMIT;

                SELECT  email_script_seq.NEXTVAL
                INTO    v_number
                FROM    DUAL;

        ELSIF ( p_command = 'update' ) THEN
                v_address := SUBSTR(p_address,1,500);

                -- ------------- ***** ------------------
                select  count(*) into v_count2
                from    organization
                where   organization_pk = v_organization_pk
                and     accepted = 1
                and     membership_fk IS NOT NULL;

                if (v_count2 <> 0) then
                        select  membership_fk
                        into    v_membership_pk
                        from    organization
                        where   organization_pk = v_organization_pk;
                else
                        v_membership_pk := p_membership_pk;
                end if;
                -- ------------- ***** ------------------

                UPDATE  organization
                SET     name=p_name,
                        org_number=p_org_number,
                        geography_fk=p_geography_pk,
                        address=v_address,
                        url=p_url,
                        email=p_email,
                        phone=p_phone,
                        fax=p_fax,
                        membership_fk = v_membership_pk
                WHERE  organization_pk = v_organization_pk
                ;

                COMMIT;



                IF ( v_count = 0 ) THEN
                        INSERT INTO org_service ( organization_fk, service_fk )
                        VALUES ( v_organization_pk, p_service_pk )
                        ;
                        COMMIT;
                ELSIF ( p_service_pk IS NOT NULL ) THEN
                        UPDATE org_service
                        SET service_fk = p_service_pk
                        WHERE organization_fk = v_organization_pk
                        ;
                        COMMIT;
                        UPDATE fora_type
                        SET service_fk = p_service_pk
                        WHERE organization_fk = v_organization_pk
                        ;
                END IF;

                IF ( p_number IS NOT NULL ) THEN
                        v_file := utl_file.fopen( get.value('email_dir'), v_number||'_email-list.txt' , 'w');
                        utl_file.put_line(v_file, get.value('email_new_org') );
                        utl_file.fclose(v_file);

                        v_file := utl_file.fopen( get.value('email_dir'), v_number||'_message.txt' , 'w');
                        utl_file.put_line(v_file, get.txt('email_new_org') );
                        utl_file.put_line(v_file,
p_info );
                        utl_file.fclose(v_file);
                END IF;
        END IF;

        IF ( p_service_pk IS NOT NULL ) THEN
                SELECT  name_sg_fk
                INTO    v_name_sg_fk
                FROM    service
                WHERE   service_pk = p_service_pk
                ;
        ELSIF (( p_command IS NOT NULL OR v_user_pk IS NULL ) AND v_organization_pk IS NOT NULL ) THEN
                SELECT  s.name_sg_fk
                INTO    v_name_sg_fk
                FROM    service s, org_service os
                WHERE   os.organization_fk = v_organization_pk
                AND     os.service_fk = s.service_pk
                ;
        END IF;

        SELECT  name, org_number, address, url,
                email, phone, fax, logo, geography_fk,
                accepted
        INTO    v_name, v_org_number, v_address, v_url, v_email,
                v_phone, v_fax, v_logo, v_geography_pk, v_accepted
        FROM    organization
        WHERE   organization_pk = v_organization_pk
        ;

        IF ( p_command = 'update') THEN
                IF ( p_number is not null ) then
                        html.b_page(NULL,20,'admin.startup',2);
                ELSE
                        html.b_page(NULL,NULL,NULL,2);
                END IF;
                html.b_box( get.txt('reg_organization'),'100%' , 'admin.edit_org_info' );
                html.b_table;

                SELECT  m.name_sg_fk
                INTO    v_name_sg_fk2
                FROM    membership m, organization o
                WHERE   o.membership_fk = m.membership_pk
                AND     o.organization_pk = v_organization_pk
                ;

                htp.p('<tr><td colspan="2">'|| get.txt('org_update_info') ||'</td></tr>
                <tr><td colspan="2"><HR size="2" noshade="noshade"></td></tr>
                <tr><td>'|| get.txt('org_name') ||':</td><td>'||v_name||'</td></tr>
                <tr><td>'|| get.txt('org_number') ||':</td><td>'||v_org_number||'</td></tr>
                <tr><td>'|| get.txt('geography_location') ||':</td><td>'||get.locn(v_geography_pk)||'</td></tr>
                <tr><td>'|| get.txt('address') ||':</td><td>'||html.rem_tag(v_address)||'</td></tr>
                <tr><td>'|| get.txt('url') ||':</td><td>'||v_url||'</td></tr>
                <tr><td valign="top">'|| get.txt('org_email_address') ||':</td><td>'||v_email||'</td></tr>
                <tr><td>'|| get.txt('phone') ||':</td><td>'||v_phone||'</td></tr>
                <tr><td>'|| get.txt('fax') ||':</td><td>'||v_fax||'</td></tr>
                <tr><td>'|| get.txt('service') ||':</td><td>'||get.text(v_name_sg_fk)||'</td></tr>
                <tr><td>'|| get.txt('membership') ||':</td><td>'||get.text(v_name_sg_fk2)||'</td></tr>
                <tr><td colspan="2">');
                html.button_link( get.txt('update'), 'admin.edit_org_info' );
                htp.p('</td></tr>
                <tr><td colspan="2">');
                html.button_link( get.txt('accept_link'), 'admin.startup' );
                htp.p('</td></tr>');
        ELSE
                html.b_page(NULL,NULL,NULL,2);
                html.b_box( get.txt('reg_organization'),'100%' , 'admin.edit_org_info' );
                html.b_table;
                html.b_form('admin.edit_org_info');
                htp.p('<tr><td colspan="2">'|| get.txt('org_reg_info') ||'</td></tr>
                <tr><td colspan="2"><HR size="2" noshade="noshade"></td></tr>
                <input type="hidden" name="p_command" value="update">
                <tr><td>'|| get.txt('org_name') ||':</td><td>');
                htp.p( html.text('p_name', '30', '150', v_name));
                htp.p('</td></tr>');
                htp.p('<tr><td>'|| get.txt('org_number') ||':</td><td>');
                htp.p( html.text('p_org_number', '30', '20', v_org_number));
                htp.p('</td></tr>
                <tr><td>'|| get.txt('geography_location') ||':</td><td>
                <input type="hidden" name="p_geography_pk" value="'|| v_geography_pk ||'">
                <input type="text" size="30" name="p_geo_name" value="'|| get.locn(v_geography_pk) ||'&nbsp;" onSelect=javascript:openWin(''select_geo.select_location?p_no_levels=4'',300,200) onClick=javascript:openWin(''select_geo.select_location?p_no_levels=4'',300,200)>
                ');
                IF ( v_count > 0 ) THEN
                        htp.p(html.popup( get.txt('change'), 'select_geo.select_location?p_no_levels=4', '300', '200'));
                ELSE
                        htp.p(html.popup( get.txt('specify_geo_to_select_from'), 'select_geo.select_location?p_no_levels=4', '300', '200'));
                END IF;
                htp.p('</td></tr>
                <tr><td valign="top">'|| get.txt('address') ||':</td><td>
                <textarea name="p_address" rows="5" cols="30" wrap="virtual">'||v_address||'</textarea>
                </td></tr>
                <tr><td>'|| get.txt('url') ||':</td><td>
                '|| html.text('p_url', '30', '150', v_url) ||'
                </td></tr>
                <tr><td>'|| get.txt('org_email_address') ||':</td><td>
                '|| html.text('p_email', '30', '150', v_email) ||'
                </td></tr>
                <tr><td>'|| get.txt('phone') ||':</td><td>
                '|| html.text('p_phone', '30', '20', v_phone) ||'
                </td></tr>
                <tr><td>'|| get.txt('fax') ||':</td><td>
                '|| html.text('p_fax', '30', '20', v_fax) ||'
                </td></tr>');

                IF ( v_accepted > 0 ) THEN
                        htp.p(get.text(v_name_sg_fk));
                        htp.p('</td></tr>
                        <tr><td>'|| get.txt('membership') ||':</td><td>');
                        SELECT  m.name_sg_fk
                        INTO    v_name_sg_fk2
                        FROM    membership m, organization o
                        WHERE   o.membership_fk = m.membership_pk
                        AND     o.organization_pk = v_organization_pk
                        ;
                        htp.p('<tr><td>'|| get.txt('service') ||':</td><td>');
                        htp.p(get.text(v_name_sg_fk2));
                ELSE
--                        html.select_service(get.serv);
                        admin.select_membership;
                END IF;
                htp.p('</td></tr>');
                IF ( v_number IS NOT NULL ) THEN
                        htp.p('<tr><td colspan="2"><font color="red">'|| get.txt('help_new_org') ||'</font></td></tr>
                        <tr><td valign="top">'|| get.txt('short_info_about_org') ||':</td><td>
                        <textarea name="p_info" rows="5" cols="30" wrap="virtual">'||get.txt('short_info')||':
</textarea>
                        <input type="hidden" name="p_number" value="'||v_number||'">
                        </td></tr>');
                END IF;
                htp.p('<tr><td colspan="2">'); html.submit_link( get.txt('register') ); htp.p('</td></tr>');
                html.e_form;
        END IF;
        html.e_table;
        html.e_box;
        html.e_page;
END IF;
EXCEPTION
WHEN NO_DATA_FOUND
THEN
htp.p('No data found! admin.sql -> edit_org_info
<br>
v_organization_pk:'||v_organization_pk);
END;
END; -- prosedyren edit_org_info

---------------------------------------------------------------------
-- Name: publish_document
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE publish_document (    p_heading               IN document.heading%TYPE                DEFAULT NULL,
                                p_ingress               IN LONG                                 DEFAULT NULL,
                                p_main                  IN LONG                                 DEFAULT NULL,
                                p_footer                IN LONG                                 DEFAULT NULL,
                                p_command               IN VARCHAR2                             DEFAULT NULL,
                                p_document_pk           IN document.document_pk%TYPE            DEFAULT NULL,
                                p_document_type_pk      IN document_type.document_type_pk%TYPE  DEFAULT NULL,
                                p_language_pk           IN la.language_pk%TYPE                  DEFAULT NULL,
                                p_publish_date          IN VARCHAR2                             DEFAULT NULL,
                                p_publish_date2         IN VARCHAR2                             DEFAULT NULL,
                                p_picture_name          IN VARCHAR2                             DEFAULT NULL,
                                p_picture_pk            IN picture.picture_pk%TYPE              DEFAULT NULL,
                                p_name                  IN VARCHAR2                             DEFAULT NULL,
                                p_error                 IN VARCHAR2                             DEFAULT NULL
                                )
IS
BEGIN
DECLARE

v_organization_pk       organization.organization_pk%TYPE       DEFAULT NULL;
v_inserted_date         document.inserted_date%TYPE             DEFAULT NULL;
v_publish_date          document.publish_date%TYPE              DEFAULT NULL;
v_document_pk           document.document_pk%TYPE               DEFAULT NULL;
v_heading               document.heading%TYPE                   DEFAULT NULL;
v_ingress               VARCHAR2(4000)                          DEFAULT NULL;
v_main1                 document.main1%TYPE                     DEFAULT NULL;
v_main2                 document.main2%TYPE                     DEFAULT NULL;
v_main3                 document.main3%TYPE                     DEFAULT NULL;
v_main4                 document.main4%TYPE                     DEFAULT NULL;
v_main5                 document.main5%TYPE                     DEFAULT NULL;
v_main6                 document.main6%TYPE                     DEFAULT NULL;
v_main7                 document.main7%TYPE                     DEFAULT NULL;
v_main8                 document.main8%TYPE                     DEFAULT NULL;
v_main9                 document.main9%TYPE                     DEFAULT NULL;
v_main10                document.main10%TYPE                    DEFAULT NULL;
v_footer                VARCHAR2(4000)                          DEFAULT NULL;
v_language_pk           document.language_fk%TYPE               DEFAULT NULL;
v_document_type_pk      document.document_type_fk%TYPE          DEFAULT NULL;
v_length                NUMBER                                  DEFAULT NULL;
v_logo                  organization.logo%TYPE                  DEFAULT NULL;
v_path                  picture.path%TYPE                       DEFAULT NULL;
v_main                  LONG                                    DEFAULT NULL;
v_error                 NUMBER                                  DEFAULT NULL;
v_check                 NUMBER                                  DEFAULT NULL;

CURSOR  get_document IS
SELECT  document_pk, heading, publish_date
FROM    document
WHERE   organization_fk = v_organization_pk
ORDER BY publish_date DESC
;

CURSOR  get_old_doc IS
  SELECT language_fk, document_pk
    FROM  document
    WHERE language_fk = (
                         SELECT language_fk
                         FROM document
                         WHERE document_pk=p_document_pk
                         )
    AND   document_pk != p_document_pk
    AND   organization_fk = v_organization_pk;

BEGIN

   IF (login.timeout('admin.publish_document') > 0 ) THEN
      v_organization_pk := get.oid;

      IF ( v_organization_pk IS NULL ) THEN
         -- Hopper ut hvis brukeren ikke er en organization bruker!
         html.jump_to('http://www.in4mant.com/not_org_user.html');
      ELSE
         IF ( p_command IS NULL ) THEN
            html.b_page(NULL,NULL,NULL,2);
            html.b_box( get.txt('publish_document') ,'100%' , 'admin.publish_document');
            html.b_form('admin.publish_document');
            htp.p('<input type="hidden" name="p_command" value="preview">');
            html.b_table;
            htp.p('<tr><td colspan=2 align=center><b>'|| get.oname(v_organization_pk) ||'</b></td></tr>
                  <tr><td colspan=2>'||get.txt('new_document_info')||'</td></tr>
                    <tr><td colspan=2><HR size=2 noshade=noshade></td></tr>
                    <tr><td>'|| get.txt('select_lang_org') ||':</td><td>');
            select_lang_org(v_organization_pk);
            htp.p('</td></tr>
                  <tr><td>'|| get.txt('select_doc_type') ||':</td><td>');
            admin.select_doc_type( NULL, get.oserv(v_organization_pk));
            htp.p('</td></tr>
                  <tr><td>'|| get.txt('publish_date') ||':</td><td>
                    <input type="text" name="p_publish_date" value="'||to_char(sysdate,get.txt('date_long'))||'"
                  onSelect=javascript:openWin(''date_time.startup?p_variable=p_publish_date'',300,250)
                  onClick=javascript:openWin(''date_time.startup?p_variable=p_publish_date'',300,250)>');
            htp.p( html.popup( get.txt('change_date'),'date_time.startup?p_variable=p_publish_date','300', '250') );
            htp.p('</td></tr>
                    <tr><td>'|| get.txt('add_photo') ||':</td><td>
                    <input type="hidden" name="p_picture_pk" value="">
                    <input type="text" name="p_picture_name" value="'||get.txt('no_photo')||'"
                  onSelect=javascript:openWin(''admin.photo?p_organization_pk='||v_organization_pk||''',300,150)
                  onClick=javascript:openWin(''admin.photo?p_organization_pk='||v_organization_pk||''',300,150)>');
            htp.p( html.popup( get.txt('add_photo'),'admin.photo?p_organization_pk='||v_organization_pk||'','300', '150') );
            htp.p('</td></tr>
                    <tr><td>'|| get.txt('heading') ||':</td><td>'||
                  html.text('p_heading', '70', '150', '')
                  ||'</td></tr>

                    <tr><td valign="top">'|| get.txt('ingress') ||':</td><td>
                    <textarea name="p_ingress" rows="6" cols="70" wrap="virtual"></textarea>
                    </td></tr>

                    <tr><td valign="top">'|| get.txt('main') ||':</td><td>
                    <textarea name="p_main" rows="15" cols="70" wrap="virtual"></textarea>
                    </td></tr>

                    <tr><td valign="top">'|| get.txt('p_footer') ||':</td><td>
                    <textarea name="p_footer" rows="3" cols="70" wrap="virtual"></textarea>
                    </td></tr>
                    <tr><td colspan="2" align="center">'); html.submit_link( get.txt('publish') ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page;
         ELSIF ( p_command = 'preview' ) THEN

            v_main := p_main;
            v_footer := SUBSTR(p_footer,1,3900);
            v_ingress := SUBSTR(p_ingress,1,3900);
            IF ( LENGTH(v_footer) > 500 AND
                 LENGTH(v_ingress) > 2000 ) THEN
               v_main1 := SUBSTR(v_ingress,1,3900);
               v_main2 := SUBSTR(v_main,1,3900);--
               v_main3 := SUBSTR(v_main,3901,3900);--7800);
               v_main4 := SUBSTR(v_main,7801,3900);--11700);
               v_main5 := SUBSTR(v_main,11701,3900);--13600);
               v_main6 := SUBSTR(v_main,15601,3900);--17500);
               v_main7 := SUBSTR(v_main,19501,3900);--21400);
               v_main8 := SUBSTR(v_main,23401,3900);--25300);
               v_main9 := SUBSTR(v_main,27301,3900);--29200);
               v_main10 := SUBSTR(v_footer,1,3900);
               v_main1 := v_main1 ||'

';
               v_main10 := '

'||v_main10;
               v_footer := '';
               v_ingress := '';
               v_error := 1;
            ELSIF ( LENGTH(v_footer) > 500 ) THEN
               v_main1 := SUBSTR(v_main,1,3900);
               v_main2 := SUBSTR(v_main,3901,3900);--7800);
               v_main3 := SUBSTR(v_main,7801,3900);--11700);
               v_main4 := SUBSTR(v_main,11701,3900);--13600);
               v_main5 := SUBSTR(v_main,15601,3900);--17500);
               v_main6 := SUBSTR(v_main,19501,3900);--21400);
               v_main7 := SUBSTR(v_main,23401,3900);--25300);
               v_main8 := SUBSTR(v_main,27301,3900);--29200);
               v_main9 := SUBSTR(v_main,31201,3900);--33100);
               v_main10 := SUBSTR(v_footer,1,3900);
               v_error := 3;
               v_footer := '';
            ELSIF ( LENGTH(v_ingress) > 2000 ) THEN
               v_main1 := SUBSTR(v_ingress,1,3900);
               v_main2 := SUBSTR(v_main,1,3900);
               v_main3 := SUBSTR(v_main,3901,3900);--7800);
               v_main4 := SUBSTR(v_main,7801,3900);--11700);
               v_main5 := SUBSTR(v_main,11701,3900);--13600);
               v_main6 := SUBSTR(v_main,15601,3900);--17500);
               v_main7 := SUBSTR(v_main,19501,3900);--21400);
               v_main8 := SUBSTR(v_main,23401,3900);--25300);
               v_main9 := SUBSTR(v_main,27301,3900);--29200);
               v_main10 := SUBSTR(v_main,31201,3900);--33100);
               v_ingress := '';
               v_error := 2;
            ELSE
               v_main1 := SUBSTR(v_main,1,3900);
               v_main2 := SUBSTR(v_main,3901,3900);--7800);
               v_main3 := SUBSTR(v_main,7801,3900);--11700);
               v_main4 := SUBSTR(v_main,11701,3900);--13600);
               v_main5 := SUBSTR(v_main,15601,3900);--17500);
               v_main6 := SUBSTR(v_main,19501,3900);--21400);
               v_main7 := SUBSTR(v_main,23401,3900);--25300);
               v_main8 := SUBSTR(v_main,27301,3900);--29200);
               v_main9 := SUBSTR(v_main,31201,3900);--33100);
               v_main10 := SUBSTR(v_main,35101,3900);--37000);
            END IF;


            SELECT  document_seq.NEXTVAL
              INTO    v_document_pk
              FROM dual
              ;

            SELECT  logo
              INTO    v_logo
              FROM    organization
              WHERE   organization_pk = v_organization_pk
              ;
            v_heading := SUBSTR(p_heading,1,250);
            v_ingress := SUBSTR(v_ingress,1,2000);
            INSERT INTO document
              ( document_pk, organization_fk, language_fk, document_type_fk,
                inserted_date, publish_date, heading, ingress, main1, main2, main3,
                main4, main5, main6, main7, main8, main9, main10, footer )
              VALUES
              ( v_document_pk, v_organization_pk, p_language_pk, p_document_type_pk,
                SYSDATE, to_date(p_publish_date,get.txt('date_long')),v_heading,
                v_ingress, v_main1, v_main2,
                v_main3, v_main4, v_main5,
                v_main6, v_main7, v_main8, v_main9,
                v_main10, SUBSTR(v_footer,1,500) )
              ;
            COMMIT;

            IF ( p_document_pk IS NOT NULL ) THEN
               DELETE  from document
                 WHERE   document_pk = p_document_pk;
               COMMIT;
            END IF;

            IF ( p_picture_pk IS NOT NULL ) THEN
               SELECT  path
                 INTO    v_path
                 FROM    picture
                 WHERE   picture_pk = p_picture_pk
                 ;
               INSERT INTO picture_in_document
                 (picture_fk, document_fk)
                 VALUES
                 (p_picture_pk, v_document_pk);
               COMMIT;

            END IF;

            IF ( p_heading IS NOT NULL ) THEN
               html.b_page(NULL,NULL,NULL,2);
               html.b_box( get.txt('preview_document') ,'100%' , 'admin.preview_document');
               html.b_table;
               htp.p('<tr><td>'||get.txt('prew_document_info')||'</td></tr>
                     <tr><td colspan="2"><HR size="2" noshade="noshade"></td></tr>
                       <tr><td><img
                     src="'||get.value('org_logo_dir')||'/'||v_logo ||'"
                     alt="logo"></td></tr>
                       <tr><td>&nbsp;</td></tr>
                       <tr><td>'|| p_publish_date ||'</td></tr>
                       <tr><td><h2><b>'||html.rem_tag(SUBSTR(p_heading,1,250)) ||'</b></h2></td></tr>
                       <tr><td>&nbsp;</td></tr>
                       <tr><td><b>'|| html.rem_tag(SUBSTR(v_ingress,1,2000))||'</b></td></tr>
                       <tr><td>&nbsp;</td></tr>');

               IF ( p_picture_pk IS NOT NULL ) THEN
               htp.p('<tr><td><em><img src="'|| get.value('photo_archive') ||'/'|| v_path ||'.jpg"></em></td></tr>');
               END IF;

               htp.p('<tr><td>'|| html.rem_tag(v_main1) ||'
                     '||html.rem_tag(v_main2) ||''|| html.rem_tag(v_main3)||'
                     '|| html.rem_tag(v_main4) ||''||html.rem_tag(v_main5) ||'
                     '|| html.rem_tag(v_main6)||''|| html.rem_tag(v_main7) ||'
                     '||html.rem_tag(v_main8) ||''|| html.rem_tag(v_main9)||'
                     '|| html.rem_tag(v_main10) ||'
                     </td></tr>
                       <tr><td>&nbsp;</td></tr>
                       <tr><td><i>'|| html.rem_tag(SUBSTR(v_footer,1,500)) ||'</i></td></tr>');

               IF ( v_error IS NOT NULL ) THEN
                     htp.p('<tr><td><font color="red">'||
                           get.txt('publish_document_error'||v_error)
                           ||'</font></td></tr>');
               END IF;

               htp.p('<tr><td>');
               html.b_form('admin.publish_document');
               htp.p('<input type="hidden" name="p_command" value="insert">
                     <input type="hidden" name="p_document_pk" value="'||v_document_pk||'">');
               html.submit_link( get.txt('publish') );
               html.e_form;
               htp.p('</td></tr>
                       <tr><td>');
               html.b_form('admin.publish_document','form1');
               htp.p('<input type="hidden" name="p_command" value="update">
                     <input type="hidden" name="p_picture_pk" value="'||p_picture_pk||'">
                       <input type="hidden" name="p_document_pk" value="'||v_document_pk||'">');
               html.submit_link( get.txt('Update document'),'form1' );
               html.e_form;
               htp.p('</td></tr>');
               html.e_table;
               html.e_box;
               html.e_page;
            ELSE
               html.jump_to('admin.publish_document?p_command=update&p_picture_pk='||p_picture_pk||'&p_document_pk='||v_document_pk||'&p_error=no_heading');
            END IF;

         ELSIF ( p_command = 'update' ) THEN
            SELECT language_fk, document_type_fk, heading, ingress,
              main1, main2, main3, main4, main5, main6, main7,
              main8, main9, main10, footer, publish_date
              INTO
              v_language_pk, v_document_type_pk, v_heading, v_ingress,
              v_main1, v_main2, v_main3, v_main4, v_main5 ,v_main6, v_main7,
              v_main8, v_main9, v_main10, v_footer, v_publish_date
              FROM document
              WHERE document_pk = p_document_pk
              ;
            html.b_page(NULL,NULL,NULL,2);
            html.b_box( get.txt('publish_document') ,'100%' , 'admin.publish_document');
            html.b_form('admin.publish_document');
            htp.p('<input type="hidden" name="p_command" value="preview">
              <input type="hidden" name="p_document_pk" value="'||p_document_pk||'">');
            html.b_table;
            htp.p('<tr><td colspan="2" align="center"><b>'|| get.oname(v_organization_pk) ||'</b></td></tr>');
            IF ( p_error IS NOT NULL ) THEN
               htp.p('<tr><td colspan="2"><font color="red"><b>'|| get.txt(p_error) ||'</b></font></td></tr>');
            END IF;
            htp.p('<tr><td colspan="2">'||get.txt('update_document_info')||'</td></tr>
                  <tr><td>'|| get.txt('select_lang_org') ||':</td><td>');

            select_lang_org(v_organization_pk);

            htp.p('</td></tr>

                  <tr><td>'|| get.txt('select_doc_type') ||':</td><td>');
                select_doc_type( NULL, get.oserv(v_organization_pk));
                htp.p('</td></tr>

                      <tr><td>'|| get.txt('publish_date') ||':</td><td>
                        <input type="hidden" name="p_publish_date" value="'||to_char(v_publish_date,get.txt('date_long'))||'">
                        <input type="text" size="16"
                      name="p_publish_date2"
                      value="'||to_char(v_publish_date,get.txt('date_long'))||'"
                  onSelect=javascript:openWin(''date_time.startup'',300,250)
                      onClick=javascript:openWin(''date_time.startup'',300,250)>'||
                      html.popup( get.txt('change_date'),'date_time.startup','300', '250') ||'
                  </td></tr>

                        <tr><td>'|| get.txt('add_photo') ||':</td><td>
                        <input type="hidden" name="p_picture_pk" value="'||p_picture_pk||'">
                  <input type="text" name="p_picture_name" value="'|| get.ap_text(p_picture_pk, 'what') ||'"
                  onSelect=javascript:openWin(''admin.photo?p_organization_pk='||v_organization_pk||''',300,150)
                onClick=javascript:openWin(''admin.photo?p_organization_pk='||v_organization_pk||''',300,150)>'||
                html.popup( get.txt('add_photo'),'admin.photo?p_organization_pk='||v_organization_pk||'','300', '150') ||'
                </td></tr>

                <tr><td>'|| get.txt('heading') ||':</td><td>'||
                html.text('p_heading', '70', '250', v_heading )||'
                </td></tr>
                <tr><td valign="top">'|| get.txt('ingress') ||':</td><td>
                <textarea name="p_ingress" rows="6" cols="70" wrap="virtual">'|| v_ingress ||'</textarea>
                </td></tr>

                <tr><td valign="top">'|| get.txt('main') ||':</td><td>
                <textarea name="p_main" rows="15" cols="70" wrap="virtual">'||v_main1||''||v_main2||''||v_main3||''||v_main4||''||v_main5||''||v_main6||''||v_main7||''||v_main8||''||v_main9||''||v_main10||'</textarea>
                </td></tr>

                <tr><td valign="top">'|| get.txt('p_footer') ||':</td><td>
                <textarea name="p_footer" rows="3" cols="70" wrap="virtual">'||v_footer||'</textarea>
                </td></tr>
                <tr><td colspan="2" align="center">'); html.submit_link( get.txt('publish') ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page;
        ELSIF ( p_command = 'insert' ) THEN
                -- brukeren skal legge inn meldingen etter preview
                UPDATE document
                SET accepted=1
                WHERE document_pk = p_document_pk
                ;
                COMMIT;

                SELECT organization_fk, language_fk
                INTO   v_organization_pk, v_language_pk
                FROM   document
                WHERE  document_pk = p_document_pk
                ;
                v_check := system.inc_pages( v_organization_pk, v_language_pk, NULL, NULL, p_document_pk);

                html.jump_to('admin.related_docs?p_document_pk='||p_document_pk);

         ELSIF ( p_command = 'delete') THEN
                 IF ( p_name = 'yes') THEN
                        DELETE  FROM picture_in_document
                        WHERE   document_fk=p_document_pk
                        ;

                        OPEN get_old_doc;
                        FETCH get_old_doc INTO v_language_pk, v_document_pk;
                        CLOSE get_old_doc;
                        v_check := system.inc_pages( v_organization_pk, v_language_pk, NULL, NULL, v_document_pk );

                         DELETE  FROM document
                         WHERE   document_pk=p_document_pk
                         AND     organization_fk = v_organization_pk;
                         COMMIT;

                         html.jump_to ( 'admin.publish_document?p_command=list' );
                 ELSIF ( p_name IS NULL ) THEN
                         html.jump_to ( 'admin.publish_document?p_command=list' );
                 ELSE
                         html.b_page(NULL,NULL,NULL,2);
                         html.b_box( get.txt('publish_document'), '100%', 'admin.publish_document-delete' );
                         html.b_table;
                         html.b_form('admin.publish_document');
                         htp.p('<input type="hidden" name="p_command" value="delete">
                         <input type="hidden" name="p_name" value="yes">
                         <input type="hidden" name="p_document_pk" value="'||p_document_pk||'">
                         <td>'|| get.txt('delete_info')||' "'||p_name||'" ?'); html.submit_link( get.txt('YES') ); htp.p('</td>');
                         html.e_form;
                         htp.p('<td>'); html.button_link( get.txt('NO'), 'admin.publish_document');htp.p('</td></tr>');
                         html.e_table;
                         html.e_box;
                         html.e_page;
                 END IF;
         ELSIF ( p_command = 'list' ) THEN
                 html.b_page(NULL,NULL,NULL,2);
                 html.b_box( get.txt('delete_document'), '100%', 'admin.delete_document' );
                 html.b_table;
                 htp.p('<tr><td colspan="4">'||get.txt('delete_doc_info')||'</td></tr>
                 <tr><td colspan="4"><HR size="2" noshade="noshade"></td></tr>');
                 OPEN get_document;
                 LOOP
                         fetch get_document INTO v_document_pk, v_heading, v_publish_date;
                         exit when get_document%NOTFOUND;

                         htp.p('<tr><td align="left" nowrap>'||to_char(v_publish_date,get.txt('date_long'))||' :</td><td align="left" width="60%">'||v_heading||'</td>
                         <td>');
                         html.b_form('admin.publish_document','delete'|| v_document_pk);
                         htp.p('<input type="hidden" name="p_command" value="delete">
                         <input type="hidden" name="p_name" value="'||replace(v_heading,'"','')||'">
                         <input type="hidden" name="p_document_pk" value="'||v_document_pk||'">
                         '); html.submit_link( get.txt('delete'), 'delete'||v_document_pk ); htp.p('</td></tr>');
                         html.e_form;
                 END LOOP;
                 close get_document;
                 html.e_table;
                 html.e_box;
                 html.e_page;
        END IF;
END IF;
END IF;
END;
END;

---------------------------------------------------------------------
-- Name: select_lang_org
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE select_lang_org (     p_organization_pk       IN organization.organization_pk%TYPE    DEFAULT NULL,
                                p_language_pk           IN la.language_pk%TYPE                  DEFAULT NULL )
IS
BEGIN
DECLARE
        CURSOR  all_languages IS
        SELECT  l.language_pk, l.language_name
        FROM    la l, service s, org_service os, service_on_lang sol
        WHERE   os.organization_fk = p_organization_pk
        AND     os.service_fk = s.service_pk
        AND     s.service_pk = sol.service_fk
        AND     sol.language_fk = l.language_pk
        ORDER BY l.language_name
        ;

        v_language_pk           la.language_pk%type             default NULL;
        v_language_pk2          la.language_pk%type             default NULL;
        v_language_name         la.language_name%type           default NULL;
        v_value                 VARCHAR2(20)                    DEFAULT NULL;
        v_nr                    INT                             DEFAULT NULL;
BEGIN
        IF ( p_language_pk IS NULL ) THEN
                v_language_pk2 := get.lan;
        END IF;

        SELECT COUNT(*) INTO v_nr
          FROM    la l, service s, org_service os, service_on_lang sol
          WHERE   os.organization_fk = p_organization_pk
          AND     os.service_fk = s.service_pk
          AND     s.service_pk = sol.service_fk
          AND     sol.language_fk = l.language_pk
          ;

        OPEN all_languages;
        IF ( v_nr > 1 ) THEN
           htp.p('<select name="p_language_pk">');
           LOOP
              fetch all_languages into v_language_pk, v_language_name;
              exit when all_languages%NOTFOUND;
              IF ( v_language_pk = p_language_pk ) THEN
                 v_value := ' selected';
              ELSIF ( v_language_pk = v_language_pk2 ) THEN
                 v_value := ' selected';
              ELSE
                 v_value := '';
              END IF;
              htp.p('<option value="'|| v_language_pk ||'"'||v_value||'>'|| v_language_name ||'</option>');
           END LOOP;
           htp.p('</select>');
        ELSE
           fetch all_languages into v_language_pk, v_language_name;
           htp.p('<input TYPE="hidden" name="p_language_pk" value="'|| v_language_pk ||'">'|| v_language_name ||'');
        END IF;
        close all_languages;
END;
END select_lang_org;


---------------------------------------------------------------------
-- Name: select_doc_type
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE select_doc_type (     p_document_type_pk      IN document_type.document_type_pk%TYPE  DEFAULT NULL,
                                p_service_pk            IN service.service_pk%TYPE              DEFAULT NULL )
IS
BEGIN
DECLARE

   CURSOR  all_doc_types IS
     SELECT  document_type_pk, name_sg_fk
       FROM  document_type
       WHERE service_fk = p_service_pk
       ;

     v_document_type_pk      document_type.document_type_pk%TYPE             DEFAULT NULL;
     v_name_sg_fk            document_type.name_sg_fk%TYPE                   DEFAULT NULL;
     v_value                 VARCHAR2(20)                                    DEFAULT NULL;

BEGIN

        htp.p('<select name="p_document_type_pk">');
        OPEN all_doc_types;
        LOOP
                fetch all_doc_types into v_document_type_pk, v_name_sg_fk;
                exit when all_doc_types%NOTFOUND;

                IF ( v_document_type_pk = p_document_type_pk ) THEN
                        v_value := ' selected';
                ELSE
                        v_value := '';
                END IF;

                htp.p('<option value="'|| v_document_type_pk ||'"'||v_value||'>'|| get.text(v_name_sg_fk) ||'</option>');
        END LOOP;
        close all_doc_types;
        htp.p('</select>');
END;
END select_doc_type;

---------------------------------------------------------------------
-- Name: select_membership2
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE select_membership2 (     p_membership_pk      IN membership.membership_pk%TYPE  DEFAULT NULL)
IS
BEGIN
DECLARE

CURSOR  all_services IS
SELECT  service_pk, name_sg_fk
FROM    service
WHERE   service_pk <> 0
AND     activated > 0;

CURSOR  all_mem IS
SELECT  m.membership_pk, m.service_fk, m.name_sg_fk, m.price, c.currency_code
FROM    membership m, currency c
WHERE   m.currency_fk = c.currency_pk
ORDER BY m.service_fk, m.membership_pk
;

v_membership_pk           membership.membership_pk%TYPE             DEFAULT NULL;
v_service_fk              membership.service_fk%TYPE                DEFAULT NULL;
v_service_fk2             membership.service_fk%TYPE                DEFAULT NULL;
v_name_sg_fk              membership.name_sg_fk%TYPE                DEFAULT NULL;
v_price                   membership.price%TYPE                     DEFAULT NULL;
v_currency_code           currency.currency_code%TYPE               DEFAULT NULL;
v_value                   VARCHAR2(10)                              DEFAULT NULL;
v_count                   NUMBER                                    DEFAULT NULL;
v_service_text            VARCHAR2(1000)                            DEFAULT NULL;
v_service_value           VARCHAR2(1000)                            DEFAULT NULL;


BEGIN

htp.p('<SCRIPT language=JavaScript>
var parentserviceIDArray = new Array();
');

OPEN all_mem;
--   htp.p('<select name="p_membership_pk">');
LOOP
      FETCH all_mem INTO v_membership_pk, v_service_fk, v_name_sg_fk, v_price, v_currency_code;
      exit when all_mem%NOTFOUND;

      IF ( v_membership_pk = p_membership_pk ) THEN
         v_value := ' selected';
      ELSE
         v_value := '';
      END IF;
      IF ( v_service_fk2 != v_service_fk OR v_service_fk2 IS NULL ) THEN
            IF ( v_service_text IS NOT NULL ) THEN
                  htp.p(v_service_value||'];'||
                  v_service_text||'];');
            END IF;
            htp.p('
                  var serviceValue_'||v_service_fk||' = new Array();
                  var serviceText_'||v_service_fk||' = new Array();
                  ');
--            v_service_value := 'serviceValue_'||v_service_fk||'=["'||v_service_fk||'|'||v_membership_pk||'"';
            v_service_value := 'serviceValue_'||v_service_fk||'=["'||v_membership_pk||'"';
            v_service_text  := 'serviceText_'||v_service_fk||'=["'|| get.text(v_name_sg_fk) ||' ('||v_price||' '||v_currency_code||')"';
      ELSE
--          v_service_value := v_service_value||',"'||v_service_fk||'|'||v_membership_pk||'"';
            v_service_value := v_service_value||',"'||v_membership_pk||'"';
            v_service_text  := v_service_text||',"'|| get.text(v_name_sg_fk) ||' ('||v_price||' '||v_currency_code||')"';
      END IF;
      v_service_fk2 := v_service_fk;
--      htp.p('<option value="'|| v_membership_pk ||'"'||v_value||'>'|| get.text(v_name_sg_fk) ||'&nbsp;&nbsp;('||v_price||' '||v_currency_code||')</option>');
END LOOP;
--htp.p('</select>');

CLOSE all_mem;
htp.p(v_service_value||'];'||
v_service_text||'];
</SCRIPT>

<SCRIPT language=JavaScript>
function setservice() {
        i = 0;
        var optionName = new Option();
        var addItemValue = new Array();
        var addItemText = new Array();
        strGruppe = document.form.p_service_pk.options[document.form.p_service_pk.selectedIndex].value;
        if (document.form.p_service_pk.options[document.form.p_service_pk.length-1].value < 1) {
                document.form.p_service_pk.options[document.form.p_service_pk.length-1] = null;
        }
        for (i = 0; eval("serviceValue_" + strGruppe + "[i]")!=null; i++) {
                addItemValue[i] = eval("serviceValue_" + strGruppe + "[i]");
                addItemText[i] = eval("serviceText_" + strGruppe + "[i]");
        }
        for (var q = document.form.p_membership_pk.length-1; q >= 0; q--) {
                document.form.p_membership_pk.options[q] = null;
        }
        for (var j = 0, k=0; addItemValue[k]!=null; j++, k++) {
                optionName = new Option(addItemText[k], addItemValue[k], 0, 0);
                document.form.p_membership_pk.options[j]= optionName;
        }
        document.form.p_membership_pk.selectedIndex = 0;
}

</SCRIPT>
<select onchange="setservice();" onclick="setservice();" name="p_service_pk" width="180">');

htp.p('<option value="" selected>'|| get.txt('service') ||'</option>');

open all_services;
loop
        fetch all_services into v_service_fk, v_name_sg_fk;
        exit when all_services%NOTFOUND;
        htp.p('<option value="'|| v_service_fk ||'">'|| get.text(v_name_sg_fk) ||'</option>');
end loop;
close all_services;
htp.p('</select>
</td></tr>
<tr><td>'|| get.txt('membership') ||':</td><td>
<SELECT name="p_membership_pk" width="180">
<OPTION value="" selected>'||get.txt('Choose_service_over')||'                 </OPTION>
</SELECT>
');

END;
END select_membership2;

---------------------------------------------------------------------
-- Name: select_membership
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE select_membership (     p_membership_pk      IN membership.membership_pk%TYPE  DEFAULT NULL)
IS
BEGIN

htp.p('<input type="hidden" name="p_service_pk" value="3">');
htp.p('<input type="hidden" name="p_membership_pk" value="7">');

END select_membership;

---------------------------------------------------------------------
-- Name: publish_calendar
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE publish_calendar (    p_command               IN VARCHAR2                             DEFAULT NULL,
                                p_heading               IN VARCHAR2                             DEFAULT NULL,
                                p_message               IN LONG                                 DEFAULT NULL,
                                p_event_date            IN VARCHAR2                             DEFAULT NULL,
                                p_end_date              IN VARCHAR2                             DEFAULT NULL,
                                p_name                  IN VARCHAR2                             DEFAULT NULL,
                                p_language_pk           IN calendar.language_fk%TYPE            DEFAULT NULL,
                                p_calendar_pk           IN calendar.calendar_pk%TYPE            DEFAULT NULL,
                                p_picture_name          IN VARCHAR2                             DEFAULT NULL,
                                p_picture_pk            IN picture.picture_pk%TYPE              DEFAULT NULL,
                                p_error                 IN VARCHAR2                             DEFAULT NULL
                                )
IS
BEGIN
DECLARE

v_organization_pk       organization.organization_pk%TYPE       DEFAULT NULL;
v_message               calendar.message%TYPE                   DEFAULT NULL;
v_heading               calendar.heading%TYPE                   DEFAULT NULL;
v_event_date            calendar.event_date%TYPE                DEFAULT NULL;
v_end_date              calendar.end_date%TYPE                  DEFAULT NULL;
v_calendar_pk           calendar.calendar_pk%TYPE               DEFAULT NULL;
v_language_pk           calendar.language_fk%TYPE               DEFAULT NULL;
v_picture_name          VARCHAR2(100)                           DEFAULT NULL;
v_picture_pk            picture.picture_pk%TYPE                 DEFAULT NULL;
v_check                 NUMBER                                  DEFAULT NULL;

CURSOR  get_calendar IS
SELECT  calendar_pk, heading, event_date
FROM    calendar
WHERE   organization_fk = v_organization_pk
AND     ( event_date > SYSDATE-1 or end_date > SYSDATE-1 )
ORDER BY event_date
;

CURSOR  get_image IS
SELECT  picture_fk
FROM    picture_in_calendar
WHERE   calendar_fk = p_calendar_pk
;


BEGIN
IF (login.timeout('admin.publish_calendar') > 0 ) THEN
        v_organization_pk := get.oid;

        IF ( v_organization_pk IS NULL ) THEN
                get.error_page(3, get.txt('not_a_client_user'), NULL, 'main_page.startup');
        END IF;

        IF ( p_command = 'new' ) THEN
                html.b_page(NULL,NULL,NULL,2);
                html.b_box( get.txt('publish_calendar'), '100%', 'admin.publish_calendar-new');
                html.b_form('admin.publish_calendar');
                html.b_table;
                htp.p('<input type="hidden" name="p_command" value="insert">
                <tr><td colspan="2" align="center"><b>'|| get.oname(v_organization_pk) ||'</b></td></tr>
                <tr><td colspan="2">'||get.txt('new_cal_info')||'</td></tr>
                <tr><td colspan="2"><HR size="2" noshade="noshade"></td></tr>
                <tr><td>'|| get.txt('select_lang_org') ||':</td><td>');
                select_lang_org(v_organization_pk);
                htp.p('</td></tr>

                <tr><td>'|| get.txt('event_date') ||':</td><td>
                <input type="text" length="16" maxlength="16"
                name="p_event_date" value="'||TO_CHAR(sysdate,get.txt('date_long'))||'"
                onSelect=javascript:openWin(''date_time.startup?p_variable=p_event_date'',300,250)
                onClick=javascript:openWin(''date_time.startup?p_variable=p_event_date'',300,250)>'||
                html.popup( get.txt('change_entry_date'),'date_time.startup?p_variable=p_event_date','300', '250') ||'
                </td></tr>

                <tr><td>'|| get.txt('event_end_date') ||':</td><td>
                <input type="text" length="16" maxlength="16"
                name="p_end_date" value=""
                onSelect=javascript:openWin(''date_time.startup?p_variable=p_end_date'',300,250)
                onClick=javascript:openWin(''date_time.startup?p_variable=p_end_date'',300,250)>'||
                html.popup( get.txt('change_event_end_date'),'date_time.startup?p_variable=p_end_date','300', '250') ||'
                &nbsp;&nbsp;&nbsp;
                <SCRIPT language="JavaScript">
                function clear_field(){
                        document.form.p_end_date.value = '''';
                }
                </SCRIPT>
                <a href="JavaScript:clear_field()">'|| get.txt('clear_end_date_field') ||'</a>
                </td></tr>

                <tr><td>'|| get.txt('add_photo_cal') ||':</td><td>
                <input type="hidden" name="p_picture_pk" value="">
                <input type="text" name="p_picture_name" value="'||get.txt('no_photo')||'"
                onSelect=javascript:openWin(''admin.photo?p_organization_pk='||v_organization_pk||''',300,150)
                onClick=javascript:openWin(''admin.photo?p_organization_pk='||v_organization_pk||''',300,150)>');
                htp.p( html.popup( get.txt('add_photo_cal'),'admin.photo?p_organization_pk='||v_organization_pk||'','300', '150') );
                htp.p('</td></tr>

                <tr><td>'|| get.txt('heading') ||':</td><td>
                '|| html.text('p_heading', '70', '150', '' ) ||'
                </td></tr>

                <tr><td valign="top">'|| get.txt('message') ||':</td><td>
                <textarea name="p_message" rows="15" cols="70" wrap="virtual"></textarea>
                </td></tr>
                <tr><td colspan="2" align="center">'); html.submit_link( get.txt('Publish calendar') ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page;
        ELSIF ( p_command = 'edit') THEN
                v_heading := SUBSTR(p_heading,1,150);
                UPDATE  calendar
                SET     heading=v_heading,
                        message=p_message,
                        event_date=to_date(p_event_date,get.txt('date_long')),
                        end_date=to_date(p_end_date,get.txt('date_long')),
                        accepted=NULL,
                        language_fk=p_language_pk
                WHERE   calendar_pk=p_calendar_pk;

                v_check := system.inc_pages( v_organization_pk, p_language_pk, NULL, p_calendar_pk );

                DELETE FROM picture_in_calendar
                WHERE calendar_fk = p_calendar_pk
                ;
                COMMIT;
                IF ( p_picture_pk IS NOT NULL ) THEN
                   INSERT INTO picture_in_calendar
                   (picture_fk, calendar_fk)
                   VALUES
                   (p_picture_pk, p_calendar_pk);
                   COMMIT;
                END IF;
                COMMIT;
                html.jump_to ( 'admin.publish_calendar');
        ELSIF ( p_command = 'insert' ) THEN
                SELECT  calendar_seq.NEXTVAL
                INTO    v_calendar_pk
                FROM    dual
                ;
                v_heading := SUBSTR(p_heading,1,150);
                v_message := SUBSTR(p_message,1,4000);
                INSERT INTO calendar
                (calendar_pk, organization_fk, language_fk, event_date, heading, message, end_date)
                VALUES
                (
                v_calendar_pk, v_organization_pk, p_language_pk,
                to_date(p_event_date,get.txt('date_long')),
                v_heading , v_message,
                to_date(p_end_date,get.txt('date_long'))
                )
                ;
                COMMIT;

                v_check := system.inc_pages( v_organization_pk, p_language_pk, NULL, v_calendar_pk );

                IF ( p_picture_pk IS NOT NULL ) THEN
                   INSERT INTO picture_in_calendar
                   (picture_fk, calendar_fk)
                   VALUES
                   (p_picture_pk, v_calendar_pk);
                   COMMIT;
                END IF;

                IF ( p_heading IS NOT NULL ) THEN
                        html.jump_to('admin.publish_calendar');
                ELSE
                        html.jump_to('admin.publish_calendar?p_command=update&p_calendar_pk='||v_calendar_pk||'&p_error=no_heading');
                END IF;
        ELSIF ( p_command = 'update' ) THEN
                SELECT  calendar_pk, heading, event_date, message, language_fk, end_date
                INTO    v_calendar_pk, v_heading, v_event_date, v_message, v_language_pk, v_end_date
                FROM    calendar
                WHERE   calendar_pk = p_calendar_pk
                ;
                html.b_page(NULL,NULL,NULL,2);
                html.b_box( get.txt('edit_calendar'), '100%', 'admin.edit_calendar-update' );
                html.b_form('admin.publish_calendar');
                html.b_table;
                htp.p('<input type="hidden" name="p_command" value="edit">
                <input type="hidden" name="p_calendar_pk" value="'||p_calendar_pk||'">
                <tr><td colspan="2" align="center"><b>'|| get.oname(v_organization_pk) ||'</b></td></tr>');
                IF ( p_error IS NOT NULL ) THEN
                        htp.p('<tr><td colspan="2"><font color="red"><b>'|| get.txt(p_error) ||'</b></font></td></tr>');
                END IF;

                open get_image;
                fetch get_image into v_picture_pk;
                close get_image;
                IF ( v_picture_pk is NULL ) THEN
                   v_picture_name := get.txt('no_photo');
                ELSE
                   v_picture_name := get.ap_text(v_picture_pk, 'what');
                END IF;

                htp.p('<tr><td colspan="2">'||get.txt('update_cal_info')||'</td></tr>
                <tr><td colspan="2"><HR size="2" noshade="noshade"></td></tr>
                <tr><td>'|| get.txt('select_lang_org') ||':</td><td>');
                select_lang_org(v_organization_pk,v_language_pk);
                htp.p('</td></tr>

                <tr><td>'|| get.txt('event_date') ||':</td><td>
                <input type="text" length="16" maxlength="16"
                name="p_event_date" value="'||TO_CHAR(v_event_date,get.txt('date_long'))||'"
                onSelect=javascript:openWin(''date_time.startup?p_variable=p_event_date&p_start_date='||TO_CHAR(v_event_date,get.txt('date_full'))||''',300,250)
                onClick=javascript:openWin(''date_time.startup?p_variable=p_event_date&p_start_date='||TO_CHAR(v_event_date,get.txt('date_full'))||''',300,250)>
                '|| html.popup( get.txt('change_entry_date'),'date_time.startup?p_variable=p_event_date&p_start_date='||TO_CHAR(v_event_date,get.txt('date_full'))||'','300', '250') ||'
                </td></tr>

                <tr><td>'|| get.txt('event_end_date') ||':</td><td>
                <input type="text" length="16" maxlength="16"
                name="p_end_date" value="'||TO_CHAR(v_end_date,get.txt('date_long'))||'"
                onSelect=javascript:openWin(''date_time.startup?p_variable=p_end_date&p_start_date='||TO_CHAR(v_end_date,get.txt('date_full'))||''',300,250)
                onClick=javascript:openWin(''date_time.startup?p_variable=p_end_date&p_start_date='||TO_CHAR(v_end_date,get.txt('date_full'))||''',300,250)>
                '|| html.popup( get.txt('change_event_end_date'),'date_time.startup?p_variable=p_end_date&p_start_date='||TO_CHAR(v_end_date,get.txt('date_full'))||'','300', '250') ||'
                &nbsp;&nbsp;&nbsp;
                <SCRIPT language="JavaScript">
                function clear_field(){
                        document.form.p_end_date.value = '''';
                }
                </SCRIPT>
                <a href="JavaScript: clear_field();">'|| get.txt('clear_end_date_field') ||'</a>
                </td></tr>


                <SCRIPT language="JavaScript">
                function clear_picture_field(){
                        document.form.p_picture_pk.value = '''';
                        document.form.p_picture_name.value = '''||get.txt('no_photo')||''';
                }
                </SCRIPT>

                <tr><td>'|| get.txt('add_photo_cal') ||':</td><td>
                <input type="hidden" name="p_picture_pk" value="'||v_picture_pk||'">
                <input type="text" name="p_picture_name" value="'||v_picture_name||'"
                onSelect=javascript:openWin(''admin.photo?p_organization_pk='||v_organization_pk||''',300,150)
                onClick=javascript:openWin(''admin.photo?p_organization_pk='||v_organization_pk||''',300,150)>');
                htp.p( html.popup( get.txt('add_photo_cal'),'admin.photo?p_organization_pk='||v_organization_pk||'','300', '150') );
                htp.p('&nbsp;&nbsp;&nbsp;
                <a href="JavaScript: clear_picture_field();">'|| get.txt('clear_picture_field') ||'</a>
                </td></tr>

                <tr><td>'|| get.txt('heading') ||':</td><td>
                '|| html.text('p_heading', '70', '150', v_heading)||'
                </td></tr>

                <tr><td valign="top">'|| get.txt('message') ||':</td><td>
                <textarea name="p_message" rows="15" cols="70" wrap="virtual">'||v_message||'</textarea>
                </td></tr>
                <tr><td colspan="2" align="center">'); html.submit_link( get.txt('Update calendar') ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page;
        ELSIF ( p_command = 'delete') THEN
                IF ( p_name = 'yes') THEN
                        DELETE  FROM picture_in_calendar
                        WHERE   calendar_fk=p_calendar_pk
                        ;

                        SELECT language_fk
                        into   v_language_pk
                        from   calendar
                        where  calendar_pk=p_calendar_pk;
                        v_check := system.inc_pages( v_organization_pk, v_language_pk, NULL, p_calendar_pk );

                        DELETE  FROM calendar
                        WHERE   calendar_pk=p_calendar_pk
                        AND     organization_fk = v_organization_pk;
                        COMMIT;
--                      UPDATE  calendar
--                      SET     accepted = NULL
--                      WHERE   organization_fk = v_organization_pk;
--                      COMMIT;


                        html.jump_to ( 'admin.publish_calendar' );
                ELSIF ( p_name IS NULL ) THEN
                        html.jump_to ( 'admin.publish_calendar' );
                ELSE
                        html.b_page(NULL,NULL,NULL,2);
                        html.b_box( get.txt('publish_calendar'), '100%', 'admin.publish_calendar-delete' );
                        html.b_table;
                        html.b_form('admin.publish_calendar');
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="yes">
                        <input type="hidden" name="p_calendar_pk" value="'||p_calendar_pk||'">
                        <td>'|| get.txt('delete_info')||' "'||p_name||'" ?'); html.submit_link( get.txt('YES') ); htp.p('</td>');
                        html.e_form;
                        htp.p('<td>'); html.button_link( get.txt('NO'), 'admin.publish_calendar');htp.p('</td></tr>');
                        html.e_table;
                        html.e_box;
                        html.e_page;
                END IF;
        ELSE
                html.b_page(NULL,NULL,NULL,2);
                html.b_box( get.txt('edit_calendar'), '100%', 'admin.edit_calendar' );
                html.b_table;
                htp.p('<tr><td colspan="4">'||get.txt('cal_info')||'</td></tr>
                <tr><td colspan="4"><HR size="2" noshade="noshade"></td></tr>');
                OPEN get_calendar;
                LOOP
                        fetch get_calendar INTO v_calendar_pk, v_heading, v_event_date;
                        exit when get_calendar%NOTFOUND;
                        html.b_form('admin.publish_calendar','form'|| v_calendar_pk);
                        htp.p('<input type="hidden" name="p_command" value="update">
                        <input type="hidden" name="p_calendar_pk" value="'||v_calendar_pk||'">
                        <tr><td align="left" nowrap>'||to_char(v_event_date,get.txt('date_long'))||' :</td><td align="left" width="60%">'||html.rem_tag(v_heading)||'</td>
                        <td>'); html.submit_link( get.txt('edit'), 'form'||v_calendar_pk ); htp.p('</td>');
                        html.e_form;
                        html.b_form('admin.publish_calendar','delete'|| v_calendar_pk);
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="'||replace(v_heading,'"','')||'">
                        <input type="hidden" name="p_calendar_pk" value="'||v_calendar_pk||'">
                        <td>'); html.submit_link( get.txt('delete'), 'delete'||v_calendar_pk ); htp.p('</td></tr>');
                        html.e_form;
                END LOOP;
                close get_calendar;
                html.b_form('admin.publish_calendar','form_new');
                htp.p('<input type="hidden" name="p_command" value="new">
                <td>'); html.submit_link( get.txt('new'), 'form_new'); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page;
        END IF;
END IF;
END;
END publish_calendar;

---------------------------------------------------------------------
-- Name: publish_org_info
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE publish_org_info (    p_command               IN VARCHAR2                             DEFAULT NULL,
                                p_info_name_pk          IN org_info.info_name_fk%TYPE           DEFAULT NULL,
                                p_value                 IN LONG                                 DEFAULT NULL,
                                p_org_info_pk           IN org_info.org_info_pk%TYPE            DEFAULT NULL,
                                p_name                  IN VARCHAR2                             DEFAULT NULL
                                )
IS
BEGIN
DECLARE

v_organization_pk       organization.organization_pk%TYPE       DEFAULT NULL;
v_org_info_pk           org_info.org_info_pk%TYPE               DEFAULT NULL;
v_info_name_fk          org_info.info_name_fk%TYPE              DEFAULT NULL;
v_name_sg_fk            info_name.name_sg_fk%TYPE                       DEFAULT NULL;
v_value                 org_info.value%TYPE                     DEFAULT NULL;

CURSOR  get_org_info IS
SELECT  oi.org_info_pk, oi.info_name_fk, oi.value, i.name_sg_fk
FROM    org_info oi, info_name i
WHERE   organization_fk = v_organization_pk
AND     oi.info_name_fk = i.info_name_pk
ORDER BY value
;


BEGIN
IF (login.timeout('admin.publish_org_info') > 0 ) THEN
        v_organization_pk := get.oid;

        IF ( v_organization_pk IS NULL ) THEN
                -- Hopper ut hvis brukeren ikke er en organization bruker!
                html.jump_to('http://www.in4mant.com/not_org_user.html');
        END IF;

        IF ( p_command = 'update' ) THEN
                SELECT  info_name_fk, value
                INTO    v_info_name_fk, v_value
                FROM    org_info
                WHERE   org_info_pk = p_org_info_pk
                ;
                html.b_page(NULL,NULL,NULL,2);
                html.b_box( get.txt('publish_org_info'), '100%', 'admin.publish_org_info' );
                html.b_table;
                html.b_form('admin.publish_org_info');
                htp.p('<tr><td colspan="2">'||get.txt('update_org_info')||'</td></tr>
                <tr><td colspan="2"><HR size="2" noshade="noshade"></td></tr>
                <input type="hidden" name="p_command" value="edit">
                <input type="hidden" name="p_org_info_pk" value="'||p_org_info_pk||'">

                <tr><td>'|| get.txt('select_info_name') ||':</td><td>');
                select_info_name(v_info_name_fk);
                htp.p('</td></tr>

                <tr><td valign="top">'|| get.txt('value') ||':(maks 4000 tegn)</td><td>
                <textarea name="p_value" rows="10" cols="70" wrap="virtual">'||v_value||'</textarea>
                </td></tr>
                <tr><td valign="top">'); html.submit_link( get.txt('update') ); htp.p('</td>
                <td align="right">'); html.button_link( get.txt('back'), 'admin.publish_org_info' ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page;
        ELSIF ( p_command = 'edit') THEN
                v_value := SUBSTR(p_value,1,4000);
                UPDATE  org_info
                SET     value=v_value,
                        info_name_fk=p_info_name_pk
                WHERE   org_info_pk=p_org_info_pk;
                COMMIT;
                html.jump_to ( 'admin.publish_org_info');
        ELSIF ( p_command = 'new' ) THEN
                html.b_page(NULL,NULL,NULL,2);
                html.b_box( get.txt('publish_org_info'), '100%', 'admin.publish_org_info' );
                html.b_form('admin.publish_org_info');
                html.b_table;
                htp.p('<input type="hidden" name="p_command" value="insert">
                <tr><td colspan="2" align="center"><b>'|| get.oname(v_organization_pk) ||'</b></td></tr>
                <tr><td colspan="2">'||get.txt('new_org_info')||'</td></tr>
                <tr><td colspan="2"><HR size="2" noshade="noshade"></td></tr>
                <tr><td>'|| get.txt('select_info_name') ||':</td><td>');
                select_info_name;
                htp.p('</td></tr>

                <tr><td valign="top">'|| get.txt('value') ||':(maks 4000 tegn)</td><td>
                <textarea name="p_value" rows="10" cols="70" wrap="virtual"></textarea>
                </td></tr>
                <tr><td colspan="2" align="center">'); html.submit_link( get.txt('Publish org_info') ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page;
        ELSIF ( p_command = 'insert' ) THEN
                v_value := SUBSTR(p_value,1,4000);
                INSERT INTO org_info
                (org_info_pk, organization_fk, info_name_fk, value)
                VALUES
                (org_info_seq.NEXTVAL, v_organization_pk, p_info_name_pk, v_value )
                ;
                html.jump_to('admin.publish_org_info');
        ELSIF ( p_command = 'delete') THEN
                IF ( p_name = 'yes') THEN
                        DELETE  FROM org_info
                        WHERE   org_info_pk=p_org_info_pk;
                        COMMIT;
                        html.jump_to ( 'admin.publish_org_info' );
                ELSIF ( p_name IS NULL ) THEN
                        html.jump_to ( 'admin.publish_org_info' );
                ELSE
                        html.b_page(NULL,NULL,NULL,2);
                        html.b_box( get.txt('publish_org_info') , '100%', 'admin.publish_org_info');
                        html.b_table;
                        html.b_form('admin.publish_org_info');
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="yes">
                        <input type="hidden" name="p_org_info_pk" value="'||p_org_info_pk||'">
                        <td>'|| get.txt('delete_info')||' "'||p_name||'" ?'); html.submit_link( get.txt('YES') ); htp.p('</td>');
                        html.e_form;
                        htp.p('<td>'); html.button_link( get.txt('NO'), 'admin.publish_org_info');htp.p('</td></tr>');
                        html.e_table;
                        html.e_box;
                        html.e_page;
                END IF;
        ELSE
                html.b_page(NULL,NULL,NULL,2);
                html.b_box( get.txt('info_name') , '100%', 'admin.info_name');
                html.b_table;
                htp.p('<tr><td colspan="4">'||get.txt('org_info')||'</td></tr>
                <tr><td colspan="4"><HR size="2" noshade="noshade"></td></tr>');
                OPEN get_org_info;
                LOOP
                        fetch get_org_info INTO v_org_info_pk, v_info_name_fk, v_value, v_name_sg_fk;
                        exit when get_org_info%NOTFOUND;
                        html.b_form('admin.publish_org_info','form'|| v_org_info_pk);
                        htp.p('<input type="hidden" name="p_command" value="update">
                        <input type="hidden" name="p_org_info_pk" value="'||v_org_info_pk||'">
                        <tr><td valign="top"><b>'|| get.text(v_name_sg_fk) ||':</b></td><td>'||v_value||'</td>
                        <td>'); html.submit_link( get.txt('edit'), 'form'||v_org_info_pk ); htp.p('</td>');
                        html.e_form;
                        html.b_form('admin.publish_org_info','delete'||v_org_info_pk);
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="'||v_value||'">
                        <input type="hidden" name="p_org_info_pk" value="'||v_org_info_pk||'">
                        <td>'); html.submit_link( get.txt('delete'), 'delete'||v_org_info_pk ); htp.p('</td></tr>');
                        html.e_form;
                END LOOP;
                close get_org_info;
                html.b_form('admin.publish_org_info','form_new');
                htp.p('<input type="hidden" name="p_command" value="new">
                <td>'); html.submit_link( get.txt('new'), 'form_new'); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page;
        END IF;
END IF;
END;
END publish_org_info;

---------------------------------------------------------------------
-- Name: select_info_name
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE select_info_name ( p_info_name_fk             IN info_name.info_name_pk%TYPE                  DEFAULT NULL)
IS
BEGIN
DECLARE

v_info_name_pk          info_name.info_name_pk%TYPE                     DEFAULT NULL;
v_name_sg_fk            info_name.name_sg_fk%TYPE                       DEFAULT NULL;
v_value                 VARCHAR2(20)                                    DEFAULT NULL;
v_organization_pk       organization.organization_pk%TYPE               DEFAULT NULL;
v_count                 NUMBER                                          DEFAULT NULL;
v_service_pk            service.service_pk%TYPE                         DEFAULT NULL;

CURSOR  all_info_name IS
SELECT  info_name_pk, name_sg_fk
FROM    info_name
WHERE   service_fk = v_service_pk
order by description
;

BEGIN
        v_organization_pk := get.oid;
        v_service_pk := get.oserv( v_organization_pk );

        htp.p('<select name="p_info_name_pk">');
        OPEN all_info_name;
        LOOP
                fetch all_info_name into v_info_name_pk, v_name_sg_fk;
                exit when all_info_name%NOTFOUND;
                IF ( v_info_name_pk = p_info_name_fk) THEN
                        v_value := ' selected';
                ELSE
                        SELECT  count(*)
                        INTO    v_count
                        FROM    info_name i, org_info oi
                        WHERE   oi.info_name_fk = i.info_name_pk
                        AND     oi.organization_fk = v_organization_pk
                        AND     i.info_name_pk = v_info_name_pk
                        ;
                        v_value := '';
                END IF;
                IF ( v_count > 0 ) THEN
                        v_count := 0;
                ELSE
                        htp.p('<option value="'|| v_info_name_pk ||'"'||v_value||'>'|| get.text(v_name_sg_fk) ||'</option>');
                END IF;
        END LOOP;
        close all_info_name;
        htp.p('</select>');
END;
END select_info_name;

---------------------------------------------------------------------
-- Name: photo
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 15.08.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE photo          (      p_organization_pk       IN organization.organization_pk%TYPE    DEFAULT NULL,
                                p_album_pk              IN album.album_pk%TYPE                  DEFAULT NULL,
                                p_picture_pk            IN picture.picture_pk%TYPE              DEFAULT NULL )
IS
BEGIN
DECLARE

v_organization_pk       organization.organization_pk%TYPE       DEFAULT NULL;
v_album_pk              album.album_pk%TYPE                     DEFAULT NULL;
v_picture_pk            picture.picture_pk%TYPE                 DEFAULT NULL;

CURSOR  select_all IS
SELECT  album_pk
FROM    album
WHERE   organization_fk = p_organization_pk
ORDER BY create_date desc;

CURSOR  select_all_pic IS
SELECT  picture_pk
FROM    picture, picture_in_album
WHERE   picture_fk = picture_pk
AND     album_fk = p_album_pk
ORDER BY when_taken DESC;

BEGIN

IF ( p_album_pk IS NULL ) THEN
        html.b_page_2;
        html.b_box(get.txt('choose_album'), '100%', 'admin.choose_album');
        html.b_form('admin.photo');
        htp.p('<select name="p_album_pk">');
        OPEN select_all;
        LOOP
                FETCH select_all INTO v_album_pk;
                EXIT WHEN select_all%NOTFOUND;
                htp.p('<option value="'|| v_album_pk ||'">'|| get.ap_text(v_album_pk, 'title') ||'</option>');
        END LOOP;
        CLOSE select_all;
        htp.p('</select><br>');
        html.submit_link( get.txt('register'));
        html.e_form;
        html.e_box;
        html.e_page_2;
ELSIF ( p_picture_pk IS NULL ) THEN
        html.b_page_2;
        html.b_box(get.txt('choose_picture'), '100%', 'admin.choose_picture');
        html.b_form('admin.photo');
        htp.p('
        <input type="hidden" name="p_album_pk" value="'||p_album_pk||'">
        <select name="p_picture_pk">');
        OPEN select_all_pic;
        LOOP
                FETCH select_all_pic INTO v_picture_pk;
                EXIT WHEN select_all_pic%NOTFOUND;
                htp.p('<option value="'|| v_picture_pk ||'">'|| get.ap_text(v_picture_pk, 'what') ||'</option>');
        END LOOP;
        CLOSE select_all_pic;
        htp.p('</select><br>');
        html.submit_link( get.txt('register'));
        html.e_form;
        html.e_box;
        html.e_page_2;
ELSE
        htp.p('<SCRIPT LANGUAGE="JavaScript">
        function update_photo(picture_pk,picture_name) {
                if(opener.document.form.p_picture_name) {
                        opener.document.form.p_picture_name.value = picture_name;
                        opener.document.form.p_picture_pk.value = picture_pk;
                }
                window.close();
        }
        </SCRIPT>
        <SCRIPT language=JavaScript>
                update_photo('''||p_picture_pk||''',"'|| replace(get.ap_text(p_picture_pk, 'what'),'"','') ||'");
        </SCRIPT>
        ');
--                update_photo('''||p_picture_pk||''','''|| replace(get.ap_text(p_picture_pk, 'what'),'','') ||''');
END IF;
END;
END photo;

---------------------------------------------------------------------
-- Name: related_docs
-- Type: procedure
-- What: Genererer dokumentarkiv
-- Made: Espen Messel
-- Date: 15.07.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE related_docs (       p_organization_pk       IN organization.organization_pk%TYPE            DEFAULT NULL,
                               p_language_pk           IN document.language_fk%TYPE                    DEFAULT NULL,
                               p_service_pk            IN service.service_pk%TYPE                      DEFAULT NULL,
                               p_document_pk           IN document.document_pk%TYPE                    DEFAULT NULL,
                               p_new_date              IN VARCHAR2                                     DEFAULT to_char(SYSDATE,get.txt('date_long')),
                               p_old_date              IN VARCHAR2                                     DEFAULT NULL,
                               p_quit                  IN VARCHAR2                                     DEFAULT NULL,
                               p_count                 IN NUMBER                                       DEFAULT 1
                        )
IS
BEGIN
DECLARE
v_service_pk            service.service_pk%TYPE                 DEFAULT NULL;
v_publish_date          document.publish_date%TYPE              DEFAULT NULL;
v_publish_date2         document.publish_date%TYPE              DEFAULT NULL;
v_heading               document.heading%TYPE                   DEFAULT NULL;
v_ingress               document.ingress%TYPE                   DEFAULT NULL;
v_file_path             document.file_path%TYPE                 DEFAULT NULL;
v_document_name         document.document_name%TYPE             DEFAULT NULL;
v_document_pk           document.document_pk%TYPE               DEFAULT NULL;
v_document_pk_2         document.document_pk%TYPE               DEFAULT NULL;
v_name                  organization.name%TYPE                  DEFAULT NULL;
v_count                 NUMBER                                  DEFAULT NULL;
v_count2                NUMBER                                  DEFAULT NULL;
v_count3                NUMBER                                  DEFAULT NULL;
v_doc_arc_nr            NUMBER                                  DEFAULT NULL;
v_organization_pk       organization.organization_pk%TYPE       DEFAULT NULL;
v_language_pk           document.language_fk%TYPE               DEFAULT NULL;
v_checked               VARCHAR2(10)                            DEFAULT NULL;

CURSOR  document_archive IS
SELECT  d.publish_date, d.heading, d.document_pk
FROM    document d, document_type dt, organization o
WHERE   d.document_type_fk=dt.document_type_pk
AND     d.language_fk=v_language_pk
AND     d.organization_fk = v_organization_pk
AND     d.organization_fk = o.organization_pk
AND     d.publish_date < to_date(p_new_date,get.txt('date_long'))
AND     d.publish_date < SYSDATE
AND     d.accepted > 0
AND     dt.service_fk=v_service_pk
AND     d.document_pk != p_document_pk
ORDER BY d.publish_date DESC
;

CURSOR  document_selected IS
SELECT  rd.document_fk_2
FROM    relation_d2d rd, document d, organization o
WHERE   d.language_fk=v_language_pk
AND     d.organization_fk = v_organization_pk
AND     d.organization_fk = o.organization_pk
AND     d.publish_date < to_date(p_new_date,get.txt('date_long'))
AND     d.publish_date < SYSDATE
AND     d.accepted > 0
AND     d.document_pk != p_document_pk
AND     rd.document_fk_1 = p_document_pk
AND     d.document_pk = rd.document_fk_2
ORDER BY d.publish_date DESC
;

CURSOR  document_archive2 IS
SELECT  d.publish_date, d.heading, d.document_pk
FROM    document d, document_type dt, organization o
WHERE   d.document_type_fk=dt.document_type_pk
AND     d.language_fk=v_language_pk
AND     d.organization_fk = v_organization_pk
AND     d.organization_fk = o.organization_pk
AND     d.publish_date < to_date(p_new_date,get.txt('date_long'))
AND     d.publish_date < SYSDATE
AND     d.accepted > 0
AND     dt.service_fk=v_service_pk
ORDER BY d.publish_date DESC
;

BEGIN

IF (login.timeout('admin.related_docs') > 0 ) THEN

   v_organization_pk := get.oid;

   IF ( v_organization_pk IS NULL ) THEN
         html.jump_to('main_page.startup',2,'Not a client user!');
   ELSE

      select count(*) into v_count
      from document
      where organization_fk = v_organization_pk
      and   accepted > 0
      ;

      IF ( v_count = 1 ) THEN
          html.jump_to('admin.startup');
      ELSE

--      v_doc_arc_nr := get.value('doc_arc_nr');
      v_doc_arc_nr := 100;

      html.b_page(NULL,NULL,NULL,2);

      IF ( p_service_pk IS NULL ) THEN
         v_service_pk := get.oserv(v_organization_pk);
      ELSE
         v_service_pk := p_service_pk;
      END IF;
      IF ( p_language_pk IS NULL ) THEN
         v_language_pk := get.lan;
      ELSE
         v_language_pk := p_language_pk;
      END IF;

      IF ( p_document_pk IS NOT NULL ) THEN

        html.b_box( get.txt('related_documents') , '100%', 'admin.related_docs');
        html.b_table;
        html.b_form('admin.related_docs2','next');

         SELECT  count(*)
           INTO    v_count
           FROM    document d, document_type dt, organization o
           WHERE   d.document_type_fk=dt.document_type_pk
           AND     d.language_fk=v_language_pk
           AND     d.organization_fk = v_organization_pk
           AND     d.organization_fk = o.organization_pk
           AND     d.publish_date < SYSDATE
           AND     d.accepted > 0
           ;
         htp.p('<input type="hidden" name="p_document_pk" value="'|| p_document_pk ||'">
                 <input type="hidden" name="p_rel_doc_pk" value="0">');
         SELECT publish_date, heading, ingress
           INTO    v_publish_date, v_heading, v_ingress
           FROM    document
           WHERE   document_pk = p_document_pk
           AND     organization_fk = v_organization_pk
           AND     accepted > 0
           ;
         htp.p('<tr><td colspan="3">'|| get.txt('help_rel_doc') ||'</td></tr>
               <tr><td width="20%" align="left">'|| to_char(v_publish_date,get.txt('date_long')) ||':</td>
               <td width="80%" align="left" colspan="2">'|| html.rem_tag(v_heading) ||'</td></tr>
                 <tr><td colspan="3"><HR size="2" noshade="noshade"></td></tr>');
         OPEN document_archive;
         OPEN document_selected;
--         WHILE ( document_archive%ROWCOUNT < v_doc_arc_nr )
         LOOP
            FETCH document_archive INTO v_publish_date, v_heading, v_document_pk;
            EXIT when document_archive%NOTFOUND;
            IF ( v_document_pk_2 IS NULL ) THEN
               FETCH document_selected INTO v_document_pk_2;
            END IF;
            IF ( v_document_pk = v_document_pk_2 ) THEN
                  v_checked := ' checked';
                  v_document_pk_2 := NULL;
            ELSE
                  v_checked := '';
            end if;
            htp.p('<tr><td width="20%" align="left">'|| to_char(v_publish_date,get.txt('date_long')) ||':</td>
                  <td width="60%" align="left">'|| v_heading ||'</td>
                    <td width="20%" align="left">
                    <input type="checkbox" name="p_rel_doc_pk" value="'||v_document_pk||'"'|| v_checked ||'>
                    <input type="hidden" name="p_del_doc_pk" value="'||v_document_pk||'"></td></tr>');
         END LOOP;
         CLOSE document_selected;
         CLOSE document_archive;
         htp.p('<tr><td colspan="3">&nbsp;</td></tr>
<tr><td align="center" valign="top">&nbsp;<input type="submit"
               name="p_quit" value="'||get.txt('reg_quit')||'"></td>');
         v_count2 := p_count+v_doc_arc_nr-1;
         IF ( v_count2 > v_count ) THEN
            htp.p('<td valign="center" align="center">'|| p_count ||'-'|| v_count ||'/'|| v_count ||'</td>');
         ELSE
            htp.p('<td valign="center" align="center">'|| p_count ||'-'|| v_count2 ||'/'|| v_count ||'</td>');
         END IF;
         IF ( v_count2 < v_count ) THEN
            v_count3 := p_count+v_doc_arc_nr;
            IF ( v_count - v_count2 < v_doc_arc_nr ) THEN
               v_doc_arc_nr := v_count - v_count2;
            END IF;
            htp.p('<input type="hidden" name="p_service_pk" value="'|| v_service_pk ||'">
                    <input type="hidden" name="p_document_pk" value="'|| p_document_pk ||'">
                    <input type="hidden" name="p_language_pk" value="'|| p_language_pk ||'">
                    <input type="hidden" name="p_organization_pk" value="'|| v_organization_pk ||'">
                    <input type="hidden" name="p_count" value="'|| v_count3 ||'">
                    <input type="hidden" name="p_new_date" value="'|| to_char( v_publish_date,get.txt('date_long')) ||'">
                    <input type="hidden" name="p_old_date" value="'|| p_old_date ||';;'|| p_new_date ||'">
                    <td align="center" valign="top">&nbsp;');
            html.submit_link(get.txt('reg_go_to')||' '||get.txt('next')||' '||v_doc_arc_nr,'next');
            htp.p('</td></tr>');
            html.e_form;
         ELSE
            htp.p('<td align="center" valign="top">&nbsp;</td></tr>');
         END IF;
      ELSE
        html.b_box( get.txt('related_documents') , '100%', 'admin.related_docs(2)');
        html.b_table;
        html.b_form('admin.related_docs2','next');

         htp.p('<tr><td colspan="3">'|| get.txt('help_rel_old_doc') ||'</td></tr>
                 <tr><td colspan="3"><HR size="2" noshade="noshade"></td></tr>');
         OPEN document_archive2;
--         WHILE ( document_archive2%ROWCOUNT < v_doc_arc_nr )
         LOOP
            FETCH document_archive2 INTO v_publish_date, v_heading, v_document_pk;
            EXIT when document_archive2%NOTFOUND;
            htp.p('<tr><td width="20%" align="left">'|| to_char(v_publish_date,get.txt('date_long')) ||':</td>
                  <td width="80%" align="left"><a href="admin.related_docs?p_document_pk='||v_document_pk||'">'|| v_heading ||'</a></td></tr>
                    ');
         END LOOP;
         CLOSE document_archive2;
      END IF;
      END IF;

   html.e_table;
   html.e_box;
   html.e_page;
   END IF;

END IF;
END;
END related_docs;

---------------------------------------------------------------------
-- Name: related_docs2
-- Type: procedure
-- What: Genererer dokumentarkiv
-- Made: Espen Messel
-- Date: 15.07.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE related_docs2 (       p_organization_pk       IN organization.organization_pk%TYPE            DEFAULT NULL,
                               p_language_pk           IN document.language_fk%TYPE                    DEFAULT NULL,
                               p_service_pk            IN service.service_pk%TYPE                      DEFAULT NULL,
                               p_document_pk           IN document.document_pk%TYPE                    DEFAULT NULL,
                               p_new_date              IN VARCHAR2                                     DEFAULT to_char(SYSDATE,get.txt('date_long')),
                               p_old_date              IN VARCHAR2                                     DEFAULT NULL,
                               p_quit                  IN VARCHAR2                                     DEFAULT NULL,
                               p_count                 IN NUMBER                                       DEFAULT 1,
                               p_rel_doc_pk            IN owa_util.ident_arr,
                               p_del_doc_pk            IN owa_util.ident_arr
)
IS
BEGIN
DECLARE
--                               p_rel_doc_pk            IN owa_util.ident_arr
v_service_pk            service.service_pk%TYPE                 DEFAULT NULL;
v_publish_date          document.publish_date%TYPE              DEFAULT NULL;
v_publish_date2         document.publish_date%TYPE              DEFAULT NULL;
v_heading               document.heading%TYPE                   DEFAULT NULL;
v_ingress               document.ingress%TYPE                   DEFAULT NULL;
v_file_path             document.file_path%TYPE                 DEFAULT NULL;
v_document_name         document.document_name%TYPE             DEFAULT NULL;
v_document_pk           document.document_pk%TYPE               DEFAULT NULL;
v_name                  organization.name%TYPE                  DEFAULT NULL;
v_count                 NUMBER                                  DEFAULT NULL;
v_count2                NUMBER                                  DEFAULT NULL;
v_count3                NUMBER                                  DEFAULT NULL;
v_doc_arc_nr            NUMBER                                  DEFAULT NULL;
v_organization_pk       organization.organization_pk%TYPE       DEFAULT NULL;
v_language_pk           document.language_fk%TYPE               DEFAULT NULL;

BEGIN

IF (login.timeout('admin.related_docs') > 0 ) THEN

   v_organization_pk := get.oid;

   IF ( v_organization_pk IS NULL ) THEN
      html.jump_to('main_page.startup',2,'Not a client user!');
   ELSE

      FOR v_count IN 1 .. p_del_doc_pk.COUNT
      LOOP
         IF ( p_del_doc_pk(v_count) > 0 ) THEN
            DELETE FROM relation_d2d
              WHERE  document_fk_1 = p_document_pk
              AND    document_fk_2 = p_del_doc_pk(v_count)
              ;
            COMMIT;
         END IF;
      END LOOP;

      FOR v_count IN 1 .. p_rel_doc_pk.COUNT
      LOOP
         IF ( p_rel_doc_pk(v_count) > 0 ) THEN
            INSERT INTO relation_d2d
              ( document_fk_1, document_fk_2 )
              VALUES
              ( p_document_pk, p_rel_doc_pk(v_count) );
            COMMIT;
         END IF;
      END LOOP;

      html.jump_to('admin.startup');

   END IF;

END IF;
END;
END related_docs2;


---------------------------------------------------------------------
-- Name: tip_user
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 30.08.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE tip_user (    p_email_address         IN VARCHAR2                     DEFAULT NULL,
                        p_organization_pk       IN VARCHAR2                     DEFAULT NULL,
                        p_command               IN VARCHAR2                     DEFAULT NULL
                )
IS
BEGIN
DECLARE

v_file          utl_file.file_type              default NULL;
v_number        NUMBER                          DEFAULT NULL;
v_message       VARCHAR2(4000)                  DEFAULT NULL;
v_email_dir     VARCHAR2(500)                   DEFAULT NULL;
v_name          organization.name%TYPE          DEFAULT NULL;
v_email         VARCHAR2(500)                   default NULL;

BEGIN
IF ( get.oid IS NULL ) THEN
        html.b_page(NULL,3,'/',2);
        html.b_table;
        htp.p('<tr><td>'||get.txt('not_org_user')||'</td></tr>');
        html.e_table;
        html.e_page;
ELSIF ( p_email_address IS NULL AND p_command IS NULL ) THEN
        html.b_page(NULL,NULL,NULL,2);
        html.b_form('admin.tip_user');
        html.b_box( get.txt('tip_user') , '100%', 'admin.tip_user');
        html.b_table;
        htp.p('<tr><td colspan="2">'|| get.txt('tip_message') ||'</td></tr>
        <td><b>'|| get.txt('rec_email_address') ||': </b>
        '|| html.text('p_email_address', '20', '50', '') ||'
        </td><td>
        <input type="hidden" name="p_organization_pk" value="'||get.oid||'">
        <input type="hidden" name="p_command" value="send">');
        html.submit_link( get.txt('tip_user') );
        htp.p('</td></tr>');
        html.e_table;
        html.e_form;
        html.e_box;
        html.e_page;
ELSIF ( p_email_address IS NULL) THEN
        html.b_page(NULL,3,owa_util.get_cgi_env('HTTP_REFERER'),2);
        html.b_table;
        htp.p('<tr><td>'||get.txt('error_email_address_missing')||'</td></tr>');
        html.e_table;
        html.e_page;
ELSE
        html.b_page(NULL,3,owa_util.get_cgi_env('HTTP_REFERER'),2);
        html.b_table;
        htp.p('<tr><td>'||get.txt('tip_email_send')||'</td></tr>');
        html.e_table;
        html.e_page;
        SELECT  email_script_seq.NEXTVAL
        INTO    v_number
        FROM    DUAL;

        SELECT  name
        INTO    v_name
        FROM    organization
        WHERE   organization_pk = get.oid
        ;

        v_email := get.oemail(p_organization_pk);
        if (v_email IS NULL) then
                v_email := get.value('email_info');
        end if;

        v_message := get.txt('org_email_tip');
        v_message := REPLACE ( v_message, '[org_name]', v_name );
        v_message := REPLACE ( v_message, '[subscribe_link]', get.olink(p_organization_pk,'subscribe.on_org?p_organization_pk='||p_organization_pk) );
        v_message := REPLACE ( v_message, '[org_email]', v_email );
        v_email_dir := get.value('email_dir');
        v_file := utl_file.fopen( v_email_dir, v_number||'_email-list.txt' , 'w');
        utl_file.put_line(v_file, p_email_address);
        utl_file.fclose(v_file);
        v_file := utl_file.fopen( v_email_dir, v_number||'_message.txt' , 'w');
        utl_file.put_line(v_file, v_message );
        utl_file.fclose(v_file);

END IF;
END;
END tip_user;


---------------------------------------------------------------------
-- Name: edit_fora
-- Type: procedure
-- What: Genererer dokumentarkiv
-- Made: Espen Messel
-- Date: 30.07.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE edit_fora (   p_command               IN varchar2                             DEFAULT NULL,
                        p_name                  IN fora_type.name%TYPE                  DEFAULT NULL,
                        p_description           IN fora_type.description%TYPE           DEFAULT NULL,
                        p_language_pk           IN fora_type.language_fk%TYPE           DEFAULT NULL,
                        p_fora_type_pk          IN fora_type.fora_type_pk%TYPE          DEFAULT NULL,
                        p_activated             IN fora_type.activated%TYPE             DEFAULT NULL
                         )
IS
BEGIN
DECLARE

CURSOR  get_fora_type ( v_org_pk in number ) IS
SELECT  fora_type_pk, name, description, language_fk,
        activated
FROM    fora_type
WHERE   organization_fk = v_org_pk
ORDER BY name
;

v_fora_type_pk          fora_type.fora_type_pk%TYPE             DEFAULT NULL;
v_name                  fora_type.name%TYPE                     DEFAULT NULL;
v_org_name              organization.name%TYPE                  DEFAULT NULL;
v_description           fora_type.description%TYPE              DEFAULT NULL;
v_language_pk           fora_type.language_fk%TYPE              DEFAULT NULL;
v_organization_pk       fora_type.organization_fk%TYPE          DEFAULT NULL;
v_activated             fora_type.activated%TYPE                DEFAULT NULL;

BEGIN

IF (    login.timeout('admin.edit_fora') > 0 ) THEN
        v_organization_pk := get.oid;
        IF ( v_organization_pk IS NULL ) THEN
                html.b_page(NULL,3,'/',2);
                html.b_table;
                htp.p('<tr><td>'||get.txt('not_org_user')||'</td></tr>');
                html.e_table;
                html.e_page;
        ELSE
                IF ( p_command = 'activate' ) THEN
                        UPDATE  fora_type
                        SET     activated=p_activated
                        WHERE   fora_type_pk=p_fora_type_pk
                        AND     organization_fk = v_organization_pk;
                        commit;
                END IF;
                IF ( p_command = 'update') THEN
                        SELECT  fora_type_pk, name, description, language_fk
                        INTO    v_fora_type_pk, v_name, v_description, v_language_pk
                        FROM    fora_type
                        WHERE   fora_type_pk=p_fora_type_pk
                        AND     organization_fk = v_organization_pk
                        ;
                        commit;
                        html.b_page(NULL,NULL,NULL,2);
                        html.b_box( get.txt('fora_type'), '100%', 'admin.edit_fora-update' );
                        html.b_table;
                        html.b_form('admin.edit_fora');
                        htp.p('<input type="hidden" name="p_command" value="edit">
                        <input type="hidden" name="p_fora_type_pk" value="'||v_fora_type_pk||'">
                        <tr><td>'|| get.txt('language') ||':</td><td>');
                        html.select_lang(v_language_pk);
                        htp.p('</td></tr>
                        <tr><td>'|| get.txt('organization') ||':</td><td>
                        '||get.oname(v_organization_pk)||'
                        </td></tr>
                        <tr><td>'|| get.txt('name') ||':</td><td>
                        '|| html.text('p_name', '50', '50', v_name ) ||'
                        </td></tr>
                        <tr><td>'|| get.txt('description') ||':(maks 150 tegn)</td><td>
                        '|| html.text('p_description', '50', '150', v_description ) ||'
                        </td></tr>
                        <tr><td colspan="2">'); html.submit_link( get.txt('update') ); htp.p('</td></tr>');
                        html.e_form;
                        html.e_table;
                        html.e_box;
                        html.e_page;
                ELSIF ( p_command = 'edit') THEN
                        UPDATE  fora_type
                        SET     name=p_name,
                                description=p_description,
                                language_fk=p_language_pk
                        WHERE   fora_type_pk=p_fora_type_pk
                        AND     organization_fk = v_organization_pk;
                        commit;
                        html.jump_to ( 'admin.edit_fora' );
                ELSIF ( p_command = 'delete') THEN
                        IF ( p_name = 'yes') THEN
                                DELETE  FROM fora_type
                                WHERE   fora_type_pk=p_fora_type_pk
                                AND     organization_fk = v_organization_pk;
                                commit;
                                html.jump_to ( 'admin.edit_fora');
                        ELSIF ( p_name IS NULL ) THEN
                                html.jump_to ( 'admin.edit_fora');
                        ELSE
                                html.b_page(NULL,NULL,NULL,2);
                                html.b_box( get.txt('fora_type') , '100%', 'admin.edit_fora-delete');
                                html.b_table;
                                html.b_form('admin.edit_fora');
                                htp.p('<input type="hidden" name="p_command" value="delete">
                                <input type="hidden" name="p_name" value="yes">
                                <input type="hidden" name="p_fora_type_pk" value="'||p_fora_type_pk||'">
                                <td>'|| get.txt('delete_info')||' "'||p_name||'" ?'); html.submit_link( get.txt('YES') ); htp.p('</td>');
                                html.e_form;
                                htp.p('<td>'); html.button_link( get.txt('NO'), 'admin.edit_fora');htp.p('</td></tr>');
                                html.e_table;
                                html.e_box;
                                html.e_page;
                        END IF;
                ELSIF ( p_command = 'new') THEN
                        html.b_page(NULL,NULL,NULL,2);
                        html.b_box( get.txt('fora_type'), '100%', 'admin.edit_fora-new' );
                        html.b_table;
                        html.b_form('admin.edit_fora','form_insert');
                        htp.p('<input type="hidden" name="p_command" value="insert">
                        <tr><td>'|| get.txt('language') ||':</td><td>');
                        html.select_lang(get.lan,get.oserv(v_organization_pk));
                        htp.p('</td></tr>
                        <tr><td>'|| get.txt('organization') ||':</td><td>
                        '||get.oname(v_organization_pk)||'
                        </td></tr>
                        <tr><td>'|| get.txt('name') ||':</td><td>
                        '|| html.text('p_name', '50', '50', '' ) ||'
                        </td></tr>
                        <tr><td>'|| get.txt('description') ||':(maks 150 tegn)</td><td>
                        '|| html.text('p_description', '50', '150', '' ) ||'
                        </td></tr>
                        <tr><td colspan="2">'); html.submit_link( get.txt('insert'), 'form_insert' ); htp.p('</td></tr>');
                        html.e_form;
                        html.e_table;
                        html.e_box;
                        html.e_page;
                ELSIF ( p_command = 'insert') THEN
                        v_name := SUBSTR(p_name,1,50);
                        v_description := SUBSTR(p_description,1,150);
                        INSERT INTO fora_type
                        ( fora_type_pk, name, description, language_fk, organization_fk )
                        VALUES
                        ( fora_type_seq.NEXTVAL, v_name, v_description, p_language_pk, v_organization_pk );
                        commit;
                        html.jump_to ( 'admin.edit_fora' );
                ELSE
                        html.b_page(NULL,NULL,NULL,2);
                        html.b_box( get.txt('fora_type'), '100%', 'admin.edit_fora' );
                        html.b_table;
                        htp.p('<tr><td colspan="5">'||get.txt('edit_fora_help')||'</td></tr>
                        <tr><td colspan="5"><HR size="2" noshade="noshade"></td></tr>');
                        OPEN get_fora_type( v_organization_pk );
                        LOOP
                                FETCH get_fora_type INTO v_fora_type_pk, v_name, v_description,
                                                         v_language_pk, v_activated;
                                exit when get_fora_type%NOTFOUND;

                                htp.p('<tr><td>'||v_name||'</td>
                                <td>'||get.oname(v_organization_pk)||'</td>');

                                html.b_form('admin.edit_fora','form'|| v_fora_type_pk);
                                htp.p('<input type="hidden" name="p_command" value="activate">
                                <input type="hidden" name="p_fora_type_pk" value="'||v_fora_type_pk||'">
                                <td>');
                                IF ( v_activated > 0 ) THEN
                                        htp.p('<input type="hidden" name="p_activated" value="0">');
                                        html.submit_link( get.txt('deactivate'), 'form'||v_fora_type_pk );
                                ELSE
                                        htp.p('<input type="hidden" name="p_activated" value="1">');
                                        html.submit_link( get.txt('activate'), 'form'||v_fora_type_pk );
                                END IF;
                                htp.p('</td>');
                                html.e_form;

                                html.b_form('admin.edit_fora','form'|| v_fora_type_pk);
                                htp.p('<input type="hidden" name="p_command" value="update">
                                <input type="hidden" name="p_fora_type_pk" value="'||v_fora_type_pk||'">
                                <td>'); html.submit_link( get.txt('edit'), 'form'||v_fora_type_pk ); htp.p('</td>');
                                html.e_form;
                                html.b_form('admin.edit_fora','delete'|| v_fora_type_pk);
                                htp.p('<input type="hidden" name="p_command" value="delete">
                                <input type="hidden" name="p_name" value="'||v_name||'">
                                <input type="hidden" name="p_fora_type_pk" value="'||v_fora_type_pk||'">
                                <td>'); html.submit_link( get.txt('delete'), 'delete'||v_fora_type_pk ); htp.p('</td></tr>');
                                html.e_form;
                        END LOOP;
                        CLOSE get_fora_type;
                        html.b_form('admin.edit_fora','form_new');
                        htp.p('<input type="hidden" name="p_command" value="new">
                        <td>'); html.submit_link( get.txt('new'), 'form_new'); htp.p('</td></tr>');
                        html.e_form;
                        html.e_table;
                        html.e_box;
                        html.e_page;
                END IF;
        END IF;
END IF;
EXCEPTION
WHEN NO_DATA_FOUND
THEN
htp.p('No data found! edit_reg_date.sql -> edit_fora');
END;
END edit_fora;




-- ++++++++++++++++++++++++++++++++++++++++++++++ --
end; -- slutter pakke kroppen
/
show errors;
