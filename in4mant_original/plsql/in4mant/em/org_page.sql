set define off
PROMPT *** package: ORG_PAGE ***


CREATE OR REPLACE PACKAGE org_page IS

PROCEDURE startup;
PROCEDURE main (        p_organization_pk       IN organization.organization_pk%TYPE    DEFAULT NULL);
PROCEDURE address(      p_organization_pk       IN organization.organization_pk%TYPE    DEFAULT NULL);
PROCEDURE cal(          p_organization_pk       IN calendar.organization_fk%TYPE        DEFAULT NULL,
                        p_language_pk           IN calendar.language_fk%TYPE            DEFAULT NULL);
PROCEDURE doc(          p_organization_pk       IN organization.organization_pk%TYPE    DEFAULT NULL,
                        p_language_pk           IN document.language_fk%TYPE            DEFAULT NULL );
PROCEDURE org_select;
PROCEDURE info(         p_organization_pk       IN organization.organization_pk%TYPE    DEFAULT NULL,
                        p_language_pk           IN document.language_fk%TYPE            DEFAULT NULL );
PROCEDURE other (       p_organization_pk       IN organization.organization_pk%TYPE    DEFAULT NULL );
PROCEDURE more_info(    p_organization_pk       IN organization.organization_pk%TYPE    DEFAULT NULL,
                        p_language_pk           IN document.language_fk%TYPE            DEFAULT NULL );
PROCEDURE more_info2(   p_organization_pk       IN organization.organization_pk%TYPE    DEFAULT NULL,
                        p_language_pk           IN document.language_fk%TYPE            DEFAULT NULL );
PROCEDURE sub_page(     p_organization_pk       IN organization.organization_pk%TYPE    DEFAULT NULL );

END;
/
CREATE OR REPLACE PACKAGE BODY org_page IS

---------------------------------------------------------------------
---------------------------------------------------------------------
-- Name: startup
-- Type: procedure
-- What: Genererer opp hovedsiden for en organisasjon
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE startup
IS
BEGIN
        org_select;
END startup;

---------------------------------------------------------------------
-- Name: main
-- Type: procedure
-- What: Genererer opp hovedsiden for admin av en organisasjon
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE main(         p_organization_pk               IN organization.organization_pk%TYPE    DEFAULT NULL
                                )
IS
BEGIN
DECLARE
        v_language_pk   la.language_pk%TYPE     DEFAULT NULL;
BEGIN
        v_language_pk := get.lan;
        stat.reg(p_organization_pk,NULL,NULL,NULL,NULL,NULL,19);
        html.b_page(NULL,NULL,NULL,NULL,3);
        html.org_menu(p_organization_pk);

        htp.p('<table cellspacing="0" cellpadding="10">');
        htp.p('<tr><td valign="top" width="30%" align="left">');

        -- ----------------
        -- venstre stolpe
        -- ----------------

        htp.p('<br>');

        address(p_organization_pk);

        htp.p('<br><br>');

        fora_util.all_fora(get.serv,v_language_pk,p_organization_pk);

        htp.p('<br><br>');

        link_list.show_links(p_organization_pk);

        htp.p('<br><br>');

        other(p_organization_pk);

        htp.p('<br><br>');

        htp.p('</td><td valign="top" width="490">');

        -- --------------
        -- h�yre stolpe
        -- --------------

        htp.p('<br>');

        info(p_organization_pk,v_language_pk);

        cal(p_organization_pk,v_language_pk);

        doc(p_organization_pk,v_language_pk);

        photo.user_last_pictures(p_organization_pk);

        photo.user_list_albums(p_organization_pk,1);

        photo.user_last_comment(p_organization_pk);

        htp.p('<br><br><br><br>');


        -- -------------
        -- bunn
        -- -------------

        htp.p('</td></tr><tr><td colspan="2">');

        tip.someone;

        htp.p('<br><br><br><br>');
        htp.p('</td></tr></table>');
        html.e_page;
END;
END main;

---------------------------------------------------------------------
-- Name: sub_page
-- Type: procedure
-- What: Genererer opp hovedsiden for admin av en organisasjon
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE sub_page(     p_organization_pk               IN organization.organization_pk%TYPE    DEFAULT NULL
                                )
IS
BEGIN
DECLARE
        v_language_pk   la.language_pk%TYPE     DEFAULT NULL;
BEGIN
        v_language_pk := get.lan;
        stat.reg(p_organization_pk,NULL,NULL,NULL,NULL,NULL,19);
        html.b_page(NULL,NULL,NULL,NULL,3);
        html.org_menu(p_organization_pk);
        htp.p('<table cellspacing="0" cellpadding="10">');
        htp.p('<tr><td valign="top" width="25%" align="left">');

        -- Venstre stolpe
        html.b_table;
        htp.p('<tr><td valign="top">');
        address(p_organization_pk);
        htp.p('<br></td></tr>
        <tr><td valign="top">');
        more_info2(p_organization_pk,v_language_pk);
        htp.p('<br></td></tr>');
        html.e_table;
        htp.p('</td><td valign="top">');
        -- H�yre stolpe
        html.b_table;
        htp.p('<tr><td valign="top">');
        more_info(p_organization_pk,v_language_pk);
        htp.p('</td></tr>');
        html.e_table;
        htp.p('</td></tr>');
        html.e_table;
        html.e_page;
END;
END sub_page;


---------------------------------------------------------------------
-- Name: address
-- Type: procedure
-- What: Genererer opp hovedsiden for admin av en organisasjon
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE address(      p_organization_pk               IN organization.organization_pk%TYPE    DEFAULT NULL
                                )
IS
BEGIN
DECLARE
v_address       organization.address%TYPE       DEFAULT NULL;
v_url           organization.url%TYPE           DEFAULT NULL;
v_email         organization.email%TYPE         DEFAULT NULL;
v_phone         organization.phone%TYPE         DEFAULT NULL;
v_fax           organization.fax%TYPE           DEFAULT NULL;
v_name_sg_fk    list.name_sg_fk%TYPE            DEFAULT NULL;
v_org_number    organization.org_number%TYPE    DEFAULT NULL;
v_geography_pk  organization.geography_fk%TYPE  DEFAULT NULL;

CURSOR  get_address IS
SELECT  address, url, email, phone, fax, org_number, geography_fk
FROM    organization
WHERE   organization_pk=p_organization_pk
;

CURSOR  get_lists IS
SELECT  distinct(name_sg_fk), url
FROM    list
WHERE   level1 in (
SELECT  distinct(l.level1)
FROM    list l, list_org lo
WHERE   lo.organization_fk=p_organization_pk
AND     lo.list_fk = l.list_pk
AND     name_sg_fk IS NOT NULL
AND     l.url IS NOT NULL
      )
AND     level2 IS NULL
AND     level3 IS NULL
AND     level4 IS NULL
AND     level5 IS NULL
;
--ORDER BY level5 DESC, level4 DESC, level3 DESC, level2 DESC, level1 DESC, level0 DESC

BEGIN
        OPEN get_address;
        fetch get_address into v_address, v_url, v_email, v_phone, v_fax, v_org_number, v_geography_pk;
        close get_address;
        html.b_box_2(get.oname(p_organization_pk),'100%', 'org_page.address');
        html.b_table;
        htp.p('<tr><td colspan=2>'||html.rem_tag(v_address)||'</td></tr>');
        IF ( v_geography_pk IS NOT NULL ) THEN
           htp.p('<tr><td colspan=2>'||get.txt('geography_location')||':</td></tr>
                 <tr><td colspan=2>'||get.locn(v_geography_pk)||'</td></tr>');
        END IF;
        IF ( v_org_number IS NOT NULL ) THEN
           htp.p('<tr><td colspan=2>'||get.txt('org_number')||':</td></tr>
                 <tr><td colspan=2>'||v_org_number||'</td></tr>');
        END IF;
        IF ( v_url IS NOT NULL ) THEN
           htp.p('<tr><td colspan=2>'||get.txt('url')||':</td></tr>
                 <tr><td colspan=2><a href="http://'||replace(v_url,'http://','')||'" target="new">http://'||replace(v_url,'http://','')||'</a></td></tr>');
        END IF;
        IF ( v_email IS NOT NULL ) THEN
           htp.p('<tr><td colspan=2>'||get.txt('email')||':</td></tr>
                   <tr><td colspan=2><a href="tip.mailform?p_contact_email='||v_email||'&p_url=org_page.main?p_organization_pk='||p_organization_pk||'">'||v_email||'</a></td></tr>');
        END IF;
        IF ( v_phone IS NOT NULL ) THEN
           htp.p('<tr><td>'||get.txt('phone')||':</td><td>'||v_phone||'</td></tr>');
        END IF;
        IF ( v_fax IS NOT NULL ) THEN
           htp.p('<tr><td>'||get.txt('fax')||':</td><td>'||v_fax||'</td></tr>');
        END IF;

        OPEN get_lists;
        LOOP
           FETCH get_lists INTO v_name_sg_fk, v_url;
           EXIT WHEN get_lists%notfound;
           IF ( get_lists%ROWCOUNT = 1 ) THEN
              htp.p('<tr><td><br>'||get.txt('on_list')||'</td></tr>');
           END IF;
           htp.p('<tr><td colspan="2"><a href="'|| v_url ||'" target="new">'|| get.text(v_name_sg_fk) ||'</a></td></tr>');
        END LOOP;
        close get_lists;

        html.e_table;
        html.e_box_2;
END;
END address;

---------------------------------------------------------------------
-- Name: cal
-- Type: procedure
-- What: Genererer opp hovedsiden for admin av en organisasjon
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE cal(  p_organization_pk       IN calendar.organization_fk%TYPE        DEFAULT NULL,
                p_language_pk           IN calendar.language_fk%TYPE            DEFAULT NULL)
IS
BEGIN
DECLARE
v_calendar_pk           calendar.calendar_pk%TYPE       DEFAULT NULL;
v_heading               calendar.heading%TYPE           DEFAULT NULL;
v_event_date            calendar.event_date%TYPE        DEFAULT NULL;
v_message               calendar.message%TYPE           DEFAULT NULL;
v_language_pk           calendar.language_fk%TYPE       DEFAULT NULL;
v_organization_pk       calendar.organization_fk%TYPE   DEFAULT NULL;
v_name                  organization.name%TYPE          DEFAULT NULL;
v_logo                  organization.logo%TYPE          DEFAULT NULL;
v_path                  VARCHAR2(100)                   DEFAULT NULL;
v_to_day                date                            DEFAULT NULL;
v_end_date              calendar.end_date%TYPE          DEFAULT NULL;


CURSOR  get_calendar IS
SELECT  c.calendar_pk, c.heading, c.event_date, c.end_date, sysdate as to_day
FROM    calendar c, organization o
WHERE   c.organization_fk = p_organization_pk
AND     c.language_fk = p_language_pk
AND     ( c.event_date > sysdate-(1/24) OR c.end_date > sysdate-(1/24) )
AND     c.organization_fk = o.organization_pk
AND     o.accepted>=1
ORDER BY event_date
;

BEGIN

OPEN get_calendar;
LOOP
        fetch get_calendar into v_calendar_pk, v_heading, v_event_date, v_end_date, v_to_day;
        exit when get_calendar%NOTFOUND;

        -- Skriver ut topp
        IF ( get_calendar%ROWCOUNT = 1 ) THEN
           htp.p('<br><br><br><br>');
           html.b_box(get.txt('calendar'),'100%','cal_util.show_calendar','cal_util.cal_archive?p_organization_pk='||p_organization_pk||'&p_language_pk='||p_language_pk);
           html.b_table;
           htp.p('<tr><td align="left" width="100"><b>'||get.txt('date')||':</b></td><td width="390"><b>'||get.txt('happening')||':</b></td></tr>');
        END IF;

        IF ( to_char(v_event_date,'YYYYMMDD') <= to_char(v_to_day,'YYYYMMDD') AND
           ( v_end_date >= sysdate )) THEN
             htp.p('<tr><td valign="top" nowrap>'||to_char(v_to_day,get.txt('date_short_day_2'))||':</td>');
        ELSE
             htp.p('<tr><td valign="top" nowrap>'||to_char(v_event_date,get.txt('date_short_day'))||':</td>');
        END IF;

        htp.p('<td align="left"><a href="cal_util.show_calendar?p_calendar_pk='||v_calendar_pk||'">'||v_heading||'</a></td></tr>');
        exit when get_calendar%ROWCOUNT = get.value('cal_nr');
END LOOP;
IF ( get_calendar%ROWCOUNT > 0 ) THEN
   html.e_table;
   html.e_box;
END IF;
close get_calendar;

EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
        html.jump_to('/');
END;
END cal;


---------------------------------------------------------------------
-- Name: org_select
-- Type: procedure
-- What: Genererer opp hovedsiden for admin av en organisasjon
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE org_select
IS
BEGIN
DECLARE
v_organization_pk       organization.organization_pk%TYPE       DEFAULT NULL;
v_name                  organization.name%TYPE                  DEFAULT NULL;
v_service_pk            service.service_pk%TYPE                 DEFAULT NULL;

CURSOR  get_org IS
SELECT  organization_pk, name
FROM    organization o, org_service os
WHERE   os.organization_fk=o.organization_pk
AND     os.service_fk=v_service_pk
ORDER BY name;

BEGIN
        v_service_pk:=get.serv;
        IF ( v_service_pk = 0 ) THEN
                v_service_pk := 1;
        END IF;
        html.b_page_2;
        html.b_box(get.txt('Select_organization '),200);
        html.b_table(200);
        OPEN get_org;
        LOOP
                fetch get_org into v_organization_pk, v_name;
                exit when get_org%NOTFOUND;
                htp.p('<tr><td><a href="org_page.main?p_organization_pk='||v_organization_pk||'">'||v_name||'</a></td></tr>');
        END LOOP;
        close get_org;
        html.e_table;
        html.e_box;
        html.e_page;
END;
END org_select;


---------------------------------------------------------------------
-- Name: doc
-- Type: procedure
-- What: Genererer opp hovedsiden for admin av en organisasjon
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE doc(  p_organization_pk       IN organization.organization_pk%TYPE    DEFAULT NULL,
                p_language_pk           IN document.language_fk%TYPE            DEFAULT NULL )
IS
BEGIN
DECLARE

v_document_pk           document.document_pk%TYPE       DEFAULT NULL;
v_publish_date          document.publish_date%TYPE      DEFAULT NULL;
v_heading               document.heading%TYPE           DEFAULT NULL;
v_ingress               document.ingress%TYPE           DEFAULT NULL;

CURSOR  get_doc IS
SELECT  d.document_pk, d.publish_date, d.heading, d.ingress
FROM    document d, organization o
WHERE   d.organization_fk=o.organization_pk
AND     d.organization_fk=p_organization_pk
AND     d.language_fk=p_language_pk
AND     o.accepted >= 1
AND     d.accepted > 0
AND     d.publish_date < SYSDATE
ORDER BY d.publish_date DESC;

BEGIN

OPEN get_doc;
LOOP
        fetch get_doc into v_document_pk, v_publish_date, v_heading, v_ingress;
        exit when get_doc%NOTFOUND;

        IF ( get_doc%ROWCOUNT = 1 ) THEN
           htp.p('<br><br><br><br>');
           html.b_box(get.txt('latest_news'),'100%','org_page.doc','doc_util.doc_archive?p_organization_pk='||p_organization_pk||'&p_language_pk='||p_language_pk);
           html.b_table('100%');
           htp.p('<tr><td align="left" width="100"><b>'||get.txt('date')||':</b></td><td width="390"><b>'||get.txt('subject')||':</b></td></tr>');
        END IF;

        htp.p('<tr><td valign="top">'||to_char(v_publish_date,get.txt('date_long'))||'</td>
        <td align="left"><b><a href="doc_util.show_document?p_document_pk='||v_document_pk||'">
        '||html.rem_tag(v_heading)||'</a></b></td></tr>
        ');
        exit when get_doc%ROWCOUNT = get.value('doc_nr');
END LOOP;
IF ( get_doc%ROWCOUNT > 0 ) THEN
   html.e_table;
   html.e_box;
END IF;
close get_doc;

END;
END doc;



---------------------------------------------------------------------
-- Name: info
-- Type: procedure
-- What: Genererer opp hovedsiden for admin av en organisasjon
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE info(         p_organization_pk       IN organization.organization_pk%TYPE    DEFAULT NULL,
                        p_language_pk           IN document.language_fk%TYPE            DEFAULT NULL )
IS
BEGIN
DECLARE

v_logo          organization.logo%TYPE          DEFAULT NULL;
v_value         org_info.value%TYPE             DEFAULT NULL;
v_count         NUMBER                          DEFAULT NULL;
v_img           varchar2(256)                   DEFAULT '';
v_name          organization.name%TYPE          DEFAULT NULL;
v_accepted      organization.accepted%TYPE      DEFAULT NULL;

CURSOR  get_org_info IS
SELECT  oi.value, o.name
FROM    organization o, org_info oi, info_name inn
WHERE   o.organization_pk=p_organization_pk
AND     o.organization_pk=oi.organization_fk
AND     oi.info_name_fk = inn.info_name_pk
AND     inn.service_fk = get.serv
AND     inn.description like 'Description_short%'
AND     accepted>=1
;

CURSOR  get_logo IS
SELECT  logo, accepted
FROM    organization
WHERE   organization_pk=p_organization_pk
;

BEGIN
        html.b_box(get.txt('info'),'100%','org_page.main','org_page.sub_page?p_organization_pk='||p_organization_pk);
        html.b_table('100%');

        OPEN get_org_info;
        FETCH get_org_info INTO v_value, v_name;
        CLOSE get_org_info;

        OPEN get_logo;
        FETCH get_logo INTO v_logo, v_accepted;
        CLOSE get_logo;

        SELECT name INTO v_name FROM organization WHERE organization_pk = p_organization_pk;


        IF ( v_logo IS NULL ) THEN
           htp.p('<tr><td align="center"><h1>'||v_name||'</h1></td></tr>');
        ELSE
           htp.p('<tr><td align="center"><img src="'||get.value('org_logo_dir')||'/'||v_logo||'" border="0"></td></tr>');
        END IF;

        IF ( v_accepted > 1 ) THEN
           v_img := ' height="80" width="100%" style="background-image: url(/images/closed_1.gif); background-repeat: no-repeat;background-position: center;"';
        END IF;

        htp.p('<tr><td><hr noshade size="1"></td></tr>
        <tr><td'||v_img||'>'||html.rem_tag(v_value)||'</td></tr>');

        SELECT COUNT(*)
        INTO   v_count
        FROM   org_info
        WHERE  organization_fk=p_organization_pk
        ;

        IF ( ( v_count > 1 AND v_value IS NOT NULL ) OR ( v_count >= 1 AND v_value IS NULL ) ) THEN
             htp.p('<tr><td><a href="org_page.sub_page?p_organization_pk='||p_organization_pk||'">'||get.txt('more_org_info')||'</a></td></tr>');
        END IF;
        html.e_table;
        html.e_box;

END;
END info;

---------------------------------------------------------------------
-- Name: other
-- Type: procedure
-- What: Genererer opp ett element til hovedsiden for en organisasjon
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE other (       p_organization_pk       IN organization.organization_pk%TYPE    DEFAULT NULL )
IS
BEGIN
html.b_box_2(get.txt('other'),'100%', 'org_page.other');
html.b_table;

        if ( get.has_menu(p_organization_pk) ) then
                htp.p('
                        <tr><td><a href="menu_user.view_menu?p_organization_pk='||p_organization_pk||'">
                        '||get.txt('menu_user')||'</a></td></tr>
                ');
        end if;

        htp.p('
                <tr><td><a href="subscribe.on_org?p_organization_pk='||p_organization_pk||'">
                '||get.txt('subscribe_to_messages')||'</a></td></tr>
        ');

html.e_table;
html.e_box_2;

END other;


---------------------------------------------------------------------
-- Name: more_info
-- Type: procedure
-- What: Genererer opp hovedsiden for admin av en organisasjon
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE more_info(    p_organization_pk       IN organization.organization_pk%TYPE    DEFAULT NULL,
                        p_language_pk           IN document.language_fk%TYPE            DEFAULT NULL )
IS
BEGIN
DECLARE

v_logo          organization.logo%TYPE          DEFAULT NULL;
v_value         org_info.value%TYPE             DEFAULT NULL;

/*
CURSOR  get_org_info IS
SELECT  oi.value
FROM    organization o, org_info oi
WHERE   o.organization_pk=p_organization_pk
AND     o.organization_pk=oi.organization_fk
AND     oi.info_name_fk = 4
AND     accepted=1
;
*/

CURSOR  get_org_info IS
SELECT  oi.value
FROM    organization o, org_info oi, info_name inn
WHERE   o.organization_pk=p_organization_pk
AND     o.organization_pk=oi.organization_fk
AND     oi.info_name_fk = inn.info_name_pk
AND     inn.service_fk = get.serv
AND     inn.description like 'Description_long%'
AND     accepted>=1
;

CURSOR  get_logo IS
SELECT  logo
FROM    organization
WHERE   organization_pk=p_organization_pk
;

BEGIN
        html.b_box(get.txt('info'),500,'org_page.more_info','org_page.main?p_organization_pk='||p_organization_pk);
        html.b_table(500);

        OPEN get_org_info;
        FETCH get_org_info into v_value;
        CLOSE get_org_info;

        OPEN get_logo;
        FETCH get_logo into v_logo;
        CLOSE get_logo;

        IF ( v_logo IS NULL ) THEN
           v_logo := 'no_logo.gif';
        END IF;

        htp.p('<tr><td align="center"><img src="'||get.value('org_logo_dir')||'/'||v_logo||'" border="0"></td></tr>
        <tr><td><hr noshade size="1"></td></tr>
        <tr><td>'||html.rem_tag(v_value)||'</td></tr>');
        html.e_table;
        html.e_box;

END;
END more_info;


---------------------------------------------------------------------
-- Name: more_info2
-- Type: procedure
-- What: Genererer opp hovedsiden for admin av en organisasjon
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE more_info2(   p_organization_pk       IN organization.organization_pk%TYPE    DEFAULT NULL,
                        p_language_pk           IN document.language_fk%TYPE            DEFAULT NULL )
IS
BEGIN
DECLARE

v_value         org_info.value%TYPE             DEFAULT NULL;
v_name_sg_fk    info_name.name_sg_fk%TYPE       DEFAULT NULL;

CURSOR  get_more_org_info IS
SELECT  oi.value, name_sg_fk
FROM    organization o, org_info oi, info_name i
WHERE   o.organization_pk=p_organization_pk
AND     o.organization_pk=oi.organization_fk
AND     oi.info_name_fk = i.info_name_pk
AND     i.description not like 'Description_%'
AND     o.accepted>=1
AND     oi.info_name_fk = i.info_name_pk
ORDER BY i.description, oi.value
;

BEGIN
        OPEN get_more_org_info;
        LOOP
                FETCH get_more_org_info into v_value, v_name_sg_fk;
                EXIT WHEN get_more_org_info%NOTFOUND;
                html.b_box_2(get.text(v_name_sg_fk),'100%','org_page.more_info2');
                html.b_table;
                htp.p('<tr><td>'||html.rem_tag_url(v_value)||'</td></tr>');
                html.e_table;
                html.e_box_2;
                htp.p('<br>');
        END LOOP;
        CLOSE get_more_org_info;
END;
END more_info2;



-- ++++++++++++++++++++++++++++++++++++++++++++++ --
END; -- slutter pakke kroppen
/
show errors;
