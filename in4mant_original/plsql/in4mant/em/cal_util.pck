CREATE OR REPLACE PACKAGE cal_util IS

PROCEDURE startup;

PROCEDURE cal_archive   (       p_organization_pk       IN organization.organization_pk%TYPE    DEFAULT NULL,
                                p_language_pk           IN document.language_fk%TYPE            DEFAULT NULL,
                                p_service_pk            IN service.service_pk%TYPE              DEFAULT NULL,
                                p_new_date              IN VARCHAR2                             DEFAULT to_char(SYSDATE,get.txt('date_long')),
                                p_old_date              IN VARCHAR2                             DEFAULT NULL,
                                p_count                 IN NUMBER                               DEFAULT 1,
                                p_my_cal                IN NUMBER                               DEFAULT NULL,
                                p_calendar_pk           IN calendar.calendar_pk%TYPE            DEFAULT 0
                        );
PROCEDURE show_calendar(        p_calendar_pk           IN calendar.calendar_pk%TYPE            DEFAULT NULL,
                                p_search                IN VARCHAR2                             DEFAULT NULL,
                                p_command               IN VARCHAR2                             DEFAULT NULL );

PROCEDURE cal_serv(             p_service_pk            IN service.service_pk%TYPE              DEFAULT NULL,
                                p_language_pk           IN calendar.language_fk%TYPE            DEFAULT NULL);

PROCEDURE whatsup(     p_start_date              IN VARCHAR2                                     DEFAULT NULL,
		       p_geography_pk		 IN geography.geography_pk%TYPE			 DEFAULT NULL);


END;
/
CREATE OR REPLACE PACKAGE BODY cal_util IS

---------------------------------------------------------------------
---------------------------------------------------------------------
-- Name: startup
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 15.07.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE startup
IS
BEGIN
        cal_archive;
END startup;

---------------------------------------------------------------------
-- Name: cal_archive
-- Type: procedure
-- What: Genererer dokumentarkiv
-- Made: Espen Messel
-- Date: 15.07.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE cal_archive   (       p_organization_pk       IN organization.organization_pk%TYPE            DEFAULT NULL,
                                p_language_pk           IN document.language_fk%TYPE                    DEFAULT NULL,
                                p_service_pk            IN service.service_pk%TYPE                      DEFAULT NULL,
                                p_new_date              IN VARCHAR2                                     DEFAULT to_char(SYSDATE,get.txt('date_long')),
                                p_old_date              IN VARCHAR2                                     DEFAULT NULL,
                                p_count                 IN NUMBER                                       DEFAULT 1,
                                p_my_cal                IN NUMBER                                       DEFAULT NULL,
                                p_calendar_pk           IN calendar.calendar_pk%TYPE                    DEFAULT 0
                        )
IS
BEGIN
DECLARE

        v_calendar_pk           calendar.calendar_pk%TYPE               DEFAULT NULL;
        v_organization_pk       organization.organization_pk%TYPE       DEFAULT NULL;
        v_language_pk           calendar.language_fk%TYPE               DEFAULT NULL;
        v_service_pk            service.service_pk%TYPE                 DEFAULT NULL;
        v_geography_pk          calendar.geography_fk%TYPE              DEFAULT NULL;
        v_event_date            calendar.event_date%TYPE                DEFAULT NULL;
        v_heading               calendar.heading%TYPE                   DEFAULT NULL;
        v_message               calendar.message%TYPE                   DEFAULT NULL;
        v_name                  organization.name%TYPE                  DEFAULT NULL;
        v_count                 NUMBER                                  DEFAULT NULL;
        v_count2                NUMBER                                  DEFAULT NULL;
        v_count3                NUMBER                                  DEFAULT NULL;
        v_cal_arc_nr            NUMBER                                  DEFAULT NULL;
        v_user_pk               usr.user_pk%TYPE                        DEFAULT NULL;

        CURSOR  calendar_archive IS
        SELECT  c.event_date, c.heading, c.calendar_pk, o.name, o.organization_pk
        FROM    calendar c, organization o, org_service os
        WHERE   c.language_fk=v_language_pk
        AND     c.organization_fk = p_organization_pk
        AND     c.organization_fk = o.organization_pk
        AND     c.event_date > to_date(trans(p_new_date),get.txt('date_long'))
        AND     o.accepted=1
        AND     os.service_fk=v_service_pk
        AND     c.organization_fk=os.organization_fk
        OR      c.language_fk=v_language_pk
        AND     c.organization_fk = p_organization_pk
        AND     c.organization_fk = o.organization_pk
        AND     c.event_date = to_date(trans(p_new_date),get.txt('date_long'))
        AND     o.accepted=1
        AND     os.service_fk=v_service_pk
        AND     c.organization_fk=os.organization_fk
        AND     c.calendar_pk > p_calendar_pk
        ORDER BY c.event_date, c.calendar_pk
        ;


        CURSOR  calendar_archive_all IS
        SELECT  c.event_date, c.heading, c.calendar_pk, o.name, o.organization_pk, o.geography_fk
        FROM    calendar c, organization o, org_service os
        WHERE   c.language_fk=v_language_pk
        AND     c.organization_fk = o.organization_pk
        AND     c.event_date > to_date(trans(p_new_date),get.txt('date_long'))
        AND     o.accepted=1
        AND     c.organization_fk=os.organization_fk
        AND     os.service_fk=v_service_pk
        OR      c.language_fk=v_language_pk
        AND     c.organization_fk = o.organization_pk
        AND     c.event_date = to_date(trans(p_new_date),get.txt('date_long'))
        AND     o.accepted=1
        AND     os.service_fk=v_service_pk
        AND     c.organization_fk=os.organization_fk
        AND     c.calendar_pk > p_calendar_pk
        ORDER BY c.event_date, c.calendar_pk
        ;


        CURSOR  my_calendar_archive(v_user_pk usr.user_pk%type) IS
        SELECT  c.event_date, c.heading, c.calendar_pk, o.name, o.organization_pk, o.geography_fk
        FROM    calendar c, organization o, org_service os
        WHERE   c.organization_fk = o.organization_pk
        AND     c.event_date > to_date(trans(p_new_date),get.txt('date_long'))
        AND     o.accepted=1
        AND     c.organization_fk=os.organization_fk
        AND     os.service_fk=v_service_pk
        AND     (o.organization_pk, c.language_fk) IN
                (
                        SELECT  DISTINCT organization_fk, lu.language_fk
                        FROM    list_org lo, list l, list_user lu, list_type lt
                        WHERE   lo.list_fk = l.list_pk
                        AND     lt.list_type_pk = l.list_type_fk
                        AND     lu.user_fk = v_user_pk
                        AND
                        (
                                lu.list_fk = l.list_pk
                        OR      lu.list_fk = l.level0
                        OR      lu.list_fk = l.level1
                        OR      lu.list_fk = l.level2
                        OR      lu.list_fk = l.level3
                        OR      lu.list_fk = l.level4
                        OR      lu.list_fk = l.level5
                        )
                )
        OR      c.organization_fk = o.organization_pk
        AND     c.event_date = to_date(trans(p_new_date),get.txt('date_long'))
        AND     o.accepted=1
        AND     c.organization_fk=os.organization_fk
        AND     os.service_fk=v_service_pk
        AND     c.calendar_pk > p_calendar_pk
        AND     (o.organization_pk, c.language_fk) IN
                (
                        SELECT  DISTINCT organization_fk, lu.language_fk
                        FROM    list_org lo, list l, list_user lu, list_type lt
                        WHERE   lo.list_fk = l.list_pk
                        AND     lt.list_type_pk = l.list_type_fk
                        AND     lu.user_fk = v_user_pk
                        AND
                        (
                                lu.list_fk = l.list_pk
                        OR      lu.list_fk = l.level0
                        OR      lu.list_fk = l.level1
                        OR      lu.list_fk = l.level2
                        OR      lu.list_fk = l.level3
                        OR      lu.list_fk = l.level4
                        OR      lu.list_fk = l.level5
                        )
                )
        ORDER BY c.event_date, c.calendar_pk
        ;

BEGIN

v_cal_arc_nr := get.value('cal_arc_nr');

IF ( p_service_pk IS NULL ) THEN
        v_service_pk := get.serv;
ELSE
        v_service_pk := p_service_pk;
END IF;

IF ( p_organization_pk IS NOT NULL ) THEN
        html.b_page(NULL,NULL,NULL,NULL,3);
        html.org_menu(p_organization_pk);
        -- Legger inn i statistikk tabellen
        stat.reg(p_organization_pk,NULL,NULL,NULL,NULL,NULL,19,v_service_pk);
ELSIF ( p_my_cal IS NOT NULL ) THEN
        html.b_page;
        html.my_menu;
        -- Legger inn i statistikk tabellen
        stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,28,v_service_pk);
ELSE
	html.b_page(NULL, NULL, NULL, NULL, 1);
	html.home_menu;
        -- Legger inn i statistikk tabellen
        stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,21,v_service_pk);
END IF;
html.b_box( get.txt('calendar_archive') , '100%', 'org_page.cal' );
html.b_table;

IF ( p_language_pk IS NULL ) THEN
        v_language_pk := get.lan;
ELSE
        v_language_pk := p_language_pk;
END IF;

IF ( p_organization_pk IS NULL AND p_language_pk IS NULL AND p_my_cal IS NULL) THEN
        html.b_form('cal_util.cal_archive');
        htp.p('<tr><td>');
        html.select_service;
        htp.p('</td><td>');
        html.select_lang;
        htp.p('</td><td>');
        html.submit_link( get.txt('Show_archive') );
        htp.p('</td></tr>');
        html.e_form;
ELSE
        IF ( p_organization_pk IS NOT NULL AND p_my_cal IS NULL) THEN

                SELECT  count(*)
                INTO    v_count
                FROM    calendar c, organization o, org_service os
                WHERE   c.language_fk=v_language_pk
                AND     c.organization_fk = p_organization_pk
                AND     c.organization_fk = o.organization_pk
                AND     c.event_date > SYSDATE
                AND     o.accepted=1
                AND     c.organization_fk=os.organization_fk
                AND     os.service_fk=v_service_pk
                ;

                OPEN calendar_archive;
                WHILE ( calendar_archive%ROWCOUNT < v_cal_arc_nr )
                LOOP
                        FETCH calendar_archive INTO v_event_date,
                                v_heading, v_calendar_pk, v_name,
                                v_organization_pk;
                        exit when calendar_archive%NOTFOUND;
                        htp.p('<tr><td width="20%" align="left">'|| to_char(v_event_date,get.txt('date_long')) ||':</td>
                        <td width="80%" align="left"><a href="cal_util.show_calendar?p_calendar_pk='||v_calendar_pk||'">'|| v_heading ||'</a></td>
                        ');
--                      <td width="20%" align="left"><a href="org_page.main?p_organization_pk='||v_organization_pk||'">'|| v_name ||'</a></td></tr>
                END LOOP;
                CLOSE calendar_archive;
        ELSIF (p_my_cal IS NULL) then
                SELECT  count(*)
                INTO    v_count
                FROM    calendar c, organization o, org_service os
                WHERE   c.language_fk=v_language_pk
                AND     c.organization_fk = o.organization_pk
                AND     c.event_date > SYSDATE
                AND     o.accepted=1
                AND     c.organization_fk=os.organization_fk
                AND     os.service_fk=v_service_pk
                ;
		htp.p('<tr><td align="left"><b>'||get.txt('date')||':</b></td><td><b>'||get.txt('subject')||':</b></td><td><b>'||get.txt('source')||':</b></td><td><b>'||get.txt('geography_location')||':</b></td></tr>');

                OPEN calendar_archive_all;
                WHILE ( calendar_archive_all%ROWCOUNT < v_cal_arc_nr )
                LOOP
                        FETCH calendar_archive_all INTO v_event_date,
                                v_heading, v_calendar_pk, v_name,
                                v_organization_pk, v_geography_pk;
                        exit when calendar_archive_all%NOTFOUND;
                        htp.p('<tr><td width="15%" align="left">'|| to_char(v_event_date,get.txt('date_long')) ||':</td>
                        <td width="55%" align="left"><a href="cal_util.show_calendar?p_calendar_pk='||v_calendar_pk||'">'|| v_heading ||'</a></td>
                        <td width="15%" align="left"><a href="org_page.main?p_organization_pk='||v_organization_pk||'">'|| v_name ||'</a></td>
                        <td width="15%" align="left">'|| get.locn(v_geography_pk) ||'</a></td></tr>
                        ');
                END LOOP;
                CLOSE calendar_archive_all;
        ELSE
                v_user_pk := get.uid;

                SELECT  count(*)
                INTO    v_count
                FROM    calendar c, organization o, org_service os
                WHERE   c.language_fk=v_language_pk
                AND     c.organization_fk = o.organization_pk
                AND     c.event_date > to_date(trans(p_new_date),get.txt('date_long'))
                AND     o.accepted=1
                AND     c.organization_fk=os.organization_fk
                AND     os.service_fk=v_service_pk
                AND     (o.organization_pk, c.language_fk) IN
                        (
                                SELECT  DISTINCT organization_fk, lu.language_fk
                                FROM    list_org lo, list l, list_user lu, list_type lt
                                WHERE   lo.list_fk = l.list_pk
                                AND     lt.list_type_pk = l.list_type_fk
                                AND     lu.user_fk = v_user_pk
                                AND
                                (
                                        lu.list_fk = l.list_pk
                                OR      lu.list_fk = l.level0
                                OR      lu.list_fk = l.level1
                                OR      lu.list_fk = l.level2
                                OR      lu.list_fk = l.level3
                                OR      lu.list_fk = l.level4
                                OR      lu.list_fk = l.level5
                                )
                        );
                htp.p('<tr><td align="left"><b>'||get.txt('date')||':</b></td><td><b>'||get.txt('subject')||':</b></td><td><b>'||get.txt('source')||':</b></td><td><b>'||get.txt('geography_location')||':</b></td></tr>');

                OPEN my_calendar_archive(v_user_pk);
                WHILE ( my_calendar_archive%ROWCOUNT < v_cal_arc_nr )
                LOOP
                        FETCH my_calendar_archive INTO v_event_date,
                                v_heading, v_calendar_pk, v_name,
                                v_organization_pk, v_geography_pk;
                        exit when my_calendar_archive%NOTFOUND;
                        htp.p('<tr><td width="15%" align="left">'|| to_char(v_event_date,get.txt('date_long')) ||':</td>
                        <td width="55%" align="left"><a href="cal_util.show_calendar?p_calendar_pk='||v_calendar_pk||'">'|| v_heading ||'</a></td>
                        <td width="15%" align="left"><a href="org_page.main?p_organization_pk='||v_organization_pk||'">'|| v_name ||'</a></td>
                        <td width="15%" align="left">'|| get.locn(v_geography_pk) ||'</a></td></tr>
                        ');
                END LOOP;
                CLOSE my_calendar_archive;

        END IF;




        htp.p('<tr><td colspan="3">&nbsp;</td></tr>');
        IF ( p_old_date IS NOT NULL ) THEN
                v_count3 := p_count-v_cal_arc_nr;
                html.b_form('cal_util.cal_archive','prev');
                htp.p('<input type="hidden" name="p_service_pk" value="'|| v_service_pk ||'">
                <input type="hidden" name="p_my_cal" value="'|| p_my_cal ||'">
                <input type="hidden" name="p_language_pk" value="'|| v_language_pk ||'">
                <input type="hidden" name="p_organization_pk" value="'|| p_organization_pk ||'">
                <input type="hidden" name="p_count" value="'|| v_count3 ||'">
                <input type="hidden" name="p_new_date" value="'|| SUBSTR(trans(p_old_date),LENGTH(trans(p_old_date))-LENGTH(to_char(SYSDATE,get.txt('date_long')))+1,LENGTH(to_char(SYSDATE,get.txt('date_long')))+1) ||'">
                <input type="hidden" name="p_old_date" value="'|| SUBSTR(trans(p_old_date),0,LENGTH(trans(p_old_date))-(LENGTH(to_char(SYSDATE,get.txt('date_long')))+2)) ||'">
                <tr><td align="center" valign="bottom">');
                html.submit_link( get.txt('previous')||' '||v_cal_arc_nr,'prev' );
                htp.p('&nbsp;</td>');
                html.e_form;
        ELSE
                htp.p('<tr><td align="center" valign="bottom">&nbsp;</td>');
        END IF;
        v_count2 := p_count+v_cal_arc_nr-1;
        IF ( v_count2 > v_count ) THEN
                htp.p('<td valign="center" align="center">'|| p_count ||'-'|| v_count ||'/'|| v_count ||'</td>');
        ELSE
                htp.p('<td valign="center" align="center">'|| p_count ||'-'|| v_count2 ||'/'|| v_count ||'</td>');
        END IF;
        IF ( v_count2 < v_count ) THEN
                v_count3 := p_count+v_cal_arc_nr;
                IF ( v_count - v_count2 < v_cal_arc_nr ) THEN
                        v_cal_arc_nr := v_count - v_count2;
                END IF;
                html.b_form('cal_util.cal_archive','next');
                htp.p('<input type="hidden" name="p_service_pk" value="'|| v_service_pk ||'">
                <input type="hidden" name="p_my_cal" value="'|| p_my_cal ||'">
                <input type="hidden" name="p_calendar_pk" value="'|| v_calendar_pk ||'">
                <input type="hidden" name="p_language_pk" value="'|| v_language_pk ||'">
                <input type="hidden" name="p_organization_pk" value="'|| p_organization_pk ||'">
                <input type="hidden" name="p_count" value="'|| v_count3 ||'">
                <input type="hidden" name="p_new_date" value="'|| to_char( v_event_date,get.txt('date_long')) ||'">
                <input type="hidden" name="p_old_date" value="'|| trans(p_old_date) ||';;'|| trans(p_new_date) ||'">
                <td align="center" valign="top">&nbsp;');
                html.submit_link( get.txt('next')||' '||v_cal_arc_nr,'next');
                htp.p('</td></tr>');
                html.e_form;
        ELSE
                htp.p('<td align="center" valign="top">&nbsp;</td></tr>');
        END IF;
        IF ( p_organization_pk IS NOT NULL ) THEN
                IF ( v_name IS NULL ) THEN
                        SELECT  name
                        INTO    v_name
                        FROM    organization
                        WHERE   organization_pk = p_organization_pk;
                END IF;
                htp.p('
<tr><td colspan="3">&nbsp;</td></tr>
<tr><td align="center" colspan="3">
<a href="org_page.main?p_organization_pk='|| p_organization_pk ||'">['||v_name||']</a>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href="/">['||get.domain||']</a>
</td></tr>
<tr><td colspan="3">&nbsp;</td></tr>
');
        END IF;
END IF;

html.e_table;
html.e_box;
html.e_page;

EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
        htp.p(v_language_pk);

END;
END cal_archive;

---------------------------------------------------------------------
-- Name: show_calendar
-- Type: procedure
-- What: Genererer opp hovedsiden for admin av en organisasjon
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE show_calendar(        p_calendar_pk           IN calendar.calendar_pk%TYPE            DEFAULT NULL,
                                p_search                IN VARCHAR2                             DEFAULT NULL,
                                p_command               IN VARCHAR2                             DEFAULT NULL )
IS
BEGIN
DECLARE

v_calendar_pk           calendar.calendar_pk%TYPE               DEFAULT NULL;
v_heading               calendar.heading%TYPE                   DEFAULT NULL;
v_event_date            calendar.event_date%TYPE                DEFAULT NULL;
v_end_date              calendar.end_date%TYPE                	DEFAULT NULL;
v_message               calendar.message%TYPE                   DEFAULT NULL;
v_language_pk           calendar.language_fk%TYPE               DEFAULT NULL;
v_organization_pk       calendar.organization_fk%TYPE           DEFAULT NULL;
v_name                  organization.name%TYPE                  DEFAULT NULL;
v_logo                  organization.logo%TYPE                  DEFAULT NULL;
v_path                  VARCHAR2(100)                           DEFAULT NULL;
v_registration_pk       registration.registration_pk%type       DEFAULT NULL;

BEGIN

SELECT  c.calendar_pk, c.event_date, c.end_date, c.heading, c.message,
        c.language_fk, o.organization_pk, o.name, o.logo
INTO    v_calendar_pk, v_event_date, v_end_date, v_heading, v_message,
        v_language_pk, v_organization_pk, v_name, v_logo
FROM    calendar c, organization o
WHERE   c.calendar_pk = p_calendar_pk
AND     c.organization_fk = o.organization_pk
AND     o.accepted=1
;

IF ( p_command IS NULL ) THEN

	v_path := owa_util.get_cgi_env('HTTP_REFERER');

	IF ( owa_pattern.match( v_path, 'p_organization_pk=\d*', 'i') ) THEN
	        owa_pattern.change(v_path, '^.*p_organization_pk=', '');
	        owa_pattern.change(v_path, '&p_language_pk=\d*', '');
		html.b_page(NULL, NULL, NULL, NULL, 3);
		if ( v_organization_pk is not null ) THEN
		        html.org_menu( v_organization_pk );
		else
			html.org_menu(to_number(v_path));
		END IF;
	ELSE
		html.b_page(NULL, NULL, NULL, NULL, 1);
		html.home_menu;
	END IF;
ELSE
        html.b_page_2('600', NULL, NULL, NULL, 'JavaScript:self.print();');
	html.empty_menu;
END IF;



-- Legger inn i statistikk tabellen
stat.reg(v_organization_pk, NULL, v_calendar_pk, NULL, NULL, NULL,19 );

html.b_box('','100%','cal_util.show_document','cal_util.cal_archive?p_organization_pk='||v_organization_pk||'&p_language_pk='||v_language_pk);
html.b_table;
htp.p('<tr><td colspan="2">&nbsp;</td></tr>');

IF( v_logo IS NULL ) THEN
    v_logo := 'no_logo.gif';
END IF;

htp.p(' <tr><td colspan="2"><b>'||get.txt('date')||':</b>&nbsp;&nbsp;'|| to_char(v_event_date,get.txt('date_long')) );

if (v_end_date IS NOT NULL and v_end_date > v_event_date) then
 htp.p(' - '||to_char(v_end_date,get.txt('date_long')) );
end if;

htp.p(' &nbsp;(CET)<br></td></tr>
<tr><td colspan="2"><h2><b>'|| html.rem_tag(v_heading) ||'</b></h2></td></tr>
<tr><td width="90%"><b>'|| html.rem_tag(v_message) ||'</b></td>
<td rowspan="2" valign="top"><a href="org_page.main?p_organization_pk='|| v_organization_pk ||'">
<img src="'||get.value('org_logo_dir')||'/'|| v_logo ||'" alt="'||get.txt('back')||'"  border="0"></a></td>
</tr>
<tr><td>&nbsp;</td></tr>');

IF ( p_command IS NULL ) THEN
        htp.p('<tr><td colspan="2">');
	html.b_table;
	htp.p('<tr><td align="left" width="10%">'||
        html.popup( get.txt('print_page'),
        'cal_util.show_calendar?p_calendar_pk='||p_calendar_pk||'&p_command=print','620', '700', '1' )
        ||'</td><td align="left" width="90%"><b>'||get.txt('written_by')||':</b>&nbsp;&nbsp;<a href="org_page.main?p_organization_pk='|| v_organization_pk ||'">'||v_name||'</a>
<br>&nbsp;<br><b>'||get.txt('date')||':</b>&nbsp;&nbsp;'|| to_char(v_event_date,get.txt('date_long')) ||'&nbsp;(CET)
</td></tr>');
	html.e_table;
	htp.p('</td></tr>');
END IF;

-- sjekker om brukeren og kalenderen har en p�melding tilknyttet
v_registration_pk := get.reg_event(p_calendar_pk);
IF ( v_registration_pk IS NOT NULL) THEN
        IF ( get.has_reg(p_calendar_pk) ) THEN
                htp.p( '<tr><td align="right" colspan="2">'|| get.txt('entered_button') ||'</td></tr>');
        ELSE
                htp.p('<tr><td align="right" colspan="2"><a href="register.enter_for?p_registration_pk='||v_registration_pk||'">'|| get.txt('enter_for_button') ||'</a></td></tr>');
        END IF;
END IF;


IF ( p_search IS NOT NULL ) THEN
        htp.p('<tr><td colspan="2"><a
        href="search.cal?p_search='||REPLACE(trans(p_search),' ','+')||'">'||get.txt('back_to_search')||'</a></td></tr>
        <tr><td>&nbsp;</td></tr>');
END IF;

htp.p('<tr><td colspan="2">&nbsp;</td></tr>
<tr><td align="center" colspan="2">
<a href="org_page.main?p_organization_pk='|| v_organization_pk ||'">['||v_name||']</a>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href="/">['||get.domain||']</a>
</td></tr>
<tr><td colspan="2">&nbsp;</td></tr>
');

html.e_table;
html.e_box;
IF ( p_command IS NULL ) THEN
        tip.someone;
        html.e_page;
ELSE
        html.e_page_2;
END IF;

EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
        html.jump_to('/');
END;
END show_calendar;


---------------------------------------------------------------------
-- Name: cal_serv
-- Type: procedure
-- What: Genererer opp hovedsiden for admin av en organisasjon
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE cal_serv(     p_service_pk    IN service.service_pk%TYPE      DEFAULT NULL,
                        p_language_pk   IN calendar.language_fk%TYPE    DEFAULT NULL)
IS
BEGIN
DECLARE
v_service_pk            service.service_pk%TYPE                 DEFAULT NULL;
v_language_pk           document.language_fk%TYPE               DEFAULT NULL;
v_calendar_pk           calendar.calendar_pk%TYPE       DEFAULT NULL;
v_heading               calendar.heading%TYPE           DEFAULT NULL;
v_event_date            calendar.event_date%TYPE        DEFAULT NULL;
v_geography_pk          organization.geography_fk%TYPE          DEFAULT NULL;
v_organization_pk       calendar.organization_fk%TYPE   DEFAULT NULL;
v_name                  organization.name%TYPE          DEFAULT NULL;
v_logo                  organization.logo%TYPE          DEFAULT NULL;
v_path                  VARCHAR2(100)                   DEFAULT NULL;

CURSOR  get_calendar IS
SELECT  c.calendar_pk, c.heading, c.event_date, o.name, o.organization_pk, o.geography_fk
FROM    calendar c, organization o, org_service os
WHERE   c.organization_fk = o.organization_pk
AND     c.organization_fk=os.organization_fk
AND     c.language_fk = v_language_pk
AND     c.event_date > sysdate-(1/24)
AND     o.accepted=1
AND     os.service_fk=v_service_pk
ORDER BY event_date
;

BEGIN

IF ( p_service_pk IS NULL ) THEN
        v_service_pk := get.serv;
ELSE
        v_service_pk := p_service_pk;
END IF;
IF ( p_language_pk IS NULL ) THEN
        v_language_pk := get.lan;
ELSE
        v_language_pk := p_language_pk;
END IF;

html.b_box(get.txt('calendar'),'100%','cal_util.show_calendar','cal_util.cal_archive?p_language_pk='||p_language_pk);
html.b_table;
htp.p('<tr><td align="left"><b>'||get.txt('date')||':</b></td><td><b>'||get.txt('happening')||':</b></td><td><b>'||get.txt('source')||':</b></td><td><b>'||get.txt('geography_location')||':</b></td></tr>');

OPEN get_calendar;
LOOP
        fetch get_calendar into v_calendar_pk, v_heading, v_event_date, v_name, v_organization_pk, v_geography_pk;
        exit when get_calendar%NOTFOUND;
        htp.p('<tr><td valign="top" nowrap>'||to_char(v_event_date,get.txt('date_short'))||':</td>
        <td valign="top" align="left" width="90%"><b><a href="cal_util.show_calendar?p_calendar_pk='||v_calendar_pk||'">'||html.rem_tag(v_heading)||'</a></b></td>
        <td nowrap><a href="org_page.main?p_organization_pk='||v_organization_pk||'">'||v_name||'</a></td><td>'||get.locn(v_geography_pk)||'</td></tr>');
        exit when get_calendar%ROWCOUNT = get.value('cal_nr');
END LOOP;
close get_calendar;
html.e_table;
html.e_box;

EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
        html.jump_to('/');
END;
END cal_serv;


---------------------------------------------------------------------
-- Name: whatsup
-- Type: procedure
-- What: Genererer opp hovedsiden for admin av en organisasjon
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE whatsup(     p_start_date              IN VARCHAR2                                     DEFAULT NULL,
		       p_geography_pk		 IN geography.geography_pk%TYPE			 DEFAULT NULL)
IS
BEGIN
DECLARE
v_event_date            calendar.event_date%TYPE                DEFAULT NULL;
v_heading               calendar.heading%TYPE                   DEFAULT NULL;
v_calendar_pk           calendar.calendar_pk%TYPE               DEFAULT NULL;
v_name                  organization.name%TYPE                  DEFAULT NULL;
v_organization_pk       organization.organization_pk%TYPE       DEFAULT NULL;
v_cal_arc_nr		NUMBER					DEFAULT NULL;
v_language_pk           calendar.language_fk%TYPE               DEFAULT NULL;
v_service_pk            service.service_pk%TYPE                 DEFAULT NULL;
v_geography_pk          calendar.geography_fk%TYPE              DEFAULT NULL;

CURSOR  calendar_archive_all IS
SELECT  c.event_date, c.heading, c.calendar_pk, o.name, o.organization_pk
FROM    calendar c, organization o, org_service os
WHERE   c.language_fk=v_language_pk
AND     c.organization_fk = o.organization_pk
AND     to_char(c.event_date,'YYYYMMDD') = trans(p_start_date)
AND     o.accepted=1
AND     c.organization_fk=os.organization_fk
AND     os.service_fk=v_service_pk
AND     o.geography_fk = p_geography_pk
ORDER BY c.event_date, c.calendar_pk
;

BEGIN

v_cal_arc_nr := get.value('cal_arc_nr');

v_service_pk := get.serv;
v_language_pk := get.lan;

html.b_page(NULL, NULL, NULL, NULL, 1);
html.home_menu;
-- Legger inn i statistikk tabellen
stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,21,v_service_pk);

html.b_box( get.txt('whatsup') , '100%', 'cal_util.whatsup' );
html.b_table;

OPEN calendar_archive_all;
-- WHILE ( calendar_archive_all%ROWCOUNT < v_cal_arc_nr )
LOOP
	FETCH calendar_archive_all INTO v_event_date,
	      v_heading, v_calendar_pk, v_name,
	      v_organization_pk;
        exit when calendar_archive_all%NOTFOUND;
        htp.p('<tr><td width="20%" align="left">'|| to_char(v_event_date,get.txt('date_long')) ||':</td>
        <td width="60%" align="left"><a href="cal_util.show_calendar?p_calendar_pk='||v_calendar_pk||'">'|| v_heading ||'</a></td>
        <td width="20%" align="left"><a href="org_page.main?p_organization_pk='||v_organization_pk||'">'|| v_name ||'</a></td></tr>
        ');
END LOOP;
CLOSE calendar_archive_all;

html.e_table;
html.e_box;
html.e_page;

END;
END;
---------------------------------------------------------------------
---------------------------------------------------------------------
END;
/
