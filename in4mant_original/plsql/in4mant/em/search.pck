set define off
PROMPT *** package: search ***

CREATE OR REPLACE PACKAGE search IS

	PROCEDURE startup;
	PROCEDURE forum (       p_search        IN VARCHAR2     DEFAULT NULL );
	PROCEDURE doc (         p_search        IN VARCHAR2     DEFAULT NULL );
	PROCEDURE cal (         p_search        IN VARCHAR2     DEFAULT NULL );
	PROCEDURE album
	                (
	                        p_search        IN VARCHAR2     DEFAULT NULL,
	                        p_type          IN VARCHAR2     DEFAULT NULL
	                );
	PROCEDURE the_bboard;

END;
/
CREATE OR REPLACE PACKAGE BODY search IS
---------------------------------------------------------------------
---------------------------------------------------------------------
-- Name: startup
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 30.08.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE startup
IS
BEGIN
        html.b_page;
        html.b_box;
        html.b_table;
        htp.p('<tr><td><a href="search.forum">Search in forum</a></td></tr>');
        htp.p('<tr><td><a href="search.doc">Search in documents</a></td></tr>');
        htp.p('<tr><td><a href="search.cal">Search in calendar</a></td></tr>');
        html.e_table;
        html.e_box;
        html.e_page;
END startup;

---------------------------------------------------------------------
-- Name: forum
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 30.08.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE forum (       p_search        IN VARCHAR2     DEFAULT NULL )
IS
BEGIN
DECLARE

v_fora_pk               fora.fora_pk%TYPE                       DEFAULT NULL;
v_fora_type_pk          fora_type.fora_type_pk%TYPE             DEFAULT NULL;
v_sub_title             fora.sub_title%TYPE                     DEFAULT NULL;
v_published_date        fora.published_date%TYPE                DEFAULT NULL;
v_string                VARCHAR2(30)                            DEFAULT NULL;
v_count                 NUMBER                                  DEFAULT NULL;
v_name                  fora_type.name%TYPE                     DEFAULT NULL;
v_organization_pk       organization.organization_pk%TYPE       DEFAULT NULL;
v_service_pk		fora_type.service_fk%TYPE 		DEFAULT NULL;

CURSOR  fora_search (v_string2 in VARCHAR2) IS
SELECT  DISTINCT(f.fora_pk), f.sub_title, f.published_date,
        f.fora_type_fk, ft.name, ft.organization_fk
FROM    fora f, usr u, fora_type ft
WHERE   upper(f.sub_title) like '%'|| upper(v_string2) ||'%'
AND     f.fora_type_fk = ft.fora_type_pk
AND	ft.service_fk = v_service_pk
OR      upper(f.body) like '%'|| upper(v_string2) ||'%'
AND     f.fora_type_fk = ft.fora_type_pk
AND	ft.service_fk = v_service_pk
OR      upper(u.login_name) like '%'|| upper(v_string2) ||'%'
AND     f.user_fk = u.user_pk
AND     f.fora_type_fk = ft.fora_type_pk
AND	ft.service_fk = v_service_pk
ORDER   BY f.published_date DESC
;

BEGIN
v_service_pk := get.serv;
-- Legger inn i statistikk tabellen
stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,24,v_service_pk);

html.b_page;
html.search_menu;
html.b_box( get.txt('search_forum'), '100%', 'search.forum');
html.b_table;
IF ( p_search IS NULL ) THEN
        htp.p('<tr><td colspan="2">'||get.txt('search_help_forum')||'</td></tr>');
END IF;
html.b_form('search.forum');
htp.p('<tr><td><b>'|| get.txt('search_forum') ||': </b></td><td>
'|| html.text('p_search', '20', '50', trans(p_search) ));
html.submit_link( get.txt('search_forum') );
htp.p('</td></tr>');
html.e_form;
IF ( p_search IS NOT NULL ) THEN
        v_string := trans(p_search);
        v_string := REPLACE(v_string, ' ','% %');
        v_string := REPLACE(v_string, ',','% %');
        v_string := REPLACE(v_string, '+','% %');

        SELECT  COUNT(DISTINCT(f.fora_pk))
        INTO    v_count
        FROM    fora f, usr u, fora_type ft
        WHERE   upper(f.sub_title) like '%'|| upper(v_string) ||'%'
	AND     f.fora_type_fk = ft.fora_type_pk
	AND	ft.service_fk = v_service_pk
        OR      upper(f.body) like '%'|| upper(v_string) ||'%'
	AND     f.fora_type_fk = ft.fora_type_pk
	AND	ft.service_fk = v_service_pk
        OR      upper(u.login_name) like '%'|| upper(v_string) ||'%'
	AND     f.fora_type_fk = ft.fora_type_pk
	AND	ft.service_fk = v_service_pk
        AND     f.user_fk = u.user_pk
        ;

        htp.p('<tr><td colpsan="4">'||REPLACE(REPLACE(get.txt('search_result'), '[nr]', v_count), '[type]', get.txt('forum'))||'</td></tr>
        <tr><td><b>'||get.txt('date')||':</b></td><td><b>'||get.txt('heading')||':</b></td><td><b>'||get.txt('forum')||':</b></td><td><b>'||get.txt('source')||':</b></td></tr>');
        open fora_search(v_string);
        loop
                fetch fora_search into v_fora_pk, v_sub_title,
                v_published_date, v_fora_type_pk, v_name, v_organization_pk;
                exit when fora_search%NOTFOUND;
                htp.p('<tr><td nowrap>'||to_char(v_published_date,get.txt('date_long'))||'</td>
                <td><a href="fora_util.frames?p_fora_pk='||v_fora_pk||'&p_fora_type_pk='||v_fora_type_pk||'"">
                '||v_sub_title||'</a></td><td nowrap>'||v_name||'</td><td nowrap>');
                IF (v_organization_pk > 0 ) THEN
                        htp.p('<a href="org_page.main?p_organization_pk='||v_organization_pk||'">
                        '||get.oname(v_organization_pk)||'</a>');
                END IF;
                htp.p('&nbsp;</td></tr>');
        end loop;
        close fora_search;
END IF;

html.e_table;
html.e_box;
html.e_page;
END;
END forum;

---------------------------------------------------------------------
-- Name: doc
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 30.08.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE doc ( p_search        IN VARCHAR2     DEFAULT NULL )
IS
BEGIN
DECLARE

v_document_pk           document.document_pk%TYPE       	DEFAULT NULL;
v_heading               document.heading%TYPE           	DEFAULT NULL;
v_publish_date          document.publish_date%TYPE      	DEFAULT NULL;
v_string                VARCHAR2(30)                    	DEFAULT NULL;
v_count                 NUMBER                          	DEFAULT NULL;
v_name                  organization.name%TYPE          	DEFAULT NULL;
v_organization_pk       organization.organization_pk%TYPE       DEFAULT NULL;
v_service_pk		service.service_pk%TYPE			DEFAULT NULL;

CURSOR  doc_search (v_string2 in VARCHAR2) IS
SELECT  DISTINCT(d.document_pk), d.heading, d.publish_date, o.name, o.organization_pk
FROM    document d, organization o, org_service os
WHERE   upper(d.heading) like '%'|| upper(v_string2) ||'%'
AND     d.publish_date < SYSDATE
AND     d.accepted>0
AND     d.organization_fk = o.organization_pk
AND     o.accepted>0
AND     os.organization_fk = o.organization_pk
AND	os.service_fk = v_service_pk
OR      upper(d.ingress) like '%'|| upper(v_string2) ||'%'
AND     d.publish_date < SYSDATE
AND     d.accepted>0
AND     d.organization_fk = o.organization_pk
AND     o.accepted>0
AND     os.organization_fk = o.organization_pk
AND	os.service_fk = v_service_pk
OR      upper(d.main1) like '%'|| upper(v_string2) ||'%'
AND     d.publish_date < SYSDATE
AND     d.accepted>0
AND     d.organization_fk = o.organization_pk
AND     o.accepted>0
AND     os.organization_fk = o.organization_pk
AND	os.service_fk = v_service_pk
OR      upper(d.main2) like '%'|| upper(v_string2) ||'%'
AND     d.publish_date < SYSDATE
AND     d.accepted>0
AND     d.organization_fk = o.organization_pk
AND     o.accepted>0
AND     os.organization_fk = o.organization_pk
AND	os.service_fk = v_service_pk
OR      upper(d.main3) like '%'|| upper(v_string2) ||'%'
AND     d.publish_date < SYSDATE
AND     d.accepted>0
AND     d.organization_fk = o.organization_pk
AND     o.accepted>0
AND     os.organization_fk = o.organization_pk
AND	os.service_fk = v_service_pk
OR      upper(d.main4) like '%'|| upper(v_string2) ||'%'
AND     d.publish_date < SYSDATE
AND     d.accepted>0
AND     d.organization_fk = o.organization_pk
AND     o.accepted>0
AND     os.organization_fk = o.organization_pk
AND	os.service_fk = v_service_pk
OR      upper(d.main5) like '%'|| upper(v_string2) ||'%'
AND     d.publish_date < SYSDATE
AND     d.accepted>0
AND     d.organization_fk = o.organization_pk
AND     o.accepted>0
AND     os.organization_fk = o.organization_pk
AND	os.service_fk = v_service_pk
OR      upper(d.main6) like '%'|| upper(v_string2) ||'%'
AND     d.publish_date < SYSDATE
AND     d.accepted>0
AND     d.organization_fk = o.organization_pk
AND     o.accepted>0
AND     os.organization_fk = o.organization_pk
AND	os.service_fk = v_service_pk
OR      upper(d.main7) like '%'|| upper(v_string2) ||'%'
AND     d.publish_date < SYSDATE
AND     d.accepted>0
AND     d.organization_fk = o.organization_pk
AND     o.accepted>0
AND     os.organization_fk = o.organization_pk
AND	os.service_fk = v_service_pk
OR      upper(d.main8) like '%'|| upper(v_string2) ||'%'
AND     d.publish_date < SYSDATE
AND     d.accepted>0
AND     d.organization_fk = o.organization_pk
AND     o.accepted>0
AND     os.organization_fk = o.organization_pk
AND	os.service_fk = v_service_pk
OR      upper(d.main9) like '%'|| upper(v_string2) ||'%'
AND     d.publish_date < SYSDATE
AND     d.accepted>0
AND     d.organization_fk = o.organization_pk
AND     o.accepted>0
AND     os.organization_fk = o.organization_pk
AND	os.service_fk = v_service_pk
OR      upper(d.main10) like '%'|| upper(v_string2) ||'%'
AND     d.publish_date < SYSDATE
AND     d.accepted>0
AND     d.organization_fk = o.organization_pk
AND     o.accepted>0
AND     os.organization_fk = o.organization_pk
AND	os.service_fk = v_service_pk
OR      upper(d.footer) like '%'|| upper(v_string2) ||'%'
AND     d.publish_date < SYSDATE
AND     d.accepted>0
AND     d.organization_fk = o.organization_pk
AND     o.accepted>0
AND     os.organization_fk = o.organization_pk
AND	os.service_fk = v_service_pk
OR      upper(o.name) like '%'|| upper(v_string2) ||'%'
AND     d.publish_date < SYSDATE
AND     o.organization_pk = d.organization_fk
AND     d.accepted>0
AND     o.accepted>0
AND     os.organization_fk = o.organization_pk
AND	os.service_fk = v_service_pk
ORDER   BY d.publish_date DESC
;

BEGIN
v_service_pk := get.serv;
-- Legger inn i statistikk tabellen
stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,24,v_service_pk);

html.b_page;
html.search_menu;
html.b_box( get.txt('search_doc') , '100%', 'search.doc');
html.b_table;
IF ( p_search IS NULL ) THEN
        htp.p('<tr><td colspan="2">'||get.txt('search_help_doc')||'</td></tr>');
END IF;
html.b_form('search.doc');
htp.p('<tr><td><b>'|| get.txt('search_doc') ||': </b></td><td>
'|| html.text('p_search', '20', '50', trans(p_search) ));
html.submit_link( get.txt('search_doc') );
htp.p('</td></tr>');
html.e_form;

IF ( p_search IS NOT NULL ) THEN
        v_string := trans(p_search);
        v_string := REPLACE(v_string, ' ','% %');
        v_string := REPLACE(v_string, ',','% %');
        v_string := REPLACE(v_string, '+','% %');
        SELECT  COUNT(DISTINCT(d.document_pk))
        INTO    v_count
        FROM    document d, organization o, org_service os
        WHERE   upper(d.heading) like '%'|| upper(v_string) ||'%'
        AND     d.publish_date < SYSDATE
        AND     d.accepted>0
        AND     d.organization_fk = o.organization_pk
        AND     o.accepted>0
	AND     os.organization_fk = o.organization_pk
	AND	os.service_fk = v_service_pk
        OR      upper(d.ingress) like '%'|| upper(v_string) ||'%'
        AND     d.publish_date < SYSDATE
        AND     d.accepted>0
        AND     d.organization_fk = o.organization_pk
        AND     o.accepted>0
	AND     os.organization_fk = o.organization_pk
	AND	os.service_fk = v_service_pk
        OR      upper(d.main1) like '%'|| upper(v_string) ||'%'
        AND     d.publish_date < SYSDATE
        AND     d.accepted>0
        AND     d.organization_fk = o.organization_pk
        AND     o.accepted>0
	AND     os.organization_fk = o.organization_pk
	AND	os.service_fk = v_service_pk
        OR      upper(d.main2) like '%'|| upper(v_string) ||'%'
        AND     d.publish_date < SYSDATE
        AND     d.accepted>0
        AND     d.organization_fk = o.organization_pk
        AND     o.accepted>0
	AND     os.organization_fk = o.organization_pk
	AND	os.service_fk = v_service_pk
        OR      upper(d.main3) like '%'|| upper(v_string) ||'%'
        AND     d.publish_date < SYSDATE
        AND     d.accepted>0
        AND     d.organization_fk = o.organization_pk
        AND     o.accepted>0
	AND     os.organization_fk = o.organization_pk
	AND	os.service_fk = v_service_pk
        OR      upper(d.main4) like '%'|| upper(v_string) ||'%'
        AND     d.publish_date < SYSDATE
        AND     d.accepted>0
        AND     d.organization_fk = o.organization_pk
        AND     o.accepted>0
	AND     os.organization_fk = o.organization_pk
	AND	os.service_fk = v_service_pk
        OR      upper(d.main5) like '%'|| upper(v_string) ||'%'
        AND     d.publish_date < SYSDATE
        AND     d.accepted>0
        AND     d.organization_fk = o.organization_pk
        AND     o.accepted>0
	AND     os.organization_fk = o.organization_pk
	AND	os.service_fk = v_service_pk
        OR      upper(d.main6) like '%'|| upper(v_string) ||'%'
        AND     d.publish_date < SYSDATE
        AND     d.accepted>0
        AND     d.organization_fk = o.organization_pk
        AND     o.accepted>0
	AND     os.organization_fk = o.organization_pk
	AND	os.service_fk = v_service_pk
        OR      upper(d.main7) like '%'|| upper(v_string) ||'%'
        AND     d.publish_date < SYSDATE
        AND     d.accepted>0
        AND     d.organization_fk = o.organization_pk
        AND     o.accepted>0
	AND     os.organization_fk = o.organization_pk
	AND	os.service_fk = v_service_pk
        OR      upper(d.main8) like '%'|| upper(v_string) ||'%'
        AND     d.publish_date < SYSDATE
        AND     d.accepted>0
        AND     d.organization_fk = o.organization_pk
        AND     o.accepted>0
	AND     os.organization_fk = o.organization_pk
	AND	os.service_fk = v_service_pk
        OR      upper(d.main9) like '%'|| upper(v_string) ||'%'
        AND     d.publish_date < SYSDATE
        AND     d.accepted>0
        AND     d.organization_fk = o.organization_pk
        AND     o.accepted>0
	AND     os.organization_fk = o.organization_pk
	AND	os.service_fk = v_service_pk
        OR      upper(d.main10) like '%'|| upper(v_string) ||'%'
        AND     d.publish_date < SYSDATE
        AND     d.accepted>0
        AND     d.organization_fk = o.organization_pk
        AND     o.accepted>0
	AND     os.organization_fk = o.organization_pk
	AND	os.service_fk = v_service_pk
        OR      upper(d.footer) like '%'|| upper(v_string) ||'%'
        AND     d.publish_date < SYSDATE
        AND     d.accepted>0
        AND     d.organization_fk = o.organization_pk
        AND     o.accepted>0
	AND     os.organization_fk = o.organization_pk
	AND	os.service_fk = v_service_pk
        OR      upper(o.name) like '%'|| upper(v_string) ||'%'
        AND     d.publish_date < SYSDATE
        AND     o.organization_pk = d.organization_fk
        AND     d.accepted>0
        AND     o.accepted>0
	AND     os.organization_fk = o.organization_pk
	AND	os.service_fk = v_service_pk
        ;

        htp.p('<tr><td colspan="2">'||REPLACE(REPLACE(get.txt('search_result'), '[nr]', v_count), '[type]', get.txt('documents'))||'</td></tr>
        <tr><td><b>'||get.txt('date')||':</b></td><td><b>'||get.txt('heading')||':</b></td><td><b>'||get.txt('source')||':</b></td></tr>');

        open doc_search(v_string);
        loop
                fetch doc_search into v_document_pk, v_heading, v_publish_date, v_name, v_organization_pk;
                exit when doc_search%NOTFOUND;
                htp.p('<tr><td nowrap>'||to_char(v_publish_date, get.txt('date_long'))||'</td>
                <td><a href="doc_util.show_document?p_document_pk='||v_document_pk||'&p_search='||REPLACE(trans(p_search),' ','+')||'">'||v_heading||'</a></td>
                <td nowrap><a href="org_page.main?p_organization_pk='||v_organization_pk||'">'||v_name||'</a></td></tr>');
        end loop;
        close doc_search;
END IF;

html.e_table;
html.e_box;
html.e_page;
END;
END doc;

---------------------------------------------------------------------
-- Name: cal
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 30.08.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE cal ( p_search        IN VARCHAR2     DEFAULT NULL )
IS
BEGIN
DECLARE

v_calendar_pk           calendar.calendar_pk%TYPE               DEFAULT NULL;
v_heading               calendar.heading%TYPE           DEFAULT NULL;
v_event_date            calendar.event_date%TYPE        DEFAULT NULL;
v_string                VARCHAR2(30)                    DEFAULT NULL;
v_count                 NUMBER                          DEFAULT NULL;
v_name                  organization.name%TYPE          DEFAULT NULL;
v_organization_pk       organization.organization_pk%TYPE       DEFAULT NULL;
v_service_pk		service.service_pk%TYPE			DEFAULT NULL;

CURSOR  cal_search (v_string2 in VARCHAR2) IS
SELECT  DISTINCT(c.calendar_pk), c.heading, c.event_date, o.name, o.organization_pk
FROM    calendar c, organization o, org_service os
WHERE   upper(c.heading) like '%'|| upper(v_string2) ||'%'
AND     o.organization_pk = c.organization_fk
AND     o.accepted = 1
AND     os.organization_fk = o.organization_pk
AND	os.service_fk = v_service_pk
OR      upper(c.message) like '%'|| upper(v_string2) ||'%'
AND     o.organization_pk = c.organization_fk
AND     o.accepted = 1
AND     os.organization_fk = o.organization_pk
AND	os.service_fk = v_service_pk
OR      upper(o.name) like '%'|| upper(v_string2) ||'%'
AND     o.organization_pk = c.organization_fk
AND     o.accepted = 1
AND     os.organization_fk = o.organization_pk
AND	os.service_fk = v_service_pk
ORDER   BY c.event_date DESC
;

BEGIN
v_service_pk := get.serv;
-- Legger inn i statistikk tabellen
stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,24,v_service_pk);

html.b_page;
html.search_menu;
html.b_box( get.txt('search_cal') , '100%', 'search.cal');
html.b_table;
IF ( p_search IS NULL ) THEN
        htp.p('<tr><td colspan="2">'||get.txt('search_help_cal')||'</td></tr>');
END IF;
html.b_form('search.cal');
htp.p('<tr><td><b>'|| get.txt('search_cal') ||': </b></td><td>
'|| html.text('p_search', '20', '50', trans(p_search) ));
html.submit_link( get.txt('search_cal') );
htp.p('</td></tr>');
html.e_form;

IF ( p_search IS NOT NULL ) THEN
        v_string := trans(p_search);
        v_string := REPLACE(v_string, ' ','% %');
        v_string := REPLACE(v_string, ',','% %');
        v_string := REPLACE(v_string, '+','% %');

        SELECT  COUNT(DISTINCT(c.calendar_pk))
        INTO    v_count
        FROM    calendar c, organization o, org_service os
        WHERE   upper(c.heading) like '%'|| upper(v_string) ||'%'
        AND     o.organization_pk = c.organization_fk
        AND     o.accepted = 1
	AND     os.organization_fk = o.organization_pk
	AND	os.service_fk = v_service_pk
        OR      upper(c.message) like '%'|| upper(v_string) ||'%'
        AND     o.organization_pk = c.organization_fk
        AND     o.accepted = 1
	AND     os.organization_fk = o.organization_pk
	AND	os.service_fk = v_service_pk
        OR      upper(o.name) like '%'|| upper(v_string) ||'%'
        AND     o.organization_pk = c.organization_fk
        AND     o.accepted = 1
	AND     os.organization_fk = o.organization_pk
	AND	os.service_fk = v_service_pk
        ;

        htp.p('<tr><td colspan="2">'||REPLACE(REPLACE(get.txt('search_result'), '[nr]', v_count), '[type]', get.txt('calendars'))||'</td></tr>
        <tr><td><b>'||get.txt('date')||':</b></td><td><b>'||get.txt('heading')||':</b></td><td><b>'||get.txt('source')||':</b></td></tr>');

        open cal_search(v_string);
        loop
                fetch cal_search into v_calendar_pk, v_heading, v_event_date, v_name, v_organization_pk;
                exit when cal_search%NOTFOUND;
                htp.p('<tr><td nowrap>'||to_char(v_event_date, get.txt('date_long'))||'</td>
                <td align="left"><a href="cal_util.show_calendar?p_calendar_pk='||v_calendar_pk||'&p_search='||REPLACE(trans(p_search),' ','+')||'">'||v_heading||'</a></td>
                <td nowrap><a href="org_page.main?p_organization_pk='||v_organization_pk||'">'||v_name||'</a></td></tr>');
        end loop;
        close cal_search;
END IF;

html.e_table;
html.e_box;
html.e_page;
END;
END cal;

---------------------------------------------------------------------
-- Name: album
-- Type: procedure
-- What:
-- Made: Frode Klevstul
-- Date: 02.09.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE album
        (
                p_search        IN VARCHAR2     DEFAULT NULL,
                p_type          IN VARCHAR2     DEFAULT NULL
        )
IS
BEGIN
DECLARE

        v_string                VARCHAR2(30)                            DEFAULT NULL;
        v_album_pk              album_text.album_fk%type                DEFAULT NULL;
        v_title                 album_text.title%type                   DEFAULT NULL;
        v_when_period           album_text.when_period%type             DEFAULT NULL;
        v_create_date           DATE                                    DEFAULT NULL;
        v_organization_pk       organization.organization_pk%type       DEFAULT NULL;

        v_album_search          VARCHAR2(50)                            DEFAULT NULL;
        v_picture_search        VARCHAR2(50)                            DEFAULT NULL;
        v_comment_search        VARCHAR2(50)                            DEFAULT NULL;

        v_picture_pk            picture.picture_pk%type                 DEFAULT NULL;
        v_what                  picture_text.what%type                  DEFAULT NULL;
        v_when_taken            picture.when_taken%type                 DEFAULT NULL;
        v_count                 NUMBER                                  DEFAULT NULL;
        v_language_pk           la.language_pk%type                     DEFAULT NULL;

        v_colspan               number                                  DEFAULT NULL;

        CURSOR  album_search (v_string2 in VARCHAR2, v_language_pk in la.language_pk%type) IS
        SELECT  DISTINCT(album_pk), title, when_period, create_date, a.organization_fk
        FROM    album a, album_text alt, org_service os
        WHERE   alt.album_fk = a.album_pk
        AND     a.activated = 1
        AND     alt.language_fk = v_language_pk
        AND     (
                                upper(title) like '%'|| upper(v_string2) ||'%'
                        OR      upper(description) like '%'|| upper(v_string2) ||'%'
                        OR      upper(when_period) like '%'|| upper(v_string2) ||'%'
                )
	AND	os.organization_fk = a.organization_fk
	AND	os.service_fk = get.serv
        ORDER   BY create_date;


        CURSOR  picture_search (v_string2 in VARCHAR2, v_language_pk in la.language_pk%type) IS
        SELECT  DISTINCT(picture_pk), what, when_taken, a.organization_fk, title, album_pk
        FROM    picture p, picture_text pt, picture_in_album pia, album a, album_text alt, org_service os
        WHERE   pt.picture_fk = p.picture_pk
        AND     alt.album_fk = a.album_pk
        AND     alt.language_fk = v_language_pk
        AND     pia.picture_fk = p.picture_pk
        AND     pia.album_fk = a.album_pk
        AND     pt.language_fk = v_language_pk
        AND     a.activated = 1
        AND     (
                                upper(pt.what) like '%'|| upper(v_string2) ||'%'
                        OR      upper(p.whom) like '%'|| upper(v_string2) ||'%'
                        OR      upper(p.photographer) like '%'|| upper(v_string2) ||'%'
                )
	AND	os.organization_fk = a.organization_fk
	AND	os.service_fk = get.serv
        ORDER   BY when_taken;


        CURSOR  comment_search (v_string2 in VARCHAR2, v_language_pk in la.language_pk%type) IS
        SELECT  DISTINCT(picture_pk), what, when_taken, a.organization_fk, title, album_pk
        FROM    picture p, picture_comment pc, picture_in_album pia, album a, usr u, picture_text pt, album_text alt, org_service os
        WHERE   pc.picture_fk = p.picture_pk
        AND     alt.album_fk = a.album_pk
        AND     alt.language_fk = v_language_pk
        AND     pt.picture_fk = p.picture_pk
        AND     pt.language_fk = v_language_pk
        AND     pia.picture_fk = p.picture_pk
        AND     pia.album_fk = a.album_pk
        AND     pc.user_fk = u.user_pk
        AND     a.activated = 1
        AND     (
                                upper(pc.the_comment) like '%'|| upper(v_string2) ||'%'
                        OR      upper(u.login_name) like '%'|| upper(v_string2) ||'%'
                )
	AND	os.organization_fk = a.organization_fk
	AND	os.service_fk = get.serv
        ORDER   BY when_taken;

BEGIN
-- Legger inn i statistikk tabellen
stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,24);


        -- -------------------
        -- starter html kode
        -- -------------------
        html.b_page;
        html.search_menu;
        html.b_box( get.txt('search_album'), '100%', 'search.album');

        -- ---------------------------------------------------------------
        -- skriver ut hjelpe tekst f�rste gang man kommer inn
        -- ---------------------------------------------------------------
        if ( p_search IS NULL ) then
                htp.p( get.txt('search_help_album') ||'<br><br>');
        end if;

        html.b_table;

        -- --------------------------------------------
        -- setter colspan utifra hva vi skal s�ke i
        -- --------------------------------------------
        if (p_type = 'album') then
                v_colspan := 2;
        elsif (p_type = 'picture' OR p_type = 'comment') then
                v_colspan := 3;
        end if;

        -- --------------------------------------------------------------
        -- setter "value" som skal i <input> feltet dersom s�k i album
        -- --------------------------------------------------------------
        if (p_type = 'album') then
                v_album_search := p_search;
        end if;

        -- ---------------------------------------------------
        -- skriver ut <input> felt og knapp for s�k i album
        -- ---------------------------------------------------
        html.b_form('search.album');
        htp.p('<input type="hidden" name="p_type" value="album">
        <tr><td><b>'|| get.txt('search_albums') ||': </b></td><td colspan="'||v_colspan||'">
        '|| html.text('p_search', '20', '50', trans(v_album_search) ));
        html.submit_link( get.txt('search_albums') );
        htp.p('</td></tr>');
        html.e_form;

        -- --------------------------------------------------------------
        -- setter "value" som skal i <input> feltet dersom s�k i bilder
        -- --------------------------------------------------------------
        if (p_type = 'picture') then
                v_picture_search := p_search;
        end if;

        -- ---------------------------------------------------
        -- skriver ut <input> felt og knapp for s�k i bilder
        -- ---------------------------------------------------
        html.b_form('search.album');
        htp.p('<input type="hidden" name="p_type" value="picture">
        <tr><td><b>'|| get.txt('search_pictures') ||': </b></td><td colspan="'||v_colspan||'">
        '|| html.text('p_search', '20', '50', trans(v_picture_search) ));
        html.submit_link( get.txt('search_pictures') );
        htp.p('</td></tr>');
        html.e_form;

        -- --------------------------------------------------------------
        -- setter "value" som skal i <input> feltet dersom s�k i kommentarer
        -- --------------------------------------------------------------
        if (p_type = 'comment') then
                v_comment_search := p_search;
        end if;

        -- ---------------------------------------------------
        -- skriver ut <input> felt og knapp for s�k i kommentarer
        -- ---------------------------------------------------
        html.b_form('search.album');
        htp.p('<input type="hidden" name="p_type" value="comment">
        <tr><td><b>'|| get.txt('search_picture_comments') ||': </b></td><td colspan="'||v_colspan||'">
        '|| html.text('p_search', '20', '50', trans(v_comment_search) ));
        html.submit_link( get.txt('search_picture_comments') );
        htp.p('</td></tr>');
        html.e_form;

        v_colspan := v_colspan + 2;
        htp.p('<tr><td colspan="'||v_colspan||'">&nbsp;</td></tr>');

        -- ----------------------------------
        -- skriver ut resultatene av s�ket
        -- ----------------------------------
        IF ( p_search IS NOT NULL ) THEN
                v_language_pk := get.lan;
                v_string := trans(p_search);
                v_string := REPLACE(v_string, ' ','% %');
                v_string := REPLACE(v_string, ',','% %');
                v_string := REPLACE(v_string, '+','% %');

                if (p_type = 'album') then

                        SELECT  count(DISTINCT(album_pk)) INTO v_count
		        FROM    album a, album_text alt, org_service os
		        WHERE   alt.album_fk = a.album_pk
		        AND     a.activated = 1
		        AND     alt.language_fk = v_language_pk
		        AND     (
		                                upper(title) like '%'|| upper(v_string) ||'%'
		                        OR      upper(description) like '%'|| upper(v_string) ||'%'
		                        OR      upper(when_period) like '%'|| upper(v_string) ||'%'
		                )
			AND	os.organization_fk = a.organization_fk
			AND	os.service_fk = get.serv;

                        htp.p('<tr><td colspan="3">'||REPLACE(REPLACE(get.txt('search_result'), '[nr]', v_count), '[type]', get.txt('album') )||'</td></tr>
                        <tr><td><b>'|| get.txt('when') ||'</b>:</td><td><b>'|| get.txt('title') ||'</b>:</td><td><b>'|| get.txt('source') ||'</b>:</td></tr>');
                        open album_search(v_string, get.lan);
                        loop
                                fetch album_search into v_album_pk, v_title, v_when_period, v_create_date, v_organization_pk;
                                exit when album_search%NOTFOUND;
                                htp.p('<tr><td>'|| v_when_period ||'</td><td><a href="photo.user_list_pictures?p_album_pk='||v_album_pk||'">'||v_title||'</a></td><td><a href="org_page.main?p_organization_pk='||v_organization_pk||'">'||get.oname(v_organization_pk)||'</a></td></tr>');
                        end loop;
                        close album_search;
                elsif (p_type = 'picture') then

                        SELECT  count(DISTINCT(picture_pk)) INTO v_count
                        FROM    picture p, picture_text pt, picture_in_album pia, album a, org_service os
                        WHERE   pt.picture_fk = p.picture_pk
                        AND     pia.picture_fk = p.picture_pk
                        AND     pia.album_fk = a.album_pk
                        AND     pt.language_fk = v_language_pk
                        AND     a.activated = 1
                        AND     (
                                                upper(pt.what) like '%'|| upper(v_string) ||'%'
                                        OR      upper(p.whom) like '%'|| upper(v_string) ||'%'
                                        OR      upper(p.photographer) like '%'|| upper(v_string) ||'%'
                                )
			AND	os.organization_fk = a.organization_fk
			AND	os.service_fk = get.serv;

                        htp.p('<tr><td colspan="3">'||REPLACE(REPLACE(get.txt('search_result'), '[nr]', v_count), '[type]', get.txt('pictures') )||'</td></tr>
                        <tr><td><b>'|| get.txt('when') ||'</b>:</td><td><b>'|| get.txt('picture') ||'</b>:</td><td><b>'|| get.txt('album') ||'</b>:</td><td><b>'|| get.txt('source') ||'</b>:</td></tr>');

                        open picture_search(v_string, get.lan);
                        loop
                                fetch picture_search into v_picture_pk, v_what, v_when_taken, v_organization_pk, v_title, v_album_pk;
                                exit when picture_search%NOTFOUND;
                                htp.p('<tr><td>');
                                if (v_when_taken IS NULL) then
                                        htp.p('-');
                                else
                                        htp.p( to_char(v_when_taken, get.txt('date_year')) );
                                end if;
                                htp.p('</td><td><a href="photo.user_show_picture?p_picture_pk='||v_picture_pk||'">');
                                if (v_what IS NULL) then
                                        htp.p('-');
                                else
                                        htp.p( v_what );
                                end if;



                                htp.p('</a></td><td><a href="photo.user_list_pictures?p_album_pk='||v_album_pk||'">'|| v_title ||'</a></td><td nowrap><a href="org_page.main?p_organization_pk='||v_organization_pk||'">'||get.oname(v_organization_pk)||'</a></td></tr>');
                        end loop;
                        close picture_search;
                elsif (p_type = 'comment') then

                        SELECT  count(DISTINCT(picture_pk)) INTO v_count
                        FROM    picture p, picture_comment pc, picture_in_album pia, album a, usr u, picture_text pt, org_service os
                        WHERE   pc.picture_fk = p.picture_pk
                        AND     pt.picture_fk = p.picture_pk
                        AND     pt.language_fk = v_language_pk
                        AND     pia.picture_fk = p.picture_pk
                        AND     pia.album_fk = a.album_pk
                        AND     pc.user_fk = u.user_pk
                        AND     a.activated = 1
                        AND     (
                                                upper(pc.the_comment) like '%'|| upper(v_string) ||'%'
                                        OR      upper(u.login_name) like '%'|| upper(v_string) ||'%'
                                )
			AND	os.organization_fk = a.organization_fk
			AND	os.service_fk = get.serv;

                        htp.p('<tr><td colspan="3">'||REPLACE(REPLACE(get.txt('search_result'), '[nr]', v_count), '[type]', get.txt('pictures') )||'</td></tr>
                        <tr><td><b>'|| get.txt('when') ||'</b>:</td><td><b>'|| get.txt('picture') ||'</b>:</td><td><b>'|| get.txt('album') ||'</b>:</td><td><b>'|| get.txt('source') ||'</b>:</td></tr>');

                        open comment_search(v_string, get.lan);
                        loop
                                fetch comment_search into v_picture_pk, v_what, v_when_taken, v_organization_pk, v_title, v_album_pk;
                                exit when comment_search%NOTFOUND;
                                htp.p('<tr><td>');
                                if (v_when_taken IS NULL) then
                                        htp.p('-');
                                else
                                        htp.p( to_char(v_when_taken, get.txt('date_year')) );
                                end if;
                                htp.p('</td><td><a href="photo.user_show_picture?p_picture_pk='||v_picture_pk||'">');
                                if (v_what IS NULL) then
                                        htp.p('-');
                                else
                                        htp.p( v_what );
                                end if;
                                htp.p('</a></td><td><a href="photo.user_list_pictures?p_album_pk='||v_album_pk||'">'|| v_title ||'</a></td><td nowrap><a href="org_page.main?p_organization_pk='||v_organization_pk||'">'||get.oname(v_organization_pk)||'</a></td></tr>');
                        end loop;
                        close comment_search;
                end if;
        END IF;

        html.e_table;
        html.e_box;
        html.e_page;
END;
END album;





---------------------------------------------------------
-- Name: 	bboard
-- Type: 	procedure
-- What: 	search in bboard items
-- Author:  	Frode Klevstul
-- Start date: 	20.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE the_bboard
IS
BEGIN

	html.b_page;
	html.search_menu;
	html.b_box( get.txt('search_items'), '100%', 'bboard.search');
	html.b_form('bboard.search', 'search_form');
	htp.p('<input type="hidden" name="p_type" value="show_result">');
	html.b_table;
	htp.p('
		<tr><td colspan="2">'|| get.txt('bboard_search_desc') ||'</td></tr>
		<tr><td>'|| get.txt('trade_type') ||':</td><td>'); bboard.select_trade_type(NULL, 'all'); htp.p('</td></tr>
		<tr><td>'|| get.txt('item_type') ||':</td><td>'); bboard.select_item_type(NULL, 'all'); htp.p('</td></tr>
		<tr><td>'|| get.txt('bboard_string') ||':</td><td>'|| html.text('p_string', 15, 200, NULL) ||'</td></tr>
		<tr><td colspan="2" align="right">
	');
	html.submit_link( get.txt('search_items') );
	htp.p('</td></tr>');
	html.e_table;
	html.e_form;
	html.e_box;
	html.e_page;

END the_bboard;




---------------------------------------------------------------------
---------------------------------------------------------------------
END;
/
/
show errors;
