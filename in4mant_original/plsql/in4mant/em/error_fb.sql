set define off
PROMPT *** package: error_fb ***

CREATE OR REPLACE PACKAGE error_fb IS

PROCEDURE startup;
PROCEDURE register      (       p_command               IN varchar2                                     DEFAULT NULL,
                                p_error_message         IN LONG                                         DEFAULT NULL,
                                p_email_address         IN error_feedback.email_address%TYPE            DEFAULT NULL,
                                p_fixed_by              IN error_feedback.fixed_by%TYPE                 DEFAULT NULL,
                                p_fixed_message         IN LONG                                         DEFAULT NULL,
                                p_error_feedback_pk     IN error_feedback.error_feedback_pk%TYPE        DEFAULT NULL,
                                p_fixed_date            IN error_feedback.fixed_date%TYPE               DEFAULT NULL,
                                p_priority              IN error_feedback.priority%TYPE                 DEFAULT NULL
                        );

END;
/
CREATE OR REPLACE PACKAGE BODY error_fb IS
---------------------------------------------------------------------
---------------------------------------------------------------------
-- Name: startup
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 15.08.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE startup
IS
BEGIN
        register;
END startup;

---------------------------------------------------------------------
-- Name: register
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 15.08.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE register      (       p_command               IN varchar2                                     DEFAULT NULL,
                                p_error_message         IN LONG                                         DEFAULT NULL,
                                p_email_address         IN error_feedback.email_address%TYPE            DEFAULT NULL,
                                p_fixed_by              IN error_feedback.fixed_by%TYPE                 DEFAULT NULL,
                                p_fixed_message         IN LONG                                         DEFAULT NULL,
                                p_error_feedback_pk     IN error_feedback.error_feedback_pk%TYPE        DEFAULT NULL,
                                p_fixed_date            IN error_feedback.fixed_date%TYPE               DEFAULT NULL,
                                p_priority              IN error_feedback.priority%TYPE                 DEFAULT NULL
                        )
IS
BEGIN
DECLARE

v_fixed_message         error_feedback.fixed_message%TYPE       DEFAULT NULL;
v_error_feedback_pk     error_feedback.error_feedback_pk%TYPE   DEFAULT NULL;
v_inserted_date         error_feedback.inserted_date%TYPE       DEFAULT NULL;
v_email_address         error_feedback.email_address%TYPE       DEFAULT NULL;
v_error_message         error_feedback.error_message%TYPE       DEFAULT NULL;
v_fixed_by              error_feedback.fixed_by%TYPE            DEFAULT NULL;
v_fixed_date            error_feedback.fixed_date%TYPE          DEFAULT NULL;
v_priority              error_feedback.priority%TYPE            DEFAULT NULL;
v_file                  utl_file.file_type                      DEFAULT NULL;
v_number                NUMBER                                  DEFAULT NULL;
v_email_dir             VARCHAR2(100)                           DEFAULT NULL;
v_count_all             NUMBER                                  DEFAULT NULL;
v_count_fixed           NUMBER                                  DEFAULT NULL;
v_count_error           NUMBER                                  DEFAULT NULL;
v_priority_txt		varchar2(50)				DEFAULT get.txt('priority');
v_high_txt		varchar2(50)				DEFAULT get.txt('high');
v_medium_txt		varchar2(50)				DEFAULT get.txt('medium');
v_low_txt		varchar2(50)				DEFAULT get.txt('low');
v_date_long_txt		varchar2(50)				DEFAULT get.txt('date_long');
v_error_message_txt	varchar2(50)				DEFAULT get.txt('error_message');
v_fixed_txt		varchar2(50)				DEFAULT get.txt('fixed');
v_edit_txt		varchar2(50)				DEFAULT get.txt('edit');
v_from_txt		varchar2(50)				DEFAULT get.txt('from');
v_inserted_date_txt	varchar2(50)				DEFAULT get.txt('inserted_date');

CURSOR  get_error IS
SELECT  error_feedback_pk, inserted_date, email_address, error_message, priority
FROM    error_feedback
WHERE   fixed_date IS NULL
ORDER BY priority,inserted_date DESC
;

CURSOR  get_old_error IS
SELECT  inserted_date, fixed_date, email_address, error_message,
        fixed_by, fixed_message
FROM    error_feedback
WHERE   fixed_date IS NOT NULL
AND     fixed_date > SYSDATE-30
ORDER BY fixed_date DESC
;

BEGIN

IF ( p_command IS NULL ) THEN
        IF (    login.timeout('error_fb.register') > 0  AND
                login.right('error_fb.register') > 0 ) THEN

                SELECT  count(*)
                INTO    v_count_error
                FROM    error_feedback
                WHERE   fixed_date IS NULL
                ;

                html.b_page(750);
                html.b_box(get.txt('error_feedback'));
                html.b_table;
                htp.p('<tr><td colspan="2"><a href="error_fb.register?p_command=register">'||get.txt('reg_error')||'</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="error_fb.register?p_command=old">'||get.txt('show_fixed_errors')||'</a></td></tr>
                <tr><td colspan="2">'|| REPLACE( get.txt('unresolved_errors'), '[count]', v_count_error)||'</td></tr>
                <tr><td colspan="2"><hr></td></tr>');
                OPEN get_error;
                LOOP
                        FETCH get_error INTO v_error_feedback_pk, v_inserted_date, v_email_address, v_error_message, v_priority;
                        EXIT WHEN get_error%NOTFOUND;
                        html.b_form('error_fb.register','form'|| v_error_feedback_pk);
                        htp.p('<input type="hidden" name="p_command" value="fixed">
                        <input type="hidden" name="p_error_feedback_pk" value="'||v_error_feedback_pk||'">
                        <tr><td><b>'||get_error%ROWCOUNT||'.</b></td><td>'||v_priority_txt||':');
                        IF ( v_priority = 3 ) THEN htp.p('<b>'||v_low_txt||'</b>');
                        ELSIF ( v_priority = 2 ) THEN htp.p('<b><font color="green">'||v_medium_txt||'</font></b>');
                        ELSE htp.p('<b><font color="red">'||v_high_txt||'</font></b>');
                        END IF;

                        htp.p('</td></tr>
                        <tr><td>'|| v_inserted_date_txt ||':</td>
                        <td>'||to_char(v_inserted_date,v_date_long_txt)||'</td></tr>
                        <tr><td>'|| v_from_txt ||':</td><td>'||v_email_address||'</td></tr>
                        <tr><td valign="top">'|| v_error_message_txt ||':</td>
                        <td>'||html.rem_tag(v_error_message)||'</td></tr>
                        <tr><td>'); html.submit_link( v_fixed_txt,'form'|| v_error_feedback_pk);
                        htp.p('</td>');
                        html.e_form;

                        html.b_form('error_fb.register','edit'|| v_error_feedback_pk);
                        htp.p('
                        <input type="hidden" name="p_command" value="edit">
                        <input type="hidden" name="p_error_feedback_pk" value="'||v_error_feedback_pk||'">
                        <td>');
                        html.submit_link( v_edit_txt,'edit'||v_error_feedback_pk);
                        htp.p('</td></tr>');
                        html.e_form;
                       htp.p('<tr><td colspan="2"><hr></td></tr>');
                END LOOP;
                CLOSE get_error;
                html.e_table;
                html.e_box;
                html.e_page;
        END IF;
ELSIF ( p_command = 'old' ) THEN
        IF (    login.timeout('error_fb.register') > 0  AND
                login.right('error_fb.register') > 0 ) THEN
                SELECT  count(*)
                INTO    v_count_all
                FROM    error_feedback
                ;
                SELECT  count(*)
                INTO    v_count_fixed
                FROM    error_feedback
                WHERE   fixed_date IS NOT NULL
                ;

                html.b_page(750);
                html.b_box(get.txt('error_feedback'));
                html.b_table;
                htp.p('<tr><td colspan="2"><a href="error_fb.register?p_command=register">'||get.txt('reg_error')||'</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="error_fb.register">'||get.txt('show_all_errors')||'</a></td></tr>

                <tr><td colspan="2">'|| REPLACE( REPLACE( get.txt('registered_errors'), '[count]', v_count_all), '[count2]', v_count_fixed)||'</td></tr>
                <tr><td colspan="2"><hr></td></tr>');
                OPEN get_old_error;
                LOOP
                        FETCH get_old_error INTO v_inserted_date, v_fixed_date, v_email_address,
                        v_error_message, v_fixed_by, v_fixed_message;
                        EXIT WHEN get_old_error%NOTFOUND;
                        htp.p('<tr><td>'|| get.txt('inserted_date')
                        ||':</td><td>'||to_char(v_inserted_date,get.txt('date_long'))||'</td></tr>
                        <tr><td>'|| get.txt('fixed_date')
                        ||':</td><td>'||to_char(v_fixed_date,get.txt('date_long'))||'</td></tr>
                        <tr><td>'|| get.txt('from') ||':</td><td>'||v_email_address||'</td></tr>
                        <tr><td>'|| get.txt('fixed_by') ||':</td><td>'||v_fixed_by||'</td></tr>
                        <tr><td valign="top">'||get.txt('error_message') ||':</td>
                        <td>'||html.rem_tag(v_error_message)||'</td></tr>
                        <tr><td valign="top">'||get.txt('fixed_message') ||':</td>
                        <td>'||html.rem_tag(v_fixed_message)||'</td></tr>
                        <tr><td colspan="2"><hr></td></tr>');
                END LOOP;
                CLOSE get_old_error;
                html.e_table;
                html.e_box;
                html.e_page;
        END IF;
ELSIF ( p_command = 'fixed' ) THEN
        IF (    login.timeout('error_fb.register') > 0  AND
                login.right('error_fb.register') > 0 ) THEN
                html.b_page(750);
                html.b_box(get.txt('error_feedback'));
                html.b_table;
                SELECT  inserted_date, email_address, error_message
                INTO    v_inserted_date, v_email_address, v_error_message
                FROM    error_feedback
                WHERE   error_feedback_pk = p_error_feedback_pk
                ;
                htp.p(' <tr><td>'|| get.txt('inserted_date') ||':</td>
                <td>'||to_char(v_inserted_date,get.txt('date_long'))||'</td></tr>
                <tr><td>'|| get.txt('from') ||':</td><td>'||v_email_address||'</td></tr>
                <tr><td valign="top">'|| get.txt('error_message')||':</td>
                <td>'||html.rem_tag(v_error_message)||'</td></tr>');

                html.b_form('error_fb.register');
                htp.p('<input type="hidden" name="p_command" value="update">
                <input type="hidden" name="p_error_feedback_pk" value="'||p_error_feedback_pk||'">
                <tr><td>'|| get.txt('fixed_by') ||':</td><td>
                '|| html.text('p_fixed_by', '50', '200', get.uname ) ||'
                </td></tr>
                <tr><td valign="top">'|| get.txt('fixed_message') ||':</td><td>
                <textarea name="p_fixed_message" rows="15" cols="55" wrap="virtual"></textarea>
                </td></tr>
                <tr><td colspan="2">');
                html.submit_link( get.txt('insert'));
                htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page;
        END IF;
ELSIF ( p_command = 'update' ) THEN
        IF (    login.timeout('error_fb.register') > 0  AND
                login.right('error_fb.register') > 0 ) THEN
                UPDATE  error_feedback
                SET     fixed_by = SUBSTR(p_fixed_by,1,200),
                        fixed_message = SUBSTR(p_fixed_message,1,4000),
                        fixed_date = SYSDATE
                WHERE   error_feedback_pk = p_error_feedback_pk
                ;
                commit;
                SELECT  email_address
                INTO    v_email_address
                FROM    error_feedback
                WHERE   error_feedback_pk = p_error_feedback_pk
                ;
                html.b_page(NULL,10,'error_fb.register?p_command=old');
                html.b_table;
                htp.p('<tr><td><a href="error_fb.register?p_command=email&p_error_feedback_pk='||p_error_feedback_pk||'">
                '||get.txt('send_email_to')||' '||v_email_address||'</a></td></tr>
                <tr><td><a href="error_fb.register?p_command=old">Show fixed errors</a></td></tr>
                <tr><td><a href="error_fb.register">Show errors</a></td></tr>');
                html.e_table;
                html.e_page;
        END IF;
ELSIF ( p_command = 'email' ) THEN
        IF (    login.timeout('error_fb.register') > 0  AND
                login.right('error_fb.register') > 0 ) THEN
                SELECT  email_address, error_message, fixed_message
                INTO    v_email_address, v_error_message, v_fixed_message
                FROM    error_feedback
                WHERE   error_feedback_pk = p_error_feedback_pk
                ;
                html.b_page(750);
                html.b_box(get.txt('error_feedback'));
                html.b_table;
                html.b_form('error_fb.register');
                htp.p('<input type="hidden" name="p_command" value="send">
                <input type="hidden" name="p_error_feedback_pk" value="'||p_error_feedback_pk||'">
                <tr><td>To:</td><td>
                '|| v_email_address ||'</td></tr>
                <tr><td>'|| get.txt('error_message') ||':</td><td>
                '|| v_error_message ||'</td></tr>
                <tr><td>'|| get.txt('fixed_message') ||':</td><td>
                '|| v_fixed_message ||'</td></tr>
                <tr><td colspan="2">');
                html.submit_link( get.txt('send_email'));
                htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page;
        END IF;
ELSIF ( p_command = 'send' ) THEN
        IF (    login.timeout('error_fb.register') > 0  AND
                login.right('error_fb.register') > 0 ) THEN
                SELECT  email_address, error_message, fixed_message
                INTO    v_email_address, v_error_message, v_fixed_message
                FROM    error_feedback
                WHERE   error_feedback_pk = p_error_feedback_pk
                ;
                SELECT  email_script_seq.NEXTVAL
                INTO    v_number
                FROM    DUAL;
                v_email_dir := get.value('email_dir');
                v_file := utl_file.fopen( v_email_dir, v_number||'_email-list.txt' , 'w');
                utl_file.put_line(v_file, v_email_address);
                utl_file.fclose(v_file);
                v_file := utl_file.fopen( v_email_dir, v_number||'_message.txt' , 'w');
                utl_file.put_line(v_file, v_fixed_message||'

'||v_error_message );
                utl_file.fclose(v_file);
                html.b_page(NULL,3,'error_fb.startup');
                html.b_table;
                htp.p('<tr><td>The message has been send!</td></tr>');
                html.e_table;
                html.e_page;
        END IF;
ELSIF ( p_command = 'update2' ) THEN
        IF (    login.timeout('error_fb.register') > 0  AND
                login.right('error_fb.register') > 0 ) THEN
                UPDATE  error_feedback
                SET     email_address = SUBSTR(p_email_address,1,200),
                        error_message = SUBSTR(p_error_message,1,4000),
                        priority = p_priority
                WHERE   error_feedback_pk = p_error_feedback_pk
                ;
                commit;
                html.jump_to('error_fb.register');
        END IF;
ELSIF ( p_command = 'register' ) THEN
        html.b_page(750);
        html.b_box(get.txt('error_feedback'));
        html.b_table;
        html.b_form('/cgi-bin/error_fb.register');
        IF ( owa_util.get_cgi_env('REDIRECT_STATUS') IS NOT NULL ) THEN
                v_error_message := 'Error code: '||owa_util.get_cgi_env('REDIRECT_STATUS') ||'
Filename: '||owa_util.get_cgi_env('REDIRECT_URL');
        END IF;
        htp.p('<input type="hidden" name="p_command" value="insert">
        <tr><td>'|| get.txt('email_address') ||':</td><td>
        '|| html.text('p_email_address', '50', '200', get.uemail ) ||'
        </td></tr>
        <tr><td>'|| get.txt('priority') ||':</td>
        <td valign="top">
        <select name="p_priority">
        <option value="1">High
        <option value="2" selected>Medium
        <option value="3">Low
        </select>
        </td></tr>
        <tr><td valign="top">'|| get.txt('error_message') ||':</td><td>
        <textarea name="p_error_message" rows="15" cols="55" wrap="virtual">'||v_error_message||'</textarea>
        </td></tr>
        <tr><td colspan="2">');
        html.submit_link( get.txt('insert'));
        htp.p('</td></tr>');
        html.e_form;
        html.e_table;
        html.e_box;
        html.e_page;
ELSIF ( p_command = 'edit' ) THEN
        IF (    login.timeout('error_fb.register') > 0  AND
                login.right('error_fb.register') > 0 ) THEN

                SELECT  email_address, error_message, priority
                INTO    v_email_address, v_error_message, v_priority
                FROM    error_feedback
                WHERE   error_feedback_pk = p_error_feedback_pk
                ;

                html.b_page(750);
                html.b_box(get.txt('error_feedback'));
                html.b_table;
                html.b_form('error_fb.register');

                htp.p('<input type="hidden" name="p_command" value="update2">
                <input type="hidden" name="p_error_feedback_pk" value="'||p_error_feedback_pk||'">
                <tr><td>'|| get.txt('email_address') ||':</td><td>
                '||html.text('p_email_address', '50', '200', v_email_address )||'
                </td></tr>
                <tr><td>'|| get.txt('priority') ||':</td>
                <td valign="top">
                <select name="p_priority">');
                IF ( v_priority = 1 ) THEN
                        htp.p('<option value="1" selected>High
                        <option value="2">Medium
                        <option value="3">Low');
                ELSIF ( v_priority = 2 OR v_priority IS NULL) THEN
                        htp.p('<option value="1">High
                        <option value="2" selected>Medium
                        <option value="3">Low');
                ELSE
                        htp.p('<option value="1">High
                        <option value="2">Medium
                        <option value="3" selected>Low');
                END IF;
                htp.p('</select>
                </td></tr>
                <tr><td valign="top">'|| get.txt('error_message') ||':</td><td>
                <textarea name="p_error_message" rows="15" cols="55" wrap="virtual">'||v_error_message||'</textarea>
                </td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('update')); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page;
        END IF;
ELSIF ( p_command = 'insert' ) THEN
        html.b_page(750);
        html.b_box(get.txt('error_feedback'));
        html.b_table;
        INSERT INTO error_feedback
        (error_feedback_pk, inserted_date, email_address,
                error_message, priority)
        VALUES
        (error_feedback_seq.NEXTVAL, SYSDATE, SUBSTR(p_email_address,1,200),
        SUBSTR(p_error_message,1,4000), p_priority);
        commit;
        htp.p('
        <tr><td>'|| get.txt('email_address') ||':</td><td>'||p_email_address||'</td></tr>
        <tr><td valign="top">'|| get.txt('error_message') ||':</td><td>'||html.rem_tag(p_error_message)||'</td></tr>
        ');
        html.e_table;
        html.e_box;
        html.e_page;
END IF;

END;
END register;
---------------------------------------------------------------------
---------------------------------------------------------------------
END;
/
show errors;
