set define off

CREATE OR REPLACE FUNCTION banan
       RETURN varchar2
IS
BEGIN
DECLARE

v_code			varchar2(30000) default null;
v_string_group_pk       string_group.string_group_pk%TYPE       DEFAULT NULL;
v_name                  string_group.name%TYPE                  DEFAULT NULL;
v_info			ingredient.info%TYPE			DEFAULT NULL;
v_alcohol		ingredient.alcohol%TYPE			DEFAULT NULL;
v_info_sg_fk		ingredient.info_sg_fk%TYPE		DEFAULT NULL;

CURSOR get_ing IS
SELECT  sg.string_group_pk, sg.name, sg.description, i.info_sg_fk
FROM    glass i, string_group sg
WHERE   i.name_sg_fk=sg.string_group_pk
ORDER BY sg.name
;

BEGIN

OPEN get_ing;
LOOP
	fetch get_ing into v_string_group_pk, v_name, v_info, v_info_sg_fk;
	exit when get_ing%NOTFOUND;
	v_code := v_code ||'INSERT INTO string_group ( string_group_pk, name, description ) 
VALUES ( '||v_info_sg_fk||', '''||v_name||'-INFO'', '''||v_info||''' );
COMMIT;
';
	
END LOOP;
CLOSE get_ing;

RETURN v_code;

END;
END banan;
/
show errors;
