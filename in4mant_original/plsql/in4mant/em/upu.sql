set define off

CREATE OR REPLACE FUNCTION upu ( p_name in VARCHAR2 default NULL )
         RETURN varchar2
IS
BEGIN
DECLARE
        CURSOR get_usr IS
          SELECT u.login_name, u.password, u.user_pk, ud.first_name || ' ' || ud.last_name
            FROM   usr u, user_details ud
            WHERE  u.user_pk = ud.user_fk
            AND    ( upper(ud.last_name) like '%'||upper(p_name)||'%'
            OR     upper(ud.first_name) like '%'||upper(p_name)||'%'
            OR     upper(u.login_name) like '%'||upper(p_name)||'%'
            OR     upper(ud.email) like '%'||upper(p_name)||'%' )
            ;

        v_user_name             usr.login_name%type                     default NULL;
        v_password              usr.password%type                       default NULL;
        v_name                  VARCHAR2(500)                           default NULL;
        v_user_pk               usr.user_pk%TYPE                        default NULL;
        v_code                  varchar2(4000)                          default NULL;
BEGIN
        OPEN get_usr;
        LOOP
                fetch get_usr into v_user_name, v_password, v_user_pk, v_name;
                exit when get_usr%NOTFOUND;
                v_code := v_code || 'Name = '|| v_name ||'
User_pk = '||to_char(v_user_pk)||'
'||v_user_name||' : '||v_password||'
';
        END LOOP;
        close get_usr;
        RETURN v_code;

END;
END upu;

/
show errors;





