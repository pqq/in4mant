set define off
PROMPT *** package: UGH ***

---------------------------------------------------------
CREATE OR REPLACE PACKAGE ugh IS

PROCEDURE startup( P_NAME VARCHAR2);
FUNCTION  serv        RETURN varchar2;
PROCEDURE test;
PROCEDURE banan;
PROCEDURE upload ( p_file VARCHAR2 DEFAULT null) ;

END;
/
show errors;

CREATE OR REPLACE PACKAGE BODY ugh IS

---------------------------------------------------------------------
-- Name: startup
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 15.07.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE startup ( P_NAME VARCHAR2)
IS
 NAME                           VARCHAR2(100);
 NCOMPRESS                      BINARY_INTEGER;
BEGIN
        html.b_page_2;
        htp.p('<center><font size="+7">'||ugh.serv||'</font></center>');
--        htp.download_file('/mn/chomolungma/u11/esjen/kart.txt');
        htp.GET_DOWNLOAD_FILES_LIST(NAME,NCOMPRESS);
        htp.p(NAME||NCOMPRESS);
        html.e_page_2;
END startup;

PROCEDURE test
IS
BEGIN
   htp.p('

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"
"http://www.w3.org/TR/REC-html40/loose.dtd">

<html>

<head>
<title>in4mant.com</title>
</head>

<body text="Black" vlink="Black" alink="#cccccc" link="Black" onload="Javascript: document.forms.upload.submit();">

<form name="upload" action="/cgi/upload2.cgi" method="post" enctype="multipart/form-data">
<input type="hidden" name="p_type" value="picture">
<input type="hidden" name="p_picture_id" value="113-278">
<input type="file" name="p_file" value="/mn/chomolungma/u11/esjen/org2_no.gif">
</FORM>
</BODY>
</html>
');
END test;

---------------------------------------------------------
-- Name:        serv
-- Type:        function
-- What:        returns the service_pk the user has choosen
-- Author:      Frode Klevstul
-- Start date:  01.06.2000
---------------------------------------------------------
FUNCTION serv
        RETURN varchar2
IS
        v_service_pk    service.service_pk%type         default NULL;
        v_server_name   VARCHAR2(30)                    DEFAULT NULL;
        v_check         number                          default NULL;
 OWNER                          VARCHAR2(30);
 NAME                           VARCHAR2(30);
 LINENO                         NUMBER;
 CALLER_T                       VARCHAR2(30);

BEGIN
--        v_server_name := owa_util.get_cgi_env('SERVER_NAME');
--      v_server_name := owa_util.get_env('SCRIPT_NAME');
--      OWNER,NAME,LINENO,CALLER_T := owa_util.WHO_CALLED_ME;
        owa_util.WHO_CALLED_ME(OWNER,NAME,LINENO,CALLER_T);
        SELECT  count( DISTINCT(service_fk) )
        INTO    v_check
        FROM    service_domain
        WHERE   domain = v_server_name;

        if (v_check = 1) then
                SELECT  DISTINCT(service_fk)
                INTO    v_service_pk
                FROM    service_domain
                WHERE   domain = v_server_name;
        else
                SELECT  service_pk
                INTO    v_service_pk
                FROM    service
                WHERE   description = (
                        SELECT value
                        FROM parameter
                        WHERE name = 'default_service'
                        );
        end if;

--        RETURN v_service_pk;
        RETURN CALLER_T;

END serv;

PROCEDURE banan
IS
BEGIN
html.b_page;
htp.p(userenv('entryid')||'<br>');
htp.p(userenv('language')||'<br>');
htp.p(userenv('sessionid')||'<br>');
htp.p(userenv('terminal')||'<br>');
htp.p(uid||'<br>');
htp.p(user||'<br>');
html.e_page;
END banan;

PROCEDURE upload ( p_file VARCHAR2 DEFAULT NULL )
IS
BEGIN
   DECLARE
      v_file                  utl_file.file_type                      default NULL;
      v_email_dir             VARCHAR2(100)                           DEFAULT NULL;

   BEGIN
      htp.htmlopen;
      htp.headopen;
      htp.title('File Uploaded');
      htp.headclose;
      htp.bodyopen;
      htp.header(1, 'Upload Status');

      IF ( p_file IS NULL ) THEN
         htp.print('Uploaded a file');
         htp.p('<FORM enctype="multipart/form-data"
           action="/cgi-bin/ugh.upload" method="POST">
           <p>File TO upload:
           <INPUT TYPE="file" name="file"><br>
           <p>
           <INPUT TYPE="submit">
           </FORM>
           ');
      ELSE
         htp.print('Uploaded ' || p_file || ' successfully');
/*
         v_email_dir := get.value('email_dir')||'/email_error';
         v_file := utl_file.fopen( v_email_dir, 'uploadfile' ,'a');
         utl_file.put(v_file, p_file);
         utl_file.fclose(v_file);
*/
      END IF;
      htp.bodyclose;
      htp.htmlclose;
   END;
END;
-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/
show errors;




