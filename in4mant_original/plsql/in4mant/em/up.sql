set define off

CREATE OR REPLACE FUNCTION up ( p_orgname  in organization.name%type default NULL )
         RETURN varchar2
IS
BEGIN
DECLARE
        CURSOR get_org IS
        SELECT u.login_name, u.password, o.organization_pk, u.user_pk, o.name
        FROM   usr u, organization o, client_user cu
        WHERE  upper(o.name) like '%'||upper(p_orgname)||'%'
        AND    o.organization_pk = cu.organization_fk
        AND    cu.user_fk = u.user_pk
        ;

        v_user_name             usr.login_name%type                     default NULL;
        v_password              usr.password%type                       default NULL;
        v_organization_pk       organization.organization_pk%TYPE       default NULL;
        v_name                  organization.name%TYPE                  default NULL;
        v_user_pk               usr.user_pk%TYPE                        default NULL;
        v_code                  varchar2(4000)                          default NULL;
BEGIN
        OPEN get_org;
        LOOP
                fetch get_org into v_user_name, v_password, v_organization_pk, v_user_pk, v_name;
                exit when get_org%NOTFOUND;
                v_code := v_code ||''||v_name||'
'||v_user_name||' : '||v_password||'
Organization_pk = '||v_organization_pk||'   User_pk = '||v_user_pk||'
';
        END LOOP;
        close get_org;
        RETURN v_code;

END;
END up;

/
show errors;





