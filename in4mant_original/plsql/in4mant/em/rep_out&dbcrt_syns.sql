SQL> 
SQL> select 'CREATE '|| decode(owner,'PUBLIC','PUBLIC ',null) ||
  2         'SYNONYM ' || decode(owner,'PUBLIC',null, owner || '.') ||
  3          lower(synonym_name) || ' FOR ' || lower(table_owner) ||
  4          '.' || lower(table_name) ||
  5          decode(db_link,null,null,'@'||db_link) || ';'
  6    from sys.dba_synonyms
  7    where table_owner != 'SYS'
  8    order by owner
  9  /
CREATE PUBLIC SYNONYM temp$lob FOR system.def$_temp$lob;                                                                            
CREATE PUBLIC SYNONYM product_profile FOR system.product_privs;                                                                     
CREATE PUBLIC SYNONYM product_user_profile FOR system.product_privs;                                                                
CREATE PUBLIC SYNONYM dba_cartridge_components FOR ordsys.dba_cartridge_components;                                                 
CREATE PUBLIC SYNONYM all_md_tables FOR mdsys.all_md_tables;                                                                        
CREATE PUBLIC SYNONYM md FOR mdsys.md;                                                                                              
CREATE PUBLIC SYNONYM md$dictver FOR mdsys.md$dictver;                                                                              
CREATE PUBLIC SYNONYM dba_cartridges FOR ordsys.dba_cartridges;                                                                     
CREATE PUBLIC SYNONYM user_md_tables FOR mdsys.user_md_tables;                                                                      
CREATE PUBLIC SYNONYM user_md_dimensions FOR mdsys.user_md_dimensions;                                                              
CREATE PUBLIC SYNONYM all_md_dimensions FOR mdsys.all_md_dimensions;                                                                
CREATE PUBLIC SYNONYM user_md_columns FOR mdsys.user_md_columns;                                                                    
CREATE PUBLIC SYNONYM all_md_columns FOR mdsys.all_md_columns;                                                                      
CREATE PUBLIC SYNONYM user_md_partitions FOR mdsys.user_md_partitions;                                                              
CREATE PUBLIC SYNONYM all_md_partitions FOR mdsys.all_md_partitions;                                                                
CREATE PUBLIC SYNONYM user_md_tablespaces FOR mdsys.user_md_tablespaces;                                                            
CREATE PUBLIC SYNONYM all_md_tablespaces FOR mdsys.all_md_tablespaces;                                                              
CREATE PUBLIC SYNONYM mdtab FOR mdsys.mdtab;                                                                                        
CREATE PUBLIC SYNONYM sdo_relate_mask FOR mdsys.sdo_relate_mask;                                                                    
CREATE PUBLIC SYNONYM sdo_relatemask_table FOR mdsys.sdo_relatemask_table;                                                          
CREATE PUBLIC SYNONYM md_part FOR mdsys.md_part;                                                                                    
CREATE PUBLIC SYNONYM md_dml FOR mdsys.md_dml;                                                                                      
CREATE PUBLIC SYNONYM md_ddl FOR mdsys.md_ddl;                                                                                      
CREATE PUBLIC SYNONYM dba_md_exceptions FOR mdsys.dba_md_exceptions;                                                                
CREATE PUBLIC SYNONYM dba_md_loader_errors FOR mdsys.dba_md_loader_errors;                                                          
CREATE PUBLIC SYNONYM dba_md_dimensions FOR mdsys.dba_md_dimensions;                                                                
CREATE PUBLIC SYNONYM dba_md_columns FOR mdsys.dba_md_columns;                                                                      
CREATE PUBLIC SYNONYM ctx_object_attributes FOR ctxsys.ctx_object_attributes;                                                       
CREATE PUBLIC SYNONYM ctx_objects FOR ctxsys.ctx_objects;                                                                           
CREATE PUBLIC SYNONYM ctx_classes FOR ctxsys.ctx_classes;                                                                           
CREATE PUBLIC SYNONYM ctx_parameters FOR ctxsys.ctx_parameters;                                                                     
CREATE PUBLIC SYNONYM geocoder_http FOR mdsys.geocoder_http;                                                                        
CREATE PUBLIC SYNONYM geocode_result FOR mdsys.geocode_result;                                                                      
CREATE PUBLIC SYNONYM sdo_migrate FOR mdsys.sdo_migrate;                                                                            
CREATE PUBLIC SYNONYM sdo_index_metadata FOR mdsys.sdo_index_metadata;                                                              
CREATE PUBLIC SYNONYM sdo_catalog FOR mdsys.sdo_catalog;                                                                            
CREATE PUBLIC SYNONYM ctx_stoplists FOR ctxsys.ctx_stoplists;                                                                       
CREATE PUBLIC SYNONYM ctx_user_sections FOR ctxsys.ctx_user_sections;                                                               
CREATE PUBLIC SYNONYM ctx_sections FOR ctxsys.ctx_sections;                                                                         
CREATE PUBLIC SYNONYM ctx_user_section_groups FOR ctxsys.ctx_user_section_groups;                                                   
CREATE PUBLIC SYNONYM ctx_section_groups FOR ctxsys.ctx_section_groups;                                                             
CREATE PUBLIC SYNONYM ctx_user_thesauri FOR ctxsys.ctx_user_thesauri;                                                               
CREATE PUBLIC SYNONYM ctx_thesauri FOR ctxsys.ctx_thesauri;                                                                         
CREATE PUBLIC SYNONYM ctx_user_sqes FOR ctxsys.ctx_user_sqes;                                                                       
CREATE PUBLIC SYNONYM ctx_sqes FOR ctxsys.ctx_sqes;                                                                                 
CREATE PUBLIC SYNONYM main FOR in4mant_adm.main;                                                                                    
CREATE PUBLIC SYNONYM hello FOR system.hello;                                                                                       
CREATE PUBLIC SYNONYM oci_www FOR system.oci_www;                                                                                   
CREATE PUBLIC SYNONYM owa_content FOR websys.owa_content;                                                                           
CREATE PUBLIC SYNONYM context FOR ctxsys.context;                                                                                   
CREATE PUBLIC SYNONYM ctx_thes FOR ctxsys.ctx_thes;                                                                                 
CREATE PUBLIC SYNONYM ctx_query FOR ctxsys.ctx_query;                                                                               
CREATE PUBLIC SYNONYM ctx_output FOR ctxsys.ctx_output;                                                                             
CREATE PUBLIC SYNONYM ctx_ddl FOR ctxsys.ctx_ddl;                                                                                   
CREATE PUBLIC SYNONYM admin FOR in4mant_adm.admin;                                                                                  
CREATE PUBLIC SYNONYM get FOR in4mant_adm.get;                                                                                      
CREATE PUBLIC SYNONYM organization FOR in4mant_adm.organization;                                                                    
CREATE PUBLIC SYNONYM main_page FOR in4mant_adm.main_page;                                                                          
CREATE PUBLIC SYNONYM ctx_doc FOR ctxsys.ctx_doc;                                                                                   
CREATE PUBLIC SYNONYM score FOR ctxsys.score;                                                                                       
CREATE PUBLIC SYNONYM contains FOR ctxsys.contains;                                                                                 
CREATE PUBLIC SYNONYM ctx_user_index_errors FOR ctxsys.ctx_user_index_errors;                                                       
CREATE PUBLIC SYNONYM ctx_user_pending FOR ctxsys.ctx_user_pending;                                                                 
CREATE PUBLIC SYNONYM ctx_user_stopwords FOR ctxsys.ctx_user_stopwords;                                                             
CREATE PUBLIC SYNONYM ctx_stopwords FOR ctxsys.ctx_stopwords;                                                                       
CREATE PUBLIC SYNONYM ctx_user_stoplists FOR ctxsys.ctx_user_stoplists;                                                             
CREATE PUBLIC SYNONYM ctx_user_index_objects FOR ctxsys.ctx_user_index_objects;                                                     
CREATE PUBLIC SYNONYM ctx_user_index_values FOR ctxsys.ctx_user_index_values;                                                       
CREATE PUBLIC SYNONYM ctx_user_indexes FOR ctxsys.ctx_user_indexes;                                                                 
CREATE PUBLIC SYNONYM ctx_user_preference_values FOR ctxsys.ctx_user_preference_values;                                             
CREATE PUBLIC SYNONYM ctx_preference_values FOR ctxsys.ctx_preference_values;                                                       
CREATE PUBLIC SYNONYM ctx_user_preferences FOR ctxsys.ctx_user_preferences;                                                         
CREATE PUBLIC SYNONYM ctx_preferences FOR ctxsys.ctx_preferences;                                                                   
CREATE PUBLIC SYNONYM ctx_object_attribute_lov FOR ctxsys.ctx_object_attribute_lov;                                                 
CREATE PUBLIC SYNONYM ogis_spatial_reference_systems FOR mdsys.ogis_spatial_reference_systems;                                      
CREATE PUBLIC SYNONYM user_geometry_columns FOR mdsys.user_geometry_columns;                                                        
CREATE PUBLIC SYNONYM all_geometry_columns FOR mdsys.all_geometry_columns;                                                          
CREATE PUBLIC SYNONYM dba_geometry_columns FOR mdsys.dba_geometry_columns;                                                          
CREATE PUBLIC SYNONYM sdo_geom FOR mdsys.sdo_geom;                                                                                  
CREATE PUBLIC SYNONYM sdo_admin FOR mdsys.sdo_admin;                                                                                
CREATE PUBLIC SYNONYM sdo FOR mdsys.sdo;                                                                                            
CREATE PUBLIC SYNONYM sdo_3gl FOR mdsys.sdo_3gl;                                                                                    
CREATE PUBLIC SYNONYM dba_md_partitions FOR mdsys.dba_md_partitions;                                                                
CREATE PUBLIC SYNONYM dba_md_tablespaces FOR mdsys.dba_md_tablespaces;                                                              
CREATE PUBLIC SYNONYM dba_md_tables FOR mdsys.dba_md_tables;                                                                        
CREATE PUBLIC SYNONYM user_md_exceptions FOR mdsys.user_md_exceptions;                                                              
CREATE PUBLIC SYNONYM all_md_exceptions FOR mdsys.all_md_exceptions;                                                                
CREATE PUBLIC SYNONYM user_md_loader_errors FOR mdsys.user_md_loader_errors;                                                        
CREATE PUBLIC SYNONYM md_loader_errors FOR mdsys.all_md_loader_errors;                                                              
CREATE PUBLIC SYNONYM all_md_loader_errors FOR mdsys.all_md_loader_errors;                                                          
CREATE SYNONYM SYS.def$_aqcall FOR system.def$_aqcall;                                                                              
CREATE SYNONYM SYS.def$_calldest FOR system.def$_calldest;                                                                          
CREATE SYNONYM SYS.def$_schedule FOR system.def$_schedule;                                                                          
CREATE SYNONYM SYS.def$_error FOR system.def$_error;                                                                                
CREATE SYNONYM SYS.def$_defaultdest FOR system.def$_defaultdest;                                                                    
CREATE SYNONYM SYS.def$_lob FOR system.def$_lob;                                                                                    
CREATE SYNONYM SYSTEM.product_user_profile FOR system.sqlplus_product_profile;                                                      
SQL> spool off
