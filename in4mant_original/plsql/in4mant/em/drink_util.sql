set define off
PROMPT *** package: drink_util ***

CREATE OR REPLACE PACKAGE drink_util IS
empty_array owa_util.ident_arr;
PROCEDURE startup;
PROCEDURE show_drink (     p_drink_pk           in drink.drink_pk%TYPE            DEFAULT NULL );
PROCEDURE random_drink;
PROCEDURE edit_drink (  p_command               IN varchar2                             DEFAULT NULL,
                        p_name                  IN string_group.name%TYPE               DEFAULT NULL,
                        p_description           IN string_group.description%TYPE        DEFAULT NULL,
                        p_string_group_pk       IN string_group.string_group_pk%TYPE    DEFAULT NULL,
                        p_glass_sg_fk           IN drink.glass_sg_fk%TYPE               DEFAULT NULL,
                        p_category_sg_fk        IN drink.category_sg_fk%TYPE            DEFAULT NULL,
                        p_info_sg_fk            IN drink.info_sg_fk%TYPE                DEFAULT NULL,
                        p_drink_pk              IN drink.drink_pk%TYPE                  DEFAULT NULL,
                        p_source                IN drink.source%TYPE                    DEFAULT NULL,
                        p_measurement_fk        IN NUMBER                               DEFAULT NULL,
                        p_letter                IN varchar2                             DEFAULT NULL,
                        p_amount                IN owa_util.ident_arr                   default empty_array,
                        p_measurement_sg_fk     IN owa_util.ident_arr                   default empty_array,
                        p_ingredient_fk         IN owa_util.ident_arr                   default empty_array
                         );
PROCEDURE edit_category (  p_command               IN varchar2                             DEFAULT NULL,
                           p_name                  IN string_group.name%TYPE               DEFAULT NULL,
                           p_description           IN string_group.description%TYPE        DEFAULT NULL,
                           p_string_group_pk       IN string_group.string_group_pk%TYPE    DEFAULT NULL
                         );
PROCEDURE edit_glass    (  p_command               IN varchar2                             DEFAULT NULL,
                           p_name                  IN string_group.name%TYPE               DEFAULT NULL,
                           p_description           IN string_group.description%TYPE        DEFAULT NULL,
                           p_string_group_pk       IN string_group.string_group_pk%TYPE    DEFAULT NULL
                         );
PROCEDURE edit_ing_type (  p_command               IN varchar2                             DEFAULT NULL,
                           p_name                  IN string_group.name%TYPE               DEFAULT NULL,
                           p_description           IN string_group.description%TYPE        DEFAULT NULL,
                           p_string_group_pk       IN string_group.string_group_pk%TYPE    DEFAULT NULL
                         );


PROCEDURE edit_ingredient (  p_command               IN varchar2                             DEFAULT NULL,
                             p_name                  IN string_group.name%TYPE               DEFAULT NULL,
                             p_string_group_pk       IN string_group.string_group_pk%TYPE    DEFAULT NULL,
                             p_ing_type_fk           IN ing_type.name_sg_fk%TYPE             DEFAULT NULL,
                             p_info                  IN ingredient.info%TYPE                 DEFAULT NULL,
                             p_letter                IN varchar2                             DEFAULT NULL
                         );


FUNCTION select_ing_type ( p_ing_type_fk        in ing_type.name_sg_fk%TYPE            DEFAULT NULL )
                           return varchar2;
FUNCTION select_glass (     p_glass_sg_fk           in glass.name_sg_fk%TYPE            DEFAULT NULL )
                            return varchar2;
FUNCTION select_category (     p_category_sg_fk           in category.name_sg_fk%TYPE            DEFAULT NULL )
                         return varchar2;
FUNCTION select_ingredient (     p_ingredient_fk           in ingredient.ingredient_pk%TYPE            DEFAULT NULL )
                           return varchar2;
FUNCTION select_measurement (     p_measurement_sg_fk   in measurement.name_sg_fk%TYPE  DEFAULT NULL )
                            return varchar2;
PROCEDURE edit_measurement (  p_command               IN varchar2                             DEFAULT NULL,
                              p_name                  IN string_group.name%TYPE               DEFAULT NULL,
                              p_description           IN string_group.description%TYPE        DEFAULT NULL,
                              p_string_group_pk       IN string_group.string_group_pk%TYPE    DEFAULT NULL,
                              p_metric_ml             IN measurement.metric_ml%TYPE           DEFAULT NULL
                         );
PROCEDURE browse (  p_command         IN varchar2             DEFAULT 'name',
                    p_pk              IN NUMBER               DEFAULT NULL,
                    p_letter          IN varchar2             DEFAULT NULL
                    );
PROCEDURE search (  p_string          IN varchar2             DEFAULT NULL,
                    p_command         IN varchar2             DEFAULT NULL,
                    p_ingredient_fk   IN owa_util.ident_arr   DEFAULT empty_array
                    );
PROCEDURE search_form (  p_string          IN varchar2             DEFAULT NULL,
                         p_ingredient_fk   IN owa_util.ident_arr   DEFAULT empty_array
                    );
PROCEDURE browse_menu;
END;
/
CREATE OR REPLACE PACKAGE BODY drink_util IS

--------------------------------------------------------------------
-- Setter globale variable
--------------------------------------------------------------------
--v_measurement_array     owa_util.ident_arr      DEFAULT empty_array;
/*
v_measurement_array(375):='3/8';
v_measurement_array(125):='1/8';
v_measurement_array(80):='4/5';
v_measurement_array(75):='3/4';
v_measurement_array(67):='2/3';
v_measurement_array(60):='3/5';
v_measurement_array(50):='1/2';
v_measurement_array(40):='2/5';
v_measurement_array(33):='1/3';
v_measurement_array(25):='1/4';
v_measurement_array(20):='1/5';
v_measurement_array(8):='4/5';
v_measurement_array(6):='3/5';
v_measurement_array(5):='1/2';
v_measurement_array(4):='2/5';
v_measurement_array(2):='1/5';
*/
--------------------------------------------------------------------

---------------------------------------------------------------------
---------------------------------------------------------------------
-- Name: startup
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 15.07.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE startup
IS
BEGIN
IF (    login.timeout('drink_util.startup') > 0 AND
        login.right('drink_util') > 0 ) THEN
        html.b_adm_page;
        html.b_box;
        htp.p('<a href="drink_util.edit_category">edit_category</a><br>');
        htp.p('<a href="drink_util.edit_glass">edit_glass</a><br>');
        htp.p('<a href="drink_util.edit_ing_type">edit_ing_type</a><br>');
        htp.p('<a href="drink_util.edit_ingredient">edit_ingredient</a><br>');
        htp.p('<a href="drink_util.edit_measurement">edit_measurement</a><br>');
        htp.p('<a href="drink_util.edit_drink">edit_drink</a><br>');
        htp.p('<a href="drink_util.browse">browse drink by name</a><br>');
        htp.p('<a href="drink_util.browse?p_command=glass">browse drink by glass</a><br>');
        htp.p('<a href="drink_util.browse?p_command=category">browse drink by category</a><br>');
        htp.p('<a href="drink_util.browse?p_command=ingredient">browse drink by ingredient</a><br>');
        htp.p('<a href="drink_util.browse?p_command=ing_type">browse drink by ingredient type</a><br>');
        html.e_box;
        html.e_page_2;
END IF;
END startup;

---------------------------------------------------------------------
-- Name: show_drink
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 02.03.2002
-- Chng:
---------------------------------------------------------------------

PROCEDURE show_drink (     p_drink_pk           in drink.drink_pk%TYPE            DEFAULT NULL )
IS
BEGIN
DECLARE

v_name_sg_fk            drink.name_sg_fk%TYPE                   DEFAULT NULL;
v_glass_sg_fk           glass.name_sg_fk%TYPE                   DEFAULT NULL;
v_category_sg_fk        category.name_sg_fk%TYPE                DEFAULT NULL;
v_info_sg_fk            drink.info_sg_fk%TYPE                   DEFAULT NULL;
v_info_sg_fk2           ingredient.info_sg_fk%TYPE              DEFAULT NULL;
v_ingredient_sg_fk      ingredient.name_sg_fk%TYPE              DEFAULT NULL;
v_order_in_drink        ing_in_drink.order_in_drink%TYPE        DEFAULT NULL;
v_amount                ing_in_drink.amount%TYPE                DEFAULT NULL;
v_amount2               VARCHAR2(16)                            DEFAULT NULL;
v_decimal               NUMBER                                  DEFAULT NULL;
v_number                NUMBER                                  DEFAULT NULL;
v_measurement_sg_fk     ing_in_drink.measurement_sg_fk%TYPE     DEFAULT NULL;
v_source                drink.source%TYPE                       DEFAULT NULL;
v_measurement_array     owa_util.ident_arr                      DEFAULT empty_array;
v_info                  strng.string%TYPE                       DEFAULT NULL;
v_ingredient            strng.string%TYPE                       DEFAULT NULL;

CURSOR get_drink IS
select d.name_sg_fk, d.glass_sg_fk, d.category_sg_fk, d.info_sg_fk,
       i.name_sg_fk, iid.order_in_drink, iid.amount, d.source, iid.measurement_sg_fk, i.info_sg_fk
from   drink d, ing_in_drink iid, ingredient i
where  d.drink_pk = iid.drink_fk
and    iid.ingredient_fk = i.ingredient_pk
and    d.drink_pk = p_drink_pk
order by iid.order_in_drink
;

BEGIN
/*
FOR v_number IN 1 .. 100
LOOP
  v_measurement_array(v_number):=NULL;
END LOOP;
*/
v_measurement_array(375):='3/8';
v_measurement_array(125):='1/8';
v_measurement_array(80):='4/5';
v_measurement_array(75):='3/4';
v_measurement_array(67):='2/3';
v_measurement_array(60):='3/5';
v_measurement_array(50):='1/2';
v_measurement_array(40):='2/5';
v_measurement_array(33):='1/3';
v_measurement_array(25):='1/4';
v_measurement_array(20):='1/5';
v_measurement_array(8):='4/5';
v_measurement_array(6):='3/5';
v_measurement_array(5):='1/2';
v_measurement_array(4):='2/5';
v_measurement_array(2):='1/5';

html.b_page(NULL, NULL, NULL, NULL, 1);
html.home_menu;
htp.p('<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript">
var ol_textfont="Arial, Utopia, sans-serif";
</script>
<script language="JavaScript" src="/overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
');
htp.p('<table border="0" cellspacing="0" cellpadding="10" width="100%">');
htp.p('<tr><td valign="top" width="30%" align="left">');
drink_util.browse_menu;
htp.p('<br><br>');
drink_util.search_form;
htp.p('</td><td valign="top" width="490">');
html.b_box('','100%','drink_util.show_drink');
html.b_table;

-- Legger inn i statistikk tabellen
stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,31,get.serv);

htp.p( htmf.getcode('popup_print') );

open get_drink;
LOOP
        fetch get_drink INTO v_name_sg_fk, v_glass_sg_fk, v_category_sg_fk,
                v_info_sg_fk, v_ingredient_sg_fk, v_order_in_drink, v_amount,
                v_source, v_measurement_sg_fk, v_info_sg_fk2;
        exit when get_drink%NOTFOUND;
        IF ( v_source IS NULL ) THEN
           v_source := get.txt('unknown');
        END IF;

        IF ( get_drink%ROWCOUNT = 1 ) THEN
           htp.p('<tr><td colspan="2"><span class="boxheading">'||get.text( v_name_sg_fk )||'</span><br></td></tr>
           <tr><td width="20%">'||get.txt('category')||':</td><td width="80%">'||get.text( v_category_sg_fk )||'</td></tr>
           <tr><td>'||get.txt('glass')||':</td><td>'||get.text( v_glass_sg_fk )||'<br></td></tr>
           <tr><td colspan="2">'||get.txt('ingredients')||':</td></tr>
           <tr><td>&nbsp;</td><td align="left">');
        END IF;
        IF ( instr(v_amount,',') > 0 ) THEN
           v_number  := substr(v_amount,1,(instr(v_amount,',')-1));
           v_decimal := substr(v_amount,(instr(v_amount,',')+1),length(v_amount));
           IF ( v_number > 0 ) THEN
              v_amount2 := to_char(v_number)||'&nbsp;'||v_measurement_array(v_decimal);
           ELSE
              v_amount2 := v_measurement_array(v_decimal);
           END IF;
        ELSIF ( v_amount < 1 ) THEN
           v_amount2 := '0'||to_char(v_amount);
        ELSE
           v_amount2 := to_char(v_amount);
        END IF;

        v_info := get.text(v_info_sg_fk2);
        IF ( owa_pattern.match( v_info, 'string_group', 'g') ) THEN
           htp.p(v_amount2||' '||get.text(v_measurement_sg_fk)||' '||get.text(v_ingredient_sg_fk)||'<br>');
        ELSE
           v_ingredient := get.text(v_ingredient_sg_fk);
           owa_pattern.change( v_ingredient, '\015?\n?', '','g');
           owa_pattern.change( v_info, '\015?\n+', '<br>','g');
           v_info := replace (v_info, '"', '&quot;');
           v_info := v_info ||'<br><br><a href=JavaScript:openInfo('||v_info_sg_fk2||');>'||get.txt('print')||'</a>';
           htp.p(v_amount2||' '||get.text(v_measurement_sg_fk)||' <A HREF="javascript:void(0);" onMouseOver="return overlib('''||v_info||''',CAPTION,'''||v_ingredient||''',FGCOLOR,'''||get.value( 'c_norsk_utelivs_tjeneste_bbs' )||''',BGCOLOR,'''||get.value( 'c_norsk_utelivs_tjeneste_mmb' )||''',CAPCOLOR,''#000000'',WIDTH,500,CLOSECOLOR,''#000000'',CLOSETEXT,'''||get.txt('close_window')||''',OFFSETX,-150,STICKY)" onmouseout="return nd();">'||v_ingredient||'</a><br>');

        END IF;

END LOOP;
htp.p('&nbsp;<br></td></tr>');

htp.p('<tr><td colspan="2">'||get.txt('Mixing instructions')||':</td></tr>
<tr><td colspan="2">'||get.text(v_info_sg_fk)||'</td></tr>
<tr><td colspan="2"><br><b><i>'||get.txt('source')||':</i></b>&nbsp;<i>'||v_source||'</i></td></tr>');
close get_drink;
html.e_table;
html.e_box;
htp.p('</td></tr>');
html.e_table;
html.e_page;

END;
END show_drink;

---------------------------------------------------------------------
-- Name: random_drink
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 02.03.2002
-- Chng:
---------------------------------------------------------------------

PROCEDURE random_drink
IS
BEGIN
DECLARE
v_drink_pk              drink.drink_pk%TYPE                     DEFAULT NULL;
v_drink_pk_prev         drink.drink_pk%TYPE                     DEFAULT NULL;
v_name_sg_fk            drink.name_sg_fk%TYPE                   DEFAULT NULL;
v_glass_sg_fk           glass.name_sg_fk%TYPE                   DEFAULT NULL;
v_category_sg_fk        category.name_sg_fk%TYPE                DEFAULT NULL;
v_info_sg_fk            drink.info_sg_fk%TYPE                   DEFAULT NULL;
v_ingredient_sg_fk      ingredient.name_sg_fk%TYPE              DEFAULT NULL;
v_order_in_drink        ing_in_drink.order_in_drink%TYPE        DEFAULT NULL;
v_amount                ing_in_drink.amount%TYPE                DEFAULT NULL;
v_measurement_sg_fk     ing_in_drink.measurement_sg_fk%TYPE     DEFAULT NULL;
v_source                drink.source%TYPE                       DEFAULT NULL;

CURSOR get_drink IS
select d.drink_pk, d.name_sg_fk, d.glass_sg_fk, d.category_sg_fk, d.info_sg_fk,
       i.name_sg_fk, iid.order_in_drink, iid.amount, d.source, iid.measurement_sg_fk
from   drink d, ing_in_drink iid, ingredient i
where  d.drink_pk = iid.drink_fk
and    iid.ingredient_fk = i.ingredient_pk
and    d.drink_pk >= ( select to_number(value) from parameter where name = 'random_drink' )
order by d.drink_pk, iid.order_in_drink
;

BEGIN

html.b_box_2(get.txt('bartender'),'100%','drink_util.show_drink','drink_util.browse');
html.b_table;

open get_drink;
LOOP
        fetch get_drink INTO v_drink_pk, v_name_sg_fk, v_glass_sg_fk, v_category_sg_fk,
                v_info_sg_fk, v_ingredient_sg_fk, v_order_in_drink, v_amount, v_source, v_measurement_sg_fk;
        exit when get_drink%NOTFOUND;
        IF ( v_drink_pk_prev IS NOT NULL AND v_drink_pk <> v_drink_pk_prev ) THEN
           update parameter
           set value = to_char(v_drink_pk)
           where name = 'random_drink';
           commit;
           exit;
        ELSE
           v_drink_pk_prev := v_drink_pk;
        END IF;

        IF ( v_source IS NULL ) THEN
           v_source := get.txt('unknown');
        END IF;
        IF ( get_drink%ROWCOUNT = 1 ) THEN
           htp.p('<tr><td colspan="2"><a href="drink_util.show_drink?p_drink_pk='||v_drink_pk||'"><span class="boldheading">'||get.text( v_name_sg_fk )||'</span></a></td></tr>
           <tr><td colspan="2">'||get.txt('ingredients')||':</td></tr>
           <tr><td>&nbsp;</td><td>');
--         html.b_table(NULL,0);
        END IF;
        IF ( v_amount < 1 ) THEN
--              htp.p('<tr><td align="right">0'||v_amount||' '||get.text(v_measurement_sg_fk)||'</td><td align="left">'||get.text(v_ingredient_sg_fk)||'</td></tr>');
                htp.p('0'||v_amount||' '||get.text(v_measurement_sg_fk)||' '||get.text(v_ingredient_sg_fk)||'<br>');
        ELSE
--              htp.p('<tr><td align="right">'||v_amount||' '||get.text(v_measurement_sg_fk)||'</td><td align="left">'||get.text(v_ingredient_sg_fk)||'</td></tr>');
                htp.p(v_amount||' '||get.text(v_measurement_sg_fk)||' '||get.text(v_ingredient_sg_fk)||'<br>');
        END IF;


END LOOP;

IF ( v_drink_pk = v_drink_pk_prev ) THEN
   update parameter
   set value = '1'
   where name = 'random_drink';
   commit;
END IF;

close get_drink;
htp.p('</td></tr>
<tr><td colspan=2><br>'); html.button_link_2(get.txt('more_drinks'), 'drink_util.browse', NULL);
htp.p('</td></tr>');

html.e_table;
html.e_box_2;

END;
END random_drink;


---------------------------------------------------------------------
-- Name: edit_drink
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 01.03.2002
-- Chng:
---------------------------------------------------------------------
PROCEDURE edit_drink (  p_command               IN varchar2                             DEFAULT NULL,
                        p_name                  IN string_group.name%TYPE               DEFAULT NULL,
                        p_description           IN string_group.description%TYPE        DEFAULT NULL,
                        p_string_group_pk       IN string_group.string_group_pk%TYPE    DEFAULT NULL,
                        p_glass_sg_fk           IN drink.glass_sg_fk%TYPE               DEFAULT NULL,
                        p_category_sg_fk        IN drink.category_sg_fk%TYPE            DEFAULT NULL,
                        p_info_sg_fk            IN drink.info_sg_fk%TYPE                DEFAULT NULL,
                        p_drink_pk              IN drink.drink_pk%TYPE                  DEFAULT NULL,
                        p_source                IN drink.source%TYPE                    DEFAULT NULL,
                        p_measurement_fk        IN NUMBER                               DEFAULT NULL,
                        p_letter                IN varchar2                             DEFAULT NULL,
                        p_amount                IN owa_util.ident_arr                   default empty_array,
                        p_measurement_sg_fk     IN owa_util.ident_arr                   default empty_array,
                        p_ingredient_fk         IN owa_util.ident_arr                   default empty_array
                         )
IS
BEGIN
DECLARE

CURSOR  get_string IS
SELECT  sg.string_group_pk, sg.name, sg.description, d.info_sg_fk, d.drink_pk
FROM    drink d, string_group sg
WHERE   d.name_sg_fk=sg.string_group_pk
AND     UPPER(sg.name) like UPPER(p_letter)||'%'
ORDER BY sg.name
;

CURSOR  get_drink IS
SELECT  sg.string_group_pk, sg.name, sg.description, d.drink_pk, m.name_sg_fk,
        d.glass_sg_fk, d.category_sg_fk, d.info_sg_fk, iid.ingredient_fk,
        iid.amount, iid.order_in_drink, d.source
FROM    drink d, string_group sg, ing_in_drink iid, measurement m
WHERE   d.drink_pk = p_drink_pk
AND     d.drink_pk = iid.drink_fk
AND     d.name_sg_fk = sg.string_group_pk
AND     iid.measurement_sg_fk = m.name_sg_fk
ORDER BY iid.order_in_drink
;

v_drink_pk              drink.drink_pk%TYPE                     DEFAULT NULL;
v_string_group_pk       string_group.string_group_pk%TYPE       DEFAULT NULL;
v_string_pk             strng.string_pk%TYPE                    DEFAULT NULL;
v_string_group_info_pk  string_group.string_group_pk%TYPE       DEFAULT NULL;
v_name                  string_group.name%TYPE                  DEFAULT NULL;
v_description           string_group.description%TYPE           DEFAULT NULL;
v_glass_sg_fk           drink.glass_sg_fk%TYPE                  DEFAULT NULL;
v_category_sg_fk        drink.category_sg_fk%TYPE               DEFAULT NULL;
v_info_sg_fk            drink.info_sg_fk%TYPE                   DEFAULT NULL;
v_number                number                                  default 0;
v_amount                ing_in_drink.amount%TYPE                DEFAULT NULL;
v_source                drink.source%TYPE                       DEFAULT NULL;
v_order_in_drink        ing_in_drink.order_in_drink%TYPE        DEFAULT NULL;
v_ingredient_fk         ing_in_drink.ingredient_fk%TYPE         DEFAULT NULL;
v_measurement_sg_fk     measurement.name_sg_fk%TYPE             DEFAULT NULL;
v_select_ingredient     LONG                                    DEFAULT NULL;
v_select_measurement    varchar2(4000)                          DEFAULT NULL;

BEGIN

IF (    login.timeout('drink_util.edit_drink') > 0 AND
        login.right('drink_util') > 0 ) THEN

        IF ( p_command = 'update') THEN
                html.b_adm_page;
                html.b_box( get.txt('drink') );
                html.b_table;
                html.b_form('drink_util.edit_drink');
                htp.p('<input type="hidden" name="p_command" value="edit">');
                open get_drink;
                LOOP
                        fetch get_drink INTO v_string_group_pk, v_name, v_description,
                                v_drink_pk, v_measurement_sg_fk,
                                v_glass_sg_fk, v_category_sg_fk, v_info_sg_fk, v_ingredient_fk,
                                v_amount, v_order_in_drink, v_source;
                        IF ( get_drink%ROWCOUNT = 1 ) THEN
                           htp.p('<tr><td width="25%">'|| get.txt('name') ||':</td><td align="left" width="75%">
                           <input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                           '|| html.text('p_name', '50', '50', v_name ) ||'
                           </td></tr>
                           <tr><td>'|| get.txt('description') ||':(maks 200 tegn)</td><td>
                           <textarea name="p_description" rows="5" cols="70" wrap="virtual">'||v_description||'</textarea>
                           </td></tr>
                           <tr><td>'|| get.txt('glass') ||':</td><td>
                           '|| select_glass( v_glass_sg_fk ) ||'
                           </td></tr>
                           <tr><td>'|| get.txt('category') ||':</td><td>
                           '|| select_category( v_category_sg_fk ) ||'
                           </td></tr>');
                        ELSIF ( get_drink%NOTFOUND ) THEN
                              v_ingredient_fk := NULL;
                              v_amount := NULL;
                        END IF;
                        v_number := v_number+1;
                        htp.p('<tr><td>Amount:</td><td>
                        '|| html.text('p_amount', '10', '10', v_amount ) ||'
                        '|| select_measurement( v_measurement_sg_fk ) ||'
                        '|| select_ingredient( v_ingredient_fk ) ||'
                        </td></tr>');
                        exit when v_number=15;
                end loop;
                htp.p('
                <tr><td><b><i>'||get.txt('source')||':</i></b></td><td>'|| html.text('p_source', '50', '200', v_source ) ||'</td></tr>
                <input type="hidden" name="p_drink_pk" value="'||v_drink_pk||'">
                <tr><td colspan="2">'); html.submit_link( get.txt('update') ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        ELSIF ( p_command = 'edit') THEN
                UPDATE  string_group
                SET     name=p_name,
                        description=p_description
                WHERE   string_group_pk=p_string_group_pk;
                UPDATE  drink
                SET     glass_sg_fk=p_glass_sg_fk,
                        category_sg_fk=p_category_sg_fk,
                        source=p_source
                WHERE   drink_pk=p_drink_pk;
                COMMIT;
                DELETE from ing_in_drink
                where  drink_fk = p_drink_pk;
                COMMIT;
                LOOP
                        v_number:=v_number +1;
--                           p_amount( v_number ) IS NOT NULL AND
                        IF ( p_ingredient_fk( v_number ) IS NOT NULL AND
                             p_measurement_sg_fk( v_number ) IS NOT NULL ) THEN
                             INSERT INTO ing_in_drink
                             ( ingredient_fk, drink_fk, amount, measurement_sg_fk, order_in_drink )
                             VALUES
                             ( p_ingredient_fk( v_number ), p_drink_pk, REPLACE(p_amount( v_number ),'.',','),
                             p_measurement_sg_fk( v_number ), v_number );
                             COMMIT;
                        END IF;
                        exit when v_number=10;
                END LOOP;
                html.jump_to ( 'drink_util.edit_drink?p_letter='||substr(p_name,1,1));
        ELSIF ( p_command = 'delete') THEN
                IF ( p_name = 'yes') THEN
                        DELETE  FROM ing_in_drink
                        WHERE   drink_fk=p_drink_pk;
                        COMMIT;
                        DELETE  FROM drink
                        WHERE   drink_pk=p_drink_pk;
                        COMMIT;
                        DELETE  FROM string_group
                        WHERE   string_group_pk=p_string_group_pk;
                        COMMIT;
                        DELETE  FROM string_group
                        WHERE   string_group_pk=p_info_sg_fk;
                        COMMIT;
                        html.jump_to ( 'drink_util.edit_drink');
                ELSIF ( p_name IS NULL ) THEN
                        html.jump_to ( 'drink_util.edit_drink');
                ELSE
                        html.b_adm_page;
                        html.b_box( get.txt('drink') );
                        html.b_table;
                        html.b_form('drink_util.edit_drink');
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="yes">
                        <input type="hidden" name="p_drink_pk" value="'||p_drink_pk||'">
                        <input type="hidden" name="p_info_sg_fk" value="'||p_info_sg_fk||'">
                        <input type="hidden" name="p_string_group_pk" value="'||p_string_group_pk||'">
                        <td>'|| get.txt('delete_info') ||' "'||p_name||'" ?'); html.submit_link( get.txt('YES') ); htp.p('</td>');
                        html.e_form;
                        htp.p('<td>'); html.button_link( get.txt('NO'), 'drink_util.edit_drink');htp.p('</td></tr>');
                        html.e_table;
                        html.e_box;
                        html.e_page_2;
                END IF;
        ELSIF ( p_command = 'new') THEN
                html.b_adm_page;
                html.b_box( get.txt('drink') );
                html.b_table;
                html.b_form('drink_util.edit_drink','form_insert');
                htp.p('<input type="hidden" name="p_command" value="insert">
                <tr><td>'|| get.txt('name') ||':</td><td>
                '|| html.text('p_name', '50', '50', '' ) ||'
                </td></tr>
                <tr><td>'|| get.txt('description') ||':</td><td>
                <textarea name="p_description" rows="5" cols="70" wrap="virtual"></textarea>
                </td></tr>
                <tr><td>'|| get.txt('glass') ||':</td><td>
                '|| select_glass ||'
                </td></tr>
                <tr><td>'|| get.txt('category') ||':</td><td>
               '|| select_category ||'
                </td></tr>');

                v_select_ingredient := select_ingredient;
                v_select_measurement := select_measurement;
                loop
                        v_number := v_number+1;
                        htp.p('<tr><td>'|| get.txt('ing_type') ||':</td><td>
                        Amount:
                        '|| html.text('p_amount', '10', '10', '' ) ||'
                        '|| v_select_measurement ||'
                        Off:
                        '|| v_select_ingredient ||'
                        </td></tr>');
                        exit when v_number=10;
                end loop;

                htp.p('<tr><td><b><i>'||get.txt('source')||':</i></b></td><td>'|| html.text('p_source', '50', '200', v_source ) ||'</td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('insert'), 'form_insert' ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        ELSIF ( p_command = 'insert') THEN
                SELECT string_group_seq.NEXTVAL
                INTO v_string_group_pk
                FROM dual;
                INSERT INTO string_group
                ( string_group_pk, name, description )
                VALUES
                ( v_string_group_pk, p_name, SUBSTR(p_description,1,200) );
                COMMIT;

                SELECT strng_seq.NEXTVAL
                  INTO v_string_pk
                  FROM dual;

                INSERT INTO strng
                  (string_pk, language_fk, string)
                  VALUES (v_string_pk, 1, p_name);
                COMMIT;

                INSERT INTO groups
                  (string_group_fk, string_fk)
                  VALUES (v_string_group_pk, v_string_pk);
                COMMIT;

                SELECT strng_seq.NEXTVAL
                  INTO v_string_pk
                  FROM dual;

                INSERT INTO strng
                  (string_pk, language_fk, string)
                  VALUES (v_string_pk, 21, p_name);
                COMMIT;

                INSERT INTO groups
                  (string_group_fk, string_fk)
                  VALUES (v_string_group_pk, v_string_pk);
                COMMIT;


                SELECT string_group_seq.NEXTVAL
                INTO v_info_sg_fk
                FROM dual;
                INSERT INTO string_group
                ( string_group_pk, name, description )
                VALUES
                ( v_info_sg_fk, p_name||'-INFO', SUBSTR(p_description,1,200) );
                COMMIT;

                SELECT drink_seq.NEXTVAL
                INTO v_drink_pk
                FROM dual;
                INSERT INTO drink
                ( drink_pk, name_sg_fk, glass_sg_fk, category_sg_fk, info_sg_fk, inserted_date, source )
                VALUES
                ( v_drink_pk, v_string_group_pk, p_glass_sg_fk, p_category_sg_fk, v_info_sg_fk, sysdate, SUBSTR(p_source,1,200) );
                COMMIT;
                FOR v_number IN 1 .. p_amount.COUNT
                LOOP
--p_amount(v_number) is not null AND
                        IF ( p_ingredient_fk(v_number) is not null AND
                             p_measurement_sg_fk( v_number ) IS NOT NULL ) THEN
                             INSERT INTO ing_in_drink
                             ( ingredient_fk, drink_fk, amount, measurement_sg_fk, order_in_drink)
                             VALUES
                             ( p_ingredient_fk(v_number), v_drink_pk, REPLACE(p_amount( v_number ),'.',','),
                             p_measurement_sg_fk( v_number ), v_number );
                             COMMIT;
                        END IF;
                END LOOP;
                html.jump_to ( 'drink_util.edit_drink?p_letter='||substr(p_name,1,1) );
        ELSE
                html.b_adm_page;
                html.b_box( get.txt('drink') );
                html.b_table;
htp.p('<tr><td colspan="7" align="center">');
                html.b_table;
                htp.p('<tr><td>&nbsp;<br>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=A">A</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=B">B</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=C">C</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=D">D</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=E">E</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=F">F</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=G">G</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=H">H</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=I">I</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=J">J</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=K">K</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=L">L</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=M">M</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=N">N</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=O">O</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=P">P</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=Q">Q</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=R">R</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=S">S</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=T">T</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=U">U</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=V">V</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=W">W</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=X">X</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=Y">Y</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=Z">Z</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=�">�</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=�">�</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=�">�</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <br>&nbsp;</td></tr>');
                html.e_table;
                htp.p('</td></tr>');
                IF ( p_letter is not null ) THEN
                OPEN get_string;
                LOOP
                        fetch get_string INTO v_string_group_pk, v_name, v_description, v_info_sg_fk, v_drink_pk;
                        exit when get_string%NOTFOUND;

                        html.b_form('multilang.add_string','string'|| v_string_group_pk, 'new');
                        htp.p('<input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                        <tr'||system.bgcolor(get_string%ROWCOUNT)||'><td>'||v_name||'</td>
                        <td>'); html.submit_link( get.txt('add_string'), 'form'||v_string_group_pk ); htp.p('</td>');
                        html.e_form;
                        html.b_form('multilang.add_string','string'|| v_string_group_pk, 'new');
                        htp.p('<input type="hidden" name="p_string_group_pk" value="'||v_info_sg_fk||'">
                        <td>'); html.submit_link( get.txt('add_string')||'-info', 'form'||v_info_sg_fk );
                        htp.p('</td>');
                        html.e_form;
                        html.b_form('drink_util.edit_drink','form'|| v_string_group_pk);
                        htp.p('<input type="hidden" name="p_command" value="update">
                        <input type="hidden" name="p_drink_pk" value="'||v_drink_pk||'">
                        <td>'); html.submit_link( get.txt('edit'), 'form'||v_string_group_pk ); htp.p('</td>');
                        html.e_form;
                        html.b_form('drink_util.edit_drink','delete'|| v_string_group_pk);
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="'||v_name||'">
                        <input type="hidden" name="p_drink_pk" value="'||v_drink_pk||'">
                        <input type="hidden" name="p_info_sg_fk" value="'||v_info_sg_fk||'">
                        <input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                        <td>'); html.submit_link( get.txt('delete'), 'delete'||v_string_group_pk ); htp.p('</td>');
                        html.e_form;
                        html.b_form('drink_util.show_drink','show_drink', 'new');
                        htp.p('<input type="hidden" name="p_drink_pk" value="'||v_drink_pk||'">
                        <td>'); html.submit_link( get.txt('show_drink'), 'show_drink' );
                        htp.p('</td></tr>');
                        html.e_form;
                END LOOP;
                close get_string;
                html.b_form('drink_util.edit_drink','form_new');
                htp.p('<input type="hidden" name="p_command" value="new">
                <td>'); html.submit_link( get.txt('new'), 'form_new'); htp.p('</td></tr>');
                html.e_form;
                END IF;
                html.e_table;
                html.e_box;
                html.e_page_2;
        END IF;
END IF;
EXCEPTION
WHEN NO_DATA_FOUND
THEN
htp.p('No data found! drink_util.sql -> edit_drink');
END;
END edit_drink;

---------------------------------------------------------------------
-- Name: edit_category
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 01.03.2002
-- Chng:
---------------------------------------------------------------------
PROCEDURE edit_category (  p_command               IN varchar2                             DEFAULT NULL,
                           p_name                  IN string_group.name%TYPE               DEFAULT NULL,
                           p_description           IN string_group.description%TYPE        DEFAULT NULL,
                           p_string_group_pk       IN string_group.string_group_pk%TYPE    DEFAULT NULL
                         )
IS
BEGIN
DECLARE

CURSOR  get_string IS
SELECT  sg.string_group_pk, sg.name, sg.description, c.info_sg_fk
FROM    category c, string_group sg
WHERE   c.name_sg_fk=sg.string_group_pk
ORDER BY sg.name
;

v_string_group_pk       string_group.string_group_pk%TYPE       DEFAULT NULL;
v_name                  string_group.name%TYPE                  DEFAULT NULL;
v_description           string_group.description%TYPE           DEFAULT NULL;
v_category_pk           category.name_sg_fk%TYPE                DEFAULT NULL;
v_info_sg_fk            category.info_sg_fk%TYPE                DEFAULT NULL;

BEGIN

IF (    login.timeout('drink_util.edit_category') > 0 AND
        login.right('drink_util') > 0 ) THEN

        IF ( p_command = 'update') THEN
                SELECT  sg.string_group_pk, sg.name, sg.description
                INTO    v_string_group_pk, v_name, v_description
                FROM    string_group sg, category c
                WHERE   sg.string_group_pk=p_string_group_pk
                AND     sg.string_group_pk=c.name_sg_fk
                ;
                html.b_adm_page;
                html.b_box( get.txt('category') );
                html.b_table;
                html.b_form('drink_util.edit_category');
                htp.p('<input type="hidden" name="p_command" value="edit">
                <input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                <tr><td>'|| get.txt('name') ||':</td><td>
                '|| html.text('p_name', '50', '50', v_name ) ||'
                </td></tr>
                <tr><td>'|| get.txt('description') ||':(maks 200 tegn)</td><td>
                <textarea name="p_description" rows="5" cols="70" wrap="virtual">'||v_description||'</textarea>
                </td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('update') ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        ELSIF ( p_command = 'edit') THEN
                UPDATE  string_group
                SET     name=p_name,
                        description=p_description
                WHERE   string_group_pk=p_string_group_pk;
                UPDATE  category
                SET     description=p_name
                WHERE   name_sg_fk=p_string_group_pk;
                COMMIT;
                html.jump_to ( 'drink_util.edit_category');
        ELSIF ( p_command = 'delete') THEN
                IF ( p_name = 'yes') THEN
                        DELETE  FROM category
                        WHERE   name_sg_fk=p_string_group_pk;
                        COMMIT;
                        DELETE  FROM string_group
                        WHERE   string_group_pk=p_string_group_pk;
                        COMMIT;
                        html.jump_to ( 'drink_util.edit_category');
                ELSIF ( p_name IS NULL ) THEN
                        html.jump_to ( 'drink_util.edit_category');
                ELSE
                        html.b_adm_page;
                        html.b_box( get.txt('category') );
                        html.b_table;
                        html.b_form('drink_util.edit_category');
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="yes">
                        <input type="hidden" name="p_string_group_pk" value="'||p_string_group_pk||'">
                        <td>'|| get.txt('delete_info') ||' "'||p_name||'" ?'); html.submit_link( get.txt('YES') ); htp.p('</td>');
                        html.e_form;
                        htp.p('<td>'); html.button_link( get.txt('NO'), 'drink_util.edit_category');htp.p('</td></tr>');
                        html.e_table;
                        html.e_box;
                        html.e_page_2;
                END IF;
        ELSIF ( p_command = 'new') THEN
                html.b_adm_page;
                html.b_box( get.txt('category') );
                html.b_table;
                html.b_form('drink_util.edit_category','form_insert');
                htp.p('<input type="hidden" name="p_command" value="insert">
                <tr><td>'|| get.txt('name') ||':</td><td>
                '|| html.text('p_name', '50', '50', '' ) ||'
                </td></tr>
                <tr><td>'|| get.txt('description') ||':</td><td>
                <textarea name="p_description" rows="5" cols="70" wrap="virtual"></textarea>
                </td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('insert'), 'form_insert' ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        ELSIF ( p_command = 'insert') THEN
                SELECT string_group_seq.NEXTVAL
                INTO v_string_group_pk
                FROM dual;
                INSERT INTO string_group
                ( string_group_pk, name, description )
                VALUES
                ( v_string_group_pk, p_name, SUBSTR(p_description,1,200) );
                COMMIT;

                SELECT string_group_seq.NEXTVAL
                INTO v_info_sg_fk
                FROM dual;
                INSERT INTO string_group
                ( string_group_pk, name, description )
                VALUES
                ( v_info_sg_fk, p_name||'-INFO', SUBSTR(p_description,1,200) );
                COMMIT;

                INSERT INTO category
                ( name_sg_fk, description, info_sg_fk )
                VALUES
                ( v_string_group_pk, SUBSTR(p_description,1,4000), v_info_sg_fk );
                COMMIT;
                html.jump_to ( 'drink_util.edit_category' );
        ELSE
                html.b_adm_page;
                html.b_box( get.txt('category') );
                html.b_table;
                OPEN get_string;
                LOOP
                        fetch get_string INTO v_string_group_pk, v_name, v_description, v_info_sg_fk;
                        exit when get_string%NOTFOUND;
                        html.b_form('multilang.add_string','string'|| v_string_group_pk, 'new');
                        htp.p('<input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                        <tr><td>'||v_name||'</td>
                        <td>'); html.submit_link( get.txt('add_string'), 'form'||v_string_group_pk ); htp.p('</td>');
                        html.e_form;
                        html.b_form('multilang.add_string','string'|| v_string_group_pk, 'new');
                        htp.p('<input type="hidden" name="p_string_group_pk" value="'||v_info_sg_fk||'">
                        <td>'); html.submit_link( get.txt('add_string')||'-info', 'form'||v_info_sg_fk );
                        htp.p('</td>');
                        html.e_form;
                        html.b_form('drink_util.edit_category','form'|| v_string_group_pk);
                        htp.p('<input type="hidden" name="p_command" value="update">
                        <input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                        <td>'); html.submit_link( get.txt('edit'), 'form'||v_string_group_pk ); htp.p('</td>');
                        html.e_form;
                        html.b_form('drink_util.edit_category','delete'|| v_string_group_pk);
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="'||v_name||'">
                        <input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                        <td>'); html.submit_link( get.txt('delete'), 'delete'||v_string_group_pk ); htp.p('</td></tr>');
                        html.e_form;
                END LOOP;
                close get_string;
                html.b_form('drink_util.edit_category','form_new');
                htp.p('<input type="hidden" name="p_command" value="new">
                <td>'); html.submit_link( get.txt('new'), 'form_new'); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        END IF;
END IF;
EXCEPTION
WHEN NO_DATA_FOUND
THEN
htp.p('No data found! drink_util.sql -> edit_category');
END;
END edit_category;


---------------------------------------------------------------------
-- Name: edit_glass
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 01.03.2002
-- Chng:
---------------------------------------------------------------------
PROCEDURE edit_glass (  p_command               IN varchar2                             DEFAULT NULL,
                           p_name                  IN string_group.name%TYPE               DEFAULT NULL,
                           p_description           IN string_group.description%TYPE        DEFAULT NULL,
                           p_string_group_pk       IN string_group.string_group_pk%TYPE    DEFAULT NULL
                         )
IS
BEGIN
DECLARE

CURSOR  get_string IS
SELECT  sg.string_group_pk, sg.name, sg.description, g.info_sg_fk
FROM    glass g, string_group sg
WHERE   g.name_sg_fk=sg.string_group_pk
ORDER BY sg.name
;

v_string_group_pk       string_group.string_group_pk%TYPE       DEFAULT NULL;
v_name                  string_group.name%TYPE                  DEFAULT NULL;
v_description           string_group.description%TYPE           DEFAULT NULL;
v_glass_pk              glass.name_sg_fk%TYPE                   DEFAULT NULL;
v_info_sg_fk            glass.info_sg_fk%TYPE                   DEFAULT NULL;

BEGIN

IF (    login.timeout('drink_util.edit_glass') > 0 AND
        login.right('drink_util') > 0 ) THEN

        IF ( p_command = 'update') THEN
                SELECT  sg.string_group_pk, sg.name, sg.description
                INTO    v_string_group_pk, v_name, v_description
                FROM    string_group sg, glass g
                WHERE   sg.string_group_pk=p_string_group_pk
                AND     sg.string_group_pk=g.name_sg_fk
                ;
                html.b_adm_page;
                html.b_box( get.txt('glass') );
                html.b_table;
                html.b_form('drink_util.edit_glass');
                htp.p('<input type="hidden" name="p_command" value="edit">
                <input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                <tr><td>'|| get.txt('name') ||':</td><td>
                '|| html.text('p_name', '50', '50', v_name ) ||'
                </td></tr>
                <tr><td>'|| get.txt('description') ||':(maks 200 tegn)</td><td>
                <textarea name="p_description" rows="5" cols="70" wrap="virtual">'||v_description||'</textarea>
                </td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('update') ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        ELSIF ( p_command = 'edit') THEN
                UPDATE  string_group
                SET     name=p_name,
                        description=p_description
                WHERE   string_group_pk=p_string_group_pk;
                UPDATE  glass
                SET     description=p_name
                WHERE   name_sg_fk=p_string_group_pk;
                COMMIT;
                html.jump_to ( 'drink_util.edit_glass');
        ELSIF ( p_command = 'delete') THEN
                IF ( p_name = 'yes') THEN
                        DELETE  FROM glass
                        WHERE   name_sg_fk=p_string_group_pk;
                        COMMIT;
                        DELETE  FROM string_group
                        WHERE   string_group_pk=p_string_group_pk;
                        COMMIT;
                        html.jump_to ( 'drink_util.edit_glass');
                ELSIF ( p_name IS NULL ) THEN
                        html.jump_to ( 'drink_util.edit_glass');
                ELSE
                        html.b_adm_page;
                        html.b_box( get.txt('glass') );
                        html.b_table;
                        html.b_form('drink_util.edit_glass');
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="yes">
                        <input type="hidden" name="p_string_group_pk" value="'||p_string_group_pk||'">
                        <td>'|| get.txt('delete_info') ||' "'||p_name||'" ?'); html.submit_link( get.txt('YES') ); htp.p('</td>');
                        html.e_form;
                        htp.p('<td>'); html.button_link( get.txt('NO'), 'drink_util.edit_glass');htp.p('</td></tr>');
                        html.e_table;
                        html.e_box;
                        html.e_page_2;
                END IF;
        ELSIF ( p_command = 'new') THEN
                html.b_adm_page;
                html.b_box( get.txt('glass') );
                html.b_table;
                html.b_form('drink_util.edit_glass','form_insert');
                htp.p('<input type="hidden" name="p_command" value="insert">
                <tr><td>'|| get.txt('name') ||':</td><td>
                '|| html.text('p_name', '50', '50', '' ) ||'
                </td></tr>
                <tr><td>'|| get.txt('description') ||':</td><td>
                <textarea name="p_description" rows="5" cols="70" wrap="virtual"></textarea>
                </td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('insert'), 'form_insert' ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        ELSIF ( p_command = 'insert') THEN
                SELECT string_group_seq.NEXTVAL
                INTO v_string_group_pk
                FROM dual;
                INSERT INTO string_group
                ( string_group_pk, name, description )
                VALUES
                ( v_string_group_pk, p_name, SUBSTR(p_description,1,200) );
                COMMIT;

                SELECT string_group_seq.NEXTVAL
                INTO v_info_sg_fk
                FROM dual;
                INSERT INTO string_group
                ( string_group_pk, name, description )
                VALUES
                ( v_info_sg_fk, p_name||'-INFO', SUBSTR(p_description,1,200) );
                COMMIT;

                INSERT INTO glass
                ( name_sg_fk, description, info_sg_fk )
                VALUES
                ( v_string_group_pk, SUBSTR(p_description,1,4000), v_info_sg_fk );
                COMMIT;
                html.jump_to ( 'drink_util.edit_glass' );
        ELSE
                html.b_adm_page;
                html.b_box( get.txt('glass') );
                html.b_table;
                OPEN get_string;
                LOOP
                        fetch get_string INTO v_string_group_pk, v_name, v_description, v_info_sg_fk;
                        exit when get_string%NOTFOUND;
                        html.b_form('multilang.add_string','string'|| v_string_group_pk, 'new');
                        htp.p('<input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                        <tr><td>'||v_name||'</td>
                        <td>'); html.submit_link( get.txt('add_string'), 'form'||v_string_group_pk ); htp.p('</td>');
                        html.e_form;
                        html.b_form('multilang.add_string','string'|| v_string_group_pk, 'new');
                        htp.p('<input type="hidden" name="p_string_group_pk" value="'||v_info_sg_fk||'">
                        <td>'); html.submit_link( get.txt('add_string')||'-info', 'form'||v_info_sg_fk );
                        htp.p('</td>');
                        html.e_form;
                        html.b_form('drink_util.edit_glass','form'|| v_string_group_pk);
                        htp.p('<input type="hidden" name="p_command" value="update">
                        <input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                        <td>'); html.submit_link( get.txt('edit'), 'form'||v_string_group_pk ); htp.p('</td>');
                        html.e_form;
                        html.b_form('drink_util.edit_glass','delete'|| v_string_group_pk);
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="'||v_name||'">
                        <input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                        <td>'); html.submit_link( get.txt('delete'), 'delete'||v_string_group_pk ); htp.p('</td></tr>');
                        html.e_form;
                END LOOP;
                close get_string;
                html.b_form('drink_util.edit_glass','form_new');
                htp.p('<input type="hidden" name="p_command" value="new">
                <td>'); html.submit_link( get.txt('new'), 'form_new'); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        END IF;
END IF;
EXCEPTION
WHEN NO_DATA_FOUND
THEN
htp.p('No data found! drink_util.sql -> edit_glass');
END;
END edit_glass;


---------------------------------------------------------------------
-- Name: edit_ing_type
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 01.03.2002
-- Chng:
---------------------------------------------------------------------
PROCEDURE edit_ing_type (  p_command               IN varchar2                             DEFAULT NULL,
                           p_name                  IN string_group.name%TYPE               DEFAULT NULL,
                           p_description           IN string_group.description%TYPE        DEFAULT NULL,
                           p_string_group_pk       IN string_group.string_group_pk%TYPE    DEFAULT NULL
                         )
IS
BEGIN
DECLARE

CURSOR  get_string IS
SELECT  sg.string_group_pk, sg.name, sg.description
FROM    ing_type it, string_group sg
WHERE   it.name_sg_fk=sg.string_group_pk
ORDER BY sg.name
;

v_string_group_pk       string_group.string_group_pk%TYPE       DEFAULT NULL;
v_name                  string_group.name%TYPE                  DEFAULT NULL;
v_description           string_group.description%TYPE           DEFAULT NULL;
v_ing_type_pk           ing_type.name_sg_fk%TYPE                DEFAULT NULL;

BEGIN

IF (    login.timeout('drink_util.edit_ing_type') > 0 AND
        login.right('drink_util') > 0 ) THEN

        IF ( p_command = 'update') THEN
                SELECT  sg.string_group_pk, sg.name, sg.description
                INTO    v_string_group_pk, v_name, v_description
                FROM    string_group sg, ing_type it
                WHERE   sg.string_group_pk=p_string_group_pk
                AND     sg.string_group_pk=it.name_sg_fk
                ;
                html.b_adm_page;
                html.b_box( get.txt('ing_type') );
                html.b_table;
                html.b_form('drink_util.edit_ing_type');
                htp.p('<input type="hidden" name="p_command" value="edit">
                <input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                <tr><td>'|| get.txt('name') ||':</td><td>
                '|| html.text('p_name', '50', '50', v_name ) ||'
                </td></tr>
                <tr><td>'|| get.txt('description') ||':(maks 200 tegn)</td><td>
                <textarea name="p_description" rows="5" cols="70" wrap="virtual">'||v_description||'</textarea>
                </td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('update') ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        ELSIF ( p_command = 'edit') THEN
                UPDATE  string_group
                SET     name=p_name,
                        description=p_description
                WHERE   string_group_pk=p_string_group_pk;
                UPDATE  ing_type
                SET     description=p_name
                WHERE   name_sg_fk=p_string_group_pk;
                COMMIT;
                html.jump_to ( 'drink_util.edit_ing_type');
        ELSIF ( p_command = 'delete') THEN
                IF ( p_name = 'yes') THEN
                        DELETE  FROM ing_type
                        WHERE   name_sg_fk=p_string_group_pk;
                        COMMIT;
                        DELETE  FROM string_group
                        WHERE   string_group_pk=p_string_group_pk;
                        COMMIT;
                        html.jump_to ( 'drink_util.edit_ing_type');
                ELSIF ( p_name IS NULL ) THEN
                        html.jump_to ( 'drink_util.edit_ing_type');
                ELSE
                        html.b_adm_page;
                        html.b_box( get.txt('ing_type') );
                        html.b_table;
                        html.b_form('drink_util.edit_ing_type');
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="yes">
                        <input type="hidden" name="p_string_group_pk" value="'||p_string_group_pk||'">
                        <td>'|| get.txt('delete_info') ||' "'||p_name||'" ?'); html.submit_link( get.txt('YES') ); htp.p('</td>');
                        html.e_form;
                        htp.p('<td>'); html.button_link( get.txt('NO'), 'drink_util.edit_ing_type');htp.p('</td></tr>');
                        html.e_table;
                        html.e_box;
                        html.e_page_2;
                END IF;
        ELSIF ( p_command = 'new') THEN
                html.b_adm_page;
                html.b_box( get.txt('ing_type') );
                html.b_table;
                html.b_form('drink_util.edit_ing_type','form_insert');
                htp.p('<input type="hidden" name="p_command" value="insert">
                <tr><td>'|| get.txt('name') ||':</td><td>
                '|| html.text('p_name', '50', '50', '' ) ||'
                </td></tr>
                <tr><td>'|| get.txt('description') ||':</td><td>
                <textarea name="p_description" rows="5" cols="70" wrap="virtual"></textarea>
                </td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('insert'), 'form_insert' ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        ELSIF ( p_command = 'insert') THEN
                SELECT string_group_seq.NEXTVAL
                INTO v_string_group_pk
                FROM dual;
                INSERT INTO string_group
                ( string_group_pk, name, description )
                VALUES
                ( v_string_group_pk, p_name, SUBSTR(p_description,1,200) );
                COMMIT;
                INSERT INTO ing_type
                ( name_sg_fk, description )
                VALUES
                ( v_string_group_pk, SUBSTR(p_description,1,4000) );
                COMMIT;
                html.jump_to ( 'drink_util.edit_ing_type' );
        ELSE
                html.b_adm_page;
                html.b_box( get.txt('ing_type') );
                html.b_table;
                OPEN get_string;
                LOOP
                        fetch get_string INTO v_string_group_pk, v_name, v_description;
                        exit when get_string%NOTFOUND;
                        html.b_form('multilang.add_string','string'|| v_string_group_pk, 'new');
                        htp.p('<input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                        <tr><td>'||v_name||'</td>
                        <td>'); html.submit_link( get.txt('add_string'), 'form'||v_string_group_pk ); htp.p('</td>');
                        html.e_form;
                        html.b_form('drink_util.edit_ing_type','form'|| v_string_group_pk);
                        htp.p('<input type="hidden" name="p_command" value="update">
                        <input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                        <td>'); html.submit_link( get.txt('edit'), 'form'||v_string_group_pk ); htp.p('</td>');
                        html.e_form;
                        html.b_form('drink_util.edit_ing_type','delete'|| v_string_group_pk);
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="'||v_name||'">
                        <input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                        <td>'); html.submit_link( get.txt('delete'), 'delete'||v_string_group_pk ); htp.p('</td></tr>');
                        html.e_form;
                END LOOP;
                close get_string;
                html.b_form('drink_util.edit_ing_type','form_new');
                htp.p('<input type="hidden" name="p_command" value="new">
                <td>'); html.submit_link( get.txt('new'), 'form_new'); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        END IF;
END IF;
EXCEPTION
WHEN NO_DATA_FOUND
THEN
htp.p('No data found! drink_util.sql -> edit_ing_type');
END;
END edit_ing_type;

---------------------------------------------------------------------
-- Name: edit_ingredient
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 01.03.2002
-- Chng:
---------------------------------------------------------------------
PROCEDURE edit_ingredient (  p_command               IN varchar2                             DEFAULT NULL,
                             p_name                  IN string_group.name%TYPE               DEFAULT NULL,
                             p_string_group_pk       IN string_group.string_group_pk%TYPE    DEFAULT NULL,
                             p_ing_type_fk           IN ing_type.name_sg_fk%TYPE             DEFAULT NULL,
                             p_info                  IN ingredient.info%TYPE                 DEFAULT NULL,
                             p_letter                IN varchar2                             DEFAULT NULL
                         )
IS
BEGIN
DECLARE

CURSOR  get_string IS
SELECT  sg.string_group_pk, sg.name, sg.description, i.info_sg_fk
FROM    ingredient i, string_group sg
WHERE   i.name_sg_fk=sg.string_group_pk
AND     UPPER(sg.name) like UPPER(p_letter)||'%'
ORDER BY sg.name
;

v_ingredient_pk         ingredient.name_sg_fk%TYPE              DEFAULT NULL;
v_string_group_pk       string_group.string_group_pk%TYPE       DEFAULT NULL;
v_name                  string_group.name%TYPE                  DEFAULT NULL;
v_ing_type_sg_fk        ingredient.ing_type_sg_fk%TYPE          DEFAULT NULL;
v_info                  ingredient.info%TYPE                    DEFAULT NULL;
v_info_sg_fk            ingredient.info_sg_fk%TYPE              DEFAULT NULL;

BEGIN

IF (    login.timeout('drink_util.edit_ingredient') > 0 AND
        login.right('drink_util') > 0 ) THEN

        IF ( p_command = 'update') THEN
                SELECT  sg.string_group_pk, sg.name, i.info, i.ing_type_sg_fk
                INTO    v_string_group_pk, v_name, v_info, v_ing_type_sg_fk
                FROM    string_group sg, ingredient i
                WHERE   sg.string_group_pk=p_string_group_pk
                AND     sg.string_group_pk=i.name_sg_fk
                ;
                html.b_adm_page;
                html.b_box( get.txt('ingredient') );
                html.b_table;
                html.b_form('drink_util.edit_ingredient');
                htp.p('<input type="hidden" name="p_command" value="edit">
                <input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                <tr><td>'|| get.txt('name') ||':</td><td>
                '|| html.text('p_name', '50', '50', v_name ) ||'
                </td></tr>
                <tr><td>'|| get.txt('description') ||':(maks 200 tegn)</td><td>
                <textarea name="p_info" rows="5" cols="70" wrap="virtual">'||v_info||'</textarea>
                </td></tr>
                <tr><td>'|| get.txt('ing_type') ||':</td><td>
                '|| select_ing_type(v_ing_type_sg_fk) ||'
                </td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('update') ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        ELSIF ( p_command = 'edit') THEN
                UPDATE  string_group
                SET     name=p_name,
                        description=substr(p_info,1,200)
                WHERE   string_group_pk=p_string_group_pk;
                UPDATE  ingredient
                SET     ing_type_sg_fk = p_ing_type_fk,
                        info = p_info
                WHERE   name_sg_fk=p_string_group_pk;
                COMMIT;
                html.jump_to ( 'drink_util.edit_ingredient');
        ELSIF ( p_command = 'delete') THEN
                IF ( p_name = 'yes') THEN
                        DELETE  FROM ingredient
                        WHERE   name_sg_fk=p_string_group_pk;
                        COMMIT;
                        DELETE  FROM string_group
                        WHERE   string_group_pk=p_string_group_pk;
                        COMMIT;
                        html.jump_to ( 'drink_util.edit_ingredient');
                ELSIF ( p_name IS NULL ) THEN
                        html.jump_to ( 'drink_util.edit_ingredient');
                ELSE
                        html.b_adm_page;
                        html.b_box( get.txt('ingredient') );
                        html.b_table;
                        html.b_form('drink_util.edit_ingredient');
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="yes">
                        <input type="hidden" name="p_string_group_pk" value="'||p_string_group_pk||'">
                        <td>'|| get.txt('delete_info') ||' "'||p_name||'" ?'); html.submit_link( get.txt('YES') ); htp.p('</td>');
                        html.e_form;
                        htp.p('<td>'); html.button_link( get.txt('NO'), 'drink_util.edit_ingredient');htp.p('</td></tr>');
                        html.e_table;
                        html.e_box;
                        html.e_page_2;
                END IF;
        ELSIF ( p_command = 'new') THEN
                html.b_adm_page;
                html.b_box( get.txt('ingredient') );
                html.b_table;
                html.b_form('drink_util.edit_ingredient','form_insert');
                htp.p('<input type="hidden" name="p_command" value="insert">
                <tr><td>'|| get.txt('name') ||':</td><td>
                '|| html.text('p_name', '50', '50', '' ) ||'
                </td></tr>
                <tr><td>'|| get.txt('description') ||':</td><td>
                <textarea name="p_info" rows="5" cols="70" wrap="virtual"></textarea>
                </td></tr>
                <tr><td>'|| get.txt('ing_type') ||':</td><td>
                '|| select_ing_type ||'
                </td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('insert'), 'form_insert' ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        ELSIF ( p_command = 'insert') THEN
                SELECT string_group_seq.NEXTVAL
                INTO v_string_group_pk
                FROM dual;
                INSERT INTO string_group
                ( string_group_pk, name, description )
                VALUES
                ( v_string_group_pk, p_name, SUBSTR(p_info,1,200) );
                COMMIT;

                SELECT string_group_seq.NEXTVAL
                INTO v_info_sg_fk
                FROM dual;
                INSERT INTO string_group
                ( string_group_pk, name, description )
                VALUES
                ( v_info_sg_fk, p_name||'-INFO', SUBSTR(p_info,1,200) );
                COMMIT;
                INSERT INTO ingredient
                ( ingredient_pk, name_sg_fk, ing_type_sg_fk, info, info_sg_fk )
                VALUES
                ( ingredient_seq.nextval, v_string_group_pk, p_ing_type_fk, SUBSTR(p_info,1,4000), v_info_sg_fk );
                COMMIT;
                html.jump_to ( 'drink_util.edit_ingredient' );
        ELSE
                html.b_adm_page;
                html.b_box( get.txt('ingredient') );
                html.b_table;
                htp.p('<tr><td colspan="5" align="center">');
                html.b_table;
                htp.p('<tr><td>&nbsp;<br>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=A">A</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=B">B</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=C">C</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=D">D</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=E">E</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=F">F</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=G">G</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=H">H</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=I">I</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=J">J</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=K">K</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=L">L</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=M">M</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=N">N</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=O">O</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=P">P</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=Q">Q</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=R">R</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=S">S</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=T">T</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=U">U</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=V">V</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=W">W</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=X">X</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=Y">Y</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=Z">Z</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=�">�</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=�">�</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="?p_letter=�">�</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <br>&nbsp;</td></tr>');
                html.e_table;
                htp.p('</td></tr>');
                IF ( p_letter is not null ) THEN
                OPEN get_string;
                LOOP
                        fetch get_string INTO v_string_group_pk, v_name, v_info, v_info_sg_fk;
                        exit when get_string%NOTFOUND;
                        html.b_form('multilang.add_string','string'|| v_string_group_pk, 'new');
                        htp.p('<input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                        <tr><td>'||v_name||'</td>
                        <td>'); html.submit_link( get.txt('add_string'), 'form'||v_string_group_pk ); htp.p('</td>');
                        html.e_form;
                        html.b_form('multilang.add_string','string'|| v_string_group_pk, 'new');
                        htp.p('<input type="hidden" name="p_string_group_pk" value="'||v_info_sg_fk||'">
                        <td>'); html.submit_link( get.txt('add_string')||'-info', 'form'||v_info_sg_fk );
                        htp.p('</td>');
                        html.e_form;
                        html.b_form('drink_util.edit_ingredient','form'|| v_string_group_pk);
                        htp.p('<input type="hidden" name="p_command" value="update">
                        <input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                        <td>'); html.submit_link( get.txt('edit'), 'form'||v_string_group_pk ); htp.p('</td>');
                        html.e_form;
                        html.b_form('drink_util.edit_ingredient','delete'|| v_string_group_pk);
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="'||v_name||'">
                        <input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                        <td>'); html.submit_link( get.txt('delete'), 'delete'||v_string_group_pk ); htp.p('</td></tr>');
                        html.e_form;
                END LOOP;
                close get_string;
                html.b_form('drink_util.edit_ingredient','form_new');
                htp.p('<input type="hidden" name="p_command" value="new">
                <td>'); html.submit_link( get.txt('new'), 'form_new'); htp.p('</td></tr>');
                html.e_form;
                END IF;
                html.e_table;
                html.e_box;
                html.e_page_2;
        END IF;
END IF;
EXCEPTION
WHEN NO_DATA_FOUND
THEN
htp.p('No data found! drink_util.sql -> edit_ingredient');
END;
END edit_ingredient;

---------------------------------------------------------------------
-- Name: select_ing_type
-- Type: function
-- What:
-- Made: Espen Messel
-- Date: 02.03.2002
-- Chng:
---------------------------------------------------------------------

FUNCTION select_ing_type (     p_ing_type_fk           in ing_type.name_sg_fk%TYPE            DEFAULT NULL )
          return varchar2
IS
BEGIN
DECLARE

CURSOR  get_ing_type IS
SELECT  name_sg_fk
FROM    ing_type;

/*
SELECT  s.string, it.name_sg_fk
FROM    strng s, groups g, ing_type it
WHERE   g.string_fk = s.string_pk
AND     g.string_group_fk = it.name_sg_fk
AND     s.language_fk = 1
ORDER BY string;
*/

v_ing_type_fk        ing_type.name_sg_fk%TYPE               DEFAULT NULL;
v_code               varchar2(4000)                         default null;

BEGIN

        v_code := '<select name="p_ing_type_fk">
';

        OPEN get_ing_type;
        LOOP
                FETCH get_ing_type INTO v_ing_type_fk;
                EXIT WHEN get_ing_type%NOTFOUND;
                IF (v_ing_type_fk = p_ing_type_fk ) THEN
                        v_code := v_code || '<option value="'|| v_ing_type_fk ||'" selected>'|| get.text(v_ing_type_fk) ||'</option>
';
                ELSE
                        v_code := v_code || '<option value="'|| v_ing_type_fk ||'">'|| get.text(v_ing_type_fk) ||'</option>
';
                END IF;
        END LOOP;
        CLOSE get_ing_type;

        return( v_code || '</select>' );


END;
END select_ing_type;

---------------------------------------------------------------------
-- Name: select_glass
-- Type: function
-- What:
-- Made: Espen Messel
-- Date: 02.03.2002
-- Chng:
---------------------------------------------------------------------

FUNCTION select_glass (     p_glass_sg_fk           in glass.name_sg_fk%TYPE            DEFAULT NULL )
          return varchar2
IS
BEGIN
DECLARE

CURSOR  get_glass IS
SELECT  s.string, gl.name_sg_fk
FROM    strng s, groups g, glass gl
WHERE   g.string_fk = s.string_pk
AND     g.string_group_fk = gl.name_sg_fk
AND     s.language_fk = get.lan
ORDER BY string;


v_string        strng.string%TYPE       DEFAULT NULL;
v_glass_sg_fk   glass.name_sg_fk%TYPE   DEFAULT NULL;
v_code          varchar2(30000)         default null;

BEGIN

        v_code := '<select name="p_glass_sg_fk">
';

        OPEN get_glass;
        LOOP
                FETCH get_glass INTO v_string, v_glass_sg_fk;
                EXIT WHEN get_glass%NOTFOUND;
                IF ( v_glass_sg_fk = p_glass_sg_fk ) THEN
                        v_code := v_code || '<option value="'|| v_glass_sg_fk ||'" selected>'|| v_string ||'</option>
';
                ELSE
                        v_code := v_code || '<option value="'|| v_glass_sg_fk ||'">'|| v_string ||'</option>
';
                END IF;
        END LOOP;
        CLOSE get_glass;

        v_code := v_code || '</select>';
        return v_code;
END;
END select_glass;

---------------------------------------------------------------------
-- Name: select_category
-- Type: function
-- What:
-- Made: Espen Messel
-- Date: 02.03.2002
-- Chng:
---------------------------------------------------------------------

FUNCTION select_category (     p_category_sg_fk           in category.name_sg_fk%TYPE            DEFAULT NULL )
          return varchar2
IS
BEGIN
DECLARE

CURSOR  get_category IS
SELECT  s.string, gl.name_sg_fk
FROM    strng s, groups g, category gl
WHERE   g.string_fk = s.string_pk
AND     g.string_group_fk = gl.name_sg_fk
AND     s.language_fk = get.lan
ORDER BY string;


v_string                strng.string%TYPE               DEFAULT NULL;
v_category_sg_fk        category.name_sg_fk%TYPE        DEFAULT NULL;
v_code                  varchar2(30000)                 default null;

BEGIN

        v_code := '<select name="p_category_sg_fk">
';

        OPEN get_category;
        LOOP
                FETCH get_category INTO v_string, v_category_sg_fk;
                EXIT WHEN get_category%NOTFOUND;
                IF ( v_category_sg_fk = p_category_sg_fk ) THEN
                        v_code := v_code || '<option value="'|| v_category_sg_fk ||'" selected>'|| v_string ||'</option>
';
                ELSE
                        v_code := v_code || '<option value="'|| v_category_sg_fk ||'">'|| v_string ||'</option>
';
                END IF;
        END LOOP;
        CLOSE get_category;

        v_code := v_code || '</select>';
        return v_code;
END;
END select_category;


---------------------------------------------------------------------
-- Name: select_ingredient
-- Type: function
-- What:
-- Made: Espen Messel
-- Date: 02.03.2002
-- Chng:
---------------------------------------------------------------------

FUNCTION select_ingredient (     p_ingredient_fk           in ingredient.ingredient_pk%TYPE            DEFAULT NULL )
          return varchar2
IS
BEGIN
DECLARE

CURSOR  get_ingredient IS
SELECT  s.string, gl.name_sg_fk, gl.ingredient_pk
FROM    strng s, groups g, ingredient gl
WHERE   g.string_fk = s.string_pk
AND     g.string_group_fk = gl.name_sg_fk
AND     s.language_fk = get.lan
ORDER BY string;


v_string                strng.string%TYPE               DEFAULT NULL;
v_ingredient_sg_fk      ingredient.name_sg_fk%TYPE      DEFAULT NULL;
v_ingredient_pk         ingredient.ingredient_pk%TYPE   DEFAULT NULL;
v_code                  varchar2(30000)                 default null;

BEGIN

        v_code := '<select name="p_ingredient_fk">
                   <option value="">'||get.txt('no_ingredient')||'</option>
';

        OPEN get_ingredient;
        LOOP
                FETCH get_ingredient INTO v_string, v_ingredient_sg_fk, v_ingredient_pk;
                EXIT WHEN get_ingredient%NOTFOUND;
                IF ( v_ingredient_pk = p_ingredient_fk ) THEN
                        v_code := v_code || '<option value="'|| v_ingredient_pk ||'" selected>'|| v_string ||'</option>
';
                ELSE
                        v_code := v_code || '<option value="'|| v_ingredient_pk ||'">'|| v_string ||'</option>
';
                END IF;
        END LOOP;
        CLOSE get_ingredient;

        v_code := v_code || '</select>';
        return v_code;
END;
END select_ingredient;

---------------------------------------------------------------------
-- Name: select_measurement
-- Type: function
-- What:
-- Made: Espen Messel
-- Date: 02.03.2002
-- Chng:
---------------------------------------------------------------------

FUNCTION select_measurement (     p_measurement_sg_fk   in measurement.name_sg_fk%TYPE  DEFAULT NULL )
         return varchar2
IS
BEGIN
DECLARE

CURSOR  get_measurement IS
SELECT  s.string, m.name_sg_fk
FROM    strng s, groups g, measurement m
WHERE   g.string_fk = s.string_pk
AND     g.string_group_fk = m.name_sg_fk
AND     s.language_fk = get.lan
ORDER BY string;


v_string                strng.string%TYPE               DEFAULT NULL;
v_measurement_sg_fk     measurement.name_sg_fk%TYPE     DEFAULT NULL;
v_code                  varchar2(30000)                 default null;

BEGIN

        v_code := '<select name="p_measurement_sg_fk">
';
        OPEN get_measurement;
        LOOP
                FETCH get_measurement INTO v_string, v_measurement_sg_fk;
                EXIT WHEN get_measurement%NOTFOUND;
                IF ( v_measurement_sg_fk = p_measurement_sg_fk ) THEN
                        v_code := v_code || '<option value="'|| v_measurement_sg_fk ||'" selected>'|| v_string ||'</option>
';
                ELSE
                        v_code := v_code || '<option value="'|| v_measurement_sg_fk ||'">'|| v_string ||'</option>
';
                END IF;
        END LOOP;
        CLOSE get_measurement;

        v_code := v_code || '</select>';
        return v_code;
END;
END select_measurement;

---------------------------------------------------------------------
-- Name: edit_measurement
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 01.03.2002
-- Chng:
---------------------------------------------------------------------
PROCEDURE edit_measurement (  p_command               IN varchar2                             DEFAULT NULL,
                              p_name                  IN string_group.name%TYPE               DEFAULT NULL,
                              p_description           IN string_group.description%TYPE        DEFAULT NULL,
                              p_string_group_pk       IN string_group.string_group_pk%TYPE    DEFAULT NULL,
                              p_metric_ml             IN measurement.metric_ml%TYPE           DEFAULT NULL
                         )
IS
BEGIN
DECLARE

CURSOR  get_string IS
SELECT  sg.string_group_pk, sg.name, sg.description
FROM    measurement m, string_group sg
WHERE   m.name_sg_fk=sg.string_group_pk
ORDER BY sg.name
;

v_string_group_pk       string_group.string_group_pk%TYPE       DEFAULT NULL;
v_name                  string_group.name%TYPE                  DEFAULT NULL;
v_description           string_group.description%TYPE           DEFAULT NULL;
v_measurement_pk        measurement.name_sg_fk%TYPE             DEFAULT NULL;
v_metric_ml             measurement.metric_ml%TYPE              DEFAULT NULL;

BEGIN

IF (    login.timeout('drink_util.edit_measurement') > 0 AND
        login.right('drink_util') > 0 ) THEN

        IF ( p_command = 'update') THEN
                SELECT  sg.string_group_pk, sg.name, sg.description, m.metric_ml
                INTO    v_string_group_pk, v_name, v_description, v_metric_ml
                FROM    string_group sg, measurement m
                WHERE   sg.string_group_pk=p_string_group_pk
                AND     sg.string_group_pk=m.name_sg_fk
                ;
                html.b_adm_page;
                html.b_box( get.txt('measurement') );
                html.b_table;
                html.b_form('drink_util.edit_measurement');
                htp.p('<input type="hidden" name="p_command" value="edit">
                <input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                <tr><td>'|| get.txt('name') ||':</td><td>
                '|| html.text('p_name', '50', '50', v_name ) ||'
                </td></tr>
                <tr><td>'|| get.txt('metric_ml') ||':</td><td>
                '|| html.text('p_metric_ml', '50', '50', v_metric_ml ) ||'
                </td></tr>
                <tr><td>'|| get.txt('description') ||':(maks 200 tegn)</td><td>
                <textarea name="p_description" rows="5" cols="70" wrap="virtual">'||v_description||'</textarea>
                </td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('update') ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        ELSIF ( p_command = 'edit') THEN
                UPDATE  string_group
                SET     name=p_name,
                        description=p_description
                WHERE   string_group_pk=p_string_group_pk;
                UPDATE  measurement
                SET     metric_ml=p_metric_ml
                WHERE   name_sg_fk=p_string_group_pk;
                COMMIT;
                html.jump_to ( 'drink_util.edit_measurement');
        ELSIF ( p_command = 'delete') THEN
                IF ( p_name = 'yes') THEN
                        DELETE  FROM measurement
                        WHERE   name_sg_fk=p_string_group_pk;
                        COMMIT;
                        DELETE  FROM string_group
                        WHERE   string_group_pk=p_string_group_pk;
                        COMMIT;
                        html.jump_to ( 'drink_util.edit_measurement');
                ELSIF ( p_name IS NULL ) THEN
                        html.jump_to ( 'drink_util.edit_measurement');
                ELSE
                        html.b_adm_page;
                        html.b_box( get.txt('measurement') );
                        html.b_table;
                        html.b_form('drink_util.edit_measurement');
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="yes">
                        <input type="hidden" name="p_string_group_pk" value="'||p_string_group_pk||'">
                        <td>'|| get.txt('delete_info') ||' "'||p_name||'" ?'); html.submit_link( get.txt('YES') ); htp.p('</td>');
                        html.e_form;
                        htp.p('<td>'); html.button_link( get.txt('NO'), 'drink_util.edit_measurement');htp.p('</td></tr>');
                        html.e_table;
                        html.e_box;
                        html.e_page_2;
                END IF;
        ELSIF ( p_command = 'new') THEN
                html.b_adm_page;
                html.b_box( get.txt('measurement') );
                html.b_table;
                html.b_form('drink_util.edit_measurement','form_insert');
                htp.p('<input type="hidden" name="p_command" value="insert">
                <tr><td>'|| get.txt('name') ||':</td><td>
                '|| html.text('p_name', '50', '50', '' ) ||'
                </td></tr>
                <tr><td>'|| get.txt('metric_ml') ||':</td><td>
                '|| html.text('p_metric_ml', '50', '50', '' ) ||'
                </td></tr>
                <tr><td>'|| get.txt('description') ||':</td><td>
                <textarea name="p_description" rows="5" cols="70" wrap="virtual"></textarea>
                </td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('insert'), 'form_insert' ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        ELSIF ( p_command = 'insert') THEN
                SELECT string_group_seq.NEXTVAL
                INTO v_string_group_pk
                FROM dual;
                INSERT INTO string_group
                ( string_group_pk, name, description )
                VALUES
                ( v_string_group_pk, p_name, SUBSTR(p_description,1,200) );
                COMMIT;

                INSERT INTO measurement
                ( name_sg_fk, metric_ml )
                VALUES
                ( v_string_group_pk, p_metric_ml );
                COMMIT;
                html.jump_to ( 'drink_util.edit_measurement' );
        ELSE
                html.b_adm_page;
                html.b_box( get.txt('measurement') );
                html.b_table;
                OPEN get_string;
                LOOP
                        fetch get_string INTO v_string_group_pk, v_name, v_description;
                        exit when get_string%NOTFOUND;
                        html.b_form('multilang.add_string','string'|| v_string_group_pk, 'new');
                        htp.p('<input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                        <tr><td>'||v_name||'</td>
                        <td>'); html.submit_link( get.txt('add_string'), 'form'||v_string_group_pk ); htp.p('</td>');
                        html.e_form;
                        html.b_form('drink_util.edit_measurement','form'|| v_string_group_pk);
                        htp.p('<input type="hidden" name="p_command" value="update">
                        <input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                        <td>'); html.submit_link( get.txt('edit'), 'form'||v_string_group_pk ); htp.p('</td>');
                        html.e_form;
                        html.b_form('drink_util.edit_measurement','delete'|| v_string_group_pk);
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="'||v_name||'">
                        <input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                        <td>'); html.submit_link( get.txt('delete'), 'delete'||v_string_group_pk ); htp.p('</td></tr>');
                        html.e_form;
                END LOOP;
                close get_string;
                html.b_form('drink_util.edit_measurement','form_new');
                htp.p('<input type="hidden" name="p_command" value="new">
                <td>'); html.submit_link( get.txt('new'), 'form_new'); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        END IF;
END IF;
EXCEPTION
WHEN NO_DATA_FOUND
THEN
htp.p('No data found! drink_util.sql -> edit_measurement');
END;
END edit_measurement;

---------------------------------------------------------------------
-- Name: browse
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 01.03.2002
-- Chng:
---------------------------------------------------------------------
PROCEDURE browse (  p_command         IN varchar2             DEFAULT 'name',
                    p_pk              IN NUMBER               DEFAULT NULL,
                    p_letter          IN varchar2             DEFAULT NULL
                    )
IS
BEGIN
DECLARE

v_string_group_pk       string_group.string_group_pk%TYPE       DEFAULT NULL;
v_name                  strng.string%TYPE                       DEFAULT NULL;
v_sql                   varchar2(1024)                          DEFAULT NULL;
v_nr                    NUMBER                                  DEFAULT NULL;

TYPE cur_typ IS REF CURSOR;
c_dyn_cursor    cur_typ;

BEGIN

if ( p_pk is null ) then
   if ( p_command = 'glass' ) then
      v_sql := '
                SELECT distinct(STRING),glass_sg_fk
                FROM DRINK d, GROUPS g, strng s
                WHERE d.glass_sg_fk = g.string_group_fk
                ';
   elsif ( p_command = 'category' ) then
      v_sql := '
                SELECT distinct(STRING),category_sg_fk
                FROM DRINK d, GROUPS g, strng s
                WHERE d.category_sg_fk = g.string_group_fk
                ';
   elsif ( p_command = 'name' ) then
      v_sql := '
                SELECT distinct(STRING),drink_pk
                FROM DRINK d, GROUPS g, strng s
                WHERE d.name_sg_fk = g.string_group_fk
                ';
      IF ( p_letter IS NOT NULL ) THEN
              v_sql := v_sql||'AND upper(s.string) like '''||upper(p_letter)||'%''
              ';
      END IF;
   elsif ( p_command = 'ingredient' ) then
      v_sql := '
                SELECT distinct(STRING),ingredient_pk
                FROM ingredient i, GROUPS g, strng s
                WHERE i.name_sg_fk = g.string_group_fk
                ';
   elsif ( p_command = 'ing_type' ) then
      v_sql := '
                SELECT distinct(STRING),ing_type_sg_fk
                FROM ingredient i, GROUPS g, strng s
                WHERE i.ing_type_sg_fk = g.string_group_fk
                ';
   end if;
   v_sql := v_sql||'AND g.string_fk=s.string_pk
                AND s.language_fk = 1
                ORDER BY s.string
                ';
else
   v_sql := '
             SELECT distinct(STRING),drink_pk
             FROM DRINK d, GROUPS g, strng s
             WHERE d.name_sg_fk = g.string_group_fk
             AND g.string_fk=s.string_pk
             AND s.language_fk = 1
             ';
   if ( p_command = 'glass' ) then
      v_sql := v_sql||'AND d.glass_sg_fk = '||p_pk||'
                ORDER BY s.string
                ';
   elsif ( p_command = 'category' ) then
      v_sql := v_sql||'AND d.category_sg_fk = '||p_pk||'
                ORDER BY s.string
                ';
   elsif ( p_command = 'ingredient' ) then
      v_sql := v_sql||'AND d.drink_pk IN (
                SELECT drink_fk
                FROM   ing_in_drink
                WHERE  ingredient_fk = '||p_pk||')
                ORDER BY s.string
                ';
   elsif ( p_command = 'ing_type' ) then
      v_sql := v_sql||'AND d.drink_pk IN (
                SELECT iid.drink_fk
                FROM   ing_in_drink iid, ingredient i
                WHERE  iid.ingredient_fk = i.ingredient_pk
                AND    i.ing_type_sg_fk = '||p_pk||')
                ORDER BY s.string
                ';
   end if;
end if;

-- Legger inn i statistikk tabellen
stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,31,get.serv);

SELECT COUNT(*) INTO v_nr
FROM drink;

html.b_page(NULL, NULL, NULL, NULL, 1);
html.home_menu;
htp.p('<table border="0" cellspacing="0" cellpadding="10" width="100%">');
htp.p('<tr><td valign="top" width="30%" align="left">');
drink_util.browse_menu;
htp.p('<br><br>');
drink_util.search_form;
htp.p('</td><td valign="top" width="490">');

html.b_box(get.txt('browse_by_'||p_command),'100%','drink_util.show_drink');
html.b_table;
htp.p('<tr><td>');
        open c_dyn_cursor for v_sql;
        loop
                fetch c_dyn_cursor into v_name, v_string_group_pk;
                exit when c_dyn_cursor%NOTFOUND;

                IF ( MOD(c_dyn_cursor%ROWCOUNT,2) != 0 ) THEN
                   htp.p('<tr'||system.bgcolor((c_dyn_cursor%ROWCOUNT+1)/2)||'>');
                END IF;
                IF ( p_pk is null AND p_command != 'name' ) then
                   htp.p('<td><a href="drink_util.browse?p_command='||p_command||'&p_pk='||v_string_group_pk||'">'||v_name||'</a></td>');
                ELSE
                   htp.p('<td><a href="drink_util.show_drink?p_drink_pk='||v_string_group_pk||'">'||v_name||'</a></td>');
                END IF;
                IF ( MOD(c_dyn_cursor%ROWCOUNT,2) = 0 ) THEN
                   htp.p('</tr>');
                END IF;
        END LOOP;
        IF ( MOD(c_dyn_cursor%ROWCOUNT,2) != 0 ) THEN
           htp.p('<td>&nbsp;</td></tr>');
        END IF;
        CLOSE c_dyn_cursor;
htp.p('</td></tr>');
html.e_table;
html.e_box;
htp.p('</td></tr>');
html.e_table;
html.e_page;
END;
END browse;


---------------------------------------------------------------------
-- Name: search
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 01.03.2002
-- Chng:
---------------------------------------------------------------------
PROCEDURE search (  p_string          IN varchar2             DEFAULT NULL,
                    p_command         IN varchar2             DEFAULT NULL,
                    p_ingredient_fk   IN owa_util.ident_arr   DEFAULT empty_array
                    )
IS
BEGIN
DECLARE

v_select_ingredient     LONG                                    DEFAULT select_ingredient;
v_string_group_pk       string_group.string_group_pk%TYPE       DEFAULT NULL;
v_name                  strng.string%TYPE                       DEFAULT NULL;
v_sql                   varchar2(1024)                          DEFAULT NULL;

TYPE cur_typ IS REF CURSOR;
c_dyn_cursor    cur_typ;

BEGIN
-- Legger inn i statistikk tabellen
stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,31,get.serv);

html.b_page(NULL, NULL, NULL, NULL, 1);
html.home_menu;
htp.p('<table border="0" cellspacing="0" cellpadding="10" width="100%">');
htp.p('<tr><td valign="top" width="30%" align="left">');
drink_util.browse_menu;
htp.p('<br><br>');
drink_util.search_form(p_string);
htp.p('</td><td valign="top" width="490">');
html.b_box(get.txt('drink_search'),'100%','drink_util.search');
html.b_table;

IF ( p_command = 'search' AND p_string IS NOT NULL ) THEN
      v_sql := '
                SELECT distinct(STRING),drink_pk
                  FROM DRINK d, GROUPS g, strng s
                  WHERE upper(string) LIKE ''%''||upper('''||p_string||''')||''%''
                  AND d.name_sg_fk = g.string_group_fk
                  AND g.string_fk=s.string_pk
                  AND s.language_fk = 1
                  ORDER BY s.string
                  ';
ELSIF ( p_command = 'bartender' AND p_ingredient_fk(1) IS NOT NULL ) THEN
      v_sql := '
                SELECT distinct(STRING),drink_pk
                  FROM DRINK d, GROUPS g, strng s, ing_in_drink iid
                  WHERE d.drink_pk = iid.drink_fk
                  AND iid.ingredient_fk in ('||p_ingredient_fk(1);
      IF ( p_ingredient_fk(2) IS NOT NULL ) then
         v_sql := v_sql || ', '||p_ingredient_fk(2);
      END IF;
      v_sql := v_sql || ')
                  AND d.name_sg_fk = g.string_group_fk
                  AND g.string_fk=s.string_pk
                  AND s.language_fk = 1
                  ORDER BY s.string
                  ';
END IF;

IF ( v_sql IS NOT NULL ) THEN
   OPEN c_dyn_cursor for v_sql;
   LOOP
      fetch c_dyn_cursor into v_name, v_string_group_pk;
      exit when c_dyn_cursor%NOTFOUND;
      IF ( MOD(c_dyn_cursor%ROWCOUNT,2) != 0 ) THEN
         htp.p('<tr'||system.bgcolor((c_dyn_cursor%ROWCOUNT+1)/2)||'>');
      END IF;
      htp.p('<td><a href="drink_util.show_drink?p_drink_pk='||v_string_group_pk||'">'||v_name||'</a></td>');
      IF ( MOD(c_dyn_cursor%ROWCOUNT,2) = 0 ) THEN
         htp.p('</tr>');
      END IF;
   END LOOP;
   IF ( MOD(c_dyn_cursor%ROWCOUNT,2) != 0 ) THEN
      htp.p('<td>&nbsp;</td></tr>');
   END IF;
   CLOSE c_dyn_cursor;
ELSIF ( p_command = 'search' ) THEN
   htp.p('<tr><td>Du m� skrive inn noe i s�kefeltet!</td></tr>');
ELSIF ( p_command = 'bartender' ) THEN
   htp.p('<tr><td>Du m� velge minst en ingrediens!</td></tr>');
END IF;

html.e_table;
html.e_box;
htp.p('</td></tr>');
html.e_table;
html.e_page;
END;
END search;

---------------------------------------------------------------------
-- Name: search_form
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 01.03.2002
-- Chng:
---------------------------------------------------------------------
PROCEDURE search_form (  p_string          IN varchar2             DEFAULT NULL,
                         p_ingredient_fk   IN owa_util.ident_arr   DEFAULT empty_array
                    )
IS
BEGIN

html.b_box_2(get.txt('search_form_drink'),'100%','drink_util.show_drink');
html.b_table;
html.b_form('drink_util.search','search');
htp.p('<tr><td>'||get.txt('find_drink_by_name')||'<br>
<input TYPE="text" name="p_string" width="20" value="'||p_string||'"><br>
<input TYPE="hidden" name="p_command" value="search">
<input TYPE="submit" value="'||get.txt('find_drink')||'"></td></tr>');
html.e_form;
html.b_form('drink_util.search','bartender');
htp.p('<tr><td>'||get.txt('drinks_with_ingredient')||'<br>');
--IF ( p_ingredient_fk(1) IS NOT NULL ) THEN
 --  htp.p(select_ingredient(p_ingredient_fk(1))||'<br>');
--ELSE
   htp.p(select_ingredient||'<br>');
--END IF;
--IF ( p_ingredient_fk(2) IS NOT NULL ) THEN
--   htp.p(select_ingredient(p_ingredient_fk(2))||'<br>');
--ELSE
   htp.p(select_ingredient||'<br>');
--END IF;
htp.p('<input TYPE="hidden" name="p_command" value="bartender">
<input TYPE="submit" value="'||get.txt('find_drink')||'"></td></tr>');
html.e_form;
html.e_table;
html.e_box_2;
END search_form;

---------------------------------------------------------------------
-- Name: browse_menu
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 01.03.2002
-- Chng:
---------------------------------------------------------------------
PROCEDURE browse_menu
IS
BEGIN
DECLARE
   v_nr NUMBER NULL;
BEGIN

SELECT COUNT(*) INTO v_nr
FROM drink;

html.b_box_2(get.txt('browse_drink'),'100%','drink_util.show_drink');
html.b_table;
htp.p('
<tr><td><a href="drink_util.browse?p_command=name">'||get.txt('browse_by_name')||'</a></td></tr>
<tr><td><a href="drink_util.browse?p_command=glass">'||get.txt('browse_by_glass')||'</a></td></tr>
<tr><td><a href="drink_util.browse?p_command=category">'||get.txt('browse_by_category')||'</a></td></tr>
<tr><td><a href="drink_util.browse?p_command=ingredient">'||get.txt('browse_by_ingredient')||'</a></td></tr>
<tr><td><a href="drink_util.browse?p_command=ing_type">'||get.txt('browse_by_ing_type')||'</a></td></tr>
<tr><td>'||v_nr||'&nbsp;'||get.txt('registered_drinks')||'</td></tr>
<tr><td><a href="http://bytur.no/cgi-bin/main.contact_us">'||get.txt('drink_tip')||'</a></td></tr>');
html.e_table;
html.e_box_2;
END;
END browse_menu;

---------------------------------------------------------------------
---------------------------------------------------------------------
END;
/
show errors;
