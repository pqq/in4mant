create index error_feedback_idx1 on error_feedback ( error_feedback_pk );
create index error_feedback_idx2 on error_feedback ( fixed_date, priority,inserted_date DESC );
create index error_feedback_idx3 on error_feedback ( priority,inserted_date DESC );


SELECT  error_feedback_pk, inserted_date, email_address, error_message, priority
FROM    error_feedback
WHERE   fixed_date IS NULL
ORDER BY priority,inserted_date DESC
;



create INDEX stat_org_idx11 on stat_org ( page_type_fk, stat_date );





create INDEX stat_org_idx10 on stat_org ( stat_date, service_fk );
ALTER TABLE stat_org DROP INDEX stat_org_idx10;


SELECT  SUM(so.volume)
FROM    stat_org so
WHERE   so.stat_date > to_date('20050301','YYYYMMDD')
AND     so.stat_date <= to_date('20050401','YYYYMMDD')
AND     so.service_fk = 3


SELECT  SUM(volume)
FROM    stat_org
WHERE   page_type_fk = 31
AND     stat_date > to_date('20050301','YYYYMMDD')
AND     stat_date <= to_date('20050401','YYYYMMDD')
;
