CREATE OR REPLACE PACKAGE contact IS
PROCEDURE us (  p_message               IN VARCHAR2     DEFAULT NULL,
                p_command               IN VARCHAR2     DEFAULT NULL,
                p_email_address         IN VARCHAR2     DEFAULT NULL,
                p_name                  IN VARCHAR2     DEFAULT NULL
                );
PROCEDURE startup;
END;
/
CREATE OR REPLACE PACKAGE BODY contact IS
---------------------------------------------------------------------
---------------------------------------------------------------------
-- Name: startup
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 30.08.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE startup
IS
BEGIN
        us;
END startup;

---------------------------------------------------------------------
-- Name: us
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 30.08.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE us (  p_message               IN VARCHAR2     DEFAULT NULL,
                p_command               IN VARCHAR2     DEFAULT NULL,
                p_email_address         IN VARCHAR2     DEFAULT NULL,
                p_name                  IN VARCHAR2     DEFAULT NULL
                )
IS
BEGIN
DECLARE

v_file          utl_file.file_type              default NULL;
v_number        NUMBER                          DEFAULT NULL;
v_message       VARCHAR2(4000)                  DEFAULT NULL;
v_email_dir     VARCHAR2(100)                   DEFAULT NULL;

BEGIN
-- Legger inn i statistikk tabellen
stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,22);

IF ( p_command IS NULL OR p_command = 'adm' OR p_command = 'empty' OR p_command = 'www' ) THEN

        if (p_command = 'adm') then			-- siden er tilpasset admin topp
		html.b_page(NULL, NULL, NULL, 2);
	elsif (p_command = 'empty') then		-- siden er tilpasset alle vanlige sider (p� en tjeneste)
		html.b_page;
		html.empty_menu;
	elsif (p_command = 'www') then			-- siden er tilpasset www.in4mant.com
		html.b_page(NULL, NULL, NULL, 3);
	end if;

        html.b_box(get.txt('contact_us'),'100%','contact.us');
        html.b_table;
        html.b_form('contact.us');
        htp.p('
        <tr><td colspan="2">'|| get.txt('contact_info')||'</td></tr>
        <input type="hidden" name="p_command" value="send">
        <tr><td>'|| get.txt('from') ||':</td><td>
        '|| html.text('p_name', '50', '200', p_name ) ||'
        </td></tr>
        <tr><td>'|| get.txt('email_address') ||':</td><td>
        '|| html.text('p_email_address', '50', '200', p_email_address ) ||'
        </td></tr>
        <tr><td valign="top">'|| get.txt('message') ||':</td><td>
        <textarea name="p_message" rows="15" cols="55" wrap="virtual"></textarea>
        </td></tr>
        <tr><td colspan="2" align="right">');
        html.submit_link( get.txt('send') );
        htp.p('</td></tr>');
        html.e_form;
        html.e_table;
        html.e_box;
        html.e_page;
ELSE
        html.b_page;
        html.empty_menu;
        html.b_box(get.txt('contact_us'),'100%','contact.us');
        html.b_table;
        htp.p('<tr><td colspan="2">'|| get.txt('send_message') ||'</td></tr>
        <tr><td>'|| get.txt('from') ||':</td><td>
        '|| trans(p_name) ||'
        </td></tr>
        <tr><td>'|| get.txt('email_address') ||':</td><td>
        '|| trans(p_email_address) ||'
        </td></tr>
        <tr><td valign="top">'|| get.txt('message') ||':</td><td>
        '|| html.rem_tag(trans(substr(p_message,1,4000))) ||'
        </td></tr>');
        html.e_table;
        html.e_box;
        html.e_page;

        SELECT  email_script_seq.NEXTVAL
        INTO    v_number
        FROM    DUAL;
        v_message := 'From: info@in4mant.com
Subject: Contact us mail!

'||get.txt('from') ||':'||trans(p_name)||' - '||trans(p_email_address);
        v_message := v_message ||'
'||get.txt('message') ||':
'||trans(p_message);
        v_message := v_message ||'

'||get.txt('send_from') ||':'||owa_util.get_cgi_env('REMOTE_HOST');
        v_email_dir := get.value('email_dir');
        v_file := utl_file.fopen( v_email_dir, v_number||'_email-list.txt' , 'w');
        utl_file.put_line(v_file, get.value('email_contact_us'));
        utl_file.fclose(v_file);
        v_file := utl_file.fopen( v_email_dir, v_number||'_message.txt' , 'w');
        utl_file.put_line(v_file, v_message );
        utl_file.fclose(v_file);
END IF;
END;
END us;

---------------------------------------------------------------------
---------------------------------------------------------------------
END;
/
