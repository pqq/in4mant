SET define off
PROMPT *** PACKAGE: up ***

CREATE OR REPLACE PACKAGE up_pack IS
  PROCEDURE startup ( p_orgname  IN organization.name%TYPE DEFAULT NULL );
  PROCEDURE php ( p_orgname  IN organization.name%TYPE DEFAULT NULL );
  PROCEDURE test (
                  p_login_name            IN usr.login_name%TYPE          DEFAULT NULL,
                  p_password              IN usr.password%TYPE            DEFAULT NULL
                  );
END;
/

CREATE OR REPLACE PACKAGE BODY up_pack IS

PROCEDURE test (
                  p_login_name            IN usr.login_name%TYPE          DEFAULT NULL,
                  p_password              IN usr.password%TYPE            DEFAULT NULL
                  )
IS
BEGIN
DECLARE

        v_session_id            sessn.session_id%TYPE   DEFAULT NULL;
        v_cookie        owa_cookie.cookie;              -- cookien som vi henter
        v_value         NUMBER;                                 -- sesjonsnummeret vi henter fra cookien

BEGIN

        v_session_id := get.sess;
        IF ( v_session_id IS NOT NULL) then
                -- oppdaterer database
                UPDATE  sessn
                SET     last_action = '18-AUG-1976'
                WHERE   session_id = v_session_id;
                COMMIT;

                v_cookie := owa_cookie.get('in4mant_session');     -- henter ut cookien
                v_value := (v_cookie.vals(1));          -- henter ut cookiens verdi

                owa_util.mime_header('text/html', FALSE);
                owa_cookie.remove('in4mant_session',v_value);   -- fjerner cookien

        END IF;

  login.login_action(p_login_name, p_password, 'main_page.startup');
END;
END;



--- Tester
PROCEDURE php ( p_orgname  IN organization.name%TYPE DEFAULT NULL )
IS
BEGIN

up_pack.startup(p_orgname);

END;

---------------------------------------------------------------------
-- Name: startup
-- Type: procedure
-- What: prosedyre for � starte pakken
-- Made: Espen Messel
-- Date: 01.07.03
-- Chng:
---------------------------------------------------------------------

PROCEDURE startup ( p_orgname  IN organization.name%TYPE DEFAULT NULL )
IS
BEGIN
DECLARE
        CURSOR get_org IS
        SELECT u.login_name, u.password, o.organization_pk, u.user_pk, o.name
        FROM   usr u, organization o, client_user cu
        WHERE  upper(o.name) like '%'||upper(p_orgname)||'%'
        AND    o.organization_pk = cu.organization_fk
        AND    cu.user_fk = u.user_pk
        ;

        v_user_name             usr.login_name%type                     default NULL;
        v_password              usr.password%type                       default NULL;
        v_organization_pk       organization.organization_pk%TYPE       default NULL;
        v_name                  organization.name%TYPE                  default NULL;
        v_user_pk               usr.user_pk%TYPE                        default NULL;

BEGIN
IF ( login.timeout('up_pack.startup') > 0 AND login.right('up_pack') > 0 ) THEN
   html.b_adm_page;
   html.b_box( get.txt('up_pack') );

   html.b_table;
   html.b_form('up_pack.startup');
   htp.p('<tr><td>Skriv inn navn:</td><td><input type="text" name="p_orgname" value="'||p_orgname||'"></td><td>');
   html.submit_link( 'S�k' );
   htp.p('</td></tr>');
   html.e_form;
   html.e_table;

   IF ( p_orgname IS NOT NULL ) THEN

   html.b_table;
   htp.p('<tr><td>orgname</td><td>user_name</td><td>password</td><td>organization_pk</td><td>user_pk</td></tr>');
        OPEN get_org;
        LOOP
                fetch get_org into v_user_name, v_password, v_organization_pk, v_user_pk, v_name;
                exit when get_org%NOTFOUND;
                htp.p('<tr><td><a href="up_pack.test?p_login_name='||v_user_name||'&p_password='||v_password||'">'||v_name||'</a></td><td>'||v_user_name||'</td><td>'||v_password||'</td><td>'||v_organization_pk||'</td><td>'||v_user_pk||'</td></tr>');
        END LOOP;
        close get_org;
   html.e_table;

   END IF;

   html.e_box;
   html.e_page_2;

END IF;
END;
END startup;

END;
/
show errors;





