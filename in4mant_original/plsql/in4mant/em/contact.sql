set define off
PROMPT *** package: contact ***

CREATE OR REPLACE PACKAGE contact IS
empty_array owa_util.ident_arr;
PROCEDURE us (  p_message               IN VARCHAR2     DEFAULT NULL,
                p_command               IN VARCHAR2     DEFAULT NULL,
                p_email_address         IN VARCHAR2     DEFAULT NULL,
                p_name                  IN VARCHAR2     DEFAULT NULL
                );
PROCEDURE startup;
PROCEDURE email_org (
                    p_command                   IN varchar2             DEFAULT NULL,
                    p_from                      IN varchar2             DEFAULT NULL,
                    p_subject                   IN varchar2             DEFAULT NULL,
                    p_message                   IN varchar2             DEFAULT NULL,
                    p_organization_pk           IN owa_util.ident_arr   default empty_array
          );
END;
/

CREATE OR REPLACE PACKAGE BODY contact IS
---------------------------------------------------------------------
---------------------------------------------------------------------
-- Name: startup
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 30.08.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE startup
IS
BEGIN
        us;
END startup;

---------------------------------------------------------------------
-- Name: us
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 30.08.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE us (  p_message               IN VARCHAR2     DEFAULT NULL,
                p_command               IN VARCHAR2     DEFAULT NULL,
                p_email_address         IN VARCHAR2     DEFAULT NULL,
                p_name                  IN VARCHAR2     DEFAULT NULL
                )
IS
BEGIN
DECLARE

v_file          utl_file.file_type              default NULL;
v_number        NUMBER                          DEFAULT NULL;
v_message       VARCHAR2(4000)                  DEFAULT NULL;
v_email_dir     VARCHAR2(100)                   DEFAULT NULL;

BEGIN
-- Legger inn i statistikk tabellen
stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,22);

IF ( p_command IS NULL OR p_command = 'adm' OR p_command = 'empty' OR p_command = 'www' ) THEN

        if (p_command = 'adm') then                     -- siden er tilpasset admin topp
                html.b_page(NULL, NULL, NULL, 2);
        elsif (p_command = 'empty') then                -- siden er tilpasset alle vanlige sider (p� en tjeneste)
                html.b_page;
                html.empty_menu;
        elsif (p_command = 'www') then                  -- siden er tilpasset www.in4mant.com
                html.b_page(NULL, NULL, NULL, 3);
        end if;

        html.b_box(get.txt('contact_us'),'100%','contact.us');
        html.b_table;
        html.b_form('contact.us');
        htp.p('
        <tr><td colspan="2">'|| get.txt('contact_info')||'</td></tr>
        <input type="hidden" name="p_command" value="send">
        <tr><td>'|| get.txt('from') ||':</td><td>
        '|| html.text('p_name', '50', '200', p_name ) ||'
        </td></tr>
        <tr><td>'|| get.txt('email_address') ||':</td><td>
        '|| html.text('p_email_address', '50', '200', p_email_address ) ||'
        </td></tr>
        <tr><td valign="top">'|| get.txt('message') ||':</td><td>
        <textarea name="p_message" rows="15" cols="55" wrap="virtual"></textarea>
        </td></tr>
        <tr><td colspan="2" align="right">');
        html.submit_link( get.txt('send') );
        htp.p('</td></tr>');
        html.e_form;
        html.e_table;
        html.e_box;
        html.e_page;
ELSIF ( p_command = 'drink' ) THEN
        html.b_page;
        html.b_box(get.txt('drink_tip'),'100%','contact.us');
        html.b_table;
        html.b_form('contact.us');
        htp.p('
        <tr><td colspan="2">'|| get.txt('drink_info') ||'</td></tr>
        <input type="hidden" name="p_command" value="send">
        <tr><td>'|| get.txt('from') ||':</td><td>
        '|| html.text('p_name', '50', '200', p_name ) ||'
        </td></tr>
        <tr><td>'|| get.txt('email_address') ||':</td><td>
        '|| html.text('p_email_address', '50', '200', p_email_address ) ||'
        </td></tr>
        <tr><td valign="top">'|| get.txt('message') ||':</td><td>
        <textarea name="p_message" rows="15" cols="55" wrap="virtual"></textarea>
        </td></tr>
        <tr><td colspan="2" align="right">');
        html.submit_link( get.txt('send') );
        htp.p('</td></tr>');
        html.e_form;
        html.e_table;
        html.e_box;
        html.e_page;
ELSE
        html.b_page;
        html.empty_menu;
        html.b_box(get.txt('contact_us'),'100%','contact.us');
        html.b_table;
        htp.p('<tr><td colspan="2">'|| get.txt('send_message') ||'</td></tr>
        <tr><td>'|| get.txt('from') ||':</td><td>
        '|| p_name ||'
        </td></tr>
        <tr><td>'|| get.txt('email_address') ||':</td><td>
        '|| p_email_address ||'
        </td></tr>
        <tr><td valign="top">'|| get.txt('message') ||':</td><td>
        '|| html.rem_tag(substr(p_message,1,4000)) ||'
        </td></tr>');
        html.e_table;
        html.e_box;
        html.e_page;

        SELECT  email_script_seq.NEXTVAL
        INTO    v_number
        FROM    DUAL;
        v_message := 'From: info@in4mant.com
Subject: Contact us mail!

'||get.txt('from') ||':'||p_name||' - '||p_email_address;
        v_message := v_message ||'
'||get.txt('message') ||':
'||p_message;
        v_message := v_message ||'

'||get.txt('send_from') ||':'||owa_util.get_cgi_env('REMOTE_HOST');
        v_email_dir := get.value('email_dir');
        v_file := utl_file.fopen( v_email_dir, v_number||'_email-list.txt' , 'w');
        utl_file.put_line(v_file, get.value('email_contact_us'));
        utl_file.fclose(v_file);
        v_file := utl_file.fopen( v_email_dir, v_number||'_message.txt' , 'w');
        utl_file.put_line(v_file, v_message );
        utl_file.fclose(v_file);
END IF;
END;
END us;


---------------------------------------------------------------------
-- Name: email_org
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 30.08.2001
-- Chng:
---------------------------------------------------------------------
PROCEDURE email_org (
                    p_command                   IN varchar2             DEFAULT NULL,
                    p_from                      IN varchar2             DEFAULT NULL,
                    p_subject                   IN varchar2             DEFAULT NULL,
                    p_message                   IN varchar2             DEFAULT NULL,
                    p_organization_pk           IN owa_util.ident_arr   default empty_array
          )
IS
BEGIN
DECLARE
CURSOR  get_org IS
SELECT  organization_pk, name, geography_fk
FROM    organization
WHERE   accepted > 0
ORDER BY name
;

v_organization_pk       organization.organization_pk%TYPE       DEFAULT NULL;
v_name                  organization.name%TYPE                  DEFAULT NULL;
v_geography_fk          organization.geography_fk%TYPE          DEFAULT NULL;
v_i                     NUMBER                                  DEFAULT 1;
v_email                 user_details.email%TYPE                 DEFAULT NULL;
v_file          utl_file.file_type              default NULL;
v_number        NUMBER                          DEFAULT NULL;
v_message       VARCHAR2(4000)                  DEFAULT NULL;
v_email_dir     VARCHAR2(100)                   DEFAULT NULL;

BEGIN

IF (login.timeout('contact.email_org') > 0 AND login.right('contact.email_org') > 0 ) then

--html.b_page(NULL,NULL,NULL,2);
html.b_adm_page('contact organization');
html.b_box( get.txt('fora_type'), '100%', 'admin.edit_fora' );
html.b_table;

IF ( p_organization_pk.COUNT > 0 AND
     p_from IS NOT NULL AND
     p_subject IS NOT NULL AND
     p_message IS NOT NULL
   ) THEN



   SELECT  email_script_seq.NEXTVAL
   INTO    v_number
   FROM    DUAL;
   v_message := 'From: '||p_from||'
Subject: '||p_subject||'

'||p_message;
   v_email_dir := get.value('email_dir');

   v_file := utl_file.fopen( v_email_dir, v_number||'_message.txt' , 'w');
   utl_file.put_line(v_file, v_message );
   utl_file.fclose(v_file);

   v_file := utl_file.fopen( v_email_dir, v_number||'_email-list.txt' , 'w');

   htp.p('<tr><td valign="top" rowspan="'||p_organization_pk.COUNT||'">'|| get.txt('to') ||':</td>');
   FOR v_i IN 1 .. p_organization_pk.COUNT
   LOOP
        SELECT  o.organization_pk, o.name, o.geography_fk, ud.email
        INTO    v_organization_pk, v_name, v_geography_fk, v_email
        FROM    organization o, client_user cu, user_details ud
        WHERE   accepted > 0
        AND     organization_pk = p_organization_pk(v_i)
        AND     o.organization_pk = cu.organization_fk
        AND     cu.user_fk = ud.user_fk
        ;
        IF ( v_i != 1 ) THEN
           htp.p('<tr>');
        END IF;
        htp.p('<td>'||v_name||' / '||v_email||'</td></tr>');
        utl_file.put_line(v_file, v_email);
   END LOOP;

   utl_file.fclose(v_file);
   htp.p('<tr><td valign="top">'|| get.txt('from') ||':</td><td>'||p_from||'</td></tr>');
   htp.p('<tr><td valign="top">'|| get.txt('subject') ||':</td><td>'||p_subject||'</td></tr>');
   htp.p('<tr><td valign="top">'|| get.txt('message') ||':</td><td>'||p_message||'</td></tr>');

ELSE

   html.b_form('contact.email_org');

   open get_org;
   loop
        FETCH get_org INTO v_organization_pk, v_name, v_geography_fk;
        EXIT WHEN get_org%NOTFOUND;
        IF ( v_i = 1 ) THEN htp.p('<tr>');
        END IF;
        htp.p('<td><input type="checkbox" name="p_organization_pk" value="'||v_organization_pk||'">'||v_name||'</td>');
        IF ( v_i = 3 ) THEN
           htp.p('</tr>');
           v_i := 0;
        END IF;
        v_i := v_i + 1;
   end loop;
   close get_org;
   IF ( v_i = 3 ) THEN
      htp.p('<td>&nbsp;</td></tr>');
   ELSIF ( v_i = 2 ) THEN
      htp.p('<td colspan="2">&nbsp;</td></tr>');
   END IF;
   htp.p('<tr><td colspan="3" align="center">');
   html.b_table;
   htp.p('
   <input type="hidden" name="p_command" value="send">
   <tr><td>'|| get.txt('from') ||':</td><td>
   <select name="p_from">
   <option value="info@in4mant.com">info@in4mant.com</option>
   <option value="andreas@in4mant.com">andreas@in4mant.com</option>
   <option value="espen@in4mant.com">espen@in4mant.com</option>
   <option value="frode@in4mant.com">frode@in4mant.com</option>
   </select>
   </td></tr>
   <tr><td>'|| get.txt('subject') ||':</td><td>
   '|| html.text('p_subject', '50', '200', p_subject ) ||'
   </td></tr>
   <tr><td valign="top">'|| get.txt('message') ||':</td><td>
   <textarea name="p_message" rows="15" cols="55" wrap="virtual"></textarea>
   </td></tr>
   <tr><td colspan="2" align="right">');
   html.submit_link( get.txt('send') );
   htp.p('</td></tr>');
   html.e_table;
   htp.p('</td></tr>');
   html.e_form;

END IF;
html.e_table;
html.e_box;
html.e_adm_page;

END IF;

END;
END email_org;

---------------------------------------------------------------------
---------------------------------------------------------------------
END;
/
show errors;








