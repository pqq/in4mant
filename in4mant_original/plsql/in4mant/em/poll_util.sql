set define off
PROMPT *** package: poll_util ***

CREATE OR REPLACE PACKAGE poll_util IS

PROCEDURE startup;

PROCEDURE edit_poll_type(       p_command               IN varchar2                             DEFAULT NULL,
                                p_name                  IN poll_type.name%TYPE                  DEFAULT NULL,
                                p_description           IN poll_type.description%TYPE           DEFAULT NULL,
                                p_service_pk            IN poll_type.service_fk%TYPE            DEFAULT NULL,
                                p_language_pk           IN poll_type.language_fk%TYPE           DEFAULT NULL,
                                p_poll_type_pk          IN poll_type.poll_type_pk%TYPE          DEFAULT NULL
                        );
PROCEDURE select_poll_type(     p_poll_type_pk          IN poll_type.poll_type_pk%TYPE          DEFAULT NULL);
PROCEDURE select_fora_type (    p_fora_type_pk          IN fora_type.fora_type_pk%TYPE          DEFAULT NULL);
PROCEDURE edit_poll(            p_command               IN varchar2                             DEFAULT NULL,
                                p_poll_pk               IN poll.poll_pk%TYPE                    DEFAULT NULL,
                                p_poll_type_pk          IN poll.poll_type_fk%TYPE               DEFAULT NULL,
                                p_question              IN poll.question%TYPE                   DEFAULT NULL,
                                p_alt_1                 IN poll.alt_1%TYPE                      DEFAULT NULL,
                                p_alt_2                 IN poll.alt_2%TYPE                      DEFAULT NULL,
                                p_alt_3                 IN poll.alt_3%TYPE                      DEFAULT NULL,
                                p_alt_4                 IN poll.alt_4%TYPE                      DEFAULT NULL,
                                p_alt_5                 IN poll.alt_5%TYPE                      DEFAULT NULL,
                                p_alt_6                 IN poll.alt_6%TYPE                      DEFAULT NULL,
                                p_alt_7                 IN poll.alt_7%TYPE                      DEFAULT NULL,
                                p_alt_8                 IN poll.alt_8%TYPE                      DEFAULT NULL,
                                p_alt_9                 IN poll.alt_9%TYPE                      DEFAULT NULL,
                                p_alt_10                IN poll.alt_10%TYPE                     DEFAULT NULL,
                                p_fora_type_pk          IN poll.fora_type_fk%TYPE               DEFAULT NULL
                         );
PROCEDURE accepted (            p_poll_pk               IN poll.poll_pk%TYPE                    DEFAULT NULL );
PROCEDURE show_poll (           p_service_pk            IN service.service_pk%TYPE              DEFAULT NULL,
                                p_language_pk           IN la.language_pk%TYPE                  DEFAULT NULL,
                                p_poll_pk               IN poll.poll_pk%TYPE                    DEFAULT NULL,
                                p_alt_no                IN poll_answer.alt_no%TYPE              DEFAULT NULL,
                                p_show                  IN VARCHAR2                             DEFAULT NULL
                                );
PROCEDURE reg_date (            p_poll_pk               IN poll.poll_pk%TYPE                    DEFAULT NULL,
                                p_start_date            IN VARCHAR2                             DEFAULT NULL,
                                p_end_date              IN VARCHAR2                             DEFAULT NULL);
PROCEDURE show_result (         p_poll_pk               IN poll.poll_pk%TYPE                    DEFAULT NULL );
PROCEDURE show_all_results (    p_poll_pk               IN poll.poll_pk%TYPE                    DEFAULT NULL );


END;
/
CREATE OR REPLACE PACKAGE BODY poll_util IS
---------------------------------------------------------------------
---------------------------------------------------------------------
-- Name: startup
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 15.07.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE startup
IS
BEGIN
        show_all_results;
END startup;

---------------------------------------------------------------------
-- Name: edit_poll_type
-- Type: procedure
-- What: Genererer dokumentarkiv
-- Made: Espen Messel
-- Date: 15.07.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE edit_poll_type (      p_command               IN varchar2                             DEFAULT NULL,
                                p_name                  IN poll_type.name%TYPE                  DEFAULT NULL,
                                p_description           IN poll_type.description%TYPE           DEFAULT NULL,
                                p_service_pk            IN poll_type.service_fk%TYPE            DEFAULT NULL,
                                p_language_pk           IN poll_type.language_fk%TYPE           DEFAULT NULL,
                                p_poll_type_pk          IN poll_type.poll_type_pk%TYPE          DEFAULT NULL
                         )
IS
BEGIN
DECLARE

CURSOR  get_poll_type IS
SELECT  poll_type_pk, name, description, language_fk,
        service_fk
FROM    poll_type
ORDER BY name
;

v_poll_type_pk          poll_type.poll_type_pk%TYPE             DEFAULT NULL;
v_name                  poll_type.name%TYPE                     DEFAULT NULL;
v_description           poll_type.description%TYPE              DEFAULT NULL;
v_language_pk           poll_type.language_fk%TYPE              DEFAULT NULL;
v_service_pk            poll_type.service_fk%TYPE               DEFAULT NULL;

BEGIN

IF (    login.timeout('poll_util.edit_poll_type') > 0  AND
        login.right('poll_util.edit_poll_type') > 0 ) THEN

        IF ( p_command = 'update') THEN
                SELECT  poll_type_pk, name, description, language_fk,
                        service_fk
                INTO    v_poll_type_pk, v_name, v_description, v_language_pk,
                        v_service_pk
                FROM    poll_type
                WHERE   poll_type_pk=p_poll_type_pk
                ;
                html.b_adm_page;
                html.b_box( get.txt('poll_type') );
                html.b_table;
                html.b_form('poll_util.edit_poll_type');
                htp.p('<input type="hidden" name="p_command" value="edit">
                <input type="hidden" name="p_poll_type_pk" value="'||v_poll_type_pk||'">
                <tr><td>'|| get.txt('service') ||':</td><td>');
                html.select_service(v_service_pk, -1000);
                htp.p('</td></tr>
                <tr><td>'|| get.txt('language') ||':</td><td>');
                html.select_lang(v_language_pk);
                htp.p('</td></tr>
                <tr><td>'|| get.txt('name') ||':</td><td>
                '|| html.text('p_name', '50', '50', v_name ) ||'
                </td></tr>
                <tr><td>'|| get.txt('description') ||':(maks 150 tegn)</td><td>
                '|| html.text('p_description', '50', '150', v_description ) ||'
                </td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('update') ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page;
        ELSIF ( p_command = 'edit') THEN
                UPDATE  poll_type
                SET     name=trans(p_name),
                        description=trans(p_description),
                        language_fk=p_language_pk,
                        service_fk=p_service_pk
                WHERE   poll_type_pk=p_poll_type_pk;
                COMMIT;
                html.jump_to ( 'poll_util.edit_poll_type');
        ELSIF ( p_command = 'delete') THEN
                IF ( p_name = 'yes') THEN
                        DELETE  FROM poll_type
                        WHERE   poll_type_pk=p_poll_type_pk;
                        COMMIT;
                        html.jump_to ( 'poll_util.edit_poll_type');
                ELSIF ( p_name IS NULL ) THEN
                        html.jump_to ( 'poll_util.edit_poll_type');
                ELSE
                        html.b_adm_page;
                        html.b_box( get.txt('poll_type') );
                        html.b_table;
                        html.b_form('poll_util.edit_poll_type');
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="yes">
                        <input type="hidden" name="p_poll_type_pk" value="'||p_poll_type_pk||'">
                        <td>'|| get.txt('delete_info')||' "'||trans(p_name)||'" ?'); html.submit_link( get.txt('YES') ); htp.p('</td>');
                        html.e_form;
                        htp.p('<td>'); html.button_link( get.txt('NO'), 'poll_util.edit_poll_type');htp.p('</td></tr>');
                        html.e_table;
                        html.e_box;
                        html.e_page;
                END IF;
        ELSIF ( p_command = 'new') THEN
                html.b_adm_page;
                html.b_box( get.txt('poll_type') );
                html.b_table;
                html.b_form('poll_util.edit_poll_type','form_insert');
                htp.p('<input type="hidden" name="p_command" value="insert">
                <tr><td>'|| get.txt('service') ||':</td><td>');
                html.select_service(NULL, -1000);
                htp.p('</td></tr>
                <tr><td>'|| get.txt('language') ||':</td><td>');
                html.select_lang;
                htp.p('</td></tr>
                <tr><td>'|| get.txt('name') ||':</td><td>
                '|| html.text('p_name', '50', '50', '' ) ||'
                </td></tr>
                <tr><td>'|| get.txt('description') ||':(maks 200 tegn)</td><td>
                '|| html.text('p_description', '50', '150', '' ) ||'
                </td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('insert'), 'form_insert' ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page;
        ELSIF ( p_command = 'insert') THEN
                INSERT INTO poll_type
                ( poll_type_pk, name, description, language_fk, service_fk )
                VALUES
                ( poll_type_seq.NEXTVAL, SUBSTR(trans(p_name),1,50), SUBSTR(trans(p_description),1,150),
                p_language_pk, p_service_pk );
                COMMIT;
                html.jump_to ( 'poll_util.edit_poll_type' );
        ELSE
                html.b_adm_page;
                html.b_box( get.txt('poll_type') );
                html.b_table;
                OPEN get_poll_type;
                LOOP
                        FETCH get_poll_type INTO        v_poll_type_pk, v_name, v_description,
                                                v_language_pk, v_service_pk;
                        exit when get_poll_type%NOTFOUND;
                        html.b_form('poll_util.edit_poll_type','form'|| v_poll_type_pk);
                        htp.p('<input type="hidden" name="p_command" value="update">
                        <input type="hidden" name="p_poll_type_pk" value="'||v_poll_type_pk||'">
                        <tr><td>'|| get.txt('poll_type') ||':</td><td>'||v_name||'</td>
                        <td>'); html.submit_link( get.txt('edit'), 'form'||v_poll_type_pk ); htp.p('</td>');
                        html.e_form;
                        html.b_form('poll_util.edit_poll_type','delete'|| v_poll_type_pk);
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="'||v_name||'">
                        <input type="hidden" name="p_poll_type_pk" value="'||v_poll_type_pk||'">
                        <td>'); html.submit_link( get.txt('delete'), 'delete'||v_poll_type_pk ); htp.p('</td></tr>');
                        html.e_form;
                END LOOP;
                CLOSE get_poll_type;
                html.b_form('poll_util.edit_poll_type','form_new');
                htp.p('<input type="hidden" name="p_command" value="new">
                <td>'); html.submit_link( get.txt('new'), 'form_new'); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page;
        END IF;
END IF;
EXCEPTION
WHEN NO_DATA_FOUND
THEN
htp.p('No data found! edit_poll.sql -> edit_poll_type');
END;
END edit_poll_type;

---------------------------------------------------------------------
-- Name: select_poll_type
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE select_poll_type (p_poll_type_pk              IN poll_type.poll_type_pk%TYPE          DEFAULT NULL)
IS
BEGIN
DECLARE
        v_service_pk            poll_type.service_fk%TYPE       DEFAULT NULL;
        v_name                  poll_type.name%TYPE             DEFAULT NULL;
        v_poll_type_pk          poll_type.poll_type_pk%TYPE     DEFAULT NULL;
        v_language_pk           poll_type.language_fk%TYPE      DEFAULT NULL;
        v_value                 VARCHAR2(20)                    DEFAULT NULL;

        CURSOR  all_poll_types IS
        SELECT  name, poll_type_pk, language_fk
        FROM    poll_type
        ORDER BY name
        ;

BEGIN
        htp.p('<select name="p_poll_type_pk">');
        OPEN all_poll_types;
        LOOP
                fetch all_poll_types into v_name, v_poll_type_pk, v_language_pk;
                exit when all_poll_types%NOTFOUND;
                IF ( v_poll_type_pk = p_poll_type_pk ) THEN
                        v_value := ' selected';
                ELSE
                        v_value := '';
                END IF;
                htp.p('<option value="'|| v_poll_type_pk ||'"'||v_value||'>'|| v_name ||'</option>');
        END LOOP;
        close all_poll_types;
        htp.p('</select>');
END;
END select_poll_type;

---------------------------------------------------------------------
-- Name: edit_poll
-- Type: procedure
-- What: Genererer dokumentarkiv
-- Made: Espen Messel
-- Date: 20.07.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE edit_poll (   p_command               IN varchar2                             DEFAULT NULL,
                        p_poll_pk               IN poll.poll_pk%TYPE                    DEFAULT NULL,
                        p_poll_type_pk          IN poll.poll_type_fk%TYPE               DEFAULT NULL,
                        p_question              IN poll.question%TYPE                   DEFAULT NULL,
                        p_alt_1                 IN poll.alt_1%TYPE                      DEFAULT NULL,
                        p_alt_2                 IN poll.alt_2%TYPE                      DEFAULT NULL,
                        p_alt_3                 IN poll.alt_3%TYPE                      DEFAULT NULL,
                        p_alt_4                 IN poll.alt_4%TYPE                      DEFAULT NULL,
                        p_alt_5                 IN poll.alt_5%TYPE                      DEFAULT NULL,
                        p_alt_6                 IN poll.alt_6%TYPE                      DEFAULT NULL,
                        p_alt_7                 IN poll.alt_7%TYPE                      DEFAULT NULL,
                        p_alt_8                 IN poll.alt_8%TYPE                      DEFAULT NULL,
                        p_alt_9                 IN poll.alt_9%TYPE                      DEFAULT NULL,
                        p_alt_10                IN poll.alt_10%TYPE                     DEFAULT NULL,
                        p_fora_type_pk          IN poll.fora_type_fk%TYPE               DEFAULT NULL
                         )
IS
BEGIN
DECLARE

CURSOR  get_poll IS
SELECT  poll_pk, question
FROM    poll
WHERE   start_date IS NULL
OR      start_date > SYSDATE
ORDER BY question
;

v_poll_pk               poll.poll_pk%TYPE                       DEFAULT NULL;
v_poll_type_pk          poll.poll_type_fk%TYPE                  DEFAULT NULL;
v_question              poll.question%TYPE                      DEFAULT NULL;
v_alt_1                 poll.alt_1%TYPE                         DEFAULT NULL;
v_alt_2                 poll.alt_2%TYPE                         DEFAULT NULL;
v_alt_3                 poll.alt_3%TYPE                         DEFAULT NULL;
v_alt_4                 poll.alt_4%TYPE                         DEFAULT NULL;
v_alt_5                 poll.alt_5%TYPE                         DEFAULT NULL;
v_alt_6                 poll.alt_6%TYPE                         DEFAULT NULL;
v_alt_7                 poll.alt_7%TYPE                         DEFAULT NULL;
v_alt_8                 poll.alt_8%TYPE                         DEFAULT NULL;
v_alt_9                 poll.alt_9%TYPE                         DEFAULT NULL;
v_alt_10                poll.alt_10%TYPE                        DEFAULT NULL;
v_name                  poll_type.name%TYPE                     DEFAULT NULL;
v_fora_type_pk          poll.fora_type_fk%TYPE                  DEFAULT NULL;

BEGIN

IF ( login.timeout('poll_util.edit_poll?p_command='||p_command) > 0 ) THEN
        IF ( p_command = 'update' AND
             login.right('poll_util.edit_poll') > 0 ) THEN
                SELECT  poll_pk, poll_type_fk, question, alt_1, alt_2, alt_3,
                        alt_4, alt_5, alt_6, alt_7, alt_8, alt_9, alt_10, fora_type_fk
                INTO    v_poll_pk, v_poll_type_pk, v_question, v_alt_1, v_alt_2, v_alt_3,
                        v_alt_4, v_alt_5, v_alt_6, v_alt_7, v_alt_8, v_alt_9, v_alt_10, v_fora_type_pk
                FROM    poll
                WHERE   poll_pk=p_poll_pk
                ;
                html.b_adm_page;
                html.b_box( get.txt('edit_poll') );
                html.b_table;
                html.b_form('poll_util.edit_poll');
                htp.p('<input type="hidden" name="p_command" value="edit">
                <input type="hidden" name="p_poll_pk" value="'||v_poll_pk||'">
                <tr><td>'|| get.txt('poll_type') ||':</td><td>');
                select_poll_type;
                htp.p('</td></tr>
                <tr><td>'|| get.txt('fora_type') ||':</td><td>');
                select_fora_type(v_fora_type_pk);
                htp.p('</td></tr>
                <tr><td>'|| get.txt('question') ||':</td><td>
                '|| html.text('p_question', '50', '150', v_question ) ||'
                </td></tr>
                <tr><td>'|| get.txt('alt') ||' 1:</td><td>
                '|| html.text('p_alt_1', '50', '150', v_alt_1 ) ||'
                </td></tr>
                <tr><td>'|| get.txt('alt') ||' 2:</td><td>
                '|| html.text('p_alt_2', '50', '150', v_alt_2 ) ||'
                </td></tr>
                <tr><td>'|| get.txt('alt') ||' 3:</td><td>
                '|| html.text('p_alt_3', '50', '150', v_alt_3 ) ||'
                </td></tr>
                <tr><td>'|| get.txt('alt') ||' 4:</td><td>
                '|| html.text('p_alt_4', '50', '150', v_alt_4 ) ||'
                </td></tr>
                <tr><td>'|| get.txt('alt') ||' 5:</td><td>
                '|| html.text('p_alt_5', '50', '150', v_alt_5 ) ||'
                </td></tr>
                <tr><td>'|| get.txt('alt') ||' 6:</td><td>
                '|| html.text('p_alt_6', '50', '150', v_alt_6 ) ||'
                </td></tr>
                <tr><td>'|| get.txt('alt') ||' 7:</td><td>
                '|| html.text('p_alt_7', '50', '150', v_alt_7 ) ||'
                </td></tr>
                <tr><td>'|| get.txt('alt') ||' 8:</td><td>
                '|| html.text('p_alt_8', '50', '150', v_alt_8 ) ||'
                </td></tr>
                <tr><td>'|| get.txt('alt') ||' 9:</td><td>
                '|| html.text('p_alt_9', '50', '150', v_alt_9 ) ||'
                </td></tr>
                <tr><td>'|| get.txt('alt') ||' 10:</td><td>
                '|| html.text('p_alt_10', '50', '150', v_alt_10 ) ||'
                </td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('update') ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page;
        ELSIF ( p_command = 'edit' AND
                login.right('poll_util.edit_poll') > 0 ) THEN
                UPDATE  poll
                SET     poll_type_fk=p_poll_type_pk,
                        question=trans(p_question),
                        alt_1=trans(p_alt_1),
                        alt_2=trans(p_alt_2),
                        alt_3=trans(p_alt_3),
                        alt_4=trans(p_alt_4),
                        alt_5=trans(p_alt_5),
                        alt_6=trans(p_alt_6),
                        alt_7=trans(p_alt_7),
                        alt_8=trans(p_alt_8),
                        alt_9=trans(p_alt_9),
                        alt_10=trans(p_alt_10),
                        fora_type_fk=p_fora_type_pk
                WHERE   poll_pk=p_poll_pk;
                COMMIT;
                html.jump_to ( 'poll_util.edit_poll');
        ELSIF ( p_command = 'delete' AND
                login.right('poll_util.edit_poll') > 0 ) THEN
                IF ( p_question = 'yes') THEN
                        DELETE  FROM poll
                        WHERE   poll_pk=p_poll_pk;
                        COMMIT;
                        html.jump_to ( 'poll_util.edit_poll');
                ELSIF ( p_question IS NULL ) THEN
                        html.jump_to ( 'poll_util.edit_poll');
                ELSE
                        html.b_adm_page;
                        html.b_box( get.txt('edit_poll') );
                        html.b_table;
                        html.b_form('poll_util.edit_poll');
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_question" value="yes">
                        <input type="hidden" name="p_poll_pk" value="'||p_poll_pk||'">
                        <td>'|| get.txt('delete_info')||' "'||trans(p_question)||'" ?'); html.submit_link( get.txt('YES') ); htp.p('</td>');
                        html.e_form;
                        htp.p('<td>'); html.button_link( get.txt('NO'), 'poll_util.edit_poll');htp.p('</td></tr>');
                        html.e_table;
                        html.e_box;
                        html.e_page;
                END IF;
        ELSIF ( p_command = 'new') THEN
                IF ( owa_util.get_cgi_env('QUERY_STRING') = 'p_command=new' ) THEN
                        html.b_page;
                        html.b_box( get.txt('edit_poll'), '100%', 'poll_util.edit_poll-NEW' );
                        html.b_table;
                        htp.p('<tr><td colspan="2">'|| get.txt('help_new_poll') ||'</td></tr>');
                ELSE
                        html.b_page_2;
                        html.b_box( get.txt('edit_poll'), '100%', 'poll_util.edit_poll-NEW' );
                        html.b_table;
                END IF;
                html.b_form('poll_util.edit_poll');
                htp.p('<input type="hidden" name="p_command" value="insert">
                <tr><td>'|| get.txt('poll_type') ||':</td><td>');
                select_poll_type;
                htp.p('</td></tr>
                <tr><td>'|| get.txt('fora_type') ||':</td><td>');
                select_fora_type;
                htp.p('</td></tr>
                <tr><td>'|| get.txt('question') ||':</td><td>
                '|| html.text('p_question', '50', '150', '' ) ||'
                </td></tr>
                <tr><td>'|| get.txt('alt') ||' 1:</td><td>
                '|| html.text('p_alt_1', '50', '150', '' ) ||'
                </td></tr>
                <tr><td>'|| get.txt('alt') ||' 2:</td><td>
                '|| html.text('p_alt_2', '50', '150', '' ) ||'
                </td></tr>
                <tr><td>'|| get.txt('alt') ||' 3:</td><td>
                '|| html.text('p_alt_3', '50', '150', '' ) ||'
                </td></tr>
                <tr><td>'|| get.txt('alt') ||' 4:</td><td>
                '|| html.text('p_alt_4', '50', '150', '' ) ||'
                </td></tr>
                <tr><td>'|| get.txt('alt') ||' 5:</td><td>
                '|| html.text('p_alt_5', '50', '150', '' ) ||'
                </td></tr>
                <tr><td>'|| get.txt('alt') ||' 6:</td><td>
                '|| html.text('p_alt_6', '50', '150', '' ) ||'
                </td></tr>
                <tr><td>'|| get.txt('alt') ||' 7:</td><td>
                '|| html.text('p_alt_7', '50', '150', '' ) ||'
                </td></tr>
                <tr><td>'|| get.txt('alt') ||' 8:</td><td>
                '|| html.text('p_alt_8', '50', '150', '' ) ||'
                </td></tr>
                <tr><td>'|| get.txt('alt') ||' 9:</td><td>
                '|| html.text('p_alt_9', '50', '150', '' ) ||'
                </td></tr>
                <tr><td>'|| get.txt('alt') ||' 10:</td><td>
                '|| html.text('p_alt_10', '50', '150', '' ) ||'
                </td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('insert')); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page;
        ELSIF ( p_command = 'insert') THEN
                INSERT INTO poll
                ( poll_pk, poll_type_fk, question, alt_1, alt_2, alt_3,
                alt_4, alt_5, alt_6, alt_7, alt_8, alt_9, alt_10,
                made_date, fora_type_fk )
                VALUES
                ( poll_seq.NEXTVAL, p_poll_type_pk, SUBSTR(trans(p_question),1,150),
                SUBSTR(trans(p_alt_1),1,150), SUBSTR(trans(p_alt_2),1,150), SUBSTR(trans(p_alt_3),1,150),
                SUBSTR(trans(p_alt_4),1,150), SUBSTR(trans(p_alt_5),1,150), SUBSTR(trans(p_alt_6),1,150),
                SUBSTR(trans(p_alt_7),1,150), SUBSTR(trans(p_alt_8),1,150), SUBSTR(trans(p_alt_9),1,150),
                SUBSTR(trans(p_alt_10),1,150), SYSDATE, p_fora_type_pk );
                COMMIT;
                IF ( get.u_type < -2 ) THEN
                   html.jump_to ( 'poll_util.edit_poll' );
                ELSE
                   html.b_page(NULL,5,get.value('logout_url'));
                   html.b_box( get.txt('new_poll') );
                   htp.p(get.txt('thank_you_for_poll'));
                   html.e_box;
                   html.e_page;
                END IF;
        ELSE
                IF ( login.right('poll_util.edit_poll') > 0 ) THEN
                html.b_page_2;
                html.b_box( get.txt('edit_poll') );
                html.b_table;
                OPEN get_poll;
                LOOP
                        FETCH get_poll INTO     v_poll_pk, v_question;
                        exit when get_poll%NOTFOUND;
                        html.b_form('poll_util.edit_poll','form'|| v_poll_pk);
                        htp.p('<input type="hidden" name="p_command" value="update">
                        <input type="hidden" name="p_poll_pk" value="'||v_poll_pk||'">
                        <tr><td>'|| get.txt('poll') ||':</td><td>'||v_question||'</td>
                        <td>'); html.submit_link( get.txt('edit'), 'form'||v_poll_pk ); htp.p('</td>');
                        html.e_form;
                        html.b_form('poll_util.edit_poll','delete'|| v_poll_pk);
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_question" value="'||v_question||'">
                        <input type="hidden" name="p_poll_pk" value="'||v_poll_pk||'">
                        <td>'); html.submit_link( get.txt('delete'), 'delete'||v_poll_pk ); htp.p('</td></tr>');
                        html.e_form;
                END LOOP;
                CLOSE get_poll;
                html.b_form('poll_util.edit_poll','form_new');
                htp.p('<input type="hidden" name="p_command" value="new">
                <td>'); html.submit_link( get.txt('new'), 'form_new'); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page;
                END IF;
        END IF;
END IF;
EXCEPTION
WHEN NO_DATA_FOUND
THEN
htp.p('No data found! edit_poll.sql -> edit_poll');
END;
END edit_poll;

---------------------------------------------------------------------
-- Name: accepted
-- Type: procedure
-- What: Genererer en side over nye poll som skal aksepteres
-- Made: Espen Messel
-- Date: 25.07.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE accepted (    p_poll_pk IN poll.poll_pk%TYPE  DEFAULT NULL )
IS
BEGIN
DECLARE

v_poll_pk               poll.poll_pk%TYPE               DEFAULT NULL;
v_question              poll.question%TYPE              DEFAULT NULL;
v_start_date            poll.start_date%TYPE            DEFAULT NULL;
v_end_date              poll.end_date%TYPE              DEFAULT NULL;
v_name_sg_fk            service.name_sg_fk%TYPE         DEFAULT NULL;
v_accepted              poll.accepted%TYPE              DEFAULT NULL;

CURSOR get_poll IS
SELECT  p.poll_pk, p.question, s.name_sg_fk, p.start_date, p.end_date, p.accepted
FROM    poll p, poll_type pt, service s
WHERE   p.poll_type_fk = pt.poll_type_pk
AND     s.service_pk = pt.service_fk
ORDER BY pt.service_fk, p.question
;
/*
AND     (end_date > SYSDATE
OR      end_date IS NULL)
*/
BEGIN
IF (    login.timeout('poll_util.accepted') > 0  AND
        login.right('poll_util.accepted') > 0 ) THEN

        IF ( p_poll_pk IS NOT NULL ) THEN
                UPDATE poll
                SET    accepted=1
                WHERE  poll_pk=p_poll_pk
                ;
                COMMIT;
        END IF;
        html.b_adm_page;
        html.b_box( get.txt('accept_poll') );
        html.b_table;
        OPEN get_poll;
        LOOP
                FETCH get_poll INTO v_poll_pk, v_question, v_name_sg_fk, v_start_date, v_end_date, v_accepted;
                exit when get_poll%NOTFOUND;
                htp.p('<tr><td>'|| get.text(v_name_sg_fk) ||'</td>
                <td>'||v_question||'</td>');
                IF ( v_accepted IS NULL ) THEN
                        htp.p('<td><a href="poll_util.accepted?p_poll_pk='||v_poll_pk||'">Accept?</a></td>');
                ELSE
                        htp.p('<td>Already Accepted</td>');
                END IF;
                htp.p('<td nowrap>Start date: '||to_char(v_start_date,get.txt('date_long'))||'</td>');
                IF ( v_start_date > SYSDATE OR v_start_date IS NULL ) THEN
                        htp.p('
                <td nowrap>End date: '||to_char(v_end_date,get.txt('date_long'))||'</td>
                <td nowrap><a href="poll_util.reg_date?p_poll_pk='||v_poll_pk||'">Start-/enddate</a>');
                ELSE
                        htp.p('
                <td nowrap>End date: <a href="poll_util.reg_date?p_poll_pk='||v_poll_pk||'&p_end_date='||REPLACE(to_char(v_end_date,get.txt('date_long')),' ','+')||'">'||to_char(v_end_date,get.txt('date_long'))||'</a></td>
                <td nowrap>Poll already started');
                END IF;
                htp.p('</td>
                <td><a href="poll_util.show_poll?p_poll_pk='||v_poll_pk||'">Look at poll</a></td>
                </tr>');
        END LOOP;
        CLOSE get_poll;
        html.e_table;
        html.e_box;
        html.e_page;
END IF;
EXCEPTION
WHEN NO_DATA_FOUND
THEN
htp.p('No data found! util_poll -> accepted');
END;
END accepted;

---------------------------------------------------------------------
-- Name: show_poll
-- Type: procedure
-- What: Genererer dokumentarkiv
-- Made: Espen Messel
-- Date: 20.07.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE show_poll (   p_service_pk            IN service.service_pk%TYPE              DEFAULT NULL,
                        p_language_pk           IN la.language_pk%TYPE                  DEFAULT NULL,
                        p_poll_pk               IN poll.poll_pk%TYPE                    DEFAULT NULL,
                        p_alt_no                IN poll_answer.alt_no%TYPE              DEFAULT NULL,
                        p_show                  IN VARCHAR2                             DEFAULT NULL
                         )
IS
BEGIN
DECLARE

v_poll_pk               poll.poll_pk%TYPE                       DEFAULT NULL;
v_poll_type_pk          poll.poll_type_fk%TYPE                  DEFAULT NULL;
v_question              poll.question%TYPE                      DEFAULT NULL;
v_alt_1                 poll.alt_1%TYPE                         DEFAULT NULL;
v_alt_2                 poll.alt_2%TYPE                         DEFAULT NULL;
v_alt_3                 poll.alt_3%TYPE                         DEFAULT NULL;
v_alt_4                 poll.alt_4%TYPE                         DEFAULT NULL;
v_alt_5                 poll.alt_5%TYPE                         DEFAULT NULL;
v_alt_6                 poll.alt_6%TYPE                         DEFAULT NULL;
v_alt_7                 poll.alt_7%TYPE                         DEFAULT NULL;
v_alt_8                 poll.alt_8%TYPE                         DEFAULT NULL;
v_alt_9                 poll.alt_9%TYPE                         DEFAULT NULL;
v_alt_10                poll.alt_10%TYPE                        DEFAULT NULL;
v_name                  poll_type.name%TYPE                     DEFAULT NULL;
v_start_date            poll.start_date%TYPE                    DEFAULT NULL;
v_end_date              poll.end_date%TYPE                      DEFAULT NULL;
v_user_pk               usr.user_pk%TYPE                        DEFAULT NULL;
v_user_type_pk          user_type.user_type_pk%TYPE             DEFAULT NULL;
v_service_pk            service.service_pk%TYPE                 DEFAULT NULL;

CURSOR  get_poll IS
SELECT  p.poll_pk
FROM    poll p, poll_type pt
WHERE   p.start_date < SYSDATE
AND     p.end_date > SYSDATE
AND     p.poll_type_fk = pt.poll_type_pk
AND     pt.service_fk = p_service_pk
AND     pt.language_fk = p_language_pk
AND     p.accepted IS NOT NULL
ORDER BY p.start_date
;

BEGIN

v_service_pk := get.serv;
IF ( owa_util.get_cgi_env('PATH_INFO') <> '/main_page.startup' ) THEN
-- Legger inn i statistikk tabellen
     stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,23,v_service_pk);
END IF;
IF ( p_alt_no IS NOT NULL ) THEN
/*
        IF ( login.timeout('poll_util.show_poll?p_poll_pk='||p_poll_pk||'&p_alt_no='||p_alt_no) > 0 ) THEN
                v_user_pk := get.uid;
                INSERT INTO poll_answer
                ( poll_fk, user_fk, alt_no, answer_date )
                VALUES
                ( p_poll_pk, v_user_pk, p_alt_no, SYSDATE );
                COMMIT;
                html.jump_to('poll_util.show_result?p_poll_pk='||p_poll_pk);
        END IF;
*/
  INSERT INTO poll_answer
  ( poll_fk,  alt_no, answer_date )
  VALUES
  ( p_poll_pk, p_alt_no, SYSDATE );
  COMMIT;
  html.jump_to('poll_util.show_result?p_poll_pk='||p_poll_pk);
ELSIF ( p_show IS NOT NULL ) THEN
        html.jump_to( owa_util.get_cgi_env('HTTP_REFERER') );
ELSE
                IF ( p_poll_pk IS NULL ) THEN
                        OPEN get_poll;
                        FETCH get_poll INTO v_poll_pk;
                        CLOSE get_poll;
                ELSE
                        v_poll_pk := p_poll_pk;
                END IF;
                IF ( v_poll_pk IS NOT NULL ) THEN
                        v_user_type_pk := get.u_type;
                        IF ( v_user_type_pk <= -2 ) THEN
                                SELECT  poll_pk, poll_type_fk, question, alt_1, alt_2, alt_3,
                                        alt_4, alt_5, alt_6, alt_7, alt_8, alt_9, alt_10,
                                        start_date, end_date
                                INTO    v_poll_pk, v_poll_type_pk, v_question, v_alt_1, v_alt_2, v_alt_3,
                                        v_alt_4, v_alt_5, v_alt_6, v_alt_7, v_alt_8, v_alt_9, v_alt_10,
                                        v_start_date, v_end_date
                                FROM    poll
                                WHERE   poll_pk=v_poll_pk
                                ;
                        ELSE
                                SELECT  poll_pk, poll_type_fk, question, alt_1, alt_2, alt_3,
                                        alt_4, alt_5, alt_6, alt_7, alt_8, alt_9, alt_10,
                                        start_date, end_date
                                INTO    v_poll_pk, v_poll_type_pk, v_question, v_alt_1, v_alt_2, v_alt_3,
                                        v_alt_4, v_alt_5, v_alt_6, v_alt_7, v_alt_8, v_alt_9, v_alt_10,
                                        v_start_date, v_end_date
                                FROM    poll
                                WHERE   poll_pk=v_poll_pk
                                AND     start_date < SYSDATE
                                ;
                        END IF;
                END IF;
                IF ( v_question IS NULL ) THEN
                        html.b_box_2( get.txt('poll_caption') ,'200', 'poll_util.show_poll');
                ELSE
                        html.b_box_2( v_question ,'200', 'poll_util.show_poll');
                END IF;
                html.b_table('200');
                html.b_form('poll_util.show_poll');
                htp.p('<input type="hidden" name="p_poll_pk" value="'||v_poll_pk||'">
                <input type="hidden" name="p_show" value="OK">');
                IF (v_alt_1 IS NOT NULL) THEN htp.p('<tr><td>'||v_alt_1||'</td><td><input type="radio" name="p_alt_no" value="1"></td></tr>');END IF;
                IF (v_alt_2 IS NOT NULL) THEN htp.p('<tr><td>'||v_alt_2||'</td><td><input type="radio" name="p_alt_no" value="2"></td></tr>');END IF;
                IF (v_alt_3 IS NOT NULL) THEN htp.p('<tr><td>'||v_alt_3||'</td><td><input type="radio" name="p_alt_no" value="3"></td></tr>');END IF;
                IF (v_alt_4 IS NOT NULL) THEN htp.p('<tr><td>'||v_alt_4||'</td><td><input type="radio" name="p_alt_no" value="4"></td></tr>');END IF;
                IF (v_alt_5 IS NOT NULL) THEN htp.p('<tr><td>'||v_alt_5||'</td><td><input type="radio" name="p_alt_no" value="5"></td></tr>');END IF;
                IF (v_alt_6 IS NOT NULL) THEN htp.p('<tr><td>'||v_alt_6||'</td><td><input type="radio" name="p_alt_no" value="6"></td></tr>');END IF;
                IF (v_alt_7 IS NOT NULL) THEN htp.p('<tr><td>'||v_alt_7||'</td><td><input type="radio" name="p_alt_no" value="7"></td></tr>');END IF;
                IF (v_alt_8 IS NOT NULL) THEN htp.p('<tr><td>'||v_alt_8||'</td><td><input type="radio" name="p_alt_no" value="8"></td></tr>');END IF;
                IF (v_alt_9 IS NOT NULL) THEN htp.p('<tr><td>'||v_alt_9||'</td><td><input type="radio" name="p_alt_no" value="9"></td></tr>');END IF;
                IF (v_alt_10 IS NOT NULL) THEN htp.p('<tr><td>'||v_alt_10||'</td><td><input type="radio" name="p_alt_no" value="10"></td></tr>');END IF;
                IF (v_alt_1 IS NOT NULL) THEN
                        htp.p('<tr><td colspan="2" align="right">'); html.submit_link( get.txt('reg_poll') ); htp.p('</td></tr>');
                        html.e_form;
                        htp.p('<tr><td colspan="2" align="left"><a href="poll_util.show_result?p_poll_pk='||v_poll_pk||'">'||get.txt('look_at_results')||'</a></td></tr>');
                ELSE
                        html.e_form;
                END IF;
                htp.p('<tr><td colspan="2" align="left"><a href="poll_util.show_all_results">'||get.txt('look_at_old_results')||'</a></td></tr>
                <tr><td colspan="2" align="left"><a href="poll_util.edit_poll?p_command=new">'||get.txt('add_new_poll')||'</a></td></tr>');
                html.e_table;
                html.e_box_2;
--      END IF;
END IF;
EXCEPTION
WHEN NO_DATA_FOUND THEN
htp.p(get.txt('no_poll_available'));
WHEN DUP_VAL_ON_INDEX THEN
html.b_page(NULL,3,'/index_new.html');
htp.p(get.txt('already_answered_poll'));
html.e_page;
END;
END show_poll;

---------------------------------------------------------------------
-- Name: reg_date
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE reg_date (    p_poll_pk               IN poll.poll_pk%TYPE                    DEFAULT NULL,
                        p_start_date            IN VARCHAR2                             DEFAULT NULL,
                        p_end_date              IN VARCHAR2                             DEFAULT NULL
                        )
IS
BEGIN
DECLARE

v_question              poll.question%TYPE              DEFAULT NULL;
v_start_date            poll.start_date%TYPE            DEFAULT NULL;
v_end_date              poll.end_date%TYPE              DEFAULT NULL;


BEGIN
IF (    login.timeout('poll_util.reg_date') > 0  AND
        login.right('poll_util.reg_date') > 0 ) THEN

        IF ( p_poll_pk IS NOT NULL AND p_start_date IS NOT NULL AND p_end_date IS NOT NULL ) THEN
                UPDATE  poll
                SET     start_date = TO_DATE(trans(p_start_date),get.txt('date_long')),
                        end_date = TO_DATE(trans(p_end_date),get.txt('date_long'))
                WHERE   poll_pk = p_poll_pk;
                COMMIT;
                html.jump_to('poll_util.accepted');
        ELSIF ( p_poll_pk IS NOT NULL AND p_end_date IS NOT NULL ) THEN
                v_end_date := to_date(p_end_date,get.txt('date_long'));
                html.b_adm_page;
                html.b_box( get.txt('reg_date_poll') );
                html.b_table;
                SELECT  question, start_date
                INTO    v_question, v_start_date
                FROM    poll
                WHERE   poll_pk = p_poll_pk
                ;
                htp.p('<tr><td colspan="2">'|| v_question ||'</td><td>');
                html.b_form('poll_util.reg_date');
                htp.p('<input type="hidden" name="p_poll_pk" value="'|| p_poll_pk ||'">
                <tr><td>'|| get.txt('start_date') ||':</td><td>
                <input type="hidden" name="p_start_date" value="'||to_char(v_start_date,get.txt('date_long'))||'">
                '||to_char(v_start_date,get.txt('date_long'))||'</td></tr>
                <tr><td>'|| get.txt('end_date') ||':</td><td>
                <input type="text" size="16" name="p_end_date" value="choose_date"
                        onSelect=javascript:openWin(''date_time.startup?p_variable=p_end_date&p_start_date='||TO_CHAR(v_end_date,get.txt('date_full'))||''',300,170)
                        onClick=javascript:openWin(''date_time.startup?p_variable=p_end_date&p_start_date='||TO_CHAR(v_end_date,get.txt('date_full'))||''',300,170)>
                '|| html.popup( get.txt('change_date'),'date_time.startup?p_variable=p_end_date&p_start_date='||TO_CHAR(v_end_date,get.txt('date_full'))||'','300', '170') ||'
                </td></tr>
                <tr><td colspan="2" align="center">'); html.submit_link( get.txt('Publish poll') ); htp.p('</td></tr>');
                html.e_form;
                html.e_box;
                html.e_table;
                html.e_page_2;
        ELSE
                html.b_adm_page;
                html.b_box( get.txt('reg_date_poll') );
                html.b_table;
                SELECT  question
                INTO    v_question
                FROM    poll
                WHERE   poll_pk = p_poll_pk
                ;
                htp.p('<tr><td colspan="2">'|| v_question ||'</td><td>');
                html.b_form('poll_util.reg_date');
                htp.p('<input type="hidden" name="p_poll_pk" value="'|| p_poll_pk ||'">
                <tr><td>'|| get.txt('start_date') ||':</td><td>
                <input type="text" size="16" name="p_start_date" value="choose_date"
                        onSelect=javascript:openWin(''date_time.startup?p_variable=p_start_date'',300,170)
                        onClick=javascript:openWin(''date_time.startup?p_variable=p_start_date'',300,170)>
                '|| html.popup( get.txt('change_date'),'date_time.startup?p_variable=p_start_date','300', '170') ||'
                </td></tr>
                <tr><td>'|| get.txt('end_date') ||':</td><td>
                <input type="text" size="16" name="p_end_date" value="choose_date"
                        onSelect=javascript:openWin(''date_time.startup?p_variable=p_end_date'',300,170)
                        onClick=javascript:openWin(''date_time.startup?p_variable=p_end_date'',300,170)>
                '|| html.popup( get.txt('change_date'),'date_time.startup?p_variable=p_end_date','300', '170') ||'
                </td></tr>
                <tr><td colspan="2" align="center">'); html.submit_link( get.txt('Publish poll') ); htp.p('</td></tr>');
                html.e_form;
                html.e_box;
                html.e_table;
                html.e_page_2;
        END IF;
END IF;
END;
END reg_date;

---------------------------------------------------------------------
-- Name: show_results
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 25.07.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE show_result ( p_poll_pk               IN poll.poll_pk%TYPE                    DEFAULT NULL )
IS
BEGIN
DECLARE

v_question              poll.question%TYPE                      DEFAULT NULL;
v_value                 poll.question%TYPE                      DEFAULT NULL;
v_alt                   poll_answer.alt_no%TYPE                 DEFAULT 0;
v_alt_no                poll_answer.alt_no%TYPE                 DEFAULT NULL;
v_count                 NUMBER                                  DEFAULT NULL;
v_prosent               NUMBER                                  DEFAULT NULL;
v_alt_1                 poll.alt_1%TYPE                         DEFAULT NULL;
v_alt_2                 poll.alt_2%TYPE                         DEFAULT NULL;
v_alt_3                 poll.alt_3%TYPE                         DEFAULT NULL;
v_alt_4                 poll.alt_4%TYPE                         DEFAULT NULL;
v_alt_5                 poll.alt_5%TYPE                         DEFAULT NULL;
v_alt_6                 poll.alt_6%TYPE                         DEFAULT NULL;
v_alt_7                 poll.alt_7%TYPE                         DEFAULT NULL;
v_alt_8                 poll.alt_8%TYPE                         DEFAULT NULL;
v_alt_9                 poll.alt_9%TYPE                         DEFAULT NULL;
v_alt_10                poll.alt_10%TYPE                        DEFAULT NULL;
v_fora_type_pk          fora_type.fora_type_pk%TYPE             DEFAULT NULL;
v_name                  fora_type.name%TYPE                     DEFAULT NULL;
v_service_pk            service.service_pk%TYPE                 DEFAULT NULL;

CURSOR  get_poll_res ( v_input NUMBER ) IS
SELECT  count( * )
FROM    poll_answer
WHERE   poll_fk = p_poll_pk
AND     alt_no = v_input
;

BEGIN

-- Legger inn i statistikk tabellen
v_service_pk := get.serv;
stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,23,v_service_pk);

SELECT  question, alt_1, alt_2, alt_3, alt_4, alt_5,
        alt_6, alt_7, alt_8, alt_9, alt_10, fora_type_fk
INTO    v_question, v_alt_1, v_alt_2, v_alt_3, v_alt_4, v_alt_5,
        v_alt_6, v_alt_7, v_alt_8, v_alt_9, v_alt_10, v_fora_type_pk
FROM    poll
WHERE   poll_pk=p_poll_pk
AND     start_date < SYSDATE
;

IF ( v_fora_type_pk is not null ) THEN
   SELECT  ft.name
   INTO    v_name
   FROM    poll p, fora_type ft
   WHERE   p.poll_pk=p_poll_pk
   AND     p.start_date < SYSDATE
   AND     p.fora_type_fk = ft.fora_type_pk
   ;
END IF;

SELECT  count (*)
INTO    v_count
FROM    poll_answer
WHERE   poll_fk = p_poll_pk
;

html.b_page(NULL,'60',NULL,NULL, 1);
html.b_box( v_question, '100%', 'show_result_poll' );
html.b_table;
LOOP
        v_alt:=v_alt+1;
        IF ( v_alt = 1 ) THEN v_value:=v_alt_1;
        ELSIF ( v_alt = 2 ) THEN v_value:=v_alt_2;
        ELSIF ( v_alt = 3 ) THEN v_value:=v_alt_3;
        ELSIF ( v_alt = 4 ) THEN v_value:=v_alt_4;
        ELSIF ( v_alt = 5 ) THEN v_value:=v_alt_5;
        ELSIF ( v_alt = 6 ) THEN v_value:=v_alt_6;
        ELSIF ( v_alt = 7 ) THEN v_value:=v_alt_7;
        ELSIF ( v_alt = 8 ) THEN v_value:=v_alt_8;
        ELSIF ( v_alt = 9 ) THEN v_value:=v_alt_9;
        ELSIF ( v_alt = 10 ) THEN v_value:=v_alt_10;
        ELSE v_value := NULL;
        END IF;
        OPEN get_poll_res (v_alt);
        FETCH get_poll_res INTO v_alt_no;
        exit when v_value IS NULL;
        IF ( v_count = 0 ) THEN
                v_prosent := 0;
        ELSE
                v_prosent := (100*v_alt_no)/v_count;
        END IF;
        htp.p('<TR><TD><IMG SRC="/img/start-stopp.gif" WIDTH="1" HEIGHT="15"
        ><IMG SRC="/img/stem_nei.gif" WIDTH="'|| ROUND(v_prosent,1) ||'" HEIGHT="15"
        ><IMG SRC="/img/start-stopp.gif" WIDTH="1" HEIGHT="15"
        ><FONT SIZE="-1">&nbsp;('|| ROUND(v_prosent,1) ||'%)&nbsp;</FONT>
        </TD><TD>'||v_value||'</TD></TR>');
        CLOSE get_poll_res;
END LOOP;
CLOSE get_poll_res;
htp.p('<TR><TD colspan="2">Det er '||v_count||' personer som har avgitt stemme.</TD></TR>');
IF ( v_fora_type_pk is not null ) THEN
   htp.p('<TR><TD colspan="2">Diskuter mer i forumet: <a href="fora_util.frames?p_fora_type_pk='||v_fora_type_pk||'">'||v_name||'</a></TD></TR>');
END IF;
html.e_table;
html.e_box;
html.e_page;
EXCEPTION
WHEN NO_DATA_FOUND THEN
htp.p('Denne pollen har ikke startet enda!!!');
END;
END show_result;





---------------------------------------------------------------------
-- Name: show_all_results
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 25.07.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE show_all_results (    p_poll_pk               IN poll.poll_pk%TYPE                    DEFAULT NULL )
IS
BEGIN
DECLARE

v_question              poll.question%TYPE                      DEFAULT NULL;
v_value                 poll.question%TYPE                      DEFAULT NULL;
v_alt                   poll_answer.alt_no%TYPE                 DEFAULT 0;
v_alt_no                poll_answer.alt_no%TYPE                 DEFAULT NULL;
v_count                 NUMBER                                  DEFAULT NULL;
v_prosent               NUMBER                                  DEFAULT NULL;
v_alt_1                 poll.alt_1%TYPE                         DEFAULT NULL;
v_alt_2                 poll.alt_2%TYPE                         DEFAULT NULL;
v_alt_3                 poll.alt_3%TYPE                         DEFAULT NULL;
v_alt_4                 poll.alt_4%TYPE                         DEFAULT NULL;
v_alt_5                 poll.alt_5%TYPE                         DEFAULT NULL;
v_alt_6                 poll.alt_6%TYPE                         DEFAULT NULL;
v_alt_7                 poll.alt_7%TYPE                         DEFAULT NULL;
v_alt_8                 poll.alt_8%TYPE                         DEFAULT NULL;
v_alt_9                 poll.alt_9%TYPE                         DEFAULT NULL;
v_alt_10                poll.alt_10%TYPE                        DEFAULT NULL;
v_start_date            poll.start_date%TYPE                    DEFAULT NULL;
v_end_date              poll.end_date%TYPE                      DEFAULT NULL;
v_poll_pk               poll.poll_pk%TYPE                       DEFAULT NULL;
v_service_pk            service.service_pk%TYPE                 DEFAULT NULL;

CURSOR  get_all IS
SELECT  question, start_date, end_date, poll_pk
FROM    poll, poll_type pt
WHERE   start_date < SYSDATE
AND     poll_type_fk = pt.poll_type_pk
AND     pt.service_fk = get.serv
ORDER BY end_date DESC
;

BEGIN

-- Legger inn i statistikk tabellen
v_service_pk := get.serv;
stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,23,v_service_pk);

html.b_page(NULL,NULL,NULL,NULL, 1);
--html.b_page;
IF ( p_poll_pk IS NOT NULL ) THEN
        SELECT  question, alt_1, alt_2, alt_3, alt_4, alt_5,
                alt_6, alt_7, alt_8, alt_9, alt_10
        INTO    v_question, v_alt_1, v_alt_2, v_alt_3, v_alt_4, v_alt_5,
                v_alt_6, v_alt_7, v_alt_8, v_alt_9, v_alt_10
        FROM    poll
        WHERE   start_date < SYSDATE
        AND     poll_pk = p_poll_pk
        ;

        SELECT  count (*)
        INTO    v_count
        FROM    poll_answer
        WHERE   poll_fk = p_poll_pk
        ;

        html.b_box( v_question, '100%', 'show_result_poll' );
        html.b_table;
        htp.p('<tr><td><b>'||get.txt('prosent')||'</b></td><td><b>'||get.txt('alternative')||'</b></td><td><b>'||get.txt('amount')||'</b></td></tr>');
        LOOP
                v_alt:=v_alt+1;

                IF ( v_alt = 1 ) THEN v_value:=v_alt_1;
                ELSIF ( v_alt = 2 ) THEN v_value:=v_alt_2;
                ELSIF ( v_alt = 3 ) THEN v_value:=v_alt_3;
                ELSIF ( v_alt = 4 ) THEN v_value:=v_alt_4;
                ELSIF ( v_alt = 5 ) THEN v_value:=v_alt_5;
                ELSIF ( v_alt = 6 ) THEN v_value:=v_alt_6;
                ELSIF ( v_alt = 7 ) THEN v_value:=v_alt_7;
                ELSIF ( v_alt = 8 ) THEN v_value:=v_alt_8;
                ELSIF ( v_alt = 9 ) THEN v_value:=v_alt_9;
                ELSIF ( v_alt = 10 ) THEN v_value:=v_alt_10;
                ELSE v_value := NULL;
                END IF;
                exit when v_value IS NULL;

                SELECT  count(*)
                INTO    v_alt_no
                FROM    poll_answer
                WHERE   poll_fk = p_poll_pk
                AND     alt_no = v_alt
                ;

                IF ( v_count > 0 ) THEN
                        v_prosent := (100*v_alt_no)/v_count;
                ELSE
                        v_prosent := 0;
                END IF;
                htp.p('<TR><TD><IMG SRC="/img/start-stopp.gif" WIDTH="1" HEIGHT="15"
                ><IMG SRC="/img/stem_nei.gif" WIDTH="'|| ROUND(v_prosent,1) ||'" HEIGHT="15"
                ><IMG SRC="/img/start-stopp.gif" WIDTH="1" HEIGHT="15"
                ><FONT SIZE="-1">&nbsp;('|| ROUND(v_prosent,1) ||'%)&nbsp;</FONT>
                </TD><TD>'||v_value||'</TD><TD>'||v_alt_no||'</td></TR>');
        END LOOP;
        htp.p('<tr><td colspan="3">'||get.txt('poll_all_info')||'</td></tr>');
        html.e_table;
        html.e_box;
END IF;


html.b_box( get.txt('all_polls'), '100%', 'show_result_poll'  );
html.b_table;
htp.p('<tr><td><b>'||get.txt('question')||'</b></td><td><b>'||get.txt('date')||'</b></td><td><b>'||get.txt('amount')||'</b></td></tr>');
OPEN get_all;
LOOP
        FETCH get_all INTO v_question, v_start_date, v_end_date, v_poll_pk;
        exit when get_all%NOTFOUND;
        SELECT  count (*)
        INTO    v_count
        FROM    poll_answer
        WHERE   poll_fk = v_poll_pk
        ;
        htp.p('<TR><TD><a href="poll_util.show_all_results?p_poll_pk='||v_poll_pk||'">
        '||v_question||'</TD><TD>'||to_char(v_start_date,get.txt('date_short'))||'
        &nbsp;-&nbsp;'||to_char(v_end_date,get.txt('date_short'))||'</TD>
        <TD>'||v_count||'</td></TR>');
END LOOP;
CLOSE get_all;

html.e_table;
html.e_box;
html.e_page;
EXCEPTION
WHEN NO_DATA_FOUND THEN
htp.p('Denne pollen har ikke startet enda!!!');
END;
END show_all_results;

---------------------------------------------------------------------
-- Name: select_fora_type
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE select_fora_type (    p_fora_type_pk          IN fora_type.fora_type_pk%TYPE          DEFAULT NULL)
IS
BEGIN
DECLARE

v_service_pk            fora_type.service_fk%TYPE       DEFAULT NULL;
v_name                  fora_type.name%TYPE             DEFAULT NULL;
v_fora_type_pk          fora_type.fora_type_pk%TYPE     DEFAULT NULL;
v_language_pk           fora_type.language_fk%TYPE      DEFAULT NULL;
v_value                 VARCHAR2(20)                    DEFAULT NULL;

CURSOR  all_fora_types IS
SELECT  name, fora_type_pk, language_fk, service_fk
FROM    fora_type
WHERE   organization_fk IS NULL
AND     activated = 1
ORDER BY name
;

BEGIN

htp.p('<select name="p_fora_type_pk">
<option value="">'||get.txt('no_fora')||'</option>');
OPEN all_fora_types;
LOOP
        fetch all_fora_types into v_name, v_fora_type_pk, v_language_pk, v_service_pk;
        exit when all_fora_types%NOTFOUND;
        IF ( v_fora_type_pk = p_fora_type_pk ) THEN
                v_value := ' selected';
        ELSE
                v_value := '';
        END IF;
        htp.p('<option value="'|| v_fora_type_pk ||'"'||v_value||'>'|| v_name ||' ('|| get.serv_name(v_service_pk) ||')</option>');
END LOOP;
close all_fora_types;
htp.p('</select>');

END;
END select_fora_type;


---------------------------------------------------------------------
---------------------------------------------------------------------
END;
/
show errors;
