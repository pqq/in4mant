set define off
PROMPT *** package: fora_util ***

CREATE OR REPLACE PACKAGE fora_util IS
PROCEDURE plain (               p_fora_type_pk          IN fora_type.fora_type_pk%TYPE          DEFAULT NULL,
                                p_name                  IN fora_type.name%TYPE                  DEFAULT NULL,
                                p_bgcolor               IN VARCHAR2                             DEFAULT NULL );

PROCEDURE startup;

PROCEDURE sub_fora(             p_fora_pk               IN fora.fora_pk%TYPE                    DEFAULT NULL,
                                p_color                 IN BOOLEAN                              DEFAULT FALSE );

PROCEDURE edit_fora_type(       p_command               IN varchar2                             DEFAULT NULL,
                                p_name                  IN fora_type.name%TYPE                  DEFAULT NULL,
                                p_description           IN fora_type.description%TYPE           DEFAULT NULL,
                                p_service_pk            IN fora_type.service_fk%TYPE            DEFAULT NULL,
                                p_language_pk           IN fora_type.language_fk%TYPE           DEFAULT NULL,
                                p_fora_type_pk          IN fora_type.fora_type_pk%TYPE          DEFAULT NULL,
                                p_organization_pk       IN fora_type.organization_fk%TYPE       DEFAULT NULL,
                                p_activated             IN fora_type.activated%TYPE             DEFAULT NULL
                        );
PROCEDURE select_fora_type(     p_fora_type_pk          IN fora_type.fora_type_pk%TYPE          DEFAULT NULL);
PROCEDURE edit_fora (           p_command               IN varchar2                             DEFAULT NULL,
                                p_fora_pk               IN fora.fora_pk%TYPE                    DEFAULT NULL,
                                p_fora_type_pk          IN fora.fora_type_fk%TYPE               DEFAULT NULL,
                                p_parent_fora_pk        IN fora.parent_fora_fk%TYPE             DEFAULT NULL,
                                p_user_pk               IN fora.user_fk%TYPE                    DEFAULT NULL,
                                p_user_nick             IN fora.user_nick%TYPE                  DEFAULT NULL,
                                p_published_date        IN fora.published_date%TYPE             DEFAULT NULL,
                                p_main_title            IN VARCHAR2                             DEFAULT NULL,
                                p_sub_title             IN VARCHAR2                             DEFAULT NULL,
                                p_ingress               IN VARCHAR2                             DEFAULT NULL,
                                p_body                  IN LONG                                 DEFAULT NULL,
                                p_level_no              IN fora.level_no%TYPE                   DEFAULT NULL,
                                p_name                  IN fora_type.name%TYPE                  DEFAULT NULL
                         );
PROCEDURE all_fora(             p_service_pk            IN fora_type.service_fk%TYPE            DEFAULT NULL,
                                p_language_pk           IN fora_type.language_fk%TYPE           DEFAULT NULL,
                                p_organization_pk       IN fora_type.organization_fk%TYPE       DEFAULT NULL,
                                p_command               IN varchar2                             DEFAULT NULL
                );
PROCEDURE frames (              p_fora_type_pk          IN fora_type.fora_type_pk%TYPE          DEFAULT NULL,
                                p_name                  IN fora_type.name%TYPE                  DEFAULT NULL,
                                p_organization_pk       IN organization.organization_pk%TYPE    DEFAULT NULL,
                                p_width                 IN NUMBER                               DEFAULT NULL,
                                p_fora_pk               IN fora.fora_pk%TYPE                    DEFAULT NULL
                );
PROCEDURE select_organization ( p_organization_pk       IN organization.organization_pk%TYPE    DEFAULT NULL);
PROCEDURE print_top (         p_organization_pk         IN organization.organization_pk%TYPE    DEFAULT NULL,
                              p_b_page                  IN NUMBER                               DEFAULT NULL);

END;
/
CREATE OR REPLACE PACKAGE BODY fora_util IS
---------------------------------------------------------------------
---------------------------------------------------------------------
-- Name: startup
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 30.07.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE startup
IS
BEGIN
        all_fora;
END startup;

---------------------------------------------------------------------
-- Name: edit_fora_type
-- Type: procedure
-- What: Genererer dokumentarkiv
-- Made: Espen Messel
-- Date: 30.07.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE edit_fora_type (      p_command               IN varchar2                             DEFAULT NULL,
                                p_name                  IN fora_type.name%TYPE                  DEFAULT NULL,
                                p_description           IN fora_type.description%TYPE           DEFAULT NULL,
                                p_service_pk            IN fora_type.service_fk%TYPE            DEFAULT NULL,
                                p_language_pk           IN fora_type.language_fk%TYPE           DEFAULT NULL,
                                p_fora_type_pk          IN fora_type.fora_type_pk%TYPE          DEFAULT NULL,
                                p_organization_pk       IN fora_type.organization_fk%TYPE       DEFAULT NULL,
                                p_activated             IN fora_type.activated%TYPE             DEFAULT NULL
                         )
IS
BEGIN
DECLARE

CURSOR  get_fora_type IS
SELECT  fora_type_pk, name, description, language_fk,
        service_fk, organization_fk, activated
FROM    fora_type
WHERE   organization_fk IS NULL
ORDER BY name
;

v_fora_type_pk          fora_type.fora_type_pk%TYPE             DEFAULT NULL;
v_name                  fora_type.name%TYPE                     DEFAULT NULL;
v_org_name              organization.name%TYPE                  DEFAULT NULL;
v_description           fora_type.description%TYPE              DEFAULT NULL;
v_language_pk           fora_type.language_fk%TYPE              DEFAULT NULL;
v_service_pk            fora_type.service_fk%TYPE               DEFAULT NULL;
v_organization_pk       fora_type.organization_fk%TYPE          DEFAULT NULL;
v_activated             fora_type.activated%TYPE                DEFAULT NULL;

BEGIN

IF (    login.timeout('fora_util.edit_fora_type') > 0  AND
        login.right('fora_util.edit_fora_type') > 0 ) THEN

        IF ( p_command = 'activate' ) THEN
                UPDATE fora_type
                SET     activated=p_activated
                WHERE   fora_type_pk=p_fora_type_pk;
                commit;
        END IF;
        IF ( p_command = 'update') THEN
                SELECT  fora_type_pk, name, description, language_fk,
                        service_fk, organization_fk
                INTO    v_fora_type_pk, v_name, v_description, v_language_pk,
                        v_service_pk, v_organization_pk
                FROM    fora_type
                WHERE   fora_type_pk=p_fora_type_pk
                ;
                commit;
                html.b_page_2;
                html.b_box( get.txt('fora_type') );
                html.b_table;
                html.b_form('fora_util.edit_fora_type');
                htp.p('<input type="hidden" name="p_command" value="edit">
                <input type="hidden" name="p_fora_type_pk" value="'||v_fora_type_pk||'">
                <tr><td>'|| get.txt('service') ||':</td><td>');
                html.select_service(v_service_pk);
                htp.p('</td></tr>
                <tr><td>'|| get.txt('language') ||':</td><td>');
                html.select_lang(v_language_pk);
                htp.p('</td></tr>
                <tr><td>'|| get.txt('organization') ||':</td><td>');
                select_organization(v_organization_pk);
                htp.p('</td></tr>
                <tr><td>'|| get.txt('name') ||':</td><td>
                '|| html.text('p_name', '50', '50', v_name ) ||'
                </td></tr>
                <tr><td>'|| get.txt('description') ||':(maks 150 tegn)</td><td>
                '|| html.text('p_description', '50', '150', v_description ) ||'
                </td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('update') ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page;
        ELSIF ( p_command = 'edit') THEN
                UPDATE  fora_type
                SET     name=p_name,
                        description=p_description,
                        language_fk=p_language_pk,
                        service_fk=p_service_pk,
                        organization_fk=p_organization_pk
                WHERE   fora_type_pk=p_fora_type_pk;
                commit;
                html.jump_to ( 'fora_util.edit_fora_type' );
        ELSIF ( p_command = 'delete') THEN
                IF ( p_name = 'yes') THEN
                        DELETE  FROM fora_type
                        WHERE   fora_type_pk=p_fora_type_pk;
                        commit;
                        html.jump_to ( 'fora_util.edit_fora_type');
                ELSIF ( p_name IS NULL ) THEN
                        html.jump_to ( 'fora_util.edit_fora_type');
                ELSE
                        html.b_page_2;
                        html.b_box( get.txt('fora_type') );
                        html.b_table;
                        html.b_form('fora_util.edit_fora_type');
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="yes">
                        <input type="hidden" name="p_fora_type_pk" value="'||p_fora_type_pk||'">
                        <td>'|| get.txt('delete_info')||' "'||p_name||'" ?'); html.submit_link( get.txt('YES') ); htp.p('</td>');
                        html.e_form;
                        htp.p('<td>'); html.button_link( get.txt('NO'), 'fora_util.edit_fora_type');htp.p('</td></tr>');
                        html.e_table;
                        html.e_box;
                        html.e_page;
                END IF;
        ELSIF ( p_command = 'new') THEN
                html.b_page;
                html.b_box( get.txt('fora_type') );
                html.b_table;
                html.b_form('fora_util.edit_fora_type','form_insert');
                htp.p('<input type="hidden" name="p_command" value="insert">
                <tr><td>'|| get.txt('service') ||':</td><td>');
                html.select_service(NULL, -1000);
                htp.p('</td></tr>
                <tr><td>'|| get.txt('language') ||':</td><td>');
                html.select_lang;
                htp.p('</td></tr>
                <tr><td>'|| get.txt('organization') ||':</td><td>');
                select_organization;
                htp.p('</td></tr>
                <tr><td>'|| get.txt('name') ||':</td><td>
                '|| html.text('p_name', '50', '50', '' ) ||'
                </td></tr>
                <tr><td>'|| get.txt('description') ||':(maks 150 tegn)</td><td>
                '|| html.text('p_description', '50', '150', '' ) ||'
                </td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('insert'), 'form_insert' ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page;
        ELSIF ( p_command = 'insert') THEN
                INSERT INTO fora_type
                ( fora_type_pk, name, description, language_fk,
                service_fk, organization_fk )
                VALUES
                ( fora_type_seq.NEXTVAL, SUBSTR(p_name,1,50), SUBSTR(p_description,1,150),
                p_language_pk, p_service_pk, p_organization_pk );
                commit;
                html.jump_to ( 'fora_util.edit_fora_type' );
        ELSE
                html.b_page_2;
                html.b_box( get.txt('fora_type') );
                html.b_table;
                OPEN get_fora_type;
                LOOP
                        FETCH get_fora_type INTO v_fora_type_pk, v_name, v_description,
                                                 v_language_pk, v_service_pk, v_organization_pk,
                                                 v_activated;
                        exit when get_fora_type%NOTFOUND;
                        IF ( v_organization_pk IS NOT NULL ) THEN
                                v_org_name := get.oname(v_organization_pk);
                        ELSE
                                v_org_name := get.serv_name(v_service_pk);
                        END IF;

                        htp.p('<tr><td>'||v_name||'</td>
                        <td>'||v_org_name||'</td>');

                        html.b_form('fora_util.edit_fora_type','form'|| v_fora_type_pk);
                        htp.p('<input type="hidden" name="p_command" value="activate">
                        <input type="hidden" name="p_fora_type_pk" value="'||v_fora_type_pk||'">
                        <td>');
                        IF ( v_activated > 0 ) THEN
                                htp.p('<input type="hidden" name="p_activated" value="0">');
                                html.submit_link( get.txt('deactivate'), 'form'||v_fora_type_pk );
                        ELSE
                                htp.p('<input type="hidden" name="p_activated" value="1">');
                                html.submit_link( get.txt('activate'), 'form'||v_fora_type_pk );
                        END IF;
                        htp.p('</td>');
                        html.e_form;

                        html.b_form('fora_util.edit_fora_type','form'|| v_fora_type_pk);
                        htp.p('<input type="hidden" name="p_command" value="update">
                        <input type="hidden" name="p_fora_type_pk" value="'||v_fora_type_pk||'">
                        <td>'); html.submit_link( get.txt('edit'), 'form'||v_fora_type_pk ); htp.p('</td>');
                        html.e_form;
                        html.b_form('fora_util.edit_fora_type','delete'|| v_fora_type_pk);
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="'||v_name||'">
                        <input type="hidden" name="p_fora_type_pk" value="'||v_fora_type_pk||'">
                        <td>'); html.submit_link( get.txt('delete'), 'delete'||v_fora_type_pk ); htp.p('</td></tr>');
                        html.e_form;
                END LOOP;
                CLOSE get_fora_type;
                html.b_form('fora_util.edit_fora_type','form_new');
                htp.p('<input type="hidden" name="p_command" value="new">
                <td>'); html.submit_link( get.txt('new'), 'form_new'); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page;
        END IF;
END IF;
EXCEPTION
WHEN NO_DATA_FOUND
THEN
htp.p('No data found! edit_reg_date.sql -> edit_fora_type');
END;
END edit_fora_type;

---------------------------------------------------------------------
-- Name: select_fora_type
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE select_fora_type (    p_fora_type_pk          IN fora_type.fora_type_pk%TYPE          DEFAULT NULL)
IS
BEGIN
DECLARE

v_service_pk            fora_type.service_fk%TYPE       DEFAULT NULL;
v_name                  fora_type.name%TYPE             DEFAULT NULL;
v_fora_type_pk          fora_type.fora_type_pk%TYPE     DEFAULT NULL;
v_language_pk           fora_type.language_fk%TYPE      DEFAULT NULL;
v_value                 VARCHAR2(20)                    DEFAULT NULL;

CURSOR  all_fora_types IS
SELECT  name, fora_type_pk, language_fk
FROM    fora_type
WHERE   service_fk = v_service_pk
AND     activated > 0
ORDER BY name
;

BEGIN

v_service_pk := get.serv;
htp.p('<select name="p_fora_type_pk">');
OPEN all_fora_types;
LOOP
        fetch all_fora_types into v_name, v_fora_type_pk, v_language_pk;
        exit when all_fora_types%NOTFOUND;
        IF ( v_fora_type_pk = p_fora_type_pk ) THEN
                v_value := ' selected';
        ELSE
                v_value := '';
        END IF;
        htp.p('<option value="'|| v_fora_type_pk ||'"'||v_value||'>'|| v_name ||'</option>');
END LOOP;
close all_fora_types;
htp.p('</select>');

END;
END select_fora_type;

---------------------------------------------------------------------
-- Name: select_organization
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE select_organization (p_organization_pk        IN organization.organization_pk%TYPE            DEFAULT NULL)
IS
BEGIN
DECLARE
        v_name                  organization.name%TYPE                  DEFAULT NULL;
        v_organization_pk       organization.organization_pk%TYPE       DEFAULT NULL;
        v_value                 VARCHAR2(20)                            DEFAULT NULL;

        CURSOR  all_organizations IS
        SELECT  name, organization_pk
        FROM    organization
        ORDER BY name
        ;

BEGIN
--      v_service_pk := get.serv;
--      v_service_pk := 1;
        htp.p('<select name="p_organization_pk">
        <option value="">'||get.txt('service')||'</option>');
        OPEN all_organizations;
        LOOP
                fetch all_organizations into v_name, v_organization_pk;
                exit when all_organizations%NOTFOUND;
                IF ( v_organization_pk = p_organization_pk ) THEN
                        v_value := ' selected';
                ELSE
                        v_value := '';
                END IF;
                htp.p('<option value="'|| v_organization_pk ||'"'||v_value||'>'|| v_name ||'</option>');
        END LOOP;
        close all_organizations;
        htp.p('</select>');
END;
END select_organization;

---------------------------------------------------------------------
-- Name: edit_fora
-- Type: procedure
-- What: Genererer dokumentarkiv
-- Made: Espen Messel
-- Date: 20.07.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE edit_fora (   p_command               IN varchar2                             DEFAULT NULL,
                        p_fora_pk               IN fora.fora_pk%TYPE                    DEFAULT NULL,
                        p_fora_type_pk          IN fora.fora_type_fk%TYPE               DEFAULT NULL,
                        p_parent_fora_pk        IN fora.parent_fora_fk%TYPE             DEFAULT NULL,
                        p_user_pk               IN fora.user_fk%TYPE                    DEFAULT NULL,
                        p_user_nick             IN fora.user_nick%TYPE                  DEFAULT NULL,
                        p_published_date        IN fora.published_date%TYPE             DEFAULT NULL,
                        p_main_title            IN VARCHAR2                             DEFAULT NULL,
                        p_sub_title             IN VARCHAR2                             DEFAULT NULL,
                        p_ingress               IN VARCHAR2                             DEFAULT NULL,
                        p_body                  IN LONG                                 DEFAULT NULL,
                        p_level_no              IN fora.level_no%TYPE                   DEFAULT NULL,
                        p_name                  IN fora_type.name%TYPE                  DEFAULT NULL
                         )
IS
BEGIN
DECLARE

CURSOR  get_fora IS
SELECT  f.fora_pk, f.parent_fora_fk, f.main_title, f.sub_title,
        f.published_date, f.fora_type_fk, ft.name, f.user_nick
FROM    fora f, fora_type ft
WHERE   f.fora_type_fk = ft.fora_type_pk
AND     f.fora_type_fk = p_fora_type_pk
AND     f.level_no = 1
AND     ft.activated > 0
ORDER BY fora_pk DESC
;

CURSOR  get_sub_fora IS
SELECT  f.fora_pk, f.parent_fora_fk, f.level_no
FROM    fora f, fora_type ft
WHERE   f.fora_type_fk = ft.fora_type_pk
AND     f.fora_type_fk = p_fora_type_pk
AND     ft.activated > 0
ORDER BY f.level_no DESC , f.parent_fora_fk DESC , f.fora_pk DESC
;

v_fora_pk               fora.fora_pk%TYPE               DEFAULT NULL;
v_fora_type_pk          fora.fora_type_fk%TYPE          DEFAULT NULL;
v_parent_fora_pk        fora.parent_fora_fk%TYPE        DEFAULT NULL;
v_user_pk               fora.user_fk%TYPE               DEFAULT NULL;
v_user_nick             fora.user_nick%TYPE             DEFAULT NULL;
v_published_date        fora.published_date%TYPE        DEFAULT NULL;
v_main_title            VARCHAR2(300)                   DEFAULT NULL;
v_sub_title             VARCHAR2(300)                   DEFAULT NULL;
v_ingress               VARCHAR2(1200)                  DEFAULT NULL;
v_body                  LONG                            DEFAULT NULL;
v_level_no              fora.level_no%TYPE              DEFAULT NULL;
v_color                 varchar2(30)                    DEFAULT NULL;
v_name                  fora_type.name%TYPE             DEFAULT NULL;
v_jump                  varchar2(500)                   DEFAULT NULL;
v_count                 number                          DEFAULT NULL;
v_organization_pk       fora_type.organization_fk%TYPE  DEFAULT NULL;
v_ip_address            fora.ip_address%TYPE            DEFAULT NULL;

TYPE array_table        IS TABLE OF NUMBER              INDEX BY BINARY_INTEGER;
v_fora_read             array_table;
TYPE html_table         IS TABLE OF VARCHAR2(4000)      INDEX BY BINARY_INTEGER;
v_fora_html             html_table;

CURSOR  get_fora_read IS
SELECT  fora_fk
FROM    fora_read
WHERE   user_fk = v_user_pk
ORDER BY fora_fk
;

BEGIN
   IF ( p_command = 'new') THEN
      v_jump :='fora_util.edit_fora?p_command=new&p_parent_fora_pk='||p_parent_fora_pk||'&p_fora_type_pk='||p_fora_type_pk||'&p_level_no='||p_level_no||'&p_main_title='||p_main_title||'';
--      IF ( login.timeout(v_jump, NULL, 2, 400) > 0 ) THEN
         IF ( p_main_title IS NOT NULL ) THEN
            html.b_box( 'Re: '||p_main_title ,'100%', 'fora_util.new_fora');
            v_level_no := p_level_no;
         ELSE
            html.b_box( get.txt('new_posting') ,'100%', 'fora_util.new_fora');
            v_level_no := 0;
         END IF;
         html.b_page_3(400);
         html.b_table;
         html.b_form('fora_util.edit_fora');
         htp.p('<input type="hidden" name="p_command" value="insert">
               <input type="hidden" name="p_parent_fora_pk" value="'|| p_parent_fora_pk ||'">
                 <input type="hidden" name="p_fora_type_pk" value="'||p_fora_type_pk||'">
                 <input type="hidden" name="p_level_no" value="'||v_level_no||'">
                 <input type="hidden" name="p_main_title" value="'||p_main_title||'">
                 <tr><td valign="top">'|| get.txt('nick_name') ||':</td><td>
                 '|| html.text('p_user_nick', '42', '100', '') ||'
               </td></tr>
                 <tr><td valign="top">'|| get.txt('sub_title') ||':</td><td>
                 '|| html.text('p_sub_title', '42', '100', '') ||'
               </td></tr>
                 <tr><td valign="top">'|| get.txt('body') ||':</td><td>
                 <textarea name="p_body" rows="15" cols="40" wrap="virtual"></textarea>
                 </td></tr>
                 <tr><td colspan="2">'); html.submit_link( get.txt('insert')); htp.p('</td></tr>');
         html.e_form;
         html.e_table;
         html.e_box;
         html.e_page_2;
--      END IF;
   ELSIF ( p_command = 'insert' ) THEN
      v_jump :='fora_util.edit_fora';
--      IF ( login.timeout(v_jump, NULL, 2) > 0 ) THEN
         IF ( p_main_title IS NULL ) THEN
            v_main_title := p_sub_title;
            v_sub_title := p_sub_title;
         ELSE
            v_main_title := p_main_title;
            v_sub_title := p_sub_title;
         END IF;
         IF ( p_sub_title IS NULL OR p_user_nick IS NULL ) THEN
            html.b_page_3(400);
            html.b_box( get.txt('error') ,'100%', 'fora_util.show_fora');
            html.b_table;
            htp.p('<tr><td>'||get.txt('no_title_error')||'</td></tr><tr><td>');
            html.button_link( get.txt('go_back'), 'Javascript:history.back()');
            htp.p('</td></tr>');
            html.e_table;
            html.e_box;
            html.e_page_2;
         ELSE
            SELECT  fora_seq.NEXTVAL
              INTO    v_fora_pk
              FROM    dual;
            IF ( p_parent_fora_pk IS NULL ) THEN
               v_parent_fora_pk := v_fora_pk;
            ELSE
               v_parent_fora_pk := p_parent_fora_pk;
            END IF;
            v_user_pk := get.uid;

            INSERT INTO fora
              ( fora_pk, fora_type_fk, parent_fora_fk, user_fk,
                published_date, main_title, sub_title, ingress, body,
                level_no, user_nick, ip_address )
              VALUES
              ( v_fora_pk, p_fora_type_pk, v_parent_fora_pk, v_user_pk,
                SYSDATE, SUBSTR(v_main_title,1,100), SUBSTR(v_sub_title,1,100),
                SUBSTR(p_ingress,1,400), SUBSTR(p_body,1,4000),p_level_no+1,
                p_user_nick, owa_util.get_cgi_env('REMOTE_ADDR')  );
            commit;
/*
            INSERT INTO fora_read
              ( fora_fk, user_fk )
              VALUES
              ( fora_seq.CURRVAL, v_user_pk );
            commit;
*/
            html.jump_to ( 'fora_util.plain?p_fora_type_pk='||p_fora_type_pk||'&p_bgcolor='||REPLACE(get.value('c_'|| get.serv_name ||'_bbs' ),'#',''));
            END IF;
--         END IF;
      ELSIF ( p_command = 'show') THEN
         SELECT  parent_fora_fk, main_title, sub_title,
           published_date, fora_type_fk, ingress, body,
           user_fk, level_no, name, organization_fk, user_nick, ip_address
           INTO    v_parent_fora_pk, v_main_title, v_sub_title,
           v_published_date, v_fora_type_pk, v_ingress,
           v_body, v_user_pk, v_level_no, v_name, v_organization_pk, v_user_nick, v_ip_address
           FROM    fora, fora_type
           WHERE   fora_pk = p_fora_pk
           AND     fora_type_fk=fora_type_pk
           ;

         -- Legger inn i statistikk tabellen
         IF ( v_organization_pk > 0 ) THEN
            stat.reg(v_organization_pk,NULL,NULL,v_fora_type_pk,NULL,NULL,20);
         ELSE
            stat.reg(NULL,NULL,NULL,v_fora_type_pk,NULL,NULL,20);
         END IF;
         IF ( p_parent_fora_pk IS NOT NULL ) THEN
            v_parent_fora_pk := p_parent_fora_pk;
         ELSE
            v_parent_fora_pk := p_fora_pk;
         END IF;

         IF ( v_user_nick IS NULL ) THEN
            v_user_nick := get.login_name(v_user_pk);
         END IF;

         html.b_page_3(400);
         html.b_box( v_name ,'100%', 'fora_util.show_fora');
         html.b_table;
         html.b_form('fora_util.edit_fora','form_new');
         htp.p('<input type="hidden" name="p_command" value="new">
           <input type="hidden" name="p_parent_fora_pk" value="'|| p_fora_pk ||'">
           <input type="hidden" name="p_fora_type_pk" value="'||v_fora_type_pk||'">
           <input type="hidden" name="p_level_no" value="'||v_level_no||'">
                 <input type="hidden" name="p_main_title" value="'||v_main_title||'">
                 <tr><td colspan="2"><HR noShade SIZE="2" align="left"></td></tr>
                 <tr><td><B>'||get.txt('debate')||':</B></td><td>'|| v_name ||'</td></tr>
                 <tr><td><B>'||get.txt('date')||':</B></td><td>'||to_char(v_published_date,get.txt('date_short'))||'</td></tr>
                 <tr><td><B>'||get.txt('from')||':</B></td><td>
                 '|| v_user_nick ||'
               </td></tr>
                 <tr><td><B>'||get.txt('heading')||':</B></td><td>'||v_main_title||'</td></tr>
                 <tr><td colspan="2"><HR noShade SIZE="2" align="left"></td></tr>
                 <tr><td colspan="2"><B>'|| v_sub_title ||'</B></td></tr>
           <tr><td colspan="2">&nbsp;</td></tr>
                 <tr><td colspan="2">'|| html.rem_tag( v_body ) ||'</td></tr>
         <tr><td>');
         html.submit_link(get.txt('answer_fora'),'form_new');
         html.e_form;
         htp.p('</td><td>');
--              html.b_form('fora_util.edit_fora','new_fora');
--              htp.p('<input type="hidden" name="p_fora_type_pk" value="'||v_fora_type_pk||'">
--              <input type="hidden" name="p_command" value="new">');
--              html.submit_link(get.txt('new_posting'),'new_fora');
--              html.e_form;
                htp.p('&nbsp;</td></tr>');
                html.e_table;
                html.e_box;
                html.e_page_2;
                v_user_pk := get.uid;
                IF ( v_user_pk IS NOT NULL ) THEN
                        INSERT INTO fora_read
                        ( fora_fk, user_fk )
                        VALUES
                        (p_fora_pk, v_user_pk );
                        commit;
                END IF;
        ELSE
                html.b_page_3(260);
                v_user_pk := get.uid;
                SELECT  name, organization_fk
                INTO    v_name, v_organization_pk
                FROM    fora_type
                WHERE   fora_type_pk = p_fora_type_pk;

                -- Legger inn i statistikk tabellen
                IF ( v_organization_pk > 0 ) THEN
                        stat.reg(v_organization_pk,NULL,NULL,p_fora_type_pk,NULL,NULL,20);
                ELSE
                        stat.reg(NULL,NULL,NULL,p_fora_type_pk,NULL,NULL,20);
                END IF;

                html.b_box( v_name ,'100%', 'fora_util.in_fora');
                html.b_table;
                IF ( v_user_pk IS NOT NULL ) THEN
                        OPEN get_fora_read;
                        LOOP
                                FETCH get_fora_read INTO v_fora_pk;
                                exit when get_fora_read%NOTFOUND;
                                v_fora_read(v_fora_pk) := 1;
                        END LOOP;
                        CLOSE get_fora_read;
                END IF;
                OPEN get_sub_fora;
                LOOP
                        FETCH get_sub_fora INTO v_fora_pk, v_parent_fora_pk, v_level_no;
                        exit when get_sub_fora%NOTFOUND;
                        IF ( v_fora_pk = v_parent_fora_pk ) THEN
                                IF ( v_fora_html.EXISTS(v_parent_fora_pk) ) THEN
                                        v_fora_html(v_fora_pk) := v_fora_pk||';'||v_fora_html(v_fora_pk);
                                ELSE
                                        v_fora_html(v_fora_pk) := v_fora_pk||';';
                                END IF;
                        ELSIF ( v_fora_html.EXISTS(v_parent_fora_pk) ) THEN
                                IF ( v_fora_html.EXISTS(v_fora_pk) ) THEN
                                        v_fora_html(v_parent_fora_pk) := v_fora_pk||';'||v_fora_html(v_fora_pk)||''||v_fora_html(v_parent_fora_pk);
                                ELSE
                                        v_fora_html(v_parent_fora_pk) := v_fora_pk||';'||v_fora_html(v_parent_fora_pk);
                                END IF;
                        ELSE
                                IF ( v_fora_html.EXISTS(v_fora_pk) ) THEN
                                        v_fora_html(v_parent_fora_pk) := v_fora_pk||';'||v_fora_html(v_fora_pk);
                                ELSE
                                        v_fora_html(v_parent_fora_pk) := v_fora_pk||';';
                                END IF;
                        END IF;
--                      Til testing
--                      htp.p(' 1:'||v_parent_fora_pk||' 2:'||v_fora_html(v_parent_fora_pk)||'<br>');
                END LOOP;
                CLOSE get_sub_fora;
                OPEN get_fora;
                LOOP
                        FETCH get_fora INTO     v_fora_pk, v_parent_fora_pk, v_main_title, v_sub_title,
                                                v_published_date, v_fora_type_pk, v_name, v_user_nick;
                        exit when get_fora%NOTFOUND;
                        html.b_form('fora_util.edit_fora','form'|| v_fora_pk, 'Message');
                        htp.p('<input type="hidden" name="p_command" value="show">
                        <input type="hidden" name="p_fora_pk" value="'||v_fora_pk||'">
                        <input type="hidden" name="p_name" value="'||v_name||'">
                        <input type="hidden" name="p_fora_type_pk" value="'||v_fora_type_pk||'">
                        <tr><td nowrap colspan="2">');
                        IF ( v_fora_pk <> v_parent_fora_pk ) THEN
                                htp.p('<img src="/img/vinkel.gif" alt="">
                                <input type="hidden" name="p_parent_fora_pk" value="'||v_parent_fora_pk||'">');
                        END IF;
                        IF ( v_fora_read.EXISTS(v_fora_pk) ) THEN
                                v_color := 'green';
                        ELSE
                                v_color := 'red';
                        END IF;
                        html.submit_link('<font color="'||v_color||'">
                        '|| v_sub_title ||'</font>','form'||v_fora_pk,
                        'submit_link2');
                        htp.p('('||to_char(v_published_date,get.txt('date_short'))||')</td></tr>');
                        html.e_form;
--                      Til testing
--                      htp.p(v_fora_html(v_fora_pk)||'<br>');
                        WHILE ( instr(v_fora_html(v_fora_pk),';',1) >= 1 )
                        loop
                                v_count := instr(v_fora_html(v_fora_pk),';',1);
                                v_parent_fora_pk := substr(v_fora_html(v_fora_pk),1,v_count-1);
                                v_fora_html(v_fora_pk) := substr(v_fora_html(v_fora_pk),v_count+1,length(v_fora_html(v_fora_pk)));
                                IF ( v_parent_fora_pk <> v_fora_pk ) THEN
                                        sub_fora(v_parent_fora_pk,v_fora_read.EXISTS(v_parent_fora_pk));
                                END IF;
                        end loop;
                END LOOP;
                CLOSE get_fora;
                html.b_form('fora_util.edit_fora','form_new','Message');
                htp.p('<input type="hidden" name="p_command" value="new">
                <input type="hidden" name="p_fora_type_pk" value="'||p_fora_type_pk||'">
                <input type="hidden" name="p_name" value="'||v_name||'">
                <tr><td width="20">'); html.submit_link( get.txt('new_posting'), 'form_new'); htp.p('</td>');
                html.e_form;
                IF ( v_organization_pk > 0 ) THEN
                        html.b_form('fora_util.all_fora','change_fora');
                        htp.p('<input type="hidden" name="p_organization_pk" value="'||v_organization_pk||'">
                        <input type="hidden" name="p_command" value="new-page">
                        <input type="hidden" name="p_service_pk" value="'||get.serv||'">
                        <input type="hidden" name="p_language_pk" value="'||get.lan||'">
                        <td align="left">'); html.submit_link( get.txt('change_fora'), 'change_fora'); htp.p('</td></tr>');
                        html.e_form;
                ELSE
                        html.b_form('fora_util.all_fora','change_fora');
                        htp.p('<input type="hidden" name="p_service_pk" value="'||get.serv||'">
                        <input type="hidden" name="p_language_pk" value="'||get.lan||'">
                        <input type="hidden" name="p_command" value="new-page">
                        <td align="left">'); html.submit_link( get.txt('change_fora'), 'change_fora'); htp.p('</td></tr>');
                        html.e_form;
                END IF;
                html.e_table;
                html.e_box;
                html.e_page_2;
        END IF;
--END IF;
EXCEPTION
WHEN NO_DATA_FOUND THEN
htp.p('No data found! edit_fora.sql -> edit_fora');
WHEN DUP_VAL_ON_INDEX THEN
htp.p('&nbsp;');
END;
END edit_fora;

---------------------------------------------------------------------
-- Name: sub_fora
-- Type: procedure
-- What: Genererer liste over fora
-- Made: Espen Messel
-- Date: 20.07.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE sub_fora(             p_fora_pk               IN fora.fora_pk%TYPE                    DEFAULT NULL,
                                p_color                 IN BOOLEAN                              DEFAULT FALSE
                )
IS
BEGIN
DECLARE

v_fora_pk               fora.fora_pk%TYPE               DEFAULT NULL;
v_fora_type_pk          fora.fora_type_fk%TYPE          DEFAULT NULL;
v_parent_fora_pk        fora.parent_fora_fk%TYPE        DEFAULT NULL;
v_user_pk               fora.user_fk%TYPE               DEFAULT NULL;
v_published_date        fora.published_date%TYPE        DEFAULT NULL;
v_main_title            fora.main_title%TYPE            DEFAULT NULL;
v_sub_title             fora.sub_title%TYPE             DEFAULT NULL;
v_ingress               fora.ingress%TYPE               DEFAULT NULL;
v_body                  fora.body%TYPE                  DEFAULT NULL;
v_level_no              fora.level_no%TYPE              DEFAULT NULL;
v_color                 varchar2(30)                    DEFAULT NULL;
v_name                  fora_type.name%TYPE             DEFAULT NULL;

BEGIN

SELECT  fora_pk, parent_fora_fk, main_title, sub_title,
        published_date, fora_type_fk, level_no
INTO    v_fora_pk, v_parent_fora_pk, v_main_title, v_sub_title,
        v_published_date, v_fora_type_pk, v_level_no
FROM    fora
WHERE   fora_pk = p_fora_pk
;
        html.b_form('fora_util.edit_fora','form'|| v_fora_pk, 'Message');
        htp.p('<input type="hidden" name="p_command" value="show">
        <input type="hidden" name="p_fora_pk" value="'||v_fora_pk||'">
        <input type="hidden" name="p_name" value="'||v_name||'">
        <input type="hidden" name="p_fora_type_pk" value="'||v_fora_type_pk||'">
        <tr><td nowrap colspan="2">');
        v_level_no := (v_level_no-2)*15;
        htp.p('<img src="/img/space.gif" alt="" height="1" width="'||v_level_no||'"
        ><img src="/img/vinkel.gif" alt="">
        <input type="hidden" name="p_parent_fora_pk" value="'||v_parent_fora_pk||'">');
        IF ( p_color ) THEN
                v_color := 'green';
        ELSE
                v_color := 'red';
        END IF;
        IF ( v_sub_title IS NULL ) THEN
           v_sub_title := get.txt('no_title');
        END IF;
        html.submit_link('<font color="'||v_color||'">
        '|| v_sub_title ||'</font>','form'||v_fora_pk,
        'submit_link2');
        htp.p('('||to_char(v_published_date,get.txt('date_short'))||')</tr>');
        html.e_form;
END;
END sub_fora;

---------------------------------------------------------------------
-- Name: all_fora
-- Type: procedure
-- What: Genererer liste over fora
-- Made: Espen Messel
-- Date: 20.07.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE all_fora(             p_service_pk            IN fora_type.service_fk%TYPE            DEFAULT NULL,
                                p_language_pk           IN fora_type.language_fk%TYPE           DEFAULT NULL,
                                p_organization_pk       IN fora_type.organization_fk%TYPE       DEFAULT NULL,
                                p_command               IN varchar2                             DEFAULT NULL
                )
IS
BEGIN
DECLARE

CURSOR  get_all_fora IS
SELECT  name, fora_type_pk
FROM    fora_type
WHERE   language_fk = p_language_pk
AND     organization_fk = p_organization_pk
AND     activated > 0
ORDER BY name
;

CURSOR  get_all_fora2 IS
SELECT  name, fora_type_pk
FROM    fora_type
WHERE   language_fk = p_language_pk
AND     service_fk = p_service_pk
AND     organization_fk IS NULL
AND     activated > 0
ORDER BY name
;

v_name                  fora_type.name%TYPE                     DEFAULT NULL;
v_fora_type_pk          fora_type.fora_type_pk%TYPE             DEFAULT NULL;
v_count                 number                                  DEFAULT NULL;
v_count2                number                                  DEFAULT NULL;
v_user_pk               usr.user_pk%TYPE                        DEFAULT NULL;
v_path                  VARCHAR2(100)                           DEFAULT NULL;

BEGIN

v_path := owa_util.get_cgi_env('PATH_INFO');

IF ( p_service_pk IS NULL AND p_language_pk IS NULL ) THEN
        html.b_page_2;
        html.b_box('Select service and language');
        html.b_table;
        html.b_form('fora_util.all_fora');
        html.select_service;
        htp.p('</td><td>');
        html.select_lang;
        htp.p('</td><td>');
        html.submit_link( get.txt('Show_fora') );
        htp.p('</td></tr>');
        html.e_form;
        html.e_table;
        html.e_box;
        html.e_page_2;
ELSE
        IF ( p_command = 'new-page' ) THEN
                html.b_page_3(200);
                html.b_box(get.txt('Show_fora'),'100%','fora_util.all_fora');
        ELSE
                html.b_box_2(get.txt('Show_fora'),'100%','fora_util.all_fora');
        END IF;
        html.b_table;
        IF ( p_organization_pk > 0 ) THEN
                OPEN get_all_fora;
                LOOP
                        FETCH get_all_fora INTO v_name, v_fora_type_pk;
                        exit when get_all_fora%NOTFOUND;
                        SELECT  count (*)
                        INTO    v_count
                        FROM    fora
                        WHERE   fora_type_fk=v_fora_type_pk;
                        v_user_pk := get.uid;
                        IF ( v_user_pk IS NOT NULL ) THEN
                                SELECT  count(DISTINCT(f.fora_pk))
                                INTO    v_count2
                                FROM    fora f, fora_read fr
                                WHERE   f.fora_type_fk=v_fora_type_pk
                                AND     f.fora_pk=fr.fora_fk
                                AND     fr.user_fk = v_user_pk;
                        END IF;
                        IF ( p_command = 'new-page' ) THEN
                                html.b_form('fora_util.edit_fora','form'||v_fora_type_pk);
                        ELSE
                                html.b_form('fora_util.frames','form'||v_fora_type_pk);
                        END IF;
                        htp.p('<input type="hidden" name="p_fora_type_pk" value="'||v_fora_type_pk||'">
                        <input type="hidden" name="p_name" value="'||v_name||'">
                        <tr><td>');
                        html.submit_link( v_name,'form'||v_fora_type_pk , 'submit_link2');
                        IF ( v_count2 IS NOT NULL AND v_path <> '/main_page.startup' ) THEN
                                v_count2 := v_count - v_count2;
                                htp.p('&nbsp;&nbsp;('||get.txt('total_fora')||': '||v_count||','||get.txt('new_fora')||': '||v_count2||')</td></tr>');
                        ELSE
                                htp.p('&nbsp;&nbsp;('||v_count||')</td></tr>');
                        END IF;
                        html.e_form;
                END LOOP;
                CLOSE get_all_fora;
        ELSE
                OPEN get_all_fora2;
                LOOP
                        FETCH get_all_fora2 INTO v_name, v_fora_type_pk;
                        exit when get_all_fora2%NOTFOUND;
                        SELECT  count (*)
                        INTO    v_count
                        FROM    fora
                        WHERE   fora_type_fk=v_fora_type_pk;
                        v_user_pk := get.uid;
                        IF ( v_user_pk IS NOT NULL ) THEN
                                SELECT  count(DISTINCT(f.fora_pk))
                                INTO    v_count2
                                FROM    fora f, fora_read fr
                                WHERE   f.fora_type_fk=v_fora_type_pk
                                AND     f.fora_pk=fr.fora_fk
                                AND     fr.user_fk = v_user_pk;
                        END IF;
                        IF ( p_command = 'new-page' ) THEN
                                html.b_form('fora_util.edit_fora','form'||v_fora_type_pk);
                        ELSE
                                html.b_form('fora_util.frames','form'||v_fora_type_pk);
                        END IF;
                        htp.p('<input type="hidden" name="p_fora_type_pk" value="'||v_fora_type_pk||'">
                        <input type="hidden" name="p_name" value="'||v_name||'">
                        <tr><td>');
                        html.submit_link( v_name ,'form'||v_fora_type_pk , 'submit_link2');
                        IF ( v_count2 IS NOT NULL AND v_path <> '/main_page.startup' ) THEN
                                v_count2 := v_count - v_count2;
                                htp.p('&nbsp;&nbsp;('||get.txt('total_fora')||': '||v_count||','||get.txt('new_fora')||': '||v_count2||')</td></tr>');
                        ELSE
                                htp.p('&nbsp;&nbsp;('||v_count||')</td></tr>');
                        END IF;
                        html.e_form;
                END LOOP;
                CLOSE get_all_fora2;
        END IF;
        html.e_table;
        IF ( p_command = 'new-page' ) THEN
                html.e_page_2;
                html.e_box;
        ELSE
                html.e_box_2;
        END IF;
END IF;
EXCEPTION
WHEN NO_DATA_FOUND THEN
htp.p('No data found! all_fora.sql -> all_fora');
WHEN DUP_VAL_ON_INDEX THEN
htp.p('');
END;
END all_fora;

---------------------------------------------------------------------
-- Name: frames
-- Type: procedure
-- What: Lager frames oppsett
-- Made: Espen Messel
-- Date: 20.07.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE frames (              p_fora_type_pk          IN fora_type.fora_type_pk%TYPE          DEFAULT NULL,
                                p_name                  IN fora_type.name%TYPE                  DEFAULT NULL,
                                p_organization_pk       IN organization.organization_pk%TYPE    DEFAULT NULL,
                                p_width                 IN NUMBER                               DEFAULT NULL,
                                p_fora_pk               IN fora.fora_pk%TYPE                    DEFAULT NULL
                )
IS
BEGIN
DECLARE
v_organization_pk       organization.organization_pk%TYPE       DEFAULT NULL;
v_height                NUMBER                                  DEFAULT NULL;

CURSOR org_sel IS
SELECT  organization_fk
FROM    fora_type
WHERE   fora_type_pk = p_fora_type_pk;

BEGIN

IF ( p_organization_pk IS NULL ) THEN
        open org_sel;
        fetch org_sel into v_organization_pk;
        close org_sel;
ELSE
        v_organization_pk := p_organization_pk;
        -- Statistikk
        stat.reg(v_organization_pk,NULL,NULL,p_fora_type_pk,NULL,NULL,20);
END IF;


/*
IF ( v_organization_pk IS NULL ) THEN
        v_height := 170;
ELSE
        v_height := 170;
END IF;
*/
v_height := get.value('frame_row_top_height');

htp.p('
<html><head>
<meta http-equiv="Content-Type" content="text/html">
<title>in4mant.com</title>

</head>

<frameset cols="*,750,*" border="0" frameborder="0">

<frame name="Left" src="fora_util.plain" scrolling="no">
<frameset rows="'||v_height||',*,35" border=0 frameborder="0">
    <frame name="Navigation" src="fora_util.print_top?p_organization_pk='||v_organization_pk||'" marginwidth="0" marginheight="3" scrolling="no" frameborder="no" border=0>
    <frameset cols="300,*">
    <frame name="Forum"');
IF ( p_organization_pk IS NULL ) THEN
        htp.p('src="fora_util.edit_fora?p_fora_type_pk='||p_fora_type_pk||'&p_name='|| p_name ||'"');
ELSE
        htp.p('src="fora_util.all_fora?p_service_pk='||get.serv||'&p_language_pk='||get.lan||'&p_organization_pk='||p_organization_pk||'&p_command=new-page"');
END IF;
        htp.p('marginwidth="3" marginheight="3" scrolling="auto" frameborder="no" border="0" scrolling="auto">');
IF ( p_fora_pk IS NULL ) THEN
        htp.p('<frame name="Message" src="fora_util.plain?p_bgcolor='||REPLACE(get.value('c_'|| get.serv_name ||'_bbs' ),'#','')||'" scrolling="auto">');
ELSE
        htp.p('<frame name="Message" src="fora_util.edit_fora?p_fora_pk='||p_fora_pk||'&p_command=show" scrolling="auto">');
END IF;
htp.p('</frameset>
<frame name="Footer" src="html.bottom" scrolling="no">
</frameset><frame name="Right" src="fora_util.plain" scrolling="no">
</frameset></html>');
END;
END frames;
--html.b_page_3;html.e_page_2
---------------------------------------------------------------------
-- Name: plain
-- Type: procedure
-- What: Lager en plain side
-- Made: Espen Messel
-- Date: 20.07.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE plain (               p_fora_type_pk          IN fora_type.fora_type_pk%TYPE          DEFAULT NULL,
                                p_name                  IN fora_type.name%TYPE                  DEFAULT NULL,
                                p_bgcolor               IN VARCHAR2                             DEFAULT NULL
                )
IS
BEGIN
DECLARE
v_code  varchar2(4000)          default NULL;
v_color VARCHAR2(500)           DEFAULT NULL;

BEGIN
v_code := html.getcode('b_page_2');
v_code := REPLACE (v_code, '[width]', '100%' );
v_code := REPLACE (v_code, '[reload]', '');
v_code := REPLACE (v_code, '[jump]', '');
v_code := REPLACE (v_code, '[css]', html.getcode('css') );
v_code := REPLACE (v_code, '[javascript]', html.getcode('javascript') );

IF ( p_bgcolor IS NULL ) THEN
        v_color := get.value('c_'|| get.serv_name ||'_bbm' );
ELSE
        v_color := '#' || p_bgcolor;
END IF;

v_code := REPLACE (v_code, '#ffffff' , v_color );
v_code := REPLACE (v_code, '[c_service_bbm]' , v_color );
v_code := REPLACE (v_code, '[c_service_bbs]' , v_color );

IF ( p_fora_type_pk IS NOT NULL ) THEN
        v_code := v_code ||'<SCRIPT language="JavaScript">refresh(''Forum'', ''fora_util.edit_fora?p_fora_type_pk='||p_fora_type_pk||'&p_name='||p_name||''');</SCRIPT>';
END IF;
htp.p(v_code);
IF ( p_bgcolor IS NOT NULL ) THEN
        html.b_box(get.txt('fora_start'),'100%','fora.start');
        html.b_table;
        htp.p('<tr><td>'||get.txt('fora_info')||'</td></tr>');
        html.e_table;
        html.e_box;
END IF;
html.e_page_2;
END;
END plain;

---------------------------------------------------------------------
-- Name: print_top
-- Type: procedure
-- What: Lager en topp rammen
-- Made: Espen Messel
-- Date: 20.08.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE print_top (   p_organization_pk       IN organization.organization_pk%TYPE    DEFAULT NULL,
                        p_b_page                IN NUMBER                               DEFAULT NULL)
IS
BEGIN
IF ( p_organization_pk IS NOT NULL ) THEN
   html.b_page(NULL, NULL, NULL, NULL, 3);
   html.org_menu(p_organization_pk);
ELSIF ( p_b_page IS NOT NULL ) THEN
   html.b_page(NULL, NULL, NULL, NULL, p_b_page );
   html.empty_menu;
ELSE
   html.b_page(NULL, NULL, NULL, NULL, 1);
   html.empty_menu;
END IF;
htp.p('<tr><td colspan="2">&nbsp;</td></tr>
<tr><td colspan="2">&nbsp;</td></tr>
<tr><td colspan="2">&nbsp;</td></tr>
<tr><td colspan="2">&nbsp;</td></tr>
<tr><td colspan="2">&nbsp;</td></tr></td></tr>
</TABLE>
</BODY>
</html>');
--html.e_page_2;
END print_top;

---------------------------------------------------------------------
---------------------------------------------------------------------
END;
/
show errors;
