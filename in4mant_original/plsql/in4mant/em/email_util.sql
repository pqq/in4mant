set define off
PROMPT *** package: email_util ***

CREATE OR REPLACE PACKAGE email_util IS
PROCEDURE startup;
PROCEDURE edit_et_type (        p_command               IN varchar2                     DEFAULT NULL,
                                p_name                  IN et_type.name%TYPE            DEFAULT NULL,
                                p_description           IN et_type.description%TYPE     DEFAULT NULL,
                                p_et_type_pk            IN et_type.et_type_pk%TYPE      DEFAULT NULL
                         );
PROCEDURE select_et_type (      p_et_type_pk            IN et_type.et_type_pk%TYPE      DEFAULT NULL);
PROCEDURE edit_email_text (     p_command               IN varchar2                             DEFAULT NULL,
                                p_name                  IN string_group.name%TYPE               DEFAULT NULL,
                                p_description           IN email_text.description%TYPE          DEFAULT NULL,
                                p_string_group_pk       IN string_group.string_group_pk%TYPE    DEFAULT NULL,
                                p_et_type_pk            IN email_text.et_type_fk%TYPE           DEFAULT NULL,
                                p_volume                IN email_text.volume%TYPE               DEFAULT NULL,
                                p_start_date            IN VARCHAR2                             DEFAULT NULL,
                                p_end_date              IN VARCHAR2                             DEFAULT NULL
                         );
PROCEDURE edit_et_relation (    p_command               IN varchar2                             DEFAULT NULL,
                                p_et_relation_pk        IN et_relation.et_relation_pk%TYPE      DEFAULT NULL,
                                p_email_text_pk         IN et_relation.email_text_fk%TYPE       DEFAULT NULL,
                                p_service_pk            IN et_relation.service_fk%TYPE          DEFAULT NULL,
                                p_geography_pk          IN et_relation.geography_fk%TYPE        DEFAULT NULL,
                                p_geo_name              IN VARCHAR2                             DEFAULT NULL,
                                p_list_pk               IN et_relation.list_fk%TYPE             DEFAULT NULL,
                                p_organization_pk       IN et_relation.organization_fk%TYPE     DEFAULT NULL,
                                p_name                  IN VARCHAR2                             DEFAULT NULL
                         );
PROCEDURE select_organization (     p_organization_pk      IN organization.organization_pk%TYPE  DEFAULT NULL);
PROCEDURE select_email_text (     p_email_text_pk      IN email_text.email_text_pk%TYPE  DEFAULT NULL);

END;
/
CREATE OR REPLACE PACKAGE BODY email_util IS
---------------------------------------------------------------------
---------------------------------------------------------------------
-- Name: startup
-- Type: procedure
-- What: Genererer opp hovedsiden for admin av email
-- Made: Espen Messel
-- Date: 05.11.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE startup
IS
BEGIN
        edit_et_type;
END startup;


---------------------------------------------------------------------
-- Name: edit_et_type
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 29.06.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE edit_et_type (  p_command             IN varchar2                     DEFAULT NULL,
                          p_name                IN et_type.name%TYPE            DEFAULT NULL,
                          p_description         IN et_type.description%TYPE     DEFAULT NULL,
                          p_et_type_pk          IN et_type.et_type_pk%TYPE      DEFAULT NULL
                         )
IS
BEGIN
DECLARE

CURSOR  get_et_type IS
SELECT  et_type_pk, name, description
FROM    et_type
ORDER BY name
;

v_et_type_pk            et_type.et_type_pk%TYPE         DEFAULT NULL;
v_name                  et_type.name%TYPE               DEFAULT NULL;
v_description           et_type.description%TYPE        DEFAULT NULL;

BEGIN

IF (    login.timeout('email_util.edit_et_type') > 0 AND
        login.right('email_util') > 0 ) THEN

        IF ( p_command = 'update') THEN
                SELECT  et_type_pk, name, description
                INTO    v_et_type_pk, v_name, v_description
                FROM    et_type
                WHERE   et_type_pk=p_et_type_pk
                ;
                html.b_adm_page;
                html.b_box( get.txt('et_type') );
                html.b_table;
                html.b_form('email_util.edit_et_type');
                htp.p('<input type="hidden" name="p_command" value="edit">
                <input type="hidden" name="p_et_type_pk" value="'||v_et_type_pk||'">
                <tr><td>'|| get.txt('name') ||':</td><td>
                '|| html.text('p_name', '50', '50', v_name ) ||'
                </td></tr>
                <tr><td>'|| get.txt('description') ||':(maks 150 tegn)</td><td>
                <textarea name="p_description" rows="3" cols="70" wrap="virtual">'||v_description||'</textarea>
                </td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('update') ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        ELSIF ( p_command = 'edit') THEN
                UPDATE  et_type
                SET     name=SUBSTR(p_name,1,50),
                        description=SUBSTR(p_description,1,150)
                WHERE   et_type_pk=p_et_type_pk;
                COMMIT;
                html.jump_to ( 'email_util.edit_et_type');
        ELSIF ( p_command = 'delete') THEN
                IF ( p_name = 'yes') THEN
                        DELETE  FROM et_type
                        WHERE   et_type_pk=p_et_type_pk;
                        COMMIT;
                        html.jump_to ( 'email_util.edit_et_type');
                ELSIF ( p_name IS NULL ) THEN
                        html.jump_to ( 'email_util.edit_et_type');
                ELSE
                        html.b_adm_page;
                        html.b_box( get.txt('et_type') );
                        html.b_table;
                        html.b_form('email_util.edit_et_type');
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="yes">
                        <input type="hidden" name="p_et_type_pk" value="'||p_et_type_pk||'">
                        <td>'|| get.txt('delete_info') ||' "'||p_name||'" ?'); html.submit_link( get.txt('YES') ); htp.p('</td>');
                        html.e_form;
                        htp.p('<td>'); html.button_link( get.txt('NO'), 'email_util.edit_et_type');htp.p('</td></tr>');
                        html.e_table;
                        html.e_box;
                        html.e_page_2;
                END IF;
        ELSIF ( p_command = 'new') THEN
                html.b_adm_page;
                html.b_box( get.txt('et_type') );
                html.b_table;
                html.b_form('email_util.edit_et_type','form_insert');
                htp.p('<input type="hidden" name="p_command" value="insert">
                <tr><td>'|| get.txt('name') ||':</td><td>
                '|| html.text('p_name', '50', '50', '' ) ||'
                </td></tr>
                <tr><td>'|| get.txt('description') ||':(maks 150 tegn)</td><td>
                <textarea name="p_description" rows="3" cols="70" wrap="virtual"></textarea>
                </td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('insert'), 'form_insert' ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        ELSIF ( p_command = 'insert') THEN
                SELECT et_type_seq.NEXTVAL
                INTO v_et_type_pk
                FROM dual;
                INSERT INTO et_type
                ( et_type_pk, name, description )
                VALUES
                ( v_et_type_pk, SUBSTR(p_name,1,50),
                SUBSTR(p_description,1,150) );
                COMMIT;
                html.jump_to ( 'email_util.edit_et_type' );
        ELSE
                html.b_adm_page;
                html.b_box( get.txt('et_type') );
                html.b_table;
                OPEN get_et_type;
                LOOP
                        FETCH get_et_type INTO v_et_type_pk, v_name, v_description;
                        exit when get_et_type%NOTFOUND;
                        html.b_form('email_util.edit_et_type','form'|| v_et_type_pk);
                        htp.p('<input type="hidden" name="p_command" value="update">
                        <input type="hidden" name="p_et_type_pk" value="'||v_et_type_pk||'">
                        <tr><td>'|| get.txt('et_type') ||':</td><td>'||v_name||'</td>
                        <td>'); html.submit_link( get.txt('edit'), 'form'||v_et_type_pk ); htp.p('</td>');
                        html.e_form;
                        html.b_form('email_util.edit_et_type','delete'|| v_et_type_pk);
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="'||v_name||'">
                        <input type="hidden" name="p_et_type_pk" value="'||v_et_type_pk||'">
                        <td>'); html.submit_link( get.txt('delete'), 'delete'||v_et_type_pk ); htp.p('</td></tr>');
                        html.e_form;
                END LOOP;
                close get_et_type;
                html.b_form('email_util.edit_et_type','form_new');
                htp.p('<input type="hidden" name="p_command" value="new">
                <td>'); html.submit_link( get.txt('new'), 'form_new'); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        END IF;
END IF;
EXCEPTION
WHEN NO_DATA_FOUND
THEN
htp.p('No data found! email_util.sql -> edit_et_type');
END;
END edit_et_type;

---------------------------------------------------------------------
-- Name: select_et_type
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE select_et_type (     p_et_type_pk      IN et_type.et_type_pk%TYPE  DEFAULT NULL)
IS
BEGIN
DECLARE

CURSOR  all_et_types IS
SELECT  et_type_pk, name
FROM    et_type
ORDER BY UPPER ( name )
;

v_et_type_pk      et_type.et_type_pk%TYPE             DEFAULT NULL;
v_name            et_type.name%TYPE                   DEFAULT NULL;
v_value           VARCHAR2(10)                        DEFAULT NULL;

BEGIN

        htp.p('<select name="p_et_type_pk">');
        OPEN all_et_types;
        LOOP
                FETCH all_et_types INTO v_et_type_pk, v_name;
                EXIT WHEN all_et_types%NOTFOUND;

                IF ( v_et_type_pk = p_et_type_pk ) THEN
                        v_value := ' selected';
                ELSE
                        v_value := '';
                END IF;

                htp.p('<option value="'|| v_et_type_pk ||'"'||v_value||'>'|| v_name ||'</option>');
        END LOOP;
        close all_et_types;
        htp.p('</select>');
END;
END select_et_type;

---------------------------------------------------------------------
-- Name: edit_email_text
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 29.06.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE edit_email_text (     p_command               IN varchar2                             DEFAULT NULL,
                                p_name                  IN string_group.name%TYPE               DEFAULT NULL,
                                p_description           IN email_text.description%TYPE          DEFAULT NULL,
                                p_string_group_pk       IN string_group.string_group_pk%TYPE    DEFAULT NULL,
                                p_et_type_pk            IN email_text.et_type_fk%TYPE           DEFAULT NULL,
                                p_volume                IN email_text.volume%TYPE               DEFAULT NULL,
                                p_start_date            IN VARCHAR2                             DEFAULT NULL,
                                p_end_date              IN VARCHAR2                             DEFAULT NULL

                         )
IS
BEGIN
DECLARE

CURSOR  get_email_text IS
SELECT  sg.string_group_pk, sg.name, sg.description
FROM    email_text et, string_group sg
WHERE   et.name_sg_fk=sg.string_group_pk
ORDER BY sg.name
;

v_email_text_pk         email_text.email_text_pk%TYPE           DEFAULT NULL;
v_string_group_pk       string_group.string_group_pk%TYPE       DEFAULT NULL;
v_name                  string_group.name%TYPE                  DEFAULT NULL;
v_description           string_group.description%TYPE           DEFAULT NULL;
v_et_type_pk            email_text.et_type_fk%TYPE              DEFAULT NULL;
v_volume                email_text.volume%TYPE                  DEFAULT NULL;
v_start_date            email_text.start_date%TYPE              DEFAULT NULL;
v_end_date              email_text.end_date%TYPE                DEFAULT NULL;

BEGIN

IF (    login.timeout('email_util.edit_email_text') > 0 AND
        login.right('email_util') > 0 ) THEN

        IF ( p_command = 'update') THEN
                SELECT  sg.string_group_pk, sg.name, sg.description, et.et_type_fk,
                        et.volume, et.start_date, et.end_date
                INTO    v_string_group_pk, v_name, v_description, v_et_type_pk,
                        v_volume, v_start_date, v_end_date
                FROM    string_group sg, email_text et
                WHERE   sg.string_group_pk=p_string_group_pk
                AND     sg.string_group_pk=et.name_sg_fk
                ;
/*                IF ( v_start_date IS NULL ) then
                   v_start_date := sysdate;
                END IF;
                IF ( v_end_date IS NULL ) then
                   v_end_date := sysdate + 30;
                END IF;
*/
                html.b_adm_page;
                html.b_box( get.txt('email_text') );
                html.b_table;
                html.b_form('email_util.edit_email_text');
                htp.p('<input type="hidden" name="p_command" value="edit">
                <input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                <tr><td>'|| get.txt('select_et_type') ||':</td><td>');
                select_et_type( v_et_type_pk );
                htp.p('</td></tr>
                <tr><td>'|| get.txt('name') ||':</td><td>
                '|| html.text('p_name', '50', '50', v_name ) ||'
                </td></tr>
                <tr><td>'|| get.txt('description') ||':(maks 200 tegn)</td><td>
                <textarea name="p_description" rows="3" cols="70" wrap="virtual">'||v_description||'</textarea>
                </td></tr>
                <tr><td>'|| get.txt('max_volume') ||':</td><td>
                '|| html.text('p_volume', '20', '20', v_volume ) ||'
                </td></tr>
                <tr><td>'|| get.txt('start_date') ||':</td><td>
                <input type="text" length="16" maxlength="16"
                name="p_start_date" value="'||TO_CHAR(v_start_date,get.txt('date_long'))||'"
                onSelect="javascript:openWin(''date_time.startup?p_variable=p_start_date&p_start_date='||TO_CHAR(v_start_date,get.txt('date_full'))||''',''300'',''200'')"
                onClick="javascript:openWin(''date_time.startup?p_variable=p_start_date&p_start_date='||TO_CHAR(v_start_date,get.txt('date_full'))||''',''300'',''200'')">
                '|| html.popup( get.txt('change_start_date'),'date_time.startup?p_variable=p_start_date&p_start_date='||TO_CHAR(v_start_date,get.txt('date_full'))||'','300', '200') ||'
                      &nbsp;&nbsp;&nbsp;&nbsp;<a href="Javascript: setblankstart()">'||get.txt('clear_date')||'</a>
                        <script language=JavaScript>
                        function setblankstart(){
                      document.form.p_start_date.value="";
                      }
                      </script>

                </td></tr>
                <tr><td>'|| get.txt('end_date') ||':</td><td>
                <input type="text" length="16" maxlength="16"
                name="p_end_date" value="'||TO_CHAR(v_end_date,get.txt('date_long'))||'"
                onSelect=javascript:openWin(''date_time.startup?p_variable=p_end_date&p_start_date='||TO_CHAR(v_end_date,get.txt('date_full'))||''',''300'',''200'')
                onClick=javascript:openWin(''date_time.startup?p_variable=p_end_date&p_start_date='||TO_CHAR(v_end_date,get.txt('date_full'))||''',''300'',''200'')>
                '|| html.popup( get.txt('change_end_date'),'date_time.startup?p_variable=p_end_date&p_start_date='||TO_CHAR(v_end_date,get.txt('date_full'))||'','300', '200') ||'
                      &nbsp;&nbsp;&nbsp;&nbsp;<a href="Javascript: setblankend()">'||get.txt('clear_date')||'</a>
                        <script language=JavaScript>
                        function setblankend(){
                      document.form.p_end_date.value="";
                      }
                      </script>
                </td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('update') ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        ELSIF ( p_command = 'edit') THEN
                UPDATE  string_group
                SET     name=SUBSTR(p_name,1,50),
                        description=SUBSTR(p_description,1,200)
                WHERE   string_group_pk=p_string_group_pk;
                COMMIT;
                UPDATE  email_text
                SET     et_type_fk = p_et_type_pk,
                        description=SUBSTR(p_description,1,150),
                        volume=p_volume,
                        start_date=to_date(p_start_date,get.txt('date_long')),
                        end_date=to_date(p_end_date,get.txt('date_long'))
                WHERE   name_sg_fk=p_string_group_pk;
                COMMIT;
                html.jump_to ( 'email_util.edit_email_text');
        ELSIF ( p_command = 'delete') THEN
                IF ( p_name = 'yes') THEN
                        DELETE  FROM string_group
                        WHERE   string_group_pk=p_string_group_pk;
                        COMMIT;
                        html.jump_to ( 'email_util.edit_email_text');
                ELSIF ( p_name IS NULL ) THEN
                        html.jump_to ( 'email_util.edit_email_text');
                ELSE
                        html.b_adm_page;
                        html.b_box( get.txt('email_text') );
                        html.b_table;
                        html.b_form('email_util.edit_email_text');
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="yes">
                        <input type="hidden" name="p_string_group_pk" value="'||p_string_group_pk||'">
                        <td>'|| get.txt('delete_info') ||' "'||p_name||'" ?'); html.submit_link( get.txt('YES') ); htp.p('</td>');
                        html.e_form;
                        htp.p('<td>'); html.button_link( get.txt('NO'), 'email_util.edit_email_text');htp.p('</td></tr>');
                        html.e_table;
                        html.e_box;
                        html.e_page_2;
                END IF;
        ELSIF ( p_command = 'new') THEN
                html.b_adm_page;
                html.b_box( get.txt('email_text') );
                html.b_table;
                html.b_form('email_util.edit_email_text');
                htp.p('<input type="hidden" name="p_command" value="insert">
                <tr><td>'|| get.txt('select_et_type') ||':</td><td>');
                select_et_type;
                htp.p('</td></tr>
                <tr><td>'|| get.txt('name') ||':</td><td>
                '|| html.text('p_name', '50', '50', '' ) ||'
                </td></tr>
                <tr><td>'|| get.txt('description') ||':(maks 200 tegn)</td><td>
                <textarea name="p_description" rows="3" cols="70" wrap="virtual"></textarea>
                </td></tr>
                <tr><td>'|| get.txt('max_volume') ||':</td><td>
                '|| html.text('p_volume', '20', '20', v_volume ) ||'
                </td></tr>
                <tr><td>'|| get.txt('start_date') ||':</td><td>
                <input type="text" length="16" maxlength="16"
                name="p_start_date" value=""
                onSelect="javascript:openWin(''date_time.startup?p_variable=p_start_date'',''300'',''200'')"
                onClick="javascript:openWin(''date_time.startup?p_variable=p_start_date'',''300'',''200'')">
                '|| html.popup( get.txt('change_start_date'),'date_time.startup?p_variable=p_start_date','300', '200') ||'
                      &nbsp;&nbsp;&nbsp;&nbsp;<a href="Javascript: setblankstart()">'||get.txt('clear_date')||'</a>
                        <script language=JavaScript>
                        function setblankstart(){
                      document.form.p_start_date.value="";
                      }
                      </script>

                </td></tr>
                <tr><td>'|| get.txt('end_date') ||':</td><td>
                <input type="text" length="16" maxlength="16"
                name="p_end_date" value=""
                onSelect=javascript:openWin(''date_time.startup?p_variable=p_end_date'',''300'',''200'')
                onClick=javascript:openWin(''date_time.startup?p_variable=p_end_date'',''300'',''200'')>
                '|| html.popup( get.txt('change_end_date'),'date_time.startup?p_variable=p_end_date','300', '200') ||'
                      &nbsp;&nbsp;&nbsp;&nbsp;<a href="Javascript: setblankend()">'||get.txt('clear_date')||'</a>
                        <script language=JavaScript>
                        function setblankend(){
                      document.form.p_end_date.value="";
                      }
                      </script>
                </td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('insert'), 'form' ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        ELSIF ( p_command = 'insert') THEN
                SELECT string_group_seq.NEXTVAL
                INTO v_string_group_pk
                FROM dual;
                SELECT email_text_seq.NEXTVAL
                INTO v_email_text_pk
                FROM dual;
                INSERT INTO string_group
                ( string_group_pk, name, description )
                VALUES
                ( v_string_group_pk, SUBSTR(p_name,1,50),
                SUBSTR(p_description,1,200) );
                COMMIT;
                INSERT INTO email_text
                ( email_text_pk, name_sg_fk, description, volume, start_date, end_date )
                VALUES
                ( v_email_text_pk, v_string_group_pk,
                SUBSTR(p_description,1,150), p_volume,
                to_date(p_start_date,get.txt('date_long')),
                to_date(p_end_date,get.txt('date_long')));
                COMMIT;
                html.jump_to ( 'email_util.edit_email_text' );
        ELSE
                html.b_adm_page;
                html.b_box( get.txt('email_text') );
                html.b_table;
                OPEN get_email_text;
                LOOP
                        FETCH get_email_text INTO v_string_group_pk, v_name, v_description;
                        exit when get_email_text%NOTFOUND;
                        html.b_form('email_util.edit_email_text','form'|| v_email_text_pk);
                        htp.p('<input type="hidden" name="p_command" value="update">
                        <input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                        <tr><td>'|| get.txt('email_text') ||':</td><td>'||v_name||'</td>
                        <td>'); html.submit_link( get.txt('edit'), 'form'||v_string_group_pk ); htp.p('</td>');
                        html.e_form;
                        html.b_form('email_util.edit_email_text','delete'|| v_string_group_pk);
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="'||v_name||'">
                        <input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                        <td>'); html.submit_link( get.txt('delete'), 'delete'||v_string_group_pk ); htp.p('</td></tr>');
                        html.e_form;
                END LOOP;
                close get_email_text;
                html.b_form('email_util.edit_email_text','form_new');
                htp.p('<input type="hidden" name="p_command" value="new">
                <td>'); html.submit_link( get.txt('new'), 'form_new'); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        END IF;
END IF;
EXCEPTION
WHEN NO_DATA_FOUND
THEN
htp.p('No data found! email_util.sql -> edit_email_text');
END;
END edit_email_text;

---------------------------------------------------------------------
-- Name: edit_et_relation
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 29.06.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE edit_et_relation (    p_command               IN varchar2                             DEFAULT NULL,
                                p_et_relation_pk        IN et_relation.et_relation_pk%TYPE      DEFAULT NULL,
                                p_email_text_pk         IN et_relation.email_text_fk%TYPE       DEFAULT NULL,
                                p_service_pk            IN et_relation.service_fk%TYPE          DEFAULT NULL,
                                p_geography_pk          IN et_relation.geography_fk%TYPE        DEFAULT NULL,
                                p_geo_name              IN VARCHAR2                             DEFAULT NULL,
                                p_list_pk               IN et_relation.list_fk%TYPE             DEFAULT NULL,
                                p_organization_pk       IN et_relation.organization_fk%TYPE     DEFAULT NULL,
                                p_name                  IN VARCHAR2                             DEFAULT NULL
                         )
IS
BEGIN
DECLARE

CURSOR  get_et_relation IS
SELECT  et.et_relation_pk, sg.name, et.service_fk, et.geography_fk, et.list_fk, et.organization_fk
FROM    et_relation et, string_group sg, email_text e
WHERE   e.name_sg_fk=sg.string_group_pk
AND     e.email_text_pk = et.email_text_fk
ORDER BY sg.name
;

v_et_relation_pk        et_relation.et_relation_pk%TYPE         DEFAULT NULL;
v_email_text_pk         et_relation.email_text_fk%TYPE          DEFAULT NULL;
v_service_pk            et_relation.service_fk%TYPE             DEFAULT NULL;
v_geography_pk          et_relation.geography_fk%TYPE           DEFAULT NULL;
v_list_pk               et_relation.list_fk%TYPE                DEFAULT NULL;
v_organization_pk       et_relation.organization_fk%TYPE        DEFAULT NULL;
v_name                  string_group.name%TYPE                  DEFAULT NULL;

BEGIN

IF (    login.timeout('email_util.edit_et_relation') > 0 AND
        login.right('email_util') > 0 ) THEN

        IF ( p_command = 'update') THEN
                SELECT  et_relation_pk, email_text_fk, service_fk,
                        geography_fk, list_fk, organization_fk
                INTO    v_et_relation_pk, v_email_text_pk, v_service_pk,
                        v_geography_pk, v_list_pk, v_organization_pk
                FROM    et_relation
                WHERE   et_relation_pk=p_et_relation_pk
                ;
                html.b_adm_page;
                html.b_box( get.txt('et_relation') );
                html.b_table;
                html.b_form('email_util.edit_et_relation');
                htp.p('<input type="hidden" name="p_command" value="edit">
                <input type="hidden" name="p_et_relation_pk" value="'||v_et_relation_pk||'">
                <tr><td>'|| get.txt('select_email_text') ||':</td><td>');
                select_email_text( v_email_text_pk );
                htp.p('</td></tr>
                <tr><td>'|| get.txt('select_service') ||':</td><td>');
                html.select_service( v_service_pk , 0);
                htp.p('</td></tr>
                <tr><td>'|| get.txt('geography_location') ||':</td>
                <td><input type="hidden" name="p_geography_pk" value="'|| v_geography_pk ||'">
                <input type="text" size="30" name="p_geo_name" value="'|| get.locn(v_geography_pk) ||'"
                onSelect=javascript:openWin(''select_geo.select_location?p_no_levels=4'',300,200)
                onClick=javascript:openWin(''select_geo.select_location?p_no_levels=4'',300,200)>
                '|| html.popup( get.txt('change_geo'), 'select_geo.select_location?p_no_levels=4', '300', '200') ||'
                      &nbsp;&nbsp;&nbsp;&nbsp;<a href="Javascript: setblankgeo()">'||get.txt('clear_geo')||'</a>
                        <script language=JavaScript>
                        function setblankgeo(){
                      document.form.p_geography_pk.value="";
                      document.form.p_geo_name.value="";
                      }
                      </script>
                </td></tr>
                <tr><td>'|| get.txt('select_organization') ||':</td><td>');
                select_organization( v_organization_pk );
                htp.p('</td></tr>
                <tr><td>'|| get.txt('list') ||':</td>
                <td><input type="hidden" name="p_list_pk" value="'|| v_list_pk ||'">
                <input type="text" size="30" name="p_name" value="'|| get.listn(v_list_pk) ||'"
                onSelect=javascript:openWin(''select_list.sel_type'',300,200)
                onClick=javascript:openWin(''select_list.sel_type'',300,200)>
                '|| html.popup( get.txt('change_list'), 'select_list.sel_type', '300', '200') ||'
                      &nbsp;&nbsp;&nbsp;&nbsp;<a href="Javascript: setblank()">'||get.txt('clear_list')||'</a>
                        <script language=JavaScript>
                        function setblank(){
                      document.form.p_list_pk.value="";
                      document.form.p_name.value="";
                      }
                      </script>

                </td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('update') ); htp.p('</td></tr>');
                --?p_service_fk='|| v_service_pk ||'
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        ELSIF ( p_command = 'edit') THEN
                UPDATE  et_relation
                SET     email_text_fk = p_email_text_pk,
                        service_fk = p_service_pk,
                        geography_fk = p_geography_pk,
                        list_fk = p_list_pk,
                        organization_fk = p_organization_pk
                WHERE   et_relation_pk = p_et_relation_pk;
                COMMIT;
                html.jump_to ( 'email_util.edit_et_relation');
        ELSIF ( p_command = 'delete') THEN
                IF ( p_name = 'yes') THEN
                        DELETE  FROM et_relation
                        WHERE   et_relation_pk=p_et_relation_pk;
                        COMMIT;
                        html.jump_to ( 'email_util.edit_et_relation');
                ELSIF ( p_name IS NULL ) THEN
                        html.jump_to ( 'email_util.edit_et_relation');
                ELSE
                        html.b_adm_page;
                        html.b_box( get.txt('et_relation') );
                        html.b_table;
                        html.b_form('email_util.edit_et_relation');
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="yes">
                        <input type="hidden" name="p_et_relation_pk" value="'||p_et_relation_pk||'">
                        <td>'|| get.txt('delete_info') ||' "'||p_name||'" ?'); html.submit_link( get.txt('YES') ); htp.p('</td>');
                        html.e_form;
                        htp.p('<td>'); html.button_link( get.txt('NO'), 'email_util.edit_et_relation');htp.p('</td></tr>');
                        html.e_table;
                        html.e_box;
                        html.e_page_2;
                END IF;
        ELSIF ( p_command = 'new') THEN
                html.b_adm_page;
                html.b_box( get.txt('et_relation') );
                html.b_table;
                html.b_form('email_util.edit_et_relation');
                htp.p('<input type="hidden" name="p_command" value="insert">
                <input type="hidden" name="p_et_relation_pk" value="'||v_et_relation_pk||'">
                <tr><td>'|| get.txt('select_email_text') ||':</td><td>');
                select_email_text( v_email_text_pk );
                htp.p('</td></tr>
                <tr><td>'|| get.txt('select_service') ||':</td><td>');
                html.select_service( v_service_pk , 0);
                htp.p('</td></tr>
                <td>'|| get.txt('geography_location') ||':</td>
                <td><input type="hidden" name="p_geography_pk" value="'|| v_geography_pk ||'">
                <input type="text" size="30" name="p_geo_name" value="'|| get.txt('no_location') ||'"
                onSelect=javascript:openWin(''select_geo.select_location?p_no_levels=4'',300,200)
                onClick=javascript:openWin(''select_geo.select_location?p_no_levels=4'',300,200)>
                '|| html.popup( get.txt('change_geo'), 'select_geo.select_location?p_no_levels=4', '300', '200') ||'
                      &nbsp;&nbsp;&nbsp;&nbsp;<a href="Javascript: setblankgeo()">'||get.txt('clear_geo')||'</a>
                        <script language=JavaScript>
                        function setblankgeo(){
                      document.form.p_geography_pk.value="";
                      document.form.p_geo_name.value="";
                      }
                      </script>
                </td></tr>
                <tr><td>'|| get.txt('select_organization') ||':</td><td>');
                select_organization( v_organization_pk );
                htp.p('</td></tr>
                <tr><td>'|| get.txt('list') ||':</td>
                <td><input type="hidden" name="p_list_pk" value="">
                <input type="text" size="30" name="p_name" value=""
                onSelect=javascript:openWin(''select_list.sel_type'',300,200)
                onClick=javascript:openWin(''select_list.sel_type'',300,200)>
                '|| html.popup( get.txt('change_list'), 'select_list.sel_type', '300', '200') ||'
                      &nbsp;&nbsp;&nbsp;&nbsp;<a href="Javascript: setblank()">'||get.txt('clear_list')||'</a>
                        <script language=JavaScript>
                        function setblank(){
                      document.form.p_list_pk.value="";
                      document.form.p_name.value="";
                      }
                      </script>
                </td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('insert')); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        ELSIF ( p_command = 'insert') THEN
                SELECT et_relation_seq.NEXTVAL
                INTO v_et_relation_pk
                FROM dual;
                INSERT INTO et_relation
                ( et_relation_pk, email_text_fk, service_fk,
                geography_fk, list_fk, organization_fk )
                VALUES
                ( v_et_relation_pk, p_email_text_pk, p_service_pk,
                p_geography_pk, p_list_pk, p_organization_pk );
                COMMIT;
                html.jump_to ( 'email_util.edit_et_relation' );
        ELSE
                html.b_adm_page;
                html.b_box( get.txt('et_relation') );
                html.b_table;
                OPEN get_et_relation;
                LOOP
                        FETCH get_et_relation INTO v_et_relation_pk, v_name,
                                v_service_pk, v_geography_pk, v_list_pk, v_organization_pk;
                        EXIT WHEN get_et_relation%NOTFOUND;
                        html.b_form('email_util.edit_et_relation','form'|| v_et_relation_pk);
                        htp.p('<input type="hidden" name="p_command" value="update">
                        <input type="hidden" name="p_et_relation_pk" value="'||v_et_relation_pk||'">
                        <tr><td>'|| get.txt('et_relation') ||':</td><td>'||v_name||'</td>
                        <td>'); html.submit_link( get.txt('edit'), 'form'||v_et_relation_pk ); htp.p('</td>');
                        html.e_form;
                        html.b_form('email_util.edit_et_relation','delete'|| v_et_relation_pk);
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="'||v_name||'">
                        <input type="hidden" name="p_et_relation_pk" value="'||v_et_relation_pk||'">
                        <td>'); html.submit_link( get.txt('delete'), 'delete'||v_et_relation_pk ); htp.p('</td></tr>');
                        html.e_form;
                END LOOP;
                close get_et_relation;
                html.b_form('email_util.edit_et_relation','form_new');
                htp.p('<input type="hidden" name="p_command" value="new">
                <td>'); html.submit_link( get.txt('new'), 'form_new'); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        END IF;
END IF;
EXCEPTION
WHEN NO_DATA_FOUND
THEN
htp.p('No data found! email_util.sql -> edit_et_relation');
END;
END edit_et_relation;

---------------------------------------------------------------------
-- Name: select_organization
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE select_organization (     p_organization_pk      IN organization.organization_pk%TYPE  DEFAULT NULL)
IS
BEGIN
DECLARE

CURSOR  all_organizations IS
SELECT  organization_pk, name
FROM    organization
ORDER BY ( UPPER (name) )
;

v_organization_pk       organization.organization_pk%TYPE       DEFAULT NULL;
v_name                  organization.name%TYPE                  DEFAULT NULL;
v_value                 VARCHAR2(10)                            DEFAULT NULL;

BEGIN

        htp.p('<select name="p_organization_pk" size="5">');
        IF ( p_organization_pk > 0 ) THEN
                htp.p('<OPTION value="">'|| get.txt('no_organization') ||'</OPTION>');
        ELSE
                htp.p('<OPTION value="" selected>'|| get.txt('no_organization') ||'</OPTION>');
        END IF;
        OPEN all_organizations;
        LOOP
                FETCH all_organizations INTO v_organization_pk, v_name;
                EXIT WHEN all_organizations%NOTFOUND;

                IF ( v_organization_pk = p_organization_pk ) THEN
                        v_value := ' selected';
                ELSE
                        v_value := '';
                END IF;

                htp.p('<option value="'|| v_organization_pk ||'"'||v_value||'>'|| v_name ||'</option>');
        END LOOP;
        close all_organizations;
        htp.p('</select>');
END;
END select_organization;

---------------------------------------------------------------------
-- Name: select_email_text
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE select_email_text (     p_email_text_pk      IN email_text.email_text_pk%TYPE  DEFAULT NULL)
IS
BEGIN
DECLARE

CURSOR  all_email_texts IS
SELECT  et.email_text_pk, sg.name
FROM    email_text et, string_group sg
WHERE   et.name_sg_fk = sg.string_group_pk
ORDER BY UPPER( sg.name)
;

v_email_text_pk   email_text.email_text_pk%TYPE             DEFAULT NULL;
v_name            string_group.name%TYPE                   DEFAULT NULL;
v_value           VARCHAR2(10)                        DEFAULT NULL;

BEGIN

        htp.p('<select name="p_email_text_pk">');
        OPEN all_email_texts;
        LOOP
                FETCH all_email_texts INTO v_email_text_pk, v_name;
                EXIT WHEN all_email_texts%NOTFOUND;

                IF ( v_email_text_pk = p_email_text_pk ) THEN
                        v_value := ' selected';
                ELSE
                        v_value := '';
                END IF;

                htp.p('<option value="'|| v_email_text_pk ||'"'||v_value||'>'|| v_name ||'</option>');
        END LOOP;
        close all_email_texts;
        htp.p('</select>');
END;
END select_email_text;


---------------------------------------------------------------------
---------------------------------------------------------------------
END;
/
show errors;
