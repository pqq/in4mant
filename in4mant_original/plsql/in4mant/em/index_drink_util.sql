
DROP INDEX drink_idx_1;
CREATE INDEX drink_idx_1 ON drink ( name_sg_fk );
DROP INDEX drink_idx_2;
CREATE INDEX drink_idx_2 ON drink ( glass_sg_fk );
DROP INDEX drink_idx_3;
CREATE INDEX drink_idx_3 ON drink ( category_sg_fk );
DROP INDEX drink_idx_4;
CREATE INDEX drink_idx_4 ON drink ( info_sg_fk );

DROP INDEX drink_idx_5;
CREATE INDEX drink_idx_5 ON drink ( drink_pk, name_sg_fk );
DROP INDEX drink_idx_6;
CREATE INDEX drink_idx_6 ON drink ( drink_pk, glass_sg_fk );
DROP INDEX drink_idx_7;
CREATE INDEX drink_idx_7 ON drink ( drink_pk, category_sg_fk );
DROP INDEX drink_idx_8;
CREATE INDEX drink_idx_8 ON drink ( drink_pk, info_sg_fk );

DROP INDEX ingredient_idx_1;
CREATE INDEX ingredient_idx_1 ON ingredient ( ing_type_sg_fk );
DROP INDEX ingredient_idx_2;
CREATE INDEX ingredient_idx_2 ON ingredient ( name_sg_fk );
DROP INDEX ingredient_idx_3;
CREATE INDEX ingredient_idx_3 ON ingredient ( info_sg_fk );

DROP INDEX ingredient_idx_4;
CREATE INDEX ingredient_idx_4 ON ingredient ( ingredient_pk, ing_type_sg_fk );



