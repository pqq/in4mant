CREATE OR REPLACE PACKAGE page_adm IS

PROCEDURE startup;
PROCEDURE edit_page_type (  p_command               IN varchar2                             DEFAULT NULL,
                            p_name                  IN string_group.name%TYPE               DEFAULT NULL,
                            p_description           IN string_group.description%TYPE        DEFAULT NULL,
                            p_string_group_pk       IN string_group.string_group_pk%TYPE    DEFAULT NULL
                            );
END;
/
CREATE OR REPLACE PACKAGE BODY page_adm IS
---------------------------------------------------------------------
---------------------------------------------------------------------
-- Name: startup
-- Type: procedure
-- What: Genererer opp hovedsiden for admin av en organisasjon
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE startup
IS
BEGIN
      edit_page_type;
END startup;

---------------------------------------------------------------------
-- Name: edit_page_type
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 29.06.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE edit_page_type (  p_command               IN varchar2                             DEFAULT NULL,
                            p_name                  IN string_group.name%TYPE               DEFAULT NULL,
                            p_description           IN string_group.description%TYPE        DEFAULT NULL,
                            p_string_group_pk       IN string_group.string_group_pk%TYPE    DEFAULT NULL
                         )
IS
BEGIN
DECLARE

CURSOR  get_string IS
SELECT  sg.string_group_pk, sg.name, sg.description, dt.page_type_pk
FROM    page_type dt, string_group sg
WHERE   dt.name_sg_fk=sg.string_group_pk
ORDER BY sg.name
;

v_string_group_pk       string_group.string_group_pk%TYPE       DEFAULT NULL;
v_name                  string_group.name%TYPE                  DEFAULT NULL;
v_description           string_group.description%TYPE           DEFAULT NULL;
v_page_type_pk          page_type.page_type_pk%TYPE             DEFAULT NULL;

BEGIN

IF (    login.timeout('page_adm.edit_page_type') > 0 AND
        login.right('page_adm') > 0 ) THEN

        IF ( p_command = 'update') THEN
                SELECT  string_group_pk, name, description
                INTO    v_string_group_pk, v_name, v_description
                FROM    string_group
                WHERE   string_group_pk=p_string_group_pk
                ;

                html.b_adm_page;
                html.b_box( get.txt('page_type') );
                html.b_table;
                html.b_form('page_adm.edit_page_type');
                htp.p('<input type="hidden" name="p_command" value="edit">
                <input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                <tr><td>'|| get.txt('name') ||':</td><td>
                '|| html.text('p_name', '50', '50', v_name ) ||'
                </td></tr>
                <tr><td>'|| get.txt('description') ||':(maks 200 tegn)</td><td>
                <textarea name="p_description" rows="5" cols="70" wrap="virtual">'||v_description||'</textarea>
                </td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('update') ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page;
        ELSIF ( p_command = 'edit') THEN
                UPDATE  string_group
                SET     name=trans(p_name),
                        description=trans(p_description)
                WHERE   string_group_pk=p_string_group_pk;
                COMMIT;
                UPDATE  page_type
                SET     description=trans(p_name)
                WHERE   name_sg_fk=p_string_group_pk;
                COMMIT;
                html.jump_to ( 'page_adm.edit_page_type');
        ELSIF ( p_command = 'delete') THEN
                IF ( p_name = 'yes') THEN
                        DELETE  FROM string_group
                        WHERE   string_group_pk=p_string_group_pk;
                        COMMIT;
                        html.jump_to ( 'page_adm.edit_page_type');
                ELSIF ( p_name IS NULL ) THEN
                        html.jump_to ( 'page_adm.edit_page_type');
                ELSE
                        html.b_adm_page;
                        html.b_box( get.txt('page_type') );
                        html.b_table;
                        html.b_form('page_adm.edit_page_type');
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="yes">
                        <input type="hidden" name="p_string_group_pk" value="'||p_string_group_pk||'">
                        <td>'|| get.txt('delete_info')||' "'||trans(p_name)||'" ?'); html.submit_link( get.txt('YES') ); htp.p('</td>');
                        html.e_form;
                        htp.p('<td>'); html.button_link( get.txt('NO'), 'page_adm.edit_page_type');htp.p('</td></tr>');
                        html.e_table;
                        html.e_box;
                        html.e_page;
                END IF;
        ELSIF ( p_command = 'new') THEN
                html.b_adm_page;
                html.b_box( get.txt('page_type') );
                html.b_table;
                html.b_form('page_adm.edit_page_type','form_insert');
                htp.p('<input type="hidden" name="p_command" value="insert">
                <tr><td>'|| get.txt('name') ||':</td><td>
                '|| html.text('p_name', '50', '50', '' ) ||'
                </td></tr>
                <tr><td>'|| get.txt('description') ||':(maks 200 tegn)</td><td>
                <textarea name="p_description" rows="5" cols="70" wrap="virtual"></textarea>
                </td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('insert'), 'form_insert' ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page;
        ELSIF ( p_command = 'insert') THEN
                SELECT string_group_seq.NEXTVAL
                INTO v_string_group_pk
                FROM dual;
                SELECT page_type_seq.NEXTVAL
                INTO v_page_type_pk
                FROM dual;
                INSERT INTO string_group
                ( string_group_pk, name, description )
                VALUES
                ( v_string_group_pk, trans(p_name),
                SUBSTR(trans(p_description),1,200) );
                COMMIT;
                INSERT INTO page_type
                ( page_type_pk, name_sg_fk, description )
                VALUES
                ( v_page_type_pk, v_string_group_pk,
                SUBSTR(trans(p_description),1,150) );
                COMMIT;
                html.jump_to ( 'page_adm.edit_page_type' );
        ELSE
                html.b_adm_page;
                html.b_box( get.txt('page_type') );
                html.b_table;
                OPEN get_string;
                LOOP
                        fetch get_string INTO v_string_group_pk, v_name, v_description, v_page_type_pk;
                        exit when get_string%NOTFOUND;
                        html.b_form('page_adm.edit_page_type','form'|| v_string_group_pk);
                        htp.p('<input type="hidden" name="p_command" value="update">
                        <input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                        <tr><td>'|| v_page_type_pk ||':</td><td>'||v_name||'</td>
                        <td>'); html.submit_link( get.txt('edit'), 'form'||v_string_group_pk ); htp.p('</td>');
                        html.e_form;
                        html.b_form('page_adm.edit_page_type','delete'|| v_string_group_pk);
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="'||v_name||'">
                        <input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                        <td>'); html.submit_link( get.txt('delete'), 'delete'||v_string_group_pk ); htp.p('</td></tr>');
                        html.e_form;
                END LOOP;
                close get_string;
                html.b_form('page_adm.edit_page_type','form_new');
                htp.p('<input type="hidden" name="p_command" value="new">
                <td>'); html.submit_link( get.txt('new'), 'form_new'); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page;
        END IF;
END IF;
EXCEPTION
WHEN NO_DATA_FOUND
THEN
htp.p('No data found! page_adm.sql -> edit_page_type');
END;
END edit_page_type;

---------------------------------------------------------------------
---------------------------------------------------------------------
END;
/
