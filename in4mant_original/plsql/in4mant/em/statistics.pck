CREATE OR REPLACE PACKAGE statistics IS
PROCEDURE reg (	p_organization_pk	IN organization.organization_pk%TYPE	DEFAULT NULL,
		p_package		IN VARCHAR2				DEFAULT NULL
		);
PROCEDURE startup;
END;
/
CREATE OR REPLACE PACKAGE BODY statistics IS
---------------------------------------------------------------------
---------------------------------------------------------------------
-- Name: startup
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 30.08.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE startup
IS
BEGIN
	reg;
END startup;

---------------------------------------------------------------------
-- Name: reg
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 30.08.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE reg (	p_organization_pk	IN organization.organization_pk%TYPE	DEFAULT NULL,
		p_package		IN VARCHAR2				DEFAULT NULL
		)
IS
BEGIN
DECLARE

v_number	NUMBER				DEFAULT NULL;

BEGIN
UPDATE	STAT_ORG
SET	VOLUME = VOLUME + 1
WHERE	ORGANIZATION_FK = 3
AND	TO_CHAR(STAT_DATE,'YYYYMMDD') = TO_CHAR(SYSDATE,'YYYYMMDD')
;
IF ( SQL%NOTFOUND ) THEN
	INSERT	INTO STAT_ORG
	( STAT_ORG_PK, ORGANIZATION_FK, VOLUME, STAT_DATE )
	VALUES
	( STAT_ORG_SEQ.NEXTVAL, 3, 1, SYSDATE )
	;
END IF;
commit;
htp.p('ugh uhg');
END;
END reg;

---------------------------------------------------------------------
---------------------------------------------------------------------
END;
/
