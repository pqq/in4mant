SET define off
SET verify off
SET feedback off
SET termout on
SET echo off
SET pagesize 0

---------------------------------------------------------
-- Name:        new_synonym
-- What:        find packages and tables and make synonyms
-- Author:      Espen Messel
-- Start date:  06.01.2003
---------------------------------------------------------

CREATE OR REPLACE PROCEDURE new_synonym
IS
BEGIN
DECLARE

        CURSOR  SELECT_PACKAGES is
        SELECT  DISTINCT(NAME)
        FROM    USER_SOURCE
        ORDER   BY NAME;

        CURSOR  SELECT_TABLES is
        SELECT  DISTINCT(TABLE_NAME)
        FROM    USER_TABLES
        ORDER   BY TABLE_NAME;

        v_name  VARCHAR2(30) DEFAULT NULL;
        v_nr    NUMBER       DEFAULT 0;
        v_a     NUMBER       DEFAULT 0;

BEGIN
   OPEN select_tables;
   loop
      FETCH select_tables INTO v_name;
      EXIT WHEN select_tables%NOTFOUND;

      SELECT COUNT(*)
        INTO v_nr
        FROM DBA_SYNONYMS
        WHERE TABLE_OWNER = 'IN4MANT_ADM'
        AND   TABLE_NAME = v_name;

      IF ( v_nr = 0 ) THEN
         htp.p('CREATE PUBLIC SYNONYM '||v_name||' FOR IN4MANT_ADM.'|| v_name||';<br>');
      ELSE
         v_a:=v_a+1;
      END IF;
      END LOOP;
        CLOSE select_tables;

        OPEN select_packages;
        LOOP
           FETCH select_packages INTO v_name;
           EXIT WHEN select_packages%NOTFOUND;

           SELECT COUNT(*)
             INTO v_nr
             FROM DBA_SYNONYMS
             WHERE TABLE_OWNER = 'IN4MANT_ADM'
             AND   TABLE_NAME = v_name;

           IF ( v_nr = 0 ) THEN
              htp.p('CREATE PUBLIC SYNONYM '||v_name||' FOR IN4MANT_ADM.'|| v_name||';<br>');
           ELSE
              v_a:=v_a+1;
           END IF;
           END LOOP;
        CLOSE select_packages;
        htp.p('Det finnes '||v_a||' synonymer i databasen fra f�r.');

END;
END new_synonym;

/
show errors;


