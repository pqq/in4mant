set define off
PROMPT *** package: STAT ***


CREATE OR REPLACE PACKAGE stat IS

PROCEDURE reg (         p_organization_pk       IN organization.organization_pk%TYPE    DEFAULT NULL,
                        p_document_pk           IN document.document_pk%TYPE            DEFAULT NULL,
                        p_calendar_pk           IN calendar.calendar_pk%TYPE            DEFAULT NULL,
                        p_fora_type_pk          IN fora_type.fora_type_pk%TYPE          DEFAULT NULL,
                        p_album_pk              IN album.album_pk%TYPE                  DEFAULT NULL,
                        p_picture_pk            IN picture.picture_pk%TYPE              DEFAULT NULL,
                        p_page_type_pk          IN page_type.page_type_pk%TYPE          DEFAULT NULL,
                        p_service_pk            IN service.service_pk%TYPE              DEFAULT NULL
                        );
PROCEDURE res_page (    p_from                  IN VARCHAR2                             DEFAULT NULL,
                        p_to                    IN VARCHAR2                             DEFAULT NULL,
                        p_organization_pk       IN organization.organization_pk%TYPE    DEFAULT NULL
                        );
PROCEDURE res_page_serv (    p_from       IN VARCHAR2                  DEFAULT NULL,
                             p_to         IN VARCHAR2                  DEFAULT NULL,
                             p_service_pk IN service.service_pk%TYPE   DEFAULT NULL
                        );
PROCEDURE update_mail_log (p_mail_log_pk       IN mail_log.mail_log_pk%TYPE       DEFAULT NULL);
PROCEDURE startup;
PROCEDURE rand;
END;
/
CREATE OR REPLACE PACKAGE BODY stat IS
---------------------------------------------------------------------
---------------------------------------------------------------------
-- Name: startup
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 13.09.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE startup
IS
BEGIN
        reg;
END startup;

---------------------------------------------------------------------
-- Name: reg
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 13.09.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE reg ( p_organization_pk       IN organization.organization_pk%TYPE    DEFAULT NULL,
                p_document_pk           IN document.document_pk%TYPE            DEFAULT NULL,
                p_calendar_pk           IN calendar.calendar_pk%TYPE            DEFAULT NULL,
                p_fora_type_pk          IN fora_type.fora_type_pk%TYPE          DEFAULT NULL,
                p_album_pk              IN album.album_pk%TYPE                  DEFAULT NULL,
                p_picture_pk            IN picture.picture_pk%TYPE              DEFAULT NULL,
                p_page_type_pk          IN page_type.page_type_pk%TYPE          DEFAULT NULL,
                p_service_pk            IN service.service_pk%TYPE              DEFAULT NULL
                )
IS
BEGIN
DECLARE

v_number        NUMBER                          DEFAULT NULL;
v_service_pk    service.service_pk%TYPE         DEFAULT NULL;

BEGIN
IF ( p_service_pk IS NULL ) THEN
   v_service_pk := get.serv;
ELSE
   v_service_pk := p_service_pk;
END IF;

IF ( p_picture_pk IS NOT NULL ) THEN
        UPDATE  STAT_ORG
        SET     VOLUME = VOLUME + 1
        WHERE   ORGANIZATION_FK = p_organization_pk
        AND     PICTURE_FK = p_picture_pk
        AND     TO_CHAR(STAT_DATE,'YYYYMMDD') = TO_CHAR(SYSDATE,'YYYYMMDD')
        AND     SERVICE_FK = v_service_pk
        ;
ELSIF ( p_album_pk IS NOT NULL ) THEN
        UPDATE  STAT_ORG
        SET     VOLUME = VOLUME + 1
        WHERE   ORGANIZATION_FK = p_organization_pk
        AND     ALBUM_FK = p_album_pk
        AND     TO_CHAR(STAT_DATE,'YYYYMMDD') = TO_CHAR(SYSDATE,'YYYYMMDD')
        AND     SERVICE_FK = v_service_pk
        ;
ELSIF ( p_fora_type_pk IS NOT NULL ) THEN
        IF ( p_organization_pk IS NOT NULL ) THEN
                UPDATE  STAT_ORG
                SET     VOLUME = VOLUME + 1
                WHERE   ORGANIZATION_FK = p_organization_pk
                AND     FORA_TYPE_FK = p_fora_type_pk
                AND     TO_CHAR(STAT_DATE,'YYYYMMDD') = TO_CHAR(SYSDATE,'YYYYMMDD')
                AND     SERVICE_FK = v_service_pk
                ;
        ELSE
                UPDATE  STAT_ORG
                SET     VOLUME = VOLUME + 1
                WHERE   ORGANIZATION_FK IS NULL
                AND     FORA_TYPE_FK = p_fora_type_pk
                AND     TO_CHAR(STAT_DATE,'YYYYMMDD') = TO_CHAR(SYSDATE,'YYYYMMDD')
                AND     SERVICE_FK = v_service_pk
                ;
        END IF;
ELSIF ( p_calendar_pk IS NOT NULL ) THEN
        UPDATE  STAT_ORG
        SET     VOLUME = VOLUME + 1
        WHERE   ORGANIZATION_FK = p_organization_pk
        AND     CALENDAR_FK = p_calendar_pk
        AND     TO_CHAR(STAT_DATE,'YYYYMMDD') = TO_CHAR(SYSDATE,'YYYYMMDD')
        AND     SERVICE_FK = v_service_pk
        ;
ELSIF ( p_document_pk IS NOT NULL ) THEN
        UPDATE  STAT_ORG
        SET     VOLUME = VOLUME + 1
        WHERE   ORGANIZATION_FK = p_organization_pk
        AND     DOCUMENT_FK = p_document_pk
        AND     TO_CHAR(STAT_DATE,'YYYYMMDD') = TO_CHAR(SYSDATE,'YYYYMMDD')
        AND     SERVICE_FK = v_service_pk
        ;
ELSIF ( p_organization_pk IS NOT NULL ) THEN
        UPDATE  STAT_ORG
        SET     VOLUME = VOLUME + 1
        WHERE   ORGANIZATION_FK = p_organization_pk
        AND     DOCUMENT_FK IS NULL
        AND     CALENDAR_FK IS NULL
        AND     FORA_TYPE_FK IS NULL
        AND     ALBUM_FK IS NULL
        AND     PICTURE_FK IS NULL
        AND     TO_CHAR(STAT_DATE,'YYYYMMDD') = TO_CHAR(SYSDATE,'YYYYMMDD')
        AND     SERVICE_FK = v_service_pk
        ;
ELSIF ( p_page_type_pk IS NOT NULL ) THEN
        UPDATE  STAT_ORG
        SET     VOLUME = VOLUME + 1
        WHERE   PAGE_TYPE_FK = p_page_type_pk
        AND     ORGANIZATION_FK IS NULL
        AND     TO_CHAR(STAT_DATE,'YYYYMMDD') = TO_CHAR(SYSDATE,'YYYYMMDD')
        AND     SERVICE_FK = v_service_pk
        ;
END IF;

IF ( SQL%NOTFOUND ) THEN
        INSERT  INTO STAT_ORG
        ( STAT_ORG_PK, ORGANIZATION_FK, DOCUMENT_FK, CALENDAR_FK, FORA_TYPE_FK,
        ALBUM_FK, PICTURE_FK, PAGE_TYPE_FK, VOLUME, STAT_DATE, SERVICE_FK )
        VALUES
        ( STAT_ORG_SEQ.NEXTVAL, p_organization_pk, p_document_pk, p_calendar_pk, p_fora_type_pk,
        p_album_pk, p_picture_pk, p_page_type_pk, 1, SYSDATE, v_service_pk )
        ;
END IF;

COMMIT;

IF ( owa_util.get_cgi_env('REQUEST_URI') = '/cgi-bin/stat.reg' ) THEN
        htp.p('ugh uhg');
END IF;
END;
EXCEPTION
   WHEN OTHERS THEN
     htp.p();
END reg;

---------------------------------------------------------------------
-- Name: res_page
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 13.09.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE res_page (    p_from                  IN VARCHAR2                             DEFAULT NULL,
                        p_to                    IN VARCHAR2                             DEFAULT NULL,
                        p_organization_pk       IN organization.organization_pk%TYPE    DEFAULT NULL
                        )
IS
BEGIN
DECLARE

v_organization_pk       organization.organization_pk%TYPE       DEFAULT NULL;
v_all_date              DATE                                    DEFAULT NULL;
v_heading               VARCHAR2(4000)                          DEFAULT NULL;
v_volume                stat_org.volume%TYPE                    DEFAULT NULL;
v_name                  fora_type.name%TYPE                     DEFAULT NULL;
v_activated             fora_type.activated%TYPE                DEFAULT NULL;
v_album_pk              album.album_pk%TYPE                     DEFAULT NULL;
v_logo                  organization.logo%TYPE                  DEFAULT NULL;
v_accepted              organization.accepted%TYPE              DEFAULT NULL;
v_from                  VARCHAR2(20)                            DEFAULT NULL;
v_to                    VARCHAR2(20)                            DEFAULT NULL;
v_document_pk           document.document_pk%TYPE               DEFAULT NULL;
v_calendar_pk           calendar.calendar_pk%TYPE               DEFAULT NULL;
v_fora_type_pk          fora_type.fora_type_pk%TYPE             DEFAULT NULL;
v_picture_pk            picture.picture_pk%TYPE                 DEFAULT NULL;
v_color                 VARCHAR2(500)                           DEFAULT NULL;
v_language_pk           la.language_pk%TYPE                     DEFAULT NULL;

CURSOR  get_doc ( v_org_pk IN NUMBER ) IS
SELECT  distinct(so.document_fk), d.heading, d.publish_date
FROM    document d, stat_org so
WHERE   d.document_pk = so.document_fk
AND     so.organization_fk = v_org_pk
AND     stat_date > to_date(v_from,get.txt('date_year'))
AND     stat_date <= to_date(v_to,get.txt('date_year'))+1
ORDER BY d.publish_date DESC, d.heading
;

CURSOR  get_cal ( v_org_pk IN NUMBER ) IS
SELECT  distinct(so.calendar_fk), c.heading, c.event_date
FROM    calendar c, stat_org so
WHERE   so.organization_fk = v_org_pk
AND     c.calendar_pk = so.calendar_fk
AND     stat_date > to_date(v_from,get.txt('date_year'))
AND     stat_date <= to_date(v_to,get.txt('date_year'))+1
ORDER BY c.event_date DESC, c.heading
;

CURSOR  get_for ( v_org_pk IN NUMBER ) IS
SELECT  distinct(so.fora_type_fk), ft.name, ft.activated
FROM    fora_type ft, stat_org so
WHERE   so.organization_fk = v_org_pk
AND     ft.fora_type_pk = so.fora_type_fk
AND     stat_date > to_date(v_from,get.txt('date_year'))
AND     stat_date <= to_date(v_to,get.txt('date_year'))+1
ORDER BY ft.name
;

CURSOR  get_alb ( v_org_pk IN NUMBER ) IS
SELECT  distinct(album_fk)
FROM    stat_org
WHERE   organization_fk = v_org_pk
AND     album_fk IS NOT NULL
AND     stat_date > to_date(v_from,get.txt('date_year'))
AND     stat_date <= to_date(v_to,get.txt('date_year'))+1
ORDER BY album_fk DESC
;

CURSOR  get_pic ( v_org_pk IN NUMBER ) IS
SELECT  distinct(so.picture_fk), p.when_taken, p.whom
FROM    picture p, stat_org so
WHERE   so.organization_fk = v_org_pk
AND     p.picture_pk = so.picture_fk
AND     stat_date > to_date(v_from,get.txt('date_year'))
AND     stat_date <= to_date(v_to,get.txt('date_year'))+1
ORDER BY p.when_taken DESC, p.whom
;

CURSOR  get_org ( v_org_pk IN NUMBER ) IS
SELECT  sum(volume)
FROM    stat_org
WHERE   ORGANIZATION_FK = v_org_pk
AND     DOCUMENT_FK IS NULL
AND     CALENDAR_FK IS NULL
AND     FORA_TYPE_FK IS NULL
AND     ALBUM_FK IS NULL
AND     PICTURE_FK IS NULL
AND     stat_date > to_date(v_from,get.txt('date_year'))
AND     stat_date <= to_date(v_to,get.txt('date_year'))+1
;

CURSOR  get_tot ( v_org_pk IN NUMBER ) IS
SELECT  SUM(volume)
FROM    stat_org
WHERE   organization_fk = v_org_pk
AND     stat_date > to_date(v_from,get.txt('date_year'))
AND     stat_date <= to_date(v_to,get.txt('date_year'))+1
;

CURSOR  get_serv_lan ( v_org_pk IN NUMBER ) IS
SELECT  language_fk
FROM    service_on_lang
WHERE   service_fk = get.oserv( v_org_pk )
;

BEGIN

IF (login.timeout('stat.res_page') > 0 ) THEN
        v_organization_pk := get.oid;

        IF ( p_organization_pk IS NOT NULL
           AND get.u_type < -2 ) THEN
            v_organization_pk := p_organization_pk;
        END IF;

        IF ( v_organization_pk IS NULL ) THEN
                html.jump_to('http://www.in4mant.com/cgi-bin/main_page.startup',2,'Not a client user!');
        ELSE

                select name, logo, accepted
                into
                v_name, v_logo, v_accepted
                from organization
                where organization_pk = v_organization_pk;

                IF ( v_logo IS NULL ) THEN
                        v_logo := 'no_logo.gif';
                END IF;

                v_color := get.value( 'c_'|| get.serv_name ||'_mmb' );

                html.b_page(NULL,NULL,NULL,2);
                html.b_box(get.txt('stat_res_page'),'100%','stat.res_page');
                html.b_table;
                htp.p('
<tr><td align="right" colspan="3">
<img src="'||get.value('org_logo_dir')||'/'||v_logo ||'"</td></tr></tr>
<tr><td align="center" colspan="3"><font size="4" color="red">
<b>'||get.txt('stat_page')||' '|| v_name ||'</b></font></td></tr>
<tr><td colspan="3">'||get.txt('stat_page_info')||'</td></tr>
<tr><td colspan="3"><HR size="2" noshade="noshade"></td></tr>');

                IF ( p_from IS NOT NULL ) THEN
                        v_from := p_from;
                ELSE
                        v_from := TO_CHAR(SYSDATE,REPLACE(get.txt('date_year'),'DD','::'));
                        v_from := REPLACE(v_from ,'::','01');
                END IF;
                IF ( p_to IS NOT NULL ) THEN
                        v_to := p_to;
                ELSE
                        v_to := TO_CHAR(LAST_DAY(SYSDATE),get.txt('date_year'));
                END IF;
                html.b_form('stat.res_page');
                htp.p('<tr><td colspan="3">');
                html.b_table;
                htp.p('<tr><td>'||get.txt('period')||':
                <input type="text" size="10" name="p_from" value="'||v_from||'"
                onSelect=javascript:openWin(''date_time.startup?p_command=date&p_variable=p_from'',300,200)
                onClick=javascript:openWin(''date_time.startup?p_command=date&p_variable=p_from'',300,200)>'||
                html.popup( get.txt('from_date'),'date_time.startup?p_command=date&p_variable=p_from','300', '200')||'
                </td><td>
                <input type="text" size="10" name="p_to" value="'||v_to||'"
                onSelect=javascript:openWin(''date_time.startup?p_command=date&p_variable=p_to'',300,200)
                onClick=javascript:openWin(''date_time.startup?p_command=date&p_variable=p_to'',300,200)>'||
                html.popup( get.txt('to_date'),'date_time.startup?p_command=date&p_variable=p_to','300', '200')||'
                <input type="hidden" name="p_organization_pk" value="'||p_organization_pk||'">
                </td><td> ');
                html.submit_link( get.txt('show_statistics') );
                htp.p('</td></tr>');
                html.e_table;
                htp.p('</td></tr>');
                html.e_form;

                -------------------------------------------------
                --                   Dokumenter
                -------------------------------------------------
                htp.p('
                <tr bgcolor="'||v_color||'"><td colspan="3" align="center"><b>'||get.txt('document')||':</b></td></tr>
                <tr><td align="left"><b>'||get.txt('date')||':</b></td>
                <td><b>'||get.txt('subject')||':</b></td>
                <td align="right"><b>'||get.txt('volume')||':</b></td></tr>');
                OPEN get_doc( v_organization_pk );
                LOOP
                        FETCH get_doc INTO v_document_pk, v_heading, v_all_date;
                        EXIT WHEN get_doc%NOTFOUND;

                        SELECT  SUM(volume)
                        INTO    v_volume
                        FROM    stat_org
                        WHERE   document_fk = v_document_pk
                        AND     stat_date > to_date(v_from,get.txt('date_year'))
                        AND     stat_date <= to_date(v_to,get.txt('date_year'))+1
                        ;

                        htp.p('<tr><td>'||TO_CHAR(v_all_date,get.txt('date_long'))||'</td>
                        <td>'||v_heading||'</td>
                        <td align="right">'||v_volume||'</td></tr>');
                END LOOP;
                CLOSE get_doc;
                -------------------------------------------------
                --                   Calendar
                -------------------------------------------------
                htp.p('
                <tr bgcolor="'||v_color||'"><td colspan="3" align="center"><b>'||get.txt('calendar')||':</b></td></tr>
                <tr><td align="left"><b>'||get.txt('date')||':</b></td>
                <td><b>'||get.txt('subject')||':</b></td>
                <td align="right"><b>'||get.txt('volume')||':</b></td></tr>');
                OPEN get_cal( v_organization_pk );
                LOOP
                        FETCH get_cal INTO v_calendar_pk, v_heading, v_all_date;
                        EXIT WHEN get_cal%NOTFOUND;

                        SELECT  sum(volume)
                        INTO    v_volume
                        FROM    stat_org
                        WHERE   calendar_fk = v_calendar_pk
                        AND     stat_date > to_date(v_from,get.txt('date_year'))
                        AND     stat_date <= to_date(v_to,get.txt('date_year'))+1
                        ;

                        htp.p('<tr><td>'||TO_CHAR(v_all_date,get.txt('date_long'))||'</td>
                        <td>'||v_heading||'</td>
                        <td align="right">'||v_volume||'</td></tr>');
                END LOOP;
                CLOSE get_cal;
                -------------------------------------------------
                --                   Album
                -------------------------------------------------
                htp.p('
                <tr bgcolor="'||v_color||'"><td colspan="3" align="center"><b>'||get.txt('album')||':</b></td></tr>
                <tr><td align="left"><b>'||get.txt('date')||':</b></td>
                <td><b>'||get.txt('subject')||':</b></td>
                <td align="right"><b>'||get.txt('volume')||':</b></td></tr>');
                OPEN get_alb( v_organization_pk );
                LOOP
                        FETCH get_alb INTO v_album_pk;
                        EXIT WHEN get_alb%NOTFOUND;

                        SELECT  SUM(volume)
                        INTO    v_volume
                        FROM    stat_org
                        WHERE   album_fk = v_album_pk
                        AND     stat_date > to_date(v_from,get.txt('date_year'))
                        AND     stat_date <= to_date(v_to,get.txt('date_year'))+1
                        ;

                        htp.p('<tr><td>'||get.ap_text(v_album_pk, 'when_period')||'</td>
                        <td>'||get.ap_text(v_album_pk, 'title')||'</td>
                        <td align="right">'||v_volume||'</td></tr>');
                END LOOP;
                CLOSE get_alb;
                -------------------------------------------------
                --                   Pictures
                -------------------------------------------------
                htp.p('
                <tr bgcolor="'||v_color||'"><td colspan="3" align="center"><b>'||get.txt('picture')||':</b></td></tr>
                <tr><td align="left"><b>'||get.txt('date')||':</b></td>
                <td><b>'||get.txt('subject')||':</b></td>
                <td align="right"><b>'||get.txt('volume')||':</b></td></tr>');
                OPEN get_pic( v_organization_pk );
                LOOP
                        FETCH get_pic INTO v_picture_pk, v_all_date, v_heading;
                        EXIT WHEN get_pic%NOTFOUND;

                        SELECT  SUM(volume)
                        INTO    v_volume
                        FROM    stat_org
                        WHERE   picture_fk = v_picture_pk
                        AND     stat_date > to_date(v_from,get.txt('date_year'))
                        AND     stat_date <= to_date(v_to,get.txt('date_year'))+1
                        ;

                        htp.p('<tr><td>'||TO_CHAR(v_all_date,get.txt('date_long'))||'</td>
                        <td>'||v_heading||'</td>
                        <td align="right">'||v_volume||'</td></tr>');
                END LOOP;
                CLOSE get_pic;

                -------------------------------------------------
                --                   Forum
                -------------------------------------------------
                htp.p('
                <tr bgcolor="'||v_color||'"><td colspan="3" align="center"><b>'||get.txt('fora')||':</b></td></tr>
                <tr><td align="left"><b>'||get.txt('subject')||':</b></td>
                <td><b>'||get.txt('activated')||':</b></td>
                <td align="right"><b>'||get.txt('volume')||':</b></td></tr>');
                OPEN get_for( v_organization_pk );
                LOOP
                        FETCH get_for INTO v_fora_type_pk, v_name, v_activated;
                        EXIT WHEN get_for%NOTFOUND;
                        IF ( v_activated = 0 ) THEN
                                v_heading := 'Nei';
                        ELSE
                                v_heading := 'Ja';
                        END IF;

                        SELECT  SUM(volume)
                        INTO    v_volume
                        FROM    stat_org
                        WHERE   fora_type_fk = v_fora_type_pk
                        AND     stat_date > to_date(v_from,get.txt('date_year'))
                        AND     stat_date <= to_date(v_to,get.txt('date_year'))+1
                        ;

                        htp.p('<tr><td>'||v_name||'</td>
                        <td>'||v_heading||'</td>
                        <td align="right">'||v_volume||'</td></tr>');
                END LOOP;
                CLOSE get_for;
                -------------------------------------------------
                --            Organization_pages
                -------------------------------------------------
                htp.p('
                <tr bgcolor="'||v_color||'"><td colspan="3" align="center"><b>'||get.txt('org_page')||':</b></td></tr>
                <tr><td align="left" colspan="2">&nbsp;</td>
                <td align="right"><b>'||get.txt('volume')||':</b></td></tr>');
                OPEN get_org( v_organization_pk );
                LOOP
                        FETCH get_org INTO v_volume;
                        EXIT WHEN get_org%NOTFOUND;
                        htp.p('<tr><td colspan="2">'||get.txt('organization_pages')||'</td>
                        <td align="right">'||v_volume||'</td></tr>');
                END LOOP;
                CLOSE get_org;
                -------------------------------------------------
                --                  Total hits
                -------------------------------------------------
                htp.p('
                <tr bgcolor="'||v_color||'"><td colspan="3" align="center"><b>'||get.txt('total')||':</b></td></tr>
                <tr><td align="left" colspan="2">&nbsp;</td>
                <td align="right"><b>'||get.txt('volume')||':</b></td></tr>');
                OPEN get_tot( v_organization_pk );
                LOOP
                        FETCH get_tot INTO v_volume;
                        EXIT WHEN get_tot%NOTFOUND;
                        htp.p('<tr><td colspan="2">'||get.txt('total_org_pages')||'</td>
                        <td align="right">'||v_volume||'</td></tr>');
                END LOOP;
                CLOSE get_tot;
                -------------------------------------------------
                --              E-mail subscribers
                -------------------------------------------------

                SELECT  count( language_fk )
                INTO    v_volume
                FROM    service_on_lang
                WHERE   service_fk = get.oserv( v_organization_pk )
                ;
                IF ( v_volume = 1 ) then
                                htp.p('<tr><td align="left"><br>&nbsp;</td>
                                        <td align="right" colspan="2">'||get.txt('local')||'</td></tr>
                                        <tr><td align="left">'||get.txt('email_subscribers_now')||'</td>
                                        <td align="right" colspan="2">');
                ELSE
                                htp.p('<tr><td align="left"><br>&nbsp;</td>
                                        <td align="right">'||get.txt('local')||'</td>
                                        <td align="right">'||get.txt('english')||'</td></tr>
                                        <tr><td align="left">'||get.txt('email_subscribers_now')||'</td>
                                        <td align="right">');
                END IF;

                OPEN get_serv_lan ( v_organization_pk );
                LOOP
                        FETCH get_serv_lan INTO v_language_pk;
                        EXIT WHEN get_serv_lan%NOTFOUND;
                        IF ( get_serv_lan%ROWCOUNT > 1 ) THEN
                           htp.p('<td align="right">');
                        END IF;

                        SELECT COUNT(DISTINCT(lu.user_fk))
                        INTO    v_volume
                        FROM    list_org lo, list l, list_user lu, list_type lt
                        WHERE   lt.list_type_pk = l.list_type_fk
                        AND     lu.language_fk = v_language_pk
                        AND     lo.organization_fk = v_organization_pk
                        AND     lo.list_fk = l.list_pk
                        AND
                        (       lu.list_fk = l.level0
                        OR      lu.list_fk = l.level1
                        OR      lu.list_fk = l.level2
                        OR      lu.list_fk = l.level3
                        OR      lu.list_fk = l.level4
                        OR      lu.list_fk = l.level5
                        );

                        htp.p( v_volume ||'</td>');
                END LOOP;
                CLOSE get_serv_lan;

                htp.p('</tr>');

                html.e_table;
                html.e_box;
                html.e_page;
        END IF;
END IF;
END;
END res_page;



---------------------------------------------------------------------
-- Name: res_page_serv
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 13.09.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE res_page_serv (    p_from       IN VARCHAR2                  DEFAULT NULL,
                             p_to         IN VARCHAR2                  DEFAULT NULL,
                             p_service_pk IN service.service_pk%TYPE   DEFAULT NULL
                        )
IS
BEGIN
DECLARE

v_organization_pk       organization.organization_pk%TYPE       DEFAULT NULL;
v_all_date              DATE                                    DEFAULT NULL;
v_heading               VARCHAR2(4000)                          DEFAULT NULL;
v_volume                stat_org.volume%TYPE                    DEFAULT NULL;
v_name                  fora_type.name%TYPE                     DEFAULT NULL;
v_activated             fora_type.activated%TYPE                DEFAULT NULL;
v_album_pk              album.album_pk%TYPE                     DEFAULT NULL;
v_logo                  organization.logo%TYPE                  DEFAULT NULL;
v_accepted              organization.accepted%TYPE              DEFAULT NULL;
v_from                  VARCHAR2(20)                            DEFAULT NULL;
v_to                    VARCHAR2(20)                            DEFAULT NULL;
v_document_pk           document.document_pk%TYPE               DEFAULT NULL;
v_calendar_pk           calendar.calendar_pk%TYPE               DEFAULT NULL;
v_fora_type_pk          fora_type.fora_type_pk%TYPE             DEFAULT NULL;
v_picture_pk            picture.picture_pk%TYPE                 DEFAULT NULL;
v_color                 VARCHAR2(500)                            DEFAULT NULL;
v_user_type_pk          user_type.user_type_pk%TYPE             DEFAULT NULL;
v_page_type_pk          page_type.page_type_pk%TYPE             DEFAULT NULL;

CURSOR  get_page_type IS
SELECT  distinct(sg.name), pt.page_type_pk
FROM    string_group sg, page_type pt, stat_org so
WHERE   sg.string_group_pk = pt.name_sg_fk
AND     so.page_type_fk = pt.page_type_pk
AND     so.document_fk IS NULL
AND     so.organization_fk IS NULL
AND     so.calendar_fk IS NULL
AND     so.album_fk IS NULL
ORDER BY sg.name
;
--AND     pt.service_fk = p_service_pk

CURSOR  get_page_type_all IS
SELECT  sg.name, pt.page_type_pk
FROM    string_group sg, page_type pt
WHERE   sg.string_group_pk = pt.name_sg_fk
ORDER BY sg.name
;
--AND     pt.service_fk IN ( NULL , 0 )

CURSOR  get_tot IS
SELECT  SUM(so.volume)
FROM    stat_org so
WHERE   so.stat_date > to_date(v_from,get.txt('date_year'))
AND     so.stat_date <= to_date(v_to,get.txt('date_year'))+1
AND     so.service_fk = p_service_pk
;
--, page_type pt
--AND     so.page_type_fk = pt.page_type_pk
--;

BEGIN

IF (login.timeout('stat.res_page_serv') > 0 ) THEN

   v_user_type_pk := get.u_type;
   IF ( v_user_type_pk > -3 ) THEN
      html.jump_to('http://www.in4mant.com/cgi-bin/main_page.startup',2,'Not a admin user!');
   ELSE
      IF ( v_logo IS NULL ) THEN
         v_logo := 'no_logo.gif';
      END IF;

      v_color := get.value( 'c_'|| get.serv_name ||'_mmb' );

      html.b_adm_page('Statistics for services');

      html.b_table;
      IF ( p_from IS NOT NULL ) THEN
         v_from := p_from;
      ELSE
         v_from := TO_CHAR(SYSDATE,REPLACE(get.txt('date_year'),'DD','::'));
                        v_from := REPLACE(v_from ,'::','01');
      END IF;
      IF ( p_to IS NOT NULL ) THEN
         v_to := p_to;
      ELSE
         v_to := TO_CHAR(LAST_DAY(SYSDATE),get.txt('date_year'));
      END IF;
      html.b_form('stat.res_page_serv');
      htp.p('<tr><td colspan="3">');
      html.b_table;
      htp.p('<tr nowrap><td>'||get.txt('period')||':
            <input type="text" size="10" name="p_from" value="'||v_from||'"
            onSelect=javascript:openWin(''date_time.startup?p_command=date&p_variable=p_from'',300,200)
            onClick=javascript:openWin(''date_time.startup?p_command=date&p_variable=p_from'',300,200)>'||
            html.popup( get.txt('from_date'),'date_time.startup?p_command=date&p_variable=p_from','300', '200')||'
            </td><td>
              <input type="text" size="10" name="p_to" value="'||v_to||'"
            onSelect=javascript:openWin(''date_time.startup?p_command=date&p_variable=p_to'',300,200)
            onClick=javascript:openWin(''date_time.startup?p_command=date&p_variable=p_to'',300,200)>'||
            html.popup( get.txt('to_date'),'date_time.startup?p_command=date&p_variable=p_to','300', '200')||'
            </td><td> ');
      html.select_service( p_service_pk );
      htp.p('</td><td> ');
      html.submit_link( get.txt('show_statistics') );
      htp.p('</td></tr>');
      html.e_table;
      htp.p('</td></tr>');
      html.e_form;

      htp.p('<tr bgcolor="'||v_color||'"><td colspan="3" align="center"><b>'||get.txt('total_org_page')||':</b></td></tr>
            <tr><td align="right" colspan="3"><b>'||get.txt('volume')||':</b></td></tr>');


                -------------------------------------------------
                --                   Dokumenter
                -------------------------------------------------

                SELECT  SUM(so.volume)
                INTO    v_volume
                FROM    stat_org so, document d, org_service os
                WHERE   os.organization_fk = d.organization_fk
                AND     so.document_fk IS NOT NULL
                AND     so.document_fk = d.document_pk
                AND     so.stat_date > to_date(v_from,get.txt('date_year'))
                AND     so.stat_date <= to_date(v_to,get.txt('date_year'))+1
                AND     os.service_fk = p_service_pk
                ;
                   htp.p('<tr><td colspan="2" align="left">'||get.txt('document')||'</td>
                     <td align="right">'||v_volume||'</td></tr>');
                -------------------------------------------------
                --                   Calendar
                -------------------------------------------------

                SELECT  sum(so.volume)
                INTO    v_volume
                FROM    stat_org so, calendar c, org_service os
                WHERE   so.calendar_fk IS NOT NULL
                AND     so.stat_date > to_date(v_from,get.txt('date_year'))
                AND     so.stat_date <= to_date(v_to,get.txt('date_year'))+1
                AND     so.calendar_fk = c.calendar_pk
                AND     c.organization_fk = os.organization_fk
                AND     os.service_fk = p_service_pk
                ;

                   htp.p('<tr><td colspan="2" align="left">'||get.txt('calendar')||'</td>
                     <td align="right">'||v_volume||'</td></tr>');
                -------------------------------------------------
                --                   Album
                -------------------------------------------------

                SELECT  SUM(so.volume)
                INTO    v_volume
                FROM    stat_org so, album a, org_service os
                WHERE   so.album_fk IS NOT NULL
                AND     so.stat_date > to_date(v_from,get.txt('date_year'))
                AND     so.stat_date <= to_date(v_to,get.txt('date_year'))+1
                AND     so.album_fk = a.album_pk
                AND     a.organization_fk = os.organization_fk
                AND     os.service_fk = p_service_pk
                ;

                   htp.p('<tr><td colspan="2" align="left">'||get.txt('album')||'</td>
                     <td align="right">'||v_volume||'</td></tr>');
                -------------------------------------------------
                --                   Pictures
                -------------------------------------------------

                SELECT  SUM(volume)
                INTO    v_volume
                FROM    stat_org
                WHERE   picture_fk IS NOT NULL
                AND     stat_date > to_date(v_from,get.txt('date_year'))
                AND     stat_date <= to_date(v_to,get.txt('date_year'))+1
                AND     SERVICE_FK = p_service_pk
                ;

                htp.p('<tr><td colspan="2" align="left">'||get.txt('picture')||'</td>
                <td align="right">'||v_volume||'</td></tr>');
                -------------------------------------------------
                --                   Forum
                -------------------------------------------------

                SELECT  SUM(volume)
                INTO    v_volume
                FROM    stat_org
                WHERE   fora_type_fk IS NOT NULL
                AND     ORGANIZATION_FK IS NOT NULL
                AND     stat_date > to_date(v_from,get.txt('date_year'))
                AND     stat_date <= to_date(v_to,get.txt('date_year'))+1
                AND     SERVICE_FK = p_service_pk
                ;

                htp.p('<tr><td colspan="2" align="left">'||get.txt('fora')||'</td>
                <td align="right">'||v_volume||'</td></tr>');
                -------------------------------------------------
                --            Organization_pages
                -------------------------------------------------
                SELECT  sum(volume)
                INTO    v_volume
                FROM    stat_org
                WHERE   ORGANIZATION_FK IS NOT NULL
                AND     DOCUMENT_FK IS NULL
                AND     CALENDAR_FK IS NULL
                AND     FORA_TYPE_FK IS NULL
                AND     ALBUM_FK IS NULL
                AND     PICTURE_FK IS NULL
                AND     stat_date > to_date(v_from,get.txt('date_year'))
                AND     stat_date <= to_date(v_to,get.txt('date_year'))+1
                AND     SERVICE_FK = p_service_pk
                ;

                htp.p('<tr><td colspan="2" align="left">'||get.txt('org_page')||'</td>
                <td align="right">'||v_volume||'</td></tr>');


                -------------------------------------------------
                --                  Service pages
                -------------------------------------------------
               htp.p('
                <tr bgcolor="'||v_color||'"><td colspan="3" align="center"><b>'||get.txt('serv_pages')||':</b></td></tr>
                <tr><td colspan="3" align="right"><b>'||get.txt('volume')||':</b></td></tr>');
                OPEN get_page_type;
                LOOP
                        FETCH get_page_type INTO v_name, v_page_type_pk;
                        EXIT WHEN get_page_type%NOTFOUND;

                        SELECT  SUM(volume)
                        INTO    v_volume
                        FROM    stat_org
                        WHERE   page_type_fk = v_page_type_pk
                        AND     organization_fk is null
                        AND     stat_date > to_date(v_from,get.txt('date_year'))
                        AND     stat_date <= to_date(v_to,get.txt('date_year'))+1
                        AND     SERVICE_FK = p_service_pk
                        ;

                        htp.p('<tr><td colspan="2">'||get.txt(v_name)||'</td>
                        <td align="right">'||v_volume||'</td></tr>');
                END LOOP;
                CLOSE get_page_type;

                -------------------------------------------------
                --                  Total hits
                -------------------------------------------------
                htp.p('
                <tr bgcolor="'||v_color||'"><td colspan="3" align="center"><b>'||get.txt('total')||':</b></td></tr>
                <tr><td align="left" colspan="2">&nbsp;</td>
                <td align="right"><b>'||get.txt('volume')||':</b></td></tr>');
                OPEN get_tot;
                LOOP
                        FETCH get_tot INTO v_volume;
                        EXIT WHEN get_tot%NOTFOUND;
                        htp.p('<tr><td colspan="2">'||get.txt('total_pages')||'</td>
                        <td align="right">'||v_volume||'</td></tr>');
                END LOOP;
                CLOSE get_tot;
                -------------------------------------------------
                --                  All service
                -------------------------------------------------
               htp.p('
                <tr bgcolor="'||v_color||'"><td colspan="3" align="center"><b>'||get.txt('all_serv_pages')||':</b></td></tr>
                <tr><td colspan="3" align="right"><b>'||get.txt('volume')||':</b></td></tr>');
                OPEN get_page_type_all;
                LOOP
                        FETCH get_page_type_all INTO v_name, v_page_type_pk;
                        EXIT WHEN get_page_type_all%NOTFOUND;

                        SELECT  SUM(volume)
                        INTO    v_volume
                        FROM    stat_org
                        WHERE   page_type_fk = v_page_type_pk
                        AND     stat_date > to_date(v_from,get.txt('date_year'))
                        AND     stat_date <= to_date(v_to,get.txt('date_year'))+1
                        ;

                        htp.p('<tr><td colspan="2">'||get.txt(v_name)||'</td>
                        <td align="right">'||v_volume||'</td></tr>');
                END LOOP;
                CLOSE get_page_type_all;

                html.e_table;

                html.e_page;
        END IF;
END IF;
END;
END res_page_serv;


---------------------------------------------------------------------
-- Name: update_mail_log
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 04.10.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE update_mail_log (p_mail_log_pk       IN mail_log.mail_log_pk%TYPE       DEFAULT NULL)
IS
BEGIN
DECLARE

v_end_time              varchar2(20)                           DEFAULT NULL;
v_file                  utl_file.file_type                      default NULL;
v_mail_dir              VARCHAR2(100)                           DEFAULT NULL;

BEGIN
--        v_mail_dir := get.value('mail_dir');
        v_mail_dir := '/utl_file/';
        v_file := utl_file.fopen( v_mail_dir, p_mail_log_pk||'_mail_log.txt' , 'r');
        utl_file.get_line( v_file, v_end_time );
/*
        UPDATE mail_log
        SET    end_time = to_date(v_end_time,'YYYYMMDDHH24MI'),
               send_status = 'OK'
        WHERE  mail_log_pk = p_mail_log_pk;
        COMMIT;
*/
        utl_file.fclose(v_file);
END;
END update_mail_log;

PROCEDURE rand
IS
BEGIN
DECLARE
        my_random_number        BINARY_INTEGER  DEFAULT NULL;
BEGIN
--      my_random_number := DBMS_RANDOM.RANDOM;
        html.b_page;
--      htp.p(my_random_number);
        html.e_page;
END;
END rand;


---------------------------------------------------------------------
---------------------------------------------------------------------
END;
/
show errors;
