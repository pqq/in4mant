CREATE OR REPLACE PACKAGE org_adm IS

PROCEDURE startup (             p_command       IN varchar2                             DEFAULT NULL,
                                p_name          IN organization.name%TYPE               DEFAULT NULL,
                                p_org_number    IN organization.org_number%TYPE         DEFAULT NULL,
                                p_address       IN organization.address%TYPE            DEFAULT NULL,
                                p_url           IN organization.url%TYPE                DEFAULT NULL,
                                p_email         IN organization.email%TYPE              DEFAULT NULL,
                                p_phone         IN organization.phone%TYPE              DEFAULT NULL,
                                p_fax           IN organization.fax%TYPE                DEFAULT NULL,
                                p_service_pk    IN service.service_pk%TYPE              DEFAULT NULL
                                );
PROCEDURE accepted (            p_organization_pk       IN organization.organization_pk%TYPE    DEFAULT NULL
                                );
PROCEDURE delete_org (          p_command               IN varchar2                             DEFAULT NULL,
                                p_organization_pk       IN organization.organization_pk%TYPE    DEFAULT NULL
                                );
PROCEDURE delete_all_pictures
				(
					p_organization_pk	in organization.organization_pk%type	default NULL
				);
PROCEDURE edit_info_name (      p_command               IN varchar2                             DEFAULT NULL,
                                p_name                  IN string_group.name%TYPE               DEFAULT NULL,
                                p_description           IN string_group.description%TYPE        DEFAULT NULL,
                                p_string_group_pk       IN string_group.string_group_pk%TYPE    DEFAULT NULL,
                                p_service_pk            IN info_name.service_fk%TYPE            DEFAULT NULL
                                );
PROCEDURE edit_document_type (  p_command               IN varchar2                             DEFAULT NULL,
                                p_name                  IN string_group.name%TYPE               DEFAULT NULL,
                                p_description           IN string_group.description%TYPE        DEFAULT NULL,
                                p_string_group_pk       IN string_group.string_group_pk%TYPE    DEFAULT NULL,
                                p_service_pk            IN document_type.service_fk%TYPE        DEFAULT NULL
                                );
PROCEDURE edit_currency (       p_command               IN VARCHAR2                             DEFAULT NULL,
                                p_currency_pk           IN currency.currency_pk%TYPE            DEFAULT NULL,
                                p_geography_pk          IN geography.geography_pk%TYPE          DEFAULT NULL,
                                p_geo_name              IN VARCHAR2                             DEFAULT NULL,
                                p_currency_name         IN currency.currency_name%TYPE          DEFAULT NULL,
                                p_currency_code         IN currency.currency_code%TYPE          DEFAULT NULL
                                );
PROCEDURE edit_membership (     p_command               IN VARCHAR2                             DEFAULT NULL,
                                p_membership_pk         IN membership.membership_pk%TYPE        DEFAULT NULL,
                                p_service_pk            IN service.service_pk%TYPE              DEFAULT NULL,
                                p_name                  IN string_group.name%TYPE               DEFAULT NULL,
                                p_description           IN string_group.description%TYPE        DEFAULT NULL,
                                p_string_group_pk       IN string_group.string_group_pk%TYPE    DEFAULT NULL,
                                p_price                 IN membership.price%TYPE                DEFAULT NULL,
                                p_currency_pk           IN currency.currency_pk%TYPE            DEFAULT NULL
                                );
PROCEDURE select_currency (     p_currency_pk           in currency.currency_pk%TYPE            DEFAULT NULL );
PROCEDURE delete_logo ( 	p_organization_pk       IN fora_type.organization_fk%TYPE       DEFAULT NULL );

END;
/
CREATE OR REPLACE PACKAGE BODY org_adm IS
---------------------------------------------------------------------
---------------------------------------------------------------------
-- Name: startup
-- Type: procedure
-- What: Genererer opp hovedsiden for admin av en organisasjon
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE startup (     p_command       IN varchar2                             DEFAULT NULL,
                        p_name          IN organization.name%TYPE               DEFAULT NULL,
                        p_org_number    IN organization.org_number%TYPE         DEFAULT NULL,
                        p_address       IN organization.address%TYPE            DEFAULT NULL,
                        p_url           IN organization.url%TYPE                DEFAULT NULL,
                        p_email         IN organization.email%TYPE              DEFAULT NULL,
                        p_phone         IN organization.phone%TYPE              DEFAULT NULL,
                        p_fax           IN organization.fax%TYPE                DEFAULT NULL,
                        p_service_pk    IN service.service_pk%TYPE              DEFAULT NULL
                         )
IS
BEGIN
DECLARE

v_user_pk               usr.user_pk%TYPE                        DEFAULT NULL;
v_organization_pk       organization.organization_pk%TYPE       DEFAULT NULL;
v_name                  organization.name%TYPE                  DEFAULT NULL;
v_org_number            organization.org_number%TYPE            DEFAULT NULL;
v_address               organization.address%TYPE               DEFAULT NULL;
v_url                   organization.url%TYPE                   DEFAULT NULL;
v_email                 organization.email%TYPE                 DEFAULT NULL;
v_phone                 organization.phone%TYPE                 DEFAULT NULL;
v_fax                   organization.fax%TYPE                   DEFAULT NULL;
v_logo                  organization.logo%TYPE                  DEFAULT NULL;
v_name_sg_fk            service.name_sg_fk%TYPE                 DEFAULT NULL;
v_count                 NUMBER                                  DEFAULT NULL;

BEGIN

IF (    login.timeout('org_adm.startup') > 0 AND
        login.right('org_adm') > 0 ) THEN

        v_organization_pk := get.oid;

        IF ( v_organization_pk > 0 ) THEN
                SELECT count(*)
                INTO   v_count
                FROM   org_service
                WHERE  organization_fk = v_organization_pk
                ;
        END IF;
        IF ( v_organization_pk IS NULL ) THEN
                SELECT organization_seq.nextval
                INTO v_organization_pk
                FROM dual;
                INSERT INTO organization ( organization_pk )
                VALUES ( v_organization_pk );
                v_user_pk := get.uid;
                INSERT INTO client_user ( user_fk, organization_fk )
                VALUES ( v_user_pk, v_organization_pk );
                COMMIT;
        ELSIF ( p_command = 'update' ) THEN
                UPDATE  organization
                SET     name=trans(p_name),
                        org_number=p_org_number,
                        address=SUBSTR(trans(p_address),1,500),
                        url=trans(p_url),
                        email=trans(p_email),
                        phone=trans(p_phone),
                        fax=trans(p_fax),
                        membership_fk = 2
                WHERE  organization_pk = v_organization_pk
                ;
                COMMIT;
                IF ( v_count = 0 ) THEN
                        INSERT INTO org_service ( organization_fk, service_fk )
                        VALUES ( v_organization_pk, p_service_pk )
                        ;
                        COMMIT;
                END IF;
        END IF;

        IF ( p_service_pk IS NOT NULL ) THEN
                SELECT  name_sg_fk
                INTO    v_name_sg_fk
                FROM    service
                WHERE   service_pk = p_service_pk
                ;
        ELSIF ( p_command IS NOT NULL OR v_user_pk IS NULL ) THEN
                SELECT  s.name_sg_fk
                INTO    v_name_sg_fk
                FROM    service s, org_service os
                WHERE   os.organization_fk = v_organization_pk
                AND     os.service_fk = s.service_pk
                ;
        END IF;

        SELECT  name, org_number, address, url,
                email, phone, fax, logo
        INTO    v_name, v_org_number,
                v_address, v_url, v_email,
                v_phone, v_fax, v_logo
        FROM    organization
        WHERE   organization_pk = v_organization_pk
        ;
        html.b_page;
        html.b_box( get.txt('reg_organization') );
        IF ( p_command = 'update') THEN
                html.b_table;
                htp.p('<tr><td>'|| get.txt('org_name') ||':</td><td>'||v_name||'</td></tr>
                <tr><td>'|| get.txt('org_number') ||':</td><td>'||v_org_number||'</td></tr>
                <tr><td>'|| get.txt('address') ||':</td><td>'||v_address||'</td></tr>
                <tr><td>'|| get.txt('url') ||':</td><td>'||v_url||'</td></tr>
                <tr><td>'|| get.txt('email_address') ||':</td><td>'||v_email||'</td></tr>
                <tr><td>'|| get.txt('phone') ||':</td><td>'||v_phone||'</td></tr>
                <tr><td>'|| get.txt('fax') ||':</td><td>'||v_fax||'</td></tr>
                <tr><td>'|| get.txt('service') ||':</td><td>'||get.text(v_name_sg_fk)||'</td></tr>
                <tr><td colspan="2">'); html.button_link( get.txt('update'), 'org_adm.startup' ); htp.p('</td></tr>');
        ELSE
                html.b_form('org_adm.startup');
                html.b_table;
                htp.p('<input type="hidden" name="p_command" value="update">
                <tr><td>'|| get.txt('org_name') ||':</td><td>
                '|| html.text('p_name', '30', '30', v_name) ||'
                </td></tr>
                <tr><td>'|| get.txt('org_number') ||':</td><td>
                '|| html.text('p_org_number', '30', '30', v_org_number) ||'
                </td></tr>
                <tr><td>'|| get.txt('address') ||':</td><td>
                <textarea name="p_address" rows="5" cols="30" wrap="virtual">'||v_address||'</textarea>
                </td></tr>
                <tr><td>'|| get.txt('url') ||':</td><td>
                '|| html.text('p_url', '30', '30', v_url) ||'
                </td></tr>
                <tr><td>'|| get.txt('email_address') ||':</td><td>
                '|| html.text('p_email', '30', '30', v_email) ||'
                </td></tr>
                <tr><td>'|| get.txt('phone') ||':</td><td>
                '|| html.text('p_phone', '30', '30', v_phone) ||'
                </td></tr>
                <tr><td>'|| get.txt('fax') ||':</td><td>
                '|| html.text('p_fax', '30', '30', v_fax) ||'
                </td></tr>
                <tr><td>'|| get.txt('service') ||':</td><td>');
                IF ( v_count > 0 ) THEN
                        htp.p(get.text(v_name_sg_fk));
                ELSE
                        html.select_service;
                END IF;
                htp.p('</td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('register') ); htp.p('</td></tr>');
                html.e_form;
        END IF;
        html.e_table;
        html.e_box;
        html.e_page_2;
END IF;
EXCEPTION
WHEN NO_DATA_FOUND
THEN
htp.p('No data found! org_admin.sql -> startup');
END;
END; -- prosedyren startup

---------------------------------------------------------------------
-- Name: accepted
-- Type: procedure
-- What: Genererer en side over nye organisasjoner som skal aksepteres
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE accepted (    p_organization_pk IN organization.organization_pk%TYPE  DEFAULT NULL )
IS
BEGIN
DECLARE

v_organization_pk       organization.organization_pk%TYPE       DEFAULT NULL;
v_name                  organization.name%TYPE                  DEFAULT NULL;
v_number                NUMBER                                  DEFAULT NULL;
v_message               VARCHAR2(4000)                          DEFAULT NULL;
v_email_dir             VARCHAR2(100)                           DEFAULT NULL;
v_file                  utl_file.file_type                      DEFAULT NULL;
v_email                 organization.email%TYPE                 DEFAULT NULL;
v_name_sg_pk            membership.name_sg_fk%TYPE              DEFAULT NULL;
v_user_pk		client_user.user_fk%TYPE		DEFAULT NULL;

CURSOR fetch_entry IS
SELECT organization_pk, name
FROM   organization
WHERE  accepted IS NULL
ORDER BY name
;

CURSOR fetch_entry2 IS
SELECT m.name_sg_fk
FROM   organization o, membership m
WHERE  o.organization_pk = v_organization_pk
AND    o.membership_fk = m.membership_pk
;

BEGIN

IF (    login.timeout('org_adm.accepted') > 0 AND
        login.right('org_adm') > 0 ) THEN
        IF ( p_organization_pk IS NOT NULL ) THEN
                UPDATE organization
                SET    accepted=1
                WHERE  organization_pk=p_organization_pk
                ;
                SELECT  user_fk
                INTO	v_user_pk
                FROM    client_user
                WHERE   organization_fk=p_organization_pk
                ;
                UPDATE usr
                SET    user_type_fk=-2
                WHERE  user_pk = v_user_pk
                ;
                COMMIT;
                SELECT  name, email
                INTO    v_name, v_email
                FROM    organization
                WHERE   organization_pk=p_organization_pk
                ;
                IF ( v_email IS NOT NULL ) THEN
                        SELECT  email_script_seq.NEXTVAL
                        INTO    v_number
                        FROM    DUAL;
                        v_message := get.txt('email_accepted');
                        v_message := REPLACE ( v_message, '[org_name]', v_name );
                        v_message := REPLACE ( v_message, '[org_link]', get.olink(p_organization_pk, 'org_page.main?p_organization_pk='||p_organization_pk ) );
                        v_email_dir := get.value('email_dir');
                        v_file := utl_file.fopen( v_email_dir, v_number||'_email-list.txt' , 'w');
                        utl_file.put_line( v_file, v_email );
                        utl_file.fclose( v_file );
                        v_file := utl_file.fopen( v_email_dir, v_number||'_message.txt' , 'w');
                        utl_file.put_line( v_file, v_message );
                        utl_file.fclose(v_file);
                END IF;
		SELECT	email
		INTO 	v_email
		FROM 	user_details
		WHERE	user_fk = v_user_pk
		;
                IF ( v_email IS NOT NULL ) THEN
                        SELECT  email_script_seq.NEXTVAL
                        INTO    v_number
                        FROM    DUAL;
                        v_message := get.txt('email_accepted');
                        v_message := REPLACE ( v_message, '[org_name]', v_name );
                        v_message := REPLACE ( v_message, '[org_link]', get.olink(p_organization_pk, 'org_page.main?p_organization_pk='||p_organization_pk ) );
                        v_email_dir := get.value('email_dir');
                        v_file := utl_file.fopen( v_email_dir, v_number||'_email-list.txt' , 'w');
                        utl_file.put_line( v_file, v_email );
                        utl_file.fclose( v_file );
                        v_file := utl_file.fopen( v_email_dir, v_number||'_message.txt' , 'w');
                        utl_file.put_line( v_file, v_message );
                        utl_file.fclose(v_file);
                END IF;
        END IF;
        html.b_adm_page;
        html.b_box( get.txt('accept_organization') );
        html.b_table;
        OPEN fetch_entry;
        LOOP
                FETCH fetch_entry INTO v_organization_pk, v_name;
                exit when fetch_entry%NOTFOUND;
	        OPEN fetch_entry2;
                FETCH fetch_entry2 INTO v_name_sg_pk;
		CLOSE fetch_entry2;
                htp.p('<tr><td>'|| get.txt('org_name') ||':</td>
                <td><a href="org_adm.accepted?p_organization_pk='||v_organization_pk||'">'||v_name||'&nbsp;</a></td>
                <td>'|| get.txt('mebership_type') ||':</td><td>'|| get.text(v_name_sg_pk) ||'</td>
                <td><a href="org_adm.delete_org?p_organization_pk='||v_organization_pk||'">'||get.txt('delete')||'</a></td>
                <td><a href="'|| get.olink(v_organization_pk, 'org_page.main?p_organization_pk='||v_organization_pk) ||'" target="new">Look at organization</a></td>
                </tr>');
        END LOOP;
        CLOSE fetch_entry;
        html.e_table;
        html.e_box;
        html.e_page_2;
END IF;
EXCEPTION
WHEN NO_DATA_FOUND
THEN
htp.p('No data found! org_admin -> accepted');
END;
END; -- prosedyren accepted

---------------------------------------------------------------------
-- Name: delete_org
-- Type: procedure
-- What: Genererer en side over nye organisasjoner som skal aksepteres
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE delete_org (  p_command               IN varchar2                             DEFAULT NULL,
                        p_organization_pk       IN organization.organization_pk%TYPE    DEFAULT NULL )
IS
BEGIN
DECLARE

v_organization_pk       organization.organization_pk%TYPE       DEFAULT NULL;
v_name                  organization.name%TYPE                  DEFAULT NULL;
v_number                NUMBER                                  DEFAULT NULL;
v_message               VARCHAR2(4000)                          DEFAULT NULL;
v_email_dir             VARCHAR2(100)                           DEFAULT NULL;
v_file                  utl_file.file_type                      DEFAULT NULL;
v_email                 organization.email%TYPE                 DEFAULT NULL;
v_user_pk               usr.user_pk%TYPE                        DEFAULT NULL;

CURSOR  fetch_entry IS
SELECT  organization_pk, name
FROM    organization
ORDER BY name
;

CURSOR  fetch_org IS
SELECT  organization_pk, name
FROM    organization
WHERE   organization_pk = p_organization_pk
;

BEGIN

IF (    login.timeout('org_adm.delete_org') > 0 AND
        login.right('org_adm') > 0 ) THEN
        IF ( p_organization_pk IS NOT NULL AND p_command IS NULL) THEN
                html.b_adm_page;
                html.b_box( get.txt('delete_organization') );
                html.b_table;
                OPEN fetch_org;
                FETCH fetch_org INTO v_organization_pk, v_name;
                htp.p('<tr><td>'|| get.txt('delete_info') ||'
                <a href="org_adm.delete_org?p_organization_pk='||v_organization_pk||'&p_command=delete">
                '||v_name||'&nbsp;</a>?</td></tr>');
                CLOSE fetch_org;
                html.e_table;
                html.e_box;
                html.e_page_2;
        ELSIF ( p_organization_pk IS NOT NULL AND p_command IS NOT NULL ) THEN
                SELECT  name, email
                INTO    v_name, v_email
                FROM    organization
                WHERE   organization_pk=p_organization_pk
                ;
                IF ( v_email IS NOT NULL ) THEN
                        SELECT  email_script_seq.NEXTVAL
                        INTO    v_number
                        FROM    DUAL;
                        v_message := get.txt('email_delete');
                        v_message := REPLACE ( v_message, '[org_name]', v_name );
                        v_email_dir := get.value('email_dir');
                        v_file := utl_file.fopen( v_email_dir, v_number||'_email-list.txt' , 'w');
                        utl_file.put_line( v_file, v_email );
                        utl_file.fclose( v_file );
                        v_file := utl_file.fopen( v_email_dir, v_number||'_message.txt' , 'w');
                        utl_file.put_line( v_file, v_message );
                        utl_file.fclose(v_file);
                END IF;

		-- sletter alle bilder tilknyttet organisasjonen
		org_adm.delete_all_pictures(p_organization_pk);

                SELECT COUNT (*)
                INTO   v_user_pk
                FROM    client_user
                WHERE   organization_fk=p_organization_pk
                ;
                IF ( v_user_pk > 0 ) THEN
                   SELECT  user_fk
                   INTO    v_user_pk
                   FROM    client_user
                   WHERE   organization_fk=p_organization_pk
                   ;
                   DELETE
                   FROM    usr
                   WHERE   user_pk = v_user_pk
                   ;
                COMMIT;
                END IF;

                DELETE
                FROM    organization
                WHERE   organization_pk = p_organization_pk
                ;
                COMMIT;
		delete_logo ( p_organization_pk );
                html.b_page(NULL,3,'in4admin.startup');
                htp.p('<font color="red"><b>The organization has been deleted!</b></font>');
                html.e_page_2;
        ELSE
                html.b_adm_page;
                html.b_box( get.txt('delete_organization') );
                html.b_table;
                OPEN fetch_entry;
                LOOP
                        FETCH fetch_entry INTO v_organization_pk, v_name;
                        exit when fetch_entry%NOTFOUND;
                        htp.p('<tr><td>'|| get.txt('org_name') ||':</td>
                        <td><a href="org_adm.delete_org?p_organization_pk='||v_organization_pk||'">
                        '||v_name||'&nbsp;</a></td>
                        <td><a href="org_page.main?p_organization_pk='||v_organization_pk||'" target="new">
                        '|| get.txt('look_at_org') ||'</a></td>
                        </tr>');
                END LOOP;
                CLOSE fetch_entry;
                html.e_table;
                html.e_box;
                html.e_page_2;
        END IF;
END IF;
END;
END delete_org;




---------------------------------------------------------
-- Name:        delete_all_pictures
-- Type:        procedure
-- What:        deletes all pictures from db and OS related to an org
-- Author:      Frode Klevstul
-- Start date:  11.11.2000
-- Desc:	kun for administratorer
---------------------------------------------------------
PROCEDURE delete_all_pictures
	(
		p_organization_pk	in organization.organization_pk%type	default NULL
	)
IS
BEGIN
DECLARE

        CURSOR  select_albums
                (
                        v_organization_pk       in organization.organization_pk%type
                ) IS
        SELECT  album_pk
        FROM    album
        WHERE   organization_fk = v_organization_pk;

        CURSOR  select_pictures
                (
                        v_album_pk       in album.album_pk%type
                ) IS
        SELECT  picture_fk
        FROM    picture_in_album
        WHERE   album_fk = v_album_pk;


        v_album_pk              album.album_pk%type             default NULL;
	v_picture_pk		picture.picture_pk%type		default NULL;

        v_file          utl_file.file_type              default NULL;
        v_filename      varchar2(100)                   default NULL;
        v_directory     varchar2(100)                   default NULL;
        v_path          picture.path%type               default NULL;
        v_line          varchar2(200)                   default NULL;

BEGIN
if ( login.timeout('org_adm.delete_org')>0 AND login.right('org_adm')>0 ) then


        open select_albums(p_organization_pk);
        loop
                fetch select_albums into v_album_pk;
                exit when select_albums%NOTFOUND;

		open select_pictures(v_album_pk);
		loop
			fetch select_pictures into v_picture_pk;
			exit when select_pictures%NOTFOUND;

			-- --------------------------------
			-- write delete file (del from OS)
			--
		        -- oppretter en fil p� os'et som
		        -- inneholder pathen til alle filer
		        -- som skal slettes
			-- --------------------------------
		        SELECT  path INTO v_path
		        FROM    picture
		        WHERE   picture_pk = v_picture_pk;

		        SELECT  delete_from_os_file_seq.NEXTVAL
		        INTO    v_filename
		        FROM    dual;

		        v_filename      := v_filename ||'.txt';
		        v_directory     := get.value('delete_dir');
		        v_line          := get.value('photo_archive_os') ||'/'||v_path;

		        v_file := utl_file.fopen(v_directory, v_filename, 'w');
		        utl_file.put_line(v_file, v_line ||'.jpg');
		        utl_file.put_line(v_file, v_line ||'_thumb.jpg');
		        utl_file.fclose(v_file);

			-- -----------------------
			-- delete picture from db
			-- -----------------------
			DELETE FROM picture
			WHERE picture_pk = v_picture_pk;
			commit;
		end loop;
		close select_pictures;

		-- ----------------------
		-- delete album from db
		-- ----------------------
		DELETE FROM album
		WHERE album_pk = v_album_pk;
		commit;

        end loop;
        close select_albums;


end if;
END;
END delete_all_pictures;




---------------------------------------------------------------------
-- Name: edit_info_name
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 29.06.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE edit_info_name (      p_command               IN varchar2                             DEFAULT NULL,
                                p_name                  IN string_group.name%TYPE               DEFAULT NULL,
                                p_description           IN string_group.description%TYPE        DEFAULT NULL,
                                p_string_group_pk       IN string_group.string_group_pk%TYPE    DEFAULT NULL,
                                p_service_pk            IN info_name.service_fk%TYPE            DEFAULT NULL
                         )
IS
BEGIN
DECLARE

CURSOR  get_string IS
SELECT  sg.string_group_pk, sg.name, sg.description, ina.service_fk
FROM    info_name ina, string_group sg
WHERE   ina.name_sg_fk=sg.string_group_pk
ORDER BY sg.name
;

v_string_group_pk       string_group.string_group_pk%TYPE       DEFAULT NULL;
v_name                  string_group.name%TYPE                  DEFAULT NULL;
v_description           string_group.description%TYPE           DEFAULT NULL;
v_info_name_pk          info_name.info_name_pk%TYPE             DEFAULT NULL;
v_Service_pk            info_name.service_fk%TYPE               DEFAULT NULL;

BEGIN

IF (    login.timeout('org_adm.edit_info_name') > 0 AND
        login.right('org_adm') > 0 ) THEN

        IF ( p_command = 'update') THEN
                SELECT  sg.string_group_pk, sg.name, sg.description, service_fk
                INTO    v_string_group_pk, v_name, v_description, v_service_pk
                FROM    string_group sg, info_name ina
                WHERE   sg.string_group_pk=p_string_group_pk
                AND     ina.name_sg_fk=sg.string_group_pk
                ;
                html.b_adm_page;
                html.b_box( get.txt('info_name') );
                html.b_table;
                html.b_form('org_adm.edit_info_name');
                htp.p('<input type="hidden" name="p_command" value="edit">
                <input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                <tr><td>'|| get.txt('service') ||':</td><td>
                <td>');
                html.select_service(v_service_pk);
                htp.p('</td></tr>
                <tr><td>'|| get.txt('name') ||':</td><td>
                '|| html.text('p_name', '50', '50', v_name ) ||'
                </td></tr>
                <tr><td>'|| get.txt('description') ||':(maks 200 tegn)</td><td>
                <textarea name="p_description" rows="5" cols="70" wrap="virtual">'||v_description||'</textarea>
                </td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('update') ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        ELSIF ( p_command = 'edit') THEN
                UPDATE  string_group
                SET     name=trans(p_name),
                        description=trans(p_description)
                WHERE   string_group_pk=p_string_group_pk;
                commit;
                UPDATE  info_name
                SET     description=trans(p_name), service_fk=p_service_pk
                WHERE   name_sg_fk=p_string_group_pk;
                COMMIT;
                html.jump_to ( 'org_adm.edit_info_name');
        ELSIF ( p_command = 'delete') THEN
                IF ( p_name = 'yes') THEN
                        DELETE  FROM string_group
                        WHERE   string_group_pk=p_string_group_pk;
                        COMMIT;
                        html.jump_to ( 'org_adm.edit_info_name');
                ELSIF ( p_name IS NULL ) THEN
                        html.jump_to ( 'org_adm.edit_info_name');
                ELSE
                        html.b_adm_page;
                        html.b_box( get.txt('info_name') );
                        html.b_table;
                        html.b_form('org_adm.edit_info_name');
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="yes">
                        <input type="hidden" name="p_string_group_pk" value="'||p_string_group_pk||'">
                        <td>'|| get.txt('delete_info')||' "'||p_name||'" ?'); html.submit_link( get.txt('YES') ); htp.p('</td>');
                        html.e_form;
                        htp.p('<td>'); html.button_link( get.txt('NO'), 'org_adm.edit_info_name');htp.p('</td></tr>');
                        html.e_table;
                        html.e_box;
                        html.e_page_2;
                END IF;
        ELSIF ( p_command = 'new') THEN
                html.b_adm_page;
                html.b_box( get.txt('info_name') );
                html.b_table;
                html.b_form('org_adm.edit_info_name','form_insert');
                htp.p('<input type="hidden" name="p_command" value="insert">
                <tr><td>'|| get.txt('service') ||':</td><td>
                <td>');
                html.select_service;
                htp.p('</td></tr>
                <tr><td>'|| get.txt('name') ||':</td><td>
                '|| html.text('p_name', '50', '50', '' ) ||'
                </td></tr><tr><td>'|| get.txt('description') ||':(maks 200 tegn)</td><td>
                <textarea name="p_description" rows="5" cols="70" wrap="virtual"></textarea>
                </td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('insert'), 'form_insert' ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        ELSIF ( p_command = 'insert') THEN
                SELECT string_group_seq.NEXTVAL
                INTO v_string_group_pk
                FROM dual;
                SELECT info_name_seq.NEXTVAL
                INTO v_info_name_pk
                FROM dual;
                INSERT INTO string_group
                ( string_group_pk, name, description )
                VALUES
                ( v_string_group_pk, trans(p_name),
                SUBSTR(trans(p_description),1,200) );
                COMMIT;
                INSERT INTO info_name
                ( info_name_pk, name_sg_fk, description, service_fk )
                VALUES
                ( v_info_name_pk, v_string_group_pk,
                SUBSTR(trans(p_description),1,150), p_service_pk);
                COMMIT;
                html.jump_to ( 'org_adm.edit_info_name' );
        ELSE
                html.b_adm_page;
                html.b_box( get.txt('info_name') );
                html.b_table;
                OPEN get_string;
                LOOP
                        fetch get_string INTO v_string_group_pk, v_name, v_description, v_service_pk;
                        exit when get_string%NOTFOUND;
                        html.b_form('org_adm.edit_info_name','form'|| v_string_group_pk);
                        htp.p('<input type="hidden" name="p_command" value="update">
                        <input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                        <tr><td valing="top">'||v_name||'</td>
                        <td>');
                        html.select_service(v_service_pk);
                        htp.p('</td><td>'); html.submit_link( get.txt('edit'), 'form'||v_string_group_pk ); htp.p('</td>');
                        html.e_form;
                        html.b_form('org_adm.edit_info_name','delete'|| v_string_group_pk);
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="'||v_name||'">
                        <input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                        <td>'); html.submit_link( get.txt('delete'), 'delete'||v_string_group_pk ); htp.p('</td></tr>');
                        html.e_form;
                END LOOP;
                close get_string;
                html.b_form('org_adm.edit_info_name','form_new');
                htp.p('<input type="hidden" name="p_command" value="new">
                <td>'); html.submit_link( get.txt('new'), 'form_new'); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        END IF;
END IF;
EXCEPTION
WHEN NO_DATA_FOUND
THEN
htp.p('No data found! org_admin.sql -> edit_info_name');
END;
END edit_info_name;

---------------------------------------------------------------------
-- Name: edit_document_type
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 29.06.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE edit_document_type (  p_command               IN varchar2                             DEFAULT NULL,
                                p_name                  IN string_group.name%TYPE               DEFAULT NULL,
                                p_description           IN string_group.description%TYPE        DEFAULT NULL,
                                p_string_group_pk       IN string_group.string_group_pk%TYPE    DEFAULT NULL,
                                p_service_pk            IN document_type.service_fk%TYPE        DEFAULT NULL
                         )
IS
BEGIN
DECLARE

CURSOR  get_string IS
SELECT  sg.string_group_pk, sg.name, sg.description, dt.service_fk
FROM    document_type dt, string_group sg
WHERE   dt.name_sg_fk=sg.string_group_pk
ORDER BY sg.name
;

v_string_group_pk       string_group.string_group_pk%TYPE       DEFAULT NULL;
v_name                  string_group.name%TYPE                  DEFAULT NULL;
v_description           string_group.description%TYPE           DEFAULT NULL;
v_document_type_pk      document_type.document_type_pk%TYPE             DEFAULT NULL;
v_service_pk            document_type.service_fk%TYPE           DEFAULT NULL;

BEGIN

IF (    login.timeout('org_adm.edit_document_type') > 0 AND
        login.right('org_adm') > 0 ) THEN

        IF ( p_command = 'update') THEN
                SELECT  sg.string_group_pk, sg.name, sg.description, dt.service_fk
                INTO    v_string_group_pk, v_name, v_description, v_service_pk
                FROM    string_group sg, document_type dt
                WHERE   sg.string_group_pk=p_string_group_pk
                AND     sg.string_group_pk=dt.name_sg_fk
                ;
                html.b_adm_page;
                html.b_box( get.txt('document_type') );
                html.b_table;
                html.b_form('org_adm.edit_document_type');
                htp.p('<input type="hidden" name="p_command" value="edit">
                <input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                <tr><td>'|| get.txt('select_service') ||':</td><td>');
                html.select_service( v_service_pk );
                htp.p('</td></tr>
                <tr><td>'|| get.txt('name') ||':</td><td>
                '|| html.text('p_name', '50', '50', v_name ) ||'
                </td></tr>
                <tr><td>'|| get.txt('description') ||':(maks 200 tegn)</td><td>
                <textarea name="p_description" rows="5" cols="70" wrap="virtual">'||v_description||'</textarea>
                </td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('update') ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        ELSIF ( p_command = 'edit') THEN
                UPDATE  string_group
                SET     name=trans(p_name),
                        description=trans(p_description)
                WHERE   string_group_pk=p_string_group_pk;
                UPDATE  document_type
                SET     description=trans(p_name),
                        service_fk = p_service_pk
                WHERE   name_sg_fk=p_string_group_pk;
                COMMIT;
                html.jump_to ( 'org_adm.edit_document_type');
        ELSIF ( p_command = 'delete') THEN
                IF ( p_name = 'yes') THEN
                        DELETE  FROM string_group
                        WHERE   string_group_pk=p_string_group_pk;
                        COMMIT;
                        html.jump_to ( 'org_adm.edit_document_type');
                ELSIF ( p_name IS NULL ) THEN
                        html.jump_to ( 'org_adm.edit_document_type');
                ELSE
                        html.b_adm_page;
                        html.b_box( get.txt('document_type') );
                        html.b_table;
                        html.b_form('org_adm.edit_document_type');
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="yes">
                        <input type="hidden" name="p_string_group_pk" value="'||p_string_group_pk||'">
                        <td>'|| get.txt('delete_info') ||' "'||trans(p_name)||'" ?'); html.submit_link( get.txt('YES') ); htp.p('</td>');
                        html.e_form;
                        htp.p('<td>'); html.button_link( get.txt('NO'), 'org_adm.edit_document_type');htp.p('</td></tr>');
                        html.e_table;
                        html.e_box;
                        html.e_page_2;
                END IF;
        ELSIF ( p_command = 'new') THEN
                html.b_adm_page;
                html.b_box( get.txt('document_type') );
                html.b_table;
                html.b_form('org_adm.edit_document_type','form_insert');
                htp.p('<input type="hidden" name="p_command" value="insert">
                <tr><td>'|| get.txt('select_service') ||':</td><td>');
                html.select_service( get.serv );
                htp.p('</td></tr>
                <tr><td>'|| get.txt('name') ||':</td><td>
                '|| html.text('p_name', '50', '50', '' ) ||'
                </td></tr>
                <tr><td>'|| get.txt('description') ||':(maks 200 tegn)</td><td>
                <textarea name="p_description" rows="5" cols="70" wrap="virtual"></textarea>
                </td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('insert'), 'form_insert' ); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        ELSIF ( p_command = 'insert') THEN
                SELECT string_group_seq.NEXTVAL
                INTO v_string_group_pk
                FROM dual;
                SELECT document_type_seq.NEXTVAL
                INTO v_document_type_pk
                FROM dual;
                INSERT INTO string_group
                ( string_group_pk, name, description )
                VALUES
                ( v_string_group_pk, trans(p_name),
                SUBSTR(trans(p_description),1,200) );
                COMMIT;
                INSERT INTO document_type
                ( document_type_pk, name_sg_fk, description, service_fk )
                VALUES
                ( v_document_type_pk, v_string_group_pk,
                SUBSTR(trans(p_description),1,150), p_service_pk );
                COMMIT;
                html.jump_to ( 'org_adm.edit_document_type' );
        ELSE
                html.b_adm_page;
                html.b_box( get.txt('document_type') );
                html.b_table;
                OPEN get_string;
                LOOP
                        fetch get_string INTO v_string_group_pk, v_name, v_description, v_service_pk;
                        exit when get_string%NOTFOUND;
                        html.b_form('org_adm.edit_document_type','form'|| v_string_group_pk);
                        htp.p('<input type="hidden" name="p_command" value="update">
                        <input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                        <tr><td>'|| get.txt('document_type') ||':</td><td>'||v_name||'</td>
                        <td>'); html.submit_link( get.txt('edit'), 'form'||v_string_group_pk ); htp.p('</td>');
                        html.e_form;
                        html.b_form('org_adm.edit_document_type','delete'|| v_string_group_pk);
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="'||v_name||'">
                        <input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                        <td>'); html.submit_link( get.txt('delete'), 'delete'||v_string_group_pk ); htp.p('</td></tr>');
                        html.e_form;
                END LOOP;
                close get_string;
                html.b_form('org_adm.edit_document_type','form_new');
                htp.p('<input type="hidden" name="p_command" value="new">
                <td>'); html.submit_link( get.txt('new'), 'form_new'); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        END IF;
END IF;
EXCEPTION
WHEN NO_DATA_FOUND
THEN
htp.p('No data found! org_admin.sql -> edit_document_type');
END;
END edit_document_type;

---------------------------------------------------------------------
-- Name: edit_currency
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 20.10.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE edit_currency (       p_command               IN varchar2                             DEFAULT NULL,
                                p_currency_pk           IN currency.currency_pk%TYPE            DEFAULT NULL,
                                p_geography_pk          IN geography.geography_pk%TYPE          DEFAULT NULL,
                                p_geo_name              IN VARCHAR2                             DEFAULT NULL,
                                p_currency_name         IN currency.currency_name%TYPE          DEFAULT NULL,
                                p_currency_code         IN currency.currency_code%TYPE          DEFAULT NULL
                                )
IS
BEGIN
DECLARE

CURSOR  get_cur IS
SELECT  currency_pk, geography_fk, currency_name, currency_code
FROM    currency
ORDER BY currency_code
;

v_currency_pk           currency.currency_pk%TYPE            DEFAULT NULL;
v_geography_pk          geography.geography_pk%TYPE          DEFAULT NULL;
v_currency_name         currency.currency_name%TYPE          DEFAULT NULL;
v_currency_code         currency.currency_code%TYPE          DEFAULT NULL;

BEGIN

IF (    login.timeout('org_adm.edit_currency') > 0 AND
        login.right('org_adm') > 0 ) THEN

        IF ( p_command = 'update') THEN
                SELECT  currency_pk, geography_fk, currency_name, currency_code
                INTO    v_currency_pk, v_geography_pk, v_currency_name, v_currency_code
                FROM    currency
                WHERE   currency_pk = p_currency_pk
                ;

                html.b_adm_page;
                html.b_box( get.txt('edit_currency') );
                html.b_table;
                html.b_form('org_adm.edit_currency');
                htp.p('<input type="hidden" name="p_command" value="edit">
                <input type="hidden" name="p_currency_pk" value="'||v_currency_pk||'">
                <tr><td>'|| get.txt('currency_name') ||':</td><td>
                '|| html.text('p_currency_name', '50', '50', v_currency_name ) ||'
                </td></tr>
                <tr><td>'|| get.txt('currency_code') ||':</td><td>
                '|| html.text('p_currency_code', '20', '20', v_currency_code ) ||'
                </td></tr>
                <tr><td>'|| get.txt('geography_location') ||':</td><td>
                <input type="hidden" name="p_geography_pk" value="'|| v_geography_pk ||'">
                <input type="text" size="30" name="p_geo_name" value="'|| get.locn(v_geography_pk) ||'"
                onSelect=javascript:openWin(''select_geo.select_location?p_no_levels=4'',300,200)
                onClick=javascript:openWin(''select_geo.select_location?p_no_levels=4'',300,200)>
                '|| html.popup( get.txt('change'), 'select_geo.select_location?p_no_levels=2', '300', '200') ||'</td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('update')); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        ELSIF ( p_command = 'edit') THEN
                UPDATE  currency
                SET     currency_name = trans(p_currency_name),
                        currency_code = trans(p_currency_code),
                        geography_fk = p_geography_pk
                WHERE   currency_pk = p_currency_pk
                ;
                COMMIT;
                html.jump_to ( 'org_adm.edit_currency');
        ELSIF ( p_command = 'delete') THEN
                IF ( p_currency_code = 'yes') THEN
                        DELETE  FROM currency
                        WHERE   currency_pk=p_currency_pk
                        ;
                        COMMIT;
                        html.jump_to ( 'org_adm.edit_currency');
                ELSIF ( p_currency_code IS NULL ) THEN
                        html.jump_to ( 'org_adm.edit_currency');
                ELSE
                        html.b_adm_page;
                        html.b_box( get.txt('edit_currency') );
                        html.b_table;
                        html.b_form('org_adm.edit_currency');
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_currency_code" value="yes">
                        <input type="hidden" name="p_currency_pk" value="'||p_currency_pk||'">
                        <td>'|| get.txt('delete_info') ||' "'||trans(p_currency_code)||'" ?'); html.submit_link( get.txt('YES') ); htp.p('</td>');
                        html.e_form;
                        htp.p('<td>'); html.button_link( get.txt('NO'), 'org_adm.edit_currency');htp.p('</td></tr>');
                        html.e_table;
                        html.e_box;
                        html.e_page_2;
                END IF;
        ELSIF ( p_command = 'new') THEN
                html.b_adm_page;
                html.b_box( get.txt('edit_currency') );
                html.b_table;
                html.b_form('org_adm.edit_currency');
                htp.p('<input type="hidden" name="p_command" value="insert">
                <tr><td>'|| get.txt('currency_name') ||':</td><td>
                '|| html.text('p_currency_name', '50', '50', '' ) ||'
                </td></tr>
                <tr><td>'|| get.txt('currency_code') ||':</td><td>
                '|| html.text('p_currency_code', '20', '20', '' ) ||'
                </td></tr>
                <tr><td>'|| get.txt('geography_location') ||':</td><td>
                <input type="hidden" name="p_geography_pk" value="">
                <input type="text" size="30" name="p_geo_name" value=""
                onSelect=javascript:openWin(''select_geo.select_location?p_no_levels=4'',300,200)
                onClick=javascript:openWin(''select_geo.select_location?p_no_levels=4'',300,200)>
                '|| html.popup( get.txt('change'), 'select_geo.select_location?p_no_levels=2', '300', '200') ||'</td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('insert')); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        ELSIF ( p_command = 'insert') THEN
                SELECT currency_seq.NEXTVAL
                INTO v_currency_pk
                FROM dual;
                INSERT INTO currency
                ( currency_pk, geography_fk, currency_name, currency_code)
                VALUES
                ( v_currency_pk, p_geography_pk, trans(p_currency_name), trans(p_currency_code));
                COMMIT;
                html.jump_to ( 'org_adm.edit_currency' );
        ELSE
                html.b_adm_page;
                html.b_box( get.txt('edit_currency') );
                html.b_table;
                OPEN get_cur;
                LOOP
                        FETCH get_cur INTO v_currency_pk, v_geography_pk, v_currency_name, v_currency_code;
                        EXIT WHEN get_cur%NOTFOUND;
                        html.b_form('org_adm.edit_currency','form'|| v_currency_pk);
                        htp.p('<input type="hidden" name="p_command" value="update">
                        <input type="hidden" name="p_currency_pk" value="'||v_currency_pk||'">
                        <tr><td>'|| get.txt('currency_type') ||':</td><td>'||v_currency_code||'</td>
                        <td>'); html.submit_link( get.txt('edit'), 'form'||v_currency_pk ); htp.p('</td>');
                        html.e_form;
                        html.b_form('org_adm.edit_currency','delete'|| v_currency_pk);
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_currency_code" value="'||v_currency_code||'">
                        <input type="hidden" name="p_currency_pk" value="'||v_currency_pk||'">
                        <td>'); html.submit_link( get.txt('delete'), 'delete'||v_currency_pk ); htp.p('</td></tr>');
                        html.e_form;
                END LOOP;
                CLOSE get_cur;
                html.b_form('org_adm.edit_currency','form_new');
                htp.p('<input type="hidden" name="p_command" value="new">
                <td>'); html.submit_link( get.txt('new'), 'form_new'); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        END IF;

END IF;
EXCEPTION
WHEN NO_DATA_FOUND
THEN
htp.p('No data found! org_admin.sql -> edit_currency');
END;
END edit_currency;

---------------------------------------------------------------------
-- Name: edit_membership
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 20.10.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE edit_membership (     p_command               IN VARCHAR2                             DEFAULT NULL,
                                p_membership_pk         IN membership.membership_pk%TYPE        DEFAULT NULL,
                                p_service_pk            IN service.service_pk%TYPE              DEFAULT NULL,
                                p_name                  IN string_group.name%TYPE               DEFAULT NULL,
                                p_description           IN string_group.description%TYPE        DEFAULT NULL,
                                p_string_group_pk       IN string_group.string_group_pk%TYPE    DEFAULT NULL,
                                p_price                 IN membership.price%TYPE                DEFAULT NULL,
                                p_currency_pk           IN currency.currency_pk%TYPE            DEFAULT NULL
                                )
IS
BEGIN
DECLARE

CURSOR  get_string IS
SELECT  m.membership_pk, m.service_fk, sg.string_group_pk, sg.name, sg.description, m.price, m.currency_fk
FROM    membership m, string_group sg
WHERE   m.name_sg_fk=sg.string_group_pk
ORDER BY sg.name
;

v_string_group_pk       string_group.string_group_pk%TYPE       DEFAULT NULL;
v_name                  string_group.name%TYPE                  DEFAULT NULL;
v_description           string_group.description%TYPE           DEFAULT NULL;
v_membership_pk         membership.membership_pk%TYPE           DEFAULT NULL;
v_service_pk            service.service_pk%TYPE                 DEFAULT NULL;
v_price                 membership.price%TYPE                   DEFAULT NULL;
v_currency_pk           currency.currency_pk%TYPE               DEFAULT NULL;

BEGIN

IF (    login.timeout('org_adm.edit_membership') > 0 AND
        login.right('org_adm') > 0 ) THEN
        IF ( p_command = 'update') THEN

                SELECT  m.membership_pk, m.service_fk, sg.string_group_pk,
                        sg.name, sg.description, m.price, m.currency_fk
                INTO    v_membership_pk, v_service_pk, v_string_group_pk,
                        v_name, v_description, v_price, v_currency_pk
                FROM    membership m, string_group sg
                WHERE   m.membership_pk = p_membership_pk
                AND     m.name_sg_fk=sg.string_group_pk
                ;

                html.b_adm_page;
                html.b_box( get.txt('edit_currency') );
                html.b_table;
                html.b_form('org_adm.edit_membership');
                htp.p('<input type="hidden" name="p_command" value="edit">
                <input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                <input type="hidden" name="p_membership_pk" value="'||v_membership_pk||'">
                <tr><td>'|| get.txt('service') ||':</td><td>');
                html.select_service( v_service_pk );
                htp.p('</td></tr>
                <tr><td>'|| get.txt('currency') ||':</td><td>');
                org_adm.select_currency( v_currency_pk );
                htp.p('</td></tr>
                <tr><td>'|| get.txt('price') ||':</td><td>
                '|| html.text('p_price', '50', '50', v_price ) ||'
                </td></tr>
                <tr><td>'|| get.txt('name') ||':</td><td>
                '|| html.text('p_name', '50', '50', v_name ) ||'
                </td></tr>
                <tr><td>'|| get.txt('description') ||':(maks 200 tegn)</td><td>
                <textarea name="p_description" rows="5" cols="70" wrap="virtual">'|| v_description ||'</textarea>
                </td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('update')); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        ELSIF ( p_command = 'edit') THEN
                UPDATE  string_group
                SET     name=trans(p_name),
                        description=trans(p_description)
                WHERE   string_group_pk=p_string_group_pk;
                COMMIT;
                UPDATE  membership
                SET     service_fk = p_service_pk,
                        price = p_price,
                        currency_fk = p_currency_pk
                WHERE   membership_pk = p_membership_pk
                ;
                COMMIT;
                html.jump_to ( 'org_adm.edit_membership');
        ELSIF ( p_command = 'insert') THEN
                SELECT string_group_seq.NEXTVAL
                INTO v_string_group_pk
                FROM dual;
                SELECT membership_seq.NEXTVAL
                INTO v_membership_pk
                FROM dual;
                INSERT INTO string_group
                ( string_group_pk, name, description )
                VALUES
                ( v_string_group_pk, trans(p_name),
                SUBSTR(trans(p_description),1,200) );
                COMMIT;
                INSERT INTO membership
                ( membership_pk, service_fk, name_sg_fk, price, currency_fk )
                VALUES
                ( v_membership_pk, p_service_pk, v_string_group_pk,
                p_price, p_currency_pk );
                COMMIT;
                html.jump_to ( 'org_adm.edit_membership' );
        ELSIF ( p_command = 'new') THEN
                html.b_adm_page;
                html.b_box( get.txt('edit_membership') );
                html.b_table;
                html.b_form('org_adm.edit_membership');
                htp.p('<input type="hidden" name="p_command" value="insert">
                <tr><td>'|| get.txt('service') ||':</td><td>');
                html.select_service;
                htp.p('</td></tr>
                <tr><td>'|| get.txt('currency') ||':</td><td>');
                org_adm.select_currency;
                htp.p('</td></tr>
                <tr><td>'|| get.txt('price') ||':</td><td>
                '|| html.text('p_price', '50', '50', '' ) ||'
                </td></tr>
                <tr><td>'|| get.txt('name') ||':</td><td>
                '|| html.text('p_name', '50', '50', '' ) ||'
                </td></tr>
                <tr><td>'|| get.txt('description') ||':(maks 200 tegn)</td><td>
                <textarea name="p_description" rows="5" cols="70" wrap="virtual"></textarea>
                </td></tr>
                <tr><td colspan="2">'); html.submit_link( get.txt('insert')); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        ELSIF ( p_command = 'delete') THEN
                IF ( p_name = 'yes') THEN
                        DELETE  FROM string_group
                        WHERE   string_group_pk=p_string_group_pk;
                        COMMIT;
                        html.jump_to ( 'org_adm.edit_membership');
                ELSIF ( p_name IS NULL ) THEN
                        html.jump_to ( 'org_adm.edit_membership');
                ELSE
                        html.b_adm_page;
                        html.b_box( get.txt('edit_membership') );
                        html.b_table;
                        html.b_form('org_adm.edit_membership');
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="yes">
                        <input type="hidden" name="p_string_group_pk" value="'||p_string_group_pk||'">
                        <td>'|| get.txt('delete_info') ||' "'||trans(p_name)||'" ?'); html.submit_link( get.txt('YES') ); htp.p('</td>');
                        html.e_form;
                        htp.p('<td>'); html.button_link( get.txt('NO'), 'org_adm.edit_membership');htp.p('</td></tr>');
                        html.e_table;
                        html.e_box;
                        html.e_page_2;
                END IF;
        ELSE
                html.b_adm_page;
                html.b_box( get.txt('edit_membership') );
                html.b_table;
                OPEN get_string;
                LOOP
                        FETCH get_string INTO v_membership_pk, v_service_pk, v_string_group_pk, v_name, v_description, v_price, v_currency_pk;
                        EXIT WHEN get_string%NOTFOUND;
                        html.b_form('org_adm.edit_membership','form'|| v_membership_pk);
                        htp.p('<input type="hidden" name="p_command" value="update">
                        <input type="hidden" name="p_membership_pk" value="'||v_membership_pk||'">
                        <tr><td>'|| get.txt('membership_type') ||':</td><td>'||v_name||'</td>
                        <td>'); html.submit_link( get.txt('edit'), 'form'||v_membership_pk ); htp.p('</td>');
                        html.e_form;
                        html.b_form('org_adm.edit_membership','delete'|| v_membership_pk);
                        htp.p('<input type="hidden" name="p_command" value="delete">
                        <input type="hidden" name="p_name" value="'||v_name||'">
                        <input type="hidden" name="p_string_group_pk" value="'||v_string_group_pk||'">
                        <td>'); html.submit_link( get.txt('delete'), 'delete'||v_membership_pk ); htp.p('</td></tr>');
                        html.e_form;
                END LOOP;
                CLOSE get_string;
                html.b_form('org_adm.edit_membership','form_new');
                htp.p('<input type="hidden" name="p_command" value="new">
                <td>'); html.submit_link( get.txt('new'), 'form_new'); htp.p('</td></tr>');
                html.e_form;
                html.e_table;
                html.e_box;
                html.e_page_2;
        END IF;

END IF;
EXCEPTION
WHEN NO_DATA_FOUND
THEN
htp.p('No data found! org_admin.sql -> edit_membership');
END;
END edit_membership;


---------------------------------------------------------------------
-- Name: select_currency
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 20.10.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE select_currency (     p_currency_pk           in currency.currency_pk%TYPE            DEFAULT NULL )
IS
BEGIN
DECLARE

CURSOR  get_currency IS
SELECT  currency_pk, currency_code
FROM    currency
ORDER BY currency_code;

v_currency_pk           currency.currency_pk%TYPE               DEFAULT NULL;
v_currency_code         currency.currency_code%TYPE             DEFAULT NULL;

BEGIN

        htp.p('<select name="p_currency_pk">');

        OPEN get_currency;
        LOOP
                FETCH get_currency INTO v_currency_pk, v_currency_code;
                EXIT WHEN get_currency%NOTFOUND;
                IF (v_currency_pk = p_currency_pk) THEN
                        htp.p('<option value="'|| v_currency_pk ||'" selected>'|| v_currency_code ||'</option>');
                ELSE
                        htp.p('<option value="'|| v_currency_pk ||'">'|| v_currency_code ||'</option>');
                END IF;
        END LOOP;
        CLOSE get_currency;

        htp.p('</select>');

END;
END select_currency;

---------------------------------------------------------
-- Name:        delete_logo
-- Type:        procedure
-- What:        calls a script that deletes the file from the OS
-- Author:      Frode Klevstul
-- Start date:  10.08.2000
-- Desc:
---------------------------------------------------------

PROCEDURE delete_logo ( p_organization_pk       IN fora_type.organization_fk%TYPE       DEFAULT NULL )
IS
BEGIN
DECLARE

        v_file          utl_file.file_type              default NULL;
        v_filename      varchar2(100)                   default NULL;
        v_directory     varchar2(100)                   default NULL;
        v_line          varchar2(200)                   default NULL;

BEGIN
if (login.timeout('in4admin.startup') > 0 AND login.right('in4admin.main_page')>0 ) then

        SELECT  delete_from_os_file_seq.NEXTVAL
        INTO    v_filename
        FROM    dual;

        v_filename      := v_filename ||'.txt';
        v_directory     := get.value('delete_dir');
        v_line          := get.value('logo_dir_os') ||'/'||p_organization_pk;

        v_file := utl_file.fopen(v_directory, v_filename, 'w');
        utl_file.put_line(v_file, v_line ||'.jpg');
        utl_file.fclose(v_file);

end if;
END;
END delete_logo;

---------------------------------------------------------------------
---------------------------------------------------------------------
END;
/
