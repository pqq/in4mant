set define off
PROMPT *** package: tip ***

CREATE OR REPLACE PACKAGE tip IS
PROCEDURE someone (     p_email_address IN VARCHAR2                     DEFAULT NULL,
                        p_from          IN VARCHAR2                     DEFAULT NULL,
                        p_command       IN VARCHAR2                     DEFAULT NULL,
			p_message	IN VARCHAR2			DEFAULT NULL
                        );
FUNCTION someone (      p_email_address         IN VARCHAR2                     DEFAULT NULL,
                        p_from                  IN VARCHAR2                     DEFAULT NULL,
                        p_command               IN VARCHAR2                     DEFAULT NULL,
			p_message		IN VARCHAR2			DEFAULT NULL
                ) return varchar2;
PROCEDURE login (       p_email         IN user_details.email%TYPE      DEFAULT NULL );
PROCEDURE mailform (    p_url           IN VARCHAR2                     DEFAULT NULL,
                        p_email_address IN VARCHAR2                     DEFAULT NULL,
                        p_from          IN VARCHAR2                     DEFAULT NULL,
                        p_contact_email IN user_details.email%TYPE      DEFAULT NULL,
                        p_subject       IN VARCHAR2                     DEFAULT NULL,
                        p_body          IN LONG                         DEFAULT NULL );
PROCEDURE startup;
END;
/
CREATE OR REPLACE PACKAGE BODY tip IS
---------------------------------------------------------------------
---------------------------------------------------------------------
-- Name: startup
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 30.08.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE startup
IS
BEGIN
        someone;
END startup;

---------------------------------------------------------------------
-- Name: someone
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 30.08.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE someone (     p_email_address         IN VARCHAR2                     DEFAULT NULL,
                        p_from                  IN VARCHAR2                     DEFAULT NULL,
                        p_command               IN VARCHAR2                     DEFAULT NULL,
			p_message		IN VARCHAR2			DEFAULT NULL
                )
IS
BEGIN
	htp.p( someone( p_email_address, p_from, p_command, p_message) );
END someone;

---------------------------------------------------------------------
-- Name: someone
-- Type: FUNCTION
-- What:
-- Made: Espen Messel
-- Date: 30.08.2000
-- Chng:
---------------------------------------------------------------------
FUNCTION someone (     p_email_address         IN VARCHAR2                     DEFAULT NULL,
                        p_from                  IN VARCHAR2                     DEFAULT NULL,
                        p_command               IN VARCHAR2                     DEFAULT NULL,
			p_message		IN VARCHAR2			DEFAULT NULL
                ) return varchar2
IS
BEGIN
DECLARE

v_code		VARCHAR2(12000)                  DEFAULT NULL;
v_file          utl_file.file_type              default NULL;
v_number        NUMBER                          DEFAULT NULL;
v_message       VARCHAR2(4000)                  DEFAULT NULL;
v_email_dir     VARCHAR2(500)                   DEFAULT NULL;

BEGIN
IF ( p_email_address IS NULL AND p_from IS NULL AND p_command IS NULL ) THEN
                v_code := htmf.b_form('tip.someone')||'
                       <center><table border="0" width="90%" cellpadding="0" cellspacing="0" bgcolor="'||get.value('c_'|| get.serv_name ||'_mmb')||'">
		       <tr>
		       <td align="left" colspan="2"><img src="/images/ul2.gif"></td>
		       <td align="right" colspan="2"><img src="/images/ur2.gif"></td></tr>
		       <tr>
		       <td>&nbsp;<b>'|| get.txt('tip_someone') ||': </b></td>
		       <td>'|| html.text('p_email_address', '20', '50', get.txt('tip_recipient')) ||'</td>
		       <td>'||html.text('p_from', '20', '50', get.txt('tip_sender'))||'</td>
		       <td rowspan="2" valign="bottom"><input type="hidden" name="p_command" value="send">');
		       html.submit_link( get.txt('send_tip'));
		       htp.p('</td></tr>
		       <tr><td>&nbsp;</td>
		       <td colspan="2"><textarea name="p_message" rows="2" cols="45" wrap="virtual" style="width: 400;"></textarea>
		       </tr>
		       <tr><td align="left" colspan="2"><img src="/images/dl2.gif"></td>
		       <td align="right" colspan="2"><img src="/images/dr2.gif"></td></tr>'||
                	htmf.e_table||'</center>'||htmf.e_form;
ELSIF ( p_email_address IS NULL AND p_from IS NULL ) THEN
                v_code := htmf.b_page(NULL,0,owa_util.get_cgi_env('HTTP_REFERER')) ||
                	  htmf.e_page;
ELSIF ( p_email_address IS NULL) THEN
        v_code := htmf.b_page(NULL,3,owa_util.get_cgi_env('HTTP_REFERER'))||
        htmf.b_table ||
        '<tr><td>'||get.txt('error_email_address_missing')||'</td></tr>'||
        htmf.e_table||
        htmf.e_page;
ELSIF ( p_from IS NULL ) THEN
        v_code := htmf.b_page(NULL,3,owa_util.get_cgi_env('HTTP_REFERER'))||
        htmf.b_table||
        '<tr><td>'||get.txt('error_from_missing')||'</td></tr>'||
        htmf.e_table||
        htmf.e_page;
ELSE
        v_code := htmf.b_page(NULL,3,owa_util.get_cgi_env('HTTP_REFERER'))||
        htmf.b_table||
        '<tr><td>'||get.txt('this_URL')||': '|| owa_util.get_cgi_env('HTTP_REFERER') ||'
        </td><td>'||get.txt('send_to')||': '|| trans(p_email_address) ||'</td><td>'||get.txt('from')||': '|| trans(p_from) ||'</td></tr>'||
        htmf.e_table||
        htmf.e_page;
        SELECT  email_script_seq.NEXTVAL
        INTO    v_number
        FROM    DUAL;
        v_message := get.txt('email_notification');
        v_message := REPLACE ( v_message, '[name]', trans(p_from) );
        v_message := REPLACE ( v_message, '[message]', trans(p_message) );
        v_message := REPLACE ( v_message, '[link]', owa_util.get_cgi_env('HTTP_REFERER') );
        v_message := REPLACE ( v_message, '[ip]', owa_util.get_cgi_env('REMOTE_HOST') );
        v_email_dir := get.value('email_dir');
        v_file := utl_file.fopen( v_email_dir, v_number||'_email-list.txt' , 'w');
        utl_file.put_line(v_file, trans(p_email_address));
        utl_file.fclose(v_file);
        v_file := utl_file.fopen( v_email_dir, v_number||'_message.txt' , 'w');
        utl_file.put_line(v_file, v_message );
        utl_file.fclose(v_file);

END IF;

return v_code;

END;
END someone;


---------------------------------------------------------------------
-- Name: login
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 30.08.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE login (       p_email         IN user_details.email%TYPE                      DEFAULT NULL )
IS
BEGIN
DECLARE

v_login_name    usr.login_name%TYPE             DEFAULT NULL;
v_password      usr.password%TYPE               DEFAULT NULL;
v_email         user_details.email%TYPE         DEFAULT NULL;
v_file          utl_file.file_type              DEFAULT NULL;
v_number        NUMBER                          DEFAULT NULL;
v_message       VARCHAR2(4000)                  DEFAULT NULL;
v_message_more  VARCHAR2(4000)                  DEFAULT NULL;
v_email_dir     VARCHAR2(500)                   DEFAULT NULL;

CURSOR  get_passwd IS
SELECT  u.password, ud.email, u.login_name
FROM    usr u, user_details ud
WHERE   u.login_name = trans(p_email)
AND     u.user_pk = ud.user_fk
OR      ud.email = trans(p_email)
AND     u.user_pk = ud.user_fk
;

BEGIN

IF ( p_email IS NULL ) THEN
        html.b_page;

        html.b_box( get.txt('get_passwd'), '100%', 'tip.login' );
        html.b_form( 'tip.login' );
        html.b_table;

        htp.p('
                <tr><td colspan="2">'||get.txt('info_send_password')||'</td></tr>
                <tr><td>'|| get.txt('login_name-email') ||':</td>
                <td align="right"><input type="Text" name="p_email" maxlength="150" size="25"></td></tr>
                <tr><td colspan="2" align="right">');
                html.submit_link( get.txt('send_password') );
                htp.p('</td></tr>');

        html.e_table;
        html.e_form;
        html.e_box;

        htp.p('
                <script>
                document.form.p_email.focus();
                </script>
        ');

        html.e_page;
ELSE
        OPEN get_passwd;
        FETCH get_passwd INTO v_password, v_email, v_login_name;
        v_message := get.txt('email_password_web');

        html.b_page( NULL,3,'main_page.startup' );
        v_message := REPLACE ( v_message, '[email]', v_email );
        html.b_table;
        htp.p('<tr><td>'|| html.rem_tag( v_message ) ||'</td></tr>');
        html.e_table;
        html.e_page;

        IF ( v_email IS NOT NULL ) THEN
                SELECT  email_script_seq.NEXTVAL
                INTO    v_number
                FROM    DUAL;
                v_message := get.txt('email_password');
                v_message := REPLACE ( v_message, '[username]', v_login_name );
                v_message := REPLACE ( v_message, '[password]', v_password );
                v_message := REPLACE ( v_message, '[ip]', owa_util.get_cgi_env('REMOTE_HOST') );
                v_message_more := get.txt('email_password_more');
                LOOP
                        FETCH get_passwd INTO v_password, v_email, v_login_name;
                        EXIT WHEN get_passwd%NOTFOUND;
                        v_message := REPLACE ( v_message, '[more]', v_message_more||'
[more]' );
                        v_message := REPLACE ( v_message, '[username]', v_login_name );
                        v_message := REPLACE ( v_message, '[password]', v_password );
                END LOOP;
                v_message := REPLACE ( v_message, '[more]', '' );
                v_email_dir := get.value('email_dir');
                v_file := utl_file.fopen( v_email_dir, v_number||'_email-list.txt' , 'w');
                utl_file.put_line(v_file, v_email );
                utl_file.fclose(v_file);
                v_file := utl_file.fopen( v_email_dir, v_number||'_message.txt' , 'w');
                utl_file.put_line(v_file, v_message );
                utl_file.fclose(v_file);
        END IF;
        CLOSE get_passwd;
END IF;
END;
END login;

---------------------------------------------------------------------
-- Name: mailform
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 15.10.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE mailform (    p_url           IN VARCHAR2                     DEFAULT NULL,
                        p_email_address IN VARCHAR2                     DEFAULT NULL,
                        p_from          IN VARCHAR2                     DEFAULT NULL,
                        p_contact_email IN user_details.email%TYPE      DEFAULT NULL,
                        p_subject       IN VARCHAR2                     DEFAULT NULL,
                        p_body          IN LONG                         DEFAULT NULL )
IS
BEGIN
DECLARE

v_file          utl_file.file_type              DEFAULT NULL;
v_number        NUMBER                          DEFAULT NULL;
v_email_dir     VARCHAR2(500)                   DEFAULT NULL;
v_url		varchar2(300)			DEFAULT NULL;
v_email_address	VARCHAR2(300)                   DEFAULT NULL;

BEGIN

IF ( p_body IS NULL AND p_contact_email IS NOT NULL ) THEN

	-- setter url som vi skal g� til etter utsendelse
	if (p_url IS NULL) then
		v_url := owa_util.get_cgi_env('HTTP_REFERER');
	else
		v_url := REPLACE( trans(p_url), '�', '?');
		v_url := REPLACE( v_url, '[]', '&');
	end if;

        html.b_page;
        html.b_box( REPLACE( get.txt('send_mail'), '[p_email]', p_contact_email) , '100%', 'tip.mailform' );
        html.b_form( 'tip.mailform' );
        html.b_table;

        htp.p('
                <input type="hidden" name="p_contact_email" value="'||p_contact_email||'">
                <input type="hidden" name="p_url" value="'||v_url||'">
                <tr><td colspan="2">'||get.txt('info_mailform')||'</td></tr>
                <tr><td>'|| get.txt('email_address') ||':</td>
                <td>'|| html.text('p_email_address', '55', '55', p_email_address) ||'</td></tr>
                <td>'|| get.txt('your_name') ||': </td>
                <td>'||html.text('p_from', '55', '55',trans(p_from))||'</td></tr>
                <tr><td>'|| get.txt('subject') ||':</td>
                <td>'|| html.text('p_subject', '55', '150', trans(p_subject) ) ||'</td></tr>
                <tr><td valign="top">'|| get.txt('body') ||':</td>
                <td><textarea name="p_body" ROWS="10" cols="55"></textarea></td></tr>
                <tr><td colspan="2" align="right">');
                html.submit_link( get.txt('send_email') );
                htp.p('</td></tr>');

        html.e_table;
        html.e_form;
        html.e_box;
        html.e_page;
ELSIF ( p_body IS NOT NULL AND
        p_from IS NOT NULL AND
        p_contact_email IS NOT NULL ) THEN
        IF ( p_url IS NOT NULL ) THEN
            html.b_page( NULL,3,trans(p_url) );
        ELSE
            html.b_page( NULL,3,'main_page.startup' );
        END IF;
        html.b_table;
        htp.p('<tr><td valign="top">'|| get.txt('email') ||':</td><td>'|| html.rem_tag( trans(p_contact_email) ) ||'</td></tr>
        <tr><td><br>'|| get.txt('from') ||':</td><td>'|| html.rem_tag( trans(p_from) ) ||'</td></tr>
        <tr><td>'|| get.txt('from_email_address') ||':</td><td>'|| html.rem_tag( trans(p_email_address) ) ||'</td></tr>

        <tr><td><br>'|| get.txt('subject') ||':</td><td>'|| html.rem_tag( trans(p_subject) ) ||'</td></tr>
        <tr><td valign="top">'|| get.txt('body') ||':</td><td>'|| html.rem_tag( trans(p_body) ) ||'</td></tr>');
        html.e_table;
        html.e_page;

        IF ( p_contact_email IS NOT NULL ) THEN
                SELECT  email_script_seq.NEXTVAL
                INTO    v_number
                FROM    DUAL;
                v_email_dir := get.value('email_dir');
                v_file := utl_file.fopen( v_email_dir, v_number||'_email-list.txt' , 'w');
                utl_file.put_line(v_file, trans( p_contact_email ) );
                utl_file.fclose(v_file);
                v_file := utl_file.fopen( v_email_dir, v_number||'_message.txt' , 'w');
		IF ( p_email_address IS NOT NULL ) THEN
			v_email_address := p_email_address;
		ELSE
			v_email_address := get.value('email_info');
		END IF;
                utl_file.put_line(v_file, 'From: in4mant.com <'|| v_email_address ||'>
Reply-To: '|| v_email_address ||'
Subject: '|| trans ( p_subject ) ||'


'|| get.txt('from') ||' : '|| trans ( p_from ) ||'
'|| get.txt('from_email_address') ||' : '|| trans ( p_email_address ) ||'

'|| trans ( p_body ) ||'
' );
                utl_file.fclose(v_file);
        END IF;
ELSE
        html.b_page;
        html.b_box( get.txt('send_mail'), '100%', 'tip.mailform' );
        html.b_form( 'tip.mailform' );
        html.b_table;

        htp.p('
                <tr><td colspan="2">'||get.txt('info_mailform')||'</td></tr>
                <tr><td>'|| get.txt('email') ||':</td>
                <td><input type="Text" name="p_contact_email" maxlength="150" size="55"></td></tr>
                <tr><td colspan="2" align="right">');
                html.submit_link( get.txt('send_email') );
                htp.p('</td></tr>');

        html.e_table;
        html.e_form;
        html.e_box;
        html.e_page;
END IF;
END;
END mailform;

---------------------------------------------------------------------
---------------------------------------------------------------------
END;
/
show errors;
