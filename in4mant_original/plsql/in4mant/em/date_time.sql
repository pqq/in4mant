set define off
PROMPT *** package: date_time ***

CREATE OR REPLACE PACKAGE date_time IS
PROCEDURE startup (             p_command       IN VARCHAR2                             DEFAULT NULL,
                                p_jump          IN VARCHAR2                             DEFAULT 0,
                                p_hour          IN VARCHAR2                             DEFAULT NULL,
                                p_minute        IN VARCHAR2                             DEFAULT NULL,
                                p_variable      IN VARCHAR2                             DEFAULT NULL,
                                p_start_date    IN VARCHAR2                             DEFAULT NULL,
                                p_geography_pk  IN geography.geography_pk%TYPE          DEFAULT 13
                                );
PROCEDURE select_city (         p_geography_pk  IN geography.geography_pk%TYPE          DEFAULT 13);
PROCEDURE whatsup (             p_jump          IN VARCHAR2                             DEFAULT 0,
                                p_start_date    IN VARCHAR2                             DEFAULT TO_CHAR(SYSDATE,'YYYYMMDD'),
                                p_geography_pk  IN geography.geography_pk%TYPE          DEFAULT 13
                                );
END;
/
CREATE OR REPLACE PACKAGE BODY date_time IS
---------------------------------------------------------------------
---------------------------------------------------------------------
-- Name: startup
-- Type: procedure
-- What: Genererer opp hovedsiden for admin av en organisasjon
-- Made: Espen Messel
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE startup (             p_command       IN VARCHAR2                             DEFAULT NULL,
                                p_jump          IN VARCHAR2                             DEFAULT 0,
                                p_hour          IN VARCHAR2                             DEFAULT NULL,
                                p_minute        IN VARCHAR2                             DEFAULT NULL,
                                p_variable      IN VARCHAR2                             DEFAULT NULL,
                                p_start_date    IN VARCHAR2                             DEFAULT NULL,
                                p_geography_pk  IN geography.geography_pk%TYPE          DEFAULT 13
                                )
IS
BEGIN
DECLARE

v_month                 VARCHAR2(10)                                    DEFAULT NULL;
v_year                  VARCHAR2(10)                                    DEFAULT NULL;
v_month_next            VARCHAR2(10)                                    DEFAULT NULL;
v_year_next             VARCHAR2(10)                                    DEFAULT NULL;
v_month_prev            VARCHAR2(10)                                    DEFAULT NULL;
v_year_prev             VARCHAR2(10)                                    DEFAULT NULL;
v_day                   VARCHAR2(10)                                    DEFAULT NULL;
v_1day                  VARCHAR2(10)                                    DEFAULT NULL;
v_days                  VARCHAR2(10)                                    DEFAULT NULL;
v_hour                  VARCHAR2(10)                                    DEFAULT NULL;
v_minute                VARCHAR2(10)                                    DEFAULT NULL;
v_command               VARCHAR2(20)                                    DEFAULT NULL;
v_i                     NUMBER                                          DEFAULT 1;
v_j                     NUMBER                                          DEFAULT 1;
v_k                     NUMBER                                          DEFAULT 1;
v_count                 NUMBER                                          DEFAULT NULL;
v_jump                  NUMBER                                          DEFAULT NULL;
v_start_date            VARCHAR2(20)                                    DEFAULT NULL;

BEGIN

IF ( p_start_date IS NULL ) THEN
--        v_start_date := TO_CHAR(SYSDATE,get.txt('date_full'));
        v_start_date := TO_CHAR(SYSDATE,'YYYYMMDD')||'2000';
ELSE
        v_start_date := p_start_date;
        v_hour   := TO_CHAR(to_date(v_start_date,get.txt('date_full')),'HH24');
        v_minute := TO_CHAR(to_date(v_start_date,get.txt('date_full')),'MI');
END IF;

IF ( p_command = 'register') THEN
        htp.p('<SCRIPT LANGUAGE="JavaScript">
                function update_time(date_time) {
                if (opener.document.form){');
        IF ( p_variable IS NULL ) THEN
                htp.p(' if(opener.document.form.p_publish_date) { opener.document.form.p_publish_date.value = date_time; opener.document.form.p_publish_date2.value = date_time; }');
        ELSE
                htp.p(' if(opener.document.form.'||p_variable||') {opener.document.form.'||p_variable||'.value = date_time; }');
                htp.p(' if(opener.document.form.'||p_variable||'_show) {opener.document.form.'||p_variable||'_show.value = date_time; }');
        END IF;
        htp.p(' } window.close(); } </SCRIPT> ');
        IF ( p_hour IS NULL AND p_minute IS NULL ) THEN
                v_command := p_jump*10000;
        ELSE
                v_command := p_jump*10000+p_hour*100+p_minute;
        END IF;
        IF ( p_variable = 'p_from' OR p_variable = 'p_to' ) THEN
                htp.p('
                        <SCRIPT language=JavaScript>
                        update_time('''||TO_CHAR(TO_DATE(v_command,'YYYYMMDDHH24MI'),get.txt('date_year'))||''');
                        </SCRIPT>
                ');
        ELSE
                htp.p('
                        <SCRIPT language=JavaScript>
                        update_time('''||TO_CHAR(TO_DATE(v_command,'YYYYMMDDHH24MI'),get.txt('date_long'))||''');
                        </SCRIPT>
                ');
        END IF;
ELSIF ( p_command = 'time') THEN
        html.b_page_2('280');
        html.b_box( get.txt('reg_time') );
        html.b_table;
        htp.p('<tr><td>');
        html.b_form('date_time.startup');
        htp.p('<input type="hidden" name="p_jump" value="'||p_jump||'">
        <input type="hidden" name="p_command" value="register">
        <input type="hidden" name="p_variable" value="'||p_variable||'">
        Hour: <select name="p_hour">');
        v_i := 0;
        v_j := 0;
        WHILE ( v_i < 24)
        LOOP
                IF ( v_hour = v_i ) THEN
                        v_command := ' selected';
                ELSE
                        v_command := '';
                END IF;
                IF ( v_i < 10 ) THEN
                        htp.p('<option value="'|| v_i ||'"'|| v_command ||'>0'|| v_i ||'</option>');
                ELSE
                        htp.p('<option value="'|| v_i ||'"'|| v_command ||'>'|| v_i ||'</option>');
                END IF;
                v_i := v_i+1;
        END LOOP;
        htp.p('</select>
        </td><td>

        Minute: <select name="p_minute">');
        WHILE ( v_j < 60)
        LOOP
                IF (v_j = 0 or v_j = 15 or v_j = 30 or v_j = 45) THEN


                        IF ( v_minute >= v_j ) THEN
                                v_command := ' selected';
                        ELSE
                                v_command := '';
                        END IF;
                        IF ( v_j < 10 ) THEN
                                htp.p('<option value="'|| v_j ||'"'|| v_command ||'>0'|| v_j ||'</option>');
                        ELSE
                                htp.p('<option value="'|| v_j ||'"'|| v_command ||'>'|| v_j ||'</option>');
                        END IF;
                END IF;
                v_j := v_j+1;
        END LOOP;
        htp.p('</select>
        </td></tr>
        <tr><td colspan="2">');
        html.submit_link( get.txt('register'));
        htp.p('</td></tr>');
        html.e_form;
ELSE
IF ( p_command != 'cal_util' OR p_command IS NULL ) THEN
   html.b_page_2('280');
   html.b_box( get.txt('reg_date') );
ELSE
   html.b_box_2( get.txt('reg_date'),'100%','date_time' );
END IF;
--      IF ( p_jump IS NULL ) THEN
--              v_month := TO_CHAR(to_date(v_start_date,get.txt('date_long')),'MM');
--              v_year  := TO_CHAR(to_date(v_start_date,get.txt('date_long')),'YYYY');
--              v_day   := TO_CHAR(to_date(v_start_date,get.txt('date_long')),'DD');
--              v_1day  := TO_CHAR(ROUND(to_date(v_start_date,get.txt('date_long')),'W'),'D');
--              v_days  := TO_CHAR(LAST_DAY (to_date(v_start_date,get.txt('date_long'))),'DD');
--      ELSE
                v_month := TO_CHAR(ADD_MONTHS(to_date(v_start_date,get.txt('date_full')),p_jump),'MM');
                v_year  := TO_CHAR(ADD_MONTHS(to_date(v_start_date,get.txt('date_full')),p_jump),'YYYY');
                v_month_next    := TO_CHAR(ADD_MONTHS(to_date(v_start_date,get.txt('date_full')),p_jump+1),'MM');
                v_year_next     := TO_CHAR(ADD_MONTHS(to_date(v_start_date,get.txt('date_full')),p_jump+1),'YYYY');
                v_month_prev    := TO_CHAR(ADD_MONTHS(to_date(v_start_date,get.txt('date_full')),p_jump-1),'MM');
                v_year_prev     := TO_CHAR(ADD_MONTHS(to_date(v_start_date,get.txt('date_full')),p_jump-1),'YYYY');
                v_day   := TO_CHAR(ADD_MONTHS(to_date(v_start_date,get.txt('date_full')),p_jump),'DD');
                v_1day  := TO_CHAR(ROUND(ADD_MONTHS(to_date(v_start_date,get.txt('date_full')),p_jump),'W'),'D');
                v_days  := TO_CHAR(LAST_DAY (ADD_MONTHS(to_date(v_start_date,get.txt('date_full')),p_jump)),'DD');
--      END IF;

        html.b_table;
        v_count := v_days+v_1day;
        IF ( p_command = 'date' ) THEN
                html.b_form('date_time.startup','prev');
                htp.p('<input type="hidden" name="p_command" value="date">');
        ELSIF ( p_command = 'cal_util' ) THEN
                html.b_form('cal_util.whatsup');
              -- Statistikk
              stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,21);
        ELSE
              html.b_form('date_time.startup','prev');
        END IF;
        htp.p('<tr><td colspan="2" align="left" nowrap>
        '|| v_year_prev ||'
        <input type="hidden" name="p_start_date" value="'||v_start_date||'">
        <input type="hidden" name="p_jump" value="'||TO_CHAR(p_jump-1)||'">');
        IF ( p_variable IS NOT NULL ) THEN
                htp.p('<input type="hidden" name="p_variable" value="'||p_variable||'">');
        END IF;
        html.submit_link( v_month_prev,'prev');
        html.e_form;
        htp.p('</td>
        <td align="center" colspan="3">'|| TO_CHAR(TO_DATE(v_month ,'MM'),'Month') ||'</td>
        <td colspan="2" align="right" nowrap valign="top">');
        IF ( p_command = 'cal_util' ) THEN
                html.b_form('cal_util.whatsup');
        ELSE
                html.b_form('date_time.startup','next');
        END IF;
        htp.p( v_year_next ||'
        <input type="hidden" name="p_start_date" value="'||v_start_date||'">
        <input type="hidden" name="p_jump" value="'||TO_CHAR(p_jump+1)||'">');
        IF ( p_command = 'date' ) THEN
                htp.p('<input type="hidden" name="p_command" value="date">');
        END IF;
        IF ( p_variable IS NOT NULL ) THEN
                htp.p('<input type="hidden" name="p_variable" value="'||p_variable||'">');
        END IF;
        html.submit_link( v_month_next,'next');
        htp.p('</td></tr>');
        html.e_form;
        htp.p('<tr><td align="center">M</td><td align="center">T</td><td align="center">O</td><td align="center">T</td><td align="center">F</td><td align="center">L</td><td align="center">S</td></tr>');
--        htp.p('<tr><td align="center">Man</td><td align="center">Tir</td><td align="center">Ons</td><td align="center">Tor</td><td align="center">Fre</td><td align="center">L&oslash;r</td><td align="center">S&oslash;n</td></tr>');

        WHILE ( v_i < v_count)
        LOOP
                v_k := v_i-v_1day+1;
                IF ( v_j = 1 ) THEN
                        htp.p('<tr>');
                END IF;
                IF ( v_day = v_k AND p_jump = 0 ) THEN
                        v_command := ' bgcolor="red"';
                ELSE
                        v_command := '';
                END IF;
                IF ( v_i < v_1day ) THEN
                        htp.p('<td>&nbsp;</td>');
                ELSE
                        v_jump := (v_year*10000)+(v_month*100)+v_k;
                        IF ( p_command = 'date' ) THEN
                                htp.p('<td'|| v_command ||' align="center">&nbsp;<a href="date_time.startup?p_command=register&p_jump='|| v_jump ||'&p_variable='|| p_variable ||'&p_start_date='|| v_start_date ||'">'|| v_k ||'</a></td>');
                        ELSIF ( p_command = 'cal_util' ) THEN
                                htp.p('<td'|| v_command ||' align="center">&nbsp;<a href="Javascript: JumpToCalendar('''|| v_jump ||''');">'|| v_k ||'</a></td>');
                        ELSE
                                htp.p('<td'|| v_command ||' align="center">&nbsp;<a href="date_time.startup?p_command=time&p_jump='|| v_jump ||'&p_variable='|| p_variable ||'&p_start_date='|| v_start_date ||'">'|| v_k ||'</a></td>');
                        END IF;
                END IF;
                IF ( v_j = 7 ) THEN
                        htp.p('</tr>');
                        v_j := 0;
                END IF;
                v_j:=v_j+1;
                v_i:=v_i+1;
        END LOOP;
        IF ( p_command = 'cal_util' ) THEN
           htp.p('<tr><td colspan="7" align="center">');
           select_city(p_geography_pk);
           htp.p('</td></tr>');
/*
           <tr><td colspan="7" align="center">
           <form>
           <input type="button" value="'||get.txt('close_window')||'" onClick="Javascript: window.close();">
           </form>
           </td></tr>');
*/
        END IF;
END IF;
html.e_table;
IF ( p_command != 'cal_util' ) THEN
   html.e_box;
   html.e_page_2;
ELSE
   html.e_box_2;
END IF;
EXCEPTION
WHEN NO_DATA_FOUND
THEN
htp.p('No data found! date_time.sql -> startup');
END;
END; -- prosedyren startup



---------------------------------------------------------------------
-- Name: select_city
-- Type: procedure
-- What:
-- Made: Espen Messel
-- Date: 05.04.2001
-- Chng:
---------------------------------------------------------------------

PROCEDURE select_city ( p_geography_pk  IN geography.geography_pk%TYPE  DEFAULT 13 )
IS
BEGIN
DECLARE

v_string        strng.string%TYPE               DEFAULT NULL;
v_geography_pk  geography.geography_pk%TYPE     DEFAULT NULL;
v_language_pk   la.language_pk%TYPE             DEFAULT NULL;
v_service_pk    service.service_pk%TYPE         DEFAULT NULL;


CURSOR  get_city IS
select distinct(s.string), g.geography_pk
from   geography g, organization o, org_service os,
       groups g, strng s
where  g.string_group_fk = g.name_sg_fk
and    g.string_fk = s.string_pk
and    o.geography_fk = g.geography_pk
and    o.organization_pk = os.organization_fk
and    os.service_fk = v_service_pk
and    s.language_fk = v_language_pk
order by s.string
;

BEGIN
v_language_pk := get.lan;
v_service_pk := get.serv;
        htp.p('
<script language="JavaScript">
function JumpToCalendar(start_date){
         var city_selected;
         for (var i = 0; i < document.test.p_geography_pk.options.length; i++ ){
             if (document.test.p_geography_pk.options[i].selected)
                city_selected = document.test.p_geography_pk.options[i].value;
         }
         if ( document.whatsup ) {
            document.whatsup.p_start_date.value = + start_date;
            document.whatsup.p_geography_pk.value = + city_selected;
            document.whatsup.submit();
         }
         else if ( opener.document.whatsup ) {
            opener.document.whatsup.p_start_date.value = + start_date;
            opener.document.whatsup.p_geography_pk.value = + city_selected;
            opener.document.whatsup.submit();
         }
         else if ( opener.document.frames.Navigation ) {
            opener.document.frames.Navigation.whatsup.p_start_date.value = + start_date;
            opener.document.frames.Navigation.whatsup.p_geography_pk.value = + city_selected;
            opener.document.frames.Navigation.whatsup.submit();
         }

}
</script>
                ');
--       close();

        htp.p('<form name="test"><select name="p_geography_pk">');
        OPEN get_city;
        LOOP
                fetch get_city into v_string, v_geography_pk;
                exit when get_city%NOTFOUND;
                if ( p_geography_pk = v_geography_pk ) then
                   htp.p('<option value="'|| v_geography_pk ||'" selected>'|| v_string ||'</option>');
                else
                   htp.p('<option value="'|| v_geography_pk ||'">'|| v_string ||'</option>');
                end if;
        END LOOP;
        close get_city;
        htp.p('</select></form>');
END;
END select_city;

---------------------------------------------------------------------
-- Name: whatsup
-- Type: procedure
-- What: Genererer kalender for whatsup
-- Made: Espen Messel
-- Date: 25.07.2002
-- Chng:
---------------------------------------------------------------------

PROCEDURE whatsup (             p_jump          IN VARCHAR2                             DEFAULT 0,
                                p_start_date    IN VARCHAR2                             DEFAULT TO_CHAR(SYSDATE,'YYYYMMDD'),
                                p_geography_pk  IN geography.geography_pk%TYPE          DEFAULT 13
                                )
IS
BEGIN
DECLARE

v_month                 VARCHAR2(10)                                    DEFAULT NULL;
v_year                  VARCHAR2(10)                                    DEFAULT NULL;
v_month_next            VARCHAR2(10)                                    DEFAULT NULL;
v_year_next             VARCHAR2(10)                                    DEFAULT NULL;
v_selected              VARCHAR2(10)                                    DEFAULT NULL;
v_month_next2           VARCHAR2(10)                                    DEFAULT NULL;
v_year_next2            VARCHAR2(10)                                    DEFAULT NULL;
v_selected2             VARCHAR2(10)                                    DEFAULT NULL;
v_month_next3           VARCHAR2(10)                                    DEFAULT NULL;
v_year_next3            VARCHAR2(10)                                    DEFAULT NULL;
v_selected3             VARCHAR2(10)                                    DEFAULT NULL;
v_day                   VARCHAR2(10)                                    DEFAULT NULL;
v_1day                  VARCHAR2(10)                                    DEFAULT NULL;
v_days                  VARCHAR2(10)                                    DEFAULT NULL;
v_hour                  VARCHAR2(10)                                    DEFAULT NULL;
v_minute                VARCHAR2(10)                                    DEFAULT NULL;
v_command               VARCHAR2(20)                                    DEFAULT NULL;
v_i                     NUMBER                                          DEFAULT 1;
v_j                     NUMBER                                          DEFAULT 1;
v_k                     NUMBER                                          DEFAULT 1;
v_count                 NUMBER                                          DEFAULT NULL;
v_jump                  NUMBER                                          DEFAULT NULL;
v_start_date            VARCHAR2(20)                                    DEFAULT TO_CHAR(SYSDATE,'YYYYMMDD');

BEGIN

html.b_box_2( get.txt('reg_date'),'100%','date_time' );

IF ( p_start_date is null ) THEN
        v_month := TO_CHAR(ADD_MONTHS(to_date(v_start_date,get.txt('date_full')),p_jump),'MM');
        v_year  := TO_CHAR(ADD_MONTHS(to_date(v_start_date,get.txt('date_full')),p_jump),'YYYY');
ELSE
        v_month := TO_CHAR(ADD_MONTHS(to_date(p_start_date,get.txt('date_full')),p_jump),'MM');
        v_year  := TO_CHAR(ADD_MONTHS(to_date(p_start_date,get.txt('date_full')),p_jump),'YYYY');
END IF;
v_month_next    := TO_CHAR(to_date(v_start_date,get.txt('date_full')),'MM');
v_year_next     := TO_CHAR(to_date(v_start_date,get.txt('date_full')),'YYYY');
v_month_next2    := TO_CHAR(ADD_MONTHS(to_date(v_start_date,get.txt('date_full')),1),'MM');
v_year_next2     := TO_CHAR(ADD_MONTHS(to_date(v_start_date,get.txt('date_full')),1),'YYYY');
v_month_next3    := TO_CHAR(ADD_MONTHS(to_date(v_start_date,get.txt('date_full')),2),'MM');
v_year_next3     := TO_CHAR(ADD_MONTHS(to_date(v_start_date,get.txt('date_full')),2),'YYYY');
--v_day   := TO_CHAR(ADD_MONTHS(to_date(v_start_date,get.txt('date_full')),p_jump),'DD');
v_day   := TO_CHAR(to_date(p_start_date,'YYYYMMDD'),'DD');
v_1day  := TO_CHAR(ROUND(ADD_MONTHS(to_date(v_start_date,get.txt('date_full')),p_jump),'W'),'D');
v_days  := TO_CHAR(LAST_DAY (ADD_MONTHS(to_date(v_start_date,get.txt('date_full')),p_jump)),'DD');

html.b_table;
v_count := v_days+v_1day;

html.b_form('cal_util.whatsup');
-- Statistikk
stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,21);

htp.p('<tr><td align="center" colspan="7">'|| TO_CHAR(TO_DATE(v_month ,'MM'),'Month') || '&nbsp;' ||
    TO_CHAR(TO_DATE(v_year ,'YYYY'),'YYYY') ||'</td></tr>
<tr><td colspan="7" align="center" nowrap valign="top">');

html.b_form('cal_util.whatsup');

IF ( p_start_date IS NOT NULL AND v_start_date IS NOT NULL) THEN
   v_jump := TO_CHAR(TO_DATE(p_start_date,'YYYYMMDD'),'MM') - TO_CHAR(TO_DATE(v_start_date,'YYYYMMDD'),'MM');
   IF ( v_jump = 0 ) THEN v_jump := p_jump; END IF;
ELSE
     v_jump := p_jump;
END IF;
IF ( v_jump = 2 ) THEN
   v_selected3 := ' selected';
ELSIF ( v_jump = 1 ) THEN
   v_selected2 := ' selected';
ELSE
   v_selected := ' selected';
END IF;

htp.p('
<input type="hidden" name="p_start_date" value="'||v_start_date||'">
<select name="p_jump" onChange="Javascript: form.submit();">
<option value="0"'||v_selected||'>'||v_month_next||'-'||v_year_next||'</option>
<option value="1"'||v_selected2||'>'||v_month_next2||'-'||v_year_next2||'</option>
<option value="2"'||v_selected3||'>'||v_month_next3||'-'||v_year_next3||'</option>
</select>');

htp.p('</td></tr>');
html.e_form;

htp.p('<tr><td align="center">M</td><td align="center">T</td><td align="center">O</td><td align="center">T</td><td align="center">F</td><td align="center">L</td><td align="center">S</td></tr>');

        WHILE ( v_i < v_count)
        LOOP
                v_k := v_i-v_1day+1;
                IF ( v_j = 1 ) THEN
                        htp.p('<tr>');
                END IF;
                IF ( v_day = v_k ) THEN
                        v_command := ' bgcolor="red"';
                ELSE
                        v_command := '';
                END IF;
                IF ( v_i < v_1day ) THEN
                        htp.p('<td>&nbsp;</td>');
                ELSE
                        v_jump := (v_year*10000)+(v_month*100)+v_k;
                        htp.p('<td'|| v_command ||' align="center">&nbsp;<a href="Javascript: JumpToCalendar('''|| v_jump ||''');">'|| v_k ||'</a></td>');

                END IF;
                IF ( v_j = 7 ) THEN
                        htp.p('</tr>');
                        v_j := 0;
                END IF;
                v_j:=v_j+1;
                v_i:=v_i+1;
        END LOOP;

        htp.p('<tr><td colspan="7" align="center">');
        select_city(p_geography_pk);
        htp.p('</td></tr>');

html.e_table;

html.e_box_2;

EXCEPTION
WHEN NO_DATA_FOUND
THEN
htp.p('No data found! date_time.sql -> startup');
END;
END; -- prosedyren startup


---------------------------------------------------------------------
---------------------------------------------------------------------
END;
/
show errors;
