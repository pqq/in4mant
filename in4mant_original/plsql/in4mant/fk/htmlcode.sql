set define off
PROMPT *** package: htmlcode ***

CREATE OR REPLACE PACKAGE htmlcode IS

	PROCEDURE startup;
	PROCEDURE insert_html;
	PROCEDURE insertintodb
   		(
			p_html_code_pk		in html_code.html_code_pk%type 	default NULL,
			p_name 			in html_code.name%type default NULL,
	                p_description  		in html_code.description%type default NULL,
	                p_code 			in long default NULL
                );
	PROCEDURE html_top
   		(
			p_link			in number default NULL
                );
	PROCEDURE list_all;
	PROCEDURE delete_entry
		(
			p_html_code_pk 		in html_code.html_code_pk%type	default NULL
		);
	PROCEDURE delete_entry_2
		(
			p_html_code_pk 		in html_code.html_code_pk%type	default NULL,
			p_confirm		in varchar2			default NULL
                );
	PROCEDURE edit_entry
		(
			p_html_code_pk 		in html_code.html_code_pk%type	default NULL
		);

END;
/
CREATE OR REPLACE PACKAGE BODY htmlcode IS

-- **************************************************
-- "PUBLIC METODER" (KALLES DIREKTE FRA WEB)
-- **************************************************

---------------------------------------------------------
-- Name: 	startup
-- Type: 	procedure
-- What: 	kaller 'insert_html'
-- Author:  	Frode Klevstul
-- Start date: 	17.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

	list_all;

END startup;



---------------------------------------------------------
-- Name: 	insert_html
-- Type: 	procedure
-- What:
-- Author:  	Frode Klevstul
-- Start date: 	15.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE insert_html
IS
BEGIN

if ( login.timeout('htmlcode.startup')>0 AND login.right('htmlcode')>0 ) then

	html_top(1);
	html.b_form('htmlcode.insertintodb');
	html.b_box('insert into "html_code"');

	html.b_table;
	htp.p('
		<tr><td>name:</td><td><input type="Text" name="p_name" size="80" maxlength="50"></td></tr>
		<tr><td>description:</td><td><input type="Text" name="p_description" size="80" maxlength="50"></td></tr>
		<tr><td valign="top">code:</td><td><textarea wrap="physical" name="p_code" cols="80" rows="20"></textarea></td></tr>
		<tr><td colspan="2">'); html.submit_link('insert'); htp.p('</td></tr>
	');
	html.e_table;

	html.e_box;
	html.e_form;
	html.e_adm_page;

end if;

END insert_html;



---------------------------------------------------------
-- Name: 	list_all
-- Type: 	procedure
-- What: 	lister ut alle forekomster
-- Author:  	Frode Klevstul
-- Start date: 	15.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
-- beskrivelse av inn parametere:
--
-- [parameter]	[description]
---------------------------------------------------------
PROCEDURE list_all
IS
BEGIN
DECLARE

	CURSOR 	all_code IS
	SELECT 	html_code_pk, name, description
	FROM 	html_code
	ORDER BY name;

	v_html_code_pk	html_code.html_code_pk%type 	default NULL;
	v_name		html_code.name%type 		default NULL;
	v_description	varchar(100)			default NULL;
BEGIN

if ( login.timeout('htmlcode.startup')>0 AND login.right('htmlcode')>0 ) then

	html_top(2);
	htp.p('<br>');
	html.b_box('list all');
	html.b_table;

	htp.p('<tr bgcolor="#eeeeee"><td width="5%">pk:</td><td>name:</td><td>description:</td><td align="center">&nbsp;</td><td align="center">&nbsp;</td></tr>');
	-- g�r igjennom "cursoren"...
	open all_code;
	while (1>0)	-- alltid sant -> g�r igjennom alle forekomstene
	loop
		fetch all_code into v_html_code_pk, v_name, v_description;
		exit when all_code%NOTFOUND;
		v_description := html.rem_tag(v_description);
		htp.p('<tr><td width="5%"><font color="#dddddd">'|| v_html_code_pk ||'</font></td><td><b>'|| v_name ||'</b></td><td><i>'|| v_description ||'</i></td><td>'); html.button_link('UPDATE', 'htmlcode.edit_entry?p_html_code_pk='||v_html_code_pk); htp.p('</td><td>'); html.button_link('DELETE', 'htmlcode.delete_entry?p_html_code_pk='|| v_html_code_pk); htp.p('</td></tr>');
	end loop;
	close all_code;

	html.e_table;
	html.e_box;
	html.e_adm_page;

end if;

END;
END list_all;



-- **************************************************
-- "PRIVATE METODER" (KALLES IKKE DIREKTE FRA WEB)
-- **************************************************

---------------------------------------------------------
-- Name: 	insertintodb
-- Type: 	procedure
-- What: 	inserts the entry into the database
-- Author:  	Frode Klevstul
-- Start date: 	15.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE insertintodb
		(
			p_html_code_pk		in html_code.html_code_pk%type 	default NULL,
			p_name 			in html_code.name%type 		default NULL,
	                p_description  		in html_code.description%type 	default NULL,
	                p_code 			in long 			default NULL
                )
IS
BEGIN
DECLARE

	v_html_code_pk 	html_code.html_code_pk%type 	default NULL;	-- henter ut pk av sekvens
	v_check 	number	default 0;

BEGIN

if ( login.timeout('htmlcode.startup')>0 AND login.right('htmlcode')>0 ) then

	-- sjekker om det allerede finnes en forekomst med dette navnet
	SELECT count(*) INTO v_check
	FROM html_code
	WHERE name = p_name;

	if (v_check > 0 AND p_html_code_pk IS NULL) then
		htp.p('The name <b>'||p_name||'</b> alredy exists, choose another name.');
		html.e_adm_page; -- skriver ut slutten p� html siden
	else
	begin
		-- henter ut neste nummer i sekvensen til tabellen
		if (p_html_code_pk is NULL AND p_name IS NOT NULL) then
			SELECT html_code_seq.NEXTVAL
			INTO v_html_code_pk
			FROM dual;
			-- legger inn i databasen
			INSERT INTO html_code
			(html_code_pk, name, description, code)
			VALUES (v_html_code_pk, p_name, p_description, SUBSTR(REPLACE(p_code,'&amp;','&'),1,4000));
			commit;
		elsif (p_name IS NOT NULL) then
		begin
			UPDATE html_code
			SET name = p_name, description = p_description, code = SUBSTR(REPLACE(p_code,'&amp;','&'),1,4000)
			WHERE html_code_pk = p_html_code_pk;
			commit;
		end;
		end if;


	end;
	end if;

	list_all;

end if;


END;
END insertintodb;



---------------------------------------------------------
-- Name: 	delete_entry
-- Type: 	procedure
-- What:
-- Author:  	Frode Klevstul
-- Start date: 	17.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE delete_entry
		(
			p_html_code_pk 		in html_code.html_code_pk%type	default NULL
                )
IS
BEGIN
DECLARE

	v_name		html_code.name%type	default NULL;

BEGIN

if ( login.timeout('htmlcode.startup')>0 AND login.right('htmlcode')>0 ) then

	SELECT name INTO v_name
	FROM html_code
	WHERE html_code_pk = p_html_code_pk;


	html_top(2);
	html.b_form('htmlcode.delete_entry_2');
	html.b_box('delete');
	htp.p('<input type="hidden" name="p_html_code_pk" value="'|| p_html_code_pk ||'">');
	html.b_table;

	htp.p('
		<tr><td colspan="2">Are you sure you want to delete the entry <b>'|| v_name ||'</b> with pk <b>'||p_html_code_pk||'</b>?</td></tr>
		<tr><td><input type="submit" name="p_confirm" value="yes"></td><td align="right"><input type="submit" name="p_confirm" value="no"></td></tr>
	');

	html.e_table;
	html.e_box;
	html.e_form;
	html.e_adm_page;

end if;

END;
END delete_entry;


---------------------------------------------------------
-- Name: 	delete_entry_2
-- Type: 	procedure
-- What:
-- Author:  	Frode Klevstul
-- Start date: 	17.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE delete_entry_2
		(
			p_html_code_pk 		in html_code.html_code_pk%type	default NULL,
			p_confirm		in varchar2			default NULL
                )
IS
BEGIN

if ( login.timeout('htmlcode.startup')>0 AND login.right('htmlcode')>0 ) then

	if (p_confirm = 'yes') then
		DELETE FROM html_code
		WHERE html_code_pk = p_html_code_pk;
		commit;
	end if;
	list_all;

end if;

END delete_entry_2;



---------------------------------------------------------
-- Name: 	edit_entry
-- Type: 	procedure
-- What: 	edit a entry in html_code
-- Author:  	Frode Klevstul
-- Start date: 	17.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE edit_entry
		(
			p_html_code_pk 		in html_code.html_code_pk%type	default NULL
                )
IS
BEGIN
DECLARE

	v_name		html_code.name%type		default NULL;
	v_description	varchar2(100)			default NULL;
	v_code		html_code.code%type		default NULL;

BEGIN

if ( login.timeout('htmlcode.startup')>0 AND login.right('htmlcode')>0 ) then

	SELECT name, description, code INTO v_name, v_description, v_code
	FROM html_code
	WHERE html_code_pk = p_html_code_pk;

	v_description := html.rem_tag(v_description);

	html_top(2);
	html.b_form('htmlcode.insertintodb');
	html.b_box('update an entry in "html_code"');
	html.b_table;
	htp.p('
		<input type="hidden" name="p_html_code_pk" value="'|| p_html_code_pk ||'">
		<tr><td colspan=2 align="center"><b>edit an entry in "html_code":</b></td></tr>
		<tr><td>name:</td><td><input type="Text" name="p_name" size="80" maxlength="50" value="'|| v_name ||'"></td></tr>
		<tr><td>description:</td><td><input type="Text" name="p_description" size="80" maxlength="50" value="'|| v_description ||'"></td></tr>
		<tr><td valign="top">code:</td><td><textarea wrap="physical" name="p_code" cols="80" rows="20">'|| REPLACE(v_code,'&','&amp;') ||'</textarea></td></tr>
		<tr><td colspan="2">'); html.submit_link('update'); htp.p('</td></tr>
	');
	html.e_table;
	html.e_box;
	html.e_form;
	html.e_adm_page;

end if;

END;
END edit_entry;



---------------------------------------------------------
-- Name: 	html_top
-- Type: 	procedure
-- What: 	skriver ut html_koden p� toppen (linker)
-- Author:  	Frode Klevstul
-- Start date: 	15.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
-- beskrivelse av inn parametere:
--
-- [parameter]	[description]
-- p_link	gir oss nr p� linken vi er p� n�
--		1 = insert_html
--		2 = edit_html
---------------------------------------------------------
PROCEDURE html_top
		(
			p_link		in number default NULL
                )
IS
BEGIN

	html.b_adm_page('htmlcode');

	-- utskriving av linkene/menyen p� toppen av siden:
	if (p_link = '1') then
		htp.p('
		<table border="0" width="100%">
		<tr bgcolor="#cccccc"><td bgcolor="#aaaaaa" align="center" width="50%"><a href="htmlcode.insert_html"><font color="#ffffff">insert</font></a></td><td align="center"><a href="htmlcode.list_all">update/delete</a></td></tr>
		</table>
		');
	elsif (p_link = '2') then
	begin
		htp.p('
		<table border="0" width="100%">
		<tr bgcolor="#cccccc"><td align="center" width="50%"><a href="htmlcode.insert_html">insert</a></font></td><td align="center" bgcolor="#aaaaaa"><a href="htmlcode.list_all"><font color="#ffffff">update/delete</font></a></td></tr>
		</table>
		');
	end;
	end if;

END html_top;





-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/
show errors;
