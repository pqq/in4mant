set define off
PROMPT *** package: list_adm ***

CREATE OR REPLACE PACKAGE list_adm IS

	PROCEDURE startup;
	PROCEDURE list_all;
	PROCEDURE insert_action
		(
			p_list_type_pk	in list_type.list_type_pk%type	default NULL,
			p_language_pk	in list_type.language_fk%type	default NULL,
			p_service_pk	in list_type.service_fk%type	default NULL,
			p_name		in string_group.name%type	default NULL,
			p_description	in list_type.description%type	default NULL
	         );
	PROCEDURE update_listtype
		(
			p_list_type_pk 		in list_type.list_type_pk%type	default NULL
                );
	PROCEDURE delete_entry
		(
			p_list_type_pk 		in list_type.list_type_pk%type	default NULL
                );
	PROCEDURE delete_entry_2
		(
			p_list_type_pk 		in list_type.list_type_pk%type	default NULL,
			p_confirm		in varchar2			default NULL
                );
	PROCEDURE select_list
		(
			p_list_type_fk	in list.list_type_fk%type	default NULL,
			p_level_no	in number			default 0,
			p_level0	in list.level0%type		default NULL,
			p_level1	in list.level1%type		default NULL,
			p_level2	in list.level2%type		default NULL,
			p_level3	in list.level3%type		default NULL,
			p_level4	in list.level4%type		default NULL,
			p_level5	in list.level5%type		default NULL
		);
	PROCEDURE insert_list
		(
			p_list_type_fk	in list.list_type_fk%type	default NULL,
			p_list_pk	in list.list_pk%type		default NULL,
			p_above_list	in list.list_pk%type		default NULL,
			p_name		in string_group.name%type	default NULL,
			p_url		in list.url%type		default NULL
                );
	PROCEDURE update_list
		(
			p_list_pk 		in list.list_pk%type	default NULL
	        );
	PROCEDURE delete_list
		(
			p_list_pk 	in list.list_pk%type	default NULL
                );
	PROCEDURE delete_list_2
		(
			p_list_pk 		in list.list_pk%type		default NULL,
			p_confirm		in varchar2			default NULL
                );
END;
/
CREATE OR REPLACE PACKAGE BODY list_adm IS


---------------------------------------------------------
-- Name: 	startup
-- Type: 	procedure
-- What: 	start procedure, to run the package
-- Author:  	Frode Klevstul
-- Start date: 	12.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

	list_all;

END startup;



---------------------------------------------------------
-- Name: 	list_all
-- Type: 	procedure
-- What: 	lister ut alle forekomster
-- Author:  	Frode Klevstul
-- Start date: 	12.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE list_all
IS
BEGIN
DECLARE

	CURSOR 	select_all IS
	SELECT 	list_type_pk, language_fk, service_fk, name_sg_fk, description
	FROM 	list_type
	WHERE	list_type_pk > 0;

	v_list_type_pk	list_type.list_type_pk%type	default NULL;
	v_language_fk	list_type.language_fk%type	default NULL;
	v_service_fk	list_type.service_fk%type	default NULL;
	v_name_sg_fk	list_type.name_sg_fk%type	default NULL;
	v_description	list_type.description%type	default NULL;

	v_service_description	service.description%type	default NULL;
	v_language_name		la.language_name%type		default NULL;
	v_name			string_group.name%type		default NULL;

BEGIN

if ( login.timeout('list_adm.startup')>0 AND login.right('list_adm')>0 ) then

	html.b_adm_page('list_adm');
	html.b_box('all entries in ''list_type'' table');
	html.b_table;

	htp.p('<tr bgcolor="#eeeeee"><td width="5%">pk:</td><td>name:</td><td>description:</td><td>service:</td><td>language:</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>');
	-- g�r igjennom "cursoren"...
	open select_all;
	while (1>0)	-- alltid sant -> g�r igjennom alle forekomstene
	loop
		fetch select_all into v_list_type_pk, v_language_fk, v_service_fk, v_name_sg_fk, v_description;
		exit when select_all%NOTFOUND;

		SELECT 	description INTO v_service_description
		FROM 	service
		WHERE	service_pk = v_service_fk;

		SELECT	language_name INTO v_language_name
		FROM	la
		WHERE	language_pk = v_language_fk;

		htp.p('<tr><td width="5%"><font color="#bbbbbb">'|| v_list_type_pk ||'</font></td><td><b>'|| get.sg_name(v_name_sg_fk) ||'</b></td><td>'|| v_description ||'</td><td>'|| v_service_description ||'</td><td>'|| v_language_name ||'</td><td align="right">'); html.button_link('ADD/REMOVE LISTS', 'list_adm.select_list?p_list_type_fk='||v_list_type_pk); htp.p('</td><td align="center">'); html.button_link('UPDATE', 'list_adm.update_listtype?p_list_type_pk='||v_list_type_pk); htp.p('</td><td>'); html.button_link('DELETE', 'list_adm.delete_entry?p_list_type_pk='||v_list_type_pk); htp.p('</td></tr>');
	end loop;
	close select_all;

	html.e_table;





	/* kode til insert form */
	html.b_form('list_adm.insert_action');
	html.b_table('40%');
	htp.p('
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr bgcolor="#eeeeee"><td colspan="2" align="center"><b>add new entry in "list_type"</b></td></tr>
		<tr><td>name:</td><td><input type="Text" name="p_name" size="30" maxlength="50"></td></tr>
		<tr><td>description:</td><td><input type="Text" name="p_description" size="50" maxlength="50"></td></tr>
		<tr><td>service:</td><td>'); html.select_service(NULL, -1000); htp.p('</td></tr>
		<tr><td>language:</td><td>'); html.select_lang(NULL, -1000); htp.p('</td></tr>
		<tr><td colspan="2" align="right">'); html.submit_link('insert'); htp.p('
		</td></tr>
	');
	html.e_table;
	html.e_form;

	html.e_box;
	html.e_adm_page;

end if;

END;
END list_all;




---------------------------------------------------------
-- Name: 	insert_action
-- Type: 	procedure
-- What: 	inserts the entry into the database
-- Author:  	Frode Klevstul
-- Start date: 	12.07.2000
---------------------------------------------------------
PROCEDURE insert_action
		(
			p_list_type_pk	in list_type.list_type_pk%type	default NULL,
			p_language_pk	in list_type.language_fk%type	default NULL,
			p_service_pk	in list_type.service_fk%type	default NULL,
			p_name		in string_group.name%type	default NULL,
			p_description	in list_type.description%type	default NULL
                )
IS
BEGIN
DECLARE

	v_list_type_pk 		list_type.list_type_pk%type 		default NULL;	-- henter ut pk av sekvens
	v_check 		number					default 0;
	v_description		list_type.description%type 		default NULL;
	v_string_group_pk	string_group.string_group_pk%type	default NULL;

BEGIN

if ( login.timeout('list_adm.startup')>0 AND login.right('list_adm')>0 ) then

	-- sjekker om det allerede finnes en forekomst med dette navnet
	if ( p_list_type_pk IS NOT NULL ) then
		SELECT count(*) INTO v_check
		FROM string_group, list_type
		WHERE name = p_name
		AND name_sg_fk = string_group_pk
		AND list_type_pk <> p_list_type_pk;
	else
		SELECT count(*) INTO v_check
		FROM string_group
		WHERE name = p_name;
	end if;

	if (v_check > 0 OR p_description IS NULL OR p_name IS NULL) then
		htp.p( get.msg(1, 'there already exists an entry with name = <b>'||p_name||'</b>') );
		html.e_adm_page; -- slutter siden
	else
	begin
		v_description := p_description;
		v_description := html.rem_tag(v_description);

		-- henter ut neste nummer i sekvensen til tabellen
		if (p_list_type_pk is NULL AND p_name IS NOT NULL) then
			SELECT list_type_seq.NEXTVAL
			INTO v_list_type_pk
			FROM dual;

			SELECT string_group_seq.NEXTVAL
			INTO v_string_group_pk
			FROM dual;

			-- legger inn i databasen
			INSERT INTO string_group
			(string_group_pk, name, description)
			VALUES (v_string_group_pk, p_name, '[entry inserted with LIST_ADM package]');
			commit;

			INSERT INTO list_type
			(list_type_pk, language_fk, service_fk, name_sg_fk, description)
			VALUES (v_list_type_pk, p_language_pk, p_service_pk, v_string_group_pk, p_description);
			commit;

		elsif (p_name IS NOT NULL) then
		begin

			-- oppdaterer ikke navnet dersom det finnes fra f�r
			v_check := 0;
			SELECT 	count(*) INTO v_check
			FROM	string_group
			WHERE	name = p_name;

			if (v_check = 0) then
				UPDATE 	string_group
				SET	name = p_name
				WHERE	string_group_pk = (
						SELECT 	name_sg_fk
						FROM	list_type
						WHERE	list_type_pk = p_list_type_pk);
				commit;
			end if;

			UPDATE list_type
			SET language_fk = p_language_pk, service_fk = p_service_pk, description = p_description
			WHERE list_type_pk = p_list_type_pk;
			commit;
		end;
		end if;
	end;
	end if;
	list_all;

end if;

END;
END insert_action;



---------------------------------------------------------
-- Name: 	update_listtype
-- Type: 	procedure
-- What: 	edit an entry
-- Author:  	Frode Klevstul
-- Start date: 	13.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE update_listtype
		(
			p_list_type_pk 		in list_type.list_type_pk%type	default NULL
                )
IS
BEGIN
DECLARE


	v_language_fk	list_type.language_fk%type	default NULL;
	v_service_fk	list_type.service_fk%type	default NULL;
	v_name_sg_fk	list_type.name_sg_fk%type	default NULL;
	v_description	list_type.description%type	default NULL;

BEGIN


if ( login.timeout('list_adm.startup')>0 AND login.right('list_adm')>0 ) then

	SELECT 	language_fk, service_fk, name_sg_fk, description
	INTO	v_language_fk, v_service_fk, v_name_sg_fk, v_description
	FROM 	list_type
	WHERE	list_type_pk = p_list_type_pk;

	v_description := html.rem_tag(v_description);

	html.b_adm_page('list_adm');
	html.b_box('update list_type');
	html.b_form('list_adm.insert_action');
	html.b_table;

	htp.p('
		<input type="hidden" name="p_list_type_pk" value="'|| p_list_type_pk ||'">
		<tr><td colspan="2" align="center"><b>update an entry in the table "list_type"</b></td></tr>
		<tr><td>name:</td><td><input type="Text" name="p_name" size="30" maxlength="50" value="'|| get.sg_name(v_name_sg_fk) ||'"></td></tr>
		<tr><td>description:</td><td><input type="Text" name="p_description" size="50" maxlength="50" value="'|| v_description ||'"></td></tr>
		<tr><td>service:</td><td>'); html.select_service(v_service_fk,-1000); htp.p('</td></tr>
		<tr><td>language:</td><td>'); html.select_lang(v_language_fk, -1000); htp.p('</td></tr>
		<tr><td colspan="2">'); html.submit_link('update'); htp.p('</td></tr>
	');

	html.e_table;
	html.e_form;
	html.e_box;
	html.e_adm_page;

end if;

END;
END update_listtype;



---------------------------------------------------------
-- Name: 	delete_entry
-- Type: 	procedure
-- What: 	confirm a deletion of an entry in the database
-- Author:  	Frode Klevstul
-- Start date: 	13.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE delete_entry
		(
			p_list_type_pk 		in list_type.list_type_pk%type	default NULL
                )
IS
BEGIN
DECLARE

	v_name_sg_fk	list_type.name_sg_fk%type		default NULL;

BEGIN

if ( login.timeout('list_adm.startup')>0 AND login.right('list_adm')>0 ) then

	SELECT 	name_sg_fk INTO v_name_sg_fk
	FROM 	list_type
	WHERE 	list_type_pk = p_list_type_pk;

	html.b_adm_page('list_adm');
	html.b_box('delete list_type');
	html.b_form('list_adm.delete_entry_2');
	html.b_table;

	htp.p('
		<input type="hidden" name="p_list_type_pk" value="'|| p_list_type_pk ||'">
		<tr><td colspan="2">Are you sure you want to delete the entry <b>'|| get.sg_name(v_name_sg_fk) ||'</b> with pk <b>'||p_list_type_pk||'</b>?</td></tr>
		<tr><td><input type="submit" name="p_confirm" value="yes"></td><td align="right"><input type="submit" name="p_confirm" value="no"></td></tr>
		</table>
		</form>
	');

	html.e_table;
	html.e_form;
	html.e_box;
	html.e_adm_page;

end if;

END;
END delete_entry;


---------------------------------------------------------
-- Name: 	delete_entry_2
-- Type: 	procedure
-- What:
-- Author:  	Frode Klevstul
-- Start date: 	13.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE delete_entry_2
		(
			p_list_type_pk 		in list_type.list_type_pk%type	default NULL,
			p_confirm		in varchar2			default NULL
                )
IS
BEGIN
DECLARE
	v_name_sg_fk	list_type.name_sg_fk%type	default NULL;

BEGIN
if ( login.timeout('list_adm.startup')>0 AND login.right('list_adm')>0 ) then

	if (p_confirm = 'yes') then
		SELECT 	name_sg_fk INTO v_name_sg_fk
		FROM	list_type
		WHERE	list_type_pk = p_list_type_pk;

		DELETE FROM string_group
		WHERE string_group_pk = v_name_sg_fk;
		commit;

		DELETE FROM list_type
		WHERE list_type_pk = p_list_type_pk;
		commit;
	end if;
	list_all;

end if;

END;
END delete_entry_2;




---------------------------------------------------------
-- Name: 	select_list
-- Type: 	procedure
-- What: 	select out list
-- Author:  	Frode Klevstul
-- Start date: 	07.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE select_list(
		p_list_type_fk	in list.list_type_fk%type	default NULL,
		p_level_no	in number			default 0,
		p_level0	in list.level0%type		default NULL,
		p_level1	in list.level1%type		default NULL,
		p_level2	in list.level2%type		default NULL,
		p_level3	in list.level3%type		default NULL,
		p_level4	in list.level4%type		default NULL,
		p_level5	in list.level5%type		default NULL
	)
IS
BEGIN
DECLARE


	CURSOR 	select_list_0 IS
	SELECT 	name_sg_fk, level0, url
	FROM 	list
	WHERE	level1 IS NULL
	AND	list_type_fk = p_list_type_fk;

	CURSOR 	select_list_1 IS
	SELECT 	name_sg_fk, level0, level1, url
	FROM 	list
	WHERE	level0 = p_level0
	AND	level1 IS NOT NULL
	AND	level2 IS NULL
	AND	list_type_fk = p_list_type_fk;

	CURSOR 	select_list_2 IS
	SELECT 	name_sg_fk, level0, level1, level2, url
	FROM 	list
	WHERE	level1 = p_level1
	AND	level2 IS NOT NULL
	AND	level3 IS NULL
	AND	list_type_fk = p_list_type_fk;

	CURSOR 	select_list_3 IS
	SELECT 	name_sg_fk, level0, level1, level2, level3, url
	FROM 	list
	WHERE	level2 = p_level2
	AND	level3 IS NOT NULL
	AND	level4 IS NULL
	AND	list_type_fk = p_list_type_fk;

	CURSOR 	select_list_4 IS
	SELECT 	name_sg_fk, level0, level1, level2, level3, level4, url
	FROM 	list
	WHERE	level3 = p_level3
	AND	level4 IS NOT NULL
	AND	level5 IS NULL
	AND	list_type_fk = p_list_type_fk;

	CURSOR 	select_list_5 IS
	SELECT 	name_sg_fk, level0, level1, level2, level3, level4, level5, url
	FROM 	list
	WHERE	level4 = p_level4
	AND	level5 IS NOT NULL
	AND	list_type_fk = p_list_type_fk;

	v_name_sg_fk		list.name_sg_fk%type		default NULL;
	v_next_level		number				default NULL;
	v_level0		list.level0%type		default NULL;
	v_level1		list.level1%type		default NULL;
	v_level2		list.level2%type		default NULL;
	v_level3		list.level3%type		default NULL;
	v_level4		list.level4%type		default NULL;
	v_level5		list.level5%type		default NULL;
	v_url			list.url%type			default NULL;

BEGIN
if ( login.timeout('list_adm.startup')>0 AND login.right('list_adm')>0 ) then

	-- henter ut navnet til listetypen
	SELECT 	name_sg_fk INTO v_name_sg_fk
	FROM	list_type
	WHERE	list_type_pk = p_list_type_fk;


	-- teller opp hvilken level/hvilket niv� som kommer etterp�
	v_next_level := p_level_no + 1;

	html.b_adm_page('list_adm');
	html.b_box( 'level '''|| p_level_no ||''' list of type '''|| get.sg_name(v_name_sg_fk) ||'''');

	html.b_table('100%');

	-- skriver ut "path" p� toppen
	if ( p_level_no = 1 ) then
		SELECT	name_sg_fk INTO v_name_sg_fk
		FROM	list
		WHERE	list_pk = p_level0;
		htp.p('<tr><td colspan="4" align="center"><a href="list_adm.select_list?p_list_type_fk='|| p_list_type_fk ||'">level 0</a> - '|| get.sg_name(v_name_sg_fk) ||'</td></tr>');
	elsif ( p_level_no = 2 ) then

		htp.p('<tr><td colspan="4" align="center"><a href="list_adm.select_list?p_list_type_fk='|| p_list_type_fk ||'">level 0</a> - ');
		SELECT	name_sg_fk INTO v_name_sg_fk
		FROM	list
		WHERE	list_pk = p_level0;
		htp.p('<a href="list_adm.select_list?p_list_type_fk='|| p_list_type_fk ||'&p_level_no=1&p_level0='|| p_level0 ||'">'|| get.sg_name(v_name_sg_fk) ||'</a> - ' );

		SELECT	name_sg_fk INTO v_name_sg_fk
		FROM	list
		WHERE	list_pk = p_level1;
		htp.p(get.sg_name(v_name_sg_fk));

		htp.p('</td></tr>');
	elsif ( p_level_no = 3 ) then

		htp.p('<tr><td colspan="4" align="center"><a href="list_adm.select_list?p_list_type_fk='|| p_list_type_fk ||'">level 0</a> - ');
		SELECT	name_sg_fk INTO v_name_sg_fk
		FROM	list
		WHERE	list_pk = p_level0;
		htp.p('<a href="list_adm.select_list?p_list_type_fk='|| p_list_type_fk ||'&p_level_no=1&p_level0='|| p_level0 ||'">'|| get.sg_name(v_name_sg_fk) ||'</a> - ' );

		SELECT	name_sg_fk INTO v_name_sg_fk
		FROM	list
		WHERE	list_pk = p_level1;
		htp.p('<a href="list_adm.select_list?p_list_type_fk='|| p_list_type_fk ||'&p_level_no=2&p_level0='|| p_level0 ||'&p_level1='|| p_level1 ||'">'|| get.sg_name(v_name_sg_fk) ||'</a> - ' );

		SELECT	name_sg_fk INTO v_name_sg_fk
		FROM	list
		WHERE	list_pk = p_level2;
		htp.p(get.sg_name(v_name_sg_fk));

		htp.p('</td></tr>');
	elsif ( p_level_no = 4 ) then

		htp.p('<tr><td colspan="4" align="center"><a href="list_adm.select_list?p_list_type_fk='|| p_list_type_fk ||'">level 0</a> - ');
		SELECT	name_sg_fk INTO v_name_sg_fk
		FROM	list
		WHERE	list_pk = p_level0;
		htp.p('<a href="list_adm.select_list?p_list_type_fk='|| p_list_type_fk ||'&p_level_no=1&p_level0='|| p_level0 ||'">'|| get.sg_name(v_name_sg_fk) ||'</a> - ' );

		SELECT	name_sg_fk INTO v_name_sg_fk
		FROM	list
		WHERE	list_pk = p_level1;
		htp.p('<a href="list_adm.select_list?p_list_type_fk='|| p_list_type_fk ||'&p_level_no=2&p_level0='|| p_level0 ||'&p_level1='|| p_level1 ||'">'|| get.sg_name(v_name_sg_fk) ||'</a> - ' );

		SELECT	name_sg_fk INTO v_name_sg_fk
		FROM	list
		WHERE	list_pk = p_level2;
		htp.p('<a href="list_adm.select_list?p_list_type_fk='|| p_list_type_fk ||'&p_level_no=3&p_level0='|| p_level0 ||'&p_level1='|| p_level1 ||'&p_level2='|| p_level2 ||'">'|| get.sg_name(v_name_sg_fk) ||'</a> - ' );

		SELECT	name_sg_fk INTO v_name_sg_fk
		FROM	list
		WHERE	list_pk = p_level3;
		htp.p(get.sg_name(v_name_sg_fk));

		htp.p('</td></tr>');
	elsif ( p_level_no = 5 ) then

		htp.p('<tr><td colspan="4" align="center"><a href="list_adm.select_list?p_list_type_fk='|| p_list_type_fk ||'">level 0</a> - ');
		SELECT	name_sg_fk INTO v_name_sg_fk
		FROM	list
		WHERE	list_pk = p_level0;
		htp.p('<a href="list_adm.select_list?p_list_type_fk='|| p_list_type_fk ||'&p_level_no=1&p_level0='|| p_level0 ||'">'|| get.sg_name(v_name_sg_fk) ||'</a> - ' );

		SELECT	name_sg_fk INTO v_name_sg_fk
		FROM	list
		WHERE	list_pk = p_level1;
		htp.p('<a href="list_adm.select_list?p_list_type_fk='|| p_list_type_fk ||'&p_level_no=2&p_level0='|| p_level0 ||'&p_level1='|| p_level1 ||'">'|| get.sg_name(v_name_sg_fk) ||'</a> - ' );

		SELECT	name_sg_fk INTO v_name_sg_fk
		FROM	list
		WHERE	list_pk = p_level2;
		htp.p('<a href="list_adm.select_list?p_list_type_fk='|| p_list_type_fk ||'&p_level_no=3&p_level0='|| p_level0 ||'&p_level1='|| p_level1 ||'&p_level2='|| p_level2 ||'">'|| get.sg_name(v_name_sg_fk) ||'</a> - ' );

		SELECT	name_sg_fk INTO v_name_sg_fk
		FROM	list
		WHERE	list_pk = p_level3;
		htp.p('<a href="list_adm.select_list?p_list_type_fk='|| p_list_type_fk ||'&p_level_no=4&p_level0='|| p_level0 ||'&p_level1='|| p_level1 ||'&p_level2='|| p_level2 ||'&p_level3='|| p_level3 ||'">'|| get.sg_name(v_name_sg_fk) ||'</a> - ' );

		SELECT	name_sg_fk INTO v_name_sg_fk
		FROM	list
		WHERE	list_pk = p_level4;
		htp.p(get.sg_name(v_name_sg_fk));

		htp.p('</td></tr>');
	end if;


	htp.p('<tr bgcolor="#eeeeee"><td width="5%">pk:</td><td>name:</td><td>&nbsp;</td><td>&nbsp;</td></tr>');

	if (p_level_no = 0) then
		open select_list_0;
		loop
			fetch select_list_0 into v_name_sg_fk, v_level0, v_url;
			exit when select_list_0%NOTFOUND;
			htp.p('<tr><td>'|| v_level0 ||'</td><td><a href="list_adm.select_list?p_list_type_fk='|| p_list_type_fk ||'&p_level_no='|| v_next_level ||'&p_level0='|| v_level0 ||'">'|| get.sg_name(v_name_sg_fk) ||'</a> ('|| html.popup(v_url, v_url, 750, 650, 1) ||')</td><td align="right">'); html.button_link('UPDATE', 'list_adm.update_list?p_list_pk='||v_level0); htp.p('</td><td>'); html.button_link('DELETE', 'list_adm.delete_list?p_list_pk='||v_level0); htp.p('</td></tr>');
		end loop;
		close select_list_0;

	elsif (p_level_no = 1) then
		open select_list_1;
		loop
			fetch select_list_1 into v_name_sg_fk, v_level0, v_level1, v_url;
			exit when select_list_1%NOTFOUND;
			htp.p('<tr><td><font color="#bbbbbb">'|| v_level1 ||'</td><td><a href="list_adm.select_list?p_list_type_fk='|| p_list_type_fk ||'&p_level_no='|| v_next_level ||'&p_level0='|| v_level0 ||'&p_level1='|| v_level1 ||'">'|| get.sg_name(v_name_sg_fk) ||'</a> ('|| html.popup(v_url, v_url, 750, 650, 1) ||')</td><td align="right">'); html.button_link('UPDATE', 'list_adm.update_list?p_list_pk='||v_level1); htp.p('</td><td>'); html.button_link('DELETE', 'list_adm.delete_list?p_list_pk='||v_level1); htp.p('</td></tr>');
		end loop;
		close select_list_1;

	elsif (p_level_no = 2) then
		open select_list_2;
		loop
			fetch select_list_2 into v_name_sg_fk, v_level0, v_level1, v_level2, v_url;
			exit when select_list_2%NOTFOUND;
			htp.p('<tr><td><font color="#bbbbbb">'|| v_level2 ||'</td><td><a href="list_adm.select_list?p_list_type_fk='|| p_list_type_fk ||'&p_level_no='|| v_next_level ||'&p_level0='|| v_level0 ||'&p_level1='|| v_level1 ||'&p_level2='|| v_level2 ||'">'|| get.sg_name(v_name_sg_fk) ||'</a> ('|| html.popup(v_url, v_url, 750, 650, 1) ||')</td><td align="right">'); html.button_link('UPDATE', 'list_adm.update_list?p_list_pk='||v_level2); htp.p('</td><td>'); html.button_link('DELETE', 'list_adm.delete_list?p_list_pk='||v_level2); htp.p('</td></tr>');
		end loop;
		close select_list_2;

	elsif (p_level_no = 3) then
		open select_list_3;
		loop
			fetch select_list_3 into v_name_sg_fk, v_level0, v_level1, v_level2, v_level3, v_url;
			exit when select_list_3%NOTFOUND;
			htp.p('<tr><td><font color="#bbbbbb">'|| v_level3 ||'</td><td><a href="list_adm.select_list?p_list_type_fk='|| p_list_type_fk ||'&p_level_no='|| v_next_level ||'&p_level0='|| v_level0 ||'&p_level1='|| v_level1 ||'&p_level2='|| v_level2 ||'&p_level3='|| v_level3 ||'">'|| get.sg_name(v_name_sg_fk) ||'</a> ('|| html.popup(v_url, v_url, 750, 650, 1) ||')</td><td align="right">'); html.button_link('UPDATE', 'list_adm.update_list?p_list_pk='||v_level3); htp.p('</td><td>'); html.button_link('DELETE', 'list_adm.delete_list?p_list_pk='||v_level3); htp.p('</td></tr>');
		end loop;
		close select_list_3;

	elsif (p_level_no = 4) then
		open select_list_4;
		loop
			fetch select_list_4 into v_name_sg_fk, v_level0, v_level1, v_level2, v_level3, v_level4, v_url;
			exit when select_list_4%NOTFOUND;
			htp.p('<tr><td><font color="#bbbbbb">'|| v_level4 ||'</td><td><a href="list_adm.select_list?p_list_type_fk='|| p_list_type_fk ||'&p_level_no='|| v_next_level ||'&p_level0='|| v_level0 ||'&p_level1='|| v_level1 ||'&p_level2='|| v_level2 ||'&p_level3='|| v_level3 ||'&p_level4='|| v_level4 ||'">'|| get.sg_name(v_name_sg_fk) ||'</a> ('|| html.popup(v_url, v_url, 750, 650, 1) ||')</td><td align="right">'); html.button_link('UPDATE', 'list_adm.update_list?p_list_pk='||v_level4); htp.p('</td><td>'); html.button_link('DELETE', 'list_adm.delete_list?p_list_pk='||v_level4); htp.p('</td></tr>');
		end loop;
		close select_list_4;

	elsif (p_level_no = 5) then
		open select_list_5;
		loop
			fetch select_list_5 into v_name_sg_fk, v_level0, v_level1, v_level2, v_level3, v_level4, v_level5, v_url;
			exit when select_list_5%NOTFOUND;
			htp.p('<tr><td><font color="#bbbbbb">'|| v_level5 ||'</td><td>'|| get.sg_name(v_name_sg_fk) ||' ('|| html.popup(v_url, v_url, 750, 650, 1) ||')</td><td align="right">'); html.button_link('UPDATE', 'list_adm.update_list?p_list_pk='||v_level5); htp.p('</td><td>'); html.button_link('DELETE', 'list_adm.delete_list?p_list_pk='||v_level5); htp.p('</td></tr>');
		end loop;
		close select_list_5;
	end if;




	/* kode til insert form */
	html.b_form('list_adm.insert_list');
	html.b_table('40%');
	htp.p('
		<tr><td colspan="2">&nbsp;</td></tr>
		<input type="hidden" name="p_list_type_fk" value="'|| p_list_type_fk ||'">');

	if (p_level_no=1) then
		htp.p('<input type="hidden" name="p_above_list" value="'|| p_level0 ||'">');
	elsif (p_level_no=2) then
		htp.p('<input type="hidden" name="p_above_list" value="'|| p_level1 ||'">');
	elsif (p_level_no=3) then
		htp.p('<input type="hidden" name="p_above_list" value="'|| p_level2 ||'">');
	elsif (p_level_no=4) then
		htp.p('<input type="hidden" name="p_above_list" value="'|| p_level3 ||'">');
	elsif (p_level_no=5) then
		htp.p('<input type="hidden" name="p_above_list" value="'|| p_level4 ||'">');
	end if;

	htp.p('	<tr bgcolor="#eeeeee"><td colspan="2" align="center"><b>add new level '''|| p_level_no ||''' list</b></td></tr>
		<tr><td>name:</td><td><input type="Text" name="p_name" size="20" maxlength="50"></td></tr>
		<tr><td>url:</td><td><input type="Text" name="p_url" size="50" maxlength="250"></td></tr>
		<tr><td colspan="2" align="right">'); html.submit_link('insert'); htp.p('
		</td></tr>
	');
	html.e_table;
	html.e_form;


	html.e_table;
	html.e_box;
	html.e_adm_page;

end if;

END;
END select_list;



---------------------------------------------------------
-- Name: 	insert_list
-- Type: 	procedure
-- What: 	inserts the entry into the database
-- Author:  	Frode Klevstul
-- Start date: 	13.07.2000
---------------------------------------------------------
PROCEDURE insert_list
		(
			p_list_type_fk	in list.list_type_fk%type	default NULL,
			p_list_pk	in list.list_pk%type		default NULL,
			p_above_list	in list.list_pk%type		default NULL,
			p_name		in string_group.name%type	default NULL,
			p_url		in list.url%type		default NULL
                )
IS
BEGIN
DECLARE

	v_list_pk 		list.list_pk%type 			default NULL;	-- henter ut pk av sekvens
	v_check 		number					default 0;
	v_string_group_pk	string_group.string_group_pk%type	default NULL;

	v_level0		list.level0%type			default NULL;
	v_level1		list.level1%type			default NULL;
	v_level2		list.level2%type			default NULL;
	v_level3		list.level3%type			default NULL;
	v_level4		list.level4%type			default NULL;
	v_level5		list.level5%type			default NULL;

	v_level_no		number					default NULL;

BEGIN
if ( login.timeout('list_adm.startup')>0 AND login.right('list_adm')>0 ) then

	-- sjekker om det allerede finnes en forekomst med dette navnet
	if ( p_list_pk IS NOT NULL ) then
		SELECT count(*) INTO v_check
		FROM string_group, list
		WHERE name = p_name
		AND name_sg_fk = string_group_pk
		AND list_pk <> p_list_pk;
	else
		SELECT count(*) INTO v_check
		FROM string_group
		WHERE name = p_name;
	end if;

	if (v_check > 0 OR p_name IS NULL) then
		htp.p( get.msg(1, 'there already exists an entry with name = <b>'||p_name||'</b>') );
		html.e_adm_page; -- slutter siden
	else
	begin
		-- henter ut n�kler dersom vi ikke skal oppdatere
		if (p_list_pk IS NULL) then
			-- henter ut neste nummer i sekvensen til tabellen
			SELECT 	list_seq.NEXTVAL
			INTO	v_list_pk
			FROM 	dual;

			-- henter ut neste nummer i sekvensen til tabellen
			SELECT 	string_group_seq.NEXTVAL
			INTO 	v_string_group_pk
			FROM 	dual;
		end if;

		-- setter riktige "level" verdier
		if (p_above_list IS NOT NULL) then

			if (p_list_pk IS NULL) then	-- INSERT av forekomster
				SELECT 	level0, level1, level2, level3, level4, level5
				INTO	v_level0, v_level1, v_level2, v_level3, v_level4, v_level5
				FROM	list
				WHERE	list_pk = p_above_list;
			else				-- UPDATE av forekomster
				SELECT 	level0, level1, level2, level3, level4, level5
				INTO	v_level0, v_level1, v_level2, v_level3, v_level4, v_level5
				FROM	list
				WHERE	list_pk = p_list_pk;
			end if;

			if (v_level1 IS NULL) then
				v_level1 := v_list_pk;
			elsif (v_level2 IS NULL) then
				v_level2 := v_list_pk;
			elsif (v_level3 IS NULL) then
				v_level3 := v_list_pk;
			elsif (v_level4 IS NULL) then
				v_level4 := v_list_pk;
			elsif (v_level5 IS NULL) then
				v_level5 := v_list_pk;
			end if;
		elsif (p_list_pk IS NULL) then
			v_level0 := v_list_pk;
		end if;

		-- INSERT
		if (p_list_pk IS NULL AND p_name IS NOT NULL) then

			-- legger inn i databasen
			INSERT INTO string_group
			(string_group_pk, name, description)
			VALUES (v_string_group_pk, p_name, '[entry inserted with LIST_ADM package]');
			commit;

			INSERT INTO list
			(list_pk, name_sg_fk, list_type_fk, level0, level1, level2, level3, level4, level5, url)
			VALUES (v_list_pk, v_string_group_pk, p_list_type_fk, v_level0, v_level1, v_level2, v_level3, v_level4, v_level5, p_url);
			commit;

		elsif (p_name IS NOT NULL) then -- UPDATE
		begin

			UPDATE	list
			SET	url = p_url
			WHERE	list_pk = p_list_pk;
			commit;

			-- oppdaterer ikke navnet dersom det finnes fra f�r
			v_check := 0;
			SELECT 	count(*) INTO v_check
			FROM	string_group
			WHERE	name = p_name;

			if (v_check = 0) then
				UPDATE 	string_group
				SET	name = p_name
				WHERE	string_group_pk = (
						SELECT 	name_sg_fk
						FROM	list
						WHERE	list_pk = p_list_pk);
				commit;
			end if;
		end;
		end if;
	end;
	end if;

	-- sender brukeren videre etter update eller insert
	if ( v_level5 IS NOT NULL ) then
		v_level_no := 5;
		select_list(p_list_type_fk, v_level_no, v_level0, v_level1, v_level2, v_level3, v_level4);
	elsif ( v_level4 IS NOT NULL ) then
		v_level_no := 4;
		select_list(p_list_type_fk, v_level_no, v_level0, v_level1, v_level2, v_level3);
	elsif ( v_level3 IS NOT NULL ) then
		v_level_no := 3;
		select_list(p_list_type_fk, v_level_no, v_level0, v_level1, v_level2);
	elsif ( v_level2 IS NOT NULL ) then
		v_level_no := 2;
		select_list(p_list_type_fk, v_level_no, v_level0, v_level1);
	elsif ( v_level1 IS NOT NULL ) then
		v_level_no := 1;
		select_list(p_list_type_fk, v_level_no, v_level0);
	else
		select_list(p_list_type_fk);
	end if;

end if;

END;
END insert_list;



---------------------------------------------------------
-- Name: 	update_list
-- Type: 	procedure
-- What: 	edit an entry
-- Author:  	Frode Klevstul
-- Start date: 	14.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE update_list
		(
			p_list_pk 		in list.list_pk%type	default NULL
                )
IS
BEGIN
DECLARE

	v_name_sg_fk	list.name_sg_fk%type	default NULL;
	v_above_list	list.list_pk%type	default NULL;
	v_list_type_fk	list.list_type_fk%type	default NULL;

	v_level0		list.level0%type			default NULL;
	v_level1		list.level1%type			default NULL;
	v_level2		list.level2%type			default NULL;
	v_level3		list.level3%type			default NULL;
	v_level4		list.level4%type			default NULL;
	v_level5		list.level5%type			default NULL;
	v_url			list.url%type				default NULL;

BEGIN

if ( login.timeout('list_adm.startup')>0 AND login.right('list_adm')>0 ) then

	-- finner listen som er over i hierarkiet
	SELECT 	name_sg_fk, level0, level1, level2, level3, level4, level5, url, list_type_fk
	INTO	v_name_sg_fk, v_level0, v_level1, v_level2, v_level3, v_level4, v_level5, v_url, v_list_type_fk
	FROM	list
	WHERE	list_pk = p_list_pk;


	if ( v_level5 IS NOT NULL ) then
		v_above_list := v_level4;
	elsif ( v_level4 IS NOT NULL ) then
		v_above_list := v_level3;
	elsif ( v_level3 IS NOT NULL ) then
		v_above_list := v_level2;
	elsif ( v_level2 IS NOT NULL ) then
		v_above_list := v_level1;
	elsif ( v_level1 IS NOT NULL ) then
		v_above_list := v_level0;
	else
		v_above_list := NULL;
	end if;


	html.b_adm_page('list_adm');
	html.b_box('update list');
	html.b_form('list_adm.insert_list');
	html.b_table;

	htp.p('
		<input type="hidden" name="p_list_pk" value="'|| p_list_pk ||'">
		<input type="hidden" name="p_list_type_fk" value="'|| v_list_type_fk ||'">
		<input type="hidden" name="p_above_list" value="'|| v_above_list ||'">
		<tr><td colspan="2" align="center"><b>update an entry in the table "list"</b></td></tr>
		<tr><td>name:</td><td><input type="Text" name="p_name" size="30" maxlength="50" value="'|| get.sg_name(v_name_sg_fk) ||'"></td></tr>
		<tr><td>url:</td><td><input type="Text" name="p_url" size="30" maxlength="50" value="'|| v_url ||'"></td></tr>
		<tr><td colspan="2">'); html.submit_link('update'); htp.p('</td></tr>
	');

	html.e_table;
	html.e_form;
	html.e_box;
	html.e_adm_page;


end if;
END;
END update_list;



---------------------------------------------------------
-- Name: 	delete_list
-- Type: 	procedure
-- What: 	confirm a deletion of an entry in the database
-- Author:  	Frode Klevstul
-- Start date: 	14.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE delete_list
		(
			p_list_pk 	in list.list_pk%type	default NULL
                )
IS
BEGIN
DECLARE

	v_name_sg_fk	list.name_sg_fk%type		default NULL;

BEGIN

if ( login.timeout('list_adm.startup')>0 AND login.right('list_adm')>0 ) then

	SELECT 	name_sg_fk INTO v_name_sg_fk
	FROM 	list
	WHERE 	list_pk = p_list_pk;

	html.b_adm_page('list_adm');
	html.b_box('delete list');
	html.b_form('list_adm.delete_list_2');
	html.b_table;

	htp.p('
		<input type="hidden" name="p_list_pk" value="'|| p_list_pk ||'">
		<tr><td colspan="2">Are you sure you want to delete the entry <b>'|| get.sg_name(v_name_sg_fk) ||'</b> with pk <b>'||p_list_pk||'</b>?</td></tr>
		<tr><td><input type="submit" name="p_confirm" value="yes"></td><td align="right"><input type="submit" name="p_confirm" value="no"></td></tr>
		</table>
		</form>
	');

	html.e_table;
	html.e_form;
	html.e_box;
	html.e_adm_page;

end if;

END;
END delete_list;


---------------------------------------------------------
-- Name: 	delete_list_2
-- Type: 	procedure
-- What:
-- Author:  	Frode Klevstul
-- Start date: 	14.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE delete_list_2
		(
			p_list_pk 		in list.list_pk%type		default NULL,
			p_confirm		in varchar2			default NULL
                )
IS
BEGIN
DECLARE

	v_list_type_fk		list.list_type_fk%type			default NULL;
	v_level0		list.level0%type			default NULL;
	v_level1		list.level1%type			default NULL;
	v_level2		list.level2%type			default NULL;
	v_level3		list.level3%type			default NULL;
	v_level4		list.level4%type			default NULL;
	v_level5		list.level5%type			default NULL;
	v_level_no		number					default NULL;

	v_name_sg_fk		list.name_sg_fk%type			default NULL;

BEGIN

if ( login.timeout('list_adm.startup')>0 AND login.right('list_adm')>0 ) then

	SELECT 	level0, level1, level2, level3, level4, level5, list_type_fk
	INTO	v_level0, v_level1, v_level2, v_level3, v_level4, v_level5, v_list_type_fk
	FROM	list
	WHERE	list_pk = p_list_pk;

	-- sletter listene (med under-lister)
	if (p_confirm = 'yes') then
		SELECT 	name_sg_fk INTO v_name_sg_fk
		FROM	list
		WHERE	list_pk = p_list_pk;

		DELETE FROM string_group
		WHERE string_group_pk = v_name_sg_fk;
		commit;

		if ( v_level1 IS NULL ) then
			DELETE FROM list
			WHERE level0 = v_level0;
			commit;
		elsif ( v_level2 IS NULL ) then
			DELETE FROM list
			WHERE level1 = v_level1;
			commit;
		elsif ( v_level3 IS NULL ) then
			DELETE FROM list
			WHERE level2 = v_level2;
			commit;
		elsif ( v_level4 IS NULL ) then
			DELETE FROM list
			WHERE level3 = v_level3;
			commit;
		elsif ( v_level5 IS NULL ) then
			DELETE FROM list
			WHERE level4 = v_level4;
			commit;
		else
			DELETE FROM list
			WHERE list_pk = p_list_pk;
			commit;
		end if;
	end if;


	-- sender brukeren riktig videre, etter delete
	if ( v_level5 IS NOT NULL ) then
		v_level_no := 5;
		select_list(v_list_type_fk, v_level_no, v_level0, v_level1, v_level2, v_level3, v_level4);
	elsif ( v_level4 IS NOT NULL ) then
		v_level_no := 4;
		select_list(v_list_type_fk, v_level_no, v_level0, v_level1, v_level2, v_level3);
	elsif ( v_level3 IS NOT NULL ) then
		v_level_no := 3;
		select_list(v_list_type_fk, v_level_no, v_level0, v_level1, v_level2);
	elsif ( v_level2 IS NOT NULL ) then
		v_level_no := 2;
		select_list(v_list_type_fk, v_level_no, v_level0, v_level1);
	elsif ( v_level1 IS NOT NULL ) then
		v_level_no := 1;
		select_list(v_list_type_fk, v_level_no, v_level0);
	else
		select_list(v_list_type_fk);
	end if;

end if;

END;
END delete_list_2;



-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/

show errors;
