set define off
PROMPT *** package: MENU_USER ***

CREATE OR REPLACE PACKAGE menu_user IS

	PROCEDURE startup;
	PROCEDURE overview
		(
			p_organization_pk		in organization.organization_pk%type	default NULL
		);
	PROCEDURE view_menu
		(
			p_organization_pk			in organization.organization_pk%type				default NULL,
			p_menu_type_pk				in menu_type.menu_type_pk%type						default NULL,
			p_menu_serving_type_pk		in menu_serving_type.menu_serving_type_pk%type		default NULL,
			p_menu_object_type_pk		in menu_object_type.menu_object_type_pk%type		default NULL,
			p_command					in varchar2											default NULL
		);
	PROCEDURE menu_object_details
		(
			p_menu_object_pk			in menu_object.menu_object_pk%type					default NULL,
			p_organization_pk			in organization.organization_pk%type				default NULL,
			p_order_number				in menu_object.order_number%type					default NULL,
			p_title						in menu_object_text.title%type						default NULL,
			p_description				in menu_object_text.description%type				default NULL,
			p_title_spesification		in number											default NULL
		);
	
END;
/
show errors;


CREATE OR REPLACE PACKAGE BODY menu_user IS

---------------------------------------------------------
-- Name:        startup
-- Type:        procedure
-- What:        start procedure
-- Author:      Frode Klevstul
-- Start date:  25.12.2001
-- Desc:
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN
	htp.p('
		menu_user.sql
	');
END startup;



---------------------------------------------------------
-- Name:        overview
-- Type:        procedure
-- What:        an overviw over a menu
-- Author:      Frode Klevstul
-- Start date:  25.12.2001
-- Desc:
---------------------------------------------------------
PROCEDURE overview
	(
		p_organization_pk		in organization.organization_pk%type			default NULL
	)
IS
BEGIN
DECLARE

	v_language_pk			la.language_pk%type									default get.lan;
	
	CURSOR	c_select_menu_type IS
	SELECT	mt.menu_type_pk, s.string
	FROM	menu_type mt, menu m, string_group sg, groups g, strng s
	WHERE	m.organization_fk = p_organization_pk
	AND		m.menu_type_fk = mt.menu_type_pk
	AND		mt.name_sg_fk = sg.string_group_pk
	AND		g.string_group_fk = sg.string_group_pk
	AND		g.string_fk = s.string_pk
	AND		s.language_fk = v_language_pk
	AND		m.activated = 1
	ORDER BY s.string;

	v_menu_type_pk			menu_type.menu_type_pk%type							default NULL;
	v_string				strng.string%type									default NULL;
	v_name					string_group.name%type								default NULL;

	CURSOR	c_select_menu_serving_type IS
	SELECT	DISTINCT mst.menu_serving_type_pk, s.string, sg.name
	FROM	menu_serving_type mst, menu m, menu_object mo, string_group sg, groups g, strng s
	WHERE	m.organization_fk = p_organization_pk
	AND		m.menu_type_fk = v_menu_type_pk
	AND		mst.name_sg_fk = sg.string_group_pk
	AND		g.string_group_fk = sg.string_group_pk
	AND		g.string_fk = s.string_pk
	AND		s.language_fk = v_language_pk
	AND		mo.menu_fk = m.menu_pk
	AND		mo.menu_serving_type_fk = mst.menu_serving_type_pk
	ORDER BY sg.name;

	v_menu_serving_type_pk	menu_serving_type.menu_serving_type_pk%type			default NULL;

	CURSOR	c_select_menu_object_type IS
	SELECT	DISTINCT mot.menu_object_type_pk, s.string
	FROM	menu_object_type mot, menu m, menu_object mo, string_group sg, groups g, strng s
	WHERE	m.organization_fk = p_organization_pk
	AND		m.menu_type_fk = v_menu_type_pk
	AND		mot.name_sg_fk = sg.string_group_pk
	AND		g.string_group_fk = sg.string_group_pk
	AND		g.string_fk = s.string_pk
	AND		s.language_fk = v_language_pk
	AND		mo.menu_fk = m.menu_pk
	AND		mo.menu_serving_type_fk = v_menu_serving_type_pk
	AND		mo.menu_object_type_fk = mot.menu_object_type_pk
	AND		mo.activated = 1
	ORDER BY s.string;

	v_menu_object_type_pk			menu_object_type.menu_object_type_pk%type	default NULL;


BEGIN

	html.b_box( get.txt('menu_overview'), '100%', 'menu_user.overview');

	open c_select_menu_type;
	loop
		fetch c_select_menu_type into v_menu_type_pk, v_string;
		exit when c_select_menu_type%NOTFOUND;
		
		htp.p('<b><a href="menu_user.view_menu?p_organization_pk='||p_organization_pk||'&p_menu_type_pk='||v_menu_type_pk||'">'||v_string ||'</b></a><br>');
		
		open c_select_menu_serving_type;
		loop
			fetch c_select_menu_serving_type into v_menu_serving_type_pk, v_string, v_name;
			exit when c_select_menu_serving_type%NOTFOUND;
			
			htp.p('<dd> <a href="menu_user.view_menu?p_organization_pk='||p_organization_pk||'&p_menu_serving_type_pk='||v_menu_serving_type_pk||'">'||v_string||'</a><br>');
			
			open c_select_menu_object_type;
			loop
				fetch c_select_menu_object_type into v_menu_object_type_pk, v_string;
				exit when c_select_menu_object_type%NOTFOUND;
				
				htp.p('<dd><dd> <a href="menu_user.view_menu?p_organization_pk='||p_organization_pk||'&p_menu_serving_type_pk='||v_menu_serving_type_pk||'&p_menu_object_type_pk='||v_menu_object_type_pk||'">'||v_string||'</a><br>');
			
			end loop;
			close c_select_menu_object_type;
		
		end loop;
		close c_select_menu_serving_type;
		
	
	end loop;
	close c_select_menu_type;

	html.e_box;

END;
END overview;




---------------------------------------------------------
-- Name:        view_menu
-- Type:        procedure
-- What:        shows the overview (and a selected choice)
-- Author:      Frode Klevstul
-- Start date:  07.01.2002
-- Desc:
---------------------------------------------------------
PROCEDURE view_menu
	(
		p_organization_pk			in organization.organization_pk%type				default NULL,
		p_menu_type_pk				in menu_type.menu_type_pk%type						default NULL,
		p_menu_serving_type_pk		in menu_serving_type.menu_serving_type_pk%type		default NULL,
		p_menu_object_type_pk		in menu_object_type.menu_object_type_pk%type		default NULL,
		p_command					in varchar2											default NULL
	)
IS
BEGIN
DECLARE

	v_menu_object_pk		menu_object.menu_object_pk%type				default NULL;
	v_order_number			menu_object.order_number%type				default NULL;
	v_title					menu_object_text.title%type					default NULL;
	v_name_sg_fk_1			string_group.string_group_pk%type			default NULL;
	v_name_sg_fk_2			string_group.string_group_pk%type			default NULL;
	v_name_sg_fk_3			string_group.string_group_pk%type			default NULL;
	v_name_sg_fk_1_old		string_group.string_group_pk%type			default 0;
	v_name_sg_fk_2_old		string_group.string_group_pk%type			default 0;
	v_name_sg_fk_3_old		string_group.string_group_pk%type			default 0;
	v_language_pk			la.language_pk%type							default get.lan;
	v_description			menu_object_text.description%type			default NULL;
	v_string				strng.string%type							default NULL;

	CURSOR	select_mot IS
	SELECT	mo.menu_object_pk, mo.order_number, mote.title, mote.description, mt.name_sg_fk, mst.name_sg_fk, moty.name_sg_fk
	FROM	menu_object mo, menu_type mt, menu_object_text mote, menu_object_type moty, menu_serving_type mst, menu m
	WHERE	mo.menu_object_type_fk = moty.menu_object_type_pk
	AND		moty.menu_type_fk = mt.menu_type_pk
	AND		mo.menu_serving_type_fk = mst.menu_serving_type_pk
	AND		mote.menu_object_fk = mo.menu_object_pk
	AND		mote.language_fk = v_language_pk
	AND		mo.menu_fk = m.menu_pk
	AND		moty.menu_object_type_pk = p_menu_object_type_pk
	AND		m.organization_fk = p_organization_pk
	AND		mo.activated = 1
	AND		mst.menu_serving_type_pk = p_menu_serving_type_pk
	ORDER BY mo.order_number;

	CURSOR	select_mst IS
	SELECT	mo.menu_object_pk, mo.order_number, mote.title, mote.description, mt.name_sg_fk, mst.name_sg_fk, moty.name_sg_fk, s.string
	FROM	menu_object mo, menu_type mt, menu_object_text mote, menu_object_type moty, menu_serving_type mst, menu m, string_group sg, groups g, strng s
	WHERE	mo.menu_object_type_fk = moty.menu_object_type_pk
	AND		moty.menu_type_fk = mt.menu_type_pk
	AND		mo.menu_serving_type_fk = mst.menu_serving_type_pk
	AND		mote.menu_object_fk = mo.menu_object_pk
	AND		mote.language_fk = v_language_pk
	AND		mo.menu_fk = m.menu_pk
	AND		mst.menu_serving_type_pk = p_menu_serving_type_pk
	AND		m.organization_fk = p_organization_pk
	AND		mo.activated = 1
	AND		moty.name_sg_fk = sg.string_group_pk
	AND		g.string_group_fk = sg.string_group_pk
	AND		g.string_fk = s.string_pk
	AND		s.language_fk = v_language_pk
	ORDER BY s.string, mo.order_number;

	CURSOR	select_mt IS
	SELECT	mo.menu_object_pk, mo.order_number, mote.title, mote.description, mt.name_sg_fk, mst.name_sg_fk, moty.name_sg_fk
	FROM	menu m, menu_object mo, menu_object_text mote, menu_type mt, menu_object_type moty, menu_serving_type mst, string_group sg, string_group sg2, groups g2, strng s2
	WHERE	mo.menu_fk = m.menu_pk
	AND		mote.menu_object_fk = mo.menu_object_pk
	AND		mote.language_fk = v_language_pk
	AND		m.organization_fk = p_organization_pk
	AND		m.menu_type_fk = p_menu_type_pk
	AND		mo.activated = 1
	AND		mo.menu_object_type_fk = moty.menu_object_type_pk
	AND		mo.menu_serving_type_fk = mst.menu_serving_type_pk
	AND		mt.menu_type_pk = moty.menu_type_fk
	AND		mst.name_sg_fk = sg.string_group_pk
	AND		moty.name_sg_fk = sg2.string_group_pk
	AND		g2.string_group_fk = sg2.string_group_pk
	AND		g2.string_fk = s2.string_pk
	AND		s2.language_fk = v_language_pk
	ORDER BY sg.name, s2.string, mo.order_number;

	CURSOR	select_all IS
	SELECT	mo.menu_object_pk, mo.order_number, mote.title, mote.description, mt.name_sg_fk, mst.name_sg_fk, moty.name_sg_fk
	FROM	menu m, menu_object mo, menu_object_text mote, menu_type mt, menu_object_type moty, menu_serving_type mst, string_group sg, string_group sg2, groups g2, strng s2
	WHERE	mo.menu_fk = m.menu_pk
	AND		mote.menu_object_fk = mo.menu_object_pk
	AND		mote.language_fk = v_language_pk
	AND		m.organization_fk = p_organization_pk
	AND		mo.activated = 1
	AND		mo.menu_object_type_fk = moty.menu_object_type_pk
	AND		mo.menu_serving_type_fk = mst.menu_serving_type_pk
	AND		mt.menu_type_pk = moty.menu_type_fk
	AND		mst.name_sg_fk = sg.string_group_pk
	AND		moty.name_sg_fk = sg2.string_group_pk
	AND		g2.string_group_fk = sg2.string_group_pk
	AND		g2.string_fk = s2.string_pk
	AND		s2.language_fk = v_language_pk
	ORDER BY mt.name_sg_fk, sg.name, s2.string, mo.order_number;

	v_box_title				varchar2(100);

BEGIN

	-- begynner siden

	if (p_command = 'print') then
        html.b_page_2('600', NULL, NULL, NULL, 'JavaScript:self.print();');
	else
		html.b_page;
		html.org_menu(p_organization_pk);
	end if;

	-- starter en tabell
	html.b_table;

	-- skriver ikke ut venstre stolpe dersom vi skal printe
	if (p_command is null) then

		-- venstre stolpe
		htp.p('<tr><td width="30%" valign="top">');

		-- skriver ut oversikten over alle retter
		overview(p_organization_pk);

		-- h�yre side
		htp.p('</td><td valign="top">');

	else

		htp.p('<tr><td>');

	end if;

	
		-- h�yre side
		htp.p('</td><td valign="top">');
	

		-- ordner tittelen til boksen
		if (p_menu_object_type_pk is not null) then
		
			SELECT	name_sg_fk
			INTO	v_name_sg_fk_1
			FROM	menu_object_type
			WHERE	menu_object_type_pk = p_menu_object_type_pk;
	
			v_box_title := get.text(v_name_sg_fk_1);

		elsif (p_menu_serving_type_pk is not null) then
	
			SELECT	name_sg_fk
			INTO	v_name_sg_fk_1
			FROM	menu_serving_type
			WHERE	menu_serving_type_pk = p_menu_serving_type_pk;
	
			v_box_title := get.text(v_name_sg_fk_1);
	
		elsif (p_menu_type_pk is not null) then

			SELECT	name_sg_fk
			INTO	v_name_sg_fk_1
			FROM	menu_type
			WHERE	menu_type_pk = p_menu_type_pk;

			v_box_title := get.text(v_name_sg_fk_1);

		elsif (p_command = 'print') then

			v_box_title := get.oname(p_organization_pk) ||' - in4mant.com';

		else

			v_box_title := get.txt('menu_user_desc_title') ||' '|| get.txt('for') ||' '|| get.oname(p_organization_pk);
		
		end if;

		html.b_box( v_box_title );
		html.b_table('100%', 0);

		if (p_command is null) then
			htp.p('<tr><td align="right">'|| html.popup( get.txt('print_page'),'menu_user.view_menu?p_organization_pk='||p_organization_pk||'&p_command=print','650', '700', '0', '1') ||'</td></tr>');
		end if;

		-- henter ut rettene
		if (p_menu_object_type_pk is not null AND p_organization_pk is not null AND p_menu_serving_type_pk is not null) then
	
			open select_mot;
			loop
				fetch select_mot into v_menu_object_pk, v_order_number, v_title, v_description, v_name_sg_fk_1, v_name_sg_fk_2, v_name_sg_fk_3;
				exit when select_mot%NOTFOUND;

				if ( v_name_sg_fk_1 <> v_name_sg_fk_1_old) then
					htp.p('<tr><td><center><h1>'|| get.text(v_name_sg_fk_1) ||'</h1></center></td></tr>');
					v_name_sg_fk_1_old := v_name_sg_fk_1;
				end if;
				if ( v_name_sg_fk_2 <> v_name_sg_fk_2_old) then
					htp.p('<tr><td><center><h2>'|| get.text(v_name_sg_fk_2) ||'</h2></center></td></tr>');
					v_name_sg_fk_2_old := v_name_sg_fk_2;
				end if;
				if ( v_name_sg_fk_3 <> v_name_sg_fk_3_old) then
					htp.p('
						<tr><td><center><b>'|| get.text(v_name_sg_fk_3) ||'</b></center></td></tr>
						<tr><td>&nbsp;</td></tr>
					');
					v_name_sg_fk_3_old := v_name_sg_fk_3;
				end if;

				menu_object_details(v_menu_object_pk, p_organization_pk, v_order_number, v_title, v_description);
			end loop;
			close select_mot;
		
		elsif (p_menu_serving_type_pk is not null AND p_organization_pk is not null) then
		
			open select_mst;
			loop
				fetch select_mst into v_menu_object_pk, v_order_number, v_title, v_description, v_name_sg_fk_1, v_name_sg_fk_2, v_name_sg_fk_3, v_string;
				exit when select_mst%NOTFOUND;

				if ( v_name_sg_fk_1 <> v_name_sg_fk_1_old) then
					htp.p('<tr><td><center><h1>'|| get.text(v_name_sg_fk_1) ||'</h1></center></td></tr>');
					v_name_sg_fk_1_old := v_name_sg_fk_1;
				end if;
				if ( v_name_sg_fk_2 <> v_name_sg_fk_2_old) then
					htp.p('<tr><td><center><h2>'|| get.text(v_name_sg_fk_2) ||'</h2></center></td></tr>');
					v_name_sg_fk_2_old := v_name_sg_fk_2;
				end if;
				if ( v_name_sg_fk_3 <> v_name_sg_fk_3_old) then
					htp.p('
						<tr><td><center><b>'|| get.text(v_name_sg_fk_3) ||'</b></center></td></tr>
						<tr><td>&nbsp;</td></tr>
					');
					v_name_sg_fk_3_old := v_name_sg_fk_3;
				end if;

				menu_object_details(v_menu_object_pk, p_organization_pk, v_order_number, v_title, v_description);
			end loop;
			close select_mst;

		elsif (p_menu_type_pk is not null) then

			open select_mt;
			loop
				fetch select_mt into v_menu_object_pk, v_order_number, v_title, v_description, v_name_sg_fk_1, v_name_sg_fk_2, v_name_sg_fk_3;
				exit when select_mt%NOTFOUND;
				
				if ( v_name_sg_fk_1 <> v_name_sg_fk_1_old) then
					htp.p('<tr><td><center><h1>'|| get.text(v_name_sg_fk_1) ||'</h1></center></td></tr>');
					v_name_sg_fk_1_old := v_name_sg_fk_1;
				end if;
				if ( v_name_sg_fk_2 <> v_name_sg_fk_2_old) then
					htp.p('<tr><td><center><h2>'|| get.text(v_name_sg_fk_2) ||'</h2></center></td></tr>');
					v_name_sg_fk_2_old := v_name_sg_fk_2;
				end if;
				if ( v_name_sg_fk_3 <> v_name_sg_fk_3_old) then
					htp.p('
						<tr><td><center><b>'|| get.text(v_name_sg_fk_3) ||'</b></center></td></tr>
						<tr><td>&nbsp;</td></tr>
					');
					v_name_sg_fk_3_old := v_name_sg_fk_3;
				end if;
				
				menu_object_details(v_menu_object_pk, p_organization_pk, v_order_number, v_title, v_description, 1);
			end loop;
			close select_mt;

		elsif (p_command = 'print') then

			open select_all;
			loop
				fetch select_all into v_menu_object_pk, v_order_number, v_title, v_description, v_name_sg_fk_1, v_name_sg_fk_2, v_name_sg_fk_3;
				exit when select_all%NOTFOUND;
				
				if ( v_name_sg_fk_1 <> v_name_sg_fk_1_old) then
					htp.p('<tr><td><center><h1>'|| get.text(v_name_sg_fk_1) ||'</h1></center></td></tr>');
					v_name_sg_fk_1_old := v_name_sg_fk_1;
				end if;
				if ( v_name_sg_fk_2 <> v_name_sg_fk_2_old) then
					htp.p('<tr><td><center><h2>'|| get.text(v_name_sg_fk_2) ||'</h2></center></td></tr>');
					v_name_sg_fk_2_old := v_name_sg_fk_2;
				end if;
				if ( v_name_sg_fk_3 <> v_name_sg_fk_3_old) then
					htp.p('
						<tr><td><center><b>'|| get.text(v_name_sg_fk_3) ||'</b></center></td></tr>
						<tr><td>&nbsp;</td></tr>
					');
					v_name_sg_fk_3_old := v_name_sg_fk_3;
				end if;
				
				menu_object_details(v_menu_object_pk, p_organization_pk, v_order_number, v_title, v_description, 1);
			end loop;
			close select_all;

		else

			htp.p( get.txt('menu_user_desc') );

		end if;


		html.e_table;	
		-- slutter boksen
		html.e_box;
	

	htp.p('</td></tr>');

	-- slutter tabell
	html.e_table;

	-- slutter siden
	if (p_command is not null) then
		html.e_page_2;
	else
		html.e_page;
	end if;

END;
END view_menu;






---------------------------------------------------------
-- Name:        menu_object_details
-- Type:        procedure
-- What:        shows details about a menu_object
-- Author:      Frode Klevstul
-- Start date:  07.02.2002
-- Desc:
---------------------------------------------------------
PROCEDURE menu_object_details
	(
		p_menu_object_pk			in menu_object.menu_object_pk%type					default NULL,
		p_organization_pk			in organization.organization_pk%type				default NULL,
		p_order_number				in menu_object.order_number%type					default NULL,
		p_title						in menu_object_text.title%type						default NULL,
		p_description				in menu_object_text.description%type				default NULL,
		p_title_spesification		in number											default NULL
	)
IS
BEGIN
DECLARE

	v_price					size_price.price%type						default NULL;
	v_quantity				size_price.quantity%type					default NULL;

	CURSOR	select_size_price IS
	SELECT	quantity, price
	FROM	size_price
	WHERE	menu_object_fk = p_menu_object_pk
	ORDER BY price;

	v_name_sg_fk			string_group.string_group_pk%type			default NULL;
	v_start_time			organization_serving.start_time%type		default NULL;
	v_end_time				organization_serving.end_time%type			default NULL;

	CURSOR	select_serving_time IS
	SELECT	sp.name_sg_fk, ose.start_time, ose.end_time
	FROM 	organization_serving ose, object_served os, serving_period sp
	WHERE	os.menu_object_fk = p_menu_object_pk
	AND		os.organization_fk = p_organization_pk
	AND		os.serving_period_fk = ose.serving_period_fk
	AND		os.organization_fk = ose.organization_fk
	AND		ose.serving_period_fk = sp.serving_period_pk
	ORDER BY ose.start_time;

	v_new_title				varchar2(300)								default NULL;
	v_currency_code			currency.currency_code%type					default NULL;
	v_new_price 			varchar2(300)								default NULL;
	v_count					number										default NULL;
	v_description			menu_object_text.description%type			default NULL;
	
BEGIN

	v_currency_code := get.curr;
	v_description	:= p_description;
	
	owa_pattern.change(v_description, '\n', '<br>\n', 'g');

	-- order tittelen p� retten
	if (p_order_number is not null) then
		v_new_title := '<b>'|| p_order_number ||': '|| p_title ||'</b>';
	else
		v_new_title := '<b>'|| p_title ||'</b>';
	end if;


	-- endrer tittelen p� retten dersom er 'p_title_spesification' forskjellig fra null
	if (p_title_spesification is not null) then

		SELECT	mst.name_sg_fk
		INTO	v_name_sg_fk
		FROM	menu_serving_type mst, menu_object mo
		WHERE	mo.menu_serving_type_fk = mst.menu_serving_type_pk
		AND		mo.menu_object_pk = p_menu_object_pk;

		v_new_title := v_new_title ||' ('|| get.text(v_name_sg_fk);
	
		SELECT	mot.name_sg_fk
		INTO	v_name_sg_fk
		FROM	menu_object_type mot, menu_object mo
		WHERE	mo.menu_object_type_fk = mot.menu_object_type_pk
		AND		mo.menu_object_pk = p_menu_object_pk;

		v_new_title := v_new_title ||': '|| get.text(v_name_sg_fk)||')';

	end if;


	-- skriver ut tittel og beskrivelse
	htp.p('
		<tr><td>'||v_new_title||'</td></tr>
		<tr><td>'||v_description||'</td></tr>
	');


	-- serveringstidspunkt
	SELECT	count(*)
	INTO	v_count
	FROM 	organization_serving ose, object_served os, serving_period sp
	WHERE	os.menu_object_fk = p_menu_object_pk
	AND		os.organization_fk = p_organization_pk
	AND		os.serving_period_fk = ose.serving_period_fk
	AND		os.organization_fk = ose.organization_fk
	AND		ose.serving_period_fk = sp.serving_period_pk;

	if (v_count > 0) then
		htp.p('<tr><td>'|| get.txt('serving_period_for_org') ||':</td></tr>');
		open select_serving_time;
		loop
			fetch select_serving_time into v_name_sg_fk, v_start_time, v_end_time;
			exit when select_serving_time%NOTFOUND;
			htp.p('<tr><td><dd>'|| get.text(v_name_sg_fk) ||': '|| to_char(v_start_time,get.txt('date_hour')) ||'-'|| to_char(v_start_time,get.txt('date_hour')) ||'</td></tr>');
		end loop;
		close select_serving_time;
	end if;


	-- st�rrelse og pris
	open select_size_price;
	loop
		fetch select_size_price into v_quantity, v_price;
		exit when select_size_price%NOTFOUND;
		
		if (v_quantity is not null) then
			v_new_price := v_quantity ||': '||v_price||',- '||v_currency_code;
		elsif (v_price is not null) then
			v_new_price := v_price||' '||v_currency_code;
		else
			v_new_price := '';
		end if;
		
		htp.p('<tr><td align="right">'||v_new_price||'</td></tr>');
	end loop;
	close select_size_price;

	htp.p('<tr><td colspan="2">&nbsp;</td></tr>');


END;
END menu_object_details;

-- ++++++++++++++++++++++++++++++++++++++++++++++ --

END; -- slutter pakke kroppen
/
show errors;

