set define off
PROMPT *** package: TIME_REPORT ***

CREATE OR REPLACE PACKAGE time_report IS

        PROCEDURE startup;
	PROCEDURE edit_projects
		(
			p_type		in varchar2			default NULL,
			p_project_pk	in project.project_pk%type	default NULL,
			p_name		in project.name%type		default NULL,
			p_activated	in project.activated%type	default NULL,
			p_in_statistic	in project.in_statistic%type	default NULL
		);
	PROCEDURE report_work
		(
			p_type			in varchar2					default NULL,
			p_project_pk		in project.project_pk%type			default NULL,
			p_hour			in project_report.hour%type			default NULL,
			p_min			in number					default NULL,
			p_description		in project_report.description%type		default NULL,
			p_report_date		in varchar2					default NULL,
			p_report_date_show	in varchar2					default NULL,
			p_project_report_pk	in project_report.project_report_pk%type	default NULL
		);
	PROCEDURE select_project
                (
                        p_project_pk            in project.project_pk%type              default NULL
                );
	PROCEDURE grand_total;
	PROCEDURE project_overview
		(
			p_project_pk	in project.project_pk%type	default NULL,
			p_user_pk	in usr.user_pk%type		default NULL
		);
	PROCEDURE generate_report
		(
			p_project_pk		in project.project_pk%type	default NULL,
			p_from_date		in varchar2			default NULL,
			p_from_date_show	in varchar2			default NULL,
			p_to_date		in varchar2			default NULL,
			p_to_date_show		in varchar2			default NULL,
			p_user_pk		in usr.user_pk%type		default NULL,
			p_from_check		in varchar2			default NULL,
			p_to_check		in varchar2			default NULL,
			p_project_check		in varchar2			default NULL,
			p_user_check		in varchar2			default NULL,
			p_all_check		in varchar2			default NULL
		);

END;
/
show errors;

CREATE OR REPLACE PACKAGE BODY time_report IS



---------------------------------------------------------
-- Name:        startup
-- Type:        procedure
-- What:        start procedure
-- Author:      Frode Klevstul
-- Start date:  15.02.2001
-- Desc:
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN
if ( login.timeout('time_report.startup')>0 AND login.right('time_report')>0 ) then

	html.b_adm_page('TimeReporting System');
	html.b_table;
	htp.p('
		<tr><td colspan="2" height="50">&nbsp;</td></tr>
		<tr><td colspan="2" align="center"><b>FOR WORKERS</b></td></tr>
		<tr>
			<td><a href="time_report.report_work">Report work</a></td>
			<td>Report working hours</td>
		</tr>
		<tr><td colspan="2" height="50">&nbsp;</td></tr>
		<tr><td colspan="2" height="50"><hr noshade></td></tr>
		<tr><td colspan="2" align="center"><b>FOR ADMINISTRATORS</b></td></tr>
		<tr>
			<td><a href="time_report.edit_projects">Edit projects</a></td>
			<td>Administrate projects in time report module</td>
		</tr>
		<tr>
			<td><a href="time_report.grand_total">Grand total</a></td>
			<td>Grand total, everything that is done.</td>
		</tr>
		<tr>
			<td><a href="time_report.project_overview">Project overview</a></td>
			<td>An overview over all projects</td>
		</tr>
		<tr>
			<td><a href="time_report.generate_report">Generate report</a></td>
			<td>Generate a report</td>
		</tr>
	');
	html.e_table;
	html.e_adm_page;

end if;
END startup;




---------------------------------------------------------
-- Name:        edit_projects
-- Type:        procedure
-- What:        procedure to administrate all projects
-- Author:      Frode Klevstul
-- Start date:  15.02.2001
-- Desc:
---------------------------------------------------------
PROCEDURE edit_projects
	(
		p_type		in varchar2			default NULL,
		p_project_pk	in project.project_pk%type	default NULL,
		p_name		in project.name%type		default NULL,
		p_activated	in project.activated%type	default NULL,
		p_in_statistic	in project.in_statistic%type	default NULL
	)
IS
BEGIN
DECLARE

	CURSOR	select_projects IS
	SELECT	project_pk, name, activated, in_statistic
	FROM	project
	ORDER BY name ASC;

	v_project_pk	project.project_pk%type		default NULL;
	v_name		project.name%type		default NULL;
	v_activated	project.activated%type		default NULL;
	v_in_statistic	project.in_statistic%type	default NULL;

BEGIN
if ( login.timeout('time_report.edit_projects')>0 AND login.right('time_report.edit_projects')>0 ) then

	html.b_adm_page('TimeReporting System - edit_projects');

	-- ----------------------------------------------------
	-- p_type IS NULL: lister ut alle prosjekter
	-- ----------------------------------------------------
	if (p_type IS NULL) then
		html.b_box('All projects');
		html.b_table('100%', 1);
		htp.p('<tr bgcolor="'|| get.value( 'c_'|| get.serv_name ||'_mmb' ) ||'"><td><b>name:</b></td><td><b>update:</b></td><td><b>delete:</b></td><td><b>activated:</b></td><td><b>show in stat:</b></td></tr>');

		open select_projects;
		loop
			fetch select_projects into v_project_pk, v_name, v_activated, v_in_statistic;
			exit when select_projects%NOTFOUND;
			htp.p('
				<tr>
					<td>'|| v_name ||'</td>
					<td>'); html.button_link( 'update' ,'time_report.edit_projects?p_project_pk='||v_project_pk||'&p_type=update'); htp.p('</td>
					<td>'); html.button_link( 'delete' ,'time_report.edit_projects?p_project_pk='||v_project_pk||'&p_type=delete'); htp.p('</td>
					<td>');
					if (v_activated = 1) then
						html.button_link( 'activated' ,'time_report.edit_projects?p_project_pk='||v_project_pk||'&p_type=deactivate');
					else
						html.button_link( 'not activated' ,'time_report.edit_projects?p_project_pk='||v_project_pk||'&p_type=activate');
					end if;
					htp.p('</td>
					<td>');
					if (v_in_statistic = 1) then
						html.button_link( 'in stat' ,'time_report.edit_projects?p_project_pk='||v_project_pk||'&p_type=not_in_stat');
					else
						html.button_link( 'not in stat' ,'time_report.edit_projects?p_project_pk='||v_project_pk||'&p_type=in_stat');
					end if;
					htp.p('</td>
				</tr>
			');
		end loop;
		close select_projects;

		html.e_table;
		html.e_box;


		html.b_box('New project');
		html.b_form('time_report.edit_projects');
		html.b_table('100%', 1);
		htp.p('
			<input type="hidden" name="p_type" value="insert">
			<tr><td>Project name:</td><td>'|| html.text('p_name', 30, 50) ||'</td><td>'); html.submit_link('Submit'); htp.p('</td></tr>
		');
		html.e_table;
		html.e_box;
	-- ------------------------------------------------
	-- p_type = 'insert': legger inn ny forekomst
	-- ------------------------------------------------
	elsif (p_type = 'insert') then
                SELECT project_seq.NEXTVAL
                INTO v_project_pk
                FROM dual;

		INSERT INTO project
		(project_pk, inserted_date, name, activated, in_statistic)
		VALUES (v_project_pk, sysdate, p_name, NULL, NULL);
		commit;

		html.jump_to('time_report.edit_projects');
	-- ----------------------------------------------------------------
	-- p_type = 'update': oppdaterer verdier
	-- ----------------------------------------------------------------
	elsif (p_type = 'update') then

		if (p_name IS NOT NULL) then

			UPDATE	project
			SET	name = p_name
			WHERE	project_pk = p_project_pk;
			commit;

			html.jump_to('time_report.edit_projects');
		-- ---
		-- skriver ut html form
		-- ---
		else
		        html.b_box( 'update project');
		        html.b_form('time_report.edit_projects');
		        html.b_table;

		        SELECT  name
		        INTO	v_name
		        FROM    project
		        WHERE   project_pk = p_project_pk;

		        htp.p('
		                <input type="hidden" name="p_type" value="update">
		                <input type="hidden" name="p_project_pk" value="'|| p_project_pk ||'">
		                <tr><td>name: </td><td><input type="Text" name="p_name" size="50" maxlength="50" value="'|| v_name ||'"></td></tr>
		                <tr><td colspan="2" align="right">'); html.submit_link( 'update' ); htp.p('
		                </td></tr>
		        ');

			html.e_table;
			html.e_form;
			html.e_box;
		end if;
	-- ----------------------------------------------------------------
	-- p_type = 'delete': sletter verdier fra databasen
	-- ----------------------------------------------------------------
	elsif ( owa_pattern.match(p_type,'delete') ) then

		-- ---
		-- sletter i databasen
		-- ---
		if ( owa_pattern.match(p_type,'delete_action') ) then

			DELETE FROM project
			WHERE project_pk = p_project_pk;
			commit;

			html.jump_to('time_report.edit_projects');
		-- ---
		-- skriver ut html form (confirm delete)
		-- ---
		else
		        SELECT  name
		        INTO	v_name
		        FROM    project
		        WHERE   project_pk = p_project_pk;

		        html.b_box( 'delete project' );
		        html.b_form('time_report.edit_projects');
		        html.b_table;

		        htp.p('
		                <input type="hidden" name="p_type" value="delete_action">
		                <input type="hidden" name="p_project_pk" value="'|| p_project_pk ||'">
		                <tr><td> Are you sure you want to delete the project '''|| v_name ||'''?</td></tr>
		                <tr><td>'); html.submit_link( 'delete project' ); htp.p('</td></tr>
		                <tr><td>'); html.back( 'don''t delete project' ); htp.p('</td></tr>
		        ');

			html.e_table;
			html.e_form;
			html.e_box;

		end if;
	-- --------------------------------------------------------
	-- p_type = 'activate': activate or deactivate the project
	-- --------------------------------------------------------
	elsif ( owa_pattern.match(p_type,'activate') ) then
		if ( owa_pattern.match(p_type,'deactivate') ) then
			UPDATE	project
			SET	activated = NULL
			WHERE	project_pk = p_project_pk;
			commit;
		else
			UPDATE	project
			SET	activated = 1
			WHERE	project_pk = p_project_pk;
			commit;
		end if;

		html.jump_to('time_report.edit_projects');
	-- --------------------------------------------------------
	-- p_type = 'in_stat': in or not in statistic
	-- --------------------------------------------------------
	elsif ( owa_pattern.match(p_type,'in_stat') ) then
		if ( owa_pattern.match(p_type,'not_in_stat') ) then
			UPDATE	project
			SET	in_statistic = NULL
			WHERE	project_pk = p_project_pk;
			commit;
		else
			UPDATE	project
			SET	in_statistic = 1
			WHERE	project_pk = p_project_pk;
			commit;
		end if;

		html.jump_to('time_report.edit_projects');
	else
		html.jump_to('time_report.edit_projects');
	end if;

	html.e_adm_page;

end if;
END;
END edit_projects;




---------------------------------------------------------
-- Name:        report_work
-- Type:        procedure
-- What:        procedure to report hours worked
-- Author:      Frode Klevstul
-- Start date:  20.02.2001
-- Desc:
---------------------------------------------------------
PROCEDURE report_work
	(
		p_type			in varchar2					default NULL,
		p_project_pk		in project.project_pk%type			default NULL,
		p_hour			in project_report.hour%type			default NULL,
		p_min			in number					default NULL,
		p_description		in project_report.description%type		default NULL,
		p_report_date		in varchar2					default NULL,
		p_report_date_show	in varchar2					default NULL,
		p_project_report_pk	in project_report.project_report_pk%type	default NULL
	)
IS
BEGIN
DECLARE

	CURSOR	select_report(v_user_pk in usr.user_pk%type) IS
	SELECT	project_report_pk, report_date, name, description, hour
	FROM	project, project_report
	WHERE	project_pk = project_fk
	AND	report_date > (sysdate-14)
	AND	user_fk = v_user_pk
	ORDER BY report_date ASC;

	v_report_date		project_report.report_date%type		default NULL;
	v_name			project.name%type			default NULL;
	v_description		project_report.description%type		default NULL;
	v_hour			project_report.hour%type		default NULL;
	v_user_pk		usr.user_pk%type			default NULL;
	v_project_report_pk	project_report.project_report_pk%type	default NULL;
	v_total_hour		number					default NULL;
	v_project_pk		project.project_pk%type			default NULL;

BEGIN
if ( login.timeout('time_report.report_work')>0 AND login.right('time_report')>0 ) then

	html.b_adm_page('TimeReporting System - report_work');

	v_user_pk := get.uid;

	if (p_type IS NULL) then
		html.b_box('Time report for '|| get.uname);
		html.b_table('100%', 1);
		htp.p('<tr bgcolor="'|| get.value( 'c_'|| get.serv_name ||'_mmb' ) ||'"><td><b>date:</b></td><td><b>project:</b></td><td><b>description:</b></td><td><b>hour:</b></td><td><b>&nbsp;</b></td><td><b>&nbsp;</b></td></tr>');

		open select_report(v_user_pk);
		loop
			fetch select_report into v_project_report_pk, v_report_date, v_name, v_description, v_hour;
			exit when select_report%NOTFOUND;

			htp.p('
				<tr>
					<td>'|| to_char(v_report_date, get.txt('date_short')) ||'</td>
					<td>'|| v_name ||'</td>
					<td>'|| v_description ||'</td>
					<td>'|| v_hour ||'</td>
					<td>'); html.button_link( 'update' ,'time_report.report_work?p_project_report_pk='||v_project_report_pk||'&p_type=update'); htp.p('</td>
					<td>'); html.button_link( 'delete' ,'time_report.report_work?p_project_report_pk='||v_project_report_pk||'&p_type=delete'); htp.p('</td>
				</tr>
			');

		end loop;
		close select_report;

		SELECT	sum(hour)
		INTO	v_total_hour
		FROM	project_report
		WHERE	to_char(report_date, 'MM:YYYY') = to_char(sysdate, 'MM:YYYY')
		AND	user_fk = v_user_pk;

		htp.p('<tr><td colspan="3"><b>Total hours so far this month:</b></td><td><b>'|| v_total_hour ||'&nbsp;</b></td><td colspan="2">&nbsp;</td></tr>');

		html.e_table;
		html.e_box;

		htp.p('<br><br><br>');

		html.b_box('Report new hours');
		html.b_form('time_report.report_work');
		html.b_table;
		htp.p('
			<input type="hidden" name="p_type" value="insert">
			<tr><td>Project name:</td><td>'); time_report.select_project; htp.p('</td></tr>
			<tr><td>Date:</td><td>'); html.date_field('p_report_date', 'choose_date', sysdate, NULL, 'date'); htp.p('</td></tr>
			<tr>
				<td>Hour, min:</td>
				<td>
					<select name="p_hour">
					<option value="0">0</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
					<option value="13">13</option>
					<option value="14">14</option>
					<option value="15">15</option>
					</select>
					,
					<select name="p_min">
					<option value="0">0</option>
					<option value="30">30</option>
					</select>
				</td>
			</tr>
			<tr><td>Description:</td><td><input type="text" name="p_description" size="100" maxlength="250" value=""></td></tr>
			<tr><td colspan="2" align="right">'); html.submit_link( 'submit' ); htp.p('</td></tr>
		');
		html.e_table;
		html.e_box;
	-- ----------------------------------------------
	-- p_type = 'insert': legger inn ny forekomst
	-- ----------------------------------------------
	elsif (p_type = 'insert' and p_hour IS NOT NULL and p_min IS NOT NULL and p_description IS NOT NULL) then
                SELECT	project_report_seq.NEXTVAL
                INTO	v_project_report_pk
                FROM	dual;

		if (p_min = 30) then
			v_hour := p_hour + 0.5;
		else
			v_hour := p_hour;
		end if;

		INSERT INTO project_report
		(project_report_pk, project_fk, inserted_date, report_date, user_fk, description, hour)
		values(v_project_report_pk, p_project_pk, sysdate, to_date(p_report_date, get.txt('date_long')), v_user_pk, p_description, v_hour );
		commit;

		html.jump_to('time_report.report_work');
	-- -----------------------------------------------------
	-- p_type = 'update': oppdaterer verdier i databasen
	-- -----------------------------------------------------
	elsif (p_type = 'update') then

		if (p_hour IS NOT NULL and p_description IS NOT NULL) then
			if (p_min = 30) then
				v_hour := p_hour + 0.5;
			else
				v_hour := p_hour;
			end if;

			UPDATE	project_report
			SET	report_date = to_date(p_report_date, get.txt('date_long')),
				description = p_description,
				hour = v_hour,
				project_fk = p_project_pk
			WHERE	project_report_pk = p_project_report_pk
			AND	user_fk = v_user_pk;
			commit;

			html.jump_to('time_report.report_work');

		else

		        html.b_box( 'update project report');
		        html.b_form('time_report.report_work');
		        html.b_table;

		        SELECT  report_date, description, hour, project_fk
		        INTO	v_report_date, v_description, v_hour, v_project_pk
		        FROM    project_report
		        WHERE   project_report_pk = p_project_report_pk;

			htp.p('
				<input type="hidden" name="p_type" value="update">
				<input type="hidden" name="p_project_report_pk" value="'||p_project_report_pk||'">
				<tr><td>Project name:</td><td>'); time_report.select_project(v_project_pk); htp.p('</td></tr>
				<tr><td>Date:</td><td>'); html.date_field('p_report_date', 'choose_date', v_report_date, v_report_date, 'date'); htp.p('</td></tr>
				<tr>
					<td>Hour:</td>
					<td><input type="text" name="p_hour" size="5" maxlength="5" value="'||v_hour||'"></td>
				</tr>
				<tr><td>Description:</td><td><input type="text" name="p_description" size="100" maxlength="250" value="'||v_description||'"></td></tr>
				<tr><td colspan="2" align="right">'); html.submit_link( 'update' ); htp.p('</td></tr>
			');

			html.e_table;
			html.e_form;
			html.e_box;
		end if;
	-- ----------------------------------------------------------------
	-- p_type = 'delete': sletter verdier fra databasen
	-- ----------------------------------------------------------------
	elsif ( owa_pattern.match(p_type,'delete') ) then

		-- ---
		-- sletter i databasen
		-- ---
		if ( owa_pattern.match(p_type,'delete_action') ) then

			DELETE FROM project_report
			WHERE project_report_pk = p_project_report_pk
			AND user_fk = v_user_pk;
			commit;

			html.jump_to('time_report.report_work');
		-- ---
		-- skriver ut html form (confirm delete)
		-- ---
		else
		        SELECT  description
		        INTO	v_description
		        FROM    project_report
		        WHERE   project_report_pk = p_project_report_pk;

		        html.b_box( 'delete project report' );
		        html.b_form('time_report.report_work');
		        html.b_table;

		        htp.p('
		                <input type="hidden" name="p_type" value="delete_action">
		                <input type="hidden" name="p_project_report_pk" value="'|| p_project_report_pk ||'">
		                <tr><td> Are you sure you want to delete the project report '''|| v_description ||'''?</td></tr>
		                <tr><td>'); html.submit_link( 'delete project report' ); htp.p('</td></tr>
		                <tr><td>'); html.back( 'don''t delete project report' ); htp.p('</td></tr>
		        ');

			html.e_table;
			html.e_form;
			html.e_box;

		end if;
	else
		html.jump_to('time_report.report_work');
	end if;

	html.e_adm_page;

end if;
END;
END report_work;




---------------------------------------------------------
-- Name:        select_project
-- Type:        procedure
-- What:        writes out the html code for a select box
--              with all projects in the database.
-- Author:      Frode Klevstul
-- Start date:  20.02.2001
-- Desc:
---------------------------------------------------------
PROCEDURE select_project
        (
                p_project_pk            in project.project_pk%type              default NULL
        )
IS
BEGIN
DECLARE

        CURSOR  all_projects IS
        SELECT  project_pk, name
        FROM    project
        WHERE   activated = 1
	ORDER BY name;

	v_project_pk		project.project_pk%type		default NULL;
	v_name			project.name%type		default NULL;

BEGIN

        htp.p('<select name="p_project_pk">');

        open all_projects;
        loop
                fetch all_projects into v_project_pk, v_name;
                exit when all_projects%NOTFOUND;
                if (v_project_pk = p_project_pk) then
                        htp.p('<option value="'|| v_project_pk ||'" selected>'|| v_name ||'</option>');
                else
                        htp.p('<option value="'|| v_project_pk ||'">'|| v_name ||'</option>');
                end if;
        end loop;
        close all_projects;

        htp.p('</select>');

END;
END select_project;



---------------------------------------------------------
-- Name:        grand_total
-- Type:        procedure
-- What:        writes out "total" statistic
-- Author:      Frode Klevstul
-- Start date:  22.02.2001
-- Desc:
---------------------------------------------------------
PROCEDURE grand_total
IS
BEGIN
DECLARE

        CURSOR  all_values IS
        SELECT  sum(hour), user_fk
        FROM    project, project_report
        WHERE   project_pk = project_fk
        AND	in_statistic = 1
	GROUP BY user_fk
	ORDER BY sum(hour) DESC;

	v_hour			project_report.hour%type	default NULL;
	v_user_pk		usr.user_pk%type		default NULL;
	v_total_hours		number				default NULL;
	v_percent		number				default NULL;

BEGIN
if ( login.timeout('time_report.report_work')>0 AND login.right('time_report')>0 ) then

	SELECT	sum(hour)
	INTO	v_total_hours
	FROM	project, project_report
	WHERE	project_pk = project_fk
	AND	in_statistic = 1;


	html.b_adm_page('TimeReporting System - grand_total');
	html.b_box('Grand Total');
	html.b_table('50%', 1);

        open all_values;
        loop
                fetch all_values into v_hour, v_user_pk;
                exit when all_values%NOTFOUND;

                v_percent := ( (v_hour/v_total_hours)*100 );

		htp.p('<tr><td>'||get.uname(v_user_pk)||' ('||get.login_name(v_user_pk)||')</td><td align="right">'|| ROUND(v_percent,2) ||'%</td><td align="right">'||v_hour||' h</td></tr>');
        end loop;
        close all_values;

	htp.p('<tr><td align="right" colspan="3"><b>'||v_total_hours||' h</b></td></tr>');

	html.e_table;
	html.e_box;
	html.e_adm_page;

end if;
END;
END grand_total;




---------------------------------------------------------
-- Name:        project_overview
-- Type:        procedure
-- What:        lists out all projects
-- Author:      Frode Klevstul
-- Start date:  27.02.2001
-- Desc:
---------------------------------------------------------
PROCEDURE project_overview
	(
		p_project_pk	in project.project_pk%type	default NULL,
		p_user_pk	in usr.user_pk%type		default NULL
	)
IS
BEGIN
DECLARE

        CURSOR  all_projects IS
	SELECT	project_pk, name
	FROM	project
	WHERE	in_statistic = 1
	ORDER BY name ASC;

        CURSOR  project_details(p_project_pk in project.project_pk%type) IS
        SELECT  sum(hour), user_fk
        FROM    project, project_report
        WHERE   project_pk = project_fk
        AND	project_pk = p_project_pk
        AND	in_statistic = 1
	GROUP BY user_fk
	ORDER BY sum(hour) DESC;

        CURSOR  user_details(p_project_pk in project.project_pk%type, p_user_pk in usr.user_pk%type) IS
        SELECT  hour, description, report_date
        FROM    project_report
        WHERE   project_fk = p_project_pk
        AND	user_fk = p_user_pk
	ORDER BY report_date ASC;

	v_project_pk		project.project_pk%type			default NULL;
	v_name			project.name%type			default NULL;
	v_total_hours		number					default NULL;
	v_hour			project_report.hour%type		default NULL;
	v_percent		number					default NULL;
	v_report_date_first	project_report.report_date%type		default NULL;
	v_report_date_last	project_report.report_date%type		default NULL;
	v_user_pk		usr.user_pk%type			default NULL;
	v_description		project_report.description%type		default NULL;
	v_report_date		project_report.report_date%type		default NULL;


BEGIN
if ( login.timeout('time_report.project_overview')>0 AND login.right('time_report')>0 ) then

	html.b_adm_page('TimeReporting System - project_overview');

	if (p_project_pk IS NULL) then

		SELECT	sum(hour)
		INTO	v_total_hours
		FROM	project, project_report
		WHERE	project_pk = project_fk
		AND	in_statistic = 1;

		html.b_box('Project overview');
		html.b_table('100%', 1);
		htp.p('<tr><td><b>name:</b></td><td><b>start:</b></td><td><b>last:</b></td><td><b>% of tot:</b></td><td><b>total hours:</b></td></tr>');
	        open all_projects;
	        loop
	                fetch all_projects into v_project_pk, v_name;
	                exit when all_projects%NOTFOUND;

	                SELECT	sum(hour), min(report_date), max(report_date)
	                INTO	v_hour, v_report_date_first, v_report_date_last
	                FROM	project_report
	                WHERE	project_fk = v_project_pk;

	                v_percent := ( (v_hour/v_total_hours)*100 );
	                if(v_hour IS NOT NULL) then
				htp.p('<tr><td><a href="time_report.project_overview?p_project_pk='||v_project_pk||'">'|| v_name ||'</a></td><td>'|| v_report_date_first ||'</td><td>'|| v_report_date_last ||'</td><td align="right">'|| ROUND(v_percent,2) ||'%</td><td align="right">'||v_hour||' h</td></tr>');
			end if;
	        end loop;
	        close all_projects;

		htp.p('<tr><td align="right" colspan="5"><b>'||v_total_hours||' h</b></td></tr>');

		html.e_table;
		html.e_box;

	elsif (p_user_pk IS NULL) then

		SELECT	sum(hour)
		INTO	v_total_hours
		FROM	project_report
		WHERE	project_fk = p_project_pk;

		SELECT	name
		INTO	v_name
		FROM	project
		WHERE	project_pk = p_project_pk;

		html.b_box('Project details for '''|| v_name ||'''');
		html.b_table('100%', 1);
		htp.p('<tr><td><b>user:</b></td><td><b>%:</b></td><td><b>total hour:</b></td></tr>');

	        open project_details(p_project_pk);
	        loop
	                fetch project_details into v_hour, v_user_pk;
	                exit when project_details%NOTFOUND;

	                v_percent := ( (v_hour/v_total_hours)*100 );

			htp.p('<tr><td><a href="time_report.project_overview?p_project_pk='||p_project_pk||'&p_user_pk='||v_user_pk||'">'||get.uname(v_user_pk)||' ('||get.login_name(v_user_pk)||')</a></td><td align="right">'|| ROUND(v_percent,2) ||'%</td><td align="right">'||v_hour||' h</td></tr>');
	        end loop;
	        close project_details;

		htp.p('<tr><td align="right" colspan="3"><b>'||v_total_hours||' h</b></td></tr>');

		html.e_table;
		html.e_box;
	else
		SELECT	sum(hour)
		INTO	v_total_hours
		FROM	project_report
		WHERE	project_fk = p_project_pk
		AND	user_fk = p_user_pk;

		SELECT	name
		INTO	v_name
		FROM	project
		WHERE	project_pk = p_project_pk;

		html.b_box('Project details for '''||get.login_name(p_user_pk)||''' on '''|| v_name ||'''');
		html.b_table('100%', 1);
		htp.p('<tr><td><b>report date:</b></td><td><b>description:</b></td><td><b>hour:</b></td></tr>');

	        open user_details(p_project_pk, p_user_pk);
	        loop
	                fetch user_details into v_hour, v_description, v_report_date;
	                exit when user_details%NOTFOUND;

			htp.p('<tr><td>'|| to_char(v_report_date, get.txt('date_short')) ||'</td><td>'|| v_description ||'</td><td align="right">'||v_hour||' h</td></tr>');
	        end loop;
	        close user_details;

		htp.p('<tr><td align="right" colspan="3"><b>'||v_total_hours||' h</b></td></tr>');

		html.e_table;
		html.e_box;

	end if;

	html.e_adm_page;

end if;
END;
END project_overview;




---------------------------------------------------------
-- Name:        generate_report
-- Type:        procedure
-- What:        generates an report
-- Author:      Frode Klevstul
-- Start date:  27.02.2001
-- Desc:
---------------------------------------------------------
PROCEDURE generate_report
	(
		p_project_pk		in project.project_pk%type	default NULL,
		p_from_date		in varchar2			default NULL,
		p_from_date_show	in varchar2			default NULL,
		p_to_date		in varchar2			default NULL,
		p_to_date_show		in varchar2			default NULL,
		p_user_pk		in usr.user_pk%type		default NULL,
		p_from_check		in varchar2			default NULL,
		p_to_check		in varchar2			default NULL,
		p_project_check		in varchar2			default NULL,
		p_user_check		in varchar2			default NULL,
		p_all_check		in varchar2			default NULL
	)
IS
BEGIN
DECLARE

	-- NULL
	CURSOR	select_1 IS
	SELECT	report_date, name, user_fk, description, hour
	FROM	project, project_report
	WHERE	project_pk = project_fk
	ORDER BY report_date ASC;

	-- from_date
	CURSOR	select_2(p_from_date in project_report.report_date%type) IS
	SELECT	report_date, name, user_fk, description, hour
	FROM	project, project_report
	WHERE	project_pk = project_fk
	AND	report_date > p_from_date;

	-- to_date
	CURSOR	select_3(p_to_date in project_report.report_date%type) IS
	SELECT	report_date, name, user_fk, description, hour
	FROM	project, project_report
	WHERE	project_pk = project_fk
	AND	report_date < p_to_date;

	-- from_date, to_date
	CURSOR	select_4(
		p_from_date in project_report.report_date%type,
		p_to_date in project_report.report_date%type
	) IS
	SELECT	report_date, name, user_fk, description, hour
	FROM	project, project_report
	WHERE	project_pk = project_fk
	AND	report_date BETWEEN p_from_date AND p_to_date;

	-- from_date, to_date, project
	CURSOR	select_5(
		p_from_date in project_report.report_date%type,
		p_to_date in project_report.report_date%type,
		p_project_pk in project.project_pk%type
	) IS
	SELECT	report_date, name, user_fk, description, hour
	FROM	project, project_report
	WHERE	project_pk = project_fk
	AND	report_date BETWEEN p_from_date AND p_to_date
	AND	project_pk = p_project_pk;

	-- from_date, to_date, user
	CURSOR	select_6(
		p_from_date in project_report.report_date%type,
		p_to_date in project_report.report_date%type,
		p_user_pk in usr.user_pk%type
	) IS
	SELECT	report_date, name, user_fk, description, hour
	FROM	project, project_report
	WHERE	project_pk = project_fk
	AND	report_date BETWEEN p_from_date AND p_to_date
	AND	user_fk = p_user_pk;

	-- from_date, to_date, project, user
	CURSOR	select_7(
		p_from_date in project_report.report_date%type,
		p_to_date in project_report.report_date%type,
		p_project_pk in project.project_pk%type,
		p_user_pk in usr.user_pk%type
	) IS
	SELECT	report_date, name, user_fk, description, hour
	FROM	project, project_report
	WHERE	project_pk = project_fk
	AND	report_date BETWEEN p_from_date AND p_to_date
	AND	project_pk = p_project_pk
	AND	user_fk = p_user_pk;

	-- project
	CURSOR	select_8(
		p_project_pk in project.project_pk%type
	) IS
	SELECT	report_date, name, user_fk, description, hour
	FROM	project, project_report
	WHERE	project_pk = project_fk
	AND	project_pk = p_project_pk;

	-- project, user
	CURSOR	select_9(
		p_project_pk in project.project_pk%type,
		p_user_pk in usr.user_pk%type
	) IS
	SELECT	report_date, name, user_fk, description, hour
	FROM	project, project_report
	WHERE	project_pk = project_fk
	AND	project_pk = p_project_pk
	AND	user_fk = p_user_pk;

	-- user
	CURSOR	select_10(
		p_user_pk in usr.user_pk%type
	) IS
	SELECT	report_date, name, user_fk, description, hour
	FROM	project, project_report
	WHERE	project_pk = project_fk
	AND	user_fk = p_user_pk;

	v_from_date		date					default NULL;
	v_to_date		date					default NULL;
	v_total_hours		number					default NULL;

	v_report_date		project_report.report_date%type		default NULL;
	v_name			project.name%type			default NULL;
	v_user_pk		usr.user_pk%type			default NULL;
	v_description		project_report.description%type		default NULL;
	v_hour			project_report.hour%type		default NULL;

BEGIN
if ( login.timeout('time_report.generate_report')>0 AND login.right('time_report')>0 ) then

	if (p_from_date IS NULL) then
		v_from_date := sysdate-30;
	else
		v_from_date := to_date(p_from_date,get.txt('date_long'));
	end if;

	if (p_to_date IS NULL) then
		v_to_date := sysdate;
	else
		v_to_date := to_date(p_to_date,get.txt('date_long'));
	end if;

	html.b_adm_page('TimeReporting System - generate_report');
	html.b_box('Generate report');
	html.b_form('time_report.generate_report');
	html.b_table('100%', 1);

	htp.p('
		<tr><td>from:</td><td>'); html.date_field('p_from_date', 'choose_date', v_from_date, v_from_date, 'date'); htp.p('</td><td><input type="checkbox" '||p_from_check||' name="p_from_check" value="checked"></td></tr>
		<tr><td>to:</td><td>'); html.date_field('p_to_date', 'choose_date', v_to_date, v_to_date, 'date'); htp.p('</td><td><input type="checkbox" '||p_to_check||' name="p_to_check" value="checked"></td></tr>
		<tr><td>project:</td><td>'); time_report.select_project(p_project_pk); htp.p('</td><td><input type="checkbox" '||p_project_check||' name="p_project_check" value="checked"></td></tr>
		<tr><td>user:</td><td>'); html.select_user(p_user_pk); htp.p('</td><td><input type="checkbox" '||p_user_check||' name="p_user_check" value="checked"></td></tr>
		<tr><td colspan="2">select out all values:</td><td><input type="checkbox" '||p_all_check||' name="p_all_check" value="checked"></td></tr>
		<tr><td colspan="3" align="right">'); html.submit_link('generate'); htp.p('</td></tr>
	');

	html.e_table;
	html.e_form;

	html.b_table('100%', 1);
	htp.p('<tr><td><b>date:</b></td><td><b>project:</b></td><td><b>user:</b></td><td><b>description:</b></td><td><b>hour:</b></td></tr>');

	v_total_hours := 0;

	-- -------------------------------------------
	-- select all values
	-- -------------------------------------------
	if (p_from_check IS NULL and p_to_check IS NULL and p_project_check IS NULL and p_user_check IS NULL and p_all_check IS NOT NULL) then
htp.p('<h1>1</h1>');
	        open select_1;
	        loop
	                fetch select_1 into v_report_date, v_name, v_user_pk, v_description, v_hour;
	                exit when select_1%NOTFOUND;
	                v_total_hours := v_total_hours + v_hour;
			htp.p('<tr><td>'|| to_char(v_report_date, get.txt('date_short')) ||'</td><td>'|| v_name ||'</td><td>'|| get.uname(v_user_pk) ||'</td><td>'|| v_description ||'</td><td>'|| v_hour ||'</td></tr>');
	        end loop;
	        close select_1;
	-- -------------------------------------------
	-- from_date
	-- -------------------------------------------
	elsif (p_from_check IS NOT NULL and p_to_check IS NULL and p_project_check IS NULL and p_user_check IS NULL and p_all_check IS NULL) then
htp.p('<h1>2</h1>');
	        open select_2(v_from_date);
	        loop
	                fetch select_2 into v_report_date, v_name, v_user_pk, v_description, v_hour;
	                exit when select_2%NOTFOUND;
	                v_total_hours := v_total_hours + v_hour;
			htp.p('<tr><td>'|| to_char(v_report_date, get.txt('date_short')) ||'</td><td>'|| v_name ||'</td><td>'|| get.uname(v_user_pk) ||'</td><td>'|| v_description ||'</td><td>'|| v_hour ||'</td></tr>');
	        end loop;
	        close select_2;
	-- -------------------------------------------
	-- to_date
	-- -------------------------------------------
	elsif (p_from_check IS NULL and p_to_check IS NOT NULL and p_project_check IS NULL and p_user_check IS NULL and p_all_check IS NULL) then
htp.p('<h1>3</h1>');
	        open select_3(v_to_date);
	        loop
	                fetch select_3 into v_report_date, v_name, v_user_pk, v_description, v_hour;
	                exit when select_3%NOTFOUND;
	                v_total_hours := v_total_hours + v_hour;
			htp.p('<tr><td>'|| to_char(v_report_date, get.txt('date_short')) ||'</td><td>'|| v_name ||'</td><td>'|| get.uname(v_user_pk) ||'</td><td>'|| v_description ||'</td><td>'|| v_hour ||'</td></tr>');
	        end loop;
	        close select_3;
	-- -------------------------------------------
	-- from_date, to_date
	-- -------------------------------------------
	elsif (p_from_check IS NOT NULL and p_to_check IS NOT NULL and p_project_check IS NULL and p_user_check IS NULL and p_all_check IS NULL) then
htp.p('<h1>4</h1>');
	        open select_4(v_from_date, v_to_date);
	        loop
	                fetch select_4 into v_report_date, v_name, v_user_pk, v_description, v_hour;
	                exit when select_4%NOTFOUND;
	                v_total_hours := v_total_hours + v_hour;
			htp.p('<tr><td>'|| to_char(v_report_date, get.txt('date_short')) ||'</td><td>'|| v_name ||'</td><td>'|| get.uname(v_user_pk) ||'</td><td>'|| v_description ||'</td><td>'|| v_hour ||'</td></tr>');
	        end loop;
	        close select_4;
	-- -------------------------------------------
	-- from_date, to_date, project
	-- -------------------------------------------
	elsif (p_from_check IS NOT NULL and p_to_check IS NOT NULL and p_project_check IS NOT NULL and p_user_check IS NULL and p_all_check IS NULL) then
htp.p('<h1>5</h1>');
	        open select_5(v_from_date, v_to_date, p_project_pk);
	        loop
	                fetch select_5 into v_report_date, v_name, v_user_pk, v_description, v_hour;
	                exit when select_5%NOTFOUND;
	                v_total_hours := v_total_hours + v_hour;
			htp.p('<tr><td>'|| to_char(v_report_date, get.txt('date_short')) ||'</td><td>'|| v_name ||'</td><td>'|| get.uname(v_user_pk) ||'</td><td>'|| v_description ||'</td><td>'|| v_hour ||'</td></tr>');
	        end loop;
	        close select_5;
	-- -------------------------------------------
	-- from_date, to_date, user
	-- -------------------------------------------
	elsif (p_from_check IS NOT NULL and p_to_check IS NOT NULL and p_project_check IS NULL and p_user_check IS NOT NULL and p_all_check IS NULL) then
htp.p('<h1>6</h1>');
	        open select_6(v_from_date, v_to_date, p_user_pk);
	        loop
	                fetch select_6 into v_report_date, v_name, v_user_pk, v_description, v_hour;
	                exit when select_6%NOTFOUND;
	                v_total_hours := v_total_hours + v_hour;
			htp.p('<tr><td>'|| to_char(v_report_date, get.txt('date_short')) ||'</td><td>'|| v_name ||'</td><td>'|| get.uname(v_user_pk) ||'</td><td>'|| v_description ||'</td><td>'|| v_hour ||'</td></tr>');
	        end loop;
	        close select_6;
	-- -------------------------------------------
	-- from_date, to_date, project, user
	-- -------------------------------------------
	elsif (p_from_check IS NOT NULL and p_to_check IS NOT NULL and p_project_check IS NOT NULL and p_user_check IS NOT NULL and p_all_check IS NULL) then
htp.p('<h1>7</h1>');
	        open select_7(v_from_date, v_to_date, p_project_pk, p_user_pk);
	        loop
	                fetch select_7 into v_report_date, v_name, v_user_pk, v_description, v_hour;
	                exit when select_7%NOTFOUND;
	                v_total_hours := v_total_hours + v_hour;
			htp.p('<tr><td>'|| to_char(v_report_date, get.txt('date_short')) ||'</td><td>'|| v_name ||'</td><td>'|| get.uname(v_user_pk) ||'</td><td>'|| v_description ||'</td><td>'|| v_hour ||'</td></tr>');
	        end loop;
	        close select_7;
	-- -------------------------------------------
	-- project
	-- -------------------------------------------
	elsif (p_from_check IS NULL and p_to_check IS NULL and p_project_check IS NOT NULL and p_user_check IS NULL and p_all_check IS NULL) then
htp.p('<h1>8</h1>');
	        open select_8(p_project_pk);
	        loop
	                fetch select_8 into v_report_date, v_name, v_user_pk, v_description, v_hour;
	                exit when select_8%NOTFOUND;
	                v_total_hours := v_total_hours + v_hour;
			htp.p('<tr><td>'|| to_char(v_report_date, get.txt('date_short')) ||'</td><td>'|| v_name ||'</td><td>'|| get.uname(v_user_pk) ||'</td><td>'|| v_description ||'</td><td>'|| v_hour ||'</td></tr>');
	        end loop;
	        close select_8;
	-- -------------------------------------------
	-- project, user
	-- -------------------------------------------
	elsif (p_from_check IS NULL and p_to_check IS NULL and p_project_check IS NOT NULL and p_user_check IS NOT NULL and p_all_check IS NULL) then
htp.p('<h1>9</h1>');
	        open select_9(p_project_pk, p_user_pk);
	        loop
	                fetch select_9 into v_report_date, v_name, v_user_pk, v_description, v_hour;
	                exit when select_9%NOTFOUND;
	                v_total_hours := v_total_hours + v_hour;
			htp.p('<tr><td>'|| to_char(v_report_date, get.txt('date_short')) ||'</td><td>'|| v_name ||'</td><td>'|| get.uname(v_user_pk) ||'</td><td>'|| v_description ||'</td><td>'|| v_hour ||'</td></tr>');
	        end loop;
	        close select_9;
	-- -------------------------------------------
	-- user
	-- -------------------------------------------
	elsif (p_from_check IS NULL and p_to_check IS NULL and p_project_check IS NULL and p_user_check IS NOT NULL and p_all_check IS NULL) then
htp.p('<h1>10</h1>');
	        open select_10(p_user_pk);
	        loop
	                fetch select_10 into v_report_date, v_name, v_user_pk, v_description, v_hour;
	                exit when select_10%NOTFOUND;
	                v_total_hours := v_total_hours + v_hour;
			htp.p('<tr><td>'|| to_char(v_report_date, get.txt('date_short')) ||'</td><td>'|| v_name ||'</td><td>'|| get.uname(v_user_pk) ||'</td><td>'|| v_description ||'</td><td>'|| v_hour ||'</td></tr>');
	        end loop;
	        close select_10;
	else
		htp.p('<tr><td colspan="5" align="center" height="50"><b>Search method is not implemented</b></td></tr>');
	end if;


	htp.p('<tr><td colspan="5" align="right"><b>'||v_total_hours||'</b></td></tr>');
	html.e_table;

	html.e_box;
	html.e_adm_page;

end if;
END;
END generate_report;


-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/
show errors;
