set define off
PROMPT *** package: MENU_ORG ***

CREATE OR REPLACE PACKAGE menu_org IS

	empty_array owa_util.ident_arr;

    PROCEDURE startup
		(
			p_unique		in varchar2					default NULL
		);
	PROCEDURE menu_type_adm
		(
			p_menu_pk		in menu.menu_pk%type		default NULL,
			p_activated		in menu.activated%type		default NULL,
			p_unique		in varchar2					default NULL,
			p_task			in varchar2					default NULL
		);
	PROCEDURE compare_adm
		(
			p_compare_list_pk	in compare_list.compare_list_pk%type	default NULL,
			p_price				in varchar2								default NULL,
			p_amount			in varchar2								default NULL,
			p_task				in varchar2								default NULL
		);
	PROCEDURE serving_period_adm
		(
			p_serving_period_pk		in serving_period.serving_period_pk%type		default NULL,
			p_hour_start			in varchar2										default NULL,
			p_hour_end				in varchar2										default NULL,
			p_min_start				in varchar2										default NULL,
			p_min_end				in varchar2										default NULL,
			p_unique				in varchar2										default NULL,
			p_task					in varchar2										default NULL
		);
	PROCEDURE menu_object_adm
		(
			p_menu_object_pk		in menu_object.menu_object_pk%type				default NULL,
			p_menu_pk				in menu.menu_pk%type							default NULL,
			p_menu_object_type_pk	in menu_object_type.menu_object_type_pk%type	default NULL,
			p_menu_serving_type_pk	in menu_serving_type.menu_serving_type_pk%type	default NULL,
			p_serving_period_pk		in owa_util.ident_arr							default empty_array,
			p_order_number			in varchar2										default NULL,
			p_title					in menu_object_text.title%type					default NULL,
			p_description			in menu_object_text.description%type			default NULL,
			p_size_price_pk			in size_price.size_price_pk%type				default NULL,
			p_quantity				in size_price.quantity%type						default NULL,
			p_price					in varchar2										default NULL,
			p_activated				in menu_object.activated%type					default NULL,
			p_unique				in varchar2										default NULL,
			p_task					in varchar2										default NULL
		);

END;
/
show errors;


CREATE OR REPLACE PACKAGE BODY menu_org IS

---------------------------------------------------------
-- Name:        startup
-- Type:        procedure
-- What:        start procedure
-- Author:      Frode Klevstul
-- Start date:  23.08.2001
-- Desc:
---------------------------------------------------------
PROCEDURE startup
	(
		p_unique		in varchar2					default NULL
	)
IS
BEGIN
if ( login.timeout('menu_org.menu_type_adm')>0 ) then

	html.b_page;

	htp.p('<br>');
	menu_org.serving_period_adm;
	htp.p('<br>');
	menu_org.menu_type_adm;
	htp.p('<br>');
	menu_org.compare_adm;

	html.e_page;

end if;
END startup;





---------------------------------------------------------
-- Name:        menu_type_adm
-- Type:        procedure
-- What:        org administration of menu
-- Author:      Frode Klevstul
-- Start date:  25.09.2001
-- Desc:
---------------------------------------------------------
PROCEDURE menu_type_adm
	(
		p_menu_pk		in menu.menu_pk%type		default NULL,
		p_activated		in menu.activated%type		default NULL,
		p_unique		in varchar2					default NULL,
		p_task			in varchar2					default NULL
	)
IS
BEGIN
DECLARE


	CURSOR	c_select_all(p_organization_pk organization.organization_pk%type) IS
	SELECT	m.menu_pk, mt.name_sg_fk, m.activated, mt.menu_type_pk
	FROM	menu m, menu_type mt
	WHERE	m.menu_type_fk = mt.menu_type_pk
	AND		m.organization_fk = p_organization_pk
	AND		mt.activated IS NOT NULL
	ORDER BY mt.menu_type_pk;

	CURSOR	c_select_menu_types IS
	SELECT	menu_type_pk
	FROM	menu_type;

	v_organization_pk	organization.organization_pk%type	default NULL;
	v_name_sg_fk		menu_type.name_sg_fk%type			default NULL;
	v_activated			menu.activated%type					default NULL;

	v_check				number								default NULL;
	v_check2			number								default NULL;
	
	v_menu_pk			menu.menu_pk%type					default NULL;
	v_menu_type_pk		menu_type.menu_type_pk%type			default NULL;

BEGIN
if ( login.timeout('menu_org.menu_type_adm')>0 ) then

	-- henter organisasjons id
	v_organization_pk := get.oid;

	-- ----------------------------------------------------
	-- aktivering / deaktivering / oppdatering av meny
	-- ----------------------------------------------------
	if (p_menu_pk is not null and p_activated is not null) then
		
		-- sjekker at org. "eier" oppgitt meny
		SELECT 	count(*)
		INTO	v_check
		FROM	menu
		WHERE	organization_fk = v_organization_pk
		AND		menu_pk = p_menu_pk;
		
		if (v_check = 0) then
			get.error_page(3, get.txt('illegal_pk'), 'main_page', 'menu_org.menu_type_adm');
			return;
		end if;

		-- sletter alle retter/drinker til en meny
		if (owa_pattern.match(p_task,'reset_action')) then

			DELETE FROM menu_object
			WHERE	menu_fk = p_menu_pk;
			commit;

			html.jump_to('menu_org.startup?p_unique='||to_char(sysdate, get.txt('date_full')));
			return;

		-- spr�r om brukeren vil slette en meny
		elsif (owa_pattern.match(p_task,'reset_')) then

			SELECT	name_sg_fk
			INTO	v_name_sg_fk
			FROM	menu, menu_type
			WHERE	menu_type_fk = menu_type_pk
			AND		menu_pk = p_menu_pk;

			html.b_page;
			html.b_box( get.txt('confirm_reset_menu') );
			html.b_form('menu_org.menu_type_adm');
			html.b_table;
		
	        htp.p('
	                <input type="hidden" name="p_task" value="reset_action">
	                <input type="hidden" name="p_menu_pk" value="'|| p_menu_pk ||'">
	                <input type="hidden" name="p_activated" value="0">
	                <tr><td> '|| get.txt('sure_to_reset') ||' '''||get.text(v_name_sg_fk)||'''?</td></tr>
	                <tr><td>'); html.submit_link( get.txt('reset_menu') ); htp.p('</td></tr>
	                <tr><td>'); html.back( get.txt('dont_reset_menu') ); htp.p('</td></tr>
	        ');
	
			html.e_table;
			html.e_form;
			html.e_box;
			html.e_page;

			return;

		-- oppdaterer en meny
		else
	        if (p_activated = 0) then
	                v_activated := NULL;
	        else
	                v_activated := p_activated;
	        end if;
			
			UPDATE	menu
			SET		activated = v_activated
			WHERE	menu_pk = p_menu_pk
			AND		organization_fk = v_organization_pk;
			commit;

			html.jump_to('menu_org.startup?p_unique='||to_char(sysdate, get.txt('date_full')));
			return;

		end if;
		
	end if;


	-- -----------------------------------------------------------------------------------
	-- sjekker for � finne ut om org. har tilknyttet alle menyene i "menu" tabellen
	-- -----------------------------------------------------------------------------------
	SELECT	count(*) INTO v_check
	FROM	menu
	WHERE	organization_fk = v_organization_pk;

	SELECT	count(*) INTO v_check2
	FROM	menu_type;

	-- dersom org. ikke har tilknyttet alle forskjellige typer menyer som finnes ...
	if (v_check < v_check2) then
	
		-- g�r igjennom alle meny typene som finnes
		open c_select_menu_types;
		loop
			fetch c_select_menu_types into v_menu_type_pk;
			exit when c_select_menu_types%NOTFOUND;

			-- sjekker om org. har den spesifikke menyen
			SELECT	count(*) INTO v_check
			FROM	menu
			WHERE	organization_fk = v_organization_pk
			AND		menu_type_fk = v_menu_type_pk;

			-- insert dersom org. ikke har menyen
			if (v_check = 0) then
				SELECT	menu_seq.NEXTVAL
				INTO	v_menu_pk
				FROM	dual;
		
				INSERT INTO menu
				(menu_pk, organization_fk, inserted_date, modified_date, menu_type_fk)
				VALUES (v_menu_pk, v_organization_pk, sysdate, sysdate, v_menu_type_pk);
				commit;
			end if;
		
		end loop;
		close c_select_menu_types;
	
	end if;
	
	
	-- ---------------------------------
	-- lister ut tilgjengelige menyer
	-- ---------------------------------
	html.b_box( get.txt('available_menues'), '100%', 'menu_org.menu_type_adm' );
	html.b_table;
	htp.p('
		<tr>
			<td colspan="5">
			'|| get.txt('menu_type_adm_desc') ||'
			</td>
		</tr>
		<tr><td colspan="5">&nbsp;</td></tr>
		<tr>
			<td><b>'||get.txt('menu_type')||':</b></td>
			<td><b>'||get.txt('status')||':</b></td>
			<td><b>'||get.txt('edit_menu')||':</b></td>
			<td><b>'||get.txt('reset')||':</b></td>
			<td><b>'||get.txt('no_menu_objects')||':</b></td>
		</tr>
	');
	
	open c_select_all(v_organization_pk);
	loop
		fetch c_select_all into v_menu_pk, v_name_sg_fk, v_activated, v_menu_type_pk;
		exit when c_select_all%NOTFOUND;
		
		SELECT	count(*) 
		INTO 	v_check
		FROM	menu_object
		WHERE	menu_fk = v_menu_pk;
		
		htp.p('
			<tr>
				<td>'|| get.text(v_name_sg_fk) ||'</td>
				<td>');
                if (v_activated IS NULL) then
					html.button_link( get.txt('deactivated') ,'menu_org.menu_type_adm?p_menu_pk='||v_menu_pk||'&p_activated='||1||'&p_unique='||to_char(sysdate, get.txt('date_full')));
                else
					html.button_link( get.txt('activated') ,'menu_org.menu_type_adm?p_menu_pk='||v_menu_pk||'&p_activated='||0||'&p_unique='||to_char(sysdate, get.txt('date_full')));
                end if;
				htp.p('
					</td>
					<td>
					');
					html.button_link( get.txt('edit_add_menu_object') ,'menu_org.menu_object_adm?p_menu_pk='||v_menu_pk||'&p_task=list&p_unique='||to_char(sysdate, get.txt('date_full')));
				htp.p('
					</td>
					<td>
				');
					html.button_link( get.txt('reset') ,'menu_org.menu_type_adm?p_menu_pk='||v_menu_pk||'&p_activated='||0||'&p_unique='||to_char(sysdate, get.txt('date_full'))||'&p_task=reset_');
				htp.p('</td>
				<td>'|| v_check ||'</td>
			</tr>
		');
	end loop;
	close c_select_all;

	html.e_table;
	html.e_box;

end if;
END;
END menu_type_adm;





---------------------------------------------------------
-- Name:        compare_adm
-- Type:        procedure
-- What:        org administration of compare entries
-- Author:      Frode Klevstul
-- Start date:  04.10.2001
-- Desc:
---------------------------------------------------------
PROCEDURE compare_adm
	(
		p_compare_list_pk	in compare_list.compare_list_pk%type	default NULL,
		p_price				in varchar2								default NULL,
		p_amount			in varchar2								default NULL,
		p_task				in varchar2								default NULL
	)
IS
BEGIN
DECLARE

	CURSOR	c_select_all(p_organization_pk organization.organization_pk%type) IS
	SELECT	compare_list_fk, name_sg_fk
	FROM	organization_compare, compare_list
	WHERE	compare_list_pk = compare_list_fk
	AND		organization_fk = p_organization_pk;

	CURSOR	c_select_compare(p_organization_pk organization.organization_pk%type) IS
	SELECT	compare_list_pk, name_sg_fk
	FROM	compare_list
	WHERE	compare_list_pk NOT IN
		(	SELECT	compare_list_fk
			FROM	organization_compare
			WHERE	organization_fk = p_organization_pk
		);

	v_organization_pk	organization.organization_pk%type	default NULL;
	v_name_sg_fk		menu_type.name_sg_fk%type			default NULL;

	v_check				number								default NULL;
	v_check2			number								default NULL;
	
	v_compare_list_pk	compare_list.compare_list_pk%type	default NULL;
	v_price_txt			varchar2(20)						default NULL;
	v_price				organization_compare.price%type		default NULL;

BEGIN
if ( login.timeout('menu_org.compare_adm')>0 ) then

	-- henter org. id
	v_organization_pk := get.oid;

	-- ----------------------------------------------------
	-- p_task = 'activate'	:	aktiver en "compare_list"
	-- ----------------------------------------------------
	if (p_compare_list_pk is not null and owa_pattern.match(p_task,'^activate')) then

		SELECT 	count(*)
		INTO	v_check
		FROM	organization_compare
		WHERE	organization_fk = v_organization_pk
		AND		compare_list_fk = p_compare_list_pk;

		if (v_check = 0) then
			INSERT INTO organization_compare
			(compare_list_fk, organization_fk, price)
			VALUES(p_compare_list_pk, v_organization_pk, NULL);
			commit;
		end if;

		html.jump_to('menu_org.startup?p_unique='||to_char(sysdate, get.txt('date_full')));
		return;

	-- ----------------------------------------------------
	-- p_task = 'deactivate'	:	deaktiver en "compare_list"
	-- ----------------------------------------------------
	elsif (p_compare_list_pk is not null and owa_pattern.match(p_task,'^deactivate')) then

		if (owa_pattern.match(p_task,'deactivate_action')) then

			DELETE FROM organization_compare
			WHERE	organization_fk = v_organization_pk
			AND compare_list_fk = p_compare_list_pk;
			commit;

			html.jump_to('menu_org.startup?p_unique='||to_char(sysdate, get.txt('date_full')));
			return;

		else

			SELECT	name_sg_fk
			INTO	v_name_sg_fk
			FROM	compare_list
			WHERE	compare_list_pk = p_compare_list_pk;

			html.b_page;
			html.b_box( get.txt('remove_compare_list'));
			html.b_form('menu_org.compare_adm');
			html.b_table;
		
	        htp.p('
	                <input type="hidden" name="p_task" value="deactivate_action">
	                <input type="hidden" name="p_compare_list_pk" value="'|| p_compare_list_pk ||'">
	                <tr><td> '|| get.txt('sure_to_remove') ||' '''||get.text(v_name_sg_fk)||'''?</td></tr>
	                <tr><td>'); html.submit_link( get.txt('deactivate') ); htp.p('</td></tr>
	                <tr><td>'); html.back( get.txt('dont_deactivate') ); htp.p('</td></tr>
	        ');
	
			html.e_table;
			html.e_form;
			html.e_box;
			html.e_page;

			return;

		end if;

	-- ----------------------------------------------------
	-- p_task = 'update'	:	oppdater i databasen
	-- ----------------------------------------------------
	elsif (p_compare_list_pk is not null and owa_pattern.match(p_task,'^update')) then

		-- behandler input data
		v_price_txt 	:= p_price;
		owa_pattern.change(v_price_txt, '\.', ',');

		-- sjekker om prisen er p� riktig format
		if ( 
			owa_pattern.match(v_price_txt, '^\d{1,}$') or
			owa_pattern.match(v_price_txt, '^,\d{1,}$') or
			owa_pattern.match(v_price_txt, '^\d{1,},\d{1,}$')
		) then
			v_price := to_number(v_price_txt);

			-- -------------------------------------------
			-- regner om til riktig m�leenhet
			-- -------------------------------------------
			SELECT	name_sg_fk
			INTO	v_name_sg_fk
			FROM	compare_list
			WHERE	compare_list_pk = p_compare_list_pk;
	
			-- regner om til riktig verdi dersom dersom m�leenhet er oppgitt
			if (p_amount IS NOT NULL) then
	
				-- dersom vi har 'beer' ...
				if ( owa_pattern.match( get.sg_name(v_name_sg_fk), 'beer') ) then

					-- s� regner vi om til 0,5 l ...
					v_price := (v_price/p_amount) * 0.5;

				end if;
	
			end if;

			-- --------------------------------
			-- oppdaterer databasen
			-- --------------------------------
			UPDATE	organization_compare
			SET		price = v_price
			WHERE	compare_list_fk = p_compare_list_pk
			AND		organization_fk = v_organization_pk;
			commit;

		end if;

		html.jump_to('menu_org.startup?p_unique='||to_char(sysdate, get.txt('date_full')));
		return;

	end if;



	-- --------------------------------------------------------------------------------
	--
	-- Her starter utlisting av aktiverte og ikke aktiverte "compares"
	--
	-- --------------------------------------------------------------------------------

	html.b_box( get.txt('administrate_compare_lists'), '100%', 'menu_org.compare_adm');
	html.b_table;
	htp.p('
		<tr>
			<td colspan="4">
			'|| get.txt('compare_adm_desc') ||'
			</td>
		</tr>
		<tr><td colspan="4">&nbsp;</td></tr>	
	');

	-- --------------------------------------------------------------------------------
	-- sjekk om org. har tilknyttet alle tilgjengelige compare lists som finnes
	-- --------------------------------------------------------------------------------
	-- antall "compares" org har 
	SELECT	count(*) INTO v_check
	FROM	organization_compare
	WHERE	organization_fk = v_organization_pk;

	-- antall "compares" som finnes totalt
	SELECT	count(*) INTO v_check2
	FROM	compare_list;

	-- dersom org. ikke har tilknyttet alle "compare lists" s� listes disse ut
	if (v_check < v_check2) then	
		open c_select_compare(v_organization_pk);
		loop
			fetch c_select_compare into v_compare_list_pk, v_name_sg_fk;
			exit when c_select_compare%NOTFOUND;				
			htp.p('<tr><td width="30%">'|| get.text(v_name_sg_fk) ||'</td><td colspan="2">&nbsp;</td><td align="center" valign="center">'); html.button_link( get.txt('deactivated') ,'menu_org.compare_adm?p_compare_list_pk='||v_compare_list_pk||'&p_task=activate'); htp.p('</td></tr>');
		end loop;
		close c_select_compare;	
	end if;


	htp.p('<tr><td colspan="4">&nbsp;</td></tr>');

	-- ------------------------------------------------------------------------
	-- skriv ut liste med "sammenligninger" som organisasjonen har aktivert
	-- ------------------------------------------------------------------------
	if (v_check > 0) then


		open c_select_all(v_organization_pk);
		loop
			fetch c_select_all into v_compare_list_pk, v_name_sg_fk;
			exit when c_select_all%NOTFOUND;
			
			SELECT	price
			INTO	v_price
			FROM	organization_compare
			WHERE	organization_fk = v_organization_pk
			AND		compare_list_fk = v_compare_list_pk;
			
			html.b_form('menu_org.compare_adm');
			htp.p('
				<input type="hidden" name="p_task" value="update">
				<input type="hidden" name="p_compare_list_pk" value="'||v_compare_list_pk||'">				
			');
			htp.p('
				<tr>
					<td width="30%" align="center">'||get.txt('price_for'));
			
			if ( owa_pattern.match( get.sg_name(v_name_sg_fk), 'beer') ) then
				htp.p('
					<select name="p_amount">
						<option value="0,2">0,2 l</option>
						<option value="0,25">0,25 l</option>
						<option value="0,3">0,3 l</option>
						<option value="0,33">0,33 l</option>
						<option value="0,4">0,4 l</option>
						<option value="0,473">'||get.txt('american_pint')||'</option>
						<option value="0,5" selected>0,5 l</option>
						<option value="0,568">'||get.txt('english_pint')||'</option>
					</select>
				');
			end if;
					
			htp.p(	get.text(v_name_sg_fk) ||':</td>
					<td> '|| html.text('p_price', 4, 8, v_price) ||' '|| get.curr ||'</td><td align="right">');
					html.submit_link( get.txt('update') );
					htp.p('</td><td align="center"><br>');
					html.button_link( get.txt('activated') ,'menu_org.compare_adm?p_compare_list_pk='||v_compare_list_pk||'&p_task=deactivate');
				htp.p('</td></tr>
			');
			html.e_form;

		end loop;
		close c_select_all;

	end if;

	html.e_table;
	html.e_box;


end if;
END;
END compare_adm;






---------------------------------------------------------
-- Name:        serving_period_adm
-- Type:        procedure
-- What:        org administration of serving periods
-- Author:      Frode Klevstul
-- Start date:  10.10.2001
-- Desc:
---------------------------------------------------------
PROCEDURE serving_period_adm
	(
		p_serving_period_pk		in serving_period.serving_period_pk%type		default NULL,
		p_hour_start			in varchar2										default NULL,
		p_hour_end				in varchar2										default NULL,
		p_min_start				in varchar2										default NULL,
		p_min_end				in varchar2										default NULL,
		p_unique				in varchar2										default NULL,
		p_task					in varchar2										default NULL
	)
IS
BEGIN
DECLARE


	CURSOR	c_select_org(p_organization_pk organization.organization_pk%type) IS
	SELECT	sp.serving_period_pk, sp.name_sg_fk, os.start_time, os.end_time
	FROM	serving_period sp, organization_serving os
	WHERE	sp.serving_period_pk = os.serving_period_fk
	AND		os.organization_fk = p_organization_pk
	ORDER BY os.start_time asc;

	CURSOR	c_select_all IS
	SELECT	serving_period_pk, name_sg_fk
	FROM	serving_period;


	v_organization_pk	organization.organization_pk%type		default NULL;
	v_name_sg_fk		serving_period.name_sg_fk%type			default NULL;
	v_serving_period_pk	serving_period.serving_period_pk%type	default NULL;
	v_start_time		organization_serving.start_time%type	default NULL;
	v_end_time			organization_serving.end_time%type		default NULL;

	v_check				number								default NULL;
	

BEGIN
if ( login.timeout('menu_org.serving_period_adm')>0 ) then


	-- henter organisasjons id
	v_organization_pk := get.oid;


	-- ----------------------------------
	--
	-- p_task is not null ...
	--
	-- ----------------------------------
	
	if (owa_pattern.match(p_task,'^delete$')) then

		SELECT	name_sg_fk
		INTO	v_name_sg_fk
		FROM	serving_period sp, organization_serving os
		WHERE	sp.serving_period_pk = os.serving_period_fk
		AND		os.organization_fk = v_organization_pk
		AND		sp.serving_period_pk = p_serving_period_pk;

		html.b_page;
		html.b_box( get.txt('confirm_delete_serving_period'));
		html.b_form('menu_org.serving_period_adm');
		html.b_table;

        htp.p('
                <input type="hidden" name="p_task" value="delete_action">
                <input type="hidden" name="p_serving_period_pk" value="'|| p_serving_period_pk ||'">
                <tr><td> '|| get.txt('sure_to_delete') ||' '''||get.text(v_name_sg_fk)||'''?</td></tr>
                <tr><td>'); html.submit_link( get.txt('delete_serving_period') ); htp.p('</td></tr>
                <tr><td>'); html.back( get.txt('dont_delete_serving_period') ); htp.p('</td></tr>
        ');

		html.e_table;
		html.e_form;
		html.e_box;
		html.e_page;

		return;

	-- --------------------
	-- delete action
	-- --------------------
	elsif (owa_pattern.match(p_task,'^delete_action$')) then

		DELETE FROM organization_serving
		WHERE organization_fk = v_organization_pk
		AND serving_period_fk = p_serving_period_pk;
		commit;

		html.jump_to('menu_org.startup?p_unique='||to_char(sysdate, get.txt('date_full')));
		return;

	elsif (owa_pattern.match(p_task,'^insert$')) then
	
		SELECT	count(*)
		INTO	v_check
		FROM	organization_serving
		WHERE	organization_fk = v_organization_pk
		AND		serving_period_fk = p_serving_period_pk;

		-- dersom "serving_period" finnes fra f�r s� sletter vi den gamle f�r den ny legges inn (vi bytter ut)
		if (v_check > 0) then
			UPDATE	organization_serving
			SET		start_time = to_date(p_hour_start||':'||p_min_start,'HH24:MI'),
					end_time = to_date(p_hour_end||':'||p_min_end,'HH24:MI')
			WHERE	organization_fk = v_organization_pk
			AND		serving_period_fk = p_serving_period_pk;
			commit;
		else
			INSERT INTO organization_serving
			(organization_fk, serving_period_fk, start_time, end_time)
			VALUES(v_organization_pk, p_serving_period_pk, to_date(p_hour_start||':'||p_min_start,'HH24:MI'), to_date(p_hour_end||':'||p_min_end,'HH24:MI'));
			commit;
		end if;

		html.jump_to('menu_org.startup?p_unique='||to_char(sysdate, get.txt('date_full')));
		return;

	end if;

	-- ----------------------------------
	--
	-- utlisting ...
	--
	-- ----------------------------------
	html.b_box(get.txt('serving_period_for_org'), '100%', 'menu_org.serving_period_adm');
	html.b_table;
	htp.p('
		<tr>
			<td colspan="4">
			'|| get.txt('serving_period_for_org_desc') ||'
			</td>
		</tr>
		<tr><td colspan="4">&nbsp;</td></tr>
		<tr>
			<td><b>'||get.txt('serving_type')||':</b></td>
			<td><b>'||get.txt('from')||':</b></td>
			<td><b>'||get.txt('to_time')||':</b></td>
			<td align="right"><b>'||get.txt('delete')||':</b></td>
		</tr>
	');
	
	open c_select_org(v_organization_pk);
	loop
		fetch c_select_org into v_serving_period_pk, v_name_sg_fk, v_start_time, v_end_time;
		exit when c_select_org%NOTFOUND;
				
		htp.p('
			<tr>
				<td>'|| get.text(v_name_sg_fk) ||'</td>
				<td>'|| to_char(v_start_time,get.txt('date_hour')) ||'</td>
				<td>'|| to_char(v_end_time,get.txt('date_hour')) ||'</td>
				<td align="right">');
					html.button_link( get.txt('delete') ,'menu_org.serving_period_adm?p_serving_period_pk='||v_serving_period_pk||'&p_unique='||to_char(sysdate, get.txt('date_full'))||'&p_task=delete');
				htp.p('</td>
			</tr>
		');
	end loop;
	close c_select_org;

	htp.p('
			<tr><td colspan="4">&nbsp;</td></tr>
			<tr>
				<td><b>'||get.txt('serving_type')||':</b></td>
				<td><b>'||get.txt('from')||':</b></td>
				<td><b>'||get.txt('to_time')||':</b></td>
				<td>&nbsp;</td>
			</tr>
		');

	html.b_form('menu_org.serving_period_adm');

	htp.p('
		<input type="hidden" name="p_task" value="insert">
		<tr>
			<td>'); html.select_serving_period; htp.p('</td>
			<td>
				'); html.select_time('start'); htp.p('
			</td>
			<td>
				'); html.select_time('end'); htp.p('
			</td>
			<td align="right">
				'); html.submit_link( get.txt('insert_or_update') ); htp.p('
			</td>
		</tr>
	');

	html.e_table;
	html.e_form;
	html.e_box;


end if;
END;
END serving_period_adm;








---------------------------------------------------------
-- Name:        menu_object_adm
-- Type:        procedure
-- What:        org administration of menu objects
-- Author:      Frode Klevstul
-- Start date:  12.10.2001
-- Desc:
---------------------------------------------------------
PROCEDURE menu_object_adm
	(
		p_menu_object_pk		in menu_object.menu_object_pk%type				default NULL,
		p_menu_pk				in menu.menu_pk%type							default NULL,
		p_menu_object_type_pk	in menu_object_type.menu_object_type_pk%type	default NULL,
		p_menu_serving_type_pk	in menu_serving_type.menu_serving_type_pk%type	default NULL,
		p_serving_period_pk		in owa_util.ident_arr							default empty_array,
		p_order_number			in varchar2										default NULL,
		p_title					in menu_object_text.title%type					default NULL,
		p_description			in menu_object_text.description%type			default NULL,
		p_size_price_pk			in size_price.size_price_pk%type				default NULL,
		p_quantity				in size_price.quantity%type						default NULL,
		p_price					in varchar2										default NULL,
		p_activated				in menu_object.activated%type					default NULL,
		p_unique				in varchar2										default NULL,
		p_task					in varchar2										default NULL
	)
IS
BEGIN
DECLARE

	v_organization_pk		organization.organization_pk%type			default get.oid;
	v_check					number										default NULL;
	v_task					varchar2(20)								default NULL;
	v_old_task				varchar2(20)								default NULL;
	v_menu_object_pk		menu_object.menu_object_pk%type				default NULL;
	v_price					varchar2(20)								default NULL;
	v_order_number			menu_object.order_number%type				default NULL;
	v_currency_pk			currency.currency_pk%type					default get.cur;
	v_language_pk			la.language_pk%type							default get.lan;
	v_title					menu_object_text.title%type					default NULL;
	v_name_sg_fk_1			string_group.name%type						default NULL;
	v_name_sg_fk_2			string_group.name%type						default NULL;
	v_name_sg_fk_3			string_group.name%type						default NULL;
	v_menu_pk				menu.menu_pk%type							default NULL;
	v_description			menu_object_text.description%type			default NULL;
	v_button_text			varchar2(50)								default NULL;
	v_menu_serving_type_pk	menu_serving_type.menu_serving_type_pk%type	default NULL;
	v_menu_object_type_pk	menu_object_type.menu_object_type_pk%type	default NULL;
	v_i						number										default NULL;
	v_serving_period_pk		owa_util.ident_arr							default empty_array;
	v_menu_type_pk			menu_type.menu_type_pk%type					default NULL;
	v_activated				menu_object.activated%type					default NULL;
	v_size_price_pk			size_price.size_price_pk%type				default NULL;
	v_quantity				size_price.quantity%type					default NULL;

	CURSOR	select_all IS
	SELECT	mo.menu_object_pk, mo.order_number, mote.title, mt.name_sg_fk, moty.name_sg_fk, mst.name_sg_fk, mo.activated
	FROM	menu_object mo, menu_type mt, menu_object_text mote, menu_object_type moty, menu_serving_type mst, menu m, string_group sg, groups g, strng s, string_group sg2, groups g2, strng s2
	WHERE	mo.menu_object_type_fk = moty.menu_object_type_pk
	AND		moty.menu_type_fk = mt.menu_type_pk
	AND		mo.menu_serving_type_fk = mst.menu_serving_type_pk
	AND		mote.menu_object_fk = mo.menu_object_pk
	AND		mote.language_fk = v_language_pk
	AND		mt.menu_type_pk = v_menu_type_pk
	AND		mo.menu_fk = m.menu_pk
	AND		m.organization_fk = v_organization_pk
	AND		mst.name_sg_fk = sg.string_group_pk
	AND		mst.name_sg_fk = sg.string_group_pk
	AND		g.string_group_fk = sg.string_group_pk
	AND		g.string_fk = s.string_pk
	AND		s.language_fk = v_language_pk
	AND		moty.name_sg_fk = sg2.string_group_pk
	AND		g2.string_group_fk = sg2.string_group_pk
	AND		g2.string_fk = s2.string_pk
	AND		s2.language_fk = v_language_pk
	ORDER BY mt.name_sg_fk, s.string, s2.string, mo.order_number;

	CURSOR	select_serving_period IS
	SELECT	serving_period_fk
	FROM 	object_served
	WHERE	menu_object_fk = p_menu_object_pk
	AND		organization_fk = v_organization_pk;

	CURSOR	select_size_price IS
	SELECT	size_price_pk, quantity, price
	FROM	size_price
	WHERE	menu_object_fk = p_menu_object_pk
	ORDER BY price;

BEGIN
if ( login.timeout('menu_org.serving_period_adm')>0 ) then


	-- vi har behov for � kunne endre task, derfor legges innholdet inn i en variabel
	v_task := p_task;


	-- dersom en org. ikke har tilknyttet noen menu_objects s� skal vi f� opp
	-- insert formet med en gang. Endrer derfor v_task	
	if (owa_pattern.match(v_task,'^list$')) then
		SELECT	count(*)
		INTO	v_check
		FROM	menu_object mo, menu m
		WHERE	m.menu_pk = mo.menu_fk
		AND		mo.menu_fk = p_menu_pk
		AND		m.organization_fk = v_organization_pk;
		
		if (v_check = 0) then
			v_task := 'insert_first';
		end if;
	end if;


	-- dersom man fors�ker � legge inn info eller oppdatere info s� m� man ha skrevet 
	-- inn n�dvendig informasjon, hvis ikke forandres status til "error_[insert/update]_[ettellerannet]"
	if ( owa_pattern.match(v_task,'^insert_action$') ) then
		if (p_title is null) then
			v_task := 'error_insert_title';
		end if;
	elsif ( owa_pattern.match(v_task,'^update_action$') ) then
		if (p_title is null) then
			v_task := 'error_update_title';
		end if;
	end if;


	-- -------------
	-- list
	-- -------------
	if (owa_pattern.match(v_task,'^list$')) then

		-- m� ha hentet ut "v_menu_type_pk" n�r vi g�r igjennom "select_all" cursoren og "name_sg_fk" for � skrive navnet p� menyen i b_box
		SELECT	menu_type_pk, name_sg_fk
		INTO	v_menu_type_pk, v_name_sg_fk_1
		FROM	menu, menu_type
		WHERE	menu_type_fk = menu_type_pk
		AND		menu_pk = p_menu_pk;

		html.b_page;
		html.b_box( get.txt('all_menu_objects') ||' '|| get.text(v_name_sg_fk_1) );
		html.b_table;

		htp.p('
			<tr>
				<td><b>'|| get.txt('order_number') ||':</b></td>
				<td><b>'|| get.txt('menu_object_title') ||':</b></td>
				<td><b>'|| get.txt('menu_serving_type') ||':</b></td>
				<td><b>'|| get.txt('menu_object_type') ||':</b></td>
				<td><b>'|| get.txt('status') ||':</b></td>
				<td><b>'|| get.txt('update') ||':</b></td>
				<td><b>'|| get.txt('delete') ||':</b></td>
			</tr>
			<tr><td colspan="7">&nbsp;</td></tr>
		');

		open select_all;
		loop
			fetch select_all into v_menu_object_pk, v_order_number, v_title, v_name_sg_fk_1, v_name_sg_fk_2, v_name_sg_fk_3, v_activated;
			exit when select_all%NOTFOUND;
		
			htp.p('
				<tr>
					<td valign="top">'||v_order_number||'&nbsp;</td>
					<td valign="top">'||v_title||'</td>
					<td valign="top">'||get.text(v_name_sg_fk_3)||'</td>
					<td valign="top">'||get.text(v_name_sg_fk_2)||'</td>
					<td>');
					
					if( v_activated IS NULL ) then
						html.button_link( get.txt('deactivated') ,'menu_org.menu_object_adm?p_menu_object_pk='||v_menu_object_pk||'&p_menu_pk='||p_menu_pk||'&p_task=activate');
					else
						html.button_link( get.txt('activated') ,'menu_org.menu_object_adm?p_menu_object_pk='||v_menu_object_pk||'&p_menu_pk='||p_menu_pk||'&p_task=deactivate');
					end if;

					htp.p('</td>
					<td>'); 
						html.button_link( get.txt('update') ,'menu_org.menu_object_adm?p_menu_object_pk='||v_menu_object_pk||'&p_task=update&p_unique='||to_char(sysdate, get.txt('date_full')));
					htp.p('</td>
					<td>'); 
						html.button_link( get.txt('delete') ,'menu_org.menu_object_adm?p_menu_object_pk='||v_menu_object_pk||'&p_menu_pk='||p_menu_pk||'&p_task=delete');
					htp.p('</td>
				</tr>
			');
		
		end loop;
		close select_all;
		

		htp.p('<tr><td colspan="7" align="right">'); 
		html.button_link( get.txt('add_new_menu_object'), 'menu_org.menu_object_adm?p_menu_pk='||p_menu_pk||'&p_task=insert&p_unique='||to_char(sysdate, get.txt('date_full'))); 
		htp.p('</td></tr>');
		htp.p('<tr><td colspan="7" align="left">'); 
		html.button_link( get.txt('back_to_menu_start'), 'menu_org.startup?p_unique='||to_char(sysdate, get.txt('date_full')));
		htp.p('</td></tr>');
		
		html.e_table;
		html.e_box;
		html.e_page;

	-- -------------
	-- insert or update
	-- -------------
	elsif (owa_pattern.match(v_task,'^insert$') or owa_pattern.match(v_task,'^update$') or owa_pattern.match(v_task, '^error') or owa_pattern.match(v_task,'^insert_first$') ) then

		v_old_task := v_task;

		if ( owa_pattern.match(v_task,'^insert$') or owa_pattern.match(v_task,'^insert_first$') ) then

			v_task 					:= 'insert_action';
			v_menu_pk 				:= p_menu_pk;
			v_button_text 			:= 'insert_menu_object';

		elsif ( owa_pattern.match(v_task,'^error') ) then

			if ( owa_pattern.match(v_task,'^error_insert') ) then
				v_task 			:= 'insert_action';
				v_button_text	:= 'insert_menu_object';
			elsif ( owa_pattern.match(v_task,'^error_update') ) then
				v_task 			:= 'update_action';
				v_button_text 	:= 'update_menu_object';
			end if;

			v_menu_pk 				:= p_menu_pk;
			v_order_number			:= p_order_number;
			v_title 				:= p_title;
			v_description 			:= p_description;
			v_menu_object_type_pk 	:= p_menu_object_type_pk;
			v_menu_serving_type_pk	:= p_menu_serving_type_pk;
			v_menu_object_pk 		:= p_menu_object_pk;
			v_serving_period_pk		:= p_serving_period_pk;			

		else
		
			SELECT	mo.menu_fk, order_number, mote.title, mote.description, mo.menu_object_type_fk, mo.menu_serving_type_fk
			INTO	v_menu_pk, v_order_number, v_title, v_description, v_menu_object_type_pk, v_menu_serving_type_pk
			FROM	menu_object mo, menu_object_text mote
			WHERE	mo.menu_object_pk = mote.menu_object_fk
			AND		mote.language_fk = v_language_pk
			AND		mo.menu_object_pk = p_menu_object_pk;


			-- henter ut info om n�r retten serveres og legger dette i et "array" som sendes med til html.select_organization_serving
			open select_serving_period;
			loop
				fetch select_serving_period into v_serving_period_pk( (select_serving_period%ROWCOUNT+1) );
				exit when select_serving_period%NOTFOUND;		
			end loop;
			close select_serving_period;
			
			v_task := 'update_action';
			v_menu_object_pk := p_menu_object_pk;
			v_button_text 	:= 'update_menu_object';

		end if;


		-- m� ha menu_type_pk for � liste ut oversikt over typer m�ltid og mat
		SELECT	menu_type_fk
		INTO	v_menu_type_pk
		FROM	menu
		WHERE	menu_pk = v_menu_pk;


		html.b_page;
		html.b_box( get.txt('insert_new_menu_object'), '100%', 'menu_org.menu_object_adm');
		html.b_form('menu_org.menu_object_adm');
		htp.p('
			<input type="hidden" name="p_task" value="'||v_task||'">
			<input type="hidden" name="p_menu_pk" value="'|| v_menu_pk ||'">
			<input type="hidden" name="p_menu_object_pk" value="'|| v_menu_object_pk ||'">
		');
		html.b_table('100%',0);
		htp.p('
			<tr><td colspan="2">'|| get.txt('menu_object_adm_desc') ||'</td></tr>
			<tr><td colspan="2">&nbsp;</td></tr>
			<tr>
				<td><b>'|| get.txt('menu_serving_type') ||':</b></td>
				<td>'); html.select_menu_serving_type(v_menu_serving_type_pk, v_menu_type_pk); htp.p('</td>
			</tr>
			<tr>
				<td><b>'|| get.txt('menu_object_type') ||':</b></td>
				<td>'); html.select_menu_object_type(v_menu_object_type_pk, v_menu_type_pk); htp.p('</td>
			</tr>
			<tr>
				<td valign="top"><b>'|| get.txt('serving_period_for_org') ||':</b></td>
				<td>'); html.select_organization_serving(v_serving_period_pk); htp.p('</td>
			</tr>
			<tr>
				<td><b>'|| get.txt('order_number') ||':</b></td>
				<td>'|| html.text('p_order_number', 2, 4, v_order_number) ||'</td>
			</tr>
			<tr>
				<td>
		');

		if owa_pattern.match(v_old_task,'^error_.*_title$') then
			htp.p('<font color="#ff0000"><b>'|| get.txt('order_title') ||':</b></font>');
		else
			htp.p('<b>'|| get.txt('order_title') ||':</b>');
		end if;

		htp.p('
				</td>
				<td>'|| html.text('p_title', 20, 250, v_title) ||'</td>
			</tr>
			<tr>
				<td valign="top"><b>'|| get.txt('order_desc') ||':</b></td>
				<td>
					<textarea cols="50" rows="10" wrap="off" name="p_description">'||v_description||'</textarea>
				</td>
			</tr>
			<tr>
				<td valign="top"><b>'|| get.txt('order_size_prize') ||':</b></td>
				<td>
					<table border="0">
						<tr><td>'|| get.txt('size') ||':</td><td>'|| get.txt('prize') ||':</td><td>'|| get.txt('delete') ||':</td>');
				
						open select_size_price;
						loop
							fetch select_size_price into v_size_price_pk, v_quantity, v_price;
							exit when select_size_price%NOTFOUND;
							htp.p('<tr><td>'||v_quantity||'&nbsp;</td><td>'||v_price||'&nbsp;</td><td><a href="menu_org.menu_object_adm?p_size_price_pk='||v_size_price_pk||'&p_task=delete_sizeprice&p_menu_object_pk='||p_menu_object_pk||'">'|| get.txt('delete_size_price') ||'</a></td></tr>');
						end loop;
						close select_size_price;

						htp.p('<tr><td>'|| html.text('p_quantity', 8, 20, NULL) ||'</td><td>'|| html.text('p_price', 4, 6, NULL) ||' '|| get.curr ||'</td><td>'); html.submit_link( get.txt('add_size_price') ); htp.p('</td></tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="right">'); html.submit_link( get.txt(v_button_text) ); htp.p('</td>
			</tr>
			<tr>
				<td colspan="2" align="left">');
				if ( owa_pattern.match(v_old_task,'^insert_first$') ) then
					html.button_link( get.txt('back_to_menu_start'), 'menu_org.startup?p_unique='||to_char(sysdate, get.txt('date_full')));
				else
					html.button_link( get.txt('back_to_menu_overview'), 'menu_org.menu_object_adm?p_task=list&p_menu_pk='||v_menu_pk||'&p_unique='||to_char(sysdate, get.txt('date_full')));
				end if;
			htp.p('</td></tr>');

		html.e_table;
		html.e_form;
		html.e_box;
		html.e_page;

	-- -------------
	-- insert action
	-- -------------
	elsif (owa_pattern.match(v_task,'^insert_action$')) then

		-- behandler input data
		v_price 		:= p_price;
		owa_pattern.change(v_price, '\.', ',');

		-- sjekker om prisen er p� riktig format
		if ( 
			owa_pattern.match(v_price, '^\d{1,}$') or
			owa_pattern.match(v_price, '^,\d{1,}$') or
			owa_pattern.match(v_price, '^\d{1,},\d{1,}$')
		) then
			v_price := v_price;
		else
			v_price := NULL;
		end if;

		-- sjekker at sorteringsnummer er p� riktig format	
		if ( owa_pattern.match(p_order_number, '^\d{1,}$') ) then
			v_order_number := p_order_number;
		else
			v_order_number := NULL;
		end if;

		SELECT	menu_object_seq.NEXTVAL
		INTO	v_menu_object_pk
		FROM	dual;

		SELECT	size_price_seq.NEXTVAL
		INTO	v_size_price_pk
		FROM	dual;

		INSERT INTO menu_object
		(menu_object_pk, menu_fk, menu_object_type_fk, menu_serving_type_fk, order_number, activated)
		VALUES(v_menu_object_pk, p_menu_pk, p_menu_object_type_pk, p_menu_serving_type_pk, v_order_number, 1);
		commit;

		INSERT INTO menu_object_text
		(menu_object_fk, language_fk, currency_fk, title, description)
		VALUES(v_menu_object_pk, v_language_pk, v_currency_pk, p_title, p_description);
		commit;

		INSERT INTO size_price
		(size_price_pk, menu_object_fk, quantity, price)
		VALUES(v_size_price_pk, v_menu_object_pk, p_quantity, v_price);
		commit;

		FOR v_i IN 1 .. p_serving_period_pk.COUNT
		LOOP
			INSERT INTO object_served
			(menu_object_fk, organization_fk, serving_period_fk)
			VALUES(v_menu_object_pk, v_organization_pk, p_serving_period_pk(v_i));
			commit;
		END LOOP;

		html.jump_to('menu_org.menu_object_adm?p_task=update&p_menu_object_pk='||v_menu_object_pk||'&p_unique='||to_char(sysdate, get.txt('date_full')));

	-- -------------
	-- update action
	-- -------------
	elsif (owa_pattern.match(v_task,'^update_action$')) then

		-- behandler input data
		v_price 		:= p_price;
		owa_pattern.change(v_price, '\.', ',');

		-- sjekker om prisen er p� riktig format
		if ( 
			owa_pattern.match(v_price, '^\d{1,}$') or
			owa_pattern.match(v_price, '^,\d{1,}$') or
			owa_pattern.match(v_price, '^\d{1,},\d{1,}$')
		) then
			v_price := v_price;
		else
			v_price := NULL;
		end if;

		-- sjekker at sorteringsunner er p� riktig form
		if ( owa_pattern.match(p_order_number, '^\d{1,}$') ) then
			v_order_number := p_order_number;
		else
			v_order_number := NULL;
		end if;

		UPDATE	menu_object
		SET		menu_serving_type_fk	= p_menu_serving_type_pk,
				menu_object_type_fk		= p_menu_object_type_pk,
				order_number			= v_order_number
		WHERE	menu_object_pk 			= p_menu_object_pk;
		commit;
		
		UPDATE	menu_object_text
		SET		title 					= p_title,
				description 			= p_description
		WHERE	menu_object_fk			= p_menu_object_pk
		AND		language_fk				= v_language_pk;
		commit;

		if (v_price IS NOT NULL) then

			SELECT	size_price_seq.NEXTVAL
			INTO	v_size_price_pk
			FROM	dual;
	
			INSERT INTO size_price
			(size_price_pk, menu_object_fk, quantity, price)
			VALUES(v_size_price_pk, p_menu_object_pk, p_quantity, to_number(v_price));
			commit;
		
		end if;
	
		DELETE FROM object_served
		WHERE menu_object_fk = p_menu_object_pk;
		commit;
	
		FOR v_i IN 1 .. p_serving_period_pk.COUNT
		LOOP
			INSERT INTO object_served
			(menu_object_fk, organization_fk, serving_period_fk)
			VALUES(p_menu_object_pk, v_organization_pk, p_serving_period_pk(v_i));
			commit;
		END LOOP;

		html.jump_to('menu_org.menu_object_adm?p_task=update&p_menu_object_pk='||p_menu_object_pk||'&p_unique='||to_char(sysdate, get.txt('date_full')));

	-- -------------
	-- delete (confirm)
	-- -------------
	elsif (owa_pattern.match(v_task,'^delete$')) then

		SELECT	title
		INTO	v_title
		FROM	menu_object_text
		WHERE	menu_object_fk = p_menu_object_pk
		AND		language_fk = v_language_pk;

		html.b_page;
		html.b_box( get.txt('confirm_delete_menu_object'));
		html.b_form('menu_org.menu_object_adm');
		html.b_table;

        htp.p('
                <input type="hidden" name="p_task" value="delete_action">
                <input type="hidden" name="p_menu_object_pk" value="'|| p_menu_object_pk ||'">
                <input type="hidden" name="p_menu_pk" value="'|| p_menu_pk ||'">
                <tr><td> '|| get.txt('sure_to_delete') ||' '''||v_title||'''?</td></tr>
                <tr><td>'); html.submit_link( get.txt('delete_menu_object') ); htp.p('</td></tr>
                <tr><td>'); html.back( get.txt('dont_delete_menu_object') ); htp.p('</td></tr>
        ');

		html.e_table;
		html.e_form;
		html.e_box;
		html.e_page;

		return;

	-- -------------
	-- delete action
	-- -------------
	elsif (owa_pattern.match(v_task,'^delete_action$')) then

		DELETE from menu_object
		WHERE menu_object_pk = p_menu_object_pk;
		commit;

		html.jump_to('menu_org.menu_object_adm?p_task=list&p_menu_pk='||p_menu_pk||'&p_unique='||to_char(sysdate, get.txt('date_full')));

	-- -------------
	-- (de)actiavte
	-- -------------
	elsif (owa_pattern.match(v_task,'activate$')) then

		if (owa_pattern.match(v_task,'^activate$')) then
		
			UPDATE	menu_object
			SET		activated = 1
			WHERE	menu_object_pk = p_menu_object_pk;
			commit;
		
		elsif (owa_pattern.match(v_task,'^deactivate$')) then

			UPDATE	menu_object
			SET		activated = NULL
			WHERE	menu_object_pk = p_menu_object_pk;
			commit;

		end if;

		html.jump_to('menu_org.menu_object_adm?p_task=list&p_menu_pk='||p_menu_pk||'&p_unique='||to_char(sysdate, get.txt('date_full')));

	-- -------------
	-- delete action
	-- -------------
	elsif (owa_pattern.match(v_task,'^delete_sizeprice$')) then

		DELETE from size_price
		WHERE size_price_pk = p_size_price_pk;
		commit;

		html.jump_to('menu_org.menu_object_adm?p_task=update&p_menu_object_pk='||p_menu_object_pk||'&p_unique='||to_char(sysdate, get.txt('date_full')));

	end if;



end if;
END;
END menu_object_adm;




-- ++++++++++++++++++++++++++++++++++++++++++++++ --

END; -- slutter pakke kroppen
/
show errors;
