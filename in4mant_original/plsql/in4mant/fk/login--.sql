set define off
PROMPT *** package: LOGIN ***

CREATE OR REPLACE PACKAGE login IS

        PROCEDURE startup;
        PROCEDURE login_form
                (
                        p_url           in varchar2     default NULL,
                        p_heading       in varchar2     default NULL,
                        p_width         in varchar2     default '40%'
                );
        PROCEDURE login_action
                (
                        p_login_name            in usr.login_name%type          default NULL,
                        p_password              in usr.password%type            default NULL,
                        p_url                   in varchar2                     default NULL,
                        p_heading               in varchar2                     default NULL
                );
        FUNCTION timeout
                (
                        p_url           in varchar2     default NULL,
                        p_return_val    in number       default NULL,
                        p_heading       in varchar2     default NULL,
                        p_width         in varchar2     default '40%'
                )RETURN NUMBER;
        PROCEDURE logout;
        FUNCTION right
                (
                        p_name                  in module.name%type             default NULL
                )RETURN NUMBER;

END;
/
CREATE OR REPLACE PACKAGE BODY login IS

-- **************************************************
-- "PUBLIC METODER" (KALLES DIREKTE FRA WEB)
-- **************************************************

---------------------------------------------------------
-- Name:        startup
-- Type:        procedure
-- What:        start prosedyren, for � kj�re pakken
-- Author:      Frode Klevstul
-- Start date:  06.06.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter] [description]
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

        login_form;

END startup;

-- **************************************************
-- "PRIVATE METODER" (KALLES IKKE DIREKTE FRA WEB)
-- **************************************************

---------------------------------------------------------
-- Name:        login_form
-- Type:        procedure
-- What:        login window
-- Author:      Frode Klevstul
-- Start date:  15 05 2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter] [description]
---------------------------------------------------------

PROCEDURE login_form
        (
                p_url           in varchar2     default NULL,
                p_heading       in varchar2     default NULL,
                p_width         in varchar2     default '40%'
        )
IS
BEGIN
DECLARE
        v_login_name    strng.string%type       default NULL;
        v_password      strng.string%type       default NULL;
        v_login         strng.string%type       default NULL;
        v_register      strng.string%type       default NULL;

BEGIN

        v_login_name    := get.txt('login_name');
        v_password      := get.txt('password');
        v_login         := get.txt('login');
        v_register      := get.txt('register_new_user');

        if (p_heading IS NULL) then
                html.b_page;
                htp.p('<br><center>');
--                htp.p('</td></tr></table><br><br>');
        elsif (p_heading = '2') then
                html.b_page_2(p_width, NULL);
        end if;

        if ( login.timeout(NULL, 1) > 0) then
                html.b_box_2( get.txt('logged_in_as')||' '''||get.login_name||'''', p_width, 'login.login_form');
        else
                html.b_box_2( get.txt('login'), p_width, 'login.login_form');
        end if;
        htp.p('
                <script language="javascript">
                function checkLogin(){
                        var f = document.login_form;
                        if((!f.p_password.value) || (!f.p_login_name.value)){
                                alert("'||get.txt('no_login_passwd')||'");
                                return false;
                        }
                        else {
                                return true;
                        }
                }
                </script>
        ');
        html.b_form('login.login_action','login_form','" onSubmit="Javascript: return checkLogin();');
        html.b_table;

        -- dersom p_heading = NULL eller '2' s� skriver vi ut b_page + hjelpetekst
        if (p_heading IS NULL OR p_heading = '2') then
                htp.p('<tr><td colspan="2">'|| get.txt('login_desc') ||'</td></tr>');
                htp.p('<tr><td colspan="2">&nbsp;</td></tr>');
        end if;

        htp.p('
                <input type="hidden" name="p_url" value="'|| p_url ||'">
                <input type="hidden" name="p_heading" value="'|| p_heading ||'">
                <tr><td>'|| v_login_name ||':</td><td><input type="Text" name="p_login_name" maxlength="150" size="12"></td></tr>
                <tr><td>'|| v_password ||':</td><td><input type="Password" name="p_password" maxlength="50" size="12"></td></tr>
                <tr><td colspan=2 align="right">'); html.submit_link(v_login, 'login_form'); htp.p('</td></tr>
                <tr><td colspan=2>'); html.button_link_2(v_register, 'users.startup', '_top'); htp.p('</td></tr>
                <tr><td colspan=2>'); html.button_link_2(get.txt('register_new_organization'), 'users.register_form?p_org_reg=1', '_top'); htp.p('</td></tr>
                <tr><td colspan=2>'); html.button_link_2(get.txt('forgot_your_password'), 'tip.login', '_top'); htp.p('</td></tr>
        ');

        html.e_table;
        html.e_form;

        html.e_box_2;

        htp.p('
                <script>
                document.login_form.p_login_name.focus();
                </script>
        ');

        if (p_heading IS NULL) then
                htp.p('</center><br>');
                html.e_page;
        elsif (p_heading = '2') then
                html.e_page_2;
        end if;

END;
END login_form;


---------------------------------------------------------
-- Name:        login_action
-- Type:        procedure
-- What:        inserts data into db if u/p is OK
-- Author:      Frode Klevstul
-- Start date:  11 06 2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter] [description]
---------------------------------------------------------
PROCEDURE login_action
        (
                p_login_name            in usr.login_name%type          default NULL,
                p_password              in usr.password%type            default NULL,
                p_url                   in varchar2                     default NULL,
                p_heading               in varchar2                     default NULL
        )
IS
BEGIN
DECLARE

        v_user_pk               usr.user_pk%type                default NULL;
        v_check                 number                          default NULL;
        v_session_id            sessn.session_id%type           default NULL;
        v_logout_time           user_type.logout_time%type      default NULL;
        v_sessn_pk              sessn.sessn_pk%type             default NULL;
        v_user_stat_pk          user_stat.user_stat_pk%type     default NULL;
        v_user_status_fk        usr.user_status_fk%type         default NULL;
        v_user_type_fk          user_type.user_type_pk%type     default NULL;
        v_url                   varchar2(500)                   default NULL;

BEGIN

        SELECT  count(*) INTO v_check
        FROM    usr
        WHERE   login_name = p_login_name
        AND     password = p_password;
        IF ( v_check = 0 ) then

                SELECT  count(*) INTO v_check
                FROM    usr
                WHERE   login_name = p_login_name;
                if (v_check <> 0) then  -- riktig brukernavn, men feil passord

                        SELECT  user_status_fk INTO v_user_status_fk
                        FROM    usr
                        WHERE   login_name = p_login_name;
                        if (v_user_status_fk = -19) then
                                UPDATE  usr
                                SET     user_status_fk = -50   -- -50: suspended because of to many wrong logins
                                WHERE   login_name = p_login_name;
                                commit;
                                get.error_page(3, get.txt('wrong_login_account_suspended') );
                                return;
                        elsif (v_user_status_fk = -18) then
                                UPDATE  usr
                                SET     user_status_fk = -19   -- -19: 5 wrong logins
                                WHERE   login_name = p_login_name;
                                commit;
                                get.error_page(3, get.txt('wrong_login_5') );
                                return;
                        elsif (v_user_status_fk = -17) then
                                UPDATE  usr
                                SET     user_status_fk = -18   -- -18: 4 wrong logins
                                WHERE   login_name = p_login_name;
                                commit;
                                get.error_page(3, get.txt('wrong_login_4') );
                                return;
                        elsif (v_user_status_fk = -16) then
                                UPDATE  usr
                                SET     user_status_fk = -17   -- -17: 3 wrong logins
                                WHERE   login_name = p_login_name;
                                commit;
                                get.error_page(3, get.txt('wrong_login_3') );
                                return;
                        elsif (v_user_status_fk = -15) then
                                UPDATE  usr
                                SET     user_status_fk = -16   -- -16: 2 wrong logins
                                WHERE   login_name = p_login_name;
                                commit;
                                get.error_page(3, get.txt('wrong_login_2') );
                                return;
                        elsif (v_user_status_fk = -4) then      -- -4: the user has logged in several times (should know the password)
                                UPDATE  usr
                                SET     user_status_fk = -15   -- -15: 1 wrong logins
                                WHERE   login_name = p_login_name;
                                commit;
                                get.error_page(3, get.txt('wrong_login_1') );
                                return;
                        else                                    -- brukeren har ikke v�rt logget inn mer enn maks to ganger
                                get.error_page(3, get.txt('wrong_login_1') );
                                return;
                        end if;
                else    -- ukjent brukernavn
                        get.error_page(3, get.txt('wrong_login_1'), p_heading, p_url);
                        return;
                end if;

                login_form(p_url);
        else
                SELECT logout_time, user_status_fk, user_pk, user_type_fk
                INTO v_logout_time, v_user_status_fk, v_user_pk, v_user_type_fk
                FROM usr, user_type
                WHERE login_name = p_login_name
                AND password = p_password
                AND user_type_fk = user_type_pk;

                -- sjekker om bruker status er i orden
                if ( (v_user_status_fk > -53) AND (v_user_status_fk < -49) ) then
                        html.jump_to( get.value('susp_account') );
                        return; -- fortsetter ikke videre nedover i koden
                end if;

                -- henter ut gjennomsnittsverdien til session_id i sessn tabellen
                -- dersom denne verdien er NULL s� settes den til '1', ellers settes den til avg()
                -- parameter 1: hva vi skal sjekke (hente ut)
                -- parameter 2: exception (dersom resultatet er dette)
                -- parameter 3: hvis exception, s� returneres denne verdien
                -- parameter 4: hvis ikke exception hentes denne verdien
                SELECT decode( avg(session_id+1), NULL, 1, avg(session_id+1))
                INTO v_session_id
                FROM sessn;

                -- generer et unikt sesjonsnummer (maks 20 tegn)
                v_session_id := SUBSTR(  (to_number(to_char(sysdate, 'YYYYMMDDHH24MISS'))/v_session_id/180876), 1, 20);

                -- setter cookie i browseren
                system.set_cookie('in4mant_session', v_session_id);

                SELECT sessn_seq.NEXTVAL
                INTO v_sessn_pk
                FROM dual;

                -- legger inn i sessn tabellen
                INSERT INTO sessn (sessn_pk, session_id, user_fk, login_time, last_action, logout_time)
                VALUES(v_sessn_pk, v_session_id, v_user_pk, sysdate, sysdate, v_logout_time);
                commit;

                -- legger inn i bruker statistikk tabellen
                SELECT user_stat_seq.NEXTVAL
                INTO v_user_stat_pk
                FROM dual;

                INSERT INTO user_stat (user_stat_pk, user_fk, ip_address, host_name, login_time)
                VALUES (v_user_stat_pk, v_user_pk, owa_util.get_cgi_env('REMOTE_ADDR'), owa_util.get_cgi_env('REMOTE_HOST'), sysdate );
                commit;

                -- endrer bruker status (for hvor mange ganger brukeren har v�rt logget inn)
                if (v_user_status_fk = -1) then                 -- -1: never logged in after registration
                        UPDATE  usr
                        SET     user_status_fk = -2             -- -2: logged in one time after registration
                        WHERE   user_pk = v_user_pk;
                        commit;
                elsif (v_user_status_fk = -2) then
                        UPDATE  usr
                        SET     user_status_fk = -3             -- -3: logged in two times
                        WHERE   user_pk = v_user_pk;
                        commit;
                elsif (v_user_status_fk = -3) then
                        UPDATE  usr
                        SET     user_status_fk = -4             -- -4: logged in three (or more) times
                        WHERE   user_pk = v_user_pk;
                        commit;
                elsif ( (v_user_status_fk > -20) AND (v_user_status_fk < -14) ) then -- dersom feil login
                        UPDATE  usr
                        SET     user_status_fk = -4             -- -4: logged in three (or more) times
                        WHERE   user_pk = v_user_pk;            -- (fordi feil login kun registreres dersom status = -4 fra f�r)
                        commit;
                end if;

                if ( p_url IS NULL) then
                        if (v_user_type_fk = -2) then
                                html.jump_to( 'http://admin.bytur.no/cgi-bin/'||get.value('login_client_url') );
                        else
                                html.jump_to( get.value('login_url') );
                        end if;
                else
                        v_url := p_url;

                        if (v_user_type_fk = -2) then
                                -- Client brukere kommer alltid inn p� samme side ved login (utenom f�rste gang, registrering):
                                if ( owa_pattern.match(p_url, 'admin.edit_org_info') ) then
                                        v_url := 'http://admin.bytur.no/cgi-bin/'||p_url;
                                else
                                        v_url := 'http://admin.bytur.no/cgi-bin/'||get.value('login_client_url');
                                end if;
                                /*
                                if ( owa_pattern.match(p_url, '.bytur.no') ) then
                                        owa_pattern.change(v_url, '^.*bytur.no', 'http://admin.bytur.no', 'g');
                                elsif ( owa_pattern.match(p_url, 'bytur.no') ) then
                                        v_url := REPLACE (v_url, 'bytur.no', 'admin.bytur.no');
                                else
                                        v_url := 'http://admin.bytur.no/cgi-bin/'||v_url;
                                end if;
                                */
                        end if;
                        html.jump_to(v_url);
                end if;

        end if;


END;
END login_action;





---------------------------------------------------------
-- Name:        timeout
-- Type:        function
-- What:        checks if the session has timed out
-- Author:      Frode Klevstul
-- Start date:  11 06 2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter] [description]
---------------------------------------------------------
FUNCTION timeout
        (
                p_url           in varchar2     default NULL,
                p_return_val    in number       default NULL,
                p_heading       in varchar2     default NULL,
                p_width         in varchar2     default '40%'
        )RETURN NUMBER
IS
BEGIN
DECLARE

        v_return_value          number                  default NULL;
        -- deklarer et tall med 20 siffer og 0 siffer bak komma (dvs. et heltall)
        v_rounded_value         number (20, 0)          default NULL;
        v_session_id            sessn.session_id%type   default NULL;
        v_last_action           sessn.last_action%type  default NULL;
        v_logout_time           sessn.logout_time%type  default NULL;
        v_timeout               date                    default NULL;


BEGIN

        -- retur av negative verdier: antall minutter til timeout
        -- return av positive verdier: ant. minutter siden timeout

        v_session_id := get.sess;

        SELECT  last_action, logout_time
        INTO    v_last_action, v_logout_time
        FROM    sessn
        WHERE   session_id = v_session_id;

        -- timeout: sist brukeren gjorde noe + timeout minutter
        -- enheten som legges til er dager, mens logout_time har enhet minutter
        -- for � gj�re om ett minutt til dag: ( 1 min / (24 (timer) * 60 min) )
        -- 24*60 = 1440 min; i ett d�gn

        -- tidspunktet for n�r brukeren skal f� timeout
        v_timeout := ( v_last_action + (v_logout_time/1440) );


        -- brukes til test:
        /*
        htp.p ( 'sysdate: ' || to_char(sysdate, 'YYYY-MM-DD HH24:MI:SS') || '<br>' );
        htp.p ( 'last action: ' || to_char(v_last_action, 'YYYY-MM-DD HH24:MI:SS') || '<br>' );
        htp.p ( 'timeout date: ' || to_char(v_timeout, 'YYYY-MM-DD HH24:MI:SS') || '<br>' );
        htp.p( 'timeout minutes: '|| v_logout_time || '<br>');
        htp.p( to_char(v_timeout,'YYYYMMDDHH24MISS') || ' - ' || to_char(sysdate,'YYYYMMDDHH24MISS') || '<br>');
        */

        -- tallet som returneres er positivt dersom det ikke er timout
        -- og negativt dersom "sysdate > timeout". Var meningen at talles som blir returnet
        -- skulle v�re antall minutter til/siden timeout. Vet ikke hvordan jeg skal finne
        -- antall minutter mellom to datoer.

        -- runder av til n�rmeste hele tall
        v_rounded_value := to_number(to_char(v_timeout,'YYYYMMDDHH24MISS')) - to_number(to_char(sysdate,'YYYYMMDDHH24MISS'));
        v_return_value := v_rounded_value;


        -- dersom vi ikke har timeout oppdaterer vi "last_action"
        if ( v_return_value > 0 ) then
                UPDATE  sessn
                SET     last_action = sysdate
                WHERE   session_id = v_session_id;
                commit;
        -- hvis vi sender med en verdi i "p_return_val" returneres kun en verdi som sier
        -- hvorvidt brukeren er logget inn eller ikke. Login formet skrives kun ut dersom
        -- "p_return_val" er NULL
        elsif ( p_return_val IS NULL ) then
                login_form(p_url, p_heading, p_width);
        end if;


        RETURN v_return_value;

EXCEPTION
        WHEN NO_DATA_FOUND     -- hvis den ikke finner noen cookie s� kommer login
        THEN
                if ( p_return_val IS NULL ) then
                        login_form(p_url, p_heading, p_width);
                end if;
                RETURN NULL;

END;
END timeout;




---------------------------------------------------------
-- Name:        logout
-- Type:        procedure
-- What:        remove the cookie, and logs out the user
-- Author:      Frode Klevstul
-- Start date:  11 06 2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter] [description]
---------------------------------------------------------
PROCEDURE logout
IS
BEGIN
DECLARE

        v_session_id            sessn.session_id%type   default NULL;

BEGIN

        v_session_id := get.sess;

        if ( v_session_id IS NOT NULL) then
                -- oppdaterer database
                UPDATE  sessn
                SET     last_action = '18-AUG-1976'
                WHERE   session_id = v_session_id;
                commit;

                -- fjerner cookien
                system.remove_cookie('in4mant_session');
        end if;

        html.jump_to( get.value('logout_url') );

END;
END; -- prosedyren logout





---------------------------------------------------------
-- Name:        right
-- Type:        function
-- What:        checks if the user has right to given module(package.procedure)
-- Author:      Frode Klevstul
-- Start date:  20 06 2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter] [description]
---------------------------------------------------------
FUNCTION right
        (
                p_name                  in module.name%type             default NULL
        )RETURN NUMBER
IS
BEGIN
DECLARE
        v_check         number                  default NULL;
        v_user_pk       usr.user_pk%type        default NULL;
        v_user_type_fk  usr.user_type_fk%type   default NULL;
BEGIN

        v_user_pk := get.uid;

        SELECT  user_type_fk INTO v_user_type_fk
        FROM    usr
        WHERE   user_pk = v_user_pk;

        if ( v_user_type_fk = -4 ) then -- super admin user
                return 1;
        else
                SELECT  count(*) INTO v_check
                FROM    module, user_right
                WHERE   module_pk = module_fk
                AND     name = p_name
                AND     user_fk = v_user_pk;

                if (v_check = 0) then
                        -- htp.p (get.msg(1, 'The user has no access to this module') );
                        return 0;       -- her er det en sperre p� gitt modul
                else
                        return 1;       -- ingen sperre
                end if;
        end if;

END;
END right;




-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/
show errors;
