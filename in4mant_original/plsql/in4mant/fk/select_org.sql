set define off
PROMPT *** package: select_org ***

CREATE OR REPLACE PACKAGE select_org IS

        PROCEDURE startup;
        PROCEDURE list_out
                (
                        p_service_pk            in service.service_pk%type      default NULL,
                        p_list_pk               in list.list_pk%type            default NULL,
                        p_name                  in varchar2                     default NULL,
                        p_org_name              in organization.name%type       default NULL,
                        p_confirm_list_out      in varchar2                     default 'NO',
                        p_url                   in varchar2                     default 'send_url',
                        p_heading               in number                       default NULL,
                        p_geography_pk          in geography.geography_pk%type  default NULL,
                        p_geo_name              in varchar2                     default NULL,
                        p_search_type           in number                       default NULL
                );

END;
/
CREATE OR REPLACE PACKAGE BODY select_org IS


---------------------------------------------------------
-- Name:        startup
-- Type:        procedure
-- What:        start prosedyren, for � kj�re pakken
-- Author:      Frode Klevstul
-- Start date:  18.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

        list_out(NULL, NULL, NULL, NULL, 'NO', NULL, 2);

END startup;



---------------------------------------------------------
-- Name:        list_out
-- Type:        procedure
-- What:        lister ut alle selskaper, gitt parametere
-- Author:      Frode Klevstul
-- Start date:  18.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE list_out
        (
                p_service_pk            in service.service_pk%type      default NULL,
                p_list_pk               in list.list_pk%type            default NULL,
                p_name                  in varchar2                     default NULL,
                p_org_name              in organization.name%type       default NULL,
                p_confirm_list_out      in varchar2                     default 'YES',
                p_url                   in varchar2                     default 'send_url',
                p_heading               in number                       default NULL,
                p_geography_pk          in geography.geography_pk%type  default NULL,
                p_geo_name              in varchar2                     default NULL,
                p_search_type           in number                       default NULL
        )
IS
BEGIN
DECLARE

        CURSOR  all_services IS
        SELECT  service_pk, name_sg_fk
        FROM    service
        WHERE   activated = 1;

        CURSOR  sel_org_1 IS
        SELECT  organization_pk
        FROM    organization, org_service, service
        WHERE   accepted > 0
        AND	organization_fk = organization_pk
        AND	service_fk = service_pk
        AND	activated = 1
        ORDER BY name;

        CURSOR  sel_org_1geo(v_geography_pk in geography.geography_pk%type) IS
        SELECT  organization_pk
        FROM    organization, geography, org_service, service
        WHERE   geography_pk = geography_fk
        AND     (
                        level0 = v_geography_pk OR
                        level1 = v_geography_pk OR
                        level2 = v_geography_pk OR
                        level3 = v_geography_pk OR
                        level4 = v_geography_pk OR
                        level5 = v_geography_pk
                )
        AND     accepted > 0
        AND	organization_fk = organization_pk
        AND	service_fk = service_pk
        AND	activated = 1
        ORDER BY name;

        CURSOR  sel_org_2(v_name in organization.name%type) IS
        SELECT  organization_pk, name
        FROM    organization, org_service, service
        WHERE   upper(name) LIKE '%'|| upper(v_name) ||'%'
        AND     accepted > 0
        AND	organization_fk = organization_pk
        AND	service_fk = service_pk
        AND	activated = 1
        ORDER BY name;

        CURSOR  sel_org_2geo(v_name in organization.name%type, v_geography_pk in geography.geography_pk%type) IS
        SELECT  organization_pk, name
        FROM    organization, geography, org_service, service
        WHERE   upper(name) LIKE '%'|| upper(v_name) ||'%'
        AND     geography_pk = geography_fk
        AND     (
                        level0 = v_geography_pk OR
                        level1 = v_geography_pk OR
                        level2 = v_geography_pk OR
                        level3 = v_geography_pk OR
                        level4 = v_geography_pk OR
                        level5 = v_geography_pk
                )
        AND     accepted > 0
        AND	organization_fk = organization_pk
        AND	service_fk = service_pk
        AND	activated = 1
        ORDER BY name;

        CURSOR  sel_org_3(v_name in organization.name%type, v_list_pk in list.list_pk%type) IS
        SELECT  DISTINCT organization_pk, name
        FROM    organization, list_org lo, list, org_service os, service
        WHERE   upper(name) LIKE '%'|| upper(v_name) ||'%'
        AND     organization_pk = lo.organization_fk
        AND     list_fk = list_pk
        AND     (
                        level0 = v_list_pk OR
                        level1 = v_list_pk OR
                        level2 = v_list_pk OR
                        level3 = v_list_pk OR
                        level4 = v_list_pk OR
                        level5 = v_list_pk
                )
        AND     accepted > 0
        AND	os.organization_fk = organization_pk
        AND	service_fk = service_pk
        AND	activated = 1
        ORDER BY name;

        CURSOR  sel_org_3geo(v_name in organization.name%type, v_list_pk in list.list_pk%type, v_geography_pk in geography.geography_pk%type) IS
        SELECT  DISTINCT organization_pk, name
        FROM    organization o, list_org lo, list l, org_service os, geography g, service
        WHERE   upper(name) LIKE '%'|| upper(v_name) ||'%'
        AND     organization_pk = lo.organization_fk
        AND     list_fk = list_pk
        AND     (
                        l.level0 = v_list_pk OR
                        l.level1 = v_list_pk OR
                        l.level2 = v_list_pk OR
                        l.level3 = v_list_pk OR
                        l.level4 = v_list_pk OR
                        l.level5 = v_list_pk
                )
        AND     geography_pk = o.geography_fk
        AND     (
                        g.level0 = v_geography_pk OR
                        g.level1 = v_geography_pk OR
                        g.level2 = v_geography_pk OR
                        g.level3 = v_geography_pk OR
                        g.level4 = v_geography_pk OR
                        g.level5 = v_geography_pk
                )
        AND     accepted > 0
        AND	os.organization_fk = organization_pk
        AND	service_fk = service_pk
        AND	activated = 1
        ORDER BY name;

        CURSOR  sel_org_4 (v_service_fk in org_service.service_fk%type default 0) IS
        SELECT  organization_pk
        FROM    organization, org_service
        WHERE   organization_pk = organization_fk
        AND     service_fk = v_service_fk
        AND     accepted > 0
        ORDER BY name;

        CURSOR  sel_org_4geo (v_service_fk in org_service.service_fk%type default 0, v_geography_pk in geography.geography_pk%type) IS
        SELECT  organization_pk
        FROM    organization, org_service, geography g
        WHERE   organization_pk = organization_fk
        AND     service_fk = v_service_fk
        AND     geography_pk = geography_fk
        AND     (
                        g.level0 = v_geography_pk OR
                        g.level1 = v_geography_pk OR
                        g.level2 = v_geography_pk OR
                        g.level3 = v_geography_pk OR
                        g.level4 = v_geography_pk OR
                        g.level5 = v_geography_pk
                )
        AND     accepted > 0
        ORDER BY name;

        CURSOR  sel_org_5(v_name in organization.name%type, v_service_fk in org_service.service_fk%type default 0) IS
        SELECT  DISTINCT organization_pk, name
        FROM    organization, org_service
        WHERE   upper(name) LIKE '%'|| upper(v_name) ||'%'
	AND	organization_fk = organization_pk
        AND     service_fk = v_service_fk
        AND     accepted > 0
        ORDER BY name;

        CURSOR  sel_org_5geo(v_name in organization.name%type, v_service_fk in org_service.service_fk%type default 0, v_geography_pk in geography.geography_pk%type) IS
        SELECT  DISTINCT organization_pk, name
        FROM    organization, org_service, geography g
        WHERE   upper(name) LIKE '%'|| upper(v_name) ||'%'
	AND	organization_fk = organization_pk
        AND     service_fk = v_service_fk
        AND     geography_pk = geography_fk
        AND     (
                        g.level0 = v_geography_pk OR
                        g.level1 = v_geography_pk OR
                        g.level2 = v_geography_pk OR
                        g.level3 = v_geography_pk OR
                        g.level4 = v_geography_pk OR
                        g.level5 = v_geography_pk
                )
        AND     accepted > 0
        ORDER BY name;

        CURSOR  sel_org_6(v_name in organization.name%type, v_list_pk in list.list_pk%type, v_service_fk in org_service.service_fk%type default 0) IS
        SELECT  DISTINCT organization_pk, name
        FROM    organization, list_org lo, list, org_service os
        WHERE   upper(name) LIKE '%'|| upper(v_name) ||'%'
	AND	os.organization_fk = organization_pk
        AND     organization_pk = lo.organization_fk
        AND     list_fk = list_pk
        AND     service_fk = v_service_fk
        AND     (
                        level0 = v_list_pk OR
                        level1 = v_list_pk OR
                        level2 = v_list_pk OR
                        level3 = v_list_pk OR
                        level4 = v_list_pk OR
                        level5 = v_list_pk
                )
        AND     accepted > 0
        ORDER BY name;

        CURSOR  sel_org_6geo(v_name in organization.name%type, v_list_pk in list.list_pk%type, v_service_fk in org_service.service_fk%type default 0, v_geography_pk in geography.geography_pk%type) IS
        SELECT  DISTINCT organization_pk, name
        FROM    organization, list_org lo, list l, org_service os, geography g
        WHERE   upper(name) LIKE '%'|| upper(v_name) ||'%'
	AND	os.organization_fk = organization_pk
        AND     organization_pk = lo.organization_fk
        AND     list_fk = list_pk
        AND     service_fk = v_service_fk
        AND     (
                        l.level0 = v_list_pk OR
                        l.level1 = v_list_pk OR
                        l.level2 = v_list_pk OR
                        l.level3 = v_list_pk OR
                        l.level4 = v_list_pk OR
                        l.level5 = v_list_pk
                )
        AND     geography_pk = geography_fk
        AND     (
                        g.level0 = v_geography_pk OR
                        g.level1 = v_geography_pk OR
                        g.level2 = v_geography_pk OR
                        g.level3 = v_geography_pk OR
                        g.level4 = v_geography_pk OR
                        g.level5 = v_geography_pk
                )
        AND     accepted > 0
        ORDER BY name;

        v_service_pk            service.service_pk%type                 DEFAULT NULL;
        v_service_pk_2          service.service_pk%type                 DEFAULT NULL;
        v_name                  strng.string%type                       DEFAULT NULL;
        v_name_sg_fk            service.name_sg_fk%type                 DEFAULT NULL;
        v_text                  varchar2(250)                           DEFAULT NULL;
        v_organization_pk       organization.organization_pk%type       default NULL;
        v_name_org              organization.name%type                  default NULL;
        v_url                   varchar2(250)                           default NULL;

BEGIN
-- Legger inn i statistikk tabellen
stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,24);

        if (p_heading = 1) then
                html.b_page;
                html.search_menu;
        ELSIF ( p_url = 'user_sub.sel_personal_list' ) THEN
                html.b_page_3(340);
        else
                html.b_page_2;
        end if;



        if (p_search_type IS NULL) then -- simple search

	        html.b_box( get.txt('select_organization'), '100%', 'select_org.list_out(simple_search)' );
	        html.b_table;
                html.b_form('select_org.list_out');
                htp.p('<input type="hidden" name="p_heading" value="'|| p_heading ||'">');
                htp.p('<input type="hidden" name="p_url" value="'|| p_url ||'">');
                htp.p('<input type="hidden" name="p_search_type" value="'|| p_search_type ||'">');
                htp.p('<input type="hidden" name="p_service_pk" value="'|| get.serv ||'">');
                htp.p('<input type="hidden" name="p_confirm_list_out" value="YES">');

                htp.p('
                        <tr><td colspan="2">'|| get.txt('simple_search_help_org') ||'</td></tr>
                        <tr><td colspan="2"><a href="select_org.list_out?p_heading='|| p_heading ||'&p_url='|| p_url ||'&p_search_type=2">'|| get.txt('advanced_search') ||'</a></td></tr>
                        <tr><td colspan="2">&nbsp;</td></tr>
                        <tr>
                                <td>'|| get.txt('specify_organization_name') ||' '|| v_text ||':</td>
                                <td><input type="text" name="p_org_name" value="'|| p_org_name ||'" size="20" maxlength="150"></td>
                        </tr>
                        <tr><td colspan="2" align="right">
                ');
                        html.submit_link( get.txt('list_out_simple') );
                htp.p('</td></tr>');

        elsif (p_search_type = 2) then -- advanced search

	        html.b_box( get.txt('select_organization'), '100%', 'select_org.list_out' );
	        html.b_table;
                -- henter ut tjenesten man er p�
                if (p_service_pk IS NULL) then
                        v_service_pk := get.serv;
                else
                        v_service_pk := p_service_pk;
                end if;

                -- hjelpetekst
                htp.p('<tr><td colspan="2">'|| get.txt('advanced_search_help_org') ||'</td></tr>');
                htp.p('<tr><td colspan="2"><a href="select_org.list_out?p_heading='|| p_heading ||'&p_url='|| p_url ||'">'|| get.txt('simple_search') ||'</a></td></tr><tr><td colspan="2">&nbsp;</td></tr>');
                -- skriver ut kode for � velge hvilken tjeneste man skal hente data fra
                htp.p('<tr><td valign="top">'|| get.txt('service_to_select_organizations_from') ||':</td><td>');

                html.b_select_jump;
                htp.p('<option value="select_org.list_out?p_confirm_list_out=NO&p_service_pk=0&p_url='||p_url||'&p_search_type=2&p_heading='||p_heading||'">'|| get.txt('all_services') ||'</option>');
                open all_services;
                loop
                        fetch all_services into v_service_pk_2, v_name_sg_fk;
                        exit when all_services%NOTFOUND;
                        if (v_service_pk_2 = v_service_pk) then
                                htp.p('<option value="select_org.list_out?p_confirm_list_out=NO&p_service_pk='|| v_service_pk_2 ||'&p_url='||p_url||'&p_search_type=2&p_heading='||p_heading||'" selected>'|| get.text(v_name_sg_fk) ||'</option>');
                        else
                                htp.p('<option value="select_org.list_out?p_confirm_list_out=NO&p_service_pk='|| v_service_pk_2 ||'&p_url='||p_url||'&p_search_type=2&p_heading='||p_heading||'">'|| get.text(v_name_sg_fk) ||'</option>');
                        end if;
                end loop;
                close all_services;
                html.e_select_jump;

                htp.p('</td></tr>');

                html.b_form('select_org.list_out');
                htp.p('<input type="hidden" name="p_heading" value="'|| p_heading ||'">');
                htp.p('<input type="hidden" name="p_url" value="'|| p_url ||'">');
                htp.p('<input type="hidden" name="p_search_type" value="'|| p_search_type ||'">');
                htp.p('<input type="hidden" name="p_service_pk" value="'|| v_service_pk ||'">');
                htp.p('<input type="hidden" name="p_confirm_list_out" value="YES">');


                -- henter tekst som skal skrives ut (forandres utifra valgt tjeneste)
                if (v_service_pk IS NOT NULL) then
                        SELECT  name_sg_fk INTO v_name_sg_fk
                        FROM    service
                        WHERE   service_pk = v_service_pk;

                        if (v_service_pk = 0) then -- alle tjenester
                                v_text := get.txt('on') ||' '|| get.text(v_name_sg_fk) ;
                        else
                                v_text := get.txt('on') ||' '|| get.text(v_name_sg_fk) ||' '|| get.txt('the_service');
                        end if;
                else
                        v_text := '';
                end if;

                -- spesifiser liste � hente ut data fra
                htp.p('<tr><td>'|| get.txt('specify_list_to_select_from') ||' '||v_text||':</td><td><input type="hidden" name="p_list_pk" value="'|| p_list_pk ||'"><input type="Text" name="p_name" size="30" maxlength="250" value="'||p_name||'" onSelect=javascript:openWin(''select_list.sel_type?p_service_fk='|| v_service_pk ||''',300,200) onClick=javascript:openWin(''select_list.sel_type?p_service_fk='|| v_service_pk ||''',300,200)>'|| html.popup( get.txt('select_list'), 'select_list.sel_type?p_service_fk='|| v_service_pk ||'', '300', '200') ||'</td></tr>');
                htp.p('</td></tr>');

                -- spesifiser geografi � hente ut data fra
                htp.p('<tr><td>'|| get.txt('specify_geo_to_select_from') ||':</td><td nowrap><input type="hidden" name="p_geography_pk" value="'|| p_geography_pk ||'"><input type="Text" name="p_geo_name" size="30" maxlength="250" value="'||p_geo_name||'" onSelect=javascript:openWin(''select_geo.select_location?p_no_levels=4'',300,200) onClick=javascript:openWin(''select_geo.select_location?p_no_levels=4'',300,200)>'|| html.popup( get.txt('select_geo'), 'select_geo.select_location?p_no_levels=4', '300', '200') ||'</td></tr>');
                htp.p('</td></tr>');

                -- spesifiser organisasjonsnavn
                htp.p('<tr><td>'|| get.txt('specify_organization_name') ||' '|| v_text ||':</td><td><input type="text" name="p_org_name" value="'|| p_org_name ||'" size="30" maxlength="150"></td></tr>');
                htp.p('</td></tr>');

                -- "clear" og "submit" form
                htp.p('<tr><td colspan="2" align="right">');
                htp.p('
                        <SCRIPT LANGUAGE="JavaScript"><!--
                        function ClearForm() {
                                document.form.p_name.value='''';
                                document.form.p_list_pk.value='''';
                                document.form.p_org_name.value='''';
                                document.form.p_geography_pk.value='''';
                                document.form.p_geo_name.value='''';
                        }
                        //-->
                        </SCRIPT>
                        <input type="button" value="'||get.txt('clear')||'" onClick="javascript:ClearForm()">
                ');
                htp.p('</td></tr>');
                htp.p('<tr><td colspan="2" align="right">');
                html.submit_link( get.txt('list_out') );
                htp.p('</td></tr>');
        end if;

        html.e_form;
        html.e_table;


        -- henter (eventuelt) ut selskaper, og skriver ut p� skjerm
        if (p_confirm_list_out = 'YES') then

                html.b_table;
                htp.p('<tr><td><b>'||get.txt('organization_name')||':</b></td><td><b>'||get.txt('service')||':</b></td></tr>');

                -- ----------------------
                -- s�k i alle tjenester
                -- ----------------------
                -- kriterier: ingen
                if ( (p_service_pk = 0) AND (p_list_pk IS NULL) AND (p_org_name IS NULL) AND (p_geography_pk IS NULL) ) then
                        open sel_org_1;
                        while (1>0)     -- alltid sant -> g�r igjennom alle forekomstene
                        loop
                                fetch sel_org_1 into v_organization_pk;
                                if (sel_org_1%NOTFOUND) then
                                        htp.p(REPLACE(REPLACE(get.txt('search_result'), '[nr]', sel_org_1%ROWCOUNT), '[type]', get.txt('organizations')) );
                                end if;
                                exit when sel_org_1%NOTFOUND;

                                SELECT  name_sg_fk INTO v_name_sg_fk
                                FROM    service, org_service
                                WHERE   service_pk = service_fk
                                AND     organization_fk = v_organization_pk;

                                htp.p('<tr><td><a href="'|| get.olink(v_organization_pk, p_url) ||'?p_organization_pk='||v_organization_pk||'">'||get.oname(v_organization_pk)||'</a></td><td>'||get.text(v_name_sg_fk)||'</td></tr>');
                        end loop;
                        close sel_org_1;
                -- kriterier: geo
                elsif ( (p_service_pk = 0) AND (p_list_pk IS NULL) AND (p_org_name IS NULL) AND (p_geography_pk IS NOT NULL) ) then
                        open sel_org_1geo(p_geography_pk);
                        loop
                                fetch sel_org_1geo into v_organization_pk;
                                if (sel_org_1geo%NOTFOUND) then
                                        htp.p(REPLACE(REPLACE(get.txt('search_result'), '[nr]', sel_org_1geo%ROWCOUNT), '[type]', get.txt('organizations') ));
                                end if;
                                exit when sel_org_1geo%NOTFOUND;

                                SELECT  name_sg_fk INTO v_name_sg_fk
                                FROM    service, org_service
                                WHERE   service_pk = service_fk
                                AND     organization_fk = v_organization_pk;

                                htp.p('<tr><td><a href="'|| get.olink(v_organization_pk, p_url) ||'?p_organization_pk='||v_organization_pk||'">'||get.oname(v_organization_pk)||'</a></td><td>'||get.text(v_name_sg_fk)||'</td></tr>');
                        end loop;
                        close sel_org_1geo;
                -- kriterier: org.name
                elsif ( (p_service_pk = 0) AND (p_list_pk IS NULL) AND (p_org_name IS NOT NULL) AND (p_geography_pk IS NULL) ) then
                        open sel_org_2( p_org_name );
                        while (1>0)     -- alltid sant -> g�r igjennom alle forekomstene
                        loop
                                fetch sel_org_2 into v_organization_pk, v_name_org;
                                if (sel_org_2%NOTFOUND) then
                                        htp.p(REPLACE(REPLACE(get.txt('search_result'), '[nr]', sel_org_2%ROWCOUNT), '[type]', get.txt('organizations') ));
                                end if;
                                exit when sel_org_2%NOTFOUND;

                                SELECT  name_sg_fk INTO v_name_sg_fk
                                FROM    service, org_service
                                WHERE   service_pk = service_fk
                                AND     organization_fk = v_organization_pk;

                                htp.p('<tr><td><a href="'|| get.olink(v_organization_pk, p_url) ||'?p_organization_pk='||v_organization_pk||'">'||get.oname(v_organization_pk)||'</a></td><td>'||get.text(v_name_sg_fk)||'</td></tr>');
                        end loop;
                        close sel_org_2;
                -- kriterier: org.name + geo
                elsif ( (p_service_pk = 0) AND (p_list_pk IS NULL) AND (p_org_name IS NOT NULL) AND (p_geography_pk IS NOT NULL) ) then
                        open sel_org_2geo( p_org_name, p_geography_pk );
                        while (1>0)     -- alltid sant -> g�r igjennom alle forekomstene
                        loop
                                fetch sel_org_2geo into v_organization_pk, v_name_org;
                                if (sel_org_2geo%NOTFOUND) then
                                        htp.p(REPLACE(REPLACE(get.txt('search_result'), '[nr]', sel_org_2geo%ROWCOUNT), '[type]', get.txt('organizations') ));
                                end if;
                                exit when sel_org_2geo%NOTFOUND;

                                SELECT  name_sg_fk INTO v_name_sg_fk
                                FROM    service, org_service
                                WHERE   service_pk = service_fk
                                AND     organization_fk = v_organization_pk;

                                htp.p('<tr><td><a href="'|| get.olink(v_organization_pk, p_url) ||'?p_organization_pk='||v_organization_pk||'">'||get.oname(v_organization_pk)||'</a></td><td>'||get.text(v_name_sg_fk)||'</td></tr>');
                        end loop;
                        close sel_org_2geo;
                -- kriterier: liste + eventuelt (org.name)
                elsif ( (p_service_pk = 0) AND (p_list_pk IS NOT NULL) AND (p_geography_pk IS NULL) ) then
                        open sel_org_3(p_org_name, p_list_pk);
                        while (1>0)     -- alltid sant -> g�r igjennom alle forekomstene
                        loop
                                fetch sel_org_3 into v_organization_pk, v_name_org;
                                if (sel_org_3%NOTFOUND) then
                                        htp.p(REPLACE(REPLACE(get.txt('search_result'), '[nr]', sel_org_3%ROWCOUNT), '[type]', get.txt('organizations') ));
                                end if;
                                exit when sel_org_3%NOTFOUND;

                                SELECT  name_sg_fk INTO v_name_sg_fk
                                FROM    service, org_service
                                WHERE   service_pk = service_fk
                                AND     organization_fk = v_organization_pk;

                                htp.p('<tr><td><a href="'|| get.olink(v_organization_pk, p_url) ||'?p_organization_pk='||v_organization_pk||'">'||get.oname(v_organization_pk)||'</a></td><td>'||get.text(v_name_sg_fk)||'</td></tr>');
                        end loop;
                        close sel_org_3;
                -- kriterier: liste + eventuelt (org.name) + geo
                elsif ( (p_service_pk = 0) AND (p_list_pk IS NOT NULL) AND (p_geography_pk IS NOT NULL) ) then
                        open sel_org_3geo(p_org_name, p_list_pk, p_geography_pk);
                        while (1>0)     -- alltid sant -> g�r igjennom alle forekomstene
                        loop
                                fetch sel_org_3geo into v_organization_pk, v_name_org;
                                if (sel_org_3geo%NOTFOUND) then
                                        htp.p(REPLACE(REPLACE(get.txt('search_result'), '[nr]', sel_org_3geo%ROWCOUNT), '[type]', get.txt('organizations') ));
                                end if;
                                exit when sel_org_3geo%NOTFOUND;

                                SELECT  name_sg_fk INTO v_name_sg_fk
                                FROM    service, org_service
                                WHERE   service_pk = service_fk
                                AND     organization_fk = v_organization_pk;

                                htp.p('<tr><td><a href="'|| get.olink(v_organization_pk, p_url) ||'?p_organization_pk='||v_organization_pk||'">'||get.oname(v_organization_pk)||'</a></td><td>'||get.text(v_name_sg_fk)||'</td></tr>');
                        end loop;
                        close sel_org_3geo;
                -- ---------------------------------------------
                -- tjenesten er forskjellig fra "alle tjenester"
                -- ---------------------------------------------
                -- kriterier: tjeneste
                elsif ( (p_service_pk <> 0) AND (p_list_pk IS NULL) AND (p_org_name IS NULL) AND (p_geography_pk IS NULL) ) then
                        open sel_org_4(p_service_pk);
                        while (1>0)     -- alltid sant -> g�r igjennom alle forekomstene
                        loop
                                fetch sel_org_4 into v_organization_pk;
                                if (sel_org_4%NOTFOUND) then
                                        htp.p(REPLACE(REPLACE(get.txt('search_result'), '[nr]', sel_org_4%ROWCOUNT), '[type]', get.txt('organizations') ));
                                end if;
                                exit when sel_org_4%NOTFOUND;

                                SELECT  name_sg_fk INTO v_name_sg_fk
                                FROM    service, org_service
                                WHERE   service_pk = service_fk
                                AND     organization_fk = v_organization_pk;

                                htp.p('<tr><td><a href="'|| get.olink(v_organization_pk, p_url) ||'?p_organization_pk='||v_organization_pk||'">'||get.oname(v_organization_pk)||'</a></td><td>'||get.text(v_name_sg_fk)||'</td></tr>');
                        end loop;
                        close sel_org_4;
                -- kriterier: tjeneste + geo
                elsif ( (p_service_pk <> 0) AND (p_list_pk IS NULL) AND (p_org_name IS NULL) AND (p_geography_pk IS NOT NULL) ) then
                        open sel_org_4geo(p_service_pk, p_geography_pk);
                        while (1>0)     -- alltid sant -> g�r igjennom alle forekomstene
                        loop
                                fetch sel_org_4geo into v_organization_pk;
                                if (sel_org_4geo%NOTFOUND) then
                                        htp.p(REPLACE(REPLACE(get.txt('search_result'), '[nr]', sel_org_4geo%ROWCOUNT), '[type]', get.txt('organizations') ));
                                end if;
                                exit when sel_org_4geo%NOTFOUND;

                                SELECT  name_sg_fk INTO v_name_sg_fk
                                FROM    service, org_service
                                WHERE   service_pk = service_fk
                                AND     organization_fk = v_organization_pk;

                                htp.p('<tr><td><a href="'|| get.olink(v_organization_pk, p_url) ||'?p_organization_pk='||v_organization_pk||'">'||get.oname(v_organization_pk)||'</a></td><td>'||get.text(v_name_sg_fk)||'</td></tr>');
                        end loop;
                        close sel_org_4geo;
                -- kriterier: tjeneste + org.name
                elsif ( (p_service_pk <> 0) AND (p_list_pk IS NULL) AND (p_org_name IS NOT NULL) AND (p_geography_pk IS NULL) ) then
                        open sel_org_5(p_org_name, p_service_pk);
                        while (1>0)     -- alltid sant -> g�r igjennom alle forekomstene
                        loop
                                fetch sel_org_5 into v_organization_pk, v_name_org;
                                if (sel_org_5%NOTFOUND) then
                                        htp.p(REPLACE(REPLACE(get.txt('search_result'), '[nr]', sel_org_5%ROWCOUNT), '[type]', get.txt('organizations') ));
                                end if;
                                exit when sel_org_5%NOTFOUND;

                                SELECT  name_sg_fk INTO v_name_sg_fk
                                FROM    service, org_service
                                WHERE   service_pk = service_fk
                                AND     organization_fk = v_organization_pk;

                                htp.p('<tr><td><a href="'|| get.olink(v_organization_pk, p_url) ||'?p_organization_pk='||v_organization_pk||'">'||get.oname(v_organization_pk)||'</a></td><td>'||get.text(v_name_sg_fk)||'</td></tr>');
                        end loop;
                        close sel_org_5;
                -- kriterier: tjeneste + org.name + geo
                elsif ( (p_service_pk <> 0) AND (p_list_pk IS NULL) AND (p_org_name IS NOT NULL) AND (p_geography_pk IS NOT NULL) ) then
                        open sel_org_5geo(p_org_name, p_service_pk, p_geography_pk);
                        while (1>0)     -- alltid sant -> g�r igjennom alle forekomstene
                        loop
                                fetch sel_org_5geo into v_organization_pk, v_name_org;
                                if (sel_org_5geo%NOTFOUND) then
                                        htp.p(REPLACE(REPLACE(get.txt('search_result'), '[nr]', sel_org_5geo%ROWCOUNT), '[type]', get.txt('organizations') ));
                                end if;
                                exit when sel_org_5geo%NOTFOUND;

                                SELECT  name_sg_fk INTO v_name_sg_fk
                                FROM    service, org_service
                                WHERE   service_pk = service_fk
                                AND     organization_fk = v_organization_pk;

                                htp.p('<tr><td><a href="'|| get.olink(v_organization_pk, p_url) ||'?p_organization_pk='||v_organization_pk||'">'||get.oname(v_organization_pk)||'</a></td><td>'||get.text(v_name_sg_fk)||'</td></tr>');
                        end loop;
                        close sel_org_5geo;
                -- kriterier: tjeneste + liste + (eventuelt org.name)
                elsif ( (p_service_pk <> 0) AND (p_list_pk IS NOT NULL) AND (p_geography_pk IS NULL) ) then
                        open sel_org_6(p_org_name, p_list_pk, p_service_pk);
                        while (1>0)     -- alltid sant -> g�r igjennom alle forekomstene
                        loop
                                fetch sel_org_6 into v_organization_pk, v_name_org;
                                if (sel_org_6%NOTFOUND) then
                                        htp.p(REPLACE(REPLACE(get.txt('search_result'), '[nr]', sel_org_6%ROWCOUNT), '[type]', get.txt('organizations') ));
                                end if;
                                exit when sel_org_6%NOTFOUND;

                                SELECT  name_sg_fk INTO v_name_sg_fk
                                FROM    service, org_service
                                WHERE   service_pk = service_fk
                                AND     organization_fk = v_organization_pk;

                                htp.p('<tr><td><a href="'|| get.olink(v_organization_pk, p_url) ||'?p_organization_pk='||v_organization_pk||'">'||get.oname(v_organization_pk)||'</a></td><td>'||get.text(v_name_sg_fk)||'</td></tr>');
                        end loop;
                        close sel_org_6;
                -- kriterier: tjeneste + liste + (eventuelt org.name) + geo
                elsif ( (p_service_pk <> 0) AND (p_list_pk IS NOT NULL) AND (p_geography_pk IS NOT NULL) ) then
                        open sel_org_6geo(p_org_name, p_list_pk, p_service_pk, p_geography_pk);
                        while (1>0)     -- alltid sant -> g�r igjennom alle forekomstene
                        loop
                                fetch sel_org_6geo into v_organization_pk, v_name_org;
                                if (sel_org_6geo%NOTFOUND) then
                                        htp.p(REPLACE(REPLACE(get.txt('search_result'), '[nr]', sel_org_6geo%ROWCOUNT), '[type]', get.txt('organizations') ));
                                end if;
                                exit when sel_org_6geo%NOTFOUND;

                                SELECT  name_sg_fk INTO v_name_sg_fk
                                FROM    service, org_service
                                WHERE   service_pk = service_fk
                                AND     organization_fk = v_organization_pk;

                                htp.p('<tr><td><a href="'|| get.olink(v_organization_pk, p_url) ||'?p_organization_pk='||v_organization_pk||'">'||get.oname(v_organization_pk)||'</a></td><td>'||get.text(v_name_sg_fk)||'</td></tr>');
                        end loop;
                        close sel_org_6geo;
                end if;

                html.e_table;


        end if;


        html.e_box;


        if (p_heading = 1) then
                html.e_page;
        else
                html.e_page_2;
        end if;



END;
END list_out;





-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/
show errors;
