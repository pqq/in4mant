CREATE OR REPLACE PACKAGE org_list IS

	PROCEDURE startup;
	PROCEDURE show_lists
		(
			p_organization_pk	in organization.organization_pk%type	default NULL
		);
	PROCEDURE add_to_list
		(
			p_organization_pk	in list_org.organization_fk%type	default NULL,
			p_list_pk		in list.list_pk%type			default NULL,
			p_name			in varchar2				default NULL
		);
	PROCEDURE rem_from_list
		(
			p_organization_pk	in list_org.organization_fk%type	default NULL,
			p_list_pk		in list.list_pk%type			default NULL
		);

END;
/
CREATE OR REPLACE PACKAGE BODY org_list IS

---------------------------------------------------------
-- Name: 	startup
-- Type: 	procedure
-- What: 	start prosedyren, for � kj�re pakken
-- Author:  	Frode Klevstul
-- Start date: 	24.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

if ( login.timeout('org_list.startup')>0 AND login.right('org_list')>0 ) then

	select_org.list_out(NULL, NULL, NULL, NULL, 'NO', 'org_list.show_lists');

end if;

END startup;



---------------------------------------------------------
-- Name: 	show_lists
-- Type: 	procedure
-- What: 	viser hvilke lister selskapet er med p�
-- Author:  	Frode Klevstul
-- Start date: 	24.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE show_lists
	(
		p_organization_pk	in organization.organization_pk%type	default NULL
	)
IS
BEGIN
DECLARE

	CURSOR 	select_all IS
	SELECT 	l.name_sg_fk, l.list_pk
	FROM	list_org lo, list l, list_type lt
	WHERE	lo.list_fk = list_pk
	AND	organization_fk = p_organization_pk
	AND	lt.list_type_pk = l.list_type_fk
	AND NOT	lt.list_type_pk < 0
	AND NOT	lt.description LIKE 'my_personal_list%';

	v_name_sg_fk 	list.name_sg_fk%type		default NULL;
	v_list_pk	list.list_pk%type		default NULL;

BEGIN

if ( login.timeout('org_list.startup')>0 AND login.right('org_list')>0 ) then

	html.b_adm_page('list_adm');
	html.b_box( 'lists for '|| get.oname(p_organization_pk) ||':');
	html.b_table;


	open select_all;
	loop
		fetch select_all into v_name_sg_fk, v_list_pk;
		exit when select_all%NOTFOUND;
		htp.p('<tr><td>'|| get.text(v_name_sg_fk) ||'</td><td>'); html.button_link('remove from list','org_list.rem_from_list?p_organization_pk='||p_organization_pk||'&p_list_pk='||v_list_pk); htp.p('</td></tr>');
	end loop;
	close select_all;

	html.e_table;


	html.b_form('org_list.add_to_list');
	html.b_table('60%');
	htp.p('<input type="hidden" name="p_organization_pk" value="'||p_organization_pk||'">');
	htp.p('<tr><td>add organization to list:</td><td><input type="hidden" name="p_list_pk" value=""><input type="Text" name="p_name" size="30" maxlength="250" value="" onSelect=javascript:openWin(''select_list.sel_type'',300,200) onClick=javascript:openWin(''select_list.sel_type'',300,200)>'|| html.popup( get.txt('select_list'), 'select_list.sel_type', '300', '200') ||'</td></tr>');
	htp.p('<tr><td colspan="2" align="right">');  html.submit_link('add to list'); htp.p('</td></tr>');
	html.e_table;
	html.e_form;

	html.e_box;
	html.e_page;

end if;

END;
END show_lists;




---------------------------------------------------------
-- Name: 	add_to_list
-- Type: 	procedure
-- What: 	legger et selskap til en liste
-- Author:  	Frode Klevstul
-- Start date: 	24.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE add_to_list
	(
		p_organization_pk	in list_org.organization_fk%type	default NULL,
		p_list_pk		in list.list_pk%type			default NULL,
		p_name			in varchar2				default NULL
	)
IS
BEGIN
DECLARE
	v_check		number		default NULL;
BEGIN

if ( login.timeout('org_list.startup')>0 AND login.right('org_list')>0 ) then

	SELECT 	count(*) into v_check
	FROM	list_org
	WHERE	organization_fk = p_organization_pk
	AND	list_fk = p_list_pk;

	if (v_check = 0) then
		INSERT INTO list_org
		(organization_fk, list_fk)
		VALUES (p_organization_pk, p_list_pk);
	end if;

	org_list.show_lists(p_organization_pk);

end if;

END;
END add_to_list;




---------------------------------------------------------
-- Name: 	rem_from_list
-- Type: 	procedure
-- What: 	fjerner et selskap fra en liste
-- Author:  	Frode Klevstul
-- Start date: 	24.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE rem_from_list
	(
		p_organization_pk	in list_org.organization_fk%type	default NULL,
		p_list_pk		in list.list_pk%type			default NULL
	)
IS
BEGIN

if ( login.timeout('org_list.startup')>0 AND login.right('org_list')>0 ) then

	DELETE FROM list_org
	WHERE organization_fk = p_organization_pk
	AND list_fk = p_list_pk;

	org_list.show_lists(p_organization_pk);

end if;

END rem_from_list;




-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/
