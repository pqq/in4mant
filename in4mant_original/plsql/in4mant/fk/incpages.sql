set define off
PROMPT *** package: INCPAGES ***

CREATE OR REPLACE PACKAGE incpages IS

	PROCEDURE update_album(
		p_organization_pk	IN organization.organization_pk%TYPE	DEFAULT NULL
	);
	PROCEDURE delete_calendar(
		p_organization_pk	IN organization.organization_pk%TYPE	DEFAULT NULL
	);

END;
/
show errors;



CREATE OR REPLACE PACKAGE BODY incpages IS



---------------------------------------------------------
-- Name:        incpages
-- Type:        procedure
-- What:        a procedure updates all included albums
-- Author:      Frode Klevstul
-- Start date:  13.06.2002
-- Desc:		sender kun ut for language_pk = 1 (norsk)
---------------------------------------------------------
PROCEDURE update_album
    (
		p_organization_pk	IN organization.organization_pk%TYPE	DEFAULT NULL
    )
IS
BEGIN
DECLARE

	v_organization_pk	organization.organization_pk%type		default NULL;

	CURSOR  select_all
		(
			v_organization_pk	in organization.organization_pk%type
		) IS
	SELECT	album_pk
	FROM	album
	WHERE	organization_fk = v_organization_pk
	ORDER BY create_date desc;

	v_album_pk			album.album_pk%type					default NULL;
	v_email_dir			varchar2(100)						default get.value('email_dir');
	v_email_inc			user_details.email%type				default NULL;
	v_number			number								default NULL;
	v_file				utl_file.file_type					default NULL;

BEGIN

	v_email_inc			:= 'inc@in4mant.com';

	if (p_organization_pk IS NULL) then
		v_organization_pk := get.oid;
	else
		v_organization_pk := p_organization_pk;
	end if;


	open select_all(v_organization_pk);
	loop
		fetch select_all into v_album_pk;
		exit when select_all%NOTFOUND;

		-- -----------------------------
		-- henter ut unikt email nummer
		-- -----------------------------
		SELECT	email_script_seq.NEXTVAL
		INTO	v_number
		FROM	dual;

		-- -------------------------------------------------------------------------
		-- Sender ut mail for generering av integrerte sider...
		-- -------------------------------------------------------------------------
        -- -------------------------------------------------------------------------
        -- skriver mottager fil ********* START **********
        -- -------------------------------------------------------------------------
        v_file := utl_file.fopen( v_email_dir, v_number||'_email-list.txt', 'a');
		utl_file.put_line(v_file, v_email_inc);
        utl_file.fclose(v_file);
        -- -------------------------------------------------------------------------
        -- /slutt p� � skrive mottager fil ********* END **********
        -- -------------------------------------------------------------------------

        -- ------------------------------------------------
        -- skriver selve mailen (innholdet) *** START ***
        -- ------------------------------------------------
        v_file := utl_file.fopen( v_email_dir, v_number||'_message.txt' ,'a');
        utl_file.put_line(v_file, 'From: in4mant.com<info@in4mant.com>
Subject: Update inc pages!

');
	utl_file.put_line(v_file, 'ORGANIZATION_PK::'||v_organization_pk);
	utl_file.put_line(v_file, 'LANGUAGE_PK::1');
	utl_file.put_line(v_file, 'ALBUM_PK::'||v_album_pk);
        utl_file.fclose(v_file);
        -- ---------------------------------------------------------
        -- /slutt p� � skrive innholdet av mailen *** SLUTT ***
        -- ---------------------------------------------------------
		-- -------------------------------------------------------------------------
		-- /slutt p� integrerte sider mailutsendelse
		-- -------------------------------------------------------------------------

		htp.p('Update email for album '''||v_album_pk||''' is generated<br>');

	end loop;
	close select_all;


END;
END update_album;

---------------------------------------------------------
-- Name:        delete old calendar pages 
-- Type:        procedure
-- Author:      Espen Messel
-- Start date:  03.09.2002
---------------------------------------------------------
PROCEDURE delete_calendar
    (
		p_organization_pk	IN organization.organization_pk%TYPE	DEFAULT NULL
    )
IS
BEGIN
DECLARE

	v_organization_pk	organization.organization_pk%type		default NULL;

	CURSOR  select_all
		(
			v_organization_pk	in organization.organization_pk%type
		) IS
	SELECT	organization_fk ||'/cal_'||language_fk||'_'||calendar_pk||'.html' as filename
	FROM	calendar
	WHERE	organization_fk = v_organization_pk
	AND	event_date < sysdate
	and	( end_date < sysdate or end_date is null );
 

	v_filename	varchar2(100);
	v_email_dir	varchar2(100)			default get.value('email_dir');
	v_email_inc	user_details.email%type		default 'inc@in4mant.com';
	v_number	number				default NULL;
	v_file		utl_file.file_type		default NULL;

BEGIN
if (login.timeout('in4admin.startup') > 0 AND login.right('in4admin.main_page')>0 ) then
   v_organization_pk := p_organization_pk;
   if ( v_organization_pk is not null ) then
	
	open select_all(v_organization_pk);
	loop
		fetch select_all into v_filename;
		exit when select_all%NOTFOUND;

		-- -----------------------------
		-- henter ut unikt email nummer
		-- -----------------------------
		SELECT	email_script_seq.NEXTVAL
		INTO	v_number
		FROM	dual;

		-- -------------------------------------------------------------------------
		-- Sender ut mail for generering av integrerte sider...
		-- -------------------------------------------------------------------------
		-- -------------------------------------------------------------------------
		-- skriver mottager fil ********* START **********
		-- -------------------------------------------------------------------------
		v_file := utl_file.fopen( v_email_dir, v_number||'_email-list.txt', 'a');
		utl_file.put_line(v_file, v_email_inc);
		utl_file.fclose(v_file);
		-- -------------------------------------------------------------------------
		-- /slutt p� � skrive mottager fil ********* END **********
		-- -------------------------------------------------------------------------

		-- ------------------------------------------------
		-- skriver selve mailen (innholdet) *** START ***
		-- ------------------------------------------------
		v_file := utl_file.fopen( v_email_dir, v_number||'_message.txt' ,'a');
		utl_file.put_line(v_file, 'From: in4mant.com<info@in4mant.com>
Subject: Update inc pages!

');
		utl_file.put_line(v_file, 'DELETE_FILE::'||v_filename);
		utl_file.fclose(v_file);
		-- ---------------------------------------------------------
		-- /slutt p� � skrive innholdet av mailen *** SLUTT ***
		-- ---------------------------------------------------------
		-- -------------------------------------------------------------------------
		-- /slutt p� integrerte sider mailutsendelse
		-- -------------------------------------------------------------------------

		htp.p('Delete old calendar file /home/inc/www/'||v_filename||'<br>');

	end loop;
	close select_all;
   else
	htp.p('http://www.in4mant.com/cgi-bin/incpages.delete_calendar?p_organization_pk=NUMMER<br>');
   end if;
end if;

END;
END delete_calendar;



-- ++++++++++++++++++++++++++++++++++++++++++++++ --


END; -- slutter pakke kroppen
/
show errors;


