set define off
PROMPT *** package: htmp (HTMl Procedures) ***

CREATE OR REPLACE PACKAGE htmp IS

        PROCEDURE getcode
        	(
			p_name  in html_code.name%type default NULL
		);

END;
/
show errors;


CREATE OR REPLACE PACKAGE BODY htmp IS



---------------------------------------------------------
-- Name:        getcode
---------------------------------------------------------
PROCEDURE getcode
	(
		p_name  in html_code.name%type default NULL
        )
IS
BEGIN
	htp.p( htmf.getcode(p_name) );
END getcode;





-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/
show errors;
