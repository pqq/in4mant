set define off
PROMPT *** package: MAIN ***

CREATE OR REPLACE PACKAGE main IS

        PROCEDURE choose_lang
                (
                        p_language_pk           in la.language_pk%type          default NULL,
                        p_url                   in varchar2                     default NULL,
                        p_cookie                in boolean                      default FALSE
                );
        PROCEDURE choose_service
                (
                        p_service_pk            in la.language_pk%type          default NULL,
                        p_url                   in varchar2                     default NULL,
                        p_cookie                in boolean                      default FALSE
                );
        PROCEDURE susp_account;
        PROCEDURE service_info
                (
                        p_service_pk    in service.service_pk%type      default NULL
                );
        PROCEDURE contact_us
                (
                        p_type          in number       default NULL
                );
        PROCEDURE about
                (
                        p_type          in number       default NULL
                );
        PROCEDURE adinfo;
        PROCEDURE help
                (
                        p_type          in number       default NULL
                );
        PROCEDURE different;
        PROCEDURE advertising;
        PROCEDURE list_org
                (
                        p_from_letter   in varchar2             default NULL,
                        p_to_letter     in varchar2             default NULL,
                        p_level0        in varchar2             default NULL
                );
        PROCEDURE the_line;
        PROCEDURE org_frame;
        PROCEDURE plain
                (
                        p_bgcolor       IN VARCHAR2                     DEFAULT NULL
                );
        PROCEDURE test;
        PROCEDURE clock;

END;
/
CREATE OR REPLACE PACKAGE BODY main IS

-- **************************************************
-- "PUBLIC METODER" (KALLES DIREKTE FRA WEB)
-- **************************************************


---------------------------------------------------------
-- Name:        choose_lang
-- Type:        procedure
-- What:        let's the user change language
-- Author:      Frode Klevstul
-- Start date:  01.06.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter] [description]
---------------------------------------------------------
PROCEDURE choose_lang
        (
                p_language_pk           in la.language_pk%type          default NULL,
                p_url                   in varchar2                     default NULL,
                p_cookie                in boolean                      default FALSE
        )
IS
BEGIN
DECLARE

        v_language_pk   la.language_pk%type                     default NULL;
        v_url           varchar2(300)                           default NULL;

BEGIN

        -- dersom p_language_pk sendes med skal vi forandre spr�ket til denne verdien
        if ( p_language_pk IS NOT NULL ) then

                -- dersom det ikke finnes noe cookie blir vi sendt til EXEPTION
                -- delen i denne prosedyren. Der settes det en cookie som definerer
                -- spr�ket til default. S� kalles denne prosedyren p� nytt. Da skal
                -- ikke cookien settes igjen (sender da med p_cookie = TRUE)
                if ( NOT p_cookie ) then
                        store.lang(p_language_pk); -- setter spr�k
                end if;

                -- sender brukeren videre til url'en gitt i p_url (hvis p_url ikke er NULL)
                v_url := p_url;
                if ( v_url IS NULL ) then
                        v_url := 'main.choose_lang';    -- dersom man ikke spesifiserer hvilken side man skal til
                end if;

                -- location jump
                html.jump_to(v_url);

        else

                v_language_pk := get.lan; -- henter ut satt spr�k

                html.b_box( get.txt('choose_lang') );
                html.b_form('main.choose_lang');
                htp.p(' <input type="hidden" name="p_url" value="'||p_url||'"> ');
                html.b_table;

                htp.p('<tr><td>');
                html.select_lang(v_language_pk, get.serv);
                htp.p('</td></tr>');
                htp.p('<tr><td>');
                html.submit_link( get.txt('change') );
                htp.p('</td></tr>');

                html.e_table;
                html.e_form;
                html.e_box;

        end if;

EXCEPTION
        WHEN NO_DATA_FOUND     -- dersom cookien ikke finnes (brukeren ikke har valgt spr�k enda)
        THEN
                SELECT  language_pk INTO v_language_pk
                FROM    la
                WHERE   language_code = get.value('default_lang');
                -- default spr�k er definert i parameter tabellen

                store.lang(v_language_pk);
                -- kaller seg selv ....
                choose_lang(v_language_pk, 'main.choose_lang?p_url='||p_url||'', TRUE);

END;
END choose_lang;


---------------------------------------------------------
-- Name:        choose_service
-- Type:        procedure
-- What:        let's the user change service
-- Author:      Frode Klevstul
-- Start date:  09.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE choose_service
        (
                p_service_pk            in la.language_pk%type          default NULL,
                p_url                   in varchar2                     default NULL,
                p_cookie                in boolean                      default FALSE
        )
IS
BEGIN
DECLARE

        v_service_pk    service.service_pk%type                 default NULL;
        v_url           varchar2(300)                           default NULL;

BEGIN

        -- dersom p_service_pk sendes med skal vi forandre tjensten
        if ( p_service_pk IS NOT NULL ) then

                if ( NOT p_cookie ) then
                        store.serv(p_service_pk);
                end if;

                -- sender brukeren videre til url'en gitt i p_url (hvis p_url ikke er NULL)
                v_url := p_url;
                if ( v_url IS NULL ) then
                        v_url := 'main.choose_service'; -- dersom man ikke spesifiserer hvilken side man skal til
                end if;

                -- location jump
                html.jump_to(v_url);

        else

                v_service_pk := get.serv; -- henter ut satt tjeneste

                html.b_box( get.txt('choose_service') );
                html.b_form('main.choose_service');
                htp.p(' <input type="hidden" name="p_url" value="'||p_url||'"> ');
                html.b_table;

                htp.p('<tr><td>');
                html.select_service(v_service_pk);
                htp.p('</td></tr>');
                htp.p('<tr><td>');
                html.submit_link( get.txt('change') );
                htp.p('</td></tr>');

                html.e_table;
                html.e_form;
                html.e_box;

        end if;


EXCEPTION
        WHEN NO_DATA_FOUND     -- dersom cookien ikke finnes (brukeren ikke har valgt tjeneste enda)
        THEN
                SELECT  service_pk INTO v_service_pk
                FROM    service
                WHERE   description = get.value('default_service');
                -- default spr�k er definert i parameter tabellen

                store.serv(v_service_pk);

                -- kaller seg selv ....
                choose_service(v_service_pk, 'main.choose_service?p_url='||p_url||'', TRUE);

END;
END choose_service;



---------------------------------------------------------
-- Name:        suspended_account
-- Type:        procedure
-- What:        the page the user should come to if the account is suspended
-- Author:      Frode Klevstul
-- Start date:  09.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE susp_account
IS
BEGIN


        html.b_page;
        html.b_box( get.txt('your_account_is_closed') );

        htp.p( get.txt('open_account_desc') );

        html.e_box;
        html.e_page;


END susp_account;

---------------------------------------------------------
-- Name:        service_info
-- Type:        procedure
-- What:        writes out info about a service
-- Author:      Frode Klevstul
-- Start date:  20.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE service_info
        (
                p_service_pk    in service.service_pk%type      default NULL
        )
IS
BEGIN

        if (p_service_pk = 1) then
                html.b_box( get.txt('student.in4mant.com') );
                htp.p( get.txt('student_info') );
                html.e_box;
        elsif (p_service_pk = 5) then
                html.b_box( get.txt('studnyhet.in4mant.com') );
                htp.p( get.txt('studnyhet_info') );
                html.e_box;
        elsif (p_service_pk = 3) then
                html.b_box( get.txt('uteliv.in4mant.com') );
                htp.p( get.txt('uteliv_info') );
                html.e_box;
        elsif (p_service_pk = 6) then
                html.b_box( get.txt('admin.in4mant.com') );
                htp.p( get.txt('admin_info') );
                html.e_box;
        elsif (p_service_pk = 8) then
                html.b_box( get.txt('travel.in4mant.com') );
                htp.p( get.txt('travel_info') );
                html.e_box;
        elsif (p_service_pk = 9) then
                html.b_box( get.txt('diff.in4mant.com') );
                htp.p( get.txt('diff_info') );
                html.e_box;
        end if;

END service_info;

---------------------------------------------------------
-- Name:        contact_us
-- Type:        procedure
-- What:        contact us page
-- Author:      Frode Klevstul
-- Start date:  21.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE contact_us
        (
                p_type          in number       default NULL
        )
IS
BEGIN

        if ( p_type = 1 ) then          -- vanlig hjelpeside (inne p� en tjeneste)
                contact.us(NULL, 'empty');
        elsif ( p_type = 2 ) then       -- hjelpeside inne p� admin.in4mant.com
                contact.us(NULL, 'adm', get.email, get.uname);
        elsif ( p_type = 3 ) then       -- hjelpeside inne p� www.in4mat.com
                contact.us(NULL, 'www');
        ELSE
                contact.us(NULL, 'drink');
        end if;

END contact_us;



---------------------------------------------------------
-- Name:        about
-- Type:        procedure
-- What:        about page
-- Author:      Frode Klevstul
-- Start date:  21.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE about
        (
                p_type          in number       default NULL
        )
IS
BEGIN

-- Legger inn i statistikk tabellen
     stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,27);

        if ( p_type = 1 ) then          -- vanlig hjelpeside (fra en tjeneste)
                html.b_page;
                html.empty_menu;
                html.b_box( get.txt('about_header') );
                htp.p( get.txt('about_info_1') ||' '|| get.txt('about_info_2') ||' '|| get.txt('about_info_3') );
                html.e_box;
                html.e_page;
        elsif ( p_type = 2 ) then       -- hjelpeside fra admin.in4mant.com
                html.b_page(NULL, NULL, NULL, 2);
                html.b_box( get.txt('about_header') );
                htp.p( get.txt('about_info_1') ||' '|| get.txt('about_info_2') ||' '|| get.txt('about_info_3') );
                html.e_box;
                html.e_page;
        elsif ( p_type = 3 ) then       -- hjelpeside fra www.in4mat.com
                html.b_page(NULL, NULL, NULL, 3);
                html.b_box( get.txt('about_header') );
                htp.p( get.txt('about_info_1') ||' '|| get.txt('about_info_2') ||' '|| get.txt('about_info_3') );
                html.e_box;
                html.e_page;
        end if;


END about;

---------------------------------------------------------
-- Name:        adinfo
-- Type:        procedure
-- What:        Ad info page
-- Author:      Espen Messel
-- Start date:  17.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE adinfo
IS
BEGIN
DECLARE
        v_referer       varchar2(100)   default NULL;

BEGIN

        v_referer := get.serv_url;

        IF ( owa_pattern.match( v_referer, 'www.in4mant.com', 'i') or owa_pattern.match( v_referer, '^in4mant.com', 'i')) THEN
                html.b_page(NULL, NULL, NULL, 3);
                html.b_box( get.txt('adinfo_header') );
                htp.p( get.txt('adinfo_info_1') ||' '||get.txt('adinfo_info_2') );
                html.e_box;
                html.e_page;
        elsif ( owa_pattern.match( v_referer, 'admin.in4mant.com', 'i') ) then
                html.b_page(NULL, NULL, NULL, 2);
                html.b_box( get.txt('adinfo_header') );
                htp.p( get.txt('adinfo_info_1') ||' '||get.txt('adinfo_info_2') );
                html.e_box;
                html.e_page;
        else
                html.b_page;
                html.empty_menu;
                html.b_box( get.txt('adinfo_header') );
                htp.p( get.txt('adinfo_info_1') ||' '||get.txt('adinfo_info_2') );
                html.e_box;
                html.e_page;
        end if;
END;
END adinfo;

---------------------------------------------------------
-- Name:        help
-- Type:        procedure
-- What:        help page
-- Author:      Frode Klevstul
-- Start date:  21.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE help
        (
                p_type          in number       default NULL
        )
IS
BEGIN

        -- Legger inn i statistikk tabellen
        stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,26);

        if ( p_type = 1 ) then -- vanlig hjelpeside (fra en tjeneste)
               html.b_page;
               html.empty_menu;
               html.b_box( get.txt('help_header') );
               htp.p( get.txt('help_info_1') ||' '||get.txt('help_info_2') );
               html.e_box;
               html.e_page;
        elsif ( p_type = 2 ) then -- hjelpeside fra admin.in4mant.com
                html.b_page(NULL, NULL, NULL, 2);
                html.b_box( get.txt('admin_help_header') );
                htp.p( get.txt('admin_help_info_1') ||' '||get.txt('admin_help_info_2') ||' '||get.txt('admin_help_info_3')  ||' '||get.txt('admin_help_info_4'));
                html.e_box;
                html.e_page;
        elsif ( p_type = 3 ) then -- hjelpeside fra www.in4mat.com
               html.b_page(NULL, NULL, NULL, 3);
               html.b_box( get.txt('help_header') );
               htp.p( get.txt('help_info_1') ||' '||get.txt('help_info_2') ||' '||get.txt('help_info_3') );
               html.e_box;
               html.e_page;
        end if;

END help;


---------------------------------------------------------
-- Name:        different
-- Type:        procedure
-- What:        links
-- Author:      Frode Klevstul
-- Start date:  21.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE different
IS
BEGIN
DECLARE
        v_no_items      number          default NULL;
BEGIN

        SELECT  count(*)
        INTO    v_no_items
        FROM    item
        WHERE   sysdate between publish_date and end_date;

    html.b_box_2( get.txt('other') , '100%', 'main.different');
        htp.p('<b>'|| get.txt('different') ||':</b><br>
        <a href="bboard.startup">'|| get.txt('bulletin_board') ||' ('||v_no_items||' '||get.txt('items')||') </a><br>
          ');
/*                <br><b>'|| get.txt('games') ||':</b><br>
                <a href="games.reflex">'|| get.txt('the_reflex_tester') ||'</a><br>
                <a href="games.thrower">'|| get.txt('thrower_game') ||'</a><br>
        ');
*/
    html.e_box_2;

END;
END different;


---------------------------------------------------------
-- Name:        advertising
-- Type:        procedure
-- What:        advertising banners
-- Author:      Frode Klevstul
-- Start date:  21.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE advertising
IS
BEGIN

        html.b_table('100%');
        htp.p('<tr><td>&nbsp;</td></tr>');
--        htp.p('<tr><td align="center"><a href="http://www.tyin-filefjell.no" target="new_window"><img src="/img/banners/button_tyin.gif" border="0"></a></td></tr>');
--        htp.p('<tr><td>&nbsp;</td></tr>');
        htp.p('<tr><td align="center"><a href="JavaScript:OpenLink(''main.adinfo'');"><img src="/img/banners/button_blank_1.gif" border="0"></a></td></tr>');
        htp.p('<tr><td>&nbsp;</td></tr>');
        htp.p('<tr><td align="center"><a href="JavaScript:OpenLink(''main.adinfo'');"><img src="/img/banners/button_blank_2.gif" border="0"></a></td></tr>');
        htp.p('<tr><td>&nbsp;</td></tr>');
        html.e_table;

END advertising;



---------------------------------------------------------
-- Name:        list_org
-- Type:        procedure
-- What:        start page for listout of organizations
-- Author:      Frode Klevstul
-- Start date:  22.08.2000
-- Desc:
---------------------------------------------------------

PROCEDURE list_org (
                p_from_letter   in varchar2             default NULL,
                p_to_letter     in varchar2             default NULL,
                p_level0        in varchar2             default NULL
        )
IS
BEGIN
DECLARE

CURSOR  select_all (v_from_letter in varchar2, v_to_letter in varchar2) IS
SELECT  organization_pk, name, geography_fk, accepted
FROM    organization, org_service
WHERE   UPPER(name) >= v_from_letter
AND     UPPER(name) < v_to_letter
AND     accepted > 0
AND     organization_fk = organization_pk
AND     service_fk = get.serv
ORDER BY name;

CURSOR  select_list_1 IS
SELECT  distinct(level0), name_sg_fk
FROM    list
WHERE   list_pk in (
        SELECT  distinct(l.level0)
        FROM    list l, list_org lo, org_service os
        WHERE   l.list_pk = lo.list_fk
        AND     lo.organization_fk = os.organization_fk
        AND     os.service_fk = get.serv
        )
;
--AND l.level1 IS NULL
--ORDER BY sg.name

CURSOR  select_list_2 ( v_level_0 IN NUMBER ) IS
SELECT  DISTINCT(o.organization_pk), o.name, o.geography_fk, o.accepted
FROM    list l, list_org lo, organization o, org_service os
WHERE   l.level0 = v_level_0
AND     l.list_pk = lo.list_fk
AND     o.organization_pk = lo.organization_fk
AND     o.organization_pk = os.organization_fk
AND     os.service_fk = get.serv
ORDER BY o.name
;
/*
get.text(v_name_sg_fk)
SELECT  distinct(l.level0), sg.name
FROM    list l, string_group sg, list_org lo, org_service os
WHERE   l.name_sg_fk = sg.string_group_pk
AND     l.list_pk = lo.list_fk
AND     lo.organization_fk = os.organization_fk
AND     os.service_fk = 1
AND l.level1 IS NULL
ORDER BY sg.name
;
AND     os.service_fk = get.serv
*/
v_geography_pk          organization.geography_fk%TYPE          DEFAULT NULL;
v_organization_pk       organization.organization_pk%type       DEFAULT NULL;
v_accepted              organization.accepted%TYPE              DEFAULT NULL;
v_name                  organization.name%type                  DEFAULT NULL;
c_service_mmc           varchar2(500)                           DEFAULT NULL;
v_level                 list.level0%TYPE                        DEFAULT NULL;
v_i                     NUMBER                                  DEFAULT NULL;
v_j                     NUMBER                                  DEFAULT NULL;
v_name_sg_fk            list.name_sg_fk%TYPE                    DEFAULT NULL;

BEGIN
-- Legger inn i statistikk tabellen
stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,21);

        IF ( p_from_letter IS NULL
                AND p_to_letter IS NULL
                AND p_level0 IS NULL ) THEN
                html.b_page_3('200');
                html.b_box(get.txt('all_organizations'));
                htp.p('
                <!-- list_org_menu -->
                <table cellspacing="3">

                <tr><td>
                <a target="Org_on_list"
                href="main.list_org?p_from_letter=%20&p_to_letter=F">A&nbsp;-&nbsp;E</a>
                </td></tr>
                <tr><td>
                <a target="Org_on_list"
                href="main.list_org?p_from_letter=F&p_to_letter=K">F&nbsp;-&nbsp;J</a>
                </td></tr>
                <tr><td>
                <a target="Org_on_list"
                href="main.list_org?p_from_letter=K&p_to_letter=P">K&nbsp;-&nbsp;O</a>
                </td></tr>
                <tr><td>
                <a target="Org_on_list"
                href="main.list_org?p_from_letter=P&p_to_letter=U">P&nbsp;-&nbsp;T</a>
                </td></tr>
                <tr><td>
                <a target="Org_on_list"
                href="main.list_org?p_from_letter=U&p_to_letter=��">U&nbsp;-></a>
                </td></tr>
                <tr><td>
                <a target="Org_on_list"
                href="main.list_org?p_from_letter=%20&p_to_letter=��">'||get.txt('all_on_service')||'</a>
                </td></tr>
                </table>
                <!-- /list_org_menu -->
                ');
                html.e_box;
                html.b_box(get.txt('all_lists'));
                html.b_table;
                open select_list_1;
                loop
                        fetch select_list_1 into v_level, v_name;
                        exit when select_list_1%NOTFOUND;
                        htp.p('<tr><td><a target="Org_on_list" href="main.list_org?p_level0='||v_level||'">'|| get.text(v_name) ||'</a></td></tr>');
                end loop;
                close select_list_1;
                html.e_table;
                html.e_box;
        ELSIF ( p_level0 IS NOT NULL ) THEN
                html.b_page_3(get.value('width')-250);
                SELECT  NAME_SG_FK
                INTO    v_name
                FROM    list
                WHERE   level0 = p_level0
                AND     level1 IS NULL
                ;
                html.b_box( get.txt('list_out_org_on_list') ||' '''||get.text(v_name)||'''', '100%', 'main.list_org');
                html.b_table;
                v_i := 0;
                OPEN select_list_2(p_level0);
                LOOP
                        FETCH select_list_2 INTO v_organization_pk, v_name, v_geography_pk, v_accepted;
                        EXIT WHEN select_list_2%NOTFOUND;

                        IF ( v_accepted = 2 ) THEN
                           v_name := '<i>'||v_name||'</i>';
                        END IF;

                        htp.p('<tr'||system.bgcolor(select_list_2%ROWCOUNT)||'><td nowrap width="70%"><a href="JavaScript: OpenLink(''org_page.main?p_organization_pk='||v_organization_pk||''');">'|| v_name ||'</a></td><td>('||get.locn(v_geography_pk)||')</td></tr>');
/*
                        v_i := v_i +1;
                        IF ( v_i = 1 ) THEN
                                htp.p('<tr>');
                        END IF;
                        htp.p('<td nowrap width="50%"><a href="JavaScript: OpenLink(''org_page.main?p_organization_pk='||v_organization_pk||''');">'|| v_name ||'</a>&nbsp;&nbsp;('||get.locn(v_geography_pk)||')</td>');
--                        htp.p('<td nowrap><a target="Org_on_list" href="main.list_org?p_level1='||v_level||'">'|| v_name ||'</a></td>');
                        IF ( v_i = 2 ) THEN
                                htp.p('</tr>');
                                v_i := 0;
                        END IF;
*/
                END LOOP;
                CLOSE select_list_2;
 /*
                IF ( v_i = 1 ) THEN
                        htp.p('<td colspan="2">&nbsp;</td></tr>');
                ELSIF ( v_i = 2 ) THEN
                        htp.p('<td>&nbsp;</td></tr>');
                END IF;
*/
                html.e_table;
                html.e_box;
        ELSE
                html.b_page_3(get.value('width')-250);
                html.b_box( NULL, '100%', 'main.list_org');
                html.b_table;

                v_i := 0;
                IF ( p_from_letter = '0' AND p_to_letter = '��' ) THEN
                   v_j := 2;
                ELSE
                   v_j := 1;
                END IF;
                OPEN select_all(p_from_letter,p_to_letter);
                LOOP
                        FETCH select_all INTO v_organization_pk, v_name, v_geography_pk, v_accepted;
                        EXIT WHEN select_all%NOTFOUND;

                        IF ( v_accepted = 2 ) THEN
                           v_name := '<i>'||v_name||'</i>';
                        END IF;

                        htp.p('<tr'||system.bgcolor(select_all%ROWCOUNT)||'><td nowrap width="70%"><a href="/cgi-bin/org_page.main?p_organization_pk='||v_organization_pk||'" target="_top">'|| v_name ||'</a></td><td>('||get.locn(v_geography_pk)||')</td></tr>');
--                      htp.p('<tr><td nowrap width="70%"><a href="JavaScript: OpenLink(''org_page.main?p_organization_pk='||v_organization_pk||''');">'|| v_name ||'</a></td><td>('||get.locn(v_geography_pk)||')</td></tr>');

/*                        v_i := v_i +1;
                        IF ( v_i = 1 ) THEN
                                htp.p('<tr>');
                        END IF;
                        htp.p('<td nowrap width="'|| 70 / v_j ||'%"><a href="JavaScript:OpenLink(''org_page.main?p_organization_pk='||v_organization_pk||''');">'|| v_name ||'</a></td><td>('||get.locn(v_geography_pk)||')</td>');
--  <td nowrap width="35%"><a href="JavaScript:OpenLink(''org_page.main?p_organization_pk='||v_organization_pk||''');">'|| v_name ||'</a>&nbsp;&nbsp;('||get.locn(v_geography_pk)||')</td>');
                        IF ( v_i = v_j ) THEN
                                htp.p('</tr>');
                                v_i := 0;
                        END IF;
*/
                END LOOP;
                CLOSE select_all;


                IF ( v_i = 1 AND v_j = 2 ) THEN
                        htp.p('<td>&nbsp;</td></tr>');
                END IF;

/*                IF ( v_i = 1 ) THEN
                        htp.p('<td colspan="2">&nbsp;</td></tr>');
                ELSIF ( v_i = 2 ) THEN
                        htp.p('<td>&nbsp;</td></tr>');
                END IF;
*/
                html.e_table;
                html.e_box;
        end if;

        html.e_page_2;

END;
END list_org;




---------------------------------------------------------
-- Name:        the_line
-- Type:        procedure
-- What:        gets a random "line of the load"
-- Author:      Frode Klevstul
-- Start date:  03.09.2000
-- Desc:
---------------------------------------------------------
PROCEDURE the_line
IS
BEGIN
DECLARE

        v_max                   number                                  default NULL;
        v_random                number                                  default NULL;
        v_random_2              number                                  default NULL;
        v_check                 number                                  default 0;
        v_line                  line.line%type                          default NULL;
        v_who_said              line.who_said%type                      default NULL;
        v_where_said            line.where_said%type                    default NULL;
        v_when_said             line.when_said%type                     default NULL;
        v_line_pk               line.line_pk%type                       default NULL;
        v_output                varchar2(700)                           default NULL;

        v_service_pk                    service.service_pk%type                                 default get.serv;
        v_language_pk                   la.language_pk%type                                     default get.lan;

        CURSOR  select_all IS
        SELECT  DISTINCT(line_pk), line, '<br>( '|| who_said, ' ( '|| where_said ||' - ', when_said ||' ) )'
        FROM    line l, line_type lt, line_type_on_service ltos
        WHERE   l.line_type_fk = lt.line_type_pk
        AND     ltos.line_type_fk = lt.line_type_pk
        AND     ltos.service_fk IN ( v_service_pk, 0 )
        AND     lt.language_fk IN ( v_language_pk, 0 )
        AND     l.accepted = 1
        AND     l.line_pk >= ( select to_number(value) from parameter where name = 'random_line' )
        ORDER BY l.line_pk;

BEGIN

  OPEN select_all;
  LOOP
    FETCH select_all into v_line_pk, v_line, v_who_said, v_where_said, v_when_said;
    EXIT WHEN select_all%NOTFOUND;
    IF ( select_all%ROWCOUNT = 2 ) THEN
           UPDATE parameter
           SET value = to_char(v_line_pk)
           WHERE name = 'random_line';
           COMMIT;
           EXIT;
    END IF;
    v_output := v_who_said || v_where_said || v_when_said;
  END LOOP;
  v_output := REPLACE (v_output, '(  -  )', '');
  v_output := REPLACE (v_output, '-  )', ')');


  IF ( select_all%ROWCOUNT = 1 ) THEN
     UPDATE parameter
       SET value = 1
       WHERE name = 'random_line';
     COMMIT;
  END IF;
  CLOSE select_all;

  html.b_box( get.txt('line_of_the_load') );
  html.b_table;
  htp.p('<tr><td><i>'||v_line||'</i> '|| v_output ||'</td></tr>');
  html.e_table;
  html.e_box;

END;
END the_line;

---------------------------------------------------------------------
-- Name: org_frame
-- Type: procedure
-- What: Lager frames oppsett
-- Made: Espen Messel
-- Date: 20.07.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE org_frame
IS
BEGIN

htp.p('
<html><head>
<meta http-equiv="Content-Type" content="text/html">
<title>in4mant.com</title>
</head>
<frameset cols="*,750,*" border="0" frameborder="0">

<frame name="Left" src="main.plain" scrolling="no" noresize>
<frameset rows="'||get.value('frame_row_top_height')||',*,25" border=0 frameborder="0">
    <frame name="Navigation" src="fora_util.print_top?p_b_page=3" marginwidth="0" marginheight="3" scrolling="no" frameborder="no" border=0 noresize>
    <frameset cols="210,*">
    <frame name="List"  src="main.list_org" marginwidth="3"
        marginheight="3" scrolling="auto" frameborder="no" border="0" scrolling="auto" noresize>
        <frame name="Org_on_list" src="main.plain?p_bgcolor='||REPLACE(get.value('c_'|| get.serv_name ||'_bbs' ),'#','')||'" scrolling="auto">
</frameset>
<frame name="Footer" src="html.bottom" scrolling="no" noresize>
</frameset><frame name="Right" src="main.plain" scrolling="no">
</frameset></html>');

END org_frame;

---------------------------------------------------------------------
-- Name: plain
-- Type: procedure
-- What: Lager en plain side
-- Made: Espen Messel
-- Date: 20.07.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE plain (               p_bgcolor               IN VARCHAR2                             DEFAULT NULL
                )
IS
BEGIN
DECLARE
v_code  varchar2(4000)          default NULL;
v_color VARCHAR2(500)            DEFAULT NULL;

BEGIN
v_code := html.getcode('b_page_2');
v_code := REPLACE (v_code, '[width]', '100%' );
v_code := REPLACE (v_code, '[reload]', '');
v_code := REPLACE (v_code, '[jump]', '');
v_code := REPLACE (v_code, '[css]', html.getcode('css') );
v_code := REPLACE (v_code, '[javascript]', html.getcode('javascript') );

IF ( p_bgcolor IS NULL ) THEN
        v_color := get.value('c_'|| get.serv_name ||'_bbm' );
ELSE
        v_color := '#' || p_bgcolor;
END IF;

v_code := REPLACE (v_code, '[c_service_bbm]' , v_color );
v_code := REPLACE (v_code, '[c_service_bbs]' , v_color );
v_code := REPLACE (v_code, '#ffffff' , v_color );

htp.p(v_code);
IF ( p_bgcolor IS NOT NULL ) THEN
        html.b_box(get.txt('list_org'),'100%','main.list_org');
        html.b_table;
        htp.p('<tr><td>'||get.txt('list_org_info')||'</td></tr>');
        html.e_table;
        html.e_box;
END IF;
html.e_page_2;
END;
END plain;






---------------------------------------------------------
-- Name:        test
-- Type:        procedure
-- What:        writes out all html elements, used for testing color settings on a service
-- Author:      Frode Klevstul
-- Start date:  03.09.2000
-- Desc:
---------------------------------------------------------
PROCEDURE test
IS
BEGIN

        html.b_page;
        htp.p('<br>');

        html.main_title('html.main_menu');
        htp.p('<br>');
        htp.p('<br>');

        html.b_box('html.b_box');
        html.b_table;
        htp.p('<tr><td>&nbsp;</td></tr>');
        htp.p('<tr><td>this is inside a table inside a box</td></tr>');
        htp.p('<tr><td>this is inside a table inside a box</td></tr>');
        htp.p('<tr><td>this is inside a table inside a box</td></tr>');
        htp.p('<tr><td>this is inside a table inside a box</td></tr>');
        htp.p('<tr><td>this is inside a table inside a box</td></tr>');
        html.e_table;
        html.e_box;

        htp.p('<br><br><table width="100%"><tr><td width="50%" valign="top">');

        html.b_box('html.b_box');
        html.b_table;
        htp.p('<tr><td>&nbsp;</td></tr>');
        htp.p('<tr><td>this is inside a table inside a box</td></tr>');
        html.e_table;
        html.e_box;

        htp.p('</td><td>');

        html.b_box('html.b_box');
        html.b_table;
        htp.p('<tr><td>&nbsp;</td></tr>');
        htp.p('<tr><td>this is inside a table inside a box</td></tr>');
        htp.p('<tr><td>this is inside a table inside a box</td></tr>');
        htp.p('<tr><td>this is inside a table inside a box</td></tr>');
        htp.p('<tr><td>this is inside a table inside a box</td></tr>');
        htp.p('<tr><td>this is inside a table inside a box</td></tr>');
        htp.p('<tr><td>this is inside a table inside a box</td></tr>');
        htp.p('<tr><td>this is inside a table inside a box</td></tr>');
        htp.p('<tr><td>this is inside a table inside a box</td></tr>');
        htp.p('<tr><td>this is inside a table inside a box</td></tr>');
        html.e_table;
        html.e_box;

        htp.p('</td></tr></table>');

        html.e_page;

END test;



---------------------------------------------------------
-- Name:        clock
-- Type:        procedure
-- What:        writes out server time
-- Author:      Frode Klevstul
-- Start date:  16.11.2000
-- Desc:
---------------------------------------------------------
PROCEDURE clock
IS
BEGIN
DECLARE

        v_time          date            default NULL;
        v_month         number          default NULL;

v_test varchar2(1000);

BEGIN

        v_time := sysdate;

        -- m� trekke fra en for � fa javascriptet riktig, aner ikke hvorfor
        v_month := to_number( to_char(v_time, 'MM') );
        v_month := v_month - 1;

        htp.p('
                <Script language=JavaScript>
                var timerID = null
                var timerRunning = false
                var dt = new Date('||to_char(v_time, 'YYYY')||', '|| v_month ||', '||to_char(v_time, 'DD')||', '||to_char(v_time, 'HH24')||', '||to_char(v_time, 'MI')||', '||to_char(v_time, 'SS')||' )
                var ns4 = (document.layers)? true:false
                var ie4 = (document.all)? true:false
                var ns6 = (document.getElementsByName)? true:false

                function startclock(){
                        timerID = setInterval("showtime()",1000)
                        timerRunning = true
                }

                function showtime(){
                        var timeValue
                        var dwS = ""
                        var mS = ""

                        dt.setSeconds( dt.getSeconds() + 1 )

                        var dw = dt.getDay()
                        var d = dt.getDate()
                        var m = dt.getMonth()+1
                        var hours = dt.getHours()
                        var minutes = dt.getMinutes()
                        var seconds = dt.getSeconds()

                        switch ( dw ) {
                                case 1: dwS = "'|| get.txt('mon') ||'"; break;
                                case 2: dwS = "'|| get.txt('tue') ||'"; break;
                                case 3: dwS = "'|| get.txt('wed') ||'"; break;
                                case 4: dwS = "'|| get.txt('thu') ||'"; break;
                                case 5: dwS = "'|| get.txt('fri') ||'"; break;
                                case 6: dwS = "'|| get.txt('sat') ||'"; break;
                                case 0: dwS = "'|| get.txt('sun') ||'"; break;
                        }

                        switch ( m ) {
                                case 1: mS = "'|| get.txt('jan') ||'";  break;
                                case 2: mS = "'|| get.txt('feb') ||'";  break;
                                case 3: mS = "'|| get.txt('mar') ||'";  break;
                                case 4: mS = "'|| get.txt('apr') ||'";  break;
                                case 5: mS = "'|| get.txt('may') ||'";  break;
                                case 5: mS = "'|| get.txt('jun') ||'";  break;
                                case 6: mS = "'|| get.txt('jul') ||'";  break;
                                case 8: mS = "'|| get.txt('aug') ||'";  break;
                                case 9: mS = "'|| get.txt('sep') ||'";  break;
                                case 10: mS = "'|| get.txt('oct') ||'"; break;
                                case 11: mS = "'|| get.txt('nov') ||'"; break;
                                case 12: mS = "'|| get.txt('dec') ||'"; break;
                        }

                        timeValue = dwS
                        timeValue += "&nbsp;" + d
                        timeValue += "&nbsp;" + mS
                        timeValue += "&nbsp;" + ((hours < 10) ? "0" : "") + hours
                        timeValue += ((minutes < 10) ? ":0" : ":") + minutes
                        timeValue += ((seconds < 10) ? ":0" : ":") + seconds
                        timeValue += "&nbsp;cet"

                        var id = ''clockLayer''

                        if ( ie4 ) {
                                document.all[id].innerHTML = timeValue
                        }
                        else if (ns4) {
                                document.layers[id].document.open()
                                document.layers[id].document.write(timeValue)
                                document.layers[id].document.close()
                        }
                        else if (ns6) {
                                document.getElementById(id).innerHTML = timeValue
                        }
                }
                </SCRIPT>

                <BODY onLoad="startclock()">
                <FORM NAME="clock" onSubmit="0">
                        <div id="clockLayer"><nobr></nobr></div>
                </FORM>
                </BODY>

                <layer name="clockLayer" left="0" top="0" bgcolor="#ffffff"></layer>
        ');


END;
END clock;

-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/
show errors;
