set define off
PROMPT *** package: games ***

CREATE OR REPLACE PACKAGE games IS

        PROCEDURE startup;
        PROCEDURE reflex
                (
                        p_response_time         in varchar2     default NULL
                );

        PROCEDURE the_hiscore
                (
                        p_game_pk               in number       default NULL,
                        p_number                in number       default NULL,
                        p_type                  in number       default NULL,
                        p_heading               in number       default NULL
                );
        PROCEDURE week_hiscore
                (
                        p_game_pk               in number       default NULL,
                        p_type                  in number       default NULL,
                        p_heading               in number       default NULL
                );
        PROCEDURE thrower
                (
                        p_score         in hiscore.score%type   default NULL
                );
        PROCEDURE thrower_javascript;
        PROCEDURE thrower_css;

END;
/
CREATE OR REPLACE PACKAGE BODY games IS

---------------------------------------------------------
-- Name:        startup
-- Type:        procedure
-- What:        startup procedure
-- Author:      Frode Klevstul
-- Start date:  03.02.2001
-- Desc:
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

        games.reflex;

END startup;




---------------------------------------------------------
-- Name:        reflex
-- Type:        procedure
-- What:        the game "reflex"
-- Author:      Frode Klevstul
-- Start date:  03.02.2001
-- Desc:
---------------------------------------------------------
PROCEDURE reflex
        (
                p_response_time         in varchar2     default NULL
        )
IS
BEGIN
DECLARE
        v_response_time         varchar2(20)                    default NULL;
        v_hiscore_pk            hiscore.hiscore_pk%type         default NULL;
        v_user_pk               usr.user_pk%type                default NULL;
        v_placing               number                          default NULL;
        v_check                 number                          default NULL;

BEGIN


        html.b_page;
        html.empty_menu;


        htp.p('<br><br>');


        -- -----------------------------------------------------
        -- legger inn i hiscore listen, skriver ut resultatet
        -- -----------------------------------------------------

        -- en liten sjekk som "hindrer" at man poster feil verdier / jukser med hiscore lista
        if (
                p_response_time IS NOT NULL and
                owa_pattern.match( owa_util.get_cgi_env('SERVER_NAME'), 'bytur.no') and
                owa_pattern.match( owa_util.get_cgi_env('PATH_INFO'), 'games.reflex')
        ) then

                -- behandler tekststrengen slik at denne kan legges inn i databasen som et tall;
                if not( owa_pattern.match( p_response_time, '^0' ) ) then
                        v_response_time := '0'||p_response_time;
                else
                        v_response_time := p_response_time;
                end if;
                owa_pattern.change(v_response_time, '\.', ',', 'g');


                -- finner ut hvilken plass man havnet p�
                SELECT  count(*)
                INTO    v_placing
                FROM    hiscore
                WHERE   game_fk = 1
                AND     score < to_number(v_response_time);

                v_placing := v_placing + 1;


                -- skal bare legge inn i databasen dersom man er logget inn
                -- hvis ikke man er logget inn skriver vi ut en beskjed at man m�
                -- registrere seg / logge seg inn for � komme i hiscore listen
                if ( login.timeout(NULL, 1) > 0) then

                        v_user_pk := get.uid;

                        -- en sjekk slik at man ikke skal kunne trykke "reload"
                        -- og f� alle plassene i lista dersom man f�r en bra tid
                        SELECT  count(*)
                        INTO    v_check
                        FROM    hiscore
                        WHERE   game_fk = 1
                        AND     user_fk = v_user_pk
                        AND     score = to_number(v_response_time)
                        AND     score_date > sysdate - 1/1440;

                        if (v_check = 0) then
                                SELECT  hiscore_seq.NEXTVAL
                                INTO    v_hiscore_pk
                                FROM    DUAL;

                                INSERT INTO HISCORE
                                (hiscore_pk, game_fk, user_fk, score, score_date)
                                VALUES(v_hiscore_pk, 1, v_user_pk, to_number(v_response_time), sysdate);
                                commit;
                        end if;

                        htp.p( '<center><b>'|| get.txt('you_was_number') ||'&nbsp;'|| v_placing ||'</b></center>');
                else
                        htp.p( '<center><b>'|| get.txt('you_was_number') ||'&nbsp;'|| v_placing ||'</b>');
                        htp.p( '<br>'|| get.txt('log_in_to_enter_hiscore_list') ||'<br><br>');

                        login.login_form('games.reflex?p_response_time='||v_response_time, 0, '30%');
                end if;

                htp.p('<br><br><br>');

        end if;



        html.b_table;
        htp.p('<tr><td valign="top" width="60%">');
        -- venstre side

        html.b_box( get.txt('the_reflex_tester'), '100%', 'games.reflex');

        htp.p('
                <script language="JavaScript">
                <!-- hiding for old browsers
                        // reflex tester

                var startTime=new Date();
                var endTime=new Date();
                var startPressed=false;
                var bgChangeStarted=false;
                var maxWait=20;
                var timerID;

                function startTest()
                {
                        document.bgColor=document.response.bgColorChange.options[document.response.bgColorChange.selectedIndex].text;
                        bgChangeStarted=true;
                        startTime=new Date();
                }

                function remark(responseTime)
                {
                        var responseString="";
                        if (responseTime == 0)
                                responseString="'|| get.txt('reflex_impossible') ||'";
                        if (responseTime > 0 && responseTime <0.01)
                                responseString="'|| get.txt('reflex_amazing') ||'";
                        if (responseTime >=.01 && responseTime <.05 )
                                responseString="'|| get.txt('reflex_fantastic') ||'";
                        if (responseTime >= 0.05 && responseTime <.10)
                                responseString="'|| get.txt('reflex_impressing') ||'";
                        if (responseTime >= 0.10 && responseTime < 0.20)
                                responseString="'|| get.txt('reflex_good') ||'";
                        if (responseTime >=0.20 && responseTime < 0.30)
                                responseString="'|| get.txt('reflex_ok') ||'";
                        if (responseTime >=0.30 && responseTime < 0.40)
                                responseString="'|| get.txt('reflex_can_be_better') ||'";
                        if (responseTime >=0.40 && responseTime < 0.50)
                                responseString="'|| get.txt('reflex_practice') ||'";
                        if (responseTime >=0.50 && responseTime < 0.60)
                                responseString="'|| get.txt('reflex_as_grandma') ||'";
                        if (responseTime >=0.60 && responseTime < 0.70)
                                responseString="'|| get.txt('reflex_try_again') ||'";
                        if (responseTime >=0.70 && responseTime < 0.80)
                                responseString="'|| get.txt('reflex_concentrate') ||'";
                        if (responseTime >=0.80 && responseTime < 0.90)
                                responseString="'|| get.txt('reflex_slow') ||'";
                        if (responseTime >=0.90 && responseTime < 1)
                                responseString="'|| get.txt('reflex_drunk') ||'";
                        if (responseTime >=1)
                                responseString="'|| get.txt('reflex_sleeping') ||'";

                        return responseString;
                }

                function stopTest()
                {
                        if(bgChangeStarted)
                        {
                                endTime=new Date();
                                var responseTime=(endTime.getTime()-startTime.getTime())/1000;
                                document.bgColor="'|| get.value( 'c_'|| get.serv_name ||'_bbm' ) ||'";
                                alert(" '||get.txt('your_response_time_is')||' : " + responseTime + " '||get.txt('seconds')||' " + "\n" + remark(responseTime));
                                startPressed=false;
                                bgChangeStarted=false;
                                document.hiscore.p_response_time.value = responseTime;
                                document.hiscore.submit();
                        }
                        else
                        {
                                if (!startPressed)
                                {
                                        alert("'|| get.txt('press_start_first_to_start_test') ||'");
                                }
                                else
                                {
                                        clearTimeout(timerID);
                                        startPressed=false;
                                        alert("'|| get.txt('you_pressed_too_early') ||'");
                                }
                        }
                }

                var randMULTIPLIER=0x015a4e35;
                var randINCREMENT=1;
                var today=new Date();
                var randSeed=today.getSeconds();
                function randNumber()
                {
                        randSeed = (randMULTIPLIER * randSeed + randINCREMENT) % (1 << 31);
                        return((randSeed >> 15) & 0x7fff) / 32767;
                }


                function startit()
                {
                        if(startPressed)
                        {
                                alert("'|| get.txt('already_started') ||'");
                                return;
                        }
                        else
                        {
                                startPressed=true;
                                timerID=setTimeout(''startTest()'', 6000*randNumber());
                        }
                }
                // -->
                </script>

                ');

                htp.p( get.txt('game_reflex_description') );

                htp.p('

                <p align="center">&nbsp;</p>

                <form name="response">
                  <div align="center"><center><p>
                  '|| get.txt('game_reflex_change_bgcolor_in') ||':
                  <select name="bgColorChange" size="1">
                    <option selected>yellow</option>
                    <option>blue</option>
                    <option>green</option>
                    <option>pink</option>
                  </select>
                  <input onclick="startit()" type="button" value="'|| get.txt('start') ||'">
                  <input onclick="stopTest()" type="button" value="'|| get.txt('stop') ||'">
                  </p>
                  </center></div>
                </form>

                <form name="hiscore" action="games.reflex" method="POST">
                <input type="hidden" name="p_response_time">
                </form>

        ');
        html.e_box;


        -- v. side slutt
        htp.p('</td><td>&nbsp;&nbsp;</td><td valign="top">');
        -- h�yre side

        -- ----------------------------------------------------------------------
        -- skriver ut hiscore listen til spill nummer 1 (som er REFLEX spillet)
        -- ----------------------------------------------------------------------
        games.the_hiscore(1, 30);

        -- h. side slutt
        htp.p('</td></tr><tr><td colspan="3">');

        tip.someone;

        htp.p('</td></tr>');

        html.e_table;





        html.e_page;

END;
END reflex;





---------------------------------------------------------
-- Name:        hiscore
-- Type:        procedure
-- What:        writes out the hiscore list for a game
-- Author:      Frode Klevstul
-- Start date:  06.02.2001
-- Desc:
---------------------------------------------------------
PROCEDURE the_hiscore
        (
                p_game_pk               in number       default NULL,
                p_number                in number       default NULL,
                p_type                  in number       default NULL,
                p_heading               in number       default NULL
        )
IS
BEGIN
DECLARE

        CURSOR  select_all IS
        SELECT  login_name, score, score_date
        FROM    hiscore, usr
        WHERE   user_fk = user_pk
        AND     game_fk = p_game_pk
        ORDER BY score ASC, score_date DESC;

        CURSOR  select_all_desc IS
        SELECT  login_name, score, score_date
        FROM    hiscore, usr
        WHERE   user_fk = user_pk
        AND     game_fk = p_game_pk
        ORDER BY score DESC, score_date DESC;

        v_login_name    usr.login_name%type     default NULL;
        v_score         hiscore.score%type      default NULL;
        v_score_date    hiscore.score_date%type default NULL;
        v_number        number                  default NULL;

BEGIN

        if (p_heading IS NOT NULL) then
                html.b_page;
        end if;

        html.b_box(get.txt('hiscore_list'), '100%', 'games.the_hiscore');
        html.b_table;

        htp.p('
                <tr>
                        <td><b>'|| get.txt('game_placing') ||':</b></td>
                        <td><b>'|| get.txt('game_score_date') ||':</b></td>
                        <td><b>'|| get.txt('game_user') ||':</b></td>
                        <td><b>'|| get.txt('game_score') ||':</b></td>
                </tr>
        ');


        if (p_number IS NULL) then
                v_number := 10;
        else
                v_number := p_number;
        end if;

        if (p_type IS NULL or p_type = 1) then
                open select_all;
                while (select_all%ROWCOUNT < v_number) loop
                        fetch select_all into v_login_name, v_score, v_score_date;
                        exit when select_all%NOTFOUND;

                        htp.p('
                                <tr>
                                        <td>'|| select_all%ROWCOUNT ||'</td>
                                        <td>'|| to_char(v_score_date, get.txt('date_short')) ||'</td>
                                        <td>'|| v_login_name ||'</td>
                                        <td>'|| v_score ||'</td>
                                </tr>
                        ');

                end loop;
                close select_all;
        elsif (p_type = 2) then
                open select_all_desc;
                while (select_all_desc%ROWCOUNT < v_number) loop
                        fetch select_all_desc into v_login_name, v_score, v_score_date;
                        exit when select_all_desc%NOTFOUND;

                        htp.p('
                                <tr>
                                        <td>'|| select_all_desc%ROWCOUNT ||'</td>
                                        <td>'|| to_char(v_score_date, get.txt('date_short')) ||'</td>
                                        <td>'|| v_login_name ||'</td>
                                        <td>'|| v_score ||'</td>
                                </tr>
                        ');

                end loop;
                close select_all_desc;
        end if;

        html.e_table;
        html.e_box;
        if (p_heading IS NOT NULL) then
                html.e_page;
        end if;

END;
END the_hiscore;


---------------------------------------------------------
-- Name:        hiscore
-- Type:        procedure
-- What:        writes out the hiscore list for a game
-- Author:      Frode Klevstul
-- Start date:  06.02.2001
-- Desc:
---------------------------------------------------------
PROCEDURE week_hiscore
        (
                p_game_pk               in number       default NULL,
                p_type                  in number       default NULL,
                p_heading               in number       default NULL
        )
IS
BEGIN
DECLARE

        CURSOR  select_all IS
        SELECT  login_name, score, score_date
        FROM    hiscore, usr
        WHERE   user_fk = user_pk
        AND     game_fk = p_game_pk
        AND     to_char(score_date,'WW') = to_char(sysdate,'WW')
        ORDER BY score ASC, score_date DESC;

        v_login_name    usr.login_name%type     default NULL;
        v_score         hiscore.score%type      default NULL;
        v_score_date    hiscore.score_date%type default NULL;
        v_number        number                  default 10;

BEGIN

        if (p_heading IS NOT NULL) then
                html.b_page;
        end if;

        html.b_box(get.txt('hiscore_list_week'), '100%', 'games.week_hiscore');
        html.b_table;

        htp.p('
                <tr>
                        <td><b>'|| get.txt('game_placing') ||':</b></td>
                        <td><b>'|| get.txt('game_score_date') ||':</b></td>
                        <td><b>'|| get.txt('game_user') ||':</b></td>
                        <td><b>'|| get.txt('game_score') ||':</b></td>
                </tr>
        ');


        open select_all;
        while (select_all%ROWCOUNT < v_number) loop
                fetch select_all into v_login_name, v_score, v_score_date;
                exit when select_all%NOTFOUND;

                htp.p('
                        <tr>
                                <td>'|| select_all%ROWCOUNT ||'</td>
                                <td>'|| to_char(v_score_date, get.txt('date_short')) ||'</td>
                                <td>'|| v_login_name ||'</td>
                                <td>'|| v_score ||'</td>
                        </tr>
                ');

        end loop;
        close select_all;

        html.e_table;
        html.e_box;
        if (p_heading IS NOT NULL) then
                html.e_page;
        end if;

END;
END week_hiscore;





---------------------------------------------------------
-- Name:        thrower
-- Type:        procedure
-- What:        tip thrower
-- Author:      Frode Klevstul
-- Start date:  02.03.2001
-- Desc:        sender med p_score < 1 hele tiden slik at siden
--              skal oppdatere seg n�r man skal for�ke � spille om igjen
---------------------------------------------------------
PROCEDURE thrower
        (
                p_score         in hiscore.score%type   default NULL
        )
IS
BEGIN
DECLARE

        v_login_name            usr.login_name%type             default NULL;
        v_score                 hiscore.score%type              default NULL;
        v_score_date            hiscore.score_date%type         default NULL;
        v_user_pk               usr.user_pk%type                default NULL;
        v_placing               number                          default NULL;
        v_check                 number                          default NULL;
        v_hiscore_pk            hiscore.hiscore_pk%type         default NULL;

BEGIN

        if (
                p_score IS NOT NULL and
                p_score > 1 and
                owa_pattern.match( owa_util.get_cgi_env('SERVER_NAME'), 'bytur.no') and
                owa_pattern.match( owa_util.get_cgi_env('PATH_INFO'), 'games.thrower') and
                owa_util.get_cgi_env('REQUEST_METHOD') = 'POST'
        ) then

                html.b_page;
                html.empty_menu;
                htp.p('<br><br><br>');

                -- finner ut hvilken plass man havnet p�
                SELECT  count(*)
                INTO    v_placing
                FROM    hiscore
                WHERE   game_fk = 2
                AND     score > to_number(p_score);

                v_placing := v_placing + 1;


                -- skal bare legge inn i databasen dersom man er logget inn
                -- hvis ikke man er logget inn skriver vi ut en beskjed at man m�
                -- registrere seg / logge seg inn for � komme i hiscore listen
                if ( login.timeout(NULL, 1) > 0) then

                        v_user_pk := get.uid;

                        -- en sjekk slik at man ikke skal kunne trykke "reload"
                        -- og f� alle plassene i lista dersom man f�r en bra tid
                        SELECT  count(*)
                        INTO    v_check
                        FROM    hiscore
                        WHERE   game_fk = 2
                        AND     user_fk = v_user_pk
                        AND     score = to_number(p_score)
                        AND     score_date > sysdate - 1/1440;

                        if (v_check = 0) then
                                SELECT  hiscore_seq.NEXTVAL
                                INTO    v_hiscore_pk
                                FROM    DUAL;

                                INSERT INTO HISCORE
                                (hiscore_pk, game_fk, user_fk, score, score_date)
                                VALUES(v_hiscore_pk, 2, v_user_pk, to_number(p_score), sysdate);
                                commit;
                        end if;


                        htp.p( '<center><b>'|| get.txt('you_was_number') ||'&nbsp;'|| v_placing ||'</b>');
                else
                        htp.p( '<center><b>'|| get.txt('you_was_number') ||'&nbsp;'|| v_placing ||'</b>');
                        htp.p( '<br>'|| get.txt('log_in_to_enter_hiscore_list') ||'<br><br>');

                        login.login_form('games.thrower?p_score='||p_score, 0, '30%');
                end if;

                htp.p('<br><br><br>');
                html.button_link( get.txt('try_again'), 'games.thrower?p_score=0,'||to_char(sysdate, get.txt('date_full')) );
                htp.p('<br><br><br>');

                games.the_hiscore(2, 50, 2);

                html.e_page;

        else
                html.b_page;
                html.empty_menu;
                htp.p('<bgsound src="/www/thrower/trance2.wav" loop="3">');

                thrower_css;
                thrower_javascript;

                html.b_table;
                htp.p('<tr><td width="50%" valign="top">');

                        html.b_box( get.txt('tip_thrower') , '100%', 'games.thrower');
                        html.b_table;
                        htp.p('
                                <form name="tip_frame" action="games.thrower" method="POST">
                                <input type="hidden" name="p_score" value="">
                                </form>

                                <tr><td colspan="2">
                                        '|| get.txt('thrower_description') ||'
                                        (<a href="games.the_hiscore?p_game_pk=2&p_number=100&p_type=2&p_heading=1">top 100 hiscores</a>)
                                </td></tr>
                                <tr><td colspan="2" height="10">&nbsp;</td></tr>
                                <tr>
                                        <td width="25%">'|| get.txt('thrower_points') ||':</td><td><span id="score2">0</span></td>
                                </tr>
                                <tr>
                                        <td>'|| get.txt('thrower_total_points') ||':</td><td><span id="poengsum">0</span></td>
                                </tr>
                                <tr>
                                        <td>'|| get.txt('thrower_try_left') ||':</td><td><span id="forsoek">25</span></td>
                                </tr>
                        ');
                        html.e_table;
                        html.e_box;

                htp.p('</td><td valign="top">');

                        games.the_hiscore(2, 5, 2);

                htp.p('</td></tr>');
                html.e_table;


                html.e_page;

                htp.p('
                        <DIV ID="bakgrunn"><img name="bakgrunn" src="/www/thrower/bar.jpg"></DIV>
                        <span id="katapulten"><img id="katapult" src="/www/thrower/hand_down.gif" style="cursor:hand" onmousedown="ned();" onmouseup="opp();"></span>
                        <DIV ID="poop"><IMG SRC="/www/thrower/poop.gif"></DIV>
                        <img id="person" src="/www/thrower/coin.gif">
                        <BGSOUND ID="music">
                ');
        end if;

END;
END thrower;




---------------------------------------------------------
-- Name:        thrower_javascript
-- Type:        procedure
-- What:        javscript for the thrower game
-- Author:      Frode Klevstul
-- Start date:  02.03.2001
-- Desc:
---------------------------------------------------------
PROCEDURE thrower_javascript
IS BEGIN

htp.p('

        <SCRIPT language="JavaScript">

        function msover(){
                        event.srcElement.style.color="yellow";
                        event.srcElement.style.cursor = "hand";
        }
        function msout(){
                        event.srcElement.style.color="black";
                        event.srcElement.style.cursor = "auto";
        }
        function toggle( targetId ){
                        target = document.all( targetId );
                        if (target.style.display == "none"){
                                        target.style.display = "";
                                } else {
                                        target.style.display = "none";
                                }
        }
        version = null;
        browserName = navigator.appName;
        browserVer = parseInt(navigator.appVersion);

        if ((browserName != "Microsoft Internet Explorer") || (browserVer < 4)) {
                alert("Internet Explorer 4+ only!");
                document.location.href = "main_page.startup";
        }

        bildetab = new Array();
        bildetab[0] = new Image();
        bildetab[0].src = "/www/thrower/hand_down.gif";
        bildetab[1] = new Image();
        bildetab[1].src = "/www/thrower/hand_up.gif";

        var navn = "";
        var neste ="";
        antall = 0;
        highscore = 0;
        x = -10;
        y = 1;
        posisjon = 0;
        poeng = 0;
        sum = 0;
        forsok = 25;
        kuflyr = false;
        vidde=0;

        function ned() {
        if (!kuflyr)  {
                katapult.src = bildetab[0].src;
                x = -10;
                y = 1;
                vind = Math.random();
                person.style.top = 680;
                person.style.left = 40;
                Starttid = new Date();
                start = Starttid.getTime();
          }
        }

        function opp() {
        if (!kuflyr) {
                kuflyr = true;
                forsok--;
                Stoptid = new Date();
                stop = Stoptid.getTime();
                tid = (stop - start)/50;
                katapult.src = bildetab[1].src;
                flytt();
          }
        }


        function flytt(){
          if (x < 16){
              person.style.top = (Math.pow(x,2)) + 450;
              person.style.left = y*tid + 40 + (vind*30);
                  x++;
                  y++;
              setTimeout("flytt()",0.1);
                 }
         if (x == 16) {
                        x++;
                        posisjon = parseInt(person.style.left);

                        regnUtPoeng(posisjon);
                }
         if (x >= 16) {
                        kuflyr = false;
                 }
        }


        function regnUtPoeng(score) {


        if ((score >= 505) && (score < 525)){
                poeng = 150;
                document.all.music.src="/www/thrower/scream1.wav"
        }
        else if ((score >= 466) && (score < 556)){
                poeng = 100;
                document.all.music.src="/www/thrower/ooww1.wav"
        }
        else if (((score >= 390) && (score < 636))){
                poeng = 30;
        }
        else {
                poeng = -50;
                document.all.music.src="/www/thrower/cmon1.wav"
        }


        sum = sum + poeng;
        score2.innerHTML = poeng;
        poengsum.innerHTML = sum;
        forsoek.innerHTML = forsok;

        if (forsok <= 0) {

                if ( sum == 3750 ) alert("'|| get.txt('reflex_amazing') ||'");
                else if ( sum > 3500 ) alert("'|| get.txt('reflex_good') ||'");
                else if ( sum > 3000 ) alert("'|| get.txt('reflex_ok') ||'");
                else if ( sum > 2500 ) alert("'|| get.txt('reflex_practice') ||'");
                else if ( sum > 1000 ) alert("'|| get.txt('reflex_try_again') ||'");
                else if ( sum > 0 ) alert("'|| get.txt('reflex_concentrate') ||'");
                else alert("'|| get.txt('reflex_drunk') ||'");

                insertHiscore();
                resetscore();
        }

        }


        function resetscore() {
                poeng = 0;
                sum = 0;
                forsok = 25;
                score2.innerHTML = poeng;
                poengsum.innerHTML = sum;
                forsoek.innerHTML = forsok;
                person.style.top = 680;
                person.style.left = 40;
                katapult.src = bildetab[0].src;
        }

        function resetKnapp () {
                if (!kuflyr) resetscore();
        }

        function insertHiscore(){
                document.tip_frame.p_score.value = sum;
                document.tip_frame.submit();
        }

        </SCRIPT>


');

END thrower_javascript;






---------------------------------------------------------
-- Name:        thrower_javascript
-- Type:        procedure
-- What:        javscript for the thrower game
-- Author:      Frode Klevstul
-- Start date:  02.03.2001
-- Desc:
---------------------------------------------------------
PROCEDURE thrower_css
IS BEGIN

htp.p('

        <STYLE TYPE="text/css">


        #nesteside {position: absolute;
                top: 500;
                left: 10;
                z-index: 1000;
        }
        #persnavn {position: absolute;
                top: 520;
                left: 10;
                z-index: 1001;
        }
        #bakgrunn {position: absolute;
                top: 420;
                left: 10;
                z-index: 9;
                }
        #person {position: absolute;
                top:680;
                left: 40;
                z-index: 11;
                }
        #katapulten {position: absolute;
                top:700;
                left: 36;
                z-index: 10;
                }
        #poop {position: absolute;
                top:670;
                left: 523;
                z-index: 100;
                }
        a:hover {
           color:black;
           }

        BODY
        {
            BACKGROUND-ATTACHMENT: fixed;
            BACKGROUND-IMAGE: none;
            BACKGROUND-REPEAT: repeat;
            FONT-FAMILY: "Times New Roman";
            FONT-SIZE: 10pt;
            LINE-HEIGHT: 14pt;
            background-postion: top:0px
        }
        SPAN
        {
            COLOR: #000000;
            FONT-FAMILY: "Arial";
            FONT-SIZE: 10pt;
            FONT-WEIGHT: bold
        }
        H1
        {
            COLOR: #000000;
            FONT-FAMILY: "Arial";
            FONT-SIZE: 20pt;
            FONT-WEIGHT: bold
        }
        H2
        {
            COLOR: #000000;
            FONT-FAMILY: "Arial";
            FONT-SIZE: 15pt;
            FONT-WEIGHT: bold;
            MARGIN-TOP: 30pt
        }
        H3
        {
            COLOR: #000000;
            FONT-FAMILY: "Times New Roman";
            FONT-SIZE: 12pt;
            MARGIN-TOP: 6pt
        }
        P
        {
            FONT-FAMILY: "Times New Roman";
            FONT-SIZE: 10pt;
            LINE-HEIGHT: 14pt;
            MARGIN-TOP: 6pt;
            TEXT-INDENT: 12pt
        }
        PRE
        {
            COLOR: red;
            FONT-FAMILY: "Courier";
            FONT-SIZE: 10pt;
            LINE-HEIGHT: 14pt
        }
        A
        {
            COLOR: black;
            FONT-SIZE: 10pt;
            TEXT-DECORATION: underline
        }


        </STYLE>


');

END thrower_css;




-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/
show errors;
