set define off
PROMPT *** package: PHOTO ***

CREATE OR REPLACE PACKAGE photo IS

        PROCEDURE startup;
        PROCEDURE list_albums;
        PROCEDURE insert_album
                (
                        p_album_pk              in album.album_pk%type                  default NULL,
                        p_title                 in album_text.title%type                default NULL,
                        p_when_period           in album_text.when_period%type          default NULL,
                        p_description           in album_text.description%type          default NULL,
                        p_language_fk           in album_text.language_fk%type          default NULL,
                        p_publish_date          in varchar2                             default NULL,
                        p_publish_date2         in varchar2                             default NULL,
                        p_activated             in album.activated%type                 default NULL
        );
    PROCEDURE activate
                    (
                            p_album_pk              in album.album_pk%type                  default NULL,
                            p_activated             in album.activated%type                 default NULL
                    );
    PROCEDURE delete_album
                    (
                            p_album_pk              in album.album_pk%type                  default NULL
                    );
    PROCEDURE delete_album_2
                    (
                            p_album_pk              in album.album_pk%type                  default NULL,
                            p_confirm               in varchar2                             default NULL
                    );
    PROCEDURE update_album
                    (
                            p_album_pk              in album.album_pk%type  default NULL
                    );
    PROCEDURE upload
                    (
                            p_album_pk              in album.album_pk%type  default NULL
                    );
    PROCEDURE upload2
        (
                        p_album_pk              in album.album_pk%type          default NULL,
                        p_path                  in picture.path%type            default NULL,
                        p_full_path             in picture.path%type            default NULL,
                        p_file_size             in picture.file_size%type       default NULL,
                        p_width                 in picture.width%type           default NULL,
                        p_height                in picture.height%type          default NULL
        );
    PROCEDURE upload3
        (
                        p_album_pk              in album.album_pk%type          default NULL,
                        p_path                  in picture.path%type            default NULL,
                        p_publish_date          in varchar2                     default NULL,
                        p_publish_date2         in varchar2                     default NULL,
                        p_whom                  in picture.whom%type            default NULL,
                        p_photographer          in picture.photographer%type    default NULL,
                        p_what                  in picture_text.what%type       default NULL,
                        p_activated                             in picture.activated%type               default NULL,
                        p_file_size                             in picture.file_size%type               default NULL,
                        p_width                                 in picture.width%type                   default NULL,
                        p_height                                in picture.height%type                  default NULL
                );
    PROCEDURE upload4
        (
                        p_path                  in picture.path%type            default NULL
        );
    PROCEDURE upload_error
                    (
                            p_error_txt             in varchar2             default NULL,
                            p_album_pk              in album.album_pk%type  default NULL
                    );
    PROCEDURE list_pictures
                    (
                            p_album_pk              in album.album_pk%type  default NULL
                    );
    PROCEDURE show_picture
                    (
                            p_picture_pk            in picture.picture_pk%type      default NULL
                    );
    PROCEDURE update_picture
                    (
                            p_picture_pk            in picture.picture_pk%type      default NULL
                    );
    PROCEDURE update_pic_action
                    (
                            p_picture_pk            in picture.picture_pk%type      default NULL,
                            p_publish_date          in varchar2                     default NULL,
                            p_publish_date2         in varchar2                     default NULL,
                            p_whom                  in picture.whom%type            default NULL,
                            p_photographer          in picture.photographer%type    default NULL,
                        p_what                  in picture_text.what%type       default NULL,
                        p_activated             in picture.activated%type       default NULL
                    );
    PROCEDURE delete_picture
                    (
                            p_picture_pk            in picture.picture_pk%type      default NULL
                    );
    PROCEDURE delete_picture_2
                    (
                            p_picture_pk    in picture.picture_pk%type              default NULL,
                            p_confirm       in varchar2                             default NULL
                    );
    PROCEDURE user_list_albums
            (
                    p_organization_pk               in organization.organization_pk%type    default NULL,
                    p_no                            number                                  default NULL
            );
    PROCEDURE user_list_pictures
            (
                    p_album_pk              in album.album_pk%type  default NULL
            );
    PROCEDURE user_show_picture
            (
                    p_picture_pk            in picture.picture_pk%type      default NULL
            );
    PROCEDURE user_last_pictures
            (
                    p_organization_pk               in organization.organization_pk%type    default NULL,
                    p_archive                       in number                               default NULL
            );
    PROCEDURE user_add_comment
            (
                    p_picture_pk            in picture.picture_pk%type      default NULL
            );
    PROCEDURE user_insert_comment
            (
                    p_picture_pk            in picture.picture_pk%type              default NULL,
                    p_the_comment           in picture_comment.the_comment%type     default NULL
            );
    PROCEDURE user_last_comment
            (
                    p_organization_pk               in organization.organization_pk%type    default NULL,
                    p_archive                       in number                               default NULL
            );
    PROCEDURE user_album_archive
            (
                    p_organization_pk       IN organization.organization_pk%TYPE            DEFAULT NULL,
                    p_language_pk           IN la.language_pk%TYPE                          DEFAULT NULL,
                    p_service_pk            IN service.service_pk%TYPE                      DEFAULT NULL,
                    p_new_date              IN VARCHAR2                                     DEFAULT to_char(SYSDATE,get.txt('date_long')),
                    p_old_date              IN VARCHAR2                                     DEFAULT NULL,
                    p_count                 IN NUMBER                                       DEFAULT 1,
                    p_my_archive            IN NUMBER                                       DEFAULT NULL,
                    p_album_pk              IN album.album_pk%TYPE                          DEFAULT 0
            );

END;
/
show errors;


CREATE OR REPLACE PACKAGE BODY photo IS


---------------------------------------------------------
-- Name:        startup
-- Type:        procedure
-- What:        start procedure, to run the package
-- Author:      Frode Klevstul
-- Start date:  01.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

        list_albums;

END startup;





---------------------------------------------------------
-- Name:        list_albums
-- Type:        procedure
-- What:        procedure to list all albums
-- Author:      Frode Klevstul
-- Start date:  01.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE list_albums
IS
BEGIN
DECLARE

        CURSOR  select_all
                (
                        v_organization_pk       in organization.organization_pk%type
                ) IS
        SELECT  album_pk, create_date, activated
        FROM    album
        WHERE   organization_fk = v_organization_pk
        ORDER BY create_date desc;

        v_album_pk              album.album_pk%type             default NULL;
        v_title                 album_text.title%type           default NULL;
        v_when_period           album_text.when_period%type     default NULL;
        v_description           album_text.description%type     default NULL;
        v_create_date           album.create_date%type          default NULL;
        v_activated             album.activated%type            default NULL;

        v_no_pic                number                                  default NULL;
        v_organization_pk       organization.organization_pk%type       default NULL;
        v_language_pk           la.language_pk%type                     default NULL;

        v_check                 number                                  default NULL;

BEGIN
if ( login.timeout('photo.startup')>0 ) then

        v_organization_pk       := get.oid;
        v_language_pk           := get.lan;

        if ( get.u_type <> -2 ) then
                htp.p( get.msg(1,'not a client user') );
                return;
        end if;

        html.b_page(NULL, NULL, NULL, 2);
        html.b_box( get.txt('administrate_albums_for') ||' '||get.oname(v_organization_pk) , '100%', 'photo.list_albums');
        html.b_table;

        htp.p('<tr><td colspan="9">'||get.txt('admin_photo_desc')||'</td></tr>');
        htp.p('<tr><td colspan="9"><hr noshade></td></tr>');
        htp.p('<tr><td><b>'||get.txt('title')||':</b></td><td><b>'|| get.txt('when') ||':</b></td><td colspan="4"><b>'|| get.txt('description') ||':</b></td><td><b>'|| get.txt('no_pic') ||':</b></td><td><b>'|| get.txt('order_date') ||':</b></td><td><b>'|| get.txt('activated') ||':</b></td></tr>');


        open select_all(v_organization_pk);
        loop
                fetch select_all into v_album_pk, v_create_date, v_activated;
                exit when select_all%NOTFOUND;

                SELECT  count(*) INTO v_no_pic
                FROM    picture_in_album
                WHERE   album_fk = v_album_pk;

                v_title         := get.ap_text(v_album_pk, 'title');
                v_when_period   := get.ap_text(v_album_pk, 'when_period');
                v_description   := get.ap_text(v_album_pk, 'description');

                htp.p('<tr>
                        <td nowrap valign="top"><b>'|| v_title ||'</b>&nbsp;</td>
                        <td nowrap valign="top">'|| v_when_period ||'</td>
                        <td colspan="4" valign="top"><i>'|| v_description ||'</i></td>
                        <td align="right" valign="top"><b>'|| v_no_pic ||'</b></td>
                        <td valign="top"><i>'|| to_char(v_create_date, get.txt('date_year')) ||'</i></td>
                        <td valign="top">
                ');
                if (v_activated IS NULL) then
                        html.button_link( get.txt('no') ,'photo.activate?p_album_pk='||v_album_pk||'&p_activated='||1);
                else
                        html.button_link( get.txt('yes') ,'photo.activate?p_album_pk='||v_album_pk||'&p_activated='||0);
                end if;
                htp.p('</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>');
                html.button_link( get.txt('add_pic') ,'photo.upload?p_album_pk='||v_album_pk);
                htp.p('</td><td>');
                html.button_link( get.txt('list_pic') ,'photo.list_pictures?p_album_pk='||v_album_pk);
                htp.p('</td><td>');
                html.button_link( get.txt('update'),'photo.update_album?p_album_pk='||v_album_pk);
                htp.p('</td><td>');
                html.button_link( get.txt('delete') , 'photo.delete_album?p_album_pk='||v_album_pk);
                htp.p('</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>');
                htp.p('<tr><td colspan="9"><hr noshade width="100%"></td></tr>');
        end loop;
        close select_all;

        html.e_table;
        html.e_box;

        htp.p('<br><br><br>');
        /* kode til insert form */

        html.b_box( get.txt('add_new_album'), '100%', 'photo.list_albums(add_new)');
        html.b_form('photo.insert_album');
        html.b_table;
        htp.p('<tr><td colspan="2"><br>');
        htp.p('</td></tr>');
        htp.p('
                <input type="hidden" name="p_language_fk" value="'|| v_language_pk ||'">
                <tr><td>'|| get.txt('title') ||':</td><td><input type="Text" name="p_title" size="60" maxlength="250"></td></tr>
                <tr><td>'|| get.txt('when') ||':</td><td><input type="Text" name="p_when_period" size="60" maxlength="100"></td></tr>
                <tr><td>'|| get.txt('description') ||':</td><td><input type="Text" name="p_description" size="60" maxlength="500"></td></tr>
                <tr><td colspan="2" align="right">'); html.submit_link( get.txt('add_album') ); htp.p('
                </td></tr>
        ');

        html.e_table;
        html.e_form;
        html.e_box;

        html.e_page;

end if;
END;
END list_albums;




---------------------------------------------------------
-- Name:        insert_album
-- Type:        procedure
-- What:        inserts the entry into the database
-- Author:      Frode Klevstul
-- Start date:  01.08.2000
---------------------------------------------------------
PROCEDURE insert_album
                (
                        p_album_pk              in album.album_pk%type                  default NULL,
                        p_title                 in album_text.title%type                default NULL,
                        p_when_period           in album_text.when_period%type          default NULL,
                        p_description           in album_text.description%type          default NULL,
                        p_language_fk           in album_text.language_fk%type          default NULL,
                        p_publish_date          in varchar2                             default NULL,
                        p_publish_date2         in varchar2                             default NULL,
                        p_activated             in album.activated%type                 default NULL
                )
IS
BEGIN
DECLARE

        v_album_pk              album.album_pk%type                     default NULL;
        v_title                 album_text.title%type                   default NULL;
        v_when_period           album_text.when_period%type             default NULL;
        v_description           album_text.description%type             default NULL;

        v_check                 number                                  default NULL;

BEGIN
if ( login.timeout('photo.startup')>0 ) then


        if (p_album_pk is NULL) then
                SELECT album_seq.NEXTVAL
                INTO v_album_pk
                FROM dual;

                INSERT INTO album
                (album_pk, organization_fk, create_date, activated)
                VALUES (v_album_pk, get.oid, sysdate, 1);
                commit;

                INSERT INTO album_text
                (album_fk, language_fk, title, description, when_period)
                VALUES (v_album_pk, p_language_fk, p_title, p_description, p_when_period );
                commit;
                v_check := system.inc_pages( get.oid, p_language_fk, v_album_pk);
        else
        begin
                -- -----------------------------------------------------
                -- sjekker at album_pk tilh�rer denne organisasjonen
                -- -----------------------------------------------------
                SELECT  count(*) INTO v_check
                FROM    album
                WHERE   album_pk = p_album_pk
                AND     organization_fk = get.oid;

                if (v_check = 0) then
                        htp.p( get.msg(1, 'wrong values') );
                        return;
                end if;


                UPDATE album
                SET create_date = to_date( p_publish_date,get.txt('date_long')), activated = p_activated
                WHERE album_pk = p_album_pk;
                commit;

                -- dersom albumet finnes, men album teksten ikke finnes m�
                -- den opprettes
                SELECT  count(*) INTO v_check
                FROM    album_text
                WHERE   language_fk = p_language_fk
                AND     album_fk = p_album_pk;

                if ( v_check = 1 ) then
                        UPDATE album_text
                        SET title = p_title, description = p_description, when_period = p_when_period
                        WHERE album_fk = p_album_pk
                        AND language_fk = p_language_fk;
                        commit;
                else
                        INSERT INTO album_text
                        (album_fk, language_fk, title, description, when_period)
                        VALUES (p_album_pk, p_language_fk, p_title, p_description, p_when_period);
                        commit;
                end if;

                commit;
                v_check := system.inc_pages( get.oid, p_language_fk, p_album_pk);
        end;
        end if;

        list_albums;


end if;
END;
END insert_album;




---------------------------------------------------------
-- Name:        activate
-- Type:        procedure
-- What:        activates or deactivates an album
-- Author:      Frode Klevstul
-- Start date:  01.08.2000
---------------------------------------------------------
PROCEDURE activate
                (
                        p_album_pk              in album.album_pk%type                  default NULL,
                        p_activated             in album.activated%type                 default NULL
                )
IS
BEGIN
DECLARE

        v_check         number                  default NULL;
        v_activated     album.activated%type    default NULL;

BEGIN
if ( login.timeout('photo.startup')>0 ) then

        -- -----------------------------------------------------
        -- sjekker at album_pk tilh�rer denne organisasjonen
        -- -----------------------------------------------------
        SELECT  count(*) INTO v_check
        FROM    album
        WHERE   album_pk = p_album_pk
        AND     organization_fk = get.oid;

        if (v_check = 0) then
                htp.p( get.msg(1, 'wrong values') );
                return;
        end if;

        if (p_activated = 0) then
                v_activated := NULL;
        else
                v_activated := p_activated;
        end if;

        UPDATE  album
        SET     activated = v_activated
        WHERE   album_pk = p_album_pk
        AND     organization_fk = get.oid;
        commit;

        v_check := system.inc_pages( get.oid, get.lan, p_album_pk);

        list_albums;

end if;
END;
END activate;




---------------------------------------------------------
-- Name:        delete_album
-- Type:        procedure
-- What:        confirm a deletion of an entry in the database
-- Author:      Frode Klevstul
-- Start date:  01.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE delete_album
                (
                        p_album_pk              in album.album_pk%type  default NULL
                )
IS
BEGIN
DECLARE

        v_title         album_text.title%type           default NULL;

BEGIN
if ( login.timeout('photo.startup')>0 ) then

        v_title := get.ap_text(p_album_pk, 'title');

        html.b_page(NULL, NULL, NULL, 2);
        html.b_box( get.txt('delete_album') );
        html.b_form('photo.delete_album_2');
        html.b_table;

        htp.p('
                <input type="hidden" name="p_album_pk" value="'|| p_album_pk ||'">
                <tr><td colspan="2">'|| get.txt('Confirm_deletion_of_the_album_and_all_pictures_in') ||' <b>'|| v_title ||'</b>?</td></tr>
                <tr><td colspan="2"><input type="submit" name="p_confirm" value="'|| get.txt('yes_delete_album') ||'"></td></tr>
                <tr><td colspan="2">'); html.back( get.txt('no_dont_delete_album') ); htp.p('</td></tr>
                </tr>
        ');

        html.e_table;
        html.e_form;
        html.e_box;
        html.e_page;

end if;

EXCEPTION
        WHEN NO_DATA_FOUND
        THEN list_albums;
END;
END delete_album;


---------------------------------------------------------
-- Name:        delete_album_2
-- Type:        procedure
-- What:        deletes the entry from the database
-- Author:      Frode Klevstul
-- Start date:  01.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE delete_album_2
                (
                        p_album_pk      in album.album_pk%type                  default NULL,
                        p_confirm       in varchar2                             default NULL
                )
IS
BEGIN
DECLARE

        CURSOR  select_all
                (
                        v_album_pk      in album.album_pk%type
                ) IS
        SELECT  picture_fk
        FROM    picture_in_album
        WHERE   album_fk = v_album_pk;

        v_check         number                  default NULL;
        v_picture_pk    picture.picture_pk%type default NULL;

BEGIN
if ( login.timeout('photo.startup')>0 ) then

        if (p_confirm IS NOT NULL) then

                SELECT  count(*) INTO v_check
                FROM    album
                WHERE   album_pk = p_album_pk
                AND     organization_fk = get.oid;

                -- sjekker at org. har et album med denne pk'en
                if ( v_check = 1 ) then

                        -- sletter bilder fra OS'et
                        open select_all(p_album_pk);
                        loop
                                fetch select_all into v_picture_pk;
                                exit when select_all%NOTFOUND;

                        end loop;
                        close select_all;

                        -- sletter fra databasen

                        DELETE FROM picture
                        WHERE picture_pk IN (
                                SELECT  picture_fk
                                FROM    picture_in_album
                                WHERE   album_fk = p_album_pk
                                );
                        commit;

                        DELETE FROM album
                        WHERE album_pk = p_album_pk;
                        commit;

                end if;

                v_check := system.inc_pages( get.oid, get.lan, p_album_pk);

        end if;
        list_albums;


end if;
END;
END delete_album_2;



---------------------------------------------------------
-- Name:        update_album
-- Type:        procedure
-- What:        updates an entry
-- Author:      Frode Klevstul
-- Start date:  01.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE update_album
                (
                        p_album_pk              in album.album_pk%type  default NULL
                )
IS
BEGIN
DECLARE

        v_title                 album_text.title%type           default NULL;
        v_when_period           album_text.when_period%type     default NULL;
        v_description           album_text.description%type     default NULL;
        v_create_date           album.create_date%type          default NULL;
        v_activated             album.activated%type            default NULL;

        v_language_pk           la.language_pk%type             default NULL;
        v_check                 number                          default NULL;

BEGIN
if ( login.timeout('photo.startup')>0 ) then

        -- --------------------
        -- henter ut tekster
        -- --------------------
        v_language_pk := get.lan;

        SELECT  create_date, activated INTO v_create_date, v_activated
        FROM    album
        WHERE   album_pk = p_album_pk;

        v_title         := get.ap_text(p_album_pk, 'title');
        v_when_period   := get.ap_text(p_album_pk, 'when_period');
        v_description   := get.ap_text(p_album_pk, 'description');

        -- ----------------------
        -- skriver ut HTML kode
        -- ----------------------
        html.b_page(NULL, NULL, NULL, 2);
        html.b_box( get.txt('update_album') ||' ('|| get.lname ||')', '100%', 'photo.update_album');
        html.b_form('photo.insert_album');
        html.b_table;

        htp.p('
                <input type="hidden" name="p_album_pk" value="'||p_album_pk||'">
                <input type="hidden" name="p_language_fk" value="'|| v_language_pk ||'">
                <tr><td>'|| get.txt('title') ||':</td><td><input type="Text" name="p_title" size="60" maxlength="250" value="'||v_title||'"></td></tr>
                <tr><td>'|| get.txt('when') ||':</td><td><input type="Text" name="p_when_period" size="60" maxlength="100" value="'||v_when_period||'"></td></tr>
                <tr><td>'|| get.txt('description') ||':</td><td><input type="Text" name="p_description" size="60" maxlength="500" value="'||v_description||'"></td></tr>
                <tr><td>'|| get.txt('activated') ||':   </td><td>
                                                                <select name="p_activated">');

                                                                        if (v_activated IS NULL) then
                                                                                htp.p('<option value="1">'|| get.txt('yes') ||'</option><option value="'||NULL||'" selected>'|| get.txt('no') ||'</option>');
                                                                        else
                                                                                htp.p('<option value="1" selected>'|| get.txt('yes') ||'</option><option value="'||NULL||'">'|| get.txt('no') ||'</option>');
                                                                        end if;

                                                                htp.p('</select>
                                                        </td></tr>
                <tr><td>'|| get.txt('create_date') ||':</td><td>');

                htp.p('<input type="hidden" name="p_publish_date" value="'||to_char(v_create_date, get.txt('date_long'))||'">');
                htp.p('<input type="text" size="25" name="p_publish_date2" value="'||to_char(v_create_date, get.txt('date_long'))||'"
                onSelect=javascript:openWin(''date_time.startup?p_command=date&p_start_date='|| to_char(v_create_date,get.txt('date_full'))||''',300,200)
                onClick=javascript:openWin(''date_time.startup?p_command=date&p_start_date='|| to_char(v_create_date,get.txt('date_full'))||''',300,200)>');
                htp.p( html.popup( get.txt('choose_date'),'date_time.startup?p_command=date&p_start_date='|| to_char(v_create_date,get.txt('date_full')),'300', '200') );

                htp.p('</td></tr><tr><td colspan="2" align="right">');
                html.submit_link( get.txt('update') );
                htp.p('</td></tr>');

                htp.p('<tr><td colspan="2" align="right">');
                html.button_link( get.txt('go_back'), 'photo.list_albums');
                htp.p('</td></tr>');

        html.e_table;
        html.e_form;
        html.e_box;
        html.e_page;


end if;
END;
END update_album;





---------------------------------------------------------
-- Name:        upload
-- Type:        procedure
-- What:        html form for upload of picture
-- Author:      Frode Klevstul
-- Start date:  02.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE upload
                (
                        p_album_pk              in album.album_pk%type  default NULL
                )
IS
BEGIN
DECLARE

        v_title                 album_text.title%type           default NULL;

        v_language_pk           la.language_pk%type             default NULL;
        v_check                 number                          default NULL;

BEGIN
if ( login.timeout('photo.startup')>0 ) then

        v_title         := get.ap_text(p_album_pk, 'title');

        -- ---------------------
        -- skriver ut HTML kode
        -- ---------------------
        html.b_page(NULL, NULL, NULL, 2);
        html.b_box( get.txt('upload_picture_in') ||' '''|| v_title||'''', '100%', 'photo.upload');
        html.b_table;

        htp.p('
                <form name="form" action="'|| get.value('upload_script') ||'" method="post" enctype="multipart/form-data">
                <input type="hidden" name="p_type" value="picture">
                <input type="hidden" name="p_picture_id" value="'|| get.oid ||'-'||p_album_pk||'">
                <tr><td colspan="2">'|| get.txt('add_picture_step_1') ||'</td></tr>
                <tr><td colspan="2"> &nbsp; </td></tr>
                <tr>
                        <td>'||get.txt('picture')||':</td><td><input type="file" maxlength=100 name="p_file" size=60></td>
                </tr>
        ');
        htp.p('<tr><td colspan="2" align="right">');
        html.submit_link( get.txt('upload_picture') );
        htp.p('</td></tr>');

        htp.p('<tr><td colspan="2" align="right">');
        html.button_link( get.txt('go_back'), 'photo.list_albums');
        htp.p('</td></tr>');


        html.e_table;
        html.e_form;
        html.e_box;
        html.e_page;


end if;
END;
END upload;




---------------------------------------------------------
-- Name:        upload2
-- Type:        procedure
-- What:        upload, part2
-- Author:      Frode Klevstul
-- Start date:  02.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE upload2
        (
                p_album_pk              in album.album_pk%type          default NULL,
                p_path                  in picture.path%type            default NULL,
                p_full_path             in picture.path%type            default NULL,
                p_file_size             in picture.file_size%type       default NULL,
                p_width                 in picture.width%type           default NULL,
                p_height                in picture.height%type          default NULL
        )
IS
BEGIN
DECLARE

        v_title                 album_text.title%type           default NULL;

        v_language_pk           la.language_pk%type             default NULL;
        v_check                 number                          default NULL;

BEGIN
if ( login.timeout('photo.startup')>0 ) then

        -- -----------------------------------------------------
        -- sjekker at album_pk tilh�rer denne organisasjonen
        -- -----------------------------------------------------
        SELECT  count(*) INTO v_check
        FROM    album
        WHERE   album_pk = p_album_pk
        AND     organization_fk = get.oid;

        if (v_check = 0) then
                htp.p( get.msg(1, 'wrong values') );
                return;
        end if;


        v_title         := get.ap_text(p_album_pk, 'title');


        -- ---------------------
        -- starter HTML koden
        -- ---------------------
        html.b_page(NULL, NULL, NULL, 2);
        html.b_box( get.txt('upload_picture_in') ||' '''|| v_title ||'''', '100%', 'photo.upload2');
        html.b_form('photo.upload3');
        html.b_table;

        htp.p('
                <input type="hidden" name="p_path" value="'|| p_full_path ||'">
                <input type="hidden" name="p_album_pk" value="'|| p_album_pk ||'">
                <input type="hidden" name="p_file_size" value="'|| p_file_size ||'">
                <input type="hidden" name="p_height" value="'|| p_height ||'">
                <input type="hidden" name="p_width" value="'|| p_width ||'">

                <tr><td colspan="2">'|| get.txt('add_picture_step_2') ||'</td></tr>
                <tr><td colspan="2"><hr noshade></td></tr>
                <tr><td colspan="2"><img src="'|| get.value('upload_dir') ||'/'|| p_path ||'.jpg" alt=""></td></tr>
                <tr><td colspan="2"><hr noshade></td></tr>

                <tr>
                        <td>'|| get.txt('when_taken') ||':</td><td>');

                        htp.p('<input type="hidden" name="p_publish_date" value="">');
                        htp.p('<input type="text" size="25" name="p_publish_date2" value="'|| get.txt('choose_date') ||'"
                        onSelect=javascript:openWin(''date_time.startup?p_command=date'',300,200)
                        onClick=javascript:openWin(''date_time.startup?p_command=date'',300,200)>');
                        htp.p( html.popup( get.txt('change_picture_date'),'date_time.startup?p_command=date','300', '200') );

                        htp.p('</td>

                </tr>
                <tr>
                        <td>'|| get.txt('photographer') ||':</td><td><input type="text" maxlength="150" size="50" name="p_photographer"></td>
                </tr>
                <tr>
                        <td>'|| get.txt('whom') ||':</td><td><input type="text" maxlength="150" size="50" name="p_whom"></td>
                </tr>
                <tr>
                        <td>'|| get.txt('what') ||':</td><td><input type="text" maxlength="400" size="60" name="p_what"></td>
                </tr>
                <tr>
                        <td>'|| get.txt('comment') ||'</td>
                        <td>
                                <select name="p_activated">
                                        <option value="1" selected>'||get.txt('on')||'</option>
                                        <option value="">'||get.txt('off')||'</option>
                                </select>
                        </td>
                </tr>
        ');
        htp.p('<tr><td colspan="2" align="right">'); html.submit_link( get.txt('add_pic_to_album') ); htp.p('</td></tr>');

        htp.p('<tr><td colspan="2" align="right">');
        html.button_link( get.txt('go_back_without_uploading'), 'photo.upload?p_album_pk='||p_album_pk);
        htp.p('</td></tr>');


        html.e_table;
        html.e_form;
        html.e_box;
        html.e_page;


end if;
END;
END upload2;






---------------------------------------------------------
-- Name:        upload3
-- Type:        procedure
-- What:        upload, part3
-- Author:      Frode Klevstul
-- Start date:  02.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE upload3
        (
                p_album_pk              in album.album_pk%type          default NULL,
                p_path                  in picture.path%type            default NULL,
                p_publish_date          in varchar2                     default NULL,
                p_publish_date2         in varchar2                     default NULL,
                p_whom                  in picture.whom%type            default NULL,
                p_photographer          in picture.photographer%type    default NULL,
                p_what                  in picture_text.what%type       default NULL,
                p_activated                             in picture.activated%type               default NULL,
                p_file_size                             in picture.file_size%type               default NULL,
                p_width                                 in picture.width%type                   default NULL,
                p_height                                in picture.height%type                  default NULL
        )
IS
BEGIN
DECLARE

        v_check                 number                          default NULL;
        v_picture_pk            picture.picture_pk%type         default NULL;
        v_user_pk               usr.user_pk%type                default NULL;
        v_language_pk           la.language_pk%type             default NULL;
        v_whom                  picture.whom%type                               default NULL;
        v_photographer          picture.photographer%type               default NULL;
        v_what                  picture_text.what%type                  default NULL;

BEGIN
if ( login.timeout('photo.startup')>0 ) then

        -- beandler tekst fra bruker
        v_whom                  := p_whom;
        v_photographer  := p_photographer;
        v_what                  := p_what;
        owa_pattern.change(v_whom, '"', '''');
        owa_pattern.change(v_photographer, '"', '''');
        owa_pattern.change(v_what, '"', '''');

    -- -----------------------------------------------------
    -- sjekker at album_pk tilh�rer denne organisasjonen
    -- -----------------------------------------------------
    SELECT  count(*) INTO v_check
    FROM    album
    WHERE   album_pk = p_album_pk
    AND     organization_fk = get.oid;

    if (v_check = 0) then
            htp.p( get.msg(1, 'wrong values') );
            return;
    end if;

    v_user_pk       := get.uid;
    v_language_pk   := get.lan;

    SELECT picture_seq.NEXTVAL
    INTO v_picture_pk
    FROM dual;

    INSERT INTO picture
    (picture_pk, user_fk, path, upload_date, modified_date, when_taken, whom, photographer, activated, width, height, file_size)
    VALUES(v_picture_pk, v_user_pk, p_path, sysdate, sysdate, to_date(p_publish_date,get.txt('date_long')), v_whom, v_photographer, p_activated, p_width, p_height, p_file_size);
    commit;

    INSERT INTO picture_text
    (picture_fk, language_fk, what)
    values(v_picture_pk, v_language_pk, v_what);
    commit;

    INSERT INTO picture_in_album
    (picture_fk, album_fk)
    VALUES(v_picture_pk, p_album_pk);
    commit;

        v_check := system.inc_pages( get.oid, v_language_pk, p_album_pk);

    html.jump_to( get.value('upload_thumb_and_move') ||'?'||p_path );

end if;
END;
END upload3;




---------------------------------------------------------
-- Name:        upload4
-- Type:        procedure
-- What:        upload, part4
-- Author:      Frode Klevstul
-- Start date:  02.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE upload4
                (
                        p_path                  in picture.path%type            default NULL
                )
IS
BEGIN
DECLARE

        v_album_pk              album.album_pk%type             default NULL;
        v_picture_pk            picture.picture_pk%type         default NULL;

BEGIN
if ( login.timeout('photo.startup')>0 ) then

        SELECT  picture_pk INTO v_picture_pk
        FROM    picture
        WHERE   path = p_path;

        SELECT  album_fk INTO v_album_pk
        FROM    picture_in_album
        WHERE   picture_fk = v_picture_pk;

        photo.upload(v_album_pk);

end if;
EXCEPTION
        WHEN NO_DATA_FOUND
                THEN upload_error('wrong_picture_inserting');
END;
END upload4;




---------------------------------------------------------
-- Name:        upload_error
-- Type:        procedure
-- What:        writes out error message
-- Author:      Frode Klevstul
-- Start date:  02.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE upload_error
                (
                        p_error_txt             in varchar2             default NULL,
                        p_album_pk              in album.album_pk%type  default NULL
                )
IS
BEGIN
if ( login.timeout('photo.startup')>0 ) then

        html.b_page(NULL, NULL, NULL, 2);
        html.b_box( get.txt('upload_error') );
        html.b_table;

        htp.p('
                <tr><td>'|| get.txt(p_error_txt) ||'</td></tr>
        ');

        htp.p('<tr><tdcolspan="2"> &nbsp; </td></tr>');
        htp.p('<tr><td align="right">');
        if (p_album_pk IS NOT NULL) then
                html.button_link( get.txt('try_upload_again'), 'photo.upload?p_album_pk='||p_album_pk);
        end if;
        htp.p('</td></tr>');

        html.e_table;
        html.e_form;
        html.e_box;
        html.e_page;


end if;
END upload_error;




---------------------------------------------------------
-- Name:        list_pictures
-- Type:        procedure
-- What:        lists out pictures in one album
-- Author:      Frode Klevstul
-- Start date:  03.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE list_pictures
                (
                        p_album_pk              in album.album_pk%type  default NULL
                )
IS
BEGIN
DECLARE

        CURSOR  select_all
                (
                        v_album_pk in album.album_pk%type
                ) IS
        SELECT  picture_pk, path, when_taken, whom, photographer, activated
        FROM    picture, picture_in_album
        WHERE   picture_fk = picture_pk
        AND     album_fk = v_album_pk
        ORDER BY when_taken DESC;

        v_picture_pk            picture.picture_pk%type         default NULL;
        v_path                  picture.path%type               default NULL;
        v_when_taken            picture.when_taken%type         default NULL;
        v_whom                  picture.whom%type               default NULL;
        v_photographer          picture.photographer%type       default NULL;
        v_activated             picture.activated%type          default NULL;
        v_what                  picture_text.what%type          default NULL;

        v_title                 album_text.title%type           default NULL;
        v_when_period           album_text.when_period%type     default NULL;
        v_description           album_text.description%type     default NULL;

        v_language_pk           la.language_pk%type             default NULL;
        v_check                 number                          default NULL;



BEGIN
if ( login.timeout('photo.startup')>0 ) then

        -- -----------------------------------------------------
        -- sjekker at album_pk tilh�rer denne organisasjonen
        -- -----------------------------------------------------
        SELECT  count(*) INTO v_check
        FROM    album
        WHERE   album_pk = p_album_pk
        AND     organization_fk = get.oid;

        if (v_check = 0) then
                htp.p( get.msg(1, 'wrong values') );
                return;
        end if;

        v_title         := get.ap_text(p_album_pk, 'title');
        v_when_period   := get.ap_text(p_album_pk, 'when_period');
        v_description   := get.ap_text(p_album_pk, 'description');

        -- ---------------------
        -- skriver ut HTML kode
        -- ---------------------
        html.b_page(NULL, NULL, NULL, 2);
        html.b_box( get.txt('pictures_in_album'), '100%', 'photo.list_pictures');
        html.b_table;

        htp.p('<tr><td colspan="5" align="center"><b>'|| v_title ||'</b></td></tr>');
        htp.p('<tr><td colspan="5" align="center"><i>'|| v_when_period ||'</i></td></tr>');
        htp.p('<tr><td colspan="5" align="center">'|| v_description ||'</td></tr>');
        htp.p('<tr><td colspan="5"><hr noshade></td></tr>');

        open select_all(p_album_pk);
        loop
                fetch select_all into v_picture_pk, v_path, v_when_taken, v_whom, v_photographer, v_activated;
                exit when select_all%NOTFOUND;

                v_what  := get.ap_text(v_picture_pk, 'what');

                htp.p('<tr><td rowspan="6"><a href="photo.show_picture?p_picture_pk='||v_picture_pk||'"><img border="0" src="'||get.value('photo_archive')||'/'|| v_path ||'_thumb.jpg" alt=""></a></td></tr>');
                htp.p('
                        <tr>
                                <td>'|| get.txt('when') ||': </td>
                                <td colspan="3" width="70%"><i>'|| to_char(v_when_taken, get.txt('date_year') ) ||'&nbsp;</i></td>
                        </tr>
                        <tr>
                                <td> '|| get.txt('whom') ||': </td>
                                <td colspan="3"><i>'|| v_whom ||'&nbsp;</i></td>
                        </tr>
                        <tr>
                                <td> '|| get.txt('photographer') ||': </td>
                                <td colspan="3"><i>'|| v_photographer ||'&nbsp;</i></td>
                        </tr>
                        <tr>
                                <td> '|| get.txt('what') ||': </td>
                                <td colspan="3"><i>'||v_what||'&nbsp;</i></td>
                        </tr>
                        <tr>
                                <td> '|| get.txt('comment') ||': </td>
                                <td colspan="3">
                ');

                if (v_activated = 1) then
                        htp.p( '<i>'||get.txt('on')||'</i>' );
                else
                        htp.p( '<i>'||get.txt('off')||'</i>' );
                end if;

                htp.p('         </td>
                        </tr>
                ');
                htp.p('<tr><td>&nbsp;</td><td>&nbsp;</td><td align="right">');
                html.button_link(get.txt('show_picture'), 'photo.show_picture?p_picture_pk='||v_picture_pk);
                htp.p('</td><td align="center">');
                html.button_link(get.txt('update'), 'photo.update_picture?p_picture_pk='||v_picture_pk);
                htp.p('</td><td>');
                html.button_link(get.txt('delete'), 'photo.delete_picture?p_picture_pk='||v_picture_pk);
                htp.p('</td></tr>');

                htp.p('<tr><td colspan="5"><hr noshade></td></tr>');
        end loop;
        close select_all;

        htp.p('<tr><td colspan="5" align="right">');
        html.button_link(get.txt('back_to_my_albums'), 'photo.list_albums');
        htp.p('</td></tr>');

        html.e_table;
        html.e_form;
        html.e_box;
        html.e_page;


end if;
END;
END list_pictures;





---------------------------------------------------------
-- Name:        show_picture
-- Type:        procedure
-- What:        show one picture (big version)
-- Author:      Frode Klevstul
-- Start date:  03.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE show_picture
                (
                        p_picture_pk            in picture.picture_pk%type      default NULL
                )
IS
BEGIN
DECLARE

        v_path                  picture.path%type               default NULL;
        v_when_taken            picture.when_taken%type         default NULL;
        v_whom                  picture.whom%type               default NULL;
        v_photographer          picture.photographer%type       default NULL;
        v_activated             picture.activated%type          default NULL;
        v_what                  picture_text.what%type                  default NULL;

        v_title                 album_text.title%type           default NULL;
        v_language_pk           la.language_pk%type             default NULL;
        v_check                 number                          default NULL;

        v_album_pk              album.album_pk%type             default NULL;

BEGIN
if ( login.timeout('photo.startup')>0 ) then

        -- -----------------------------------------------------
        -- sjekker at bildet tilh�rer denne organisasjonen
        -- -----------------------------------------------------
        SELECT  count(*) INTO v_check
        FROM    album, picture_in_album
        WHERE   album_pk = album_fk
        AND     organization_fk = get.oid
        AND     picture_fk = p_picture_pk;

        if (v_check = 0) then
                htp.p( get.msg(1, 'wrong values') );
                return;
        end if;

        -- -----------------------------
        -- henter ut verdier til bildet
        -- -----------------------------
        SELECT  path, when_taken, whom, photographer, activated
        INTO    v_path, v_when_taken, v_whom, v_photographer, v_activated
        FROM    picture
        WHERE   picture_pk = p_picture_pk;

        SELECT  album_fk INTO v_album_pk
        FROM    picture_in_album
        WHERE   picture_fk = p_picture_pk;

        v_title         := get.ap_text(v_album_pk, 'title');
        v_what          := get.ap_text(p_picture_pk, 'what');

        -- ---------------------
        -- skriver ut HTML kode
        -- ---------------------
        html.b_page(NULL, NULL, NULL, 2);
        html.b_box( v_title );
        html.b_table;

        htp.p('<tr><td colspan="2"><hr noshade></td></tr>');
        htp.p('<tr><td colspan="2"><img src="'||get.value('photo_archive') ||'/'||v_path||'.jpg" alt=""></td></tr>');
        htp.p('<tr><td colspan="2"><hr noshade></td></tr>');

        htp.p('<tr><td width="20%">'|| get.txt('when') ||':</td><td>'|| to_char(v_when_taken, get.txt('date_year') ) ||'</td></tr>');
        htp.p('<tr><td>'|| get.txt('whom') ||':</td><td>'|| v_whom ||'</td></tr>');
        htp.p('<tr><td>'|| get.txt('photographer') ||':</td><td>'|| v_photographer ||'</td>');
        htp.p('<tr><td>'|| get.txt('what') ||':</td><td>'|| v_what ||'</td></tr>');
        htp.p('<tr><td colspan="2" align="right">');
        html.button_link( get.txt('go_back'), 'photo.list_pictures?p_album_pk='||v_album_pk);
        htp.p('</td></tr>');


        html.e_table;
        html.e_box;
        html.e_page;


end if;
END;
END show_picture;





---------------------------------------------------------
-- Name:        update_picture
-- Type:        procedure
-- What:        update picture information
-- Author:      Frode Klevstul
-- Start date:  09.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE update_picture
                (
                        p_picture_pk            in picture.picture_pk%type      default NULL
                )
IS
BEGIN
DECLARE

        v_path                  picture.path%type               default NULL;
        v_when_taken            picture.when_taken%type         default NULL;
        v_when_taken_2          picture.when_taken%type         default NULL;
        v_whom                  picture.whom%type               default NULL;
        v_photographer          picture.photographer%type       default NULL;
        v_what                  picture_text.what%type          default NULL;
        v_activated                             picture.activated%type          default NULL;

        v_title                 album_text.title%type           default NULL;
        v_check                 number                          default NULL;

        v_album_pk              album.album_pk%type             default NULL;


BEGIN
if ( login.timeout('photo.startup')>0 ) then

        -- -----------------------------------------------------
        -- sjekker at bildet tilh�rer denne organisasjonen
        -- -----------------------------------------------------
        SELECT  count(*) INTO v_check
        FROM    album, picture_in_album
        WHERE   album_pk = album_fk
        AND     organization_fk = get.oid
        AND     picture_fk = p_picture_pk;

        if (v_check = 0) then
                htp.p( get.msg(1, 'wrong values') );
                return;
        end if;

        -- -----------------------------
        -- henter ut verdier til bildet
        -- -----------------------------
        SELECT  path, when_taken, whom, photographer, activated
        INTO    v_path, v_when_taken, v_whom, v_photographer, v_activated
        FROM    picture
        WHERE   picture_pk = p_picture_pk;

        SELECT  album_fk INTO v_album_pk
        FROM    picture_in_album
        WHERE   picture_fk = p_picture_pk;

        v_title         := get.ap_text(v_album_pk, 'title');
        v_what          := get.ap_text(p_picture_pk, 'what');

        -- ---------------------
        -- skriver ut HTML kode
        -- ---------------------
        html.b_page(NULL, NULL, NULL, 2);
        html.b_box( v_title, '100%', 'photo.update_picture' );
        html.b_form('photo.update_pic_action');
        html.b_table;

        if (v_when_taken IS NULL) then
                v_when_taken_2 := sysdate;
        else
                v_when_taken_2 := v_when_taken;
        end if;

        htp.p('
                <input type="hidden" name="p_picture_pk" value="'||p_picture_pk||'">
                <tr><td colspan="2">'|| get.txt('update_picture') ||'</td></tr>
                <tr><td colspan="2"><hr noshade></td></tr>
                <tr><td colspan="2"><img src="'||
                get.value('photo_archive') ||'/'|| v_path ||'_thumb.jpg" alt=""></td></tr>
                <tr><td colspan="2"><hr noshade></td></tr>

                <tr>
                        <td width="15%">'|| get.txt('when_taken') ||':</td><td>');

                        htp.p('<input type="hidden" name="p_publish_date" value="'|| to_char(v_when_taken, get.txt('date_long')) ||'">');
                        htp.p('<input type="text" size="25" name="p_publish_date2" value="'|| to_char(v_when_taken, get.txt('date_long')) ||'"
                        onSelect=JavaScript:openWin(''date_time.startup?p_command=date&p_start_date='|| to_char(v_when_taken_2,get.txt('date_full'))||''',300,200)
                        onClick=JavaScript:openWin(''date_time.startup?p_command=date&p_start_date='|| to_char(v_when_taken_2,get.txt('date_full'))||''',300,200)>');
                        htp.p( html.popup( get.txt('change_picture_date'),'date_time.startup?p_command=date&p_start_date='|| to_char(v_when_taken,get.txt('date_full')),'300', '200') );

                        htp.p('</td>

                </tr>
                <tr>
                        <td>'|| get.txt('photographer') ||':</td><td><input type="text" maxlength="150" size="50" name="p_photographer" value="'||v_photographer||'"></td>
                </tr>
                <tr>
                        <td>'|| get.txt('whom') ||':</td><td><input type="text" maxlength="150" size="50" name="p_whom" value="'||v_whom||'"></td>
                </tr>
                <tr>
                        <td>'|| get.txt('what') ||':</td><td><input type="text" maxlength="400" size="60" name="p_what" value="'||v_what||'"></td>
                </tr>
                <tr>
                        <td>'|| get.txt('comment') ||':</td><td>
                                <select name="p_activated">
                                        <option value="1">'||get.txt('on')||'</option>
                                        <option value="" '); if (v_activated IS NULL) then htp.p('selected'); end if; htp.p('>'||get.txt('off')||'</option>
                                </select>
                        </td>
                </tr>

        ');
        htp.p('<tr><td align="right" colspan="2">');
        html.submit_link( get.txt('update') );
        htp.p('</td></tr><tr><td align="right" colspan="2">');
        html.button_link( get.txt('go_back'), 'photo.list_pictures?p_album_pk='||v_album_pk);
        htp.p('</td></tr>');


        html.e_table;
        html.e_form;
        html.e_box;
        html.e_page;


end if;
END;
END update_picture;





---------------------------------------------------------
-- Name:        update_pic_action
-- Type:        procedure
-- What:        update picture information in database
-- Author:      Frode Klevstul
-- Start date:  09.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE update_pic_action
                (
                        p_picture_pk            in picture.picture_pk%type      default NULL,
                        p_publish_date          in varchar2                     default NULL,
                        p_publish_date2         in varchar2                     default NULL,
                        p_whom                  in picture.whom%type            default NULL,
                        p_photographer          in picture.photographer%type    default NULL,
                        p_what                  in picture_text.what%type       default NULL,
                        p_activated             in picture.activated%type       default NULL
                )
IS
BEGIN
DECLARE

    v_album_pk          album.album_pk%type                     default NULL;
    v_check             number                                          default NULL;
    v_language_pk       la.language_pk%type                     default NULL;
    v_whom                              picture.whom%type                       default NULL;
    v_photographer              picture.photographer%type       default NULL;
    v_what                              picture_text.what%type          default NULL;

BEGIN
if ( login.timeout('photo.startup')>0 ) then

        -- beandler tekst fra bruker
        v_whom                  := p_whom;
        v_photographer  := p_photographer;
        v_what                  := p_what;
        owa_pattern.change(v_whom, '"', '''');
        owa_pattern.change(v_photographer, '"', '''');
        owa_pattern.change(v_what, '"', '''');


    -- -----------------------------------------------------
    -- sjekker at bildet tilh�rer denne organisasjonen
    -- -----------------------------------------------------
    SELECT  count(*) INTO v_check
    FROM    album, picture_in_album
    WHERE   album_pk = album_fk
    AND     organization_fk = get.oid
    AND     picture_fk = p_picture_pk;

    if (v_check = 0) then
            htp.p( get.msg(1, 'wrong values') );
            return;
    end if;

    v_language_pk := get.lan;

    UPDATE  picture
    SET     when_taken = to_date( p_publish_date, get.txt('date_long')),
            whom = v_whom,
            photographer = v_photographer,
            modified_date = sysdate,
            activated = p_activated
    WHERE   picture_pk = p_picture_pk;


    SELECT  count(*) INTO v_check
    FROM    picture_text
    WHERE   picture_fk = p_picture_pk
    AND     language_fk = v_language_pk;

    if ( v_check = 0 ) then
            INSERT INTO picture_text
            (picture_fk, language_fk, what)
            VALUES (p_picture_pk, v_language_pk, v_what);
            commit;
    else
            UPDATE  picture_text
            SET     what = v_what
            WHERE   picture_fk = p_picture_pk
            AND     language_fk = v_language_pk;
            commit;
    end if;

    SELECT  album_fk INTO v_album_pk
    FROM    picture_in_album
    WHERE   picture_fk = p_picture_pk;

        v_check := system.inc_pages( get.oid, v_language_pk, v_album_pk);

    photo.list_pictures(v_album_pk);

end if;
END;
END update_pic_action;






---------------------------------------------------------
-- Name:        delete_picture
-- Type:        procedure
-- What:        deletes picture from db
-- Author:      Frode Klevstul
-- Start date:  09.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE delete_picture
                (
                        p_picture_pk            in picture.picture_pk%type      default NULL
                )
IS
BEGIN
DECLARE

        v_path                  picture.path%type               default NULL;

        v_title                 album_text.title%type           default NULL;
        v_check                 number                          default NULL;

        v_album_pk              album.album_pk%type             default NULL;

BEGIN
if ( login.timeout('photo.startup')>0 ) then

        -- -----------------------------------------------------
        -- sjekker at bildet tilh�rer denne organisasjonen
        -- -----------------------------------------------------
        SELECT  count(*) INTO v_check
        FROM    album, picture_in_album
        WHERE   album_pk = album_fk
        AND     organization_fk = get.oid
        AND     picture_fk = p_picture_pk;

        if (v_check = 0) then
                htp.p( get.msg(1, 'wrong values') );
                return;
        end if;

        -- -----------------------------
        -- henter ut verdier til bildet
        -- -----------------------------
        SELECT  path
        INTO    v_path
        FROM    picture
        WHERE   picture_pk = p_picture_pk;

        SELECT  album_fk INTO v_album_pk
        FROM    picture_in_album
        WHERE   picture_fk = p_picture_pk;

        v_title         := get.ap_text(v_album_pk, 'title');

        -- ---------------------
        -- skriver ut HTML kode
        -- ---------------------
        html.b_page(NULL, NULL, NULL, 2);
        html.b_box( v_title, '100%', 'photo.delete_picture' );
        html.b_form('photo.delete_picture_2');
        html.b_table;

        htp.p('
                <input type="hidden" name="p_picture_pk" value="'||p_picture_pk||'">
                <tr><td colspan="2">'|| get.txt('delete_picture') ||'</td></tr>
                <tr><td colspan="2"><hr noshade></td></tr>
                <tr><td colspan="2"><img src="'||
                get.value('photo_archive') ||'/'|| v_path ||'_thumb.jpg" alt=""></td></tr>
                <tr><td colspan="2"><hr noshade></td></tr>

                <tr><td colspan="2">'|| get.txt('are_you_sure_to_delete_picture') ||'</td></tr>
                <tr>
                        <td colspan="2">
                                <input type="submit" name="p_confirm" value="'|| get.txt('delete_the_picture') ||'">
                        </td>
                </tr>
                <tr>
                        <td colspan="2">
                                '); html.back( get.txt('dont_delete_the_picture') ); htp.p('
                        </td>
                </tr>
        ');


        html.e_table;
        html.e_form;
        html.e_box;
        html.e_page;


end if;
END;
END delete_picture;



---------------------------------------------------------
-- Name:        delete_picture_2
-- Type:        procedure
-- What:        deletes the entry from the database
-- Author:      Frode Klevstul
-- Start date:  09.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE delete_picture_2
                (
                        p_picture_pk    in picture.picture_pk%type              default NULL,
                        p_confirm       in varchar2                             default NULL
                )
IS
BEGIN
DECLARE

        v_album_pk      album.album_pk%type             default NULL;
        v_check         number                          default NULL;

BEGIN
if ( login.timeout('photo.startup')>0 ) then

        -- -----------------------------------------------------
        -- sjekker at bildet tilh�rer denne organisasjonen
        -- -----------------------------------------------------
        SELECT  count(*) INTO v_check
        FROM    album, picture_in_album
        WHERE   album_pk = album_fk
        AND     organization_fk = get.oid
        AND     picture_fk = p_picture_pk;

        if (v_check = 0) then
                htp.p( get.msg(1, 'wrong values') );
                return;
        end if;

        SELECT  album_fk INTO v_album_pk
        FROM    picture_in_album
        WHERE   picture_fk = p_picture_pk;

        if (p_confirm IS NOT NULL) then
                DELETE FROM picture
                WHERE picture_pk = p_picture_pk;
                commit;
        end if;


                v_check := system.inc_pages( get.oid, get.lan, v_album_pk);

        photo.list_pictures(v_album_pk);

end if;
END;
END delete_picture_2;





-- --------------------------------------------------------------------------------
-- Under kommer prosedyrer som brukes p� org. sine egne sider for vanlige brukere
-- --------------------------------------------------------------------------------

---------------------------------------------------------
-- Name:        user_list_albums
-- Type:        procedure
-- What:        procedure to list all albums for ordinary users
-- Author:      Frode Klevstul
-- Start date:  14.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE user_list_albums
        (
                p_organization_pk               in organization.organization_pk%type    default NULL,
                p_no                            number                                  default NULL
        )
IS
BEGIN
DECLARE

        CURSOR  select_all_1
                (
                        v_organization_pk       in organization.organization_pk%type
                ) IS
        SELECT  album_pk
        FROM    album a, organization o, org_service os
        WHERE   a.organization_fk = v_organization_pk
        AND     activated = 1
        AND     o.organization_pk = a.organization_fk
        AND     o.accepted > 0
        AND     os.organization_fk = o.organization_pk
        AND     os.service_fk = get.serv
        ORDER BY create_date desc;

        CURSOR  select_all_2 IS
        SELECT  album_pk, a.organization_fk, o.geography_fk
        FROM    album a, organization o, org_service os
        WHERE   activated = 1
        AND     o.organization_pk = a.organization_fk
        AND     o.accepted > 0
        AND     os.organization_fk = o.organization_pk
        AND     os.service_fk = get.serv
        ORDER BY create_date desc;

        v_album_pk              album.album_pk%type                     default NULL;
        v_title                 album_text.title%type                   default NULL;
        v_when_period           album_text.when_period%type             default NULL;
        v_organization_pk       organization.organization_pk%type       default NULL;
        v_geography_pk                  geography.geography_pk%type                             default NULL;

        v_no_pic                number                          default NULL;
        v_tmp2                  number                          default 1;
        v_bgcolor               varchar2(10)                    default NULL;

BEGIN

        -- henter ut fargen som skal brukes i headingen til tabellen
        v_bgcolor := get.value( 'c_'|| get.serv_name ||'_mmb' );


        -- dersom man ikke sender med noe bestem antall henter vi ut MANGE bilder...
        if (p_no IS NULL) then
                v_tmp2 := 9999999;

                if (p_organization_pk IS NOT NULL) then
                        html.b_page(NULL, NULL, NULL, NULL, 3);
                        html.org_menu(p_organization_pk);
                else
                        html.b_page;
                end if;

                html.b_box( get.txt('last_albums'), '100%', 'photo.user_album_archive');
        else
                v_tmp2 := get.value('no_albums');
                html.b_box( get.txt('last_albums'), '100%', 'photo.user_list_album', 'photo.user_album_archive?p_organization_pk='||p_organization_pk||'&p_language_pk='||get.lan );
        end if;

        html.b_table;

        if (p_organization_pk IS NOT NULL) then
                htp.p('<tr><td><b>'|| get.txt('when') ||':</b></td><td><b>'||get.txt('title')||':</b></td><td align="right"><b>'|| get.txt('no_pic') ||':</b></td></tr>');
                open select_all_1(p_organization_pk);
                while ( select_all_1%ROWCOUNT < v_tmp2)
                loop
                        fetch select_all_1 into v_album_pk;
                        exit when select_all_1%NOTFOUND;

                        SELECT  count(*) INTO v_no_pic
                        FROM    picture_in_album
                        WHERE   album_fk = v_album_pk;

                        v_title         := get.ap_text(v_album_pk, 'title');
                        v_when_period   := get.ap_text(v_album_pk, 'when_period');

                        htp.p('<tr>
                                <td nowrap valign="top">'|| v_when_period ||'&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td valign="top"><a href="photo.user_list_pictures?p_album_pk='||v_album_pk||'">'|| v_title ||'</a>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td align="right" valign="top">'|| v_no_pic ||'</td>
                                </tr>
                        ');
                end loop;
                close select_all_1;

                                if ( p_no IS NULL ) THEN
                                   -- registrerer statistikk
                                   stat.reg( p_organization_pk, NULL, NULL, NULL, v_album_pk, NULL ,29 );
                                end if;

        else
                htp.p('<tr><td><b>'|| get.txt('when') ||':</b></td><td><b>'||get.txt('title')||':</b></td><td align="right"><b>'|| get.txt('no_pic') ||':</b></td><td>&nbsp;&nbsp;<b>'|| get.txt('source') ||':</b></td></tr>');
                open select_all_2;
                while ( select_all_2%ROWCOUNT < v_tmp2)
                loop
                        fetch select_all_2 into v_album_pk, v_organization_pk, v_geography_pk;
                        exit when select_all_2%NOTFOUND;

                        SELECT  count(*) INTO v_no_pic
                        FROM    picture_in_album
                        WHERE   album_fk = v_album_pk;

                        v_title         := get.ap_text(v_album_pk, 'title');
                        v_when_period   := get.ap_text(v_album_pk, 'when_period');

                        htp.p('<tr>
                                <td nowrap valign="top">'|| v_when_period ||'&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td valign="top"><a href="photo.user_list_pictures?p_album_pk='||v_album_pk||'">'|| v_title ||'</a>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td align="right" valign="top">'|| v_no_pic ||'</td>
                                <td valign="top" nowrap>&nbsp;&nbsp;<a href="org_page.main?p_organization_pk='||v_organization_pk||'">'|| get.oname(v_organization_pk) ||'</a>&nbsp;('||get.locn(v_geography_pk)||')</td>
                                </tr>
                        ');

                end loop;
                close select_all_2;
        end if;

        html.e_table;
        html.e_box;


        if (p_no IS NULL) then
                html.e_page;
        end if;



END;
END user_list_albums;




---------------------------------------------------------
-- Name:        user_list_pictures
-- Type:        procedure
-- What:        lists out pictures in one album for ordinary users
-- Author:      Frode Klevstul
-- Start date:  14.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE user_list_pictures
                (
                        p_album_pk              in album.album_pk%type  default NULL
                )
IS
BEGIN
DECLARE

    CURSOR  select_all
            (
                    v_album_pk in album.album_pk%type
            ) IS
    SELECT  picture_pk, path, whom
    FROM    picture p, picture_in_album pia, album a
    WHERE   pia.picture_fk = picture_pk
    AND     pia.album_fk = v_album_pk
    AND     pia.album_fk = a.album_pk
    AND     a.activated = 1
    ORDER BY p.when_taken;

    v_picture_pk            picture.picture_pk%type             default NULL;
    v_path                  picture.path%type                   default NULL;
    v_organization_pk       organization.organization_pk%type   default NULL;
        v_whom                                  picture.whom%type                                       default NULL;

    v_title                 album_text.title%type                               default NULL;
    v_description           album_text.description%type                 default NULL;
    v_when_period           album_text.when_period%type                 default NULL;

BEGIN

        v_description   := get.ap_text(p_album_pk, 'description');
        v_title         := get.ap_text(p_album_pk, 'title');
        v_when_period   := get.ap_text(p_album_pk, 'when_period');

        SELECT  organization_fk INTO v_organization_pk
        FROM    album
        WHERE   album_pk = p_album_pk;

        -- ---------------------
        -- skriver ut HTML kode
        -- ---------------------
        html.b_page(NULL, NULL, NULL, NULL, 3);
        html.org_menu(v_organization_pk);
        html.b_box( v_title, '100%', 'photo.user_list_pictures' );
        html.b_table;

        htp.p('
                <tr>
                        <td align="center">'|| v_description ||'</td>
                </tr>
                <tr>
                        <td align="center"><i>'|| v_when_period ||'</i></td>
                </tr>
                <tr><td>&nbsp;</td></tr><tr><td><blockquote>');

        open select_all(p_album_pk);
        loop
                fetch select_all into v_picture_pk, v_path, v_whom;
                exit when select_all%NOTFOUND;

                htp.p('
                        <a href="photo.user_show_picture?p_picture_pk='|| v_picture_pk ||'"><img
                        border="0" src="'||get.value('photo_archive')||'/'||v_path ||'_thumb.jpg" alt="'|| v_whom ||'"
                        ></a>
                ');

        end loop;
        close select_all;

        htp.p('</blockquote></td></tr>');
        --htp.p('<tr><td colspan="5" align="right">');
        --html.back( get.txt('back') );
        -- html.button_link(get.txt('back_to_albums'), 'photo.user_list_albums?p_organization_pk='||v_organization_pk);
        --htp.p('</td></tr>');

        html.e_table;
        html.e_form;
        html.e_box;
        html.e_page;

        -- registrerer statistikk
        stat.reg( v_organization_pk, NULL, NULL, NULL, p_album_pk, NULL ,29 );

END;
END user_list_pictures;




---------------------------------------------------------
-- Name:        user_show_picture
-- Type:        procedure
-- What:        show one picture (big version)
-- Author:      Frode Klevstul
-- Start date:  03.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE user_show_picture
                (
                        p_picture_pk            in picture.picture_pk%type      default NULL
                )
IS
BEGIN
DECLARE

        CURSOR  select_all IS
        SELECT  the_comment, login_name, comment_date
        FROM    picture_comment pc, usr u
        WHERE   pc.user_fk = u.user_pk
        AND     picture_fk = p_picture_pk
        ORDER BY comment_date desc;

        v_path                  picture.path%type                       default NULL;
        v_when_taken            picture.when_taken%type                 default NULL;
        v_whom                  picture.whom%type                       default NULL;
        v_photographer          picture.photographer%type               default NULL;
        v_activated             picture.activated%type                  default NULL;
        v_what                  picture_text.what%type                  default NULL;

        v_title                 album_text.title%type                   default NULL;
        v_album_pk              album.album_pk%type                     default NULL;

        v_the_comment           picture_comment.the_comment%type        default NULL;
        v_login_name            usr.login_name%type                     default NULL;
        v_comment_date          picture_comment.comment_date%type       default NULL;
        v_organization_pk       organization.organization_pk%type       default NULL;
        v_picture_pk            picture.picture_pk%type                                 default NULL;

        cursor next_picture is
        select picture_pk
        FROM   picture p, picture_in_album pia
        where  pia.album_fk = v_album_pk
        and    pia.picture_fk = p.picture_pk
        and    p.picture_pk > p_picture_pk;

        cursor prev_picture is
        select picture_pk
        FROM   picture p, picture_in_album pia
        where  pia.album_fk = v_album_pk
        and    pia.picture_fk = p.picture_pk
        and    p.picture_pk < p_picture_pk
        order by picture_pk desc;

BEGIN

        -- -----------------------------
        -- henter ut verdier til bildet
        -- -----------------------------
        SELECT  path, when_taken, whom, photographer, activated
        INTO    v_path, v_when_taken, v_whom, v_photographer, v_activated
        FROM    picture
        WHERE   picture_pk = p_picture_pk;

        SELECT  album_fk INTO v_album_pk
        FROM    picture_in_album
        WHERE   picture_fk = p_picture_pk;

        SELECT  organization_fk INTO v_organization_pk
        FROM    album
        WHERE   album_pk = v_album_pk
        AND     activated = 1;

        v_title         := get.oname(v_organization_pk) || ' - ' || get.ap_text(v_album_pk, 'title');
        v_what          := get.ap_text(p_picture_pk, 'what');

        -- ---------------------
        -- skriver ut HTML kode
        -- ---------------------
        html.b_page(NULL, NULL, NULL, NULL, 3);
        html.org_menu(v_organization_pk);
        html.b_box( v_title , '100%', 'photo.user_show_picture');
        html.b_table;

        htp.p('<tr><td colspan="2">
                <img alt="'|| v_whom ||'" border="0" src="'|| get.value('photo_archive') ||'/'||v_path||'.jpg" alt="">
                </td></tr>
        ');

        htp.p('<tr><td width="15%">&nbsp;</td><td>&nbsp;</td></tr>');

        if ( v_when_taken IS NOT NULL) then
                htp.p('<tr><td>'|| get.txt('when_taken') ||':</td><td>'|| to_char(v_when_taken, get.txt('date_year') ) ||'</td></tr>');
        end if;

        if ( v_whom IS NOT NULL) then
                htp.p('<tr><td>'|| get.txt('whom') ||':</td><td>'|| v_whom ||'</td></tr>');
        end if;

        if ( v_photographer IS NOT NULL) then
                htp.p('<tr><td>'|| get.txt('photographer') ||':</td><td>'|| v_photographer ||'</td>');
        end if;

        if ( v_what IS NOT NULL) then
                htp.p('<tr><td>'|| get.txt('what') ||':</td><td>'|| v_what ||'</td></tr>');
        end if;

        v_picture_pk := NULL;
        open prev_picture;
        fetch prev_picture into v_picture_pk;
        close prev_picture;
        htp.p('<tr><td align="left">&nbsp;');
        if ( v_picture_pk IS NOT NULL) then
                html.button_link( get.txt('previous'), 'photo.user_show_picture?p_picture_pk='||v_picture_pk);
        end if;
        htp.p('</td><td align="right">');
        v_picture_pk := NULL;
        open next_picture;
        fetch next_picture into v_picture_pk;
        close next_picture;
        if ( v_picture_pk IS NOT NULL) then
                html.button_link( get.txt('next'), 'photo.user_show_picture?p_picture_pk='||v_picture_pk);
        end if;
        htp.p('</td></tr>');

        if (v_activated IS NOT NULL) then
                htp.p('<tr><td colspan="2" align="right">');
                html.button_link( get.txt('add_comment'), 'photo.user_add_comment?p_picture_pk='||p_picture_pk);
                htp.p('</td></tr>');
        end if;

        htp.p('<tr><td colspan="2" align="right">');
        html.button_link( get.txt('to_album'), 'photo.user_list_pictures?p_album_pk='||v_album_pk);
        htp.p('</td></tr>');


        if (v_activated IS NOT NULL) then
                open select_all;
                loop
                        fetch select_all into v_the_comment, v_login_name, v_comment_date;
                        exit when select_all%NOTFOUND;

                        htp.p('<tr><td colspan="2"><b>'||v_login_name||'</b> (<i>'|| to_char(v_comment_date, get.txt('date_long')) ||'</i>):<dd>'|| v_the_comment ||'<br>&nbsp;</td></tr>');

                end loop;
                close select_all;
        end if;

        htp.p('<tr><td colspan="2">');
        tip.someone;
        htp.p('</td></tr>');

        html.e_table;
        html.e_box;
        html.e_page;

        -- registrer statistikk
        stat.reg( v_organization_pk, NULL, NULL, NULL, NULL, p_picture_pk, 30 );

END;
END user_show_picture;



---------------------------------------------------------
-- Name:        user_last_pictures
-- Type:        procedure
-- What:        lists out an org last pic
-- Author:      Frode Klevstul
-- Start date:  14.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE user_last_pictures
                (
                        p_organization_pk               in organization.organization_pk%type    default NULL,
                        p_archive                       in number                               default NULL
                )
IS
BEGIN
DECLARE

        v_picture_pk            picture.picture_pk%type         default NULL;
        v_path                  picture.path%type               default NULL;
        v_whom                  picture.whom%type               default NULL;
        v_no                    number                          default NULL;
        v_tmp                   number                          default 0;
        v_sql                   VARCHAR2(1024)                          DEFAULT NULL;


TYPE cur_typ IS REF CURSOR;
photo_dyn_cursor    cur_typ;

BEGIN
   v_sql := 'SELECT  picture_pk, path, whom
        FROM    picture p, picture_in_album pia,
        album a, organization o, org_service os
        WHERE   pia.picture_fk = picture_pk
        AND     pia.album_fk = a.album_pk
        AND     a.activated = 1';
   IF ( p_organization_pk IS NOT NULL) THEN
        v_sql := v_sql ||'
        AND     a.organization_fk = '||p_organization_pk;
   END IF;
   v_sql := v_sql ||'
     AND     o.organization_pk = a.organization_fk
     AND     o.accepted > 0
     AND     os.organization_fk = o.organization_pk
     AND     os.service_fk = '||get.serv||'
     ORDER BY upload_date DESC
     ';

        IF ( p_archive IS NULL ) then
           v_no := get.value('no_pictures');
        else
           v_no := get.value('no_pictures_all');
        END IF;

        -- ---------------------
        -- skriver ut HTML kode
        -- ---------------------
        OPEN photo_dyn_cursor FOR v_sql;
        loop
           fetch photo_dyn_cursor into v_picture_pk, v_path, v_whom;
              exit when photo_dyn_cursor%ROWCOUNT = v_no;
              exit when photo_dyn_cursor%NOTFOUND;

              IF ( photo_dyn_cursor%ROWCOUNT = 1 ) THEN
                 if ( p_archive IS NULL ) then
                    html.b_box( get.txt('last_pictures'), '100%','photo.user_last_pictures', 'photo.user_last_pictures?p_organization_pk='||p_organization_pk||'&p_archive=1');
                 else
                    html.b_page(NULL, NULL, NULL, NULL, 1);
                    IF ( p_organization_pk IS NULL ) THEN
                       html.b_box( get.txt('last_pictures'), '100%','photo.user_last_pictures');
                       IF ( owa_util.get_cgi_env('PATH_INFO') <> '/main_page.startup' ) THEN
                          -- Legger inn i statistikk tabellen
                          stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,28);
                       END IF;
                       html.home_menu;
                    ELSE
                       html.b_box( get.txt('last_pictures'), '100%','photo.user_last_pictures', 'photo.user_last_pictures?p_organization_pk='||p_organization_pk||'&p_archive=1');
                       IF ( owa_util.get_cgi_env('PATH_INFO') <> '/org_page.main' ) THEN
                          -- Legger inn i statistikk tabellen
                          stat.reg(p_organization_pk,NULL,NULL,NULL,NULL,NULL,29);
                       END IF;
                       html.org_menu(p_organization_pk);
                    END IF;
                 end if;
                 html.b_table;
                 htp.p('<tr><td align="center">');
              end if;
              htp.p('<a href="photo.user_show_picture?p_picture_pk='||v_picture_pk||'"><img width="40" border="0" src="'||get.value('photo_archive')||'/'|| v_path ||'_thumb.jpg" alt="'||v_whom||'"></a>');

        end loop;

IF ( photo_dyn_cursor%ROWCOUNT > 0 ) THEN
   htp.p('</td></tr>');
   html.e_table;
   html.e_box;
END IF;

if ( p_archive IS NOT NULL ) then
   html.e_page;
end if;

close photo_dyn_cursor;

END;
END user_last_pictures;




---------------------------------------------------------
-- Name:        user_add_comment
-- Type:        procedure
-- What:        add an comment on a picture
-- Author:      Frode Klevstul
-- Start date:  14.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE user_add_comment
                (
                        p_picture_pk            in picture.picture_pk%type      default NULL
                )
IS
BEGIN
DECLARE

        v_path                  picture.path%type                               default NULL;
        v_title                 album_text.title%type                           default NULL;
        v_album_pk              album.album_pk%type                             default NULL;
        v_check                 number                                                                  default NULL;
        v_organization_pk       organization.organization_pk%TYPE       default NULL;

BEGIN
if ( login.timeout('photo.user_add_comment?p_picture_pk='||p_picture_pk)>0 ) then


        -- -----------------------------------------------------
        -- sjekker at bildet ligger i et album som er aktivert
        -- -----------------------------------------------------
        SELECT  count(*) INTO v_check
        FROM    album, picture_in_album
        WHERE   album_pk = album_fk
        AND     picture_fk = p_picture_pk
        AND     activated = 1;

        if (v_check = 0) then
                htp.p( get.msg(1, 'wrong values') );
                return;
        end if;


        -- -----------------------------
        -- henter ut verdier til bildet
        -- -----------------------------
        SELECT  path
        INTO    v_path
        FROM    picture
        WHERE   picture_pk = p_picture_pk;

        SELECT  album_fk INTO v_album_pk
        FROM    picture_in_album
        WHERE   picture_fk = p_picture_pk;

                SELECT  organization_fk
                INTO    v_organization_pk
                FROM    album
                WHERE   album_pk = v_album_pk;

        v_title         := get.ap_text(v_album_pk, 'title');

        -- ---------------------
        -- skriver ut HTML kode
        -- ---------------------
        html.b_page;
                html.org_menu(v_organization_pk);
        html.b_box(v_title, '100%', 'photo.user_add_comment');
        html.b_table;

        htp.p('<tr><td colspan="2">
                <a href="photo.user_list_pictures?p_album_pk='||v_album_pk||'"><img
                alt="'|| get.txt('go_back') ||'" border="0" src="'|| get.value('photo_archive') ||'/'||v_path||'_thumb.jpg" alt=""
                ></a></td></tr>');

        html.b_form('photo.user_insert_comment');
        htp.p('<input type="hidden" name="p_picture_pk" value="'|| p_picture_pk ||'">');
        htp.p('
                <tr>
                        <td>'|| get.txt('comment:') ||'</td><td><input type="text" name="p_the_comment" size="60" maxlength="200"></td>
                </tr>
        ');
        htp.p('<tr><td colspan="2" align="right">');
        html.submit_link( get.txt('insert_comment') );
        htp.p('</td></tr>');

        html.e_form;

        htp.p('<tr><td colspan="2" align="right">');
        html.back( get.txt('back') );
        htp.p('</td></tr>');

        html.e_table;
        html.e_box;
        html.e_page;

end if;
END;
END user_add_comment;


---------------------------------------------------------
-- Name:        user_insert_comment
-- Type:        procedure
-- What:        add an comment on a picture
-- Author:      Frode Klevstul
-- Start date:  14.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE user_insert_comment
                (
                        p_picture_pk            in picture.picture_pk%type              default NULL,
                        p_the_comment           in picture_comment.the_comment%type     default NULL
                )
IS
BEGIN
DECLARE

        v_check                 number                                  default NULL;
        v_picture_comment_pk    picture_comment.picture_comment_pk%type default NULL;
        v_user_pk               usr.user_pk%type                        default NULL;

BEGIN
if ( login.timeout('photo.user_add_comment')>0 ) then

        v_user_pk := get.uid;

        -- -----------------------------------------------------
        -- sjekker at bildet ligger i et album som er aktivert
        -- -----------------------------------------------------
        SELECT  count(*) INTO v_check
        FROM    album, picture_in_album
        WHERE   album_pk = album_fk
        AND     picture_fk = p_picture_pk
        AND     activated = 1;

        if (v_check = 0) then
                htp.p( get.msg(1, 'wrong values') );
                return;
        end if;

        if (p_the_comment IS NOT NULL) then
                SELECT  picture_comment_seq.NEXTVAL
                INTO    v_picture_comment_pk
                FROM    dual;

                INSERT INTO picture_comment
                (picture_comment_pk, picture_fk, user_fk, the_comment, comment_date)
                VALUES (v_picture_comment_pk, p_picture_pk, v_user_pk, html.rem_tag(p_the_comment), sysdate);
                commit;
        end if;

        html.jump_to('photo.user_show_picture?p_picture_pk='||p_picture_pk);

end if;
END;
END user_insert_comment;



---------------------------------------------------------
-- Name:        user_last_comment
-- Type:        procedure
-- What:        lists out an org last commented pic
-- Author:      Frode Klevstul
-- Start date:  15.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE user_last_comment
                (
                        p_organization_pk               in organization.organization_pk%type    default NULL,
                        p_archive                       in number                               default NULL
                )
IS
BEGIN
DECLARE


        CURSOR  select_all_1 IS
        SELECT DISTINCT picture_pk, path, comment_date, whom
        FROM            picture p, picture_in_album pia, album a,
                        picture_comment pc, organization o, org_service os
        WHERE           pia.picture_fk = p.picture_pk
        AND             pia.album_fk = a.album_pk
        AND             a.activated = 1
        AND             p.activated = 1
        AND             pc.picture_fk = p.picture_pk
        AND             comment_date IN(
                                SELECT max(comment_date)
                                FROM picture_comment
                                group by picture_fk
                        )
        AND             o.organization_pk = a.organization_fk
        AND             o.accepted > 0
        AND             os.organization_fk = o.organization_pk
        AND             os.service_fk = get.serv
        ORDER BY        comment_date DESC;


        CURSOR  select_all_2(v_organization_pk in organization.organization_pk%type) IS
        SELECT DISTINCT picture_pk, path, comment_date, whom
        FROM            picture p, picture_in_album pia, album a,
                        picture_comment pc, organization o
        WHERE           pia.picture_fk = p.picture_pk
        AND             pia.album_fk = a.album_pk
        AND             a.activated = 1
        AND             p.activated = 1
        AND             pc.picture_fk = p.picture_pk
        AND             a.organization_fk = v_organization_pk
        AND             comment_date IN(
                                SELECT max(comment_date)
                                FROM picture_comment
                                group by picture_fk
                        )
        AND             o.organization_pk = a.organization_fk
        AND             o.accepted > 0
        ORDER BY        comment_date DESC;


        v_picture_pk            picture.picture_pk%type                 default NULL;
        v_path                  picture.path%type                       default NULL;
        v_comment_date          picture_comment.comment_date%type       default NULL;
        v_whom                  picture.whom%type                       default NULL;

        v_no                    number                                  default NULL;
        v_tmp                   number                                  default 0;
BEGIN

        if ( p_archive IS NULL ) then
                v_no := get.value('no_pictures');
                html.b_box( get.txt('last_comments'), '100%','photo.user_last_comment', 'photo.user_last_comment?p_organization_pk='||p_organization_pk||'&p_archive=1');
        else
                v_no := get.value('no_pictures_all');
                html.b_page(NULL, NULL, NULL, NULL, 1);
                html.home_menu;
                html.b_box( get.txt('last_comments'), '100%','photo.user_last_comment');
        end if;

        -- ---------------------
        -- skriver ut HTML kode
        -- ---------------------

        html.b_table;
        htp.p('<tr><td align="center">');

        if (p_organization_pk IS NULL) then
                IF ( owa_util.get_cgi_env('PATH_INFO') <> '/main_page.startup' ) THEN
                     -- Legger inn i statistikk tabellen
                     stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,21);
                END IF;
                open select_all_1;
                while (v_tmp < v_no)
                loop
                        v_tmp := v_tmp + 1;
                        fetch select_all_1 into v_picture_pk, v_path, v_comment_date, v_whom;
                        exit when select_all_1%NOTFOUND;

                        htp.p('<a href="photo.user_show_picture?p_picture_pk='||v_picture_pk||'"><img width="40" border="0" src="'||get.value('photo_archive')||'/'|| v_path ||'_thumb.jpg" alt="'||v_whom||'"></a>');

                end loop;
                close select_all_1;
        else
                IF ( owa_util.get_cgi_env('PATH_INFO') <> '/org_page.main' ) THEN
                     -- Legger inn i statistikk tabellen
                     stat.reg(p_organization_pk,NULL,NULL,NULL,NULL,NULL,29);
                END IF;
                open select_all_2(p_organization_pk);
                while (v_tmp < v_no)
                loop
                        v_tmp := v_tmp + 1;
                        fetch select_all_2 into v_picture_pk, v_path, v_comment_date, v_whom;
                        exit when select_all_2%NOTFOUND;

                        htp.p('<a href="photo.user_show_picture?p_picture_pk='||v_picture_pk||'"><img width="40" border="0" src="'||get.value('photo_archive')||'/'|| v_path ||'_thumb.jpg" alt="'||v_whom||'"></a>');

                end loop;
                close select_all_2;
        end if;

        htp.p('</td></tr>');
        html.e_table;
        html.e_box;

        if ( p_archive IS NOT NULL ) then
                html.e_page;
        end if;

END;
END user_last_comment;





---------------------------------------------------------------------
-- Name: user_album_archive
-- Type: procedure
-- What: Genererer albumarkiv
-- Made: Frode Klevstul
-- Date: 11.09.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE user_album_archive
                        (
                                p_organization_pk       IN organization.organization_pk%TYPE            DEFAULT NULL,
                                p_language_pk           IN la.language_pk%TYPE                          DEFAULT NULL,
                                p_service_pk            IN service.service_pk%TYPE                      DEFAULT NULL,
                                p_new_date              IN VARCHAR2                                     DEFAULT to_char(SYSDATE,get.txt('date_long')),
                                p_old_date              IN VARCHAR2                                     DEFAULT NULL,
                                p_count                 IN NUMBER                                       DEFAULT 1,
                                p_my_archive            IN NUMBER                                       DEFAULT NULL,
                                p_album_pk              IN album.album_pk%TYPE                          DEFAULT 0
                        )
IS
BEGIN
DECLARE

        v_service_pk            service.service_pk%TYPE                 DEFAULT NULL;
        v_create_date           album.create_date%TYPE                  DEFAULT NULL;
        v_create_date2          album.create_date%TYPE                  DEFAULT NULL;
        v_title                 album_text.title%TYPE                   DEFAULT NULL;
        v_album_pk              album.album_pk%TYPE                     DEFAULT NULL;
        v_name                  organization.name%TYPE                  DEFAULT NULL;
        v_count                 NUMBER                                  DEFAULT NULL;
        v_count2                NUMBER                                  DEFAULT NULL;
        v_count3                NUMBER                                  DEFAULT NULL;
        v_album_arc_no          NUMBER                                  DEFAULT NULL;
        v_organization_pk       organization.organization_pk%TYPE       DEFAULT NULL;
        v_language_pk           document.language_fk%TYPE               DEFAULT NULL;
        v_user_pk               usr.user_pk%TYPE                        DEFAULT NULL;
                v_geography_pk                  geography.geography_pk%type                             DEFAULT NULL;

        CURSOR  album_archive IS
        SELECT  a.create_date, alt.title, a.album_pk, o.name, o.organization_pk
        FROM    album a, album_text alt, organization o, org_service os
        WHERE   a.album_pk = alt.album_fk
        AND     o.organization_pk = os.organization_fk
        AND     os.service_fk = v_service_pk
        AND     alt.language_fk = v_language_pk
        AND     a.organization_fk = p_organization_pk
        AND     a.organization_fk = o.organization_pk
        AND     a.create_date < to_date(p_new_date,get.txt('date_long'))
        AND     a.activated = 1
        AND     o.accepted = 1
        OR      a.album_pk = alt.album_fk
        AND     o.organization_pk = os.organization_fk
        AND     os.service_fk = v_service_pk
        AND     alt.language_fk = v_language_pk
        AND     a.organization_fk = p_organization_pk
        AND     a.organization_fk = o.organization_pk
        AND     a.create_date = to_date(p_new_date,get.txt('date_long'))
        AND     a.activated = 1
        AND     a.album_pk > p_album_pk
        AND     o.accepted = 1
        ORDER BY a.create_date DESC, a.album_pk
        ;

        CURSOR  album_archive_all IS
        SELECT  a.create_date, alt.title, a.album_pk, o.name, o.organization_pk, o.geography_fk
        FROM    album a, album_text alt, organization o, org_service os
        WHERE   a.album_pk = alt.album_fk
        AND     o.organization_pk = os.organization_fk
        AND     os.service_fk = v_service_pk
        AND     alt.language_fk = v_language_pk
        AND     a.organization_fk = o.organization_pk
        AND     a.create_date < to_date(p_new_date,get.txt('date_long'))
        AND     a.activated = 1
        AND     o.accepted = 1
        OR      a.album_pk = alt.album_fk
        AND     o.organization_pk = os.organization_fk
        AND     os.service_fk = v_service_pk
        AND     alt.language_fk = v_language_pk
        AND     a.organization_fk = o.organization_pk
        AND     a.create_date = to_date(p_new_date,get.txt('date_long'))
        AND     a.activated = 1
        AND     a.album_pk > p_album_pk
        AND     o.accepted = 1
        ORDER BY a.create_date DESC, a.album_pk
        ;

        CURSOR  my_album_archive(v_user_pk in usr.user_pk%type) IS
        SELECT  a.create_date, alt.title, a.album_pk, o.name, o.organization_pk, o.geography_fk
        FROM    album a, album_text alt, organization o, org_service os
        WHERE   a.album_pk = alt.album_fk
        AND     o.organization_pk = os.organization_fk
        AND     os.service_fk = v_service_pk
        AND     alt.language_fk = v_language_pk
        AND     a.organization_fk = o.organization_pk
        AND     a.create_date < to_date(p_new_date,get.txt('date_long'))
        AND     a.activated = 1
        AND     o.accepted = 1
        AND     o.organization_pk IN
                (
                        SELECT  DISTINCT organization_fk
                        FROM    list_org lo, list l, list_user lu, list_type lt
                        WHERE   lo.list_fk = l.list_pk
                        AND     lt.list_type_pk = l.list_type_fk
                        AND     lu.user_fk = v_user_pk
                        AND
                        (
                                lu.list_fk = l.list_pk
                        OR      lu.list_fk = l.level0
                        OR      lu.list_fk = l.level1
                        OR      lu.list_fk = l.level2
                        OR      lu.list_fk = l.level3
                        OR      lu.list_fk = l.level4
                        OR      lu.list_fk = l.level5
                        )
                )
        OR      a.album_pk = alt.album_fk
        AND     o.organization_pk = os.organization_fk
        AND     os.service_fk = v_service_pk
        AND     alt.language_fk = v_language_pk
        AND     a.organization_fk = o.organization_pk
        AND     a.create_date = to_date(p_new_date,get.txt('date_long'))
        AND     a.activated = 1
        AND     a.album_pk > p_album_pk
        AND     o.accepted = 1
        AND     o.organization_pk IN
                (
                        SELECT  DISTINCT organization_fk
                        FROM    list_org lo, list l, list_user lu, list_type lt
                        WHERE   lo.list_fk = l.list_pk
                        AND     lt.list_type_pk = l.list_type_fk
                        AND     lu.user_fk = v_user_pk
                        AND
                        (
                                lu.list_fk = l.list_pk
                        OR      lu.list_fk = l.level0
                        OR      lu.list_fk = l.level1
                        OR      lu.list_fk = l.level2
                        OR      lu.list_fk = l.level3
                        OR      lu.list_fk = l.level4
                        OR      lu.list_fk = l.level5
                        )
                )
        ORDER BY a.create_date DESC, a.album_pk
        ;


BEGIN


        -- henter ut antall forekomster som skal vises pr side
        v_album_arc_no := get.value('album_arc_no');

        -- starter siden og skriver ut riktige menyer


        IF ( p_organization_pk IS NULL AND p_my_archive IS NULL) THEN
                html.b_page(NULL, NULL, NULL, NULL, 1);
                html.home_menu;
                html.b_box( get.txt('album_archive'), '100%', 'photo.user_album_archive(all)');
        ELSIF ( p_organization_pk IS NOT NULL ) THEN
                html.b_page(NULL, NULL, NULL, NULL, 3);
                html.org_menu(p_organization_pk);
                html.b_box( get.txt('album_archive'), '100%', 'photo.user_album_archive(org)');
        ELSIF (p_my_archive IS NOT NULL) THEN
                html.b_page(NULL, NULL, NULL, NULL, 2);
                html.my_menu;
                html.b_box( get.txt('album_archive'), '100%', 'photo.user_album_archive(my)');
        END IF;
        html.b_table;

        -- henter riktig language_pk and service_pk
        IF ( p_service_pk IS NULL ) THEN
                v_service_pk := get.serv;
        ELSE
                v_service_pk := p_service_pk;
        END IF;
        IF ( p_language_pk IS NULL ) THEN
                v_language_pk := get.lan;
        ELSE
                v_language_pk := p_language_pk;
        END IF;

        -- Legger inn i statistikk tabellen
        IF ( p_organization_pk IS NOT NULL ) THEN
           IF ( owa_util.get_cgi_env('PATH_INFO') <> '/main_page.startup' ) THEN
              stat.reg(p_organization_pk,NULL,NULL,NULL,NULL,NULL,29);
           END IF;
        ELSE
           IF ( owa_util.get_cgi_env('PATH_INFO') <> '/org_page.main' ) THEN
                 stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,28);
           END IF;
        END IF;
        IF ( p_organization_pk IS NULL AND p_language_pk IS NULL AND p_my_archive IS NULL) THEN
                html.b_form('photo.user_album_archive');
                htp.p('<tr><td>');
                html.select_service;
                htp.p('</td><td>');
                html.select_lang;
                htp.p('</td><td>');
                html.submit_link( get.txt('Show_archive') );
                htp.p('</td></tr>');
                html.e_form;
        ELSE

                -- arkiv for en org
                IF ( p_organization_pk IS NOT NULL ) THEN

                        SELECT  count(*)
                        INTO    v_count
                        FROM    album a, organization o
                        WHERE   a.organization_fk = p_organization_pk
                        AND     a.organization_fk = o.organization_pk
                        AND     o.accepted = 1
                        AND     a.activated = 1;

                        htp.p('<tr><td align="left"><b>'||get.txt('date')||':</b></td><td><b>'||get.txt('title')||':</b></td></tr>');
                        OPEN album_archive;
                        WHILE ( album_archive%ROWCOUNT < v_album_arc_no ) LOOP
                                FETCH   album_archive INTO v_create_date, v_title, v_album_pk, v_name, v_organization_pk;
                                EXIT when album_archive%NOTFOUND;
                                IF ( album_archive%ROWCOUNT = 1 ) THEN
                                        v_create_date2 := v_create_date;
                                END IF;

                                if (v_title IS NULL) then
                                        v_title := '-';
                                end if;

                                htp.p('<tr><td width="20%" align="left">'|| to_char(v_create_date,get.txt('date_long')) ||':</td>
                                <td width="80%" align="left"><a href="photo.user_list_pictures?p_album_pk='||v_album_pk||'">'|| v_title ||'</a></td>
                                ');
                        END LOOP;
                        CLOSE album_archive;

                -- arkiv for alle organisasjoner
                ELSIF(p_my_archive IS NULL) then

                        SELECT  count(*)
                        INTO    v_count
                        FROM    album a, organization o, org_service os
                        WHERE   a.organization_fk = o.organization_pk
                        AND     o.accepted = 1
                        AND     a.activated = 1
                        AND     o.organization_pk = os.organization_fk
                        AND     os.service_fk = get.serv;

                        htp.p('<tr><td align="left"><b>'||get.txt('date')||':</b></td><td><b>'||get.txt('title')||':</b></td><td><b>'||get.txt('source')||':</b></td></tr>');
                        OPEN album_archive_all;
                        WHILE ( album_archive_all%ROWCOUNT < v_album_arc_no ) LOOP
                                FETCH   album_archive_all INTO v_create_date, v_title, v_album_pk, v_name, v_organization_pk, v_geography_pk;
                                EXIT when album_archive_all%NOTFOUND;
                                IF ( album_archive_all%ROWCOUNT = 1 ) THEN
                                        v_create_date2 := v_create_date;
                                END IF;

                                if (v_title IS NULL) then
                                        v_title := '-';
                                end if;

                                htp.p('<tr><td width="20%" align="left">'|| to_char(v_create_date,get.txt('date_long')) ||':</td>
                                <td width="40%" align="left"><a href="photo.user_list_pictures?p_album_pk='||v_album_pk||'">'|| v_title ||'</a></td>
                                <td width="40%" align="left"><a href="org_page.main?p_organization_pk='||v_organization_pk||'">'|| v_name ||'</a>&nbsp;('||get.locn(v_geography_pk)||')</td></tr>
                                ');
                        END LOOP;
                        CLOSE album_archive_all;

                -- mitt personlige arkiv
                ELSE
                        v_user_pk := get.uid;

                        SELECT  count(*)
                        INTO    v_count
                        FROM    album a, organization o
                        WHERE   a.organization_fk = o.organization_pk
                        AND     a.activated = 1
                        AND     o.accepted = 1
                        AND     o.organization_pk IN
                                (
                                        SELECT  DISTINCT organization_fk
                                        FROM    list_org lo, list l, list_user lu, list_type lt
                                        WHERE   lo.list_fk = l.list_pk
                                        AND     lt.list_type_pk = l.list_type_fk
                                        AND     lu.user_fk = v_user_pk
                                        AND
                                        (
                                                lu.list_fk = l.list_pk
                                        OR      lu.list_fk = l.level0
                                        OR      lu.list_fk = l.level1
                                        OR      lu.list_fk = l.level2
                                        OR      lu.list_fk = l.level3
                                        OR      lu.list_fk = l.level4
                                        OR      lu.list_fk = l.level5
                                        )
                                )
                        ;

                        htp.p('<tr><td align="left"><b>'||get.txt('date')||':</b></td><td><b>'||get.txt('title')||':</b></td><td><b>'||get.txt('source')||':</b></td></tr>');
                        OPEN my_album_archive(v_user_pk);
                        WHILE ( my_album_archive%ROWCOUNT < v_album_arc_no ) LOOP
                                FETCH   my_album_archive INTO v_create_date, v_title, v_album_pk, v_name, v_organization_pk, v_geography_pk;
                                EXIT when my_album_archive%NOTFOUND;
                                IF ( my_album_archive%ROWCOUNT = 1 ) THEN
                                        v_create_date2 := v_create_date;
                                END IF;

                                if (v_title IS NULL) then
                                        v_title := '-';
                                end if;

                                htp.p('<tr><td width="20%" align="left">'|| to_char(v_create_date,get.txt('date_long')) ||':</td>
                                <td width="40%" align="left"><a href="photo.user_list_pictures?p_album_pk='||v_album_pk||'">'|| v_title ||'</a></td>
                                <td width="40%" align="left"><a href="org_page.main?p_organization_pk='||v_organization_pk||'">'|| v_name ||'</a>&nbsp;('||get.locn(v_geography_pk)||')</td></tr>
                                ');
                        END LOOP;
                        CLOSE my_album_archive;

                END IF;

                htp.p('<tr><td colspan="3">&nbsp;</td></tr>');
                IF ( p_old_date IS NOT NULL ) THEN
                        v_count3 := p_count - v_album_arc_no;
                        html.b_form('photo.user_album_archive','prev');
                        htp.p('
                        <input type="hidden" name="p_service_pk" value="'|| v_service_pk ||'">
                        <input type="hidden" name="p_my_archive" value="'|| p_my_archive ||'">
                        <input type="hidden" name="p_language_pk" value="'|| v_language_pk ||'">
                        <input type="hidden" name="p_organization_pk" value="'|| p_organization_pk ||'">
                        <input type="hidden" name="p_count" value="'|| v_count3 ||'">
                        <input type="hidden" name="p_new_date" value="'|| SUBSTR(p_old_date,LENGTH(p_old_date)-LENGTH(to_char(SYSDATE,get.txt('date_long')))+1,LENGTH(to_char(SYSDATE,get.txt('date_long')))+1) ||'">
                        <input type="hidden" name="p_old_date" value="'|| SUBSTR(p_old_date,0,LENGTH(p_old_date)-(LENGTH(to_char(SYSDATE,get.txt('date_long')))+2)) ||'">
                        ');
                        htp.p('<tr><td align="center" valign="bottom">');
                        html.submit_link( get.txt('previous')||' '||v_album_arc_no,'prev' );
                        htp.p('&nbsp;</td>');
                        html.e_form;
                ELSE
                        htp.p('<tr><td align="center" valign="bottom">&nbsp;</td>');
                END IF;
                v_count2 := p_count + v_album_arc_no - 1;
                IF ( v_count2 > v_count ) THEN
                        htp.p('<td valign="center" align="center">'|| p_count ||'-'|| v_count ||'/'|| v_count ||'</td>');
                ELSE
                        htp.p('<td valign="center" align="center">'|| p_count ||'-'|| v_count2 ||'/'|| v_count ||'</td>');
                END IF;
                IF ( v_count2 < v_count ) THEN
                        v_count3 := p_count + v_album_arc_no;
                        IF ( v_count - v_count2 < v_album_arc_no ) THEN
                                v_album_arc_no := v_count - v_count2;
                        END IF;
                        html.b_form('photo.user_album_archive','next');
                        htp.p('
                        <input type="hidden" name="p_service_pk" value="'|| v_service_pk ||'">
                        <input type="hidden" name="p_my_archive" value="'|| p_my_archive ||'">
                        <input type="hidden" name="p_language_pk" value="'|| v_language_pk ||'">
                        <input type="hidden" name="p_album_pk" value="'|| v_album_pk ||'">
                        <input type="hidden" name="p_organization_pk" value="'|| p_organization_pk ||'">
                        <input type="hidden" name="p_count" value="'|| v_count3 ||'">
                        <input type="hidden" name="p_new_date" value="'|| to_char( v_create_date,get.txt('date_long')) ||'">
                        <input type="hidden" name="p_old_date" value="'|| p_old_date ||';;'|| p_new_date ||'">
                        ');
                        htp.p('<td align="center" valign="top">&nbsp;');
                        html.submit_link( get.txt('next')||' '||v_album_arc_no,'next');
                        htp.p('</td></tr>');
                        html.e_form;
                ELSE
                        htp.p('<td align="center" valign="top">&nbsp;</td></tr>');
                END IF;
                IF ( p_organization_pk IS NOT NULL ) THEN
                        htp.p('
                        <tr><td colspan="3">&nbsp;</td></tr>
                        <tr><td align="center" colspan="3">
                        <a href="org_page.main?p_organization_pk='|| p_organization_pk ||'">['||v_name||']</a>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="/">['||get.serv_name||']</a>
                        </td></tr>
                        <tr><td colspan="3">&nbsp;</td></tr>
                        ');
                END IF;

        END IF;


        html.e_table;
        html.e_box;
        html.e_page;

END;
END user_album_archive;





-- ++++++++++++++++++++++++++++++++++++++++++++++ --


END; -- slutter pakke kroppen
/
show errors;




