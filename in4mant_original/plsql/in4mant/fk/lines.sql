set define off
PROMPT *** package: lines ***

CREATE OR REPLACE PACKAGE lines IS

	PROCEDURE startup;
	PROCEDURE list_line_type;
	PROCEDURE insert_line_type
			(
				p_line_type_pk		in line_type.line_type_pk%type	 	default NULL,
				p_name			in line_type.name%type	 		default NULL,
				p_description		in line_type.description%type	 	default NULL,
		                p_language_pk  		in la.language_pk%type			default NULL
	                );
	PROCEDURE delete_line_type
			(
				p_line_type_pk 		in line_type.line_type_pk%type	default NULL
	                );
	PROCEDURE delete_line_type_2
			(
				p_line_type_pk 	in line_type.line_type_pk%type		default NULL,
				p_confirm	in varchar2				default NULL
	                );
	PROCEDURE update_line_type
			(
				p_line_type_pk 		in line_type.line_type_pk%type	default NULL
	                );
	PROCEDURE list_line
			(
				p_line_type_pk		in line_type.line_type_pk%type	default NULL
			);
	PROCEDURE insert_line
			(
				p_line_pk		in line.line_pk%type	 		default NULL,
				p_line_type_fk		in line.line_type_fk%type		default NULL,
				p_accepted		in line.accepted%type	 		default NULL,
				p_line			in line.line%type	 		default NULL,
				p_who_said		in line.who_said%type	 		default NULL,
				p_where_said		in line.where_said%type	 		default NULL,
				p_when_said		in line.when_said%type	 		default NULL
	                );
	PROCEDURE update_line
			(
				p_line_pk 		in line.line_pk%type	default NULL
	                );
	PROCEDURE line_type_service
			(
				p_line_type_fk 		in line_type_on_service.line_type_fk%type	default NULL
			);
	PROCEDURE add_service
			(
				p_line_type_fk		in line_type_on_service.line_type_fk%type 		default NULL,
				p_service_pk 		in line_type_on_service.service_fk%type 		default NULL
	                );
	PROCEDURE remove_service
			(
				p_line_type_fk		in line_type_on_service.line_type_fk%type 	default NULL,
				p_service_fk 		in line_type_on_service.service_fk%type 	default NULL
	                );
	PROCEDURE delete_line
			(
				p_line_pk 		in line.line_pk%type		default NULL
	                );
	PROCEDURE delete_line_2
			(
				p_line_pk 	in line.line_pk%type			default NULL,
				p_confirm	in varchar2				default NULL
	                );

END;
/
CREATE OR REPLACE PACKAGE BODY lines IS


---------------------------------------------------------
-- Name: 	startup
-- Type: 	procedure
-- What: 	start procedure, to run the package
-- Author:  	Frode Klevstul
-- Start date: 	25.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

	list_line_type;

END startup;


---------------------------------------------------------
-- Name: 	list_line_type
-- Type: 	procedure
-- What: 	procedure to list all line types
-- Author:  	Frode Klevstul
-- Start date: 	25.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE list_line_type
IS
BEGIN
DECLARE

	CURSOR 	select_all IS
	SELECT 	line_type_pk, name, description, language_fk
	FROM	line_type;

	v_line_type_pk		line_type.line_type_pk%type	default NULL;
	v_name			line_type.name%type		default NULL;
	v_description		line_type.description%type	default NULL;
	v_language_fk		line_type.language_fk%type	default NULL;

	v_language_name		la.language_name%type		default NULL;

BEGIN
if ( login.timeout('lines.startup')>0 AND login.right('lines')>0 ) then

	html.b_adm_page('line');
	html.b_box('administrate "line type"');
	html.b_table;

	htp.p('<tr><td><b>pk:</b></td><td><b>name:</b></td><td><b>description:</b></td><td><b>language_fk:</b></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>');

	open select_all;
	loop
		fetch select_all into v_line_type_pk, v_name, v_description, v_language_fk;
		exit when select_all%NOTFOUND;

		SELECT 	language_name INTO v_language_name
		FROM	la
		WHERE	language_pk = v_language_fk;

		htp.p('<tr><td>'|| v_line_type_pk ||'</td><td>'|| v_name ||'</td><td>'|| v_description ||'</td><td>'|| v_language_name ||'</td><td>');
		html.button_link('add/rem service','lines.line_type_service?p_line_type_fk='||v_line_type_pk);
		htp.p('</td><td>');
		html.button_link('add/rem lines','lines.list_line?p_line_type_pk='||v_line_type_pk);
		htp.p('</td><td>');
		html.button_link('update', 'lines.update_line_type?p_line_type_pk='||v_line_type_pk);
		htp.p('</td><td>');
		html.button_link('delete', 'lines.delete_line_type?p_line_type_pk='||v_line_type_pk);
		htp.p('</td></tr>');

	end loop;
	close select_all;
	html.e_table;


	/* kode til insert form */
	html.b_form('lines.insert_line_type');
	html.b_table('40%');
	htp.p('
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr bgcolor="#eeeeee"><td colspan="2" align="center"><b>add new entry in "line_type"</b></td></tr>
		<tr><td>name:</td><td><input type="Text" name="p_name" size="30" maxlength="50"></td></tr>
		<tr><td>description:</td><td><input type="Text" name="p_description" size="50" maxlength="50"></td></tr>
		<tr><td>language:</td><td>'); html.select_lang(NULL, -1000); htp.p('</td></tr>
		<tr><td colspan="2" align="right">'); html.submit_link('insert'); htp.p('
		</td></tr>
	');
	html.e_table;
	html.e_form;


	html.e_box;
	html.e_adm_page;

end if;
END;
END list_line_type;




---------------------------------------------------------
-- Name: 	insert_line_type
-- Type: 	procedure
-- What: 	inserts the entry into the database
-- Author:  	Frode Klevstul
-- Start date: 	25.07.2000
---------------------------------------------------------
PROCEDURE insert_line_type
		(
			p_line_type_pk		in line_type.line_type_pk%type	 	default NULL,
			p_name			in line_type.name%type	 		default NULL,
			p_description		in line_type.description%type	 	default NULL,
	                p_language_pk  		in la.language_pk%type			default NULL
                )
IS
BEGIN
DECLARE

	v_line_type_pk		line_type.line_type_pk%type		default NULL;
	v_name			line_type.name%type			default NULL;
	v_description		line_type.description%type		default NULL;
	v_language_fk		line_type.language_fk%type		default NULL;

	v_check			number					default NULL;

BEGIN
if ( login.timeout('lines.startup')>0 AND login.right('lines')>0 ) then


	-- sjekker om det allerede finnes en forekomst med dette navnet
	if ( p_line_type_pk IS NOT NULL ) then
		SELECT 	count(*) INTO v_check
		FROM 	line_type
		WHERE 	name = p_name
		AND 	line_type_pk <> p_line_type_pk;
	else
		SELECT 	count(*) INTO v_check
		FROM 	line_type
		WHERE 	name = p_name;
	end if;

	if (v_check > 0) then
		htp.p( get.msg(1, 'there already exists an entry with name = <b>'||p_name||'</b>') );
		html.e_page; -- slutter siden
	else
	begin
		v_description := p_description;
		v_description := html.rem_tag(v_description);

		-- henter ut neste nummer i sekvensen til tabellen
		if (p_line_type_pk is NULL AND p_name IS NOT NULL) then
			SELECT line_type_seq.NEXTVAL
			INTO v_line_type_pk
			FROM dual;
			-- legger inn i databasen
			INSERT INTO line_type
			(line_type_pk, name, description, language_fk)
			VALUES (v_line_type_pk, p_name, p_description, p_language_pk);
			commit;
		elsif (p_name IS NOT NULL) then
		begin
			UPDATE line_type
			SET name = p_name, description = p_description, language_fk = p_language_pk
			WHERE line_type_pk = p_line_type_pk;
			commit;
		end;
		end if;
	end;
	end if;
	list_line_type;


end if;
END;
END insert_line_type;



---------------------------------------------------------
-- Name: 	delete_line_type
-- Type: 	procedure
-- What: 	confirm a deletion of an entry in the database
-- Author:  	Frode Klevstul
-- Start date: 	26.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE delete_line_type
		(
			p_line_type_pk 		in line_type.line_type_pk%type	default NULL
                )
IS
BEGIN
DECLARE

	v_name		line_type.name%type	default NULL;

BEGIN
if ( login.timeout('lines.startup')>0 AND login.right('lines')>0 ) then

	SELECT name INTO v_name
	FROM line_type
	WHERE line_type_pk = p_line_type_pk;

	html.b_adm_page('line');
	html.b_box('delete line_type');
	html.b_form('lines.delete_line_type_2');
	html.b_table;

	htp.p('
		<input type="hidden" name="p_line_type_pk" value="'|| p_line_type_pk ||'">
		<tr><td colspan="2">Are you sure you want to delete the entry <b>'|| v_name ||'</b> with pk <b>'||p_line_type_pk||'</b>?</td></tr>
		<tr><td><input type="submit" name="p_confirm" value="yes"></td><td align="right"><input type="submit" name="p_confirm" value="no"></td></tr>
		</table>
		</form>
	');

	html.e_table;
	html.e_form;
	html.e_box;
	html.e_page;

end if;

END;
END delete_line_type;


---------------------------------------------------------
-- Name: 	delete_line_type_2
-- Type: 	procedure
-- What: 	deletes the entry from the database
-- Author:  	Frode Klevstul
-- Start date: 	26.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE delete_line_type_2
		(
			p_line_type_pk 	in line_type.line_type_pk%type		default NULL,
			p_confirm	in varchar2				default NULL
                )
IS
BEGIN
if ( login.timeout('lines.startup')>0 AND login.right('lines')>0 ) then

	if (p_confirm = 'yes') then
		DELETE FROM line_type
		WHERE line_type_pk = p_line_type_pk;
		commit;
	end if;
	list_line_type;


end if;
END delete_line_type_2;




---------------------------------------------------------
-- Name: 	update_line_type
-- Type: 	procedure
-- What: 	updates an entry
-- Author:  	Frode Klevstul
-- Start date: 	26.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE update_line_type
		(
			p_line_type_pk 		in line_type.line_type_pk%type	default NULL
                )
IS
BEGIN
DECLARE

	v_name		line_type.name%type			default NULL;
	v_description	line_type.description%type		default NULL;
	v_language_fk	line_type.language_fk%type		default NULL;

BEGIN
if ( login.timeout('lines.startup')>0 AND login.right('lines')>0 ) then


	SELECT 	name, description, language_fk INTO v_name, v_description, v_language_fk
	FROM 	line_type
	WHERE 	line_type_pk = p_line_type_pk;

	v_description := html.rem_tag(v_description);

	html.b_adm_page('line');
	html.b_box('update line_type');
	html.b_form('lines.insert_line_type');
	html.b_table;


	htp.p('
		<tr><td colspan="2">&nbsp;</td></tr>
		<input type="hidden" name="p_line_type_pk" value="'||p_line_type_pk||'">
		<tr bgcolor="#eeeeee"><td colspan="2" align="center"><b>update entry in "line_type"</b></td></tr>
		<tr><td>name:</td><td><input type="Text" name="p_name" size="30" maxlength="50" value="'||v_name||'"></td></tr>
		<tr><td>description:</td><td><input type="Text" name="p_description" size="50" maxlength="50" value="'||v_description||'"></td></tr>
		<tr><td>language:</td><td>'); html.select_lang(v_language_fk, -1000); htp.p('</td></tr>
		<tr><td colspan="2" align="right">'); html.submit_link('update'); htp.p('
		</td></tr>
	');


	html.e_table;
	html.e_form;
	html.e_box;
	html.e_page;


end if;
END;
END update_line_type;




---------------------------------------------------------
-- Name: 	list_line
-- Type: 	procedure
-- What: 	procedure to list all lines
-- Author:  	Frode Klevstul
-- Start date: 	25.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE list_line
	(
		p_line_type_pk		in line_type.line_type_pk%type	default NULL
	)
IS
BEGIN
DECLARE


	CURSOR 	select_all(v_line_type_fk in line.line_type_fk%type) IS
	SELECT 	line_pk, accepted, line, who_said, where_said, when_said
	FROM	line
	WHERE	line_type_fk = v_line_type_fk;


	v_line_pk		line.line_pk%type		default NULL;
	v_accepted		line.accepted%type		default NULL;
	v_line			line.line%type			default NULL;
	v_who_said		line.who_said%type		default NULL;
	v_where_said		line.where_said%type		default NULL;
	v_when_said		line.when_said%type		default NULL;

	v_name			line_type.name%type		default NULL;

BEGIN
if ( login.timeout('lines.startup')>0 AND login.right('lines')>0 ) then

	SELECT 	name INTO v_name
	FROM	line_type
	WHERE	line_type_pk = p_line_type_pk;

	html.b_adm_page('line');
	html.b_box('administrate "line" with type "'||v_name||'"');
	html.b_table;

	htp.p('<tr><td><b>pk:</b></td><td><b>line:</b></td><td><b>who:</b></td><td><b>where:</b></td><td><b>when:</b></td><td><b>accepted:</b></td><td>&nbsp;</td><td>&nbsp;</td></tr>');

	open select_all(p_line_type_pk);
	loop
		fetch select_all into v_line_pk, v_accepted, v_line, v_who_said, v_where_said, v_when_said;
		exit when select_all%NOTFOUND;

		if ( v_accepted = 1 ) then
			v_name := 'YES';
		else
			v_name := '<font color="#ff0000">NO</font>';
		end if;

		htp.p('<tr><td>'|| v_line_pk ||'</td><td>'|| v_line ||'</td><td>'|| v_who_said ||'</td><td>'|| v_where_said ||'</td><td>'|| v_when_said ||'</td><td>'|| v_name ||'</td><td>'); html.button_link('update', 'lines.update_line?p_line_pk='||v_line_pk); htp.p('</td><td>'); html.button_link('delete', 'lines.delete_line?p_line_pk='||v_line_pk); htp.p('</td></tr>');

	end loop;
	close select_all;
	html.e_table;


	-- kode til insert form --
	html.b_form('lines.insert_line');
	html.b_table('85%');
	htp.p('
		<input type="hidden" name="p_line_type_fk" value="'||p_line_type_pk||'">
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr bgcolor="#eeeeee"><td colspan="2" align="center"><b>add new entry in "line"</b></td></tr>
		<tr><td valign="top">line:</td><td><textarea name="p_line" rows="5" cols="100"></textarea></td></tr>
		<tr><td>who said:</td><td><input type="Text" name="p_who_said" size="100" maxlength="200"></td></tr>
		<tr><td>where said:</td><td><input type="Text" name="p_where_said" size="100" maxlength="200"></td></tr>
		<tr><td>when said:</td><td><input type="Text" name="p_when_said" size="100" maxlength="200"></td></tr>
		<tr><td>accepted:</td><td><select name="p_accepted"><option value="1">YES</option><option value="">NO</option></select></td>
		<tr><td colspan="2" align="right">'); html.submit_link('insert'); htp.p('
		</td></tr>
	');
	html.e_table;
	html.e_form;



	html.e_table;
	html.e_box;
	html.e_page;

end if;
END;
END list_line;




---------------------------------------------------------
-- Name: 	insert_line
-- Type: 	procedure
-- What: 	inserts the entry into the database
-- Author:  	Frode Klevstul
-- Start date: 	26.07.2000
---------------------------------------------------------
PROCEDURE insert_line
		(
			p_line_pk		in line.line_pk%type	 		default NULL,
			p_line_type_fk		in line.line_type_fk%type		default NULL,
			p_accepted		in line.accepted%type	 		default NULL,
			p_line			in line.line%type	 		default NULL,
			p_who_said		in line.who_said%type	 		default NULL,
			p_where_said		in line.where_said%type	 		default NULL,
			p_when_said		in line.when_said%type	 		default NULL
                )
IS
BEGIN
DECLARE

	v_line_pk		line.line_pk%type	 		default NULL;
	v_line_type_fk		line.line_type_fk%type			default NULL;
	v_accepted		line.accepted%type	 		default NULL;
	v_line			line.line%type	 			default NULL;
	v_who_said		line.who_said%type	 		default NULL;
	v_where_said		line.where_said%type	 		default NULL;
	v_when_said		line.when_said%type	 		default NULL;

	v_check			number					default NULL;

BEGIN
if ( login.timeout('lines.startup')>0 AND login.right('lines')>0 ) then


	-- sjekker om det allerede finnes en forekomst med dette navnet
	if ( p_line_pk IS NOT NULL ) then
		SELECT 	count(*) INTO v_check
		FROM 	line
		WHERE 	line = p_line
		AND 	line_pk <> p_line_pk;
	else
		SELECT 	count(*) INTO v_check
		FROM 	line
		WHERE 	line = p_line;
	end if;

	if (v_check > 0) then
		htp.p( get.msg(1, 'there already exists an entry with line = <b>'||p_line||'</b>') );
		html.e_page; -- slutter siden
	else
	begin

		-- henter ut neste nummer i sekvensen til tabellen
		if (p_line_pk is NULL AND p_line IS NOT NULL) then
			SELECT line_seq.NEXTVAL
			INTO v_line_pk
			FROM dual;
			-- legger inn i databasen
			INSERT INTO line
			(line_pk, line_type_fk, accepted, line, who_said, where_said, when_said)
			VALUES (v_line_pk, p_line_type_fk, p_accepted, p_line, p_who_said, p_where_said, p_when_said);
			commit;
		elsif (p_line IS NOT NULL) then
		begin
			UPDATE line
			SET accepted = p_accepted, line = p_line, who_said = p_who_said, where_said = p_where_said, when_said = p_when_said
			WHERE line_pk = p_line_pk;
			commit;
		end;
		end if;
	end;
	end if;
	list_line(p_line_type_fk);


end if;
END;
END insert_line;








---------------------------------------------------------
-- Name: 	update_line
-- Type: 	procedure
-- What: 	updates an entry
-- Author:  	Frode Klevstul
-- Start date: 	26.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE update_line
		(
			p_line_pk 		in line.line_pk%type	default NULL
                )
IS
BEGIN
DECLARE

	v_line_type_fk		line.line_type_fk%type			default NULL;
	v_accepted		line.accepted%type	 		default NULL;
	v_line			line.line%type	 			default NULL;
	v_who_said		line.who_said%type	 		default NULL;
	v_where_said		line.where_said%type	 		default NULL;
	v_when_said		line.when_said%type	 		default NULL;

BEGIN
if ( login.timeout('lines.startup')>0 AND login.right('lines')>0 ) then


	SELECT 	accepted, line, who_said, where_said, when_said, line_type_fk
	INTO	v_accepted, v_line, v_who_said, v_where_said, v_when_said, v_line_type_fk
	FROM	line
	WHERE	line_pk = p_line_pk;

	v_line := html.rem_tag(v_line);

	html.b_adm_page('line');
	html.b_box('update line');
	html.b_form('lines.insert_line');
	html.b_table;


	htp.p('
		<tr><td colspan="2">&nbsp;</td></tr>
		<input type="hidden" name="p_line_pk" value="'||p_line_pk||'">
		<input type="hidden" name="p_line_type_fk" value="'||v_line_type_fk||'">
		<tr bgcolor="#eeeeee"><td colspan="2" align="center"><b>update entry in "line"</b></td></tr>
		<tr><td valign="top">line:</td><td><textarea name="p_line" rows="5" cols="100">'||v_line||'</textarea></td></tr>
		<tr><td>who said:</td><td><input type="Text" name="p_who_said" size="100" maxlength="200" value="'||v_who_said||'"></td></tr>
		<tr><td>where said:</td><td><input type="Text" name="p_where_said" size="100" maxlength="200" value="'||v_where_said||'"></td></tr>
		<tr><td>when said:</td><td><input type="Text" name="p_when_said" size="100" maxlength="200" value="'||v_when_said||'"></td></tr>
		<tr><td>accepted:</td><td><select name="p_accepted" size="1">
			');
			if  (v_accepted = 1) then
				htp.p('<option value="1" selected>YES</option><option value="">NO</option>');
			else
				htp.p('<option value="1">YES</option><option value="" selected>NO</option>');
			end if;

			htp.p('</select></td>
		<tr><td colspan="2" align="right">'); html.submit_link('update'); htp.p('
		</td></tr>
	');


	html.e_table;
	html.e_form;
	html.e_box;
	html.e_page;


end if;
END;
END update_line;






---------------------------------------------------------
-- Name: 	line_type_service
-- Type: 	procedure
-- What: 	adm av line_type og service
-- Author:  	Frode Klevstul
-- Start date: 	26.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE line_type_service
	(
		p_line_type_fk 		in line_type_on_service.line_type_fk%type	default NULL
	)
IS
BEGIN
DECLARE

	CURSOR 	select_all IS
	SELECT 	service_fk, name_sg_fk
	FROM 	service, line_type_on_service
	WHERE	service_pk = service_fk
	AND	line_type_fk = p_line_type_fk;

	v_service_fk		line_type_on_service.service_fk%type	default NULL;
	v_name_sg_fk		service.name_sg_fk%type			default NULL;
	v_name			line_type.name%type			default NULL;


BEGIN
if ( login.timeout('lines.startup')>0 AND login.right('lines')>0 ) then

	SELECT 	name INTO v_name
	FROM 	line_type
	WHERE 	line_type_pk = p_line_type_fk;

	html.b_adm_page('line');
	html.b_box('services connected to line type "'|| v_name ||'"');
	html.b_table('40%');
	htp.p('<tr bgcolor="#eeeeee"><td colspan="2">The line type "'||v_name||'" is on following services:</td></tr>');


	open select_all;
	loop
		fetch select_all into v_service_fk, v_name_sg_fk;
		exit when select_all%NOTFOUND;
		htp.p('<tr><td>'|| get.text(v_name_sg_fk) ||'</td><td>'); html.button_link('remove', 'lines.remove_service?p_line_type_fk='||p_line_type_fk||'&p_service_fk='||v_service_fk); htp.p('</td></tr>');
	end loop;
	close select_all;
	html.e_table;


	-- form for � legge til service
	html.b_form('lines.add_service');
	htp.p('<input type="hidden" name="p_line_type_fk" value="'|| p_line_type_fk ||'">');

	html.b_table('50%');
	htp.p('
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr bgcolor="#eeeeee"><td colspan="2">add service to line type "'||v_name||'":</td></tr>
		<tr><td>service:</td><td>
	');

		html.select_service(NULL, -1000);

	htp.p('
		</td></tr>
		<tr><td colspan="2" align="right">'); html.submit_link('add service'); htp.p('</td></tr>
	');

	html.e_table;
	html.e_form;
	html.e_box;
	html.e_page;


end if;
END;
END line_type_service;



---------------------------------------------------------
-- Name: 	add_service
-- Type: 	procedure
-- What: 	adds service to a line type
-- Author:  	Frode Klevstul
-- Start date: 	26.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE add_service
		(
			p_line_type_fk		in line_type_on_service.line_type_fk%type 		default NULL,
			p_service_pk 		in line_type_on_service.service_fk%type 		default NULL
                )
IS
BEGIN
DECLARE

	v_check			number					default 0;

BEGIN
if ( login.timeout('lines.startup')>0 AND login.right('lines')>0 ) then

	SELECT 	count(*) INTO v_check
	FROM 	line_type_on_service
	WHERE	service_fk = p_service_pk
	AND	line_type_fk = p_line_type_fk;


	if ( v_check = 0 ) then
		INSERT INTO line_type_on_service
		(line_type_fk, service_fk)
		VALUES (p_line_type_fk, p_service_pk);
		commit;
	end if;


	line_type_service(p_line_type_fk);


end if;
END;
END add_service;





---------------------------------------------------------
-- Name: 	remove_service
-- Type: 	procedure
-- What: 	remove user connected to a module
-- Author:  	Frode Klevstul
-- Start date: 	21.06.2000
---------------------------------------------------------
PROCEDURE remove_service
		(
			p_line_type_fk		in line_type_on_service.line_type_fk%type 	default NULL,
			p_service_fk 		in line_type_on_service.service_fk%type 	default NULL
                )
IS
BEGIN
if ( login.timeout('lines.startup')>0 AND login.right('lines')>0 ) then

	DELETE FROM line_type_on_service
	WHERE	line_type_fk = p_line_type_fk
	AND	service_fk = p_service_fk;
	commit;

	line_type_service(p_line_type_fk);

end if;
END remove_service;





---------------------------------------------------------
-- Name: 	delete_line
-- Type: 	procedure
-- What: 	confirm a deletion of an entry in the database
-- Author:  	Frode Klevstul
-- Start date: 	29.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE delete_line
		(
			p_line_pk 		in line.line_pk%type		default NULL
                )
IS
BEGIN
DECLARE

	v_line		line.line%type		default NULL;

BEGIN
if ( login.timeout('lines.startup')>0 AND login.right('lines')>0 ) then

	SELECT line INTO v_line
	FROM line
	WHERE line_pk = p_line_pk;

	html.b_adm_page('line');
	html.b_box('delete line');
	html.b_form('lines.delete_line_2');
	html.b_table;

	htp.p('
		<input type="hidden" name="p_line_pk" value="'|| p_line_pk ||'">
		<tr><td colspan="2">Are you sure you want to delete the entry <b>'|| v_line ||'</b> with pk <b>'||p_line_pk||'</b>?</td></tr>
		<tr><td><input type="submit" name="p_confirm" value="yes"></td><td align="right"><input type="submit" name="p_confirm" value="no"></td></tr>
		</table>
		</form>
	');

	html.e_table;
	html.e_form;
	html.e_box;
	html.e_page;

end if;

END;
END delete_line;


---------------------------------------------------------
-- Name: 	delete_line_2
-- Type: 	procedure
-- What: 	deletes the entry from the database
-- Author:  	Frode Klevstul
-- Start date: 	29.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE delete_line_2
		(
			p_line_pk 	in line.line_pk%type			default NULL,
			p_confirm	in varchar2				default NULL
                )
IS
BEGIN
DECLARE
	v_line_type_pk		line_type.line_type_pk%type			default NULL;

BEGIN
if ( login.timeout('lines.startup')>0 AND login.right('lines')>0 ) then

	SELECT 	line_type_fk INTO v_line_type_pk
	FROM 	line
	WHERE	line_pk = p_line_pk;

	if (p_confirm = 'yes') then
		DELETE FROM line
		WHERE line_pk = p_line_pk;
		commit;
	end if;
	list_line(v_line_type_pk);


end if;
END;
END delete_line_2;




-- ++++++++++++++++++++++++++++++++++++++++++++++ --

END; -- slutter pakke kroppen
/
show errors;
