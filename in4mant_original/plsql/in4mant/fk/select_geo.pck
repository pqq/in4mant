CREATE OR REPLACE PACKAGE select_geo IS

	PROCEDURE startup;
	PROCEDURE select_location(
			p_no_levels	in number			default NULL,
			p_level_no	in number			default 0,
			p_level0	in geography.level0%type	default NULL,
			p_level1	in geography.level1%type	default NULL,
			p_level2	in geography.level2%type	default NULL,
			p_level3	in geography.level3%type	default NULL,
			p_level4	in geography.level4%type	default NULL,
			p_level5	in geography.level5%type	default NULL
		);

END;
/
CREATE OR REPLACE PACKAGE BODY select_geo IS


---------------------------------------------------------
-- Name: 	startup
-- Type: 	procedure
-- What: 	start prosedyren, for � kj�re pakken
-- Author:  	Frode Klevstul
-- Start date: 	07.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

	select_location;

END startup;


---------------------------------------------------------
-- Name: 	select_location
-- Type: 	procedure
-- What: 	select an geographical location
-- Author:  	Frode Klevstul
-- Start date: 	07.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE select_location(
		p_no_levels	in number			default NULL,
		p_level_no	in number			default 0,
		p_level0	in geography.level0%type	default NULL,
		p_level1	in geography.level1%type	default NULL,
		p_level2	in geography.level2%type	default NULL,
		p_level3	in geography.level3%type	default NULL,
		p_level4	in geography.level4%type	default NULL,
		p_level5	in geography.level5%type	default NULL
	)
IS
BEGIN
DECLARE


	CURSOR 	select_loc_0 IS
	SELECT 	name_sg_fk, level0
	FROM 	geography, string_group, groups, strng
	WHERE	level1 IS NULL
	AND	name_sg_fk = string_group_pk
	AND	string_group_fk = string_group_pk
	AND	string_fk = string_pk
	AND	language_fk = get.lan
	ORDER BY string;

	CURSOR 	select_loc_1 IS
	SELECT 	name_sg_fk, level0, level1
	FROM 	geography, string_group, groups, strng
	WHERE	level0 = p_level0
	AND	level1 IS NOT NULL
	AND	level2 IS NULL
	AND	name_sg_fk = string_group_pk
	AND	string_group_fk = string_group_pk
	AND	string_fk = string_pk
	AND	language_fk = get.lan
	ORDER BY string;

	CURSOR 	select_loc_2 IS
	SELECT 	name_sg_fk, level0, level1, level2
	FROM 	geography, string_group, groups, strng
	WHERE	level1 = p_level1
	AND	level2 IS NOT NULL
	AND	level3 IS NULL
	AND	name_sg_fk = string_group_pk
	AND	string_group_fk = string_group_pk
	AND	string_fk = string_pk
	AND	language_fk = get.lan
	ORDER BY string;

	CURSOR 	select_loc_3 IS
	SELECT 	name_sg_fk, level0, level1, level2, level3
	FROM 	geography, string_group, groups, strng
	WHERE	level2 = p_level2
	AND	level3 IS NOT NULL
	AND	level4 IS NULL
	AND	name_sg_fk = string_group_pk
	AND	string_group_fk = string_group_pk
	AND	string_fk = string_pk
	AND	language_fk = get.lan
	ORDER BY string;

	CURSOR 	select_loc_4 IS
	SELECT 	name_sg_fk, level0, level1, level2, level3, level4
	FROM 	geography, string_group, groups, strng
	WHERE	level3 = p_level3
	AND	level4 IS NOT NULL
	AND	level5 IS NULL
	AND	name_sg_fk = string_group_pk
	AND	string_group_fk = string_group_pk
	AND	string_fk = string_pk
	AND	language_fk = get.lan
	ORDER BY string;

	CURSOR 	select_loc_5 IS
	SELECT 	name_sg_fk, level0, level1, level2, level3, level4, level5
	FROM 	geography, string_group, groups, strng
	WHERE	level4 = p_level4
	AND	level5 IS NOT NULL
	AND	name_sg_fk = string_group_pk
	AND	string_group_fk = string_group_pk
	AND	string_fk = string_pk
	AND	language_fk = get.lan
	ORDER BY string;

	v_name_sg_fk		geography.name_sg_fk%type	default NULL;
	v_next_level		number				default NULL;
	v_level0		geography.level0%type		default NULL;
	v_level1		geography.level1%type		default NULL;
	v_level2		geography.level2%type		default NULL;
	v_level3		geography.level3%type		default NULL;
	v_level4		geography.level4%type		default NULL;
	v_level5		geography.level5%type		default NULL;

	v_geography_pk		geography.geography_pk%type	default NULL;
	v_name			string_group.name%type		default NULL;

BEGIN

	-- hopper direkte til Norge
	if (p_level_no < 2) then
		html.jump_to('select_geo.select_location?p_no_levels='|| p_no_levels ||'&p_level_no=2&p_level0=1&p_level1=2');
		return;
	end if;


	-- teller opp hvilken level/hvilket niv� som kommer etterp�
	v_next_level := p_level_no + 1;

	if (p_level_no < 6 AND (p_level_no<p_no_levels) ) then
		html.b_page_2( 280 );
		html.b_box( get.txt('select_geo_step_'|| p_level_no), '100%', 'select_geo.select_location' );

		html.b_table('90%');
		html.b_select_jump;
		htp.p('<option value="" selected> ['|| get.txt('select_geo_step_'|| p_level_no) ||'] </option>');

		if (p_level_no = 0) then
			open select_loc_0;
			loop
				fetch select_loc_0 into v_name_sg_fk, v_level0;
				exit when select_loc_0%NOTFOUND;
				htp.p('<option value="select_geo.select_location?p_no_levels='|| p_no_levels ||'&p_level_no='|| v_next_level ||'&p_level0='|| v_level0 ||'">'|| get.text(v_name_sg_fk) ||'</option>');
			end loop;
			if (select_loc_0%ROWCOUNT = 0) then
				htp.p('</select>');
				html.jump_to('select_geo.select_location?p_no_levels='||p_level_no||'&p_level_no='||p_level_no||'&p_level0='|| p_level0 ||'&p_level1='|| p_level1 ||'&p_level2='|| p_level2 ||'&p_level3='|| p_level3 ||'&p_level4='|| p_level4 ||'&p_level5='|| p_level5);
			end if;
			close select_loc_0;
		elsif (p_level_no = 1) then
			open select_loc_1;
			loop
				fetch select_loc_1 into v_name_sg_fk, v_level0, v_level1;
				exit when select_loc_1%NOTFOUND;
				htp.p('<option value="select_geo.select_location?p_no_levels='|| p_no_levels ||'&p_level_no='|| v_next_level ||'&p_level0='|| v_level0 ||'&p_level1='|| v_level1 ||'">'|| get.text(v_name_sg_fk) ||'</option>');
			end loop;
			if (select_loc_1%ROWCOUNT = 0) then
				htp.p('</select>');
				html.jump_to('select_geo.select_location?p_no_levels='||p_level_no||'&p_level_no='||p_level_no||'&p_level0='|| p_level0 ||'&p_level1='|| p_level1 ||'&p_level2='|| p_level2 ||'&p_level3='|| p_level3 ||'&p_level4='|| p_level4 ||'&p_level5='|| p_level5);
			end if;
			close select_loc_1;
		elsif (p_level_no = 2) then
			open select_loc_2;
			loop
				fetch select_loc_2 into v_name_sg_fk, v_level0, v_level1, v_level2;
				exit when select_loc_2%NOTFOUND;
				htp.p('<option value="select_geo.select_location?p_no_levels='|| p_no_levels ||'&p_level_no='|| v_next_level ||'&p_level0='|| v_level0 ||'&p_level1='|| v_level1 ||'&p_level2='|| v_level2 ||'">'|| get.text(v_name_sg_fk) ||'</option>');
			end loop;
			if (select_loc_2%ROWCOUNT = 0) then
				htp.p('</select>');
				html.jump_to('select_geo.select_location?p_no_levels='||p_level_no||'&p_level_no='||p_level_no||'&p_level0='|| p_level0 ||'&p_level1='|| p_level1 ||'&p_level2='|| p_level2 ||'&p_level3='|| p_level3 ||'&p_level4='|| p_level4 ||'&p_level5='|| p_level5);
			end if;
			close select_loc_2;
		elsif (p_level_no = 3) then
			open select_loc_3;
			loop
				fetch select_loc_3 into v_name_sg_fk, v_level0, v_level1, v_level2, v_level3;
				exit when select_loc_3%NOTFOUND;
				htp.p('<option value="select_geo.select_location?p_no_levels='|| p_no_levels ||'&p_level_no='|| v_next_level ||'&p_level0='|| v_level0 ||'&p_level1='|| v_level1 ||'&p_level2='|| v_level2 ||'&p_level3='|| v_level3 ||'">'|| get.text(v_name_sg_fk) ||'</option>');
			end loop;
			if (select_loc_3%ROWCOUNT = 0) then
				htp.p('</select>');
				html.jump_to('select_geo.select_location?p_no_levels='||p_level_no||'&p_level_no='||p_level_no||'&p_level0='|| p_level0 ||'&p_level1='|| p_level1 ||'&p_level2='|| p_level2 ||'&p_level3='|| p_level3 ||'&p_level4='|| p_level4 ||'&p_level5='|| p_level5);
			end if;
			close select_loc_3;
		elsif (p_level_no = 4) then
			open select_loc_4;
			loop
				fetch select_loc_4 into v_name_sg_fk, v_level0, v_level1, v_level2, v_level3, v_level4;
				exit when select_loc_4%NOTFOUND;
				htp.p('<option value="select_geo.select_location?p_no_levels='|| p_no_levels ||'&p_level_no='|| v_next_level ||'&p_level0='|| v_level0 ||'&p_level1='|| v_level1 ||'&p_level2='|| v_level2 ||'&p_level3='|| v_level3 ||'&p_level4='|| v_level4 ||'">'|| get.text(v_name_sg_fk) ||'</option>');
			end loop;
			if (select_loc_4%ROWCOUNT = 0) then
				htp.p('</select>');
				html.jump_to('select_geo.select_location?p_no_levels='||p_level_no||'&p_level_no='||p_level_no||'&p_level0='|| p_level0 ||'&p_level1='|| p_level1 ||'&p_level2='|| p_level2 ||'&p_level3='|| p_level3 ||'&p_level4='|| p_level4 ||'&p_level5='|| p_level5);
			end if;
			close select_loc_4;
		elsif (p_level_no = 5) then
			open select_loc_5;
			loop
				fetch select_loc_5 into v_name_sg_fk, v_level0, v_level1, v_level2, v_level3, v_level4, v_level5;
				exit when select_loc_5%NOTFOUND;
				htp.p('<option value="select_geo.select_location?p_no_levels='|| p_no_levels ||'&p_level_no='|| v_next_level ||'&p_level0='|| v_level0 ||'&p_level1='|| v_level1 ||'&p_level2='|| v_level2 ||'&p_level3='|| v_level3 ||'&p_level4='|| v_level4 ||'&p_level5='|| v_level5 ||'">'|| get.text(v_name_sg_fk) ||'</option>');
			end loop;
			if (select_loc_5%ROWCOUNT = 0) then
				htp.p('</select>');
				html.jump_to('select_geo.select_location?p_no_levels='||p_level_no||'&p_level_no='||p_level_no||'&p_level0='|| p_level0 ||'&p_level1='|| p_level1 ||'&p_level2='|| p_level2 ||'&p_level3='|| p_level3 ||'&p_level4='|| p_level4 ||'&p_level5='|| p_level5);
			end if;
			close select_loc_5;
		end if;
	else
		if ( p_level_no = 1) then
			v_geography_pk := p_level0;
		elsif ( p_level_no = 2) then
			v_geography_pk := p_level1;
		elsif ( p_level_no = 3) then
			v_geography_pk := p_level2;
		elsif ( p_level_no = 4) then
			v_geography_pk := p_level3;
		elsif ( p_level_no = 5) then
			v_geography_pk := p_level4;
		elsif ( p_level_no = 6) then
			v_geography_pk := p_level5;
		end if;


		SELECT 	name_sg_fk INTO v_name_sg_fk
		FROM 	geography
		WHERE	geography_pk = v_geography_pk;

		-- m� fjerne newline pluss andre "non-word characters" (slik at ikke javascriptet kr�sjer)
		v_name := get.text(v_name_sg_fk);
		owa_pattern.change(v_name, '\W*$', NULL, 'ig');

		htp.p('

			<SCRIPT LANGUAGE=''JavaScript''>
				function update_geo(pk, name) {
						if (window.opener.document.form) {
							if (window.opener.document.form.p_geo_name) {
								window.opener.document.form.p_geography_pk.value = pk;
								window.opener.document.form.p_geo_name.value = name;
							}
						}
						window.close();
				}
			</SCRIPT>

			<SCRIPT language=''JavaScript''>
				update_geo('''|| v_geography_pk ||''', '''|| v_name ||''');
			</SCRIPT>
		');

	end if;


	html.e_select_jump;


	if ( p_level_no > 0) then

		-- m� bruke javascript for � f� dette til � virke i Netscape
		htp.p('
			<script language=''Javascript''>
			<!--
			function go_to() {
				window.location = ''select_geo.select_location?p_no_levels='||p_level_no||'&p_level_no='||p_level_no||'&p_level0='|| p_level0 ||'&p_level1='|| p_level1 ||'&p_level2='|| p_level2 ||'&p_level3='|| p_level3 ||'&p_level4='|| p_level4 ||'&p_level5='|| p_level5 ||''';
			}
			//-->
			</script>
			<form>
			<input type="button" value="'|| get.txt('stop_at_this_level')||' " onClick="Javascript:go_to()">
			</form>
		');
		html.back( get.txt('back') );
	end if;


	html.e_table;
	html.e_box;
	html.e_page_2;


END;
END select_location;


-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/
