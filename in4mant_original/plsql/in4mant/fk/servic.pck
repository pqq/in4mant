CREATE OR REPLACE PACKAGE servic IS

	PROCEDURE startup;
	PROCEDURE list_all;
	PROCEDURE update_lang
		(
			p_service_pk 		in service.service_pk%type	default NULL
		);
	PROCEDURE add_lang
		(
			p_service_pk		in service.service_pk%type 		default NULL,
			p_language_pk 		in la.language_pk%type	 		default NULL
                );
	PROCEDURE remove_lang
		(
			p_service_pk		in service.service_pk%type 		default NULL,
			p_language_pk 		in la.language_pk%type	 		default NULL
                );
	PROCEDURE turn_on
		(
			p_service_pk		in service.service_pk%type 		default NULL
		);
	PROCEDURE turn_off
		(
			p_service_pk		in service.service_pk%type 		default NULL
		);

END;
/
CREATE OR REPLACE PACKAGE BODY servic IS

-- **************************************************
-- "PUBLIC METODER" (KALLES DIREKTE FRA WEB)
-- **************************************************


---------------------------------------------------------
-- Name: 	startup
-- Type: 	procedure
-- What: 	start prosedyren, for � kj�re pakken
-- Author:  	Frode Klevstul
-- Start date: 	25.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

	list_all;

END startup;



-- **************************************************
-- "PRIVATE METODER" (KALLES IKKE DIREKTE FRA WEB)
-- **************************************************



---------------------------------------------------------
-- Name: 	list_all
-- Type: 	procedure
-- What: 	lister ut alle forekomster
-- Author:  	Frode Klevstul
-- Start date: 	06.06.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
-- beskrivelse av inn parametere:
--
-- [parameter]	[description]
---------------------------------------------------------
PROCEDURE list_all
IS
BEGIN
DECLARE

	CURSOR 	select_all IS
	SELECT 	service_pk, description, activated
	FROM 	service
	ORDER BY service_pk ASC;

	v_service_pk	service.service_pk%type 	default NULL;
	v_name		service.description%type 	default NULL;
	v_activated	service.activated%type		default NULL;

BEGIN

if ( login.timeout('servic.startup')>0 AND login.right('servic')>0 ) then

	html.b_adm_page('service');
	html.b_box('list all');
	html.b_table;

	htp.p('<tr bgcolor="#eeeeee"><td width="5%">pk:</td><td>name:</td><td>&nbsp;</td><td>&nbsp;</td></tr>');
	-- g�r igjennom "cursoren"...
	open select_all;
	while (1>0)	-- alltid sant -> g�r igjennom alle forekomstene
	loop
		fetch select_all into v_service_pk, v_name, v_activated;
		exit when select_all%NOTFOUND;
		htp.p('<tr><td width="5%"><font color="#dddddd">'|| v_service_pk ||'</font></td><td><b>'|| v_name ||'</b></td><td align="right">'); html.button_link('add/rem lang', 'servic.update_lang?p_service_pk='||v_service_pk); htp.p('</td><td align="right">');

		if (v_service_pk = 0) then
			htp.p('&nbsp;');
		else
			if (v_activated IS NULL) then
				html.button_link('turn ON', 'servic.turn_on?p_service_pk='||v_service_pk);
			else
				html.button_link('turn OFF', 'servic.turn_off?p_service_pk='||v_service_pk);
			end if;
		end if;

		htp.p('</td></tr>');
	end loop;
	close select_all;

	html.e_table;
	html.e_box;
	html.e_page;

end if;

END;
END list_all;





---------------------------------------------------------
-- Name: 	update_lang
-- Type: 	procedure
-- What: 	lister ut spr�k en tjeneste er p�
-- Author:  	Frode Klevstul
-- Start date: 	06.06.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
-- beskrivelse av inn parametere:
--
-- [parameter]	[description]
---------------------------------------------------------
PROCEDURE update_lang
	(
		p_service_pk 	in service.service_pk%type	default NULL
	)
IS
BEGIN
DECLARE

	CURSOR 	select_lang IS
	SELECT 	language_name, language_pk
	FROM 	la, service_on_lang
	WHERE 	language_fk = language_pk
	AND 	service_fk = p_service_pk;

	v_language_name		la.language_name%type	default NULL;
	v_language_pk		la.language_pk%type	default NULL;
	v_name			service.description%type		default NULL;

BEGIN

if ( login.timeout('servic.startup')>0 AND login.right('servic')>0 ) then

	SELECT 	description INTO v_name
	FROM 	service
	WHERE 	service_pk = p_service_pk;

	html.b_adm_page('service');
	html.b_box('languages connected to "'|| v_name ||'"');
	html.b_table('50%');
	htp.p('<tr bgcolor="#eeeeee"><td colspan="2">The service "'||v_name||'" is on following languages:</td></tr>');
	-- g�r igjennom "cursoren"...
	open select_lang;
	while (1>0)	-- alltid sant -> g�r igjennom alle forekomstene
	loop
		fetch select_lang into v_language_name, v_language_pk;
		exit when select_lang%NOTFOUND;
		if ( p_service_pk = 0 ) then-- hvis 'all_services' s� skal man ikke kunne fjerne spr�k
			htp.p('<tr><td>'||v_language_name||'</td><td bgcolor="#cccccc" width="10%"><font color="#ffffff">REMOVE</font></td></tr>');
		else
			htp.p('<tr><td>'||v_language_name||'</td><td bgcolor="#000000" width="10%"><a href="servic.remove_lang?p_service_pk='||p_service_pk||'&p_language_pk='||v_language_pk||'"><font color="#ffffff">REMOVE</font></a></td></tr>');
		end if;

	end loop;
	close select_lang;
	html.e_table;


	html.b_form('servic.add_lang');
	htp.p('<input type="hidden" name="p_service_pk" value="'|| p_service_pk ||'">');

	html.b_table('50%');
	htp.p('
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr bgcolor="#eeeeee"><td colspan="2" align="center"><b>add language to service "'||v_name||'"</b></td></tr>
		<tr><td>language:</td><td>
	');

		html.select_lang;

	htp.p('
		</td></tr>
		<tr><td colspan="2" align="right"><input type="Submit" value="add language"></td></tr>
	');
	html.e_table;
	html.e_form;
	html.e_box;
	html.e_page;
end if;

END;
END update_lang;


---------------------------------------------------------
-- Name: 	add_lang
-- Type: 	procedure
-- What: 	adds language
-- Author:  	Frode Klevstul
-- Start date: 	06.06.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE add_lang
		(
			p_service_pk		in service.service_pk%type 		default NULL,
			p_language_pk 		in la.language_pk%type	 		default NULL
                )
IS
BEGIN
DECLARE

	v_check		number				default 0;

BEGIN

if ( login.timeout('servic.startup')>0 AND login.right('servic')>0 ) then

	SELECT 	count(*) INTO v_check
	FROM 	service_on_lang
	WHERE	service_fk = p_service_pk
	AND	language_fk = p_language_pk;

	if ( v_check = 0 ) then
		INSERT INTO service_on_lang
		(service_fk, language_fk)
		VALUES(p_service_pk, p_language_pk);
	end if;


	update_lang(p_service_pk);
end if;

END;
END add_lang;


---------------------------------------------------------
-- Name: 	remove_lang
-- Type: 	procedure
-- What: 	remove language related to a service
-- Author:  	Frode Klevstul
-- Start date: 	06.06.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE remove_lang
		(
			p_service_pk		in service.service_pk%type 		default NULL,
			p_language_pk 		in la.language_pk%type	 		default NULL
                )
IS
BEGIN

if ( login.timeout('servic.startup')>0 AND login.right('servic')>0 ) then

	DELETE FROM service_on_lang
	WHERE	service_fk = p_service_pk
	AND	language_fk = p_language_pk;

	update_lang(p_service_pk);

end if;

END remove_lang;


---------------------------------------------------------
-- Name: 	turn_on
-- Type: 	procedure
-- What: 	activates a service
-- Author:  	Frode Klevstul
-- Start date: 	25.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE turn_on
		(
			p_service_pk		in service.service_pk%type 		default NULL
                )
IS
BEGIN

if ( login.timeout('servic.startup')>0 AND login.right('servic')>0 ) then

	UPDATE	service
	SET	activated = 1
	WHERE	service_pk = p_service_pk;

	servic.startup;

end if;

END turn_on;



---------------------------------------------------------
-- Name: 	turn_off
-- Type: 	procedure
-- What: 	activates a service
-- Author:  	Frode Klevstul
-- Start date: 	25.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE turn_off
		(
			p_service_pk		in service.service_pk%type 		default NULL
                )
IS
BEGIN

if ( login.timeout('servic.startup')>0 AND login.right('servic')>0 ) then

	UPDATE	service
	SET	activated = NULL
	WHERE	service_pk = p_service_pk;

	servic.startup;

end if;

END turn_off;




-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/
