CREATE OR REPLACE PACKAGE ok IS

        FUNCTION email
        	(
        		p_email		in varchar2	default NULL
        	)
                RETURN boolean;

END;
/
CREATE OR REPLACE PACKAGE BODY ok IS


---------------------------------------------------------
-- Name:        email
-- Type:        function
-- What:        check an email field
-- Author:      Frode Klevstul
-- Start date:  17.01.2001
-- Desc:
---------------------------------------------------------
FUNCTION email
	(
		p_email		in varchar2	default NULL
	)
        RETURN boolean
IS
BEGIN

	if not ( owa_pattern.match(p_email ,'^.+@.+\..+$', 'ig') ) then
		return false;
	else
		return true;
	end if;

END email;


-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/
