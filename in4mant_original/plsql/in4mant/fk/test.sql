set define off
PROMPT *** package: test ***

CREATE OR REPLACE PACKAGE test IS

	PROCEDURE test;
	PROCEDURE make_link;
	PROCEDURE rand;

END;
/
show errors;


CREATE OR REPLACE PACKAGE BODY test IS


---------------------------------------------------------
-- Name:        test
-- Type:        procedure
-- What:        writes out the hiscore list for a game
-- Author:      Frode Klevstul
-- Start date:  02.03.2001
-- Desc:
---------------------------------------------------------
PROCEDURE test
IS
BEGIN
DECLARE
	v_date	date default NULL;
BEGIN

if ( to_number(to_char(sysdate, 'HH24MI'))>1200 and to_number(to_char(sysdate, 'HH24MI'))<1700 ) then

	owa_util.print_cgi_env;
else
	htp.p('else');
end if;


END;
END test;







---------------------------------------------------------
-- Name:        make_link
-- Type:        procedure
-- What:        creates a link in documents
-- Author:      Frode Klevstul
-- Start date:  13.06.2001
-- Desc:
---------------------------------------------------------
PROCEDURE make_link
IS
BEGIN
DECLARE

	v_text		long 		default NULL;

BEGIN

	v_text     := '1: Dette er en link: {{http://www.klevstul.com}} her er den ferdig.<br>';
	v_text     := v_text ||'2: Dette er en ny link:{{http://www.ikke.com}} her er den ferdig.<br>';
	v_text     := v_text ||'3: Link nummer 3: {{http://www.test.com}}her er den ferdig.<br>';
	v_text     := v_text ||'4: Link nummer 4: {{  http://www.test-4.com}}her er den ferdig.<br>

';
	v_text     := v_text ||'5: Link nummer 5: {{ www.test-5.com}}her er den ferdig.<br>';
	v_text     := v_text ||'6: Link nummer 6: {{www.test-6.com}}her er den ferdig.<br>';

	htp.p(v_text||'<br>');

	-- to linker m� ikke v�re p� samme linje
	owa_pattern.change(v_text, '}}', '}}
', 'g');

	-- bytter dersom vi har med "http://"
	owa_pattern.change(v_text, '\{\{\s*http://', '\&nbsp;<a href="http://', 'g');

	-- bytter dersom vi ikke har skrevet "http://"
	owa_pattern.change(v_text, '\{\{\s*', '\&nbsp;<a href="http://', 'g');

	-- lager hoveddel og slutten av linken
	owa_pattern.change(v_text, '".*}}', '&" target="new_window">&"</a>\&nbsp;', 'g');

	-- fjerner '}}' som henger igjen fra linjen ovenfor
	owa_pattern.change(v_text, '}}', '', 'g');


	htp.p(v_text||'<br>');

END;
END make_link;




---------------------------------------------------------
-- Name:        test
-- Type:        procedure
-- What:        writes out the hiscore list for a game
-- Author:      Frode Klevstul
-- Start date:  02.03.2001
-- Desc:
---------------------------------------------------------
PROCEDURE rand
IS
BEGIN
DECLARE

	ran1 number;

BEGIN


--	random.diff(to_number(to_char(sysdate,'SSSS')));
	random.getrand(ran1);


htp.p('RANDOM NUMBER:::<br>');
htp.p(ran1);

	if length(to_char(ran1)) ='5' then
		ran1:=round(ran1/10,-1);
	end if;

htp.p('<br><br>');

htp.p(ran1);

htp.p('<br><br>');

htp.p( random.rand );

htp.p('<br><br>');

htp.p( get.random(0,999) );


END;
END rand;





-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/
show errors;
