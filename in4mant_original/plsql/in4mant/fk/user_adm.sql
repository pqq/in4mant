set define off
PROMPT *** package: user_adm ***

CREATE OR REPLACE PACKAGE user_adm IS

	PROCEDURE startup;
	PROCEDURE monitor;
	PROCEDURE del_usr
		(
			p_login_name	usr.login_name%type	DEFAULT NULL,
			p_user_pk	usr.user_pk%type	DEFAULT NULL,
			p_confirm	varchar2		DEFAULT NULL
		);
	PROCEDURE inspect
		(
			p_user_pk		in usr.user_pk%type			default NULL,
			p_login_name		in usr.login_name%type			default NULL,
			p_user_status_pk	in user_status.user_status_pk%type	default NULL,
			p_logged_in		in varchar2				default NULL
		);

END;
/
CREATE OR REPLACE PACKAGE BODY user_adm IS


---------------------------------------------------------
-- Name: 	startup
-- Type: 	procedure
-- What: 	start procedure, to run the package
-- Author:  	Frode Klevstul
-- Start date: 	25.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN
if ( login.timeout('user_adm.startup')>0 AND login.right('user_adm')>0 ) then


	monitor;


end if;
END startup;



---------------------------------------------------------
-- Name: 	monitor
-- Type: 	procedure
-- What: 	monitor users logged in
-- Author:  	Frode Klevstul
-- Start date: 	25.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE monitor
IS
BEGIN
DECLARE

	CURSOR 	select_all IS
	SELECT 	login_name, first_name, last_name, user_pk, email, ut.name, ut.user_type_pk, us.ip_address, us.host_name, s.last_action, s.logout_time
	FROM	usr u, sessn s, user_details ud, user_type ut, user_stat us
	WHERE	s.user_fk = u.user_pk
	AND	us.user_fk = u.user_pk
	AND	(us.user_stat_pk, u.user_pk) IN (
			SELECT 	MAX(user_stat_pk), user_fk
			FROM 	user_stat, usr
			WHERE 	user_fk = user_pk
			GROUP BY user_fk
			)
	AND	user_type_fk = user_type_pk
	AND	ud.user_fk = u.user_pk
	AND	(s.last_action+(s.logout_time/1440)) > sysdate
	ORDER BY last_action desc;

	v_login_name		usr.login_name%type			default NULL;
	v_first_name		user_details.first_name%type		default NULL;
	v_last_name		user_details.last_name%type		default NULL;
	v_user_pk		usr.user_pk%type			default NULL;
	v_email			user_details.email%type			default NULL;
	v_name			user_type.name%type			default NULL;
	v_user_type_pk		user_type.user_type_pk%type		default NULL;
	v_ip_address		user_stat.ip_address%type		default NULL;
	v_host_name		user_stat.host_name%type		default NULL;
	v_last_action		sessn.last_action%type			default NULL;
	v_logout_time		sessn.logout_time%type			default NULL;
	v_org_name		organization.name%type			default NULL;
	v_organization_pk	organization.organization_pk%type	default NULL;
	v_check			number					default NULL;

BEGIN
if ( login.timeout('user_adm.startup')>0 AND login.right('user_adm')>0 ) then


	html.b_adm_page('user monitor',1100,60);
	html.b_box('monitor users logged in on in4mant.com (' || to_char(sysdate, 'HH24:MI:SS') ||')');
	html.b_table;

	htp.p('<tr><td><b>user id:</b></td><td><b>login name:</b></td><td><b>first name:</b></td><td><b>last name:</b></td><td><b>email:</b></td><td><b>user status:</b></td><td><b>ip:</b></td><td><b>host:</b></td><td><b>last action:</b></td><td><b>logout time:</b></td></tr>');


	open select_all;
	loop
		fetch select_all into v_login_name, v_first_name, v_last_name, v_user_pk, v_email, v_name, v_user_type_pk, v_ip_address, v_host_name, v_last_action, v_logout_time;
		exit when select_all%NOTFOUND;

		if (v_user_type_pk = -2) then

			-- sjekk for � hindre at prog. kr�sjer dersom org. slettes mens org. brukeren er logget inn
			SELECT 	count(*) INTO v_check
			FROM	client_user
			WHERE	user_fk = v_user_pk;

			if (v_check = 1) then
				SELECT	organization_fk INTO v_organization_pk
				FROM	client_user
				WHERE	user_fk = v_user_pk;

				v_org_name := get.oname(v_organization_pk);
			else
				v_org_name := 'DELETED!';
			end if;
			v_org_name := '('|| SUBSTR(v_org_name, 1, 10) ||')';
		end if;

		htp.p('<tr><td>'|| v_user_pk ||'</td><td><a href="user_adm.inspect?p_user_pk='||v_user_pk||'">'|| v_login_name ||'</a></td><td>'|| v_first_name ||'</td><td>'|| v_last_name ||'</td><td>'|| v_email ||'</td><td>'|| v_name ||' '|| v_org_name ||'</td><td>'|| v_ip_address ||'</td><td>'|| v_host_name ||'</td><td>'|| to_char(v_last_action, 'HH24:MI:SS') ||'</td><td>'|| to_char(v_last_action+(v_logout_time/1440), 'HH24:MI:SS') ||'</td></tr>');
		v_org_name := NULL;

	end loop;
	close select_all;



	html.e_table;
	html.e_box;
	html.e_page_2;

end if;
END;
END monitor;





---------------------------------------------------------
-- Name: 	del_usr
-- Type: 	procedure
-- What: 	delete user
-- Author:  	Frode Klevstul
-- Start date: 	05.09.2000
-- Desc:
---------------------------------------------------------
PROCEDURE del_usr
	(
		p_login_name	usr.login_name%type	DEFAULT NULL,
		p_user_pk	usr.user_pk%type	DEFAULT NULL,
		p_confirm	varchar2		DEFAULT NULL
	)
IS
BEGIN
DECLARE

	CURSOR	select_all(v_login_name in usr.login_name%type) IS
	SELECT	user_pk, login_name, user_type_fk
	FROM	usr
	WHERE	login_name like '%'||v_login_name||'%';

	v_user_pk	usr.user_pk%type	default NULL;
	v_login_name	usr.login_name%type	default NULL;
	v_org_name	organization.name%type	default NULL;
	v_user_type_fk	usr.user_type_fk%type	default NULL;
	v_check		number			default NULL;

BEGIN
if ( login.timeout('user_adm.del_usr')>0 AND login.right('user_adm')>0 ) then

	html.b_adm_page('delete user');
	html.b_box('delete user');

	html.b_table;
	html.b_form('user_adm.del_usr');
	htp.p('<tr><td colspan="2">Warning: EVERYTHING related to a user will be deleted! <br>Type ''%'' to list out all users. <br>''Admin'' and ''Super'' users are not listed out here.</td></tr>');
	htp.p('<tr><td colspan="2">&nbsp;</td></tr>');
	htp.p('<tr><td>login name:</td><td><input type="text" name="p_login_name" value="'||p_login_name||'"></td></tr>');
	htp.p('<tr><td colspan="2" align="right">'); html.submit_link('list out'); htp.p('</td></tr>');
	htp.p('<tr><td colspan="2"> <hr noshade> </td></tr>');
	html.e_form;
	html.e_table;


	html.b_table;
	if (p_login_name IS NOT NULL) then
		if (p_user_pk IS NOT NULL) then
			if (p_confirm = 'yes') then
				DELETE FROM usr
				WHERE user_pk = p_user_pk;
				commit;

				html.jump_to('user_adm.del_usr?p_login_name='||p_login_name);
			elsif (p_confirm = 'no') then
				html.jump_to('user_adm.del_usr?p_login_name='||p_login_name);
			else
				html.b_form('user_adm.del_usr');
				htp.p('
					<input type="hidden" name="p_user_pk" value="'|| p_user_pk ||'">
					<input type="hidden" name="p_login_name" value="'|| p_login_name ||'">
					<tr><td colspan="2">Do you realy want to delete user <b>'|| get.login_name(p_user_pk) ||'</b> with pk '''|| p_user_pk ||'''?</td></tr>
					<tr><td><input type="submit" name="p_confirm" value="yes"></td><td align="right"><input type="submit" name="p_confirm" value="no"></td></tr>
				');
				html.e_form;
			end if;
		else
			open select_all(p_login_name);
			loop
				fetch select_all into v_user_pk, v_login_name, v_user_type_fk;
				exit when select_all%NOTFOUND;

				if (v_user_type_fk = -2) then

					SELECT	count(*) INTO v_check
					FROM	organization, client_user
					WHERE	organization_fk = organization_pk
					AND	user_fk = v_user_pk;

					if (v_check > 0) then
						SELECT	name INTO v_org_name
						FROM	organization, client_user
						WHERE	organization_fk = organization_pk
						AND	user_fk = v_user_pk;
					else
						v_org_name := '--';
					end if;

					v_org_name := '('|| SUBSTR(v_org_name, 1, 10) ||')';
				end if;

				htp.p('
					<tr>
					<td>'|| v_user_pk ||'</td>
					<td>'|| v_login_name ||'</td>
					<td>'|| get.uname(v_user_pk) ||'</td>
					<td>'|| v_org_name ||'</td>
					<td>
				');

				html.button_link('DELETE', 'user_adm.del_usr?p_user_pk='||v_user_pk||'&p_login_name='||p_login_name);

				htp.p('
					</td>
					</tr>
				');
				v_org_name := NULL;

			end loop;
			close select_all;
		end if;
	end if;

	html.e_table;
	html.e_box;
	html.e_page_2;

end if;
END;
END;



---------------------------------------------------------
-- Name: 	inspect
-- Type: 	procedure
-- What: 	a procedure to inspect/control a user
-- Author:  	Frode Klevstul
-- Start date: 	02.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE inspect
	(
		p_user_pk		in usr.user_pk%type			default NULL,
		p_login_name		in usr.login_name%type			default NULL,
		p_user_status_pk	in user_status.user_status_pk%type	default NULL,
		p_logged_in		in varchar2				default NULL
	)
IS
BEGIN
DECLARE

	-- cursor for � hente ut �nsket bruker (viser ikke super brukere)
	CURSOR 	select_all
		(
			v_user_pk 	in usr.user_pk%type,
			v_login_name 	in usr.login_name%type
		) IS
	SELECT 	v_user_pk, login_name, first_name, last_name, user_pk, email, ut.name, ut.user_type_pk, u.user_status_fk
	FROM	usr u, user_details ud, user_type ut, user_status ust
	WHERE	u.user_type_fk = ut.user_type_pk
	AND	u.user_status_fk = ust.user_status_pk
	AND	ud.user_fk = u.user_pk
	AND	(
		u.user_pk = v_user_pk OR
		u.login_name = v_login_name
		)
	;


	CURSOR	select_stat
		(
			v_user_pk 	in usr.user_pk%type
		) IS
	SELECT	ip_address, host_name, login_time
	FROM	user_stat
	WHERE	user_fk = v_user_pk
	ORDER BY login_time DESC
	;


	CURSOR	select_status IS
	SELECT	user_status_pk, name, description
	FROM	user_status
	;


	v_login_name		usr.login_name%type			default NULL;
	v_first_name		user_details.first_name%type		default NULL;
	v_last_name		user_details.last_name%type		default NULL;
	v_user_pk		usr.user_pk%type			default NULL;
	v_email			user_details.email%type			default NULL;
	v_name			user_type.name%type			default NULL;
	v_user_type_pk		user_type.user_type_pk%type		default NULL;
	v_ip_address		user_stat.ip_address%type		default NULL;
	v_host_name		user_stat.host_name%type		default NULL;
	v_last_action		sessn.last_action%type			default NULL;
	v_logout_time		sessn.logout_time%type			default NULL;
	v_org_name		organization.name%type			default NULL;
	v_organization_pk	organization.organization_pk%type	default NULL;
	v_check			number					default NULL;
	v_login_time		user_stat.login_time%type		default NULL;
	v_user_status_pk	user_status.user_status_pk%type		default NULL;
	v_user_status_pk_2	user_status.user_status_pk%type		default NULL;
	v_name_us		user_status.name%type			default NULL;
	v_description		user_status.description%type		default NULL;

BEGIN
if ( login.timeout('user_adm.startup')>0 AND login.right('user_adm')>0 ) then

	html.b_adm_page('user inspector');
	html.b_box('inspect user (' || to_char(sysdate, 'HH24:MI:SS') ||')');
	html.b_table;


	-- skriver ut HTML kode til form for � skrive inn navn/bruker id
	html.b_form('user_adm.inspect');
	htp.p('<tr><td>user_pk:</td><td><input type="text" name="p_user_pk" value="'||p_user_pk||'"></td></tr>');
	htp.p('<tr><td>login_name:</td><td><input type="text" name="p_login_name" value="'||p_login_name||'"></td></tr>');
	htp.p('<tr><td colspan="2" align="right">'); html.submit_link('list out'); htp.p('</td></tr>');
	htp.p('<tr><td colspan="2"> <hr noshade> </td></tr>');
	html.e_form;


	-- skriver ut resultatet dersom vi har id/login navn
	if (p_user_pk IS NOT NULL OR p_login_name IS NOT NULL) then

		open select_all(p_user_pk, p_login_name);
		loop
			fetch select_all into v_user_pk, v_login_name, v_first_name, v_last_name, v_user_pk, v_email, v_name, v_user_type_pk, v_user_status_pk_2;
			exit when select_all%NOTFOUND;

			-- vi gj�r update dersom verdien ikke er NULL
			if (p_user_status_pk IS NOT NULL OR p_logged_in IS NOT NULL) then

				-- vi gj�r update dersom verdien ikke er NULL
				if (p_user_status_pk IS NOT NULL) then
					UPDATE 	usr
					SET	user_status_fk = p_user_status_pk
					WHERE	user_pk = v_user_pk
					AND	user_type_fk <> -4;
					commit;
				end if;

				-- vi gj�r update dersom verdien ikke er NULL
				if (p_logged_in IS NOT NULL) then
					UPDATE 	sessn
					SET	logout_time = 0
					WHERE	user_fk = v_user_pk;
					commit;
				end if;

				html.jump_to('user_adm.inspect?p_user_pk='||v_user_pk);
				return;

			end if;


			-- skriver ut resultatet
			html.b_form('user_adm.inspect');
			htp.p('
				<input type="hidden" name="p_user_pk" value="'||p_user_pk||'">
				<input type="hidden" name="p_login_name" value="'||p_login_name||'">
				<tr><td>user_pk:</td><td>'|| v_user_pk ||'</td></tr>
				<tr><td>login name:</td><td>'|| v_login_name ||'</td></tr>
				<tr><td>first name:</td><td>'|| v_first_name ||'</td></tr>
				<tr><td>user_details.last_name:</td><td>'|| v_last_name ||'</td></tr>
				<tr><td>user_details.email:</td><td><a href="mailto:'||v_email||'">'|| v_email ||'</a></td></tr>
				<tr><td>user_type.name:</td><td>'|| v_name ||'</td></tr>
			');


			-- dersom "client_user" s� henter vi ut navnet p� organisasjonen brukerern administrerer
			if (v_user_type_pk = -2) then

				-- sjekk for � hindre at prog. kr�sjer dersom org. slettes mens org. brukeren er logget inn
				SELECT 	count(*) INTO v_check
				FROM	client_user
				WHERE	user_fk = v_user_pk;

				if (v_check = 1) then
					SELECT	organization_fk INTO v_organization_pk
					FROM	client_user
					WHERE	user_fk = v_user_pk;

					v_org_name := get.oname(v_organization_pk);
				else
					v_org_name := 'DELETED!';
				end if;

				htp.p('<tr><td>organization.name:</td><td>'|| v_org_name ||'</td></tr>');
			end if;


			-- henter ut innloggings statistikk
			htp.p('<tr><td valign="top">last logins:</td><td><select name="" size="10">');
			open select_stat(v_user_pk);
			loop
				fetch select_stat into v_ip_address, v_host_name, v_login_time;
				exit when select_stat%NOTFOUND;

				htp.p('<option value="">'||v_ip_address||' - '|| v_host_name ||' - '|| to_char(v_login_time, get.txt('date_long')) ||'</option>');

			end loop;
			close select_stat;
			htp.p('</select></td></tr>');


			-- sjekker om brukeren er logget inn akkurat n�
			SELECT 	count(*) INTO v_check
			FROM	usr u, sessn s
			WHERE	s.user_fk = u.user_pk
			AND	u.user_pk = v_user_pk
			AND	(s.last_action+(s.logout_time/1440)) > sysdate;


			htp.p('<tr><td>logged in now:</td><td>');
			if (v_check > 0) then

				SELECT 	s.last_action, s.logout_time INTO v_last_action, v_logout_time
				FROM	usr u, sessn s
				WHERE	s.user_fk = u.user_pk
				AND	u.user_pk = v_user_pk
				AND	(s.last_action+(s.logout_time/1440)) > sysdate
				AND	sessn_pk =
					(
						SELECT	MAX(sessn_pk)
						FROM	sessn
						WHERE	user_fk = v_user_pk
						AND	(last_action+(logout_time/1440)) > sysdate
					);

				htp.p('
					<select name="p_logged_in" size="1">
					<option value="YES" selected>YES</option>
					<option value="NO">NO</option>
					</select>
					<br>logout minutes: '|| v_logout_time ||'<br>logout time: '|| to_char(v_last_action+(v_logout_time/1440), 'HH24:MI:SS') );
			else
				htp.p('NO');
			end if;
			htp.p('</tr>');


			-- henter ut bruker status
			htp.p('<tr><td valign="top">user status:</td><td><select name="p_user_status_pk" size="1">');
			open select_status;
			loop
				fetch select_status into v_user_status_pk, v_name, v_description;
				exit when select_status%NOTFOUND;

				if (v_user_status_pk = v_user_status_pk_2) then
					htp.p('<option value="'|| v_user_status_pk ||'" selected>'||v_user_status_pk||': '||v_name||' ('|| v_description ||')</option>');
				else
					htp.p('<option value="'|| v_user_status_pk ||'">'||v_user_status_pk||': '||v_name||' ('|| v_description ||')</option>');
				end if;
			end loop;
			close select_status;
			htp.p('</select></td></tr>');

			htp.p('</tr><tr><td colspan="2" align="right">'); html.submit_link('update'); htp.p('</td></tr>');
			html.e_form;

		end loop;
		close select_all;

	end if;

	html.e_table;
	html.e_box;
	html.e_adm_page;

end if;
END;
END inspect;




-- ++++++++++++++++++++++++++++++++++++++++++++++ --

END; -- slutter pakke kroppen
/
show errors;
