set define off
PROMPT *** package: PORTFOLIO ***

CREATE OR REPLACE PACKAGE portfolio IS
	
	PROCEDURE startup;
	PROCEDURE main;
	PROCEDURE last_albums;
	PROCEDURE no_org;
	PROCEDURE last_pictures
		(
			p_task		in varchar2		default NULL
		);
	PROCEDURE last_comment
		(
			p_task		in varchar2		default NULL
		);
	PROCEDURE cal;
	PROCEDURE doc;
	PROCEDURE login_info;

END;
/
show errors;

CREATE OR REPLACE PACKAGE BODY portfolio IS


---------------------------------------------------------
-- Name:        startup
-- Type:        procedure
-- What:        start procedure, to run the package
-- Author:      Frode Klevstul
-- Start date:  22.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

        main;

END startup;



---------------------------------------------------------
-- Name:        main
-- Type:        procedure
-- What:        writes out all elements
-- Author:      Frode Klevstul
-- Start date:  22.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE main
IS
BEGIN
if (login.timeout('portfolio.startup') > 0 ) then

	-- Legger inn i statistikk tabellen
	stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,28);


    html.b_page(NULL, NULL, NULL, NULL, 2);
    html.my_menu;
    html.b_table;

    -- dersom det er f�rste eller andre gang man logger seg inn...
    if (get.u_status = -2 OR get.u_status = -3) then
            htp.p('<tr><td><hr noshade>');
            htp.p( get.txt('welcome_new_user') );
            htp.p('<hr noshade></td></tr>');
    end if;

    htp.p('<tr><td>');
    login_info;
    htp.p('<br><br></td></tr>');

    htp.p('<tr><td>');
    no_org;
    htp.p('<br><br></td></tr>');

    htp.p('<tr><td>');
    doc;
    htp.p('<br><br></td></tr>');

    htp.p('<tr><td>');
    cal;
    htp.p('<br><br><br><br></td></tr>');

    htp.p('<tr><td>');
    last_pictures;
    htp.p('<br><br></td></tr>');

    htp.p('<tr><td>');
    last_albums;
    htp.p('<br><br></td></tr>');

    htp.p('<tr><td>');
    last_comment;
    htp.p('<br><br></td></tr>');

    html.e_table;
    html.e_page;

end if;
END main;



---------------------------------------------------------
-- Name:        last_albums
-- Type:        procedure
-- What:        shows last albums from your organizations
-- Author:      Frode Klevstul
-- Start date:  22.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE last_albums
IS
BEGIN
DECLARE


        CURSOR  select_all(v_user_pk in usr.user_pk%type) IS
        SELECT  album_pk, organization_fk, geography_fk
        FROM    album a, organization o
        WHERE   organization_fk IN(
                        SELECT  DISTINCT organization_fk
                        FROM    list_org lo, list l, list_user lu
                        WHERE   lo.list_fk = l.list_pk
                        AND     lu.user_fk = v_user_pk
                        AND
                                (
                                        lu.list_fk = l.list_pk
                                OR      lu.list_fk = l.level0
                                OR      lu.list_fk = l.level1
                                OR      lu.list_fk = l.level2
                                OR      lu.list_fk = l.level3
                                OR      lu.list_fk = l.level4
                                OR      lu.list_fk = l.level5
                                )
                        )
        AND     a.organization_fk = o.organization_pk
        AND     a.activated = 1
        AND     o.accepted = 1
        ORDER BY a.create_date DESC, a.album_pk;

        v_album_pk              album.album_pk%type                     default NULL;
        v_title                 album_text.title%type                   default NULL;
        v_when_period           album_text.when_period%type             default NULL;
        v_organization_pk       organization.organization_pk%type       default NULL;
        v_no_pic                number                                  default NULL;
        v_tmp                   number                                  default NULL;
        v_user_pk               usr.user_pk%type                        default NULL;
		v_geography_pk			geography.geography_pk%type				default NULL;

BEGIN
if (login.timeout('portfolio.startup') > 0 ) then

        v_user_pk       := get.uid;
        v_tmp           := get.value('no_albums');

        html.b_box( get.txt('my_last_albums'), '100%', 'portfolio.last_albums', 'photo.user_album_archive?p_my_archive=1');
        html.b_table;
        htp.p('<tr><td width="15%"><b>'|| get.txt('when') ||':</b></td><td width="40%"><b>'||get.txt('title')||':</b></td><td align="right" width="10%"><b>'|| get.txt('no_pic') ||':</b></td><td>&nbsp;&nbsp;&nbsp;&nbsp;<b>'|| get.txt('source') ||':</b></td></tr>');


        open select_all(v_user_pk);
        while (select_all%ROWCOUNT < v_tmp)
        loop
                fetch select_all into v_album_pk, v_organization_pk, v_geography_pk;
                exit when select_all%NOTFOUND;

                SELECT  count(*) INTO v_no_pic
                FROM    picture_in_album
                WHERE   album_fk = v_album_pk;

                v_title         := get.ap_text(v_album_pk, 'title');
                v_when_period   := get.ap_text(v_album_pk, 'when_period');

                htp.p('
                        <tr>
                        <td nowrap valign="top">'|| v_when_period ||'</td>
                        <td valign="top"><a href="'|| get.olink( v_organization_pk, 'photo.user_list_pictures?p_album_pk='||v_album_pk)||'">'|| v_title ||'</a></b>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td align="right" valign="top">'|| v_no_pic ||'</td>
                        <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;<a href="'|| get.olink( v_organization_pk, 'org_page.main?p_organization_pk='||v_organization_pk) ||'">'|| get.oname(v_organization_pk) ||'</a>&nbsp;('||get.locn(v_geography_pk)||')</td>
                        </tr>
                ');

        end loop;
        close select_all;


        html.e_table;
        html.e_box;

end if;
END;
END last_albums;



---------------------------------------------------------
-- Name:        no_org
-- Type:        procedure
-- What:        shows how many organizations you are subscribing to
-- Author:      Frode Klevstul
-- Start date:  23.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE no_org
IS
BEGIN
DECLARE

        CURSOR  select_all(v_user_pk in usr.user_pk%type) IS
        SELECT  DISTINCT organization_fk
        FROM    list_org lo, list l, list_user lu
        WHERE   lo.list_fk = l.list_pk
        AND     lu.user_fk = v_user_pk
        AND
                (
                        lu.list_fk = l.list_pk
                OR      lu.list_fk = l.level0
                OR      lu.list_fk = l.level1
                OR      lu.list_fk = l.level2
                OR      lu.list_fk = l.level3
                OR      lu.list_fk = l.level4
                OR      lu.list_fk = l.level5
                )
        ;

        v_organization_pk       organization.organization_pk%type       default NULL;
        v_no_org                number                                  default 0;
        v_user_pk               usr.user_pk%type                        default NULL;

BEGIN
if (login.timeout('portfolio.startup') > 0 ) then

        v_user_pk := get.uid;

        open select_all(v_user_pk);
        loop
                fetch select_all into v_organization_pk;
                exit when select_all%NOTFOUND;
                v_no_org := v_no_org + 1;
        end loop;
        close select_all;

        html.b_box( get.txt('no_organizations'), '100%', 'portfolio.no_org');
        html.b_table;
        htp.p('<tr><td>'|| get.txt('you_have') ||' <b>'|| v_no_org ||'</b> ' || get.txt('organizations_in_your_portfolio') ||'</td></tr>');
        html.e_table;
        html.e_box;

end if;
END;
END no_org;



---------------------------------------------------------
-- Name:        last_pictures
-- Type:        procedure
-- What:        lists out an org last pic
-- Author:      Frode Klevstul
-- Start date:  14.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE last_pictures
	(
		p_task		in varchar2		default NULL
	)
IS
BEGIN
DECLARE

        CURSOR  select_all(v_user_pk in usr.user_pk%type) IS
        SELECT  picture_pk, path, organization_fk, whom
        FROM    picture p, picture_in_album pia, album a
        WHERE   pia.picture_fk = picture_pk
        AND     pia.album_fk = a.album_pk
        AND     a.activated = 1
        AND     a.organization_fk IN
                (
                        SELECT  DISTINCT organization_fk
                        FROM    list_org lo, list l, list_user lu
                        WHERE   lo.list_fk = l.list_pk
                        AND     lu.user_fk = v_user_pk
                        AND
                                (
                                        lu.list_fk = l.list_pk
                                OR      lu.list_fk = l.level0
                                OR      lu.list_fk = l.level1
                                OR      lu.list_fk = l.level2
                                OR      lu.list_fk = l.level3
                                OR      lu.list_fk = l.level4
                                OR      lu.list_fk = l.level5
                                )
                )
        ORDER BY upload_date DESC;

        v_picture_pk            picture.picture_pk%type         	default NULL;
        v_path                  picture.path%type               	default NULL;
        v_no                    number                          	default NULL;
        v_user_pk               usr.user_pk%type                	default NULL;
        v_organization_pk	organization.organization_pk%type	default NULL;
        v_whom			picture.whom%type			default NULL;

BEGIN
if (login.timeout('portfolio.startup') > 0 ) then


    v_user_pk       := get.uid;


    if ( owa_pattern.match(p_task, '^archive$') ) then

		v_no := get.value('no_pictures_all');

        -- ---------------------
        -- skriver ut HTML kode
        -- ---------------------
        html.b_page(NULL, NULL, NULL, NULL, 2);
		html.my_menu;
		html.b_table;
		htp.p('<tr><td>');
		
        html.b_box( get.txt('my_last_pictures'), '100%','portfolio.last_pictures', 'portfolio.startup');

        html.b_table;
        htp.p('<tr><td align="center">');

        open select_all(v_user_pk);
        while (select_all%ROWCOUNT < v_no)
        loop
                fetch select_all into v_picture_pk, v_path, v_organization_pk, v_whom;
                exit when select_all%NOTFOUND;

                htp.p('<a href="'|| get.olink( v_organization_pk, 'photo.user_show_picture?p_picture_pk='||v_picture_pk) ||'"><img width="40" border="0" src="'||get.value('photo_archive')||'/'|| v_path ||'_thumb.jpg" alt="'||v_whom||'"></a>');

        end loop;
        close select_all;

        htp.p('</td></tr>');
        html.e_table;
        html.e_box;
        
		htp.p('</td></tr>');
        html.e_table;        
        html.e_page;

	else

		v_no 			:= get.value('no_pictures');

        -- ---------------------
        -- skriver ut HTML kode
        -- ---------------------
        html.b_box( get.txt('my_last_pictures'), '100%','portfolio.last_pictures', 'portfolio.last_pictures?p_task=archive');

        html.b_table;
        htp.p('<tr><td align="center">');

        open select_all(v_user_pk);
        while (select_all%ROWCOUNT < v_no)
        loop
                fetch select_all into v_picture_pk, v_path, v_organization_pk, v_whom;
                exit when select_all%NOTFOUND;

                htp.p('<a href="'|| get.olink( v_organization_pk, 'photo.user_show_picture?p_picture_pk='||v_picture_pk) ||'"><img width="40" border="0" src="'||get.value('photo_archive')||'/'|| v_path ||'_thumb.jpg" alt="'||v_whom||'"></a>');

        end loop;
        close select_all;

        htp.p('</td></tr>');
        html.e_table;
        html.e_box;

	end if;



end if;
END;
END last_pictures;




---------------------------------------------------------
-- Name:        last_comment
-- Type:        procedure
-- What:        lists out an org last commented pic
-- Author:      Frode Klevstul
-- Start date:  24.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE last_comment
	(
		p_task		in varchar2		default NULL
	)
IS
BEGIN
DECLARE

    CURSOR  		select_all(v_user_pk in usr.user_pk%type) IS
    SELECT DISTINCT picture_pk, path, comment_date, organization_fk, whom
    FROM            picture p, picture_in_album pia, album a, picture_comment pc
    WHERE           pia.picture_fk = p.picture_pk
    AND             pia.album_fk = a.album_pk
    AND             a.activated = 1
    AND				p.activated = 1
    AND             pc.picture_fk = p.picture_pk
    AND             comment_date IN(
                            SELECT max(comment_date)
                            FROM picture_comment
                            group by picture_fk
                    )
    AND             a.organization_fk IN
                    (
                    SELECT  DISTINCT organization_fk
                    FROM    list_org lo, list l, list_user lu
                    WHERE   lo.list_fk = l.list_pk
                    AND     lu.user_fk = v_user_pk
                    AND
                        (
                                lu.list_fk = l.list_pk
                        OR      lu.list_fk = l.level0
                        OR      lu.list_fk = l.level1
                        OR      lu.list_fk = l.level2
                        OR      lu.list_fk = l.level3
                        OR      lu.list_fk = l.level4
                        OR      lu.list_fk = l.level5
                        )
                    )
    ORDER BY        comment_date DESC;

    v_picture_pk            picture.picture_pk%type                 default NULL;
    v_path                  picture.path%type                       default NULL;
    v_comment_date          picture_comment.comment_date%type       default NULL;
    v_no                    number                                  default NULL;
    v_user_pk               usr.user_pk%type                        default NULL;
    v_organization_pk	organization.organization_pk%type	default NULL;
    v_whom			picture.whom%type			default NULL;

BEGIN
if (login.timeout('portfolio.startup') > 0 ) then

    v_user_pk       := get.uid;

    if ( owa_pattern.match(p_task, '^archive$') ) then

		v_no := get.value('no_pictures_all');

        -- ---------------------
        -- skriver ut HTML kode
        -- ---------------------
        html.b_page(NULL, NULL, NULL, NULL, 2);
		html.my_menu;
		html.b_table;
		htp.p('<tr><td>');
		
        html.b_box( get.txt('my_last_comments'), '100%','portfolio.last_comment');

        html.b_table;
        htp.p('<tr><td align="center">');

        open select_all(v_user_pk);
        while (select_all%ROWCOUNT < v_no)
        loop
                fetch select_all into v_picture_pk, v_path, v_comment_date, v_organization_pk, v_whom;
                exit when select_all%NOTFOUND;

                htp.p('<a href="'|| get.olink( v_organization_pk, 'photo.user_show_picture?p_picture_pk='||v_picture_pk) ||'"><img width="40" border="0" src="'||get.value('photo_archive')||'/'|| v_path ||'_thumb.jpg" alt="'||v_whom||'"></a>');

        end loop;
        close select_all;

        htp.p('</td></tr>');
        html.e_table;
        html.e_box;
        
		htp.p('</td></tr>');
        html.e_table;        
        html.e_page;

	else

        v_no            := get.value('no_pictures');

        -- ---------------------
        -- skriver ut HTML kode
        -- ---------------------
        html.b_box( get.txt('my_last_comments'), '100%','portfolio.last_comment', 'portfolio.last_comment?p_task=archive');

        html.b_table;
        htp.p('<tr><td align="center">');

        open select_all(v_user_pk);
        while (select_all%ROWCOUNT < v_no)
        loop
                fetch select_all into v_picture_pk, v_path, v_comment_date, v_organization_pk, v_whom;
                exit when select_all%NOTFOUND;

                htp.p('<a href="'|| get.olink( v_organization_pk, 'photo.user_show_picture?p_picture_pk='||v_picture_pk) ||'"><img width="40" border="0" src="'||get.value('photo_archive')||'/'|| v_path ||'_thumb.jpg" alt="'||v_whom||'"></a>');

        end loop;
        close select_all;

        htp.p('</td></tr>');
        html.e_table;
        html.e_box;

end if;

end if;
END;
END last_comment;




---------------------------------------------------------------------
-- Name: cal
-- Type: procedure
-- What: skriver ut kommende kalenderhendelser for egne organisasjoner
-- Made: Frode Klevstul
-- Date: 24.08.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE cal
IS
BEGIN
DECLARE

        CURSOR  get_calendar(v_user_pk in usr.user_pk%type) IS
        SELECT  c.calendar_pk, c.heading, c.event_date, o.name, o.organization_pk, o.geography_fk
        FROM    calendar c, organization o
        WHERE   c.event_date > sysdate-(1/24)
        AND     c.organization_fk = o.organization_pk
        AND     o.accepted = 1
        AND     (c.organization_fk, c.language_fk) IN
                (
                        SELECT  DISTINCT organization_fk, lu.language_fk
                        FROM    list_org lo, list l, list_user lu, list_type lt
                        WHERE   lo.list_fk = l.list_pk
                        AND     lt.list_type_pk = l.list_type_fk
                        AND     lu.user_fk = v_user_pk
                        AND
                        (
                                lu.list_fk = l.list_pk
                        OR      lu.list_fk = l.level0
                        OR      lu.list_fk = l.level1
                        OR      lu.list_fk = l.level2
                        OR      lu.list_fk = l.level3
                        OR      lu.list_fk = l.level4
                        OR      lu.list_fk = l.level5
                        )
                )
        ORDER BY event_date;

        v_calendar_pk           calendar.calendar_pk%TYPE               DEFAULT NULL;
        v_heading               calendar.heading%TYPE                   DEFAULT NULL;
        v_event_date            calendar.event_date%TYPE                DEFAULT NULL;
        v_name                  organization.name%TYPE                  DEFAULT NULL;
        v_organization_pk       organization.organization_pk%TYPE       DEFAULT NULL;
	v_geography_pk		organization.geography_fk%TYPE		DEFAULT NULL;

BEGIN
if (login.timeout('portfolio.startup') > 0 ) then

        html.b_box(get.txt('my_calendar'), '100%', 'portfolio.calendar', 'cal_util.cal_archive?p_my_cal=1');
        html.b_table;
        htp.p('<tr><td align="left" width="15%"><b>'||get.txt('date')||':</b></td><td width="50%"><b>'||get.txt('happening')||':</b></td><td><b>'||get.txt('source')||':</b></td></tr>');

        OPEN get_calendar(get.uid);
        LOOP
                FETCH get_calendar into v_calendar_pk, v_heading, v_event_date, v_name, v_organization_pk, v_geography_pk;
                EXIT when get_calendar%NOTFOUND;
                htp.p('
                        <tr>
                        <td valign="top" nowrap>'||to_char(v_event_date,get.txt('date_short'))||':</td>
                        <td align="left"><a href="'|| get.olink( v_organization_pk, 'cal_util.show_calendar?p_calendar_pk='||v_calendar_pk) ||'">'||v_heading||'</a></td>
                        <td><a href="'|| get.olink( v_organization_pk, 'org_page.main?p_organization_pk='||v_organization_pk) ||'">'|| v_name ||'</a>&nbsp;('|| get.locn(v_geography_pk) ||')</td>
                        </tr>
                ');
                EXIT when get_calendar%ROWCOUNT = get.value('cal_nr');
        END LOOP;
        CLOSE get_calendar;
        html.e_table;
        html.e_box;

end if;
EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
        html.jump_to('/');
END;
END cal;




---------------------------------------------------------------------
-- Name: doc
-- Type: procedure
-- What: lister ut siste dokumenter
-- Made: Frode Klevstul
-- Date: 25.05.2000
-- Chng:
---------------------------------------------------------------------

PROCEDURE doc
IS
BEGIN
DECLARE

    v_document_pk           document.document_pk%TYPE               DEFAULT NULL;
    v_publish_date          document.publish_date%TYPE              DEFAULT NULL;
    v_heading               document.heading%TYPE                   DEFAULT NULL;
    v_organization_pk       organization.organization_pk%TYPE       DEFAULT NULL;
    v_name                  organization.name%TYPE                  DEFAULT NULL;
	v_geography_pk		organization.geography_fk%TYPE		DEFAULT NULL;

    CURSOR  get_doc(v_user_pk in usr.user_pk%type) IS
    SELECT  d.document_pk, d.publish_date, d.heading, o.name, o.organization_pk, o.geography_fk
    FROM    document d, organization o
    WHERE   d.organization_fk=o.organization_pk
    AND     o.accepted = 1
    AND     d.accepted > 0
    AND     (o.organization_pk, d.language_fk) IN
        (
            SELECT  DISTINCT organization_fk, lu.language_fk
            FROM    list_org lo, list l, list_user lu, list_type lt
            WHERE   lo.list_fk = l.list_pk
            AND     lt.list_type_pk = l.list_type_fk
            AND     lu.user_fk = v_user_pk
            AND
            (
                    lu.list_fk = l.list_pk
            OR      lu.list_fk = l.level0
            OR      lu.list_fk = l.level1
            OR      lu.list_fk = l.level2
            OR      lu.list_fk = l.level3
            OR      lu.list_fk = l.level4
            OR      lu.list_fk = l.level5
            )
        )
    ORDER BY d.publish_date DESC;

BEGIN
if (login.timeout('portfolio.startup') > 0 ) then

    html.b_box(get.txt('my_latest_news'),'100%','portfolio.doc', 'doc_util.doc_archive?p_my_archive=1');
    html.b_table;
    htp.p('<tr><td align="left" width="15%"><b>'||get.txt('date')||':</b></td><td width="50%"><b>'||get.txt('subject')||':</b></td><td><b>'||get.txt('source')||':</b></td></tr>');
    OPEN get_doc(get.uid);
    LOOP
        fetch get_doc into v_document_pk, v_publish_date, v_heading, v_name, v_organization_pk, v_geography_pk;
        exit when get_doc%NOTFOUND;
        htp.p('<tr><td valign="top" nowrap>'||to_char(v_publish_date,get.txt('date_short'))||'</td>
        <td valign="top" align="left"><b><a href="'|| get.olink( v_organization_pk, 'doc_util.show_document?p_document_pk='||v_document_pk ) ||'">
        '||html.rem_tag(v_heading)||'</a></b></td>
        <td><a href="'|| get.olink( v_organization_pk, 'org_page.main?p_organization_pk='||v_organization_pk) ||'">'||v_name||'</a>&nbsp;('|| get.locn(v_geography_pk) ||')</td>
		</tr>');
		exit when get_doc%ROWCOUNT = get.value('doc_nr');
    END LOOP;
    close get_doc;
    html.e_table;
    html.e_box;

end if;
END;
END doc;






---------------------------------------------------------------------
-- Name: login_info
-- Type: procedure
-- What: skriver en velkomstmelding - samt gir info om sist man var logget inn
-- Made: Frode Klevstul
-- Date: 08.09.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE login_info
IS
BEGIN
DECLARE

        v_host_name     user_stat.host_name%type        default NULL;
        v_ip_address    user_stat.ip_address%type       default NULL;
        v_first_name    user_details.first_name%type    default NULL;
        v_login_time    user_stat.login_time%type       default NULL;
        v_check         number                          default NULL;
        v_user_pk       usr.user_pk%type                default NULL;
        v_login_string  varchar2(1000)                  default NULL;
        v_login_name    usr.login_name%type             default NULL;

BEGIN
if (login.timeout('portfolio.startup') > 0 ) then

        v_user_pk := get.uid;

        SELECT  count(*)
        INTO    v_check
        FROM    user_stat
        WHERE   user_fk = v_user_pk;

        if (v_check > 1) then
                SELECT  host_name, ip_address, first_name, login_name, login_time
                INTO    v_host_name, v_ip_address, v_first_name, v_login_name, v_login_time
                FROM    user_stat us, usr u, user_details ud
                WHERE   u.user_pk = v_user_pk
                AND     us.user_fk = u.user_pk
                AND     ud.user_fk = u.user_pk
                AND     user_stat_pk = (
                        SELECT  max(user_stat_pk)
                        FROM    user_stat
                        WHERE   user_fk = v_user_pk
                        AND     user_stat_pk <> (
                                SELECT  max(user_stat_pk)
                                FROM    user_stat
                                WHERE   user_fk = v_user_pk
                        )
                );

                v_login_string := v_login_string ||''|| get.txt('welcome');

                if (v_first_name IS NOT NULL) then
                        v_login_string := v_login_string ||' <b>'||v_first_name||'</b>,';
                else
                        v_login_string := v_login_string ||' ''<b>'||v_login_name||'</b>'',';
                end if;

                v_login_string := v_login_string ||' '|| get.txt('last_login_was') ||' <b>'|| to_char(v_login_time, get.txt('date_short'))||'</b>';
                if (v_host_name IS NOT NULL) then
                        v_login_string := v_login_string ||' '|| get.txt('from_host') ||' <b>'||v_host_name||'</b>';
                elsif (v_ip_address IS NOT NULL) then
                        v_login_string := v_login_string ||' '|| get.txt('from_ip') ||' <b>'||v_ip_address||'</b>';
                end if;
                v_login_string := v_login_string ||'.';

                htp.p(v_login_string);

        end if;


end if;
END;
END login_info;


-- ++++++++++++++++++++++++++++++++++++++++++++++ --


END; -- slutter pakke kroppen
/
show errors;
