set define off
PROMPT *** package: modu ***

CREATE OR REPLACE PACKAGE modu IS

	PROCEDURE startup;
	PROCEDURE list_all;
	PROCEDURE insert_action
		(
			p_module_pk		in module.module_pk%type 	default NULL,
			p_name 			in module.name%type 		default NULL,
	                p_description  		in module.description%type 	default NULL
                );
	PROCEDURE update_module
		(
			p_module_pk 		in module.module_pk%type	default NULL
                );
	PROCEDURE delete_entry
		(
			p_module_pk 		in module.module_pk%type	default NULL
                );
	PROCEDURE delete_entry_2
		(
			p_module_pk 	in module.module_pk%type		default NULL,
			p_confirm	in varchar2				default NULL
                );
	PROCEDURE update_users
		(
			p_module_pk 	in module.module_pk%type	default NULL
		);
	PROCEDURE add_user
		(
			p_module_pk		in module.module_pk%type		default NULL,
			p_user_pk 		in usr.user_pk%type 			default NULL
                );
	PROCEDURE remove_user
		(
			p_module_pk		in module.module_pk%type 	default NULL,
			p_user_pk 		in usr.user_pk%type 		default NULL
                );

END;
/
CREATE OR REPLACE PACKAGE BODY modu IS


---------------------------------------------------------
-- Name: 	startup
-- Type: 	procedure
-- What: 	start prosedyren, for � kj�re pakken
-- Author:  	Frode Klevstul
-- Start date: 	21.06.2000
-- Desc:
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

	list_all;

END startup;



---------------------------------------------------------
-- Name: 	list_all
-- Type: 	procedure
-- What: 	lister ut alle forekomster
-- Author:  	Frode Klevstul
-- Start date: 	21.06.2000
-- Desc:
---------------------------------------------------------
PROCEDURE list_all
IS
BEGIN
DECLARE

	CURSOR 	select_all IS
	SELECT 	module_pk, name, description
	FROM 	module;

	v_module_pk	module.module_pk%type 		default NULL;
	v_name		module.name%type 		default NULL;
	v_description	module.description%type		default NULL;

BEGIN

if ( login.timeout('modu.startup')>0 AND login.right('modu')>0 ) then

	html.b_adm_page('module');
	html.b_box('list all');
	html.b_table;

	htp.p('<tr bgcolor="#eeeeee"><td width="5%">pk:</td><td>name:</td><td>description:</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>');
	-- g�r igjennom "cursoren"...
	open select_all;
	while (1>0)	-- alltid sant -> g�r igjennom alle forekomstene
	loop
		fetch select_all into v_module_pk, v_name, v_description;
		exit when select_all%NOTFOUND;
		htp.p('<tr><td width="5%"><font color="#bbbbbb">'|| v_module_pk ||'</font></td><td><b>'|| v_name ||'</b></td><td>'|| v_description ||'</td><td align="right">'); html.button_link('ADD/REMOVE USERS', 'modu.update_users?p_module_pk='||v_module_pk); htp.p('</td><td align="center">'); html.button_link('UPDATE', 'modu.update_module?p_module_pk='||v_module_pk); htp.p('</td><td>'); html.button_link('DELETE', 'modu.delete_entry?p_module_pk='||v_module_pk); htp.p('</td></tr>');
	end loop;
	close select_all;

	html.e_table;





	/* kode til insert form */
	html.b_form('modu.insert_action');
	html.b_table('40%');
	htp.p('
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr bgcolor="#eeeeee"><td colspan="2" align="center"><b>add new entry in "module"</b></td></tr>
		<tr><td>name:</td><td><input type="Text" name="p_name" size="20" maxlength="50"></td></tr>
		<tr><td>description:</td><td><input type="Text" name="p_description" size="50" maxlength="50"></td></tr>
		<tr><td colspan="2" align="right">'); html.submit_link('insert'); htp.p('
		</td></tr>
	');
	html.e_table;
	html.e_form;


	html.e_box;
	html.e_page;

end if;

END;
END list_all;



---------------------------------------------------------
-- Name: 	insert_action
-- Type: 	procedure
-- What: 	inserts the entry into the database
-- Author:  	Frode Klevstul
-- Start date: 	21.06.2000
---------------------------------------------------------
PROCEDURE insert_action
		(
			p_module_pk		in module.module_pk%type 	default NULL,
			p_name 			in module.name%type 		default NULL,
	                p_description  		in module.description%type 	default NULL
                )
IS
BEGIN
DECLARE

	v_module_pk 		module.module_pk%type 			default NULL;	-- henter ut pk av sekvens
	v_check 		number					default 0;
	v_description		module.description%type 		default NULL;

BEGIN

if ( login.timeout('modu.startup')>0 AND login.right('modu')>0 ) then

	-- sjekker om det allerede finnes en forekomst med dette navnet
	if ( p_module_pk IS NOT NULL ) then
		SELECT count(*) INTO v_check
		FROM module
		WHERE name = p_name
		AND module_pk <> p_module_pk;
	else
		SELECT count(*) INTO v_check
		FROM module
		WHERE name = p_name;
	end if;

	if (v_check > 0 OR p_description IS NULL) then
		htp.p( get.msg(1, 'there already exists an entry with name = <b>'||p_name||'</b>') );
		html.e_page; -- slutter siden
	else
	begin
		v_description := p_description;
		v_description := html.rem_tag(v_description);

		-- henter ut neste nummer i sekvensen til tabellen
		if (p_module_pk is NULL AND p_name IS NOT NULL) then
			SELECT module_seq.NEXTVAL
			INTO v_module_pk
			FROM dual;
			-- legger inn i databasen
			INSERT INTO module
			(module_pk, name, description)
			VALUES (v_module_pk, p_name, p_description);
			commit;
		elsif (p_name IS NOT NULL) then
		begin
			UPDATE module
			SET name = p_name, description = p_description
			WHERE module_pk = p_module_pk;
			commit;
		end;
		end if;
	end;
	end if;
	list_all;

end if;

END;
END insert_action;


---------------------------------------------------------
-- Name: 	update_module
-- Type: 	procedure
-- What: 	edit an entry
-- Author:  	Frode Klevstul
-- Start date: 	21.06.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE update_module
		(
			p_module_pk 		in module.module_pk%type	default NULL
                )
IS
BEGIN
DECLARE

	v_name		module.name%type		default NULL;
	v_description	module.description%type		default NULL;

BEGIN

if ( login.timeout('modu.startup')>0 AND login.right('modu')>0 ) then

	SELECT name, description INTO v_name, v_description
	FROM module
	WHERE module_pk = p_module_pk;

	v_description := html.rem_tag(v_description);

	html.b_adm_page('module');
	html.b_box('update module');
	html.b_form('modu.insert_action');
	html.b_table;

	htp.p('
		<input type="hidden" name="p_module_pk" value="'|| p_module_pk ||'">
		<tr><td colspan="2" align="center"><b>update an entry in the table "string_group"</b></td></tr>
		<tr><td>name:</td><td><input type="Text" name="p_name" size="20" maxlength="50" value="'||v_name||'"></td></tr>
		<tr><td>description:</td><td><input type="Text" name="p_description" size="50" maxlength="50" value="'||v_description||'"></td></tr>
		<tr><td colspan="2">'); html.submit_link('update'); htp.p('</td></tr>
	');

	html.e_table;
	html.e_form;
	html.e_box;
	html.e_page;

end if;

END;
END update_module;



---------------------------------------------------------
-- Name: 	delete_entry
-- Type: 	procedure
-- What: 	confirm a deletion of an entry in the database
-- Author:  	Frode Klevstul
-- Start date: 	21.06.2000
-- Desc:
---------------------------------------------------------
PROCEDURE delete_entry
		(
			p_module_pk 		in module.module_pk%type	default NULL
                )
IS
BEGIN
DECLARE

	v_name		module.name%type	default NULL;

BEGIN

if ( login.timeout('modu.startup')>0 AND login.right('modu')>0 ) then

	SELECT name INTO v_name
	FROM module
	WHERE module_pk = p_module_pk;

	html.b_adm_page('module');
	html.b_box('delete module');
	html.b_form('modu.delete_entry_2');
	html.b_table;

	htp.p('
		<input type="hidden" name="p_module_pk" value="'|| p_module_pk ||'">
		<tr><td colspan="2">Are you sure you want to delete the entry <b>'|| v_name ||'</b> with pk <b>'||p_module_pk||'</b>?</td></tr>
		<tr><td><input type="submit" name="p_confirm" value="yes"></td><td align="right"><input type="submit" name="p_confirm" value="no"></td></tr>
		</table>
		</form>
	');

	html.e_table;
	html.e_form;
	html.e_box;
	html.e_page;

end if;

END;
END delete_entry;


---------------------------------------------------------
-- Name: 	delete_entry_2
-- Type: 	procedure
-- What:
-- Author:  	Frode Klevstul
-- Start date: 	21.06.2000
-- Desc:
---------------------------------------------------------
PROCEDURE delete_entry_2
		(
			p_module_pk 	in module.module_pk%type		default NULL,
			p_confirm	in varchar2				default NULL
                )
IS
BEGIN

if ( login.timeout('modu.startup')>0 AND login.right('modu')>0 ) then

	if (p_confirm = 'yes') then
		DELETE FROM module
		WHERE module_pk = p_module_pk;
		commit;
	end if;
	list_all;

end if;

END delete_entry_2;



---------------------------------------------------------
-- Name: 	update_users
-- Type: 	procedure
-- What: 	legger brukere til en modul
-- Author:  	Frode Klevstul
-- Start date: 	21.06.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
-- beskrivelse av inn parametere:
--
-- [parameter]	[description]
---------------------------------------------------------
PROCEDURE update_users
	(
		p_module_pk 	in module.module_pk%type	default NULL
	)
IS
BEGIN
DECLARE

	CURSOR 	select_all IS
	SELECT 	user_pk, login_name
	FROM 	usr, user_right, module
	WHERE	user_pk = user_fk
	AND	module_fk = module_pk
	AND	module_pk = p_module_pk;

	v_user_pk		usr.user_pk%type			default NULL;
	v_login_name		usr.login_name%type			default NULL;
	v_name			module.name%type			default NULL;


BEGIN

if ( login.timeout('modu.startup')>0 AND login.right('modu')>0 ) then

	SELECT 	name INTO v_name
	FROM 	module
	WHERE 	module_pk = p_module_pk;

	html.b_adm_page('module');
	html.b_box('users connected to "'|| v_name ||'"');
	html.b_table('40%');
	htp.p('<tr bgcolor="#eeeeee"><td colspan="2">The module "'||v_name||'" has following users:</td></tr>');
	-- g�r igjennom "cursoren"...
	open select_all;
	while (1>0)	-- alltid sant -> g�r igjennom alle forekomstene
	loop
		fetch select_all into v_user_pk, v_login_name;
		exit when select_all%NOTFOUND;
		htp.p('<tr><td>'||v_login_name||'</td><td>'); html.button_link('remove', 'modu.remove_user?p_module_pk='||p_module_pk||'&p_user_pk='||v_user_pk); htp.p('</td></tr>');

	end loop;
	close select_all;
	html.e_table;

	-- form for � legge til brukere
	html.b_form('modu.add_user');
	htp.p('<input type="hidden" name="p_module_pk" value="'|| p_module_pk ||'">');

	html.b_table('50%');
	htp.p('
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr bgcolor="#eeeeee"><td colspan="2">add adm or super user to module "'||v_name||'":</td></tr>
		<tr><td>user:</td><td>
	');

		html.select_user;

	htp.p('
		</td></tr>
		<tr><td colspan="2" align="right">'); html.submit_link('add user'); htp.p('</td></tr>
	');

	html.e_table;
	html.e_form;
	html.e_box;
	html.e_page;

end if;

END;
END update_users;


---------------------------------------------------------
-- Name: 	add_user
-- Type: 	procedure
-- What: 	adds user
-- Author:  	Frode Klevstul
-- Start date: 	21.06.2000
-- Desc:
---------------------------------------------------------
PROCEDURE add_user
		(
			p_module_pk		in module.module_pk%type 		default NULL,
			p_user_pk 		in usr.user_pk%type 			default NULL
                )
IS
BEGIN
DECLARE

	v_check			number					default 0;
	v_user_right_pk		user_right.user_right_pk%type		default NULL;

BEGIN

if ( login.timeout('modu.startup')>0 AND login.right('modu')>0 ) then

	SELECT 	count(*) INTO v_check
	FROM 	user_right
	WHERE	module_fk = p_module_pk
	AND	user_fk = p_user_pk;

	if ( v_check = 0 ) then
		SELECT user_right_seq.NEXTVAL
		INTO v_user_right_pk
		FROM dual;

		INSERT INTO user_right
		(user_right_pk, user_fk, module_fk, granted_date)
		VALUES(v_user_right_pk, p_user_pk, p_module_pk, sysdate);
		commit;
	end if;


	update_users(p_module_pk);

end if;

END;
END add_user;


---------------------------------------------------------
-- Name: 	remove_user
-- Type: 	procedure
-- What: 	remove user connected to a module
-- Author:  	Frode Klevstul
-- Start date: 	21.06.2000
---------------------------------------------------------
PROCEDURE remove_user
		(
			p_module_pk		in module.module_pk%type 	default NULL,
			p_user_pk 		in usr.user_pk%type 		default NULL
                )
IS
BEGIN

if ( login.timeout('modu.startup')>0 AND login.right('modu')>0 ) then

	DELETE FROM user_right
	WHERE	module_fk = p_module_pk
	AND	user_fk = p_user_pk;
	commit;

	update_users(p_module_pk);

end if;

END remove_user;



-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/
show errors;
