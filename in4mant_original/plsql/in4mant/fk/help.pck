CREATE OR REPLACE PACKAGE help IS

        PROCEDURE startup;
        PROCEDURE module
                (
                        p_package       in varchar2     default NULL
                );


END;
/
CREATE OR REPLACE PACKAGE BODY help IS


---------------------------------------------------------
-- Name:        startup
-- Type:        procedure
-- What:        start prosedyren, for � kj�re pakken
-- Author:      Frode Klevstul
-- Start date:  11.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

        htp.p( 'help package' );

END startup;



---------------------------------------------------------
-- Name:        module
-- Type:        procedure
-- What:        writes out help text to a package/module
-- Author:      Frode Klevstul
-- Start date:  11.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE module
        (
                p_package       in varchar2     default NULL
        )
IS
BEGIN
-- Legger inn i statistikk tabellen
stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,26);

        html.b_page_2( 350 );
        html.b_box( get.txt('in4mant_help') );
        html.b_table;
        htp.p('<tr><td>');
        htp.p( get.txt('help:'||p_package) );
        htp.p('</td></tr>');
        htp.p('<tr><td align="right">');
        html.close( get.txt('close_window') );
        htp.p('</td></tr>');
        html.e_table;
        html.e_box;
        html.e_page_2;

END module;


-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/
