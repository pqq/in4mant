CREATE OR REPLACE PACKAGE subscribe IS

	PROCEDURE startup;
	PROCEDURE on_org
		(
			p_organization_pk	in organization.organization_pk%type	default NULL
		);

END;
/
CREATE OR REPLACE PACKAGE BODY subscribe IS

---------------------------------------------------------
-- Name: 	startup
-- Type: 	procedure
-- What: 	start procedure, to run the package
-- Author:  	Frode Klevstul
-- Start date: 	27.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

	on_org;

END startup;




---------------------------------------------------------
-- Name: 	on_org
-- Type: 	procedure
-- What: 	subscribe on one organization
-- Author:  	Frode Klevstul
-- Start date: 	27.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE on_org
	(
		p_organization_pk	in organization.organization_pk%type	default NULL
	)
IS
BEGIN
DECLARE

	v_url		varchar2(100)		default NULL;

BEGIN


	if ( p_organization_pk IS NULL) then
		-- error message
		htp.p( get.msg(1, 'subscribe.on_org is called widthout an primary key to organization') );
		return;
	end if;


	-- url'en som vi skal sendes til
	v_url := 'user_sub.frame?p_url=user_sub.sel_personal_list��p_organization_pk='|| p_organization_pk;

	-- sjekker om brukeren er logget inn p� tjenesten
	if ( login.timeout(NULL, 1)>0 ) then

		html.jump_to(v_url);

	else


		html.b_page;
		html.b_box( get.txt('subscribe_on') ||' '|| get.oname(p_organization_pk) );

		htp.p( get.txt('sub_on_org_desc') ||'<br><br><br><br>');
		htp.p( get.txt('sub_on_org_alt_1_registered_users') );

		html.b_table;
		html.b_form('login.login_action');
		htp.p('
		<SCRIPT language=''javaScript''>

		function submitForm(){
			if (document.register_form.p_accept_licence.checked == true){
				document.register_form.submit()
			}
			else{
				alert('''||get.txt('you_need_to_accept_license')||''')
			}
		}

		</SCRIPT>
		');
		htp.p('
			<input type="hidden" name="p_url" value="'||v_url||'">
			<tr><td>'|| get.txt('login_name') ||':</td><td><input type="Text" name="p_login_name" maxlength="150"></td></tr>
			<tr><td>'|| get.txt('password') ||':</td><td><input type="Password" name="p_password" maxlength="50"></td></tr>
			<tr><td colspan=2>'); html.submit_link( get.txt('login') ); htp.p('</td></tr>
		');
		html.e_form;
		html.e_table;

		htp.p( '<br><br><br><br>'|| get.txt('sub_on_org_alt_2_unregistered_users') );

		html.b_table;
		html.b_form('users.register_action','register_form');
                htp.p('
                        <tr><td colspan="3">'|| get.txt('register_user_desc') ||'</td></tr>
                        <tr><td colspan="3">&nbsp;</td></tr>
                        <tr><td valign="top">'|| get.txt('licence_agreement') ||':</td><td><textarea wrap="virtual" cols="55" rows="10" style="width: 600;">'|| get.txt('licence_agreement_user_1') ||' '|| get.txt('licence_agreement_user_2') ||'</textarea></td><td>'||
                        html.popup( get.txt('print_page'), 'users.licence_agreement?p_type=user','620', '500', '1', '1' )
                        ||'</td></tr>
                        <tr><td>&nbsp;</td><td colspan="2"><input type="checkbox" name="p_accept_licence" value=""> &nbsp;'|| get.txt('read_and_agree_license') ||':</td></tr>
                        <tr><td colspan="3">&nbsp;</td></tr>
		');
		htp.p('
			<input type="hidden" name="p_url" value="'||v_url||'">
			<tr><td colspan="2">'|| get.txt('register_desc') ||'</td></tr>
			<tr><td>'|| get.txt('login_name') ||':</td><td><input type="text" name="p_login_name" size="30" maxlength="30"></td></tr>
			<tr><td>'|| get.txt('email') ||':</td><td><input type="text" name="p_email" size="30" maxlength="150"></td></tr>
			<tr><td>'|| get.txt('password') ||':</td><td><input type="password" name="p_password" size="30" maxlength="30"></td></tr>
			<tr><td>'|| get.txt('repeat_password') ||':</td><td><input type="password" name="p_password_2" size="30" maxlength="30"></td></tr>
			<tr><td colspan="2">');
                        htp.p('<input type="button" value="'||get.txt('register_and_subscribe')||'" onClick="javaScript:submitForm()">');
			htp.p('</td></tr>
		');
		html.e_form;
		html.e_table;

		html.e_box;
		html.e_page;
	end if;

END;
END on_org;





-- ++++++++++++++++++++++++++++++++++++++++++++++ --

END; -- slutter pakke kroppen
/
