set define off
PROMPT *** package: FEED ***

CREATE OR REPLACE PACKAGE feed IS

        PROCEDURE status;
        PROCEDURE pluto ( p_test in varchar2 default NULL );

END;
/
show errors;


/*
        *********************************************************************************
        *** KORT BESKRIVELSE AV FEED @ BYTUR.NO ***
        **********************************************

        1) I crond kj�res shell scriptet "/home/oracle/bin/send_feed.sh"

        2) Det scriptet eksekverer pakker i denne PL/SQL prosedyren (eks feed.pluto)

        3) Til slutt kj�res perl scriptet "/home/oracle/bin/handle_feedmail.pl"
           (som behandler/formaterer mailen, og legger den i katalogen for email utsendelse).

        *********************************************************************************
*/


CREATE OR REPLACE PACKAGE BODY feed IS


---------------------------------------------------------
-- Name:        status
-- Type:        procedure
-- What:        writes out feed status
-- Author:      Frode Klevstul
-- Start date:  14.08.2002
-- Desc:
---------------------------------------------------------
PROCEDURE status
IS
BEGIN
DECLARE

        CURSOR  select_status IS
        SELECT  recipient, to_char(sendt_date,'YYYY.MM.DD'),  count(*)
        FROM    cal_feed_log
        GROUP BY recipient, to_char(sendt_date,'YYYY.MM.DD');

        v_recipient             cal_feed_log.recipient%type             default NULL;
        v_sendt_date    varchar2(10)                                    default NULL;
        v_number                number                                                  default NULL;

        v_file                  utl_file.file_type;
        v_string                varchar2 (1000);

BEGIN


        html.b_adm_page('Feed status');
        html.b_table;
        htp.p('<tr><td><b>Recipient:</b></td><td><b>Sendt date:</b></td><td><b>Number events sendt:</b></td></tr>');

        open select_status;
        loop
                fetch select_status into v_recipient, v_sendt_date, v_number;
                exit when select_status%NOTFOUND;

                htp.p('<tr><td>'||v_recipient||'</td><td>'||v_sendt_date||'</td><td>'||v_number||'</td></tr>');

        end loop;
        close select_status;


        -- selects out number of events that will be sendt next time
        SELECT  count(*)
        INTO    v_number
        FROM    calendar c, org_service os, organization o
        WHERE   o.organization_pk = c.organization_fk
        AND             os.organization_fk = o.organization_pk
        AND             c.accepted IS NOT NULL
        AND             o.accepted = 1
        AND             os.service_fk = 3                                       -- 03 = Norsk utelivstjeneste
        AND             (
                c.event_date > sysdate
                OR
                (c.end_date  > sysdate AND c.event_date < sysdate)
        )
        AND             c.calendar_pk NOT IN
                (
                        SELECT  calendar_fk
                        FROM    cal_feed_log
                );


        htp.p('<tr><td colspan="3">&nbsp;</td></tr>');

        htp.p('<tr><td colspan="3"><b>Number of new events that would have been sendt right now:</b> '||v_number||'</td></tr>');

        htp.p('<tr><td colspan="3">&nbsp;</td></tr>');
        htp.p('<tr><td colspan="3">&nbsp;</td></tr>');
        htp.p('<tr><td colspan="3"><b>feed.log:</b></td></tr>');
        htp.p('<tr><td colspan="3">&nbsp;</td></tr>');


    -- ---------------------
    -- LESER FRA LOGG FIL
    -- ---------------------
    htp.p('<pre>');
        v_file := utl_file.fopen( get.value('log_dir'), 'feed.log', 'r');
        loop
                begin
                        utl_file.get_line (v_file, v_string);
                exception
                        when no_data_found then exit;
                end;
                htp.p('<tr><td colspan="3">'||v_string||'</td></tr>');
        end loop;
    utl_file.fclose(v_file);
    htp.p('</pre>');


        html.e_table;
        html.e_adm_page;



END;
END status;








---------------------------------------------------------
-- Name:        pluto
-- Type:        procedure
-- What:        sends out feed to pluto
-- Author:      Frode Klevstul
-- Start date:  09.07.2002
-- Desc:                sender kun ut for language_pk = 1 (norsk)
---------------------------------------------------------
PROCEDURE pluto ( p_test     in varchar2      default NULL )
IS
BEGIN
DECLARE

        v_sql                   varchar2(2024)                          DEFAULT NULL;

        v_calendar_pk           calendar.calendar_pk%TYPE               default NULL;
        v_organization_pk       organization.organization_pk%type       default NULL;
        v_event_date            calendar.event_date%type                default NULL;
        v_start_time            calendar.event_date%type                default NULL;
        v_url                   organization.url%type                   default NULL;
        v_heading               calendar.heading%type                   default NULL;
        v_message               calendar.message%type                   default NULL;
        v_email                 organization.email%type                 default NULL;
        v_end_date              calendar.end_date%type                  default NULL;
        v_end_time              calendar.end_date%type                  default NULL;
        v_city                  strng.string%type                       default NULL;
        v_contact_name          org_info.value%type                     default NULL;
        v_log_dir               varchar2(100)                           default get.value('log_dir');
        v_feed_dir              varchar2(100)                           default get.value('email_dir');
        v_email_inc             user_details.email%type                 default NULL;
        v_number                number                                  default NULL;
        v_file                  utl_file.file_type                      default NULL;

        v_line                  varchar2(7000)                          default NULL;
        v_counter               number                                  default 0;
        v_test                  number                                  default 0;
        v_date_format           varchar2(30)                            default get.value('log_date');

        v_start_timestamp       date                                    default NULL;
        v_days_from_zero        integer                                 default 0;

        TYPE cur_typ IS REF CURSOR;
        pluto_dyn_cursor    cur_typ;

BEGIN

        v_start_timestamp := sysdate;

        -- Skriver mailfilene til egen katalog pga. PL/SQL problemer med fjerning av newline (funker ikke).
        -- Gj�r reg.exp. i Perl i stedenfor

        v_feed_dir      := v_feed_dir ||'/feed_mail';

        -- ----------------------------
        -- mottagere av feed mailen
        -- ----------------------------
        IF ( p_test IS NULL ) THEN
           v_email_inc                     := 'sky@wit.no, frode@bytur.no, espen@bytur.no';
        ELSE
           v_email_inc                     := 'espen@bytur.no';
        END IF;

        -- -----------------------------
        -- henter ut unikt email nummer
        -- -----------------------------
        SELECT  email_script_seq.NEXTVAL
        INTO    v_number
        FROM    dual;


    -- -------------------------------------------------------------------------
    -- skriver mottager fil ********* START **********
    -- -------------------------------------------------------------------------
    v_file := utl_file.fopen( v_feed_dir, v_number||'_email-list.txt', 'a');
    utl_file.put_line(v_file, v_email_inc);
    utl_file.fclose(v_file);
    -- -------------------------------------------------------------------------
    -- /slutt p� � skrive mottager fil ********* END **********
    -- -------------------------------------------------------------------------

    v_file := utl_file.fopen( v_feed_dir, v_number||'_message.txt' ,'a', 32767);


   /*      PLUTO FORMAT:
     start_date    start_time    link    event_name    event_description    email
     contact_name    end_date    end_time    city_location    concert    show
     seminar    exhibition    other    SKU    club
   */
     -- s.language_fk = 1  01 = Norsk
     -- os.service_fk = 3  03 = Norsk utelivstjeneste
     v_sql := 'SELECT  DISTINCT(c.calendar_pk), o.organization_pk, c.event_date,
                c.event_date as start_time, o.url, c.heading, c.message, o.email, c.end_date,
                c.end_date as end_time, s.string as city
                FROM    calendar c, org_service os, organization o, geography g, groups gr, strng s
                WHERE   o.organization_pk = c.organization_fk
                AND             os.organization_fk = o.organization_pk
                AND             o.geography_fk = g.geography_pk
                AND             g.name_sg_fk = gr.string_group_fk
                AND             gr.string_fk = s.string_pk
                AND             s.language_fk = 1
                AND             c.accepted IS NOT NULL
                AND             o.accepted = 1
                AND             os.service_fk = 3
                AND             c.event_date < sysdate+14
                AND             ( c.event_date > sysdate OR (c.end_date  > sysdate AND c.event_date < sysdate) )
                ';

                IF ( p_test IS NULL ) THEN
                         v_sql := v_sql||'AND c.calendar_pk NOT IN (
                                                              SELECT  cfl.calendar_fk
                                                              FROM    cal_feed_log cfl, calendar c
                                                              WHERE   c.calendar_pk = cfl.calendar_fk
                                                              AND     cfl.recipient LIKE ''pluto''
                                                              AND     c.event_date > sysdate
                                                              ) ORDER BY c.event_date asc';
                ELSE
                         v_sql := v_sql||'ORDER BY c.event_date asc';
                         htp.p(v_sql ||'<br><br>');
                END IF;


        open pluto_dyn_cursor FOR v_sql;
        loop
                fetch pluto_dyn_cursor into v_calendar_pk, v_organization_pk, v_event_date, v_start_time, v_url, v_heading, v_message, v_email, v_end_date, v_end_time, v_city;
                exit when pluto_dyn_cursor%NOTFOUND;

                IF ( p_test IS NULL ) THEN
                -- --------------------------------------------------------
                -- logger i databasen at arrangementet er sendt til pluto
                -- --------------------------------------------------------
                SELECT  cal_feed_log_seq.NEXTVAL
                INTO    v_number
                FROM    dual;

                INSERT INTO cal_feed_log
                (cal_feed_log_pk, recipient, calendar_fk, sendt_date)
                VALUES(v_number, 'pluto', v_calendar_pk, sysdate);
                commit;

                -- ----------------
                -- *** 2 do ***
                -- ----------------
                -- NB: I update av kalendar m� man legge inn at man sletter calendar_fk fra cal_feed_log tabellen!!!
                -- Event. sette en egen status slik at mottager vet at det er en UPDATED hendelse
                -- ----------------
                END IF;

                v_line := NULL;                 -- nulltiller linjen

                -- ---------------------------
                -- NB: All formatering fjernes i home/www-bin/handle_feedmail.pl (kj�res (i crond) p� filene som genereres)
                -- "handle_feedmail.pl" programmet flytter filen til email utsendelseskatalogen etterp�.
                -- ---------------------------

                -- ------------------------
                -- Nei, vi bruker dager fra null.
                -- Idag, den 22.07.2002 = 731418
                -- imorgen, den 23.07.2002 = 731419
                --
                -- Mvh,Carl Petter
                --
                -- select to_char(to_date('22.07.2002', 'DD.MM.YYYY'),'J') from dual;
                -- => 2452478
                --
                -- 2452478 - 731418 = 1721060 days from Julian start to year zero)
                --
                -- --------------------------------
                -- select to_char(to_date('01.01.0001', 'DD.MM.YYYY'),'J') from dual;
                -- => 1721424
                -- select to_char(to_date('01.01.0002', 'DD.MM.YYYY'),'J') from dual;
                -- => 1721789
                -- >> 1721789 - 1721424 = 365 (there is 365 days in year 0)
                --
                -- 1721424 - 365 = 1721059
                -- 1721271 = days from 1 jan 4712 BC to year zero - 1. jan
                -- >> 1721060 days from Julian start to year zero
                -- --------------------------------


                if (v_event_date is null) then
                        v_line := v_line ||'NULL���';
                else
                        v_days_from_zero := to_number(to_char(v_event_date, 'J'));
                        v_days_from_zero := v_days_from_zero - 1721060;

                        v_line := v_line ||''||v_days_from_zero||'���';
                end if;

                if (v_start_time is null) then
                        v_line := v_line ||'NULL���';
                else
                        v_line := v_line ||''||to_char(v_start_time, 'HH24:MI')||'���';
                end if;

                if (v_url is null) then
                        v_line := v_line ||'NULL���';
                else
                        v_line := v_line ||''||v_url||'���';
                end if;

                if (v_heading is null) then
                        v_line := v_line ||'NULL���';
                else
                        v_line := v_line ||''||v_heading||'���';
                end if;

                if (v_message is null) then
                        v_line := v_line ||'NULL���';
                else
                        v_line := v_line ||''||v_message||'���';
                end if;

                if (v_email is null) then
--                        v_line := v_line ||'NULL���';
                   v_line := v_line ||'info@bytur.no���';
                else
                        v_line := v_line ||''||v_email||'���';
                end if;

                SELECT  count(*)
                INTO    v_test
                FROM    org_info
                WHERE   organization_fk = v_organization_pk
                AND             info_name_fk = 15;              -- 15 = Contact_person_1(uteliv)

                if (v_test > 0) then
                        SELECT  value
                        INTO    v_contact_name
                        FROM    org_info
                        WHERE   organization_fk = v_organization_pk
                        AND             info_name_fk = 15;      -- 15 = Contact_person_1(uteliv)
                end if;

-- Harkoder URL for � f� den p� sidene til Pluto
                v_contact_name := 'bytur.no';

                v_line := v_line ||''||v_contact_name||'���';

                if (v_end_date is null) then
                        v_line := v_line ||''||v_days_from_zero||'NULL���';                             -- sender med event_date dersom end_date ikke finnes. ::::Kanskje ikke NULL p� slutten her? Espen
                else
                        v_days_from_zero := to_number(to_char(v_event_date, 'J'));
                        v_days_from_zero := v_days_from_zero - 1721060;

                        v_line := v_line ||''||to_char(v_days_from_zero,'J')||'���';
                end if;

                if (v_end_time is null) then
                        v_line := v_line ||'NULL���';
                else
                        v_line := v_line ||''||to_char(v_end_time,'HH24:MI')||'���';
                end if;

                if (v_city is null) then
                        v_line := v_line ||'NULL���';
                else
                        v_line := v_line ||''||v_city||'���';
                end if;

                v_line := v_line ||'���NULL���NULL���NULL���NULL���NULL���NULL���NULL�$�';

                IF ( p_test IS NOT NULL ) THEN
                   htp.p(v_line||'<br>');
                END IF;
                utl_file.put_line(v_file, v_line);
                v_counter := v_counter + 1;

        end loop;
        close pluto_dyn_cursor;

    utl_file.fclose(v_file);


    -- ---------------------
    -- SKRIVER LOGG FIL
    -- ---------------------
        v_file := utl_file.fopen( v_log_dir, 'feed.log', 'a');
    utl_file.put_line(v_file, '>> feed.sql (PL/SQL):');
    utl_file.put_line(v_file, '('||to_char(v_start_timestamp, v_date_format)||' - '||to_char(sysdate, v_date_format)||') '||v_counter||' happenings sendt to '||v_email_inc);
    utl_file.fclose(v_file);


END;
END pluto;



-- ++++++++++++++++++++++++++++++++++++++++++++++ --


END; -- slutter pakke kroppen
/
show errors;


