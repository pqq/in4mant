CREATE OR REPLACE PACKAGE select_list IS

	PROCEDURE startup;
	PROCEDURE sel_type
		(
			p_service_fk	in list_type.service_fk%type	default NULL,
			p_type		in varchar2			default NULL
		);
	PROCEDURE sel(
			p_list_type_fk	in list.list_type_fk%type	default NULL,
			p_level_no	in number			default 0,
			p_level0	in list.level0%type		default NULL,
			p_level1	in list.level1%type		default NULL,
			p_level2	in list.level2%type		default NULL,
			p_level3	in list.level3%type		default NULL,
			p_level4	in list.level4%type		default NULL,
			p_level5	in list.level5%type		default NULL,
			p_type		in varchar2			default NULL
		);
	PROCEDURE change_close
		(
			p_level_no	in number			default NULL,
			p_level0	in list.level0%type		default NULL,
			p_level1	in list.level1%type		default NULL,
			p_level2	in list.level2%type		default NULL,
			p_level3	in list.level3%type		default NULL,
			p_level4	in list.level4%type		default NULL,
			p_level5	in list.level5%type		default NULL,
			p_type		in varchar2			default NULL
		);


END;
/
CREATE OR REPLACE PACKAGE BODY select_list IS


---------------------------------------------------------
-- Name: 	startup
-- Type: 	procedure
-- What: 	start prosedyren, for � kj�re pakken
-- Author:  	Frode Klevstul
-- Start date: 	15.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

	sel_type;

END startup;




---------------------------------------------------------
-- Name: 	sel_type
-- Type: 	procedure
-- What: 	allows the user to select list_type
-- Author:  	Frode Klevstul
-- Start date: 	15.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE sel_type
	(
		p_service_fk	in list_type.service_fk%type	default NULL,
		p_type		in varchar2			default NULL
	)
IS
BEGIN
DECLARE

	CURSOR 	select_all IS
	SELECT 	list_type_pk, lt.name_sg_fk, s.name_sg_fk
	FROM	list_type lt, service s
	WHERE	service_fk = service_pk
	AND	list_type_pk > 0;

	CURSOR 	select_all_2 (v_service_fk IN list_type.service_fk%type) IS
	SELECT 	list_type_pk, lt.name_sg_fk, s.name_sg_fk
	FROM	list_type lt, service s
	WHERE	service_fk = service_pk
	AND	list_type_pk > 0
	AND	service_fk = v_service_fk;

	v_l_name_sg_fk	list_type.name_sg_fk%type		default NULL;
	v_s_name_sg_fk	service.name_sg_fk%type			default NULL;
	v_list_type_pk	list_type.list_type_pk%type		default NULL;
BEGIN

	if (p_type = 'user_sub.sel_personal_list_2') then
		html.b_page_3(280);
	else
		html.b_page_2( 280 );
	end if;
	html.b_box( get.txt('select_list_type'), '100%', 'select_list.sel_type');
	html.b_table('100%');
	html.b_select_jump;
	htp.p('<option value="" selected>&nbsp;&lt;&nbsp;'|| get.txt('select_list_type') ||'&nbsp;&gt;&nbsp;</option>');

	if (p_service_fk IS NULL OR p_service_fk = 0) then
		open select_all;
		while (1>0)	-- alltid sant -> g�r igjennom alle forekomstene
		loop
			fetch select_all into v_list_type_pk, v_l_name_sg_fk, v_s_name_sg_fk;
			exit when select_all%NOTFOUND;
			htp.p('<option value="select_list.sel?p_type='||p_type||'&p_list_type_fk='|| v_list_type_pk ||'">'|| get.text(v_l_name_sg_fk) ||' ('|| get.text(v_s_name_sg_fk) ||')</option>');
		end loop;
		close select_all;
	else
		open select_all_2(p_service_fk);
		while (1>0)	-- alltid sant -> g�r igjennom alle forekomstene
		loop
			fetch select_all_2 into v_list_type_pk, v_l_name_sg_fk, v_s_name_sg_fk;
			exit when select_all_2%NOTFOUND;
			htp.p('<option value="select_list.sel?p_type='||p_type||'&p_list_type_fk='|| v_list_type_pk ||'">'|| get.text(v_l_name_sg_fk) ||' ('|| get.text(v_s_name_sg_fk) ||')</option>');
		end loop;
		close select_all_2;
	end if;

	html.e_select_jump;
	html.e_table;
	html.e_box;
	html.e_page_2;

END;
END sel_type;



---------------------------------------------------------
-- Name: 	sel
-- Type: 	procedure
-- What: 	select an geographical location
-- Author:  	Frode Klevstul
-- Start date: 	07.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE sel(
		p_list_type_fk	in list.list_type_fk%type	default NULL,
		p_level_no	in number			default 0,
		p_level0	in list.level0%type		default NULL,
		p_level1	in list.level1%type		default NULL,
		p_level2	in list.level2%type		default NULL,
		p_level3	in list.level3%type		default NULL,
		p_level4	in list.level4%type		default NULL,
		p_level5	in list.level5%type		default NULL,
		p_type		in varchar2			default NULL
	)
IS
BEGIN
DECLARE


	CURSOR 	select_list_0 IS
	SELECT 	name_sg_fk, level0
	FROM 	list, string_group
	WHERE	level1 IS NULL
	AND	list_type_fk = p_list_type_fk
	AND	name_sg_fk = string_group_pk
	ORDER BY name;

	CURSOR 	select_list_1 IS
	SELECT 	name_sg_fk, level0, level1
	FROM 	list, string_group
	WHERE	level0 = p_level0
	AND	level1 IS NOT NULL
	AND	level2 IS NULL
	AND	name_sg_fk = string_group_pk
	ORDER BY name;

	CURSOR 	select_list_2 IS
	SELECT 	name_sg_fk, level0, level1, level2
	FROM 	list, string_group
	WHERE	level1 = p_level1
	AND	level2 IS NOT NULL
	AND	level3 IS NULL
	AND	name_sg_fk = string_group_pk
	ORDER BY name;

	CURSOR 	select_list_3 IS
	SELECT 	name_sg_fk, level0, level1, level2, level3
	FROM 	list, string_group
	WHERE	level2 = p_level2
	AND	level3 IS NOT NULL
	AND	level4 IS NULL
	AND	name_sg_fk = string_group_pk
	ORDER BY name;

	CURSOR 	select_list_4 IS
	SELECT 	name_sg_fk, level0, level1, level2, level3, level4
	FROM 	list, string_group
	WHERE	level3 = p_level3
	AND	level4 IS NOT NULL
	AND	level5 IS NULL
	AND	name_sg_fk = string_group_pk
	ORDER BY name;

	CURSOR 	select_list_5 IS
	SELECT 	name_sg_fk, level0, level1, level2, level3, level4, level5
	FROM 	list, string_group
	WHERE	level4 = p_level4
	AND	level5 IS NOT NULL
	AND	name_sg_fk = string_group_pk
	ORDER BY name;

	v_name_sg_fk		list.name_sg_fk%type	default NULL;
	v_next_level		number				default NULL;
	v_level0		list.level0%type		default NULL;
	v_level1		list.level1%type		default NULL;
	v_level2		list.level2%type		default NULL;
	v_level3		list.level3%type		default NULL;
	v_level4		list.level4%type		default NULL;
	v_level5		list.level5%type		default NULL;

	v_check			number				default NULL;
	v_no_org		number				default NULL;

BEGIN

	-- teller opp hvilken level/hvilket niv� som kommer etterp�
	v_next_level := p_level_no + 1;

	if (p_level_no < 6) then
		if (p_type = 'user_sub.sel_personal_list_2') then
			html.b_page_3(280);
		else
			html.b_page_2( 280 );
		end if;
		html.b_box( get.txt('select_list_step_'|| p_level_no), '100%', 'select_list.sel');

		html.b_table;
		html.b_select_jump;
		htp.p('<option value="" selected> ['|| get.txt('select_list_step_'|| p_level_no) ||'] </option>');
		if (p_level_no = 0) then
			open select_list_0;
			loop
				fetch select_list_0 into v_name_sg_fk, v_level0;
				exit when select_list_0%NOTFOUND;
				v_no_org := get.no_org_list(v_level0);
				htp.p('<option value="select_list.sel?p_type='||p_type||'&p_level_no='|| v_next_level ||'&p_level0='|| v_level0 ||'">'|| get.text(v_name_sg_fk) ||' ('||v_no_org||')</option>');
			end loop;
			close select_list_0;
		elsif (p_level_no = 1) then
			open select_list_1;
			loop
				fetch select_list_1 into v_name_sg_fk, v_level0, v_level1;
				exit when select_list_1%NOTFOUND;
				v_no_org := get.no_org_list(v_level1);
				htp.p('<option value="select_list.sel?p_type='||p_type||'&p_level_no='|| v_next_level ||'&p_level0='|| v_level0 ||'&p_level1='|| v_level1 ||'">'|| get.text(v_name_sg_fk) ||' ('||v_no_org||')</option>');
			end loop;
			close select_list_1;

			SELECT 	count(*) INTO v_check
			FROM	list
			WHERE	level0 = v_level0;
			if (v_check < 2) then
				change_close(p_level_no, p_level0, p_level1, p_level2, p_level3, p_level4, p_level5, p_type);
			end if;

		elsif (p_level_no = 2) then
			open select_list_2;
			loop
				fetch select_list_2 into v_name_sg_fk, v_level0, v_level1, v_level2;
				exit when select_list_2%NOTFOUND;
				v_no_org := get.no_org_list(v_level2);
				htp.p('<option value="select_list.sel?p_type='||p_type||'&p_level_no='|| v_next_level ||'&p_level0='|| v_level0 ||'&p_level1='|| v_level1 ||'&p_level2='|| v_level2 ||'">'|| get.text(v_name_sg_fk) ||' ('||v_no_org||')</option>');
			end loop;
			close select_list_2;

			SELECT 	count(*) INTO v_check
			FROM	list
			WHERE	level1 = v_level1;
			if (v_check < 2) then
				change_close(p_level_no, p_level0, p_level1, p_level2, p_level3, p_level4, p_level5, p_type);
			end if;

		elsif (p_level_no = 3) then
			open select_list_3;
			loop
				fetch select_list_3 into v_name_sg_fk, v_level0, v_level1, v_level2, v_level3;
				exit when select_list_3%NOTFOUND;
				v_no_org := get.no_org_list(v_level3);
				htp.p('<option value="select_list.sel?p_type='||p_type||'&p_level_no='|| v_next_level ||'&p_level0='|| v_level0 ||'&p_level1='|| v_level1 ||'&p_level2='|| v_level2 ||'&p_level3='|| v_level3 ||'">'|| get.text(v_name_sg_fk) ||' ('||v_no_org||')</option>');
			end loop;
			close select_list_3;

			SELECT 	count(*) INTO v_check
			FROM	list
			WHERE	level2 = v_level2;
			if (v_check < 2) then
				change_close(p_level_no, p_level0, p_level1, p_level2, p_level3, p_level4, p_level5, p_type);
			end if;

		elsif (p_level_no = 4) then
			open select_list_4;
			loop
				fetch select_list_4 into v_name_sg_fk, v_level0, v_level1, v_level2, v_level3, v_level4;
				exit when select_list_4%NOTFOUND;
				v_no_org := get.no_org_list(v_level4);
				htp.p('<option value="select_list.sel?p_type='||p_type||'&p_level_no='|| v_next_level ||'&p_level0='|| v_level0 ||'&p_level1='|| v_level1 ||'&p_level2='|| v_level2 ||'&p_level3='|| v_level3 ||'&p_level4='|| v_level4 ||'">'|| get.text(v_name_sg_fk) ||' ('||v_no_org||')</option>');
			end loop;
			close select_list_4;

			SELECT 	count(*) INTO v_check
			FROM	list
			WHERE	level3 = v_level3;
			if (v_check < 2) then
				change_close(p_level_no, p_level0, p_level1, p_level2, p_level3, p_level4, p_level5, p_type);
			end if;

		elsif (p_level_no = 5) then
			open select_list_5;
			loop
				fetch select_list_5 into v_name_sg_fk, v_level0, v_level1, v_level2, v_level3, v_level4, v_level5;
				exit when select_list_5%NOTFOUND;
				v_no_org := get.no_org_list(v_level5);
				htp.p('<option value="select_list.sel?p_type='||p_type||'&p_level_no='|| v_next_level ||'&p_level0='|| v_level0 ||'&p_level1='|| v_level1 ||'&p_level2='|| v_level2 ||'&p_level3='|| v_level3 ||'&p_level4='|| v_level4 ||'&p_level5='|| v_level5 ||'">'|| get.text(v_name_sg_fk) ||' ('||v_no_org||')</option>');
			end loop;
			close select_list_5;

			SELECT 	count(*) INTO v_check
			FROM	list
			WHERE	level4 = v_level4;
			if (v_check < 2) then
				change_close(p_level_no, p_level0, p_level1, p_level2, p_level3, p_level4, p_level5, p_type);
			end if;

		end if;
	else

		change_close(p_level_no, p_level0, p_level1, p_level2, p_level3, p_level4, p_level5, p_type);

	end if;

	html.e_select_jump;

	htp.p('<tr><td>');
	html.back( get.txt('back') );
	htp.p('</td></tr>');


	if (p_level_no > 0) then
		-- m� bruke javascript for � f� dette til � virke i Netscape
		htp.p('
			<script language=''Javascript''>
			<!--
			function go_to() {
				window.location = ''select_list.change_close?p_type='||p_type||'&p_level_no='||p_level_no||'&p_level0='||p_level0||'&p_level1='||p_level1||'&p_level2='||p_level2||'&p_level3='||p_level3||'&p_level4='||p_level4||'&p_level5='||p_level5||''';
			}
			//-->
			</script>

			<form>
			<input type="button" value="'|| get.txt('stop_at_this_level')||' " onClick="Javascript:go_to()">
			</form>
		');

	end if;

	html.e_table;
	html.e_box;
	html.e_page_2;


END;
END sel;





---------------------------------------------------------
-- Name: 	change_close
-- Type: 	procedure
-- What: 	change opener.values and close the window
-- Author:  	Frode Klevstul
-- Start date: 	15.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE change_close
	(
		p_level_no	in number			default NULL,
		p_level0	in list.level0%type		default NULL,
		p_level1	in list.level1%type		default NULL,
		p_level2	in list.level2%type		default NULL,
		p_level3	in list.level3%type		default NULL,
		p_level4	in list.level4%type		default NULL,
		p_level5	in list.level5%type		default NULL,
		p_type		in varchar2			default NULL
	)
IS
BEGIN
DECLARE
	v_list_pk		list.list_pk%type	default NULL;
	v_name_sg_fk		list.name_sg_fk%type	default NULL;
	v_string		strng.string%type	default NULL;

BEGIN
	if ( p_level_no = 1) then
		v_list_pk := p_level0;
	elsif ( p_level_no = 2) then
		v_list_pk := p_level1;
	elsif ( p_level_no = 3) then
		v_list_pk := p_level2;
	elsif ( p_level_no = 4) then
		v_list_pk := p_level3;
	elsif ( p_level_no = 5) then
		v_list_pk := p_level4;
	elsif ( p_level_no = 6) then
		v_list_pk := p_level5;
	end if;

	-- dersom vi skal "hoppe videre" til en bestemt side er "P_TYPE" <> NULL
	if (p_type IS NOT NULL) then

		htp.p('
			<script language="Javascript">

			function jump() {
				window.location = '''||p_type||'?p_list_pk='||v_list_pk||''';
				// window.open('''|| p_type ||''', target="_self");
			 }
			</script>

			<SCRIPT language=JavaScript>
				jump();
			</SCRIPT>
		');


	else

		SELECT 	name_sg_fk INTO v_name_sg_fk
		FROM 	list
		WHERE	list_pk = v_list_pk;

		v_string := get.text(v_name_sg_fk);
		owa_pattern.change(v_string, '\W', '', 'g');

		htp.p('

			<SCRIPT LANGUAGE=''JavaScript''>
				function update_opener(pk,name) {
						if (window.opener.document.form) {
							if (window.opener.document.form.p_name) {
								window.opener.document.form.p_list_pk.value = pk;
								window.opener.document.form.p_name.value = name;
							}
						}
						window.close();
				}
			</SCRIPT>

			<SCRIPT language=''JavaScript''>
				update_opener('|| v_list_pk ||', '''|| v_string ||''');
			</SCRIPT>
		');
	end if;

END;
END change_close;



-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/
