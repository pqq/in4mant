set define off
PROMPT *** package: multilang ***

CREATE OR REPLACE PACKAGE multilang IS

        PROCEDURE startup;
        PROCEDURE list_all
        (
                p_name          in string_group.name%type               default NULL,
                p_description   in string_group.description%type        default NULL,
                p_string        in strng.string%type                    default NULL
        );
        PROCEDURE html_top
                (
                        p_link          in number default NULL
                );
        PROCEDURE insert_action
                (
                        p_string_group_pk       in string_group.string_group_pk%type    default NULL,
                        p_name                  in string_group.name%type               default NULL,
                        p_description           in string_group.description%type        default NULL
                );
        PROCEDURE edit_entry
                (
                        p_string_group_pk       in string_group.string_group_pk%type    default NULL
                );
        PROCEDURE delete_entry
                (
                        p_string_group_pk               in string_group.string_group_pk%type    default NULL
                );
        PROCEDURE delete_entry_2
                (
                        p_string_group_pk       in string_group.string_group_pk%type    default NULL,
                        p_confirm               in varchar2                             default NULL
                );
        PROCEDURE add_string
                (
                        p_string_group_pk       in string_group.string_group_pk%type    default NULL
                );
        PROCEDURE insert_string
                (
                        p_string_pk             in strng.string_pk%type                 default NULL,
                        p_string_group_pk       in string_group.string_group_pk%type    default NULL,
                        p_language_pk           in la.language_pk%type                  default NULL,
                        p_string                in strng.string%type                    default NULL
                );
        PROCEDURE edit_string
                (
                        p_string_pk             in strng.string_pk%type default NULL
                );
        PROCEDURE delete_string
                (
                        p_string_pk             in strng.string_pk%type         default NULL
                );
        PROCEDURE delete_string_2
                (
                        p_string_pk             in strng.string_pk%type         default NULL,
                        p_confirm               in varchar2                     default NULL
                );

END;
/
CREATE OR REPLACE PACKAGE BODY multilang IS

-- **************************************************
-- "PUBLIC METODER" (KALLES DIREKTE FRA WEB)
-- **************************************************

---------------------------------------------------------
-- Name:        startup
-- Type:        procedure
-- What:        start prosedyren, for � kj�re pakken
-- Author:      Frode Klevstul
-- Start date:  25.05.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter] [description]
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

        list_all;

END startup;

-- **************************************************
-- "PRIVATE METODER" (KALLES IKKE DIREKTE FRA WEB)
-- **************************************************


---------------------------------------------------------
-- Name:        edit_entry
-- Type:        procedure
-- What:        edit an entry
-- Author:      Frode Klevstul
-- Start date:  27.05.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter] [description]
---------------------------------------------------------
PROCEDURE edit_entry
                (
                        p_string_group_pk               in string_group.string_group_pk%type    default NULL
                )
IS
BEGIN
DECLARE

        v_name          string_group.name%type          default NULL;
        v_description   string_group.description%type   default NULL;

BEGIN

if ( login.timeout('multilang.startup')>0 AND login.right('multilang')>0 ) then

        SELECT name, description INTO v_name, v_description
        FROM string_group
        WHERE string_group_pk = p_string_group_pk;

        v_description := html.rem_tag(v_description);

        html_top(0);
        htp.p('
                <form name="edit_form" action="multilang.insert_action" method="post">
                <input type="hidden" name="P_string_group_pk" value="'|| p_string_group_pk ||'">
                <table border="0">
                <tr><td colspan="2" align="center"><b>update an entry in the table "string_group"</b></td></tr>
                <tr><td>name:</td><td><input type="Text" name="p_name" size="40" maxlength="50" value="'||v_name||'"></td></tr>
                <tr><td>description:</td><td><input type="Text" name="p_description" size="80" maxlength="150" value="'||v_description||'"></td></tr>
                <tr><td colspan="2"><input type="Submit" value="Update"></td></tr>
                </table>
                </form>
        ');
        html.e_page_2;

end if;

END;
END edit_entry;




---------------------------------------------------------
-- Name:        insert_action
-- Type:        procedure
-- What:        inserts the entry into the database
-- Author:      Frode Klevstul
-- Start date:  25.05.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter] [description]
---------------------------------------------------------
PROCEDURE insert_action
                (
                        p_string_group_pk       in string_group.string_group_pk%type    default NULL,
                        p_name                  in string_group.name%type               default NULL,
                        p_description           in string_group.description%type        default NULL
                )
IS
BEGIN
DECLARE

        v_string_group_pk       string_group.string_group_pk%type       default NULL;   -- henter ut pk av sekvens
        v_check                 number                                  default 0;
        v_description           string_group.description%type           default NULL;
        v_message               varchar2(300)                           default NULL;

BEGIN

if ( login.timeout('multilang.startup')>0 AND login.right('multilang')>0 ) then

        -- sjekker om det allerede finnes en forekomst med dette navnet
        if ( p_string_group_pk IS NOT NULL ) then
                SELECT count(*) INTO v_check
                FROM string_group
                WHERE name = p_name
                AND string_group_pk <> p_string_group_pk;
        else
                SELECT count(*) INTO v_check
                FROM string_group
                WHERE name = p_name;
        end if;

        if (v_check > 0 OR p_description IS NULL) then
                v_message := get.msg(1, 'there already exists an entry with name = <b>'||p_name||'</b>');
                htp.p(v_message);
                html.write('e_page'); -- skriver ut slutten p� html siden
        else
        begin
                v_description := p_description;
                v_description := html.rem_tag(v_description);

                -- henter ut neste nummer i sekvensen til tabellen
                if (p_string_group_pk is NULL AND p_name IS NOT NULL) then
                        SELECT string_group_seq.NEXTVAL
                        INTO v_string_group_pk
                        FROM dual;
                        -- legger inn i databasen
                        INSERT INTO string_group
                        (string_group_pk, name, description)
                        VALUES (v_string_group_pk, p_name, p_description);
                        commit;
                elsif (p_name IS NOT NULL) then
                begin
                        UPDATE string_group
                        SET name = p_name, description = p_description
                        WHERE string_group_pk = p_string_group_pk;
                        commit;
                end;
                end if;
        end;
        end if;
        list_all(p_name, p_description, NULL);

end if;

END;
END insert_action;

---------------------------------------------------------
-- Name:        delete_entry
-- Type:        procedure
-- What:
-- Author:      Frode Klevstul
-- Start date:  27.05.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter] [description]
---------------------------------------------------------
PROCEDURE delete_entry
                (
                        p_string_group_pk               in string_group.string_group_pk%type    default NULL
                )
IS
BEGIN
DECLARE

        v_name          string_group.name%type  default NULL;

BEGIN

if ( login.timeout('multilang.startup')>0 AND login.right('multilang')>0 ) then

        SELECT name INTO v_name
        FROM string_group
        WHERE string_group_pk = p_string_group_pk;

        html_top(0);
        htp.p('
                <form action="multilang.delete_entry_2" method="post">
                <input type="hidden" name="p_string_group_pk" value="'|| p_string_group_pk ||'">
                <table>
                <tr><td colspan="2">Are you sure you want to delete the entry <b>'|| v_name ||'</b> with pk <b>'||p_string_group_pk||'</b>?</td></tr>
                <tr><td><input type="submit" name="p_confirm" value="yes"></td><td align="right"><input type="submit" name="p_confirm" value="no"></td></tr>
                </table>
                </form>
        ');
        html.e_page_2;

end if;

END;
END delete_entry;


---------------------------------------------------------
-- Name:        delete_entry_2
-- Type:        procedure
-- What:
-- Author:      Frode Klevstul
-- Start date:  27.05.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter] [description]
---------------------------------------------------------
PROCEDURE delete_entry_2
                (
                        p_string_group_pk       in string_group.string_group_pk%type    default NULL,
                        p_confirm               in varchar2                             default NULL
                )
IS
BEGIN

if ( login.timeout('multilang.startup')>0 AND login.right('multilang')>0 ) then

        if (p_confirm = 'yes') then
                -- sletter fra strng og groups tabellen
                DELETE FROM  (
                        SELECT  *
                        FROM    strng, groups
                        WHERE string_pk = string_fk
                        )
                WHERE string_group_fk = p_string_group_pk;
                commit;

                -- sletter fra string_group
                DELETE FROM string_group
                WHERE string_group_pk = p_string_group_pk;
                commit;
        end if;
        list_all;

end if;

END delete_entry_2;


---------------------------------------------------------
-- Name:        list_all
-- Type:        procedure
-- What:        lister ut alle forekomster
-- Author:      Frode Klevstul
-- Start date:  15.05.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
-- beskrivelse av inn parametere:
--
-- [parameter]  [description]
---------------------------------------------------------
PROCEDURE list_all
        (
                p_name          in string_group.name%type               default NULL,
                p_description   in string_group.description%type        default NULL,
                p_string        in strng.string%type                    default NULL
        )
IS
BEGIN
DECLARE

        CURSOR  select_all
                (
                        v_name          in string_group.name%type               default NULL,
                        v_description   in string_group.description%type        default NULL

                ) IS
        SELECT  DISTINCT(string_group_pk), name, description
        FROM    string_group sg
        WHERE   upper(name) LIKE upper(v_name)||'%'
        AND     upper(description) LIKE upper(v_description)||'%'
        ORDER BY name;

        CURSOR  select_all_2
                (
                        v_name          in string_group.name%type               default NULL,
                        v_description   in string_group.description%type        default NULL,
                        v_string        in strng.string%type                    default NULL

                ) IS
        SELECT  DISTINCT(string_group_pk), name, description
        FROM    string_group sg, groups g, strng s
        WHERE   upper(name) LIKE upper(v_name)||'%'
        AND     upper(description) LIKE upper(v_description)||'%'
        AND     upper(string) LIKE upper(v_string)||'%'
        AND     s.string_pk = g.string_fk
        AND     g.string_group_fk = sg.string_group_pk
        ORDER BY name;

        v_string_group_pk       string_group.string_group_pk%type       default NULL;
        v_description           string_group.description%type           default NULL;
        v_name                  string_group.name%type                  default NULL;
        v_no_strings            number                                  default NULL;
        v_no_languages          number                                  default NULL;
        v_check                 number                                  default NULL;

BEGIN


if ( login.timeout('multilang.startup')>0 AND login.right('multilang')>0 ) then

        html_top(0);

        SELECT count(*) INTO v_no_languages
        FROM la
        WHERE language_pk <> 0;

        SELECT  count(*) INTO v_check
        FROM    string_group;

        htp.p('There are <b>'||v_check||'</b> entries in STRING_GROUP and <b>');

        SELECT  count(*) INTO v_check
        FROM    strng;

        htp.p(v_check ||'</b> in STRNG on <b>'||v_no_languages||'</b> different languages.<br>');

        html.b_form('multilang.list_all');
        html.b_table;
        htp.p('<tr><td colspan="2">Type ''%'' to list out all entries!</td></tr>');
        htp.p('<tr><td><b>name</b> (starts with):</td><td><input type="text" name="p_name" value="'||p_name||'"></td></tr>');
        htp.p('<tr><td><b>description</b> (starts with):</td><td><input type="text" name="p_description" value="'||p_description||'"></td></tr>');
        htp.p('<tr><td><b>string</b> (starts with):</td><td><input type="text" name="p_string" value="'||p_string||'"></td></tr>');
        htp.p('<tr><td colspan="2" align="right">'); html.submit_link('list out'); htp.p('</td></tr>');
        htp.p('<tr><td colspan="2"><hr noshade></td></tr>');
        html.e_table;
        html.e_form;

        htp.p('
                <table border="0" width="100%"><tr><td colspan="6">&nbsp;</td></tr>
                <tr bgcolor="#eeeeee"><td width="5%">pk:</td><td>name:</td><td>description:</i></td><td>no str:</i></td><td align="center">&nbsp;</td><td align="center">&nbsp;</td><td align="center">&nbsp;</td></tr>
        ');


        if ( (p_name IS NOT NULL) OR (p_description IS NOT NULL) AND (p_string IS NULL)) then

                open select_all(p_name, p_description);
                loop
                        fetch select_all into v_string_group_pk, v_name, v_description;
                        exit when select_all%NOTFOUND;

                        SELECT count(*) INTO v_no_strings
                        FROM groups
                        WHERE string_group_fk = v_string_group_pk;
                        if ( v_no_strings < v_no_languages) then
                                htp.p('<tr><td width="5%"><font color="#aaaaaa">'|| v_string_group_pk ||'</font></td><td bgcolor="#FFB9B9"><b><a href="multilang.add_string?p_string_group_pk='||v_string_group_pk||'">'|| v_name ||'</a></b></i></td><td>'|| v_description ||'</td><td align="right" bgcolor="#ff0000"><i>'|| v_no_strings ||'</i></td><td bgcolor="#000000" align="center"><a href="multilang.add_string?p_string_group_pk='||v_string_group_pk||'"><font color="#ffffff">ADD/EDIT STRING</font></a></td><td bgcolor="#000000" align="center"><a href="multilang.edit_entry?p_string_group_pk='||v_string_group_pk||'"><font color="#ffffff">UPDATE</font></a></td><td bgcolor="#000000" align="center"><a href="multilang.delete_entry?p_string_group_pk='|| v_string_group_pk ||'"><font color="#ffffff">DELETE</font></a></td></tr>');
                        else
                                htp.p('<tr><td width="5%"><font color="#aaaaaa">'|| v_string_group_pk ||'</font></td><td><b><a href="multilang.add_string?p_string_group_pk='||v_string_group_pk||'">'|| v_name ||'</a></b></i></td><td>'|| v_description ||'</td><td align="right"><i>'|| v_no_strings ||'</i></td><td bgcolor="#000000" align="center"><a href="multilang.add_string?p_string_group_pk='||v_string_group_pk||'"><font color="#ffffff">ADD/EDIT STRING</font></a></td><td bgcolor="#000000" align="center"><a href="multilang.edit_entry?p_string_group_pk='||v_string_group_pk||'"><font color="#ffffff">UPDATE</font></a></td><td bgcolor="#000000" align="center"><a href="multilang.delete_entry?p_string_group_pk='|| v_string_group_pk ||'"><font color="#ffffff">DELETE</font></a></td></tr>');
                        end if;
                end loop;
                close select_all;

        elsif ( (p_string IS NOT NULL)) then

                open select_all_2(p_name, p_description, p_string);
                loop
                        fetch select_all_2 into v_string_group_pk, v_name, v_description;
                        exit when select_all_2%NOTFOUND;

                        SELECT count(*) INTO v_no_strings
                        FROM groups
                        WHERE string_group_fk = v_string_group_pk;
                        if ( v_no_strings < v_no_languages) then
                                htp.p('<tr><td width="5%"><font color="#aaaaaa">'|| v_string_group_pk ||'</font></td><td bgcolor="#FFB9B9"><b><a href="multilang.add_string?p_string_group_pk='||v_string_group_pk||'">'|| v_name ||'</a></b></i></td><td>'|| v_description ||'</td><td align="right" bgcolor="#ff0000"><i>'|| v_no_strings ||'</i></td><td bgcolor="#000000" align="center"><a href="multilang.add_string?p_string_group_pk='||v_string_group_pk||'"><font color="#ffffff">ADD/EDIT STRING</font></a></td><td bgcolor="#000000" align="center"><a href="multilang.edit_entry?p_string_group_pk='||v_string_group_pk||'"><font color="#ffffff">UPDATE</font></a></td><td bgcolor="#000000" align="center"><a href="multilang.delete_entry?p_string_group_pk='|| v_string_group_pk ||'"><font color="#ffffff">DELETE</font></a></td></tr>');
                        else
                                htp.p('<tr><td width="5%"><font color="#aaaaaa">'|| v_string_group_pk ||'</font></td><td><b><a href="multilang.add_string?p_string_group_pk='||v_string_group_pk||'">'|| v_name ||'</a></b></i></td><td>'|| v_description ||'</td><td align="right"><i>'|| v_no_strings ||'</i></td><td bgcolor="#000000" align="center"><a href="multilang.add_string?p_string_group_pk='||v_string_group_pk||'"><font color="#ffffff">ADD/EDIT STRING</font></a></td><td bgcolor="#000000" align="center"><a href="multilang.edit_entry?p_string_group_pk='||v_string_group_pk||'"><font color="#ffffff">UPDATE</font></a></td><td bgcolor="#000000" align="center"><a href="multilang.delete_entry?p_string_group_pk='|| v_string_group_pk ||'"><font color="#ffffff">DELETE</font></a></td></tr>');
                        end if;
                end loop;
                close select_all_2;

        end if;

        htp.p('</table>');


        /* kode for � legge til ny forekomst i string_group */

        htp.p('
                <form name="form" action="multilang.insert_action" method="post">
                <table border="0">
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr bgcolor="#eeeeee"><td colspan="2" align="center"><b>add new entry in "string_group"</b></td></tr>
                <tr><td>name:</td><td><input type="Text" name="p_name" size="40" maxlength="50"></td></tr>
                <tr><td>description:</td><td><input type="Text" name="p_description" size="70" maxlength="500"></td></tr>
                <tr><td colspan="2" align="right"><input type="Submit" value="insert"></td></tr>
                </table>
                </form>
        ');


        html.e_page_2;

end if;

END;
END list_all;



---------------------------------------------------------
-- Name:        html_top
-- Type:        procedure
-- What:        skriver ut html_koden p� toppen (linker)
-- Author:      Frode Klevstul
-- Start date:  25.05.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
-- beskrivelse av inn parametere:
--
-- [parameter]  [description]
-- p_link       gir oss nr p� linken vi er p� n�
--              1 = insert_html
--              2 = edit_html
---------------------------------------------------------
PROCEDURE html_top
                (
                        p_link          in number default NULL
                )
IS
BEGIN

        html.b_adm_page('multi language');      -- skriver ut starten p� html siden

END html_top;



---------------------------------------------------------
-- Name:        add_string
-- Type:        procedure
-- What:        adds a string to a string group
-- Author:      Frode Klevstul
-- Start date:  27.05.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
-- beskrivelse av inn parametere:
--
-- [parameter]  [description]
---------------------------------------------------------
PROCEDURE add_string
                (
                        p_string_group_pk       in string_group.string_group_pk%type    default NULL
                )

IS
BEGIN
DECLARE

        CURSOR  select_all IS
        SELECT  string_pk, language_name, string
        FROM    la l, groups g, strng s
        WHERE   s.language_fk = l.language_pk
        AND     s.string_pk = g.string_fk
        AND     g.string_group_fk = p_string_group_pk
        ORDER BY s.string;

        v_string_pk             strng.string_pk%type                    default NULL;
        v_language_name         la.language_name%type           default NULL;
        v_string                strng.string%type                       default NULL;
        v_name                  string_group.name%type                  default NULL;
BEGIN

if ( login.timeout('multilang.startup')>0 AND login.right('multilang')>0 ) then

        SELECT name INTO v_name
        FROM string_group
        WHERE string_group_pk = p_string_group_pk;

        html_top(0);
        htp.p('
                <table border="0" width="100%"><tr><td colspan="5" align="center"><b>'|| v_name ||'</b></td></tr>
                <tr bgcolor="#eeeeee"><td width="5%">pk:</td><td>language:</td><td>string:</td><td align="center">&nbsp;</td><td align="center">&nbsp;</td></tr>
        ');
        -- g�r igjennom "cursoren"...
        open select_all;
        while (1>0)     -- alltid sant -> g�r igjennom alle forekomstene
        loop
                fetch select_all into v_string_pk, v_language_name, v_string;
                exit when select_all%NOTFOUND;
                htp.p('<tr><td width="5%"><font color="#dddddd">'|| v_string_pk ||'</font></td><td><b>'|| v_language_name ||'</b></i></td><td>'|| v_string ||'</i></td><td bgcolor="#000000" align="center"><a href="multilang.edit_string?p_string_pk='||v_string_pk||'"><font color="#ffffff">UPDATE</font></a></td><td bgcolor="#000000" align="center"><a href="multilang.delete_string?p_string_pk='|| v_string_pk ||'"><font color="#ffffff">DELETE</font></a></td></tr>');
        end loop;
        close select_all;

        htp.p('</table>');

        /* kode for � legge til ny forekomst i string */

        htp.p('
                <form name="form" action="multilang.insert_string" method="post">
                <input type="hidden" name="p_string_group_pk" value="'|| p_string_group_pk ||'">
                <table border="0">
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr bgcolor="#eeeeee"><td colspan="2" align="center"><b>add new entry in "string"</b></td></tr>
                <tr><td>language:</td><td>
        ');
                html.select_lang;
        htp.p('
                </td></tr>
                <tr><td>string:</td><td>
                        <textarea name="p_string" rows="20" cols="120" wrap="virtual"></textarea>
                </td></tr>
                <tr><td colspan="2" align="right"><input type="Submit" value="insert"></td></tr>
                </table>
                </form>
        ');

        html.e_page_2;

end if;

END;
END add_string;





---------------------------------------------------------
-- Name:        insert_string
-- Type:        procedure
-- What:        inserts the string
-- Author:      Frode Klevstul
-- Start date:  27.05.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter] [description]
---------------------------------------------------------
PROCEDURE insert_string
                (
                        p_string_pk             in strng.string_pk%type                 default NULL,
                        p_string_group_pk       in string_group.string_group_pk%type    default NULL,
                        p_language_pk           in la.language_pk%type          default NULL,
                        p_string                in strng.string%type                    default NULL
                )
IS
BEGIN
DECLARE

        v_string_pk             strng.string_pk%type                    default NULL;   -- henter ut pk av sekvens
        v_string_group_pk       string_group.string_group_pk%type       default NULL;
        v_string                strng.string%type                       default NULL;
        v_check                 number                                  default 0;
        v_message               varchar2(300)                           default NULL;

BEGIN


if ( login.timeout('multilang.startup')>0 AND login.right('multilang')>0 ) then

        if (p_string_pk IS NULL) then           -- insert new string
                SELECT  count(*) INTO v_check
                FROM    groups g, strng s
                WHERE   s.language_fk = p_language_pk
                AND     g.string_fk = s.string_pk
                AND     g.string_group_fk = p_string_group_pk;
        else                                    -- update string
                -- sjekker at spr�ket vi oppdatere til ikke er tatt fra f�r
                -- av en annen streng i samme gruppe
                SELECT  count(*) INTO v_check
                FROM    groups g, strng s
                WHERE   s.string_pk = g.string_fk       -- kobler i sammen tabellene
                AND     s.language_fk = p_language_pk   -- spr�ket vi skal oppdatere til
                AND     s.language_fk <> (              -- ikke ta med spr�ket forekomsten hadde fra f�r
                                SELECT  language_fk
                                FROM    strng
                                WHERE   string_pk = p_string_pk )
                AND     g.string_group_fk = (           -- sjekke alle strengene i riktig gruppe
                                SELECT  string_group_fk
                                FROM    groups
                                WHERE   string_fk = p_string_pk );
        end if;

        if (v_check > 0) then
                v_message := get.msg(1, 'there already exists an entry on the choosen language (language_pk = <b>'||p_language_pk||'</b>)');
                htp.p(v_message);
                html.write('e_page'); -- skriver ut slutten p� html siden
        else
                v_string := p_string;
                -- v_string := lower (v_string);

                -- henter ut neste nummer i sekvensen til tabellen
                if (p_string_pk is NULL AND p_string IS NOT NULL) then
                        SELECT strng_seq.NEXTVAL
                        INTO v_string_pk
                        FROM dual;
                        -- legger inn i databasen
                        INSERT INTO strng
                        (string_pk, language_fk, string)
                        VALUES (v_string_pk, p_language_pk, v_string);
                        commit;

                        INSERT INTO groups
                        (string_group_fk, string_fk)
                        VALUES (p_string_group_pk, v_string_pk);
                        commit;

                elsif (p_string IS NOT NULL) then
                begin
                        UPDATE strng
                        SET language_fk = p_language_pk, string = v_string
                        WHERE string_pk = p_string_pk;
                        commit;
                end;
                end if;
        end if;

        if (p_string_group_pk IS NULL) then -- if UPDATE string, then "string_group_pk" is NULL
                SELECT  string_group_fk INTO v_string_group_pk
                FROM    groups
                WHERE   string_fk = p_string_pk;
        else
                v_string_group_pk := p_string_group_pk;
        end if;

        add_string(v_string_group_pk);

end if;

END;
END insert_string;


---------------------------------------------------------
-- Name:        edit_string
-- Type:        procedure
-- What:        edit a string
-- Author:      Frode Klevstul
-- Start date:  27.05.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter] [description]
---------------------------------------------------------
PROCEDURE edit_string
                (
                        p_string_pk             in strng.string_pk%type default NULL
                )
IS
BEGIN
DECLARE

        v_string                strng.string%type               default NULL;
        v_language_fk           strng.language_fk%type          default NULL;

BEGIN

if ( login.timeout('multilang.startup')>0 AND login.right('multilang')>0 ) then

        SELECT string, language_fk INTO v_string, v_language_fk
        FROM strng
        WHERE string_pk = p_string_pk;

        html_top(0);
        htp.p('
                <form name="form" action="multilang.insert_string" method="post">
                <input type="hidden" name="p_string_pk" value="'|| p_string_pk ||'">
                <table border="0">
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr bgcolor="#eeeeee"><td colspan="2" align="center"><b>add new entry in "string"</b></td></tr>
                <tr><td>language:</td><td>
        ');
                html.select_lang(v_language_fk);
        htp.p('
                </td></tr>
                <tr><td>string:</td><td><textarea name="p_string" rows="20" cols="120" wrap="virtual">'|| v_string ||'</textarea></tr>
                <tr><td colspan="2" align="right"><input type="Submit" value="update"></td></tr>
                </table>
                </form>
        ');
        html.e_page_2;

end if;

END;
END edit_string;



---------------------------------------------------------
-- Name:        delete_string
-- Type:        procedure
-- What:
-- Author:      Frode Klevstul
-- Start date:  28.05.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter] [description]
---------------------------------------------------------
PROCEDURE delete_string
                (
                        p_string_pk             in strng.string_pk%type         default NULL
                )
IS
BEGIN
DECLARE

        v_string        strng.string%type       default NULL;

BEGIN

if ( login.timeout('multilang.startup')>0 AND login.right('multilang')>0 ) then

        SELECT string INTO v_string
        FROM strng
        WHERE string_pk = p_string_pk;

        html_top(0);
        htp.p('
                <form action="multilang.delete_string_2" method="post">
                <input type="hidden" name="p_string_pk" value="'|| p_string_pk ||'">
                <table>
                <tr><td colspan="2">Are you sure you want to delete the entry <b>'|| v_string ||'</b> with pk <b>'||p_string_pk||'</b>?</td></tr>
                <tr><td><input type="submit" name="p_confirm" value="yes"></td><td align="right"><input type="submit" name="p_confirm" value="no"></td></tr>
                </table>
                </form>
        ');
        html.e_page_2;

end if;

END;
END delete_string;


---------------------------------------------------------
-- Name:        delete_string_2
-- Type:        procedure
-- What:
-- Author:      Frode Klevstul
-- Start date:  28.05.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter] [description]
---------------------------------------------------------
PROCEDURE delete_string_2
                (
                        p_string_pk             in strng.string_pk%type         default NULL,
                        p_confirm               in varchar2                     default NULL
                )
IS
BEGIN
DECLARE
        v_string_group_pk       string_group.string_group_pk%type       default NULL;

BEGIN

if ( login.timeout('multilang.startup')>0 AND login.right('multilang')>0 ) then

        SELECT  string_group_fk INTO v_string_group_pk
        FROM    groups
        WHERE   string_fk = p_string_pk;

        if (p_confirm = 'yes') then
                DELETE FROM strng
                WHERE string_pk = p_string_pk;
                commit;
        end if;

        add_string(v_string_group_pk);

end if;

END;
END delete_string_2;



-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/
show errors;

