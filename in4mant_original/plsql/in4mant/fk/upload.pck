CREATE OR REPLACE PACKAGE upload IS

	PROCEDURE startup;
	PROCEDURE org_logo;
	PROCEDURE form
		(
			p_type		in varchar2	default NULL
		);
	PROCEDURE error
		(
			p_error_txt 		in varchar2		default NULL,
			p_url			in varchar2		default NULL
		);
	PROCEDURE add_org_logo
		(
			p_picture_id 		in number		default NULL
                );

END;
/
CREATE OR REPLACE PACKAGE BODY upload IS

---------------------------------------------------------
-- Name: 	startup
-- Type: 	procedure
-- What: 	start procedure, to run the package
-- Author:  	Frode Klevstul
-- Start date: 	27.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN
if ( login.timeout('upload.startup')>0 ) then

	htp.p('upload');

end if;
END startup;


---------------------------------------------------------
-- Name: 	org_logo
-- Type: 	procedure
-- What: 	kalles fra perl script for � hoppe riktig (org logo opplasting)
-- Author:  	Frode Klevstul
-- Start date: 	13.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE org_logo
IS
BEGIN
if ( login.timeout('upload.org_logo')>0 ) then

	form('org_logo');

end if;
END org_logo;


---------------------------------------------------------
-- Name: 	form
-- Type: 	procedure
-- What: 	general form to upload files
-- Author:  	Frode Klevstul
-- Start date: 	13.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE form
	(
		p_type		in varchar2	default NULL
	)
IS
BEGIN
DECLARE
	v_picture_id		varchar2(50)	default NULL;
	v_box_heading		varchar2(150)	default NULL;
	v_upload_type		varchar2(50)	default NULL;
	v_help_module		varchar2(50)	default NULL;

BEGIN
if ( login.timeout('upload.startup')>0) then

	-- oppretter variabler utifra filtype som skal lastes opp
	if (p_type = 'org_logo') then
		v_picture_id 	:= get.oid;
		v_box_heading	:= get.txt('upload_logo_for ') ||' '|| get.oname(get.oid);
		v_upload_type	:= get.txt('organization_logo');
		v_help_module	:= 'upload(org_logo)';
	else
		htp.p( get.msg(1, 'wrong type') );
		return;
	end if;

	-- ---------------------
	-- skriver ut HTML kode
	-- ---------------------
	html.b_page(NULL, NULL, NULL, 2);
	html.b_box( v_box_heading , '100%', v_help_module);
	html.b_table;

	htp.p('
		<form name="form" action="'|| get.value('upload_script') ||'" method="post" enctype="multipart/form-data">
		<input type="hidden" name="p_type" value="'|| p_type ||'">
		<input type="hidden" name="p_picture_id" value="'|| v_picture_id ||'">
		<tr><td colspan="2">'|| get.txt('upload_'||p_type) ||'</td></tr>
		<tr><td colspan="2"> &nbsp; </td></tr>
		<tr>
			<td>'|| v_upload_type ||':</td><td><input type="file" maxlength=100 name="p_file" size=100></td>
		</tr>
	');
	htp.p('<tr><td colspan="2" align="right">');
	html.submit_link( get.txt('upload') );
	htp.p('</td></tr>');

	htp.p('<tr><td colspan="2" align="right">');
	html.back( get.txt('go_back') );
	htp.p('</td></tr>');


	html.e_table;
	html.e_form;
	html.e_box;
	html.e_page;


end if;
END;
END form;



---------------------------------------------------------
-- Name: 	error
-- Type: 	procedure
-- What: 	writes out error message
-- Author:  	Frode Klevstul
-- Start date: 	13.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE error
		(
			p_error_txt 		in varchar2		default NULL,
			p_url			in varchar2		default NULL
                )
IS
BEGIN
if ( login.timeout('upload.startup')>0 ) then

	html.b_page;
	html.b_box( get.txt('upload_error') );
	html.b_table;

	htp.p('
		<tr><td>'|| get.txt(p_error_txt) ||'</td></tr>
	');

	htp.p('<tr><tdcolspan="2"> &nbsp; </td></tr>');
	htp.p('<tr><td align="right">');
	html.button_link('try_upload_again', p_url);
	htp.p('</td></tr>');

	html.e_table;
	html.e_box;
	html.e_page;


end if;
END error;





---------------------------------------------------------
-- Name: 	add_org_logo
-- Type: 	procedure
-- What: 	accept the org logo
-- Author:  	Frode Klevstul
-- Start date: 	13.08.2000
-- Desc:
---------------------------------------------------------
PROCEDURE add_org_logo
		(
			p_picture_id 		in number		default NULL
                )
IS
BEGIN
DECLARE
	v_logo		organization.logo%type	default NULL;
BEGIN
if ( login.timeout('photo.startup')>0 ) then

	-- -----------------------------------------------------
	-- sjekker at picture_id stemmer med organisasjons pk'en
	-- -----------------------------------------------------
	if (p_picture_id <> get.oid) then
		htp.p( get.msg(1, 'wrong values') );
		return;
	end if;

	-- -----------------------
	-- oppdaterer i databasen
	-- -----------------------
	v_logo := p_picture_id ||'.jpg';

	UPDATE 	organization
	SET	logo = v_logo
	WHERE	organization_pk = p_picture_id;
	commit;

	html.jump_to( get.value('upload_thumb_and_move')||'?org_logo�admin.startup�'||v_logo);

end if;
END;
END add_org_logo;






-- ++++++++++++++++++++++++++++++++++++++++++++++ --

END; -- slutter pakke kroppen
/
