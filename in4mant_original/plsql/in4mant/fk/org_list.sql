	set define off
PROMPT *** package: org_list ***

CREATE OR REPLACE PACKAGE org_list IS

	PROCEDURE startup;
	PROCEDURE show_lists
		(
			p_organization_pk	in organization.organization_pk%type	default NULL
		);
	PROCEDURE add_to_list
		(
			p_organization_pk	in list_org.organization_fk%type	default NULL,
			p_list_pk		in list.list_pk%type			default NULL,
			p_name			in varchar2				default NULL
		);
	PROCEDURE rem_from_list
		(
			p_organization_pk	in list_org.organization_fk%type	default NULL,
			p_list_pk		in list.list_pk%type			default NULL
		);

END;
/
CREATE OR REPLACE PACKAGE BODY org_list IS

---------------------------------------------------------
-- Name: 	startup
-- Type: 	procedure
-- What: 	start prosedyren, for � kj�re pakken
-- Author:  	Frode Klevstul
-- Start date: 	24.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

 if ( login.timeout('org_list.startup')>0 AND login.right('org_list')>0 ) then

	select_org.list_out(NULL, NULL, NULL, NULL, 'NO', 'org_list.show_lists');

end if;

END startup;



---------------------------------------------------------
-- Name: 	show_lists
-- Type: 	procedure
-- What: 	viser hvilke lister selskapet er med p�
-- Author:  	Frode Klevstul
-- Start date: 	24.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE show_lists
	(
		p_organization_pk	in organization.organization_pk%type	default NULL
	)
IS
BEGIN
DECLARE

	v_name_sg_fk		list.name_sg_fk%type				default NULL;
	v_list_pk			list.list_pk%type					default NULL;
	v_organization_pk	organization.organization_pk%type	default NULL;
	v_service_pk		service.service_pk%type				default NULL;
	v_i4_admin			boolean								default FALSE;			
	v_logged_in			boolean								default FALSE;

	CURSOR 	select_all IS
	SELECT 	l.name_sg_fk, l.list_pk
	FROM	list_org lo, list l, list_type lt
	WHERE	lo.list_fk = list_pk
	AND	organization_fk = v_organization_pk
	AND	lt.list_type_pk = l.list_type_fk
	AND NOT	lt.list_type_pk < 0
	AND NOT	lt.description LIKE 'my_personal_list%';

BEGIN

	if ( login.right('org_list')>0 ) then 							-- brukeren har ADM rettigheter (og er logget inn)
	
		v_logged_in := true;										-- setter logged_in variabelen til � v�re sann

		v_organization_pk := p_organization_pk;						-- henter organisasjonsnummeret
		if (v_organization_pk IS NULL) then							-- dersom det ikke sendes inn org_pk
			htp.p('p_organization_pk IS NULL');
			return;
		end if;

		html.b_adm_page('list_adm');								-- skriver ut admin side for i4 administratorer
		v_i4_admin := true;											-- setter admin variabel til � v�re 'true'
	
	else 															-- dersom brukeren ikke har ADM rettigheter
	
		if ( login.timeout('admin.startup')>0 ) then 				-- dersom brukeren er logget inn
	
			v_logged_in := true;									-- setter logged_in variabelen til � v�re sann
	
			v_organization_pk := get.oid;							-- henter org_pk til brukeren som er logget inn
			if (v_organization_pk IS NULL) then						-- dersom det ikke sendes inn org_pk
				get.error_page(1, 'not a client user', NULL, 'cgi-bin/main_page.startup');
				return;
			end if;
	
			html.b_page(NULL,NULL,NULL,2);							-- starter admin side for vanlige brukere
	
		end if;
	
	end if;
	
	
	if ( v_logged_in ) then											-- dersom brukeren er logget inn...
	
		v_service_pk := get.oserv(v_organization_pk);				-- henter tjenestenummeret til org vi har
		html.b_box( get.txt('list_for')||' '|| get.oname(v_organization_pk),'100%','org_list.show_lists');
	 	html.b_table;
		htp.p('
			<tr><td colspan="2">'|| get.txt('org_list_desc') ||'</td></tr>
			<tr><td colspan="2">&nbsp;</td></tr>
		');
		
		open select_all;
		loop
			fetch select_all into v_name_sg_fk, v_list_pk;
			exit when select_all%NOTFOUND;
			htp.p('<tr><td>'|| get.text(v_name_sg_fk) ||'</td><td>'); html.button_link(get.txt('delete_list'),'org_list.rem_from_list?p_organization_pk='||v_organization_pk||'&p_list_pk='||v_list_pk); htp.p('</td></tr>');
		end loop;
		close select_all;
	
		html.e_table;
	
		html.b_form('org_list.add_to_list');
		html.b_table('60%');
		htp.p('<input type="hidden" name="p_organization_pk" value="'||v_organization_pk||'">');
		htp.p('<tr><td>'||get.txt('add_org_to_list')||'</td><td><input type="hidden" name="p_list_pk" value="">');

		if (v_i4_admin) then
			htp.p('<input type="Text" name="p_name" size="30" maxlength="250" value="" onSelect=javascript:openWin(''select_list.sel_type'',300,200) onClick=javascript:openWin(''select_list.sel_type'',300,200)>'|| html.popup( get.txt('select_list'), 'select_list.sel_type', '300', '200') ||'');
		else
			htp.p('<input type="Text" name="p_name" size="30" maxlength="250" value="" onSelect=javascript:openWin(''select_list.sel_type'',300,200) onClick=javascript:openWin(''select_list.sel_type?p_service_fk='||v_service_pk||''',300,200)>'|| html.popup( get.txt('select_list'), 'select_list.sel_type?p_service_fk='||v_service_pk, '300', '200') ||'');
		end if;

		htp.p('</td></tr>');
		htp.p('<tr><td colspan="2" align="right">');  html.submit_link(get.txt('add_list')); htp.p('</td></tr>');
		html.e_table;
		html.e_form;

		html.e_box;

		if (v_i4_admin) then
			html.e_adm_page;
		else
			html.e_page;
		end if;

	end if;

END;
END show_lists;




---------------------------------------------------------
-- Name: 		add_to_list
-- Type: 		procedure
-- What: 		legger et selskap til en liste
-- Author:  	Frode Klevstul
-- Start date: 	24.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE add_to_list
	(
		p_organization_pk	in list_org.organization_fk%type	default NULL,
		p_list_pk			in list.list_pk%type				default NULL,
		p_name				in varchar2							default NULL
	)
IS
BEGIN
DECLARE

	v_check				number									default NULL;
	v_organization_pk	organization.organization_pk%type       default NULL;
	v_logged_in			boolean									default FALSE;

BEGIN


	if ( login.right('org_list')>0 ) then 							-- brukeren har ADM rettigheter (og er logget inn)
	
		v_logged_in := true;										-- setter logged_in variabelen til � v�re sann

		v_organization_pk := p_organization_pk;						-- henter organisasjonsnummeret
		if (v_organization_pk IS NULL) then							-- dersom det ikke sendes inn org_pk
			htp.p('p_organization_pk IS NULL');
			return;
		end if;
	
	else 															-- dersom brukeren ikke har ADM rettigheter
	
		if ( login.timeout('admin.startup')>0 ) then 				-- dersom brukeren er logget inn
	
			v_logged_in := true;									-- setter logged_in variabelen til � v�re sann
	
			v_organization_pk := get.oid;							-- henter org_pk til brukeren som er logget inn
			if (v_organization_pk IS NULL) then						-- dersom det ikke sendes inn org_pk
				get.error_page(1, 'not a client user', NULL, 'cgi-bin/main_page.startup');
				return;
			end if;
	
		end if;
	
	end if;
	
	
	if ( v_logged_in ) then											-- dersom brukeren er logget inn...

		SELECT 	count(*) into v_check
		FROM	list_org
		WHERE	organization_fk = v_organization_pk
		AND	list_fk = p_list_pk;

		if (v_check = 0) then
			INSERT INTO list_org
			(organization_fk, list_fk)
			VALUES (v_organization_pk, p_list_pk);
		end if;

		org_list.show_lists(v_organization_pk);
	
	end if;


END;
END add_to_list;




---------------------------------------------------------
-- Name: 	rem_from_list
-- Type: 	procedure
-- What: 	fjerner et selskap fra en liste
-- Author:  	Frode Klevstul
-- Start date: 	24.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE rem_from_list
	(
		p_organization_pk	in list_org.organization_fk%type	default NULL,
		p_list_pk		in list.list_pk%type			default NULL
	)
IS
BEGIN
DECLARE

    v_organization_pk       organization.organization_pk%type       default NULL;
	v_logged_in				boolean									default FALSE;

BEGIN

	if ( login.right('org_list')>0 ) then 							-- brukeren har ADM rettigheter (og er logget inn)
	
		v_logged_in := true;										-- setter logged_in variabelen til � v�re sann

		v_organization_pk := p_organization_pk;						-- henter organisasjonsnummeret
		if (v_organization_pk IS NULL) then							-- dersom det ikke sendes inn org_pk
			htp.p('p_organization_pk IS NULL');
			return;
		end if;
	
	else 															-- dersom brukeren ikke har ADM rettigheter
	
		if ( login.timeout('admin.startup')>0 ) then 				-- dersom brukeren er logget inn
	
			v_logged_in := true;									-- setter logged_in variabelen til � v�re sann
	
			v_organization_pk := get.oid;							-- henter org_pk til brukeren som er logget inn
			if (v_organization_pk IS NULL) then						-- dersom det ikke sendes inn org_pk
				get.error_page(1, 'not a client user', NULL, 'cgi-bin/main_page.startup');
				return;
			end if;
	
		end if;
	
	end if;
	
	
	if ( v_logged_in ) then											-- dersom brukeren er logget inn...

		DELETE FROM list_org
		WHERE organization_fk = v_organization_pk
		AND list_fk = p_list_pk;

		org_list.show_lists(v_organization_pk);

	end if;

END;
END rem_from_list;




-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/

show errors;
