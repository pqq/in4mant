CREATE OR REPLACE PACKAGE store IS

	PROCEDURE serv
		(
			p_service_pk 		in service.service_pk%type	default NULL
		);
	PROCEDURE lang
		(
			p_language_pk 		in la.language_pk%type	default NULL
		);

END;
/
CREATE OR REPLACE PACKAGE BODY store IS



---------------------------------------------------------
-- Name: 	set_service
-- Type: 	procedure
-- What: 	returns the language the user has choosen
-- Author:  	Frode Klevstul
-- Start date: 	09.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE serv
	(
		p_service_pk 		in service.service_pk%type	default NULL
	)
IS
BEGIN
DECLARE
	v_check		number		default NULL;

BEGIN

	SELECT 	count(*) INTO v_check
	FROM 	service
	WHERE	service_pk = p_service_pk;

	if ( v_check IS NULL ) then -- spr�ket finnes ikke
		htp.p( get.msg(1, ' the procedure "servic.set_service" is called width false service_pk ("'||p_service_pk||'" dont exists in service table)') );
	else
		system.set_cookie('in4mant_service', p_service_pk);
	end if;

END;
END serv;



---------------------------------------------------------
-- Name: 	lang
-- Type: 	procedure
-- What: 	stores a language
-- Author:  	Frode Klevstul
-- Start date: 	01.06.2000
-- Desc:
---------------------------------------------------------
PROCEDURE lang
	(
		p_language_pk 		in la.language_pk%type	default NULL
	)
IS
BEGIN
DECLARE
	v_check		number		default NULL;

BEGIN

	SELECT 	count(*) INTO v_check
	FROM 	la
	WHERE	language_pk = p_language_pk;

	if ( v_check IS NULL ) then -- spr�ket finnes ikke
		htp.p( get.msg(1, ' the procedure "store.lang" is called width false language_pk ("'||p_language_pk||'" dont exists in language table)') );
	else
		system.set_cookie('in4mant_lang', p_language_pk);
	end if;

END;
END lang;




-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/
