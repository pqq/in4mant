---------------------------------------------------------
-- Name:        write_calendar_files
-- Type:        procedure
-- What:        a proc that goes through the database and
--              writes calendar "email" files to the OS.
--				This should be executed every day at 12:00
-- Author:      Frode Klevstul
-- Start date:  22.03.2001
-- Desc:
---------------------------------------------------------
PROCEDURE write_calendar_files
IS
BEGIN
DECLARE

    v_date		date	default to_date( (to_char(sysdate, 'YYYY DD/MM')||' 12:00'), 'YYYY DD/MM HH24:MI');

    -- Cursor that selects all documents to be sendt out with email.
    CURSOR  select_calendar IS
    SELECT  calendar_pk, organization_fk, language_fk, event_date, heading, message
    FROM    calendar c, organization o
    WHERE   c.organization_fk = o.organization_pk
    AND     o.accepted = 1
    AND     c.event_date between v_date and v_date + 1.75
    AND     c.calendar_pk = 1337;
--    AND     c.accepted = 1;

    v_calendar_pk	calendar.calendar_pk%type				default NULL;
    v_organization_pk	organization.organization_pk%type	default NULL;
    v_language_pk	la.language_pk%type						default NULL;
    v_event_date	calendar.event_date%type				default NULL;
    v_heading		calendar.heading%type					default NULL;
    v_message		calendar.message%type					default NULL;

    -- Cursor that selects out users related to an organization.
    CURSOR  select_users
            (
                    v_organization_pk       in organization.organization_pk%type,
                    v_language_pk           in la.language_pk%type
            ) IS
    SELECT  DISTINCT(lu.user_fk), ud.email
    FROM    list_org lo, list l, list_user lu, list_type lt, user_details ud
    WHERE   lo.list_fk = l.list_pk
    AND     lt.list_type_pk = l.list_type_fk
    AND     lu.language_fk = v_language_pk
    AND     lo.organization_fk = v_organization_pk
    AND     lu.user_fk = ud.user_fk
    AND
            (
                    lu.list_fk = l.list_pk
            OR      lu.list_fk = l.level0
            OR      lu.list_fk = l.level1
            OR      lu.list_fk = l.level2
            OR      lu.list_fk = l.level3
            OR      lu.list_fk = l.level4
            OR      lu.list_fk = l.level5
            );

    v_user_pk               usr.user_pk%type                        default NULL;
    v_email                 user_details.email%type                 default NULL;


    v_name                  organization.name%type                  default NULL;
    v_file                  utl_file.file_type                      default NULL;
    v_email_dir             varchar2(100)                           default NULL;
    v_email_error_dir       varchar2(100)                           default NULL;
    v_number                number                                  default NULL;
    v_message_txt           varchar2(4000)                          default NULL;
    v_msg_footer            varchar2(4000)                          default NULL;
    v_check                 number                                  default NULL;
    v_publish_info	    	varchar2(1000)			    			default NULL;
    v_publish_info_html		varchar2(1000)			    			default NULL;
    v_et_text		    	varchar2(4000)			    			default NULL;
    v_et_html		    	varchar2(4000)			    			default NULL;
    v_event_line	    	varchar2(300)			    			default NULL;
    v_source_line	    	varchar2(300)			    			default NULL;
    v_url_line_txt	    	varchar2(500)			    			default NULL;
    v_url_line_html	    	varchar2(500)			    			default NULL;
    v_url		    		varchar2(300)			    			default NULL;
    v_email_text_pk         email_text.email_text_pk%type           default NULL;
    v_name_sg_fk            email_text.name_sg_fk%type              default NULL;
    v_mail_log_pk           mail_log.mail_log_pk%type               default NULL;
    v_no_users              number                                  default NULL;

BEGIN

	v_email_dir     	:= get.value('email_dir');
	v_email_error_dir	:= v_email_dir ||'/email_error';

    -- -----------------------------------------------------------------------
    -- henter ut dokumenter som er publisert og hvor det skal sendes ut mail
    -- -----------------------------------------------------------------------
    OPEN select_calendar;
    LOOP
        FETCH select_calendar INTO v_calendar_pk, v_organization_pk, v_language_pk, v_event_date, v_heading, v_message;
        EXIT WHEN select_calendar%NOTFOUND;

        v_message_txt   := get.txt('email_update_heading', v_language_pk);
        v_msg_footer    := get.txt('email_update_footer', v_language_pk);
        v_name          := get.oname(v_organization_pk);
        v_no_users      := 0; -- antall mottagere av dette dokumentet

		v_message_txt := REPLACE (v_message_txt, '[subject]' , v_name ||' - '|| SUBSTR(v_heading, 0, 20) ||'  ...' );
	
		v_event_line			:= get.txt('event_date', v_language_pk) ||': '|| to_char(v_event_date, get.txt('date_long', v_language_pk));
		v_source_line			:= get.txt('source', v_language_pk)||': '|| v_name;
		v_url					:= get.olink(v_organization_pk, 'cal_util.show_calendar?p_calendar_pk='||v_calendar_pk, v_language_pk);
		v_url_line_txt			:= get.txt('url_to_news', v_language_pk)||': '|| v_url;
		v_url_line_html			:= get.txt('url_to_news', v_language_pk)||': <a href="'|| v_url ||'">'|| v_url ||'</a>';

		-- m� st� helt f�rst p� linja
		v_publish_info := v_event_line ||'
'|| v_source_line ||'
'|| v_url_line_txt;

		v_publish_info_html := v_event_line ||'<br>
'|| v_source_line ||'<br>
'|| v_url_line_html;

        SELECT  email_script_seq.NEXTVAL
        INTO    v_number
        FROM    dual;

        -- ------------------------------------------------
        -- skriver selve mailen (innholdet) *** START ***
        -- ------------------------------------------------

        -- legger eventuelt til reklametekst i mail
        v_email_text_pk := get.et(v_organization_pk, 'CB');
        if (v_email_text_pk IS NOT NULL) then
	        SELECT  name_sg_fk INTO v_name_sg_fk
	        FROM    email_text
	        WHERE   email_text_pk = v_email_text_pk;

            v_et_text := get.text(v_name_sg_fk, v_language_pk);
            v_et_html := v_et_text;

			-- splitter opp teksten i en TEXT del og en HTML del
			owa_pattern.change(v_et_text, '\n', '���', 'g');
			owa_pattern.change(v_et_text, '</txt-message>.*$', NULL, 'g');
			owa_pattern.change(v_et_text, '^<txt-message>', NULL);
			owa_pattern.change(v_et_text, '���', '\n', 'g');

			owa_pattern.change(v_et_html, '\n', '���', 'g');
			owa_pattern.change(v_et_html, '^.*<html-message>', NULL, 'g');
			owa_pattern.change(v_et_html, '</html-message>', NULL);
			owa_pattern.change(v_et_html, '���', '\n', 'g');
	    end if;


        v_file := utl_file.fopen( v_email_dir, v_number||'_message.txt' ,'a');

		-- ------------------------------------------------------
		-- skriver ut headeren til emailen som skal sendes ut
		-- ------------------------------------------------------
		utl_file.put(v_file, substr(v_message_txt,1,999));
		utl_file.put(v_file, substr(v_message_txt,1000,1000));
		utl_file.put(v_file, substr(v_message_txt,2000,1000));
		utl_file.put_line(v_file, substr(v_message_txt,3000,1000));

        -- -----------------------------
		-- skriver mailen som ren tekst
        -- -----------------------------
		utl_file.put_line(v_file, '<txt-message>');
		utl_file.new_line(v_file, 1);
		utl_file.put(v_file, v_publish_info );
		utl_file.new_line(v_file, 4);
        utl_file.put(v_file, v_heading );
        utl_file.new_line(v_file, 3);

		utl_file.put(v_file, substr(v_message,1,999));
		utl_file.put(v_file, substr(v_message,1000,1000));
		utl_file.put(v_file, substr(v_message,2000,1000));
		utl_file.put(v_file, substr(v_message,3000,1000));

        utl_file.new_line(v_file, 3);
        utl_file.put(v_file, substr(v_msg_footer,1,999));
        utl_file.put(v_file, substr(v_msg_footer,1000,1000));
        utl_file.put(v_file, substr(v_msg_footer,2000,1000));
        utl_file.put(v_file, substr(v_msg_footer,3000,1000));

        utl_file.new_line(v_file, 10);

        utl_file.put(v_file, substr(v_et_text,1,999));
        utl_file.put(v_file, substr(v_et_text,1000,1000));
        utl_file.put(v_file, substr(v_et_text,2000,1000));
        utl_file.put(v_file, substr(v_et_text,3000,1000));

		utl_file.new_line(v_file, 1);
		utl_file.put_line(v_file, '</txt-message>');

        -- -------------------------------------------------------------
		-- skriver mailen som html
        -- -------------------------------------------------------------
		utl_file.put_line(v_file, '<html-message>');

		utl_file.put_line(v_file, '<br>');
        utl_file.put(v_file, v_publish_info_html );
		utl_file.put_line(v_file, '<br><br><br><br><h2>');
        utl_file.put(v_file, v_heading );
		utl_file.put_line(v_file, '</h2>');

		utl_file.put(v_file, substr(v_message,1,999));
		utl_file.put(v_file, substr(v_message,1000,1000));
		utl_file.put(v_file, substr(v_message,2000,1000));
		utl_file.put(v_file, substr(v_message,3000,1000));

        utl_file.put_line(v_file, '<br><br><br><br>');

		owa_pattern.change(v_msg_footer, '\n', '<br>', 'g');
        utl_file.put(v_file, substr(v_msg_footer,1,999));
        utl_file.put(v_file, substr(v_msg_footer,1000,1000));
        utl_file.put(v_file, substr(v_msg_footer,2000,1000));
        utl_file.put(v_file, substr(v_msg_footer,3000,1000));

        utl_file.put_line(v_file, '<br><br><br><br>');
        utl_file.put(v_file, substr(v_et_html,1,999));
        utl_file.put(v_file, substr(v_et_html,1000,1000));
        utl_file.put(v_file, substr(v_et_html,2000,1000));
        utl_file.put(v_file, substr(v_et_html,3000,1000));

        utl_file.new_line(v_file, 1);
		utl_file.put_line(v_file, '</html-message>');

        utl_file.fclose(v_file);
        -- ---------------------------------------------------------
        -- /slutt p� � skrive innholdet av mailen *** SLUTT ***
        -- ---------------------------------------------------------

        -- ----------------------------------------------------------------------------------
        -- skriver mottager fil ********* START **********
        -- ----------------------------------------------------------------------------------
        v_file := utl_file.fopen( v_email_dir, v_number||'_email-list.txt', 'a');
        -- ----------------------------------------------------------
        -- henter ut brukere tilknyttet en organisasjon p� ett spr�k
        -- ----------------------------------------------------------
        OPEN select_users(v_organization_pk, v_language_pk);
        LOOP
	        FETCH select_users INTO v_user_pk, v_email;
	        EXIT WHEN select_users%NOTFOUND;

			utl_file.put_line(v_file, v_email);
			v_no_users := v_no_users + 1; -- antall email mottagere

        END LOOP;
        CLOSE select_users;
        utl_file.fclose(v_file);
        -- -------------------------------------------------------------------------
        -- /slutt p� � skrive mottager fil ********* END **********
        -- -------------------------------------------------------------------------

        -- -----------------------------------------------
        -- oppdaterer dokument status (til "mail utsendt")
        -- -----------------------------------------------
        UPDATE  calendar
        SET     accepted = 2
        WHERE   calendar_pk = v_calendar_pk;
        commit;

        -- ------------------------------------
        -- legg inn informasjon i mail_log
        -- ------------------------------------
        SELECT  mail_log_seq.NEXTVAL
        INTO    v_mail_log_pk
        FROM    dual;

        INSERT INTO mail_log
        (mail_log_pk, volume, sendt_date, organization_fk, calendar_fk, email_text_fk)
        VALUES (v_mail_log_pk, v_no_users, sysdate, v_organization_pk, v_calendar_pk, v_email_text_pk);
        commit;

	END LOOP;
	CLOSE select_calendar;


EXCEPTION
	WHEN UTL_FILE.WRITE_ERROR THEN
	BEGIN
		v_file := utl_file.fopen( v_email_error_dir, v_number||'_WRITE_ERROR.txt', 'a');
		utl_file.put_line(v_file, 'UTL_FILE EXCEPTION! calendar_pk: '||v_calendar_pk);
		utl_file.fclose(v_file);
	        UPDATE  calendar
		SET     accepted = 2
		WHERE   calendar_pk = v_calendar_pk;
		commit;
	END;

	WHEN OTHERS THEN
	BEGIN
		v_file := utl_file.fopen( v_email_error_dir, v_number||'_OTHER.txt', 'a');
		utl_file.put_line(v_file, 'UTL_FILE EXCEPTION! calendar_pk: '||v_calendar_pk);
		utl_file.fclose(v_file);
	END;

END;
END write_calendar_files;


