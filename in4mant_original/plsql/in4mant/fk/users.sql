set define off
PROMPT *** package: users ***

CREATE OR REPLACE PACKAGE users IS

        PROCEDURE startup;
        PROCEDURE register_form
                (
                        p_org_reg       in number               default NULL
                );
        PROCEDURE register_action
                (
                        p_login_name    	in usr.login_name%type          default NULL,
                        p_password      	in usr.password%type            default NULL,
                        p_password_2    	in usr.password%type            default NULL,
                        p_url           	in varchar2                     default NULL,
                        p_email         	in user_details.email%type      default NULL,
                        p_org_reg       	in number                       default NULL,
	                p_accept_licence	in varchar2			default NULL
                );
        PROCEDURE show_details;
        PROCEDURE details;
        PROCEDURE update_details(
                        p_user_pk                       in usr.user_pk%type                             default NULL,
                        p_login_name                    in usr.login_name%type                          default NULL,
                        p_email                         in user_details.email%type                      default NULL,
                        p_first_name                    in user_details.first_name%type                 default NULL,
                        p_last_name                     in user_details.last_name%type                  default NULL,
                        p_password                      in usr.password%type                            default NULL,
                        p_password2                     in usr.password%type                            default NULL,
                        p_old_password                  in usr.password%type                            default NULL,
                        p_geography_pk                  in user_details.geography_location_fk%type      default NULL,
                        p_geo_name                      in varchar2                                     default NULL,
                        p_sex                           in user_details.sex%type                        default NULL,
                        p_birth_date                    in varchar2                                     default NULL
                );
        PROCEDURE end_subscription
                (
                        p_confirm       in varchar2             default NULL
                );
        PROCEDURE licence_agreement
                (
                        p_type          in varchar2     default NULL
                );

END;
/
CREATE OR REPLACE PACKAGE BODY users IS

-- **************************************************
-- "PUBLIC METODER" (KALLES DIREKTE FRA WEB)
-- **************************************************


---------------------------------------------------------
-- Name:        startup
-- Type:        procedure
-- What:        start prosedyren, for � kj�re pakken
-- Author:      Frode Klevstul
-- Start date:  09.06.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter] [description]
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

        register_form;

END startup;



---------------------------------------------------------
-- Name:        register_form
-- Type:        procedure
-- What:        writes out default form to register a user
-- Author:      Frode Klevstul
-- Start date:  09.06.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter] [description]
---------------------------------------------------------
PROCEDURE register_form
        (
                p_org_reg       in number               default NULL
        )
IS
BEGIN
DECLARE

	v_licence	long	default NULL;

BEGIN
        html.b_page(NULL, NULL, NULL, NULL, 1);
        html.empty_menu;
        if (p_org_reg IS NULL) then
                html.b_box( get.txt('reg_user'), '100%', 'users.register_form');
        else
                html.b_box( get.txt('STEP1_reg_organization_user'), '100%', 'users.register_form(org)');
        end if;

htp.p('
<SCRIPT language=''javaScript''>

function submitForm(){
	if (document.form.p_accept_licence.checked == true){
		document.form.submit()
	}
	else{
		alert('''||get.txt('you_need_to_accept_license')||''')
	}
}

</SCRIPT>
');

        html.b_form('users.register_action');
        html.b_table;


        if (p_org_reg IS NULL) then
		-- fjerner HTML tagger pga. utskriving i textarea
		v_licence := 	get.txt('licence_agreement_user_1') ||' '||
				get.txt('licence_agreement_user_2') ||' '||
				get.txt('licence_agreement_user_3');
		owa_pattern.change(v_licence, '<.{1,3}>', NULL, 'g');

                htp.p('
                        <tr><td colspan="3">'|| get.txt('register_user_desc') ||'</td></tr>
                        <tr><td colspan="3">&nbsp;</td></tr>
                        <tr><td valign="top">'|| get.txt('licence_agreement') ||':</td><td><textarea wrap="virtual" cols="55" rows="10" style="width: 600;">'|| v_licence ||'</textarea></td><td>'||
                        html.popup( get.txt('print_page'), 'users.licence_agreement?p_type=user','620', '500', '1', '1' )
                        ||'</td></tr>
                        <tr><td>&nbsp;</td><td colspan="2"><input type="checkbox" name="p_accept_licence" value=""> &nbsp;'|| get.txt('read_and_agree_license') ||':</td></tr>
                        <tr><td colspan="3">&nbsp;</td></tr>
		');
		html.e_table;

		html.b_table;
		htp.p('
                        <tr><td>'|| get.txt('login_name') ||':</td><td><input type="text" name="p_login_name" size="30" maxlength="30"></td><td>'|| get.txt('login_name_desc') ||'</td></tr>
                        <tr><td>'|| get.txt('email') ||':</td><td><input type="text" name="p_email" size="30" maxlength="150"></td><td>'|| get.txt('email_desc') ||'</td></tr>
                        <tr><td>'|| get.txt('password') ||':</td><td><input type="password" name="p_password" size="30" maxlength="30"></td><td>'|| get.txt('password_desc') ||'</td></tr>
                        <tr><td>'|| get.txt('repeat_password') ||':</td><td><input type="password" name="p_password_2" size="30" maxlength="30"></td><td>&nbsp;</td></tr>
                        <tr><td colspan="3" align="right">');
                        htp.p('<input type="button" value="'||get.txt('register')||'" onClick="javaScript:submitForm()">');
                        htp.p('</td></tr>
                ');
        else
		-- fjerner HTML tagger pga. utskriving i textarea
		v_licence := 	get.txt('licence_agreement_org_1') ||' '||
				get.txt('licence_agreement_org_2') ||' '||
				get.txt('licence_agreement_org_3');
		owa_pattern.change(v_licence, '<.{1,3}>', NULL, 'g');

                htp.p('
                        <input type="hidden" name="p_org_reg" value="'||p_org_reg||'">
                        <tr><td colspan="3">'|| get.txt('register_organization_step1_desc') ||'</td></tr>
                        <tr><td colspan="3">&nbsp;</td></tr>
                        <tr><td valign="top">'|| get.txt('licence_agreement') ||':</td><td><textarea wrap="virtual" cols="55" rows="10" style="width: 600;">'|| v_licence ||'</textarea></td><td>'||
                        html.popup( get.txt('print_page'), 'users.licence_agreement?p_type=org','620', '500', '1', '1' )
                        ||'</td></tr>
                        <tr><td>&nbsp;</td><td colspan="2"><input type="checkbox" name="p_accept_licence" value="yes"> &nbsp;'|| get.txt('read_and_agree_license') ||':</td></tr>
                        <tr><td colspan="3">&nbsp;</td></tr>
		');
		html.e_table;

		html.b_table;
		htp.p('
                        <tr><td>'|| get.txt('login_name_for_org') ||':</td><td><input type="text" name="p_login_name" size="30" maxlength="30"></td><td>'|| get.txt('login_name_desc') ||'</td></tr>
                        <tr><td>'|| get.txt('email_to_org_administrator') ||':</td><td><input type="text" name="p_email" size="30" maxlength="150"></td><td>'|| get.txt('email_desc') ||'</td></tr>
                        <tr><td>'|| get.txt('password') ||':</td><td><input type="password" name="p_password" size="30" maxlength="30"></td><td>'|| get.txt('password_desc') ||'</td></tr>
                        <tr><td>'|| get.txt('repeat_password') ||':</td><td><input type="password" name="p_password_2" size="30" maxlength="30"></td><td>&nbsp;</td></tr>
                        <tr><td colspan="3" align="right">');
                        htp.p('<input type="button" value="'||get.txt('register')||'" onClick="javaScript:submitForm()">');
                        htp.p('</td></tr>
                ');
        end if;

        html.e_table;
        html.e_form;
        html.e_box;

        htp.p('
                <script>
                document.form.p_login_name.focus();
                </script>
        ');

        html.e_page;

END;
END register_form;

---------------------------------------------------------
-- Name:        register_action
-- Type:        procedure
-- What:        checks info from register_form, and inserts
-- Author:      Frode Klevstul
-- Start date:  10.06.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter] [description]
---------------------------------------------------------
PROCEDURE register_action
        (
                p_login_name    	in usr.login_name%type          default NULL,
                p_password      	in usr.password%type            default NULL,
                p_password_2    	in usr.password%type            default NULL,
                p_url           	in varchar2                     default NULL,
                p_email         	in user_details.email%type      default NULL,
                p_org_reg       	in number                       default NULL,
                p_accept_licence	in varchar2			default NULL
        )
IS
BEGIN
DECLARE
        v_check                 number                          default NULL;
        v_login_name_check      number                          default NULL;
        v_password_check        number                          default NULL;
        v_email_check           number                          default NULL;
        v_user_pk               usr.user_pk%type                default NULL;
        v_user_stat_pk          user_stat.user_stat_pk%type     default NULL;
        v_url                   varchar2(100)                   default NULL;

        v_login_name_txt        varchar2(50)                    default NULL;
        v_email_txt             varchar2(50)                    default NULL;
        v_submit_txt            varchar2(50)                    default NULL;
        v_user_type_fk          user_type.user_type_pk%type     default NULL;

        v_file                  utl_file.file_type              default NULL;
        v_number                number                          default NULL;
        v_message               varchar2(4000)                  default NULL;
        v_email_dir             varchar2(100)                   default NULL;

BEGIN

        SELECT  count(*) INTO v_check
        FROM    usr
        WHERE   login_name = p_login_name;

        if ( v_check > 0 ) then
                v_login_name_check := 1;
        end if;

        if ( p_password <> p_password_2 ) then
                v_password_check := 1;
        end if;

        if ( p_login_name IS NULL ) then
                v_login_name_check := 1;
        end if;

        if not( ok.email(p_email) ) then
                v_email_check := 1;
        end if;

        if ( p_password IS NULL ) then
                v_password_check := 1;
        end if;

        if ( p_password_2 IS NULL ) then
                v_password_check := 1;
        end if;


        if ( v_login_name_check IS NULL AND v_password_check IS NULL AND v_email_check IS NULL) then -- alt i orden, legg inn i db

                        SELECT usr_seq.NEXTVAL
                        INTO v_user_pk
                        FROM dual;

                        -- user_type_pk = -1:   ordinary user
                        -- user_status_pk = -1: never logged in after registration
                        if (p_org_reg IS NULL) then
                                v_user_type_fk := -1;
                        else
                                v_user_type_fk := -2;
                        end if;

                        INSERT INTO usr (user_pk, login_name, password, user_type_fk, user_status_fk)
                        VALUES (v_user_pk, p_login_name, p_password, v_user_type_fk, -1);
                        commit;

                        INSERT INTO user_details (user_fk, email, register_date)
                        VALUES (v_user_pk, p_email, sysdate);
                        commit;

                        SELECT user_stat_seq.NEXTVAL
                        INTO v_user_stat_pk
                        FROM dual;

                        INSERT INTO user_stat (user_stat_pk, user_fk,
                        ip_address, host_name, login_time)
                        VALUES (v_user_stat_pk, v_user_pk,
                        owa_util.get_cgi_env('REMOTE_ADDR'),
                        owa_util.get_cgi_env('REMOTE_HOST'), sysdate );
                        commit;

                        if (p_org_reg IS NULL) then
                                v_url                   := p_url;

				if (v_url IS NULL) then
					v_url := 'users.details';
				end if;

                        else
                                v_url := 'admin.edit_org_info';
                        end if;


                        -- --------------------
                        -- write welcome email
                        -- --------------------
                        v_message := get.txt('email_welcome_message');
                        v_email_dir     := get.value('email_dir');
                        v_message := REPLACE (v_message, '[name]' , p_login_name);
                        v_message := REPLACE (v_message, '[username]' , p_login_name);
                        v_message := REPLACE (v_message, '[password]' , p_password);

                        SELECT  email_script_seq.NEXTVAL
                        INTO    v_number
                        FROM    DUAL;

                        v_file := utl_file.fopen( v_email_dir, v_number||'_message.txt' ,'w');
                        utl_file.put_line(v_file, v_message );
                        utl_file.fclose(v_file);

                        v_file := utl_file.fopen( v_email_dir, v_number||'_email-list.txt', 'w');
                        utl_file.put_line(v_file, p_email);
                        utl_file.fclose(v_file);
                        -- ------------------
                        -- end writing email
                        -- ------------------

                        -- logger inn brukeren
                        login.login_action(p_login_name, p_password, v_url);

        else
	        html.b_page(NULL, NULL, NULL, NULL, 1);

                if (p_org_reg IS NULL) then
                        html.b_box( get.txt('reg_user'), '100%', 'users.register_form');
                        v_login_name_txt        := get.txt('login_name');
                        v_email_txt             := get.txt('email');
                        v_submit_txt            := get.txt('register');
                else
                        html.b_box( get.txt('STEP1_reg_organization_user'), '100%', 'users.register_form(org)');
                        v_login_name_txt        := get.txt('login_name_for_org');
                        v_email_txt             := get.txt('email_to_org_administrator');
                        v_submit_txt            := get.txt('continue');
                end if;

                html.b_form('users.register_action');
                html.b_table;

                htp.p('<input type="hidden" name="p_url" value="'||v_url||'">');
                htp.p('<input type="hidden" name="p_org_reg" value="'||p_org_reg||'">');
                htp.p('<tr><td colspan="3">'|| get.txt('try_reg_again') ||'</td></tr>');
                htp.p('<tr><td colspan="3">&nbsp;</td></tr>');

                if ( v_login_name_check IS NULL ) then
                        htp.p('<tr><td>'|| v_login_name_txt ||':</td><td><input type="text" name="p_login_name" size="30" maxlength="30" value="'|| p_login_name ||'"></td><td>&nbsp;</td></tr>');
                else
                        htp.p('<tr bgcolor="ffff00"><td>'|| v_login_name_txt ||':</td><td><input type="text" name="p_login_name" size="30" maxlength="30"></td><td>'|| get.txt('login_name_taken') ||'</td></tr>');
                end if;
                if ( v_email_check IS NULL ) then
                        htp.p('<tr><td>'|| v_email_txt ||':</td><td><input type="text" name="p_email" size="30" maxlength="150" value="'|| p_email ||'"></td><td>&nbsp;</td></tr>');
                else
                        htp.p('<tr bgcolor="ffff00"><td>'|| v_email_txt ||':</td><td><input type="text" name="p_email" size="30" maxlength="150"></td><td>'|| get.txt('email_empty') ||'</td></tr>');
                end if;
                if ( v_password_check IS NULL ) then
                        htp.p('<tr><td>'|| get.txt('password') ||':</td><td><input type="password" name="p_password" size="30" maxlength="30" value="'|| p_password ||'"></td><td>&nbsp;</td></tr>');
                        htp.p('<tr><td>'|| get.txt('repeat_password') ||':</td><td><input type="password" name="p_password_2" size="30" maxlength="30" value="'|| p_password_2 ||'"></td><td>&nbsp;</td></tr>');
                else
                        htp.p('<tr bgcolor="ffff00"><td>'|| get.txt('password') ||':</td><td><input type="password" name="p_password" size="30" maxlength="30"></td><td>'|| get.txt('password_help_1') ||'</td></tr>');
                        htp.p('<tr bgcolor="ffff00"><td>'|| get.txt('repeat_password') ||':</td><td><input type="password" name="p_password_2" size="30" maxlength="30"></td><td>'|| get.txt('password_help_2') ||'</td></tr>');
                end if;
                htp.p('<tr><td colspan="3" align="right">'); html.submit_link( v_submit_txt ); htp.p('</td></tr>');

                html.e_table;
                html.e_form;
                html.e_box;
                html.e_page;
        end if;

END;
END register_action;




---------------------------------------------------------
-- Name:        show_details
-- Type:        procedure
-- What:        lists out user details
-- Author:      Frode Klevstul
-- Start date:  16.06.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter] [description]
---------------------------------------------------------
PROCEDURE show_details
IS
BEGIN
DECLARE
        v_user_pk                       usr.user_pk%type                                default NULL;
        v_login_name                    usr.login_name%type                             default NULL;
        v_email                         user_details.email%type                         default NULL;
        v_first_name                    user_details.first_name%type                    default NULL;
        v_last_name                     user_details.last_name%type                     default NULL;
        v_geography_location_fk         user_details.geography_location_fk%type         default NULL;
        v_user_type_pk                  user_type.user_type_pk%type                     default NULL;
        v_sex                           user_details.sex%type                           default NULL;
        v_birth_date                    user_details.birth_date%type                    default NULL;

BEGIN
if (login.timeout('users.details') > 0 ) then

   -- Legger inn i statistikk tabellen
   stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,28);

        v_user_pk       := get.uid;
        v_user_type_pk  := get.u_type(v_user_pk);

        SELECT  login_name, email, first_name, last_name, geography_location_fk, sex, birth_date
        INTO    v_login_name, v_email, v_first_name, v_last_name, v_geography_location_fk, v_sex, v_birth_date
        FROM    usr, user_details
        WHERE   user_pk = user_fk
        AND     user_pk = v_user_pk;

        if ( v_user_type_pk = -2 ) then -- if client user
                html.b_page(NULL, NULL, NULL, 2);
        else
                html.b_page(NULL, NULL, NULL, NULL, 2);
                html.my_menu;
        end if;

        html.b_box( get.txt('user_info'), '100%', 'users.show_details');
        html.b_form('users.details');
        html.b_table;
        htp.p('
                <tr><td colspan="2">'|| get.txt('user_information_desc') ||'</td></td>
		<tr><td colspan="2"><HR size="2" noshade="noshade"></td></tr>
                <tr><td colspan="2">&nbsp;</td></td>
                <tr><td>'|| get.txt('login_name') ||':</td><td>'||v_login_name||'</td></tr>
                <tr><td>'|| get.txt('email') ||':</td><td>'||v_email||'</td></tr>
                <tr><td>'|| get.txt('first_name') ||':</td><td>'||v_first_name||'</td></tr>
                <tr><td>'|| get.txt('last_name') ||':</td><td>'||v_last_name||'</td></tr>
                <tr><td>'|| get.txt('geography_location') ||':</td><td>'|| get.uln ||'</td></tr>
        ');

        if ( v_user_type_pk <> -2 ) then -- ordinary user
                htp.p('<tr><td>'|| get.txt('sex')||':</td><td>');

                if (v_sex IS NULL) then
                        htp.p('-');
                else
                        htp.p(get.txt(v_sex));
                end if;

                htp.p('</td></tr><tr><td>'|| get.txt('birth_date')||':</td><td>');

                if (v_birth_date IS NULL) then
                        htp.p('-');
                else
                        htp.p( to_char(v_birth_date, 'YYYY') );
                end if;

                htp.p('</td></tr>');

        end if;

        htp.p('<tr><td colspan="2" align="right">'); html.submit_link( get.txt('update') ); htp.p('</td></tr>');

        html.e_table;
        html.e_form;
        html.e_box;

	if ( get.u_type = -1 ) then
	        html.b_box( get.txt('end_subscription'), '100%', 'users.end_subscription');
	        html.b_form('users.end_subscription');
	        html.b_table;
	        htp.p('
	                <tr><td>'|| get.txt('end_subscription_desc') ||'</td></tr>
	                <tr><td align="right">'); html.submit_link( get.txt('end_my_subscription') ); htp.p('</td></tr>
	        ');
	        html.e_table;
	        html.e_form;
	        html.e_box;
	end if;

        html.e_page;



end if;
END;
END show_details;





---------------------------------------------------------
-- Name:        details
-- Type:        procedure
-- What:        lists out user details
-- Author:      Frode Klevstul
-- Start date:  13.06.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter] [description]
---------------------------------------------------------
PROCEDURE details
IS
BEGIN
DECLARE
        v_user_pk                       usr.user_pk%type                                default NULL;
        v_login_name                    usr.login_name%type                             default NULL;
        v_email                         user_details.email%type                         default NULL;
        v_first_name                    user_details.first_name%type                    default NULL;
        v_last_name                     user_details.last_name%type                     default NULL;
        v_geography_pk                  user_details.geography_location_fk%type         default NULL;
        v_sex                           user_details.sex%type                           default NULL;
        v_user_type_pk                  user_type.user_type_pk%type                     default NULL;
        v_birth_date                    user_details.birth_date%type                    default NULL;

BEGIN
if (login.timeout('users.details') > 0 ) then
   -- Legger inn i statistikk tabellen
   stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,28);

        v_user_pk       := get.uid;
        v_user_type_pk  := get.u_type(v_user_pk);

        SELECT  login_name, email, first_name, last_name, sex, birth_date, geography_location_fk
        INTO    v_login_name, v_email, v_first_name, v_last_name, v_sex, v_birth_date, v_geography_pk
        FROM    usr, user_details
        WHERE   user_pk = user_fk
        AND     user_pk = v_user_pk;


        if ( v_user_type_pk = -2 ) then -- if client user
                html.b_page(NULL, NULL, NULL, 2, 2);
        else
                html.b_page(NULL, NULL, NULL, NULL, 3);
                html.my_menu;
        end if;

        html.b_box( get.txt('update_user_info'), '100%', 'users.details' );
        html.b_form('users.update_details');
        html.b_table;
        htp.p('
                <input type="hidden" name="p_user_pk" value="'||v_user_pk||'">
                <tr><td colspan="2">'|| get.txt('update_details_desc') ||'</td></tr>
                <tr><td>'|| get.txt('login_name') ||':</td><td><input type="text" name="p_login_name" size="30" maxlength="30" value="'||v_login_name||'"></td></tr>
                <tr><td>'|| get.txt('email') ||':</td><td><input type="text" name="p_email" size="30" maxlength="30" value="'||v_email||'"></td></tr>
                <tr><td>'|| get.txt('first_name') ||':</td><td><input type="text" name="p_first_name" size="30" maxlength="30" value="'||v_first_name||'"></td></tr>
                <tr><td>'|| get.txt('last_name') ||':</td><td><input type="text" name="p_last_name" size="30" maxlength="30" value="'||v_last_name||'"></td></tr>
                <tr>
                    <td>'|| get.txt('geography_location') ||':</td>
                    <td><input type="hidden" name="p_geography_pk" value="'|| v_geography_pk ||'">
                    <input type="text" size="30" name="p_geo_name" value="'|| get.uln ||'&nbsp;" onSelect=javascript:openWin(''select_geo.select_location?p_no_levels=6'',300,200) onClick=javascript:openWin(''select_geo.select_location?p_no_levels=6'',300,200)>
                    '|| html.popup( get.txt('change_geo'), 'select_geo.select_location?p_no_levels=6', '300', '200') ||'
                    </td>
                </tr>
        ');

        if ( v_user_type_pk <> -2 ) then -- ordinary user
                htp.p('<tr><td>'|| get.txt('sex')||':</td><td>');

                htp.p('<select name="p_sex">');

                if (v_sex = 'f') then
                        htp.p('<option value="m">'||get.txt('m')||'</option>');
                        htp.p('<option value="f" selected>'||get.txt('f')||'</option>');
                elsif (v_sex = 'm') then
                        htp.p('<option value="m" selected>'||get.txt('m')||'</option>');
                        htp.p('<option value="f">'||get.txt('f')||'</option>');
                else
                        htp.p('<option value="" selected>&lt;'||get.txt('choose_sex')||'&gt;</option>');
                        htp.p('<option value="m">'||get.txt('m')||'</option>');
                        htp.p('<option value="f">'||get.txt('f')||'</option>');
                end if;

                htp.p('</select>');
                htp.p('</td></tr><td>'|| get.txt('birth_date')||' (''YYYY''):</td><td>');
                htp.p('<input type="text" name="p_birth_date" value="'||to_char(v_birth_date, 'YYYY')||'" size="4" maxlength="4">');
                htp.p('</td></tr>');
        end if;

        htp.p('
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr><td colspan="2">'|| get.txt('change_password_desc')||'</td></tr>
                <tr><td>'|| get.txt('new_password') ||':</td><td><input type="password" name="p_password" size="30" maxlength="30"></td></tr>
                <tr><td>'|| get.txt('repeat_password') ||':</td><td><input type="password" name="p_password2" size="30" maxlength="30"></td></tr>
                <tr><td>'|| get.txt('old_password') ||':</td><td><input type="password" name="p_old_password" size="30" maxlength="30"></td></tr>
                <tr><td colspan="2" colspan="2" align="right">'); html.submit_link( get.txt('save') ); htp.p('</td></tr>
        ');


        html.e_table;
        html.e_form;
        html.e_box;

        html.e_page;

end if;
END;
END details;




---------------------------------------------------------
-- Name:        update_details
-- Type:        procedure
-- What:        update user_info
-- Author:      Frode Klevstul
-- Start date:  15.06.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter] [description]
---------------------------------------------------------
PROCEDURE update_details(
                p_user_pk                       in usr.user_pk%type                             default NULL,
                p_login_name                    in usr.login_name%type                          default NULL,
                p_email                         in user_details.email%type                      default NULL,
                p_first_name                    in user_details.first_name%type                 default NULL,
                p_last_name                     in user_details.last_name%type                  default NULL,
                p_password                      in usr.password%type                            default NULL,
                p_password2                     in usr.password%type                            default NULL,
                p_old_password                  in usr.password%type                            default NULL,
                p_geography_pk                  in user_details.geography_location_fk%type      default NULL,
                p_geo_name                      in varchar2                                     default NULL,
                p_sex                           in user_details.sex%type                        default NULL,
                p_birth_date                    in varchar2                                     default NULL
        )
IS
BEGIN
DECLARE
        v_old_password                  usr.password%type                               default NULL;
        v_geography_location_fk         user_details.geography_location_fk%type         default NULL;
        v_check                         number                                          default NULL;
        v_birth_date                    user_details.birth_date%type                    default NULL;

	v_file			utl_file.file_type			default NULL;
	v_log_dir		varchar2(100)				default NULL;
	v_date_format		varchar2(30)				default NULL;
	v_write_log_file	boolean					default FALSE;

BEGIN
if (login.timeout('users.details') > 0 ) then

   -- Legger inn i statistikk tabellen
   stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,28);


        if ( p_birth_date IS NOT NULL ) then
                if ( owa_pattern.match(p_birth_date, '\d\d\d\d') ) then
                        v_birth_date    := to_date (p_birth_date, 'YYYY');
                end if;
        end if;

	if not ( ok.email(p_email) ) then
		get.error_page(3, get.txt('email_not_correct'));
		return;
	end if;

        SELECT  count(*) INTO v_check
        FROM    usr
        WHERE   login_name = p_login_name
        AND     user_pk <> p_user_pk;

        if (v_check = 0) then

                SELECT  password INTO v_old_password
                FROM    usr
                WHERE   user_pk = p_user_pk;

                if ( p_password IS NULL and p_password2 IS NULL) then

                        UPDATE  user_details
                        SET     email = p_email,
                                first_name = p_first_name,
                                last_name = p_last_name,
                                geography_location_fk = p_geography_pk,
                                sex = p_sex,
                                birth_date = v_birth_date
                        WHERE   user_fk = p_user_pk;
                        commit;

                        UPDATE  usr
                        SET     login_name = p_login_name
                        WHERE   user_pk = p_user_pk;
                        commit;

			v_write_log_file := TRUE;

                elsif ( v_old_password = p_old_password AND p_password = p_password2 ) then

                        UPDATE  user_details
                        SET     email = p_email,
                                first_name = p_first_name,
                                last_name = p_last_name,
                                geography_location_fk = p_geography_pk,
                                sex = p_sex,
                                birth_date = v_birth_date
                        WHERE   user_fk = p_user_pk;
                        commit;

                        UPDATE  usr
                        SET     login_name = p_login_name,
                                password = p_password
                        WHERE   user_pk = p_user_pk;
                        commit;

			v_write_log_file := TRUE;

                else
			get.error_page(3, get.txt('wrong_password_data_not_updated'));
			return;
                end if;
        else
		get.error_page(3, get.txt('username_taken'));
		return;
        end if;



	-- ------------------------------
	-- Lagre endringer p� fil
	-- ------------------------------
	if (v_write_log_file) then
		v_date_format	:= get.value('log_date');
		v_log_dir	:= get.value('log_dir');

		v_file := utl_file.fopen( v_log_dir, 'user_changes.txt', 'a');
		utl_file.put_line(v_file,
			'>>> user_pk: '||p_user_pk||
			' ��� timestamp: '||to_char(sysdate, v_date_format)||
			' ��� login_name: '||p_login_name||
			' ��� email: '||p_email||
			' ��� first_name: '||p_first_name||
			' ��� last_name: '||p_last_name||
			' ��� old pwd: '||v_old_password||
			' ��� new pwd: '||p_password
		);
		utl_file.fclose(v_file);
	end if;

        show_details;


end if;
END;
END update_details;




---------------------------------------------------------
-- Name:        end_subscription
-- Type:        procedure
-- What:        to end a users subscription
-- Author:      Frode Klevstul
-- Start date:  15.06.2000
-- Desc:
---------------------------------------------------------
PROCEDURE end_subscription
        (
                p_confirm       in varchar2             default NULL
        )
IS
BEGIN
DECLARE

        v_user_pk       usr.user_pk%type                default NULL;
        v_email         user_details.email%type         default NULL;
        v_login_name    usr.login_name%type             default NULL;

BEGIN
if (login.timeout('users.details') > 0 ) then

        if (p_confirm IS NULL) then
                html.b_page(NULL, NULL, NULL, NULL, 2);
                html.b_box( get.txt('end_subscription'), '100%', 'users.end_subscription');
                html.b_form('users.end_subscription');
                html.b_table;
                htp.p('
                        <tr><td>'|| get.txt('confirm_ending_the_subscription') ||'</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr>
                                <td><input type="submit" name="p_confirm" value="'||get.txt('i_am_sure')||'"></td>
                        </tr>
                        <tr>
                                <td>'); html.back( get.txt('dont_end_my_subscription') ); htp.p('</td></tr>');
                html.e_table;
                html.e_form;
                html.e_box;
                html.e_page;
        else

                v_user_pk := get.uid;

                -- slett alle forekomster i liste tabellene tilh�rende denne brukeren
                DELETE FROM list
                WHERE list_pk IN
                        (
                                SELECT  list_fk
                                FROM    list_user lu, list l, list_type lt
                                WHERE   lu.list_fk = l.list_pk
                                AND     l.list_type_fk = lt.list_type_pk
                                AND     lt.description LIKE 'my_personal_list%'
                                AND     user_fk = v_user_pk
                        );
                commit;

                DELETE FROM list_user
                WHERE user_fk = v_user_pk;
                commit;

                -- sett email = email���ended (pga. reminder/tip, dersom denne email adressen blir brukt senere)
                -- sett username = username_ended (frigj�r gammelt brukernavn)
                -- sett brukerstatus = -52 (canceled)
                SELECT  email INTO v_email
                FROM    user_details
                WHERE   user_fk = v_user_pk;

                SELECT  login_name INTO v_login_name
                FROM    usr
                WHERE   user_pk = v_user_pk;

                UPDATE  user_details
                SET     email = v_email ||'_ended_'||to_char(sysdate,'YYYYMMDDHH24MISS')
                WHERE   user_fk = v_user_pk;

                commit;

                UPDATE  usr
                SET     login_name = v_login_name ||'_ended',
                        user_status_fk = -52
                WHERE   user_pk = v_user_pk;
                commit;

                html.jump_to('login.logout');
        end if;

end if;
END;
END end_subscription;




---------------------------------------------------------
-- Name:        licence_agreement
-- Type:        procedure
-- What:        writes out the licence agreement
-- Author:      Frode Klevstul
-- Start date:  02.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE licence_agreement
        (
                p_type          in varchar2     default NULL
        )
IS
BEGIN

        html.b_page_2(600);

        if (p_type = 'org') then
                html.b_box( get.txt('licence_agreement_org') );
        else
                html.b_box( get.txt('licence_agreement_user') );
        end if;

        html.b_table;

        if (p_type = 'org') then
                htp.p( get.txt('licence_agreement_org_1') ||' '|| get.txt('licence_agreement_org_2') ||' '|| get.txt('licence_agreement_org_3') );
        else
                htp.p( get.txt('licence_agreement_user_1') ||' '|| get.txt('licence_agreement_user_2') ||' '|| get.txt('licence_agreement_user_3') );
        end if;

        html.e_table;
        html.close(get.txt('close_window'));

        html.e_box;
        html.e_page_2;

END licence_agreement;


-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/

show errors;
