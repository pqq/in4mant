CREATE OR REPLACE PACKAGE lang IS

	PROCEDURE startup;
	PROCEDURE html_top
		(
			p_link			in number 				default NULL
                );
	PROCEDURE insert_form;
	PROCEDURE insert_action
		(
			p_language_pk		in la.language_pk%type			default NULL,
			p_language_name		in la.language_name%type		default NULL,
			p_language_code		in la.language_code%type		default NULL
		);
	PROCEDURE list_all;
	PROCEDURE update_entry
		(
			p_language_pk 		in html_code.html_code_pk%type		default NULL
                );
	PROCEDURE delete_entry
		(
			p_language_pk 		in la.language_pk%type			default NULL
                );
	PROCEDURE delete_entry_2
		(
			p_language_pk 		in la.language_pk%type			default NULL,
			p_confirm		in varchar2				default NULL
                );


END;
/
CREATE OR REPLACE PACKAGE BODY lang IS

-- **************************************************
-- "PUBLIC METODER" (KALLES DIREKTE FRA WEB)
-- **************************************************

---------------------------------------------------------
-- Name: 	startup
-- Type: 	procedure
-- What:
-- Author:  	Frode Klevstul
-- Start date: 	18.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

	list_all;

END startup;


---------------------------------------------------------
-- Name: 	insert_form
-- Type: 	procedure
-- What: 	write out the html form for inserting
-- Author:  	Frode Klevstul
-- Start date: 	18.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE insert_form
IS
BEGIN

if ( login.timeout('lang.startup')>0 AND login.right('lang')>0 ) then

	html_top(1); -- toppen av siden, med meny
	htp.p('
		<form name="htmlcode_form" action="lang.insert_action">
		<table border=0>
		<tr><td colspan=2 align="center"><b>insert into table "la"</b></td></tr>
		<tr><td>language name:</td><td><input type="Text" name="p_language_name" size="40" maxlength="50"></td></tr>
		<tr><td>language code:</td><td><input type="Text" name="p_language_code" size="5" maxlength="5"></td></tr>
		<tr><td colspan="2" align="right"><input type="Submit" value="insert"></td></tr>
		</table>
		</form>
	');
	html.write('e_page'); -- skriver ut slutten p� html siden

end if;

END insert_form;


---------------------------------------------------------
-- Name: 	list_all
-- Type: 	procedure
-- What: 	lists out all entries
-- Author:  	Frode Klevstul
-- Start date: 	18.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
-- beskrivelse av inn parametere:
--
-- [parameter]	[description]
---------------------------------------------------------
PROCEDURE list_all
IS
BEGIN
DECLARE

	CURSOR 	all_languages IS
	SELECT 	language_pk, language_name, language_code
	FROM 	la
	ORDER BY language_name;

	v_language_pk		la.language_pk%type 		default NULL;
	v_language_name		la.language_name%type 		default NULL;
	v_language_code		la.language_code%type 		default NULL;

BEGIN

if ( login.timeout('lang.startup')>0 AND login.right('lang')>0 ) then

	html_top(2);
	htp.p('
		<table border="0" width="100%"><tr><td colspan="5">&nbsp;</td></tr>
		<tr bgcolor="#eeeeee"><td width="5%">pk:</td><td width="35%">language name:</td><td width="30%">language code:</td><td align="center" width="15%">&nbsp;</td><td align="center" width="15%">&nbsp;</td></tr>
	');
	-- g�r igjennom "cursoren"...
	open all_languages;
	while (1>0)	-- alltid sant -> g�r igjennom alle forekomstene
	loop
		fetch all_languages into v_language_pk, v_language_name, v_language_code;
		exit when all_languages%NOTFOUND;
		if (v_language_pk = 0) then
			htp.p('<tr><td width="5%"><font color="#dddddd">'|| v_language_pk ||'</font></td><td width="35%"><b>'|| v_language_name ||'</b></td><td width="30%">'|| v_language_code ||'</td><td bgcolor="#000000" align="center" width="15%">&nbsp;</td><td bgcolor="#000000" align="center" width="15%">&nbsp;</td></tr>');
		else
			htp.p('<tr><td width="5%"><font color="#dddddd">'|| v_language_pk ||'</font></td><td width="35%"><b>'|| v_language_name ||'</b></td><td width="30%">'|| v_language_code ||'</td><td bgcolor="#000000" align="center" width="15%"><a href="lang.update_entry?p_language_pk='||v_language_pk||'"><font color="#ffffff">UPDATE</font></a></td><td bgcolor="#000000" align="center" width="15%"><a href="lang.delete_entry?p_language_pk='|| v_language_pk ||'"><font color="#ffffff">DELETE</font></a></td></tr>');
		end if;
	end loop;
	close all_languages;

	htp.p('</table>');
	html.write('e_page'); -- skriver ut slutten p� html siden

end if;

END;
END list_all;




-- **************************************************
-- "PRIVATE METODER" (KALLES IKKE DIREKTE FRA WEB)
-- **************************************************

---------------------------------------------------------
-- Name: 	insert_action
-- Type: 	procedure
-- What: 	inserts the entry into the database
-- Author:  	Frode Klevstul
-- Start date: 	18.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE insert_action
		(
			p_language_pk		in la.language_pk%type		default NULL,
			p_language_name		in la.language_name%type	default NULL,
			p_language_code		in la.language_code%type	default NULL
		)
IS
BEGIN
DECLARE

	v_language_pk 	la.language_pk%type 		default NULL; -- brukes til sekvens nummeret
	v_check 	number	default 0;
	v_list_type_pk	list_type.list_type_pk%type	default NULL;

BEGIN

if ( login.timeout('lang.startup')>0 AND login.right('lang')>0 ) then

	-- sjekker om det allerede finnes en forekomst med dette navnet
	SELECT count(*) INTO v_check
	FROM la
	WHERE language_name = p_language_name;

	if ( (v_check > 0 AND p_language_pk IS NULL) OR (p_language_name IS NULL) ) then
		htp.p('The name <b>'|| p_language_name||'</b> alredy exists, choose another name.');
		html.write('e_page'); -- skriver ut slutten p� html siden
	elsif (p_language_pk IS NULL) then
	begin
		SELECT la_seq.NEXTVAL
		INTO v_language_pk
		FROM dual;

		INSERT INTO la
		(language_pk, language_name, language_code)
		VALUES (v_language_pk, p_language_name, p_language_code);

		INSERT INTO service_on_lang
		(service_fk, language_fk)
		VALUES (0, v_language_pk);

	end;
	else
	begin
		UPDATE la
		SET 	language_name = p_language_name,
			language_code = p_language_code
		WHERE language_pk = p_language_pk;
	end;
	end if;

	list_all;

end if;


END;
END insert_action;


---------------------------------------------------------
-- Name: 	html_top
-- Type: 	procedure
-- What: 	skriver ut html_koden p� toppen (+ linker)
-- Author:  	Frode Klevstul
-- Start date: 	15.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
-- beskrivelse av inn parametere:
--
-- [parameter]	[description]
-- p_link	gir oss nr p� linken vi er p� n�
--		1 = insert_html
--		2 = edit_html
---------------------------------------------------------
PROCEDURE html_top
		(
			p_link		in number default NULL
                )
IS
BEGIN

	html.b_adm_page; 	-- skriver ut starten p� html siden
	htp.p('<table width="100%"><tr><td align="center"><b>language</b></td></tr></table>');

	-- utskriving av linkene/menyen p� toppen av siden:
	if (p_link = '1') then
		htp.p('
		<table border="0" width="100%">
		<tr bgcolor="#cccccc"><td bgcolor="#aaaaaa" align="center" width="50%"><a href="lang.insert_form"><font color="#ffffff">insert</font></a></td><td align="center"><a href="lang.list_all">update/delete</a></td></tr>
		</table>
		');
	elsif (p_link = '2') then
	begin
		htp.p('
		<table border="0" width="100%">
		<tr bgcolor="#cccccc"><td align="center" width="50%"><a href="lang.insert_form">insert</a></font></td><td align="center" bgcolor="#aaaaaa"><a href="lang.list_all"><font color="#ffffff">update/delete</font></a></td></tr>
		</table>
		');
	end;
	end if;

END html_top;



---------------------------------------------------------
-- Name: 	update_entry
-- Type: 	procedure
-- What: 	updates an entry
-- Author:  	Frode Klevstul
-- Start date: 	18.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE update_entry
		(
			p_language_pk 		in html_code.html_code_pk%type	default NULL
                )
IS
BEGIN
DECLARE

	v_language_name		la.language_name%type	default NULL;
	v_language_code		la.language_code%type	default NULL;

BEGIN

if ( login.timeout('lang.startup')>0 AND login.right('lang')>0 ) then

	SELECT language_name, language_code INTO v_language_name, v_language_code
	FROM la
	WHERE language_pk = p_language_pk;

	html_top(2);
	htp.p('
		<form name="update_form" action="lang.insert_action" method="post">
		<input type="hidden" name="p_language_pk" value="'|| p_language_pk ||'">
		<table border=0>
		<tr><td colspan=2 align="center"><b>update an entry in "html_code":</b></td></tr>
		<tr><td>name:</td><td><input type="Text" name="p_language_name" size="40" maxlength="50" value="'|| v_language_name ||'"></td></tr>
		<tr><td>language code:</td><td><input type="Text" name="p_language_code" size="5" maxlength="5" value="'|| v_language_code ||'"></td></tr>
		<tr><td colspan="2"><input type="Submit" value="update"></td></tr>
		</table>
		</form>
	');
	html.write('e_page'); -- skriver ut slutten p� html siden

end if;

END;
END update_entry;


---------------------------------------------------------
-- Name: 	delete_entry
-- Type: 	procedure
-- What: 	ask the user to confirm the delete
-- Author:  	Frode Klevstul
-- Start date: 	18.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE delete_entry
		(
			p_language_pk 		in la.language_pk%type	default NULL
                )
IS
BEGIN
DECLARE

	v_language_name		la.language_name%type	default NULL;

BEGIN

if ( login.timeout('lang.startup')>0 AND login.right('lang')>0 ) then

	SELECT language_name INTO v_language_name
	FROM la
	WHERE language_pk = p_language_pk;


	html_top(2);
	htp.p('
		<form action="lang.delete_entry_2" method="post">
		<input type="hidden" name="p_language_pk" value="'|| p_language_pk ||'">
		<table>
		<tr><td colspan="2">Are you sure you want to delete the entry <b>'|| v_language_name ||'</b> with pk <b>'||p_language_pk||'</b>?</td></tr>
		<tr><td><input type="submit" name="p_confirm" value="yes"></td><td align="right"><input type="submit" name="p_confirm" value="no"></td></tr>
		</table>
		</form>
	');
	html.write('e_page'); -- skriver ut slutten p� html siden

end if;

END;
END delete_entry;



---------------------------------------------------------
-- Name: 	delete_entry_2
-- Type: 	procedure
-- What: 	removes the entry from the database
-- Author:  	Frode Klevstul
-- Start date: 	18.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE delete_entry_2
		(
			p_language_pk 		in la.language_pk%type		default NULL,
			p_confirm		in varchar2			default NULL
                )
IS
BEGIN

if ( login.timeout('lang.startup')>0 AND login.right('lang')>0 ) then

	if (p_confirm = 'yes') then
		DELETE FROM la
		WHERE language_pk = p_language_pk;
	end if;
	list_all;

end if;

END delete_entry_2;







-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/
