set define off
PROMPT *** package: param ***

CREATE OR REPLACE PACKAGE param IS

	PROCEDURE startup;
	PROCEDURE insert_form;
	PROCEDURE insert_action
   		(
			p_parameter_pk		in parameter.parameter_pk%type 	default NULL,
			p_name 			in parameter.name%type 		default NULL,
	                p_value  		in parameter.value%type 	default NULL,
	                p_description  		in parameter.description%type 	default NULL
                );
	PROCEDURE html_top
   		(
			p_link			in number default NULL
                );
	PROCEDURE list_all;
	PROCEDURE delete_entry
		(
			p_parameter_pk 		in parameter.parameter_pk%type	default NULL
		);
	PROCEDURE delete_entry_2
		(
			p_parameter_pk 		in parameter.parameter_pk%type	default NULL,
			p_confirm		in varchar2			default NULL
                );
	PROCEDURE edit_entry
		(
			p_parameter_pk 		in parameter.parameter_pk%type	default NULL
		);


END;
/
CREATE OR REPLACE PACKAGE BODY param IS

-- **************************************************
-- "PUBLIC METODER" (KALLES DIREKTE FRA WEB)
-- **************************************************

---------------------------------------------------------
-- Name: 	startup
-- Type: 	procedure
-- What: 	kaller 'insert_html'
-- Author:  	Frode Klevstul
-- Start date: 	17.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

	list_all;

END startup;



---------------------------------------------------------
-- Name: 	insert_form
-- Type: 	procedure
-- What:
-- Author:  	Frode Klevstul
-- Start date: 	15.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE insert_form
IS
BEGIN

if ( login.timeout('param.startup')>0 AND login.right('param')>0 ) then

	html_top(1);	-- skriver ut html koden p� toppen
	htp.p('
		<form name="insert_form" action="param.insert_action" method="post">
		<table border="0">
		<tr><td colspan="2" align="center"><b>insert into table "parameter"</b></td></tr>
		<tr><td>name:</td><td><input type="Text" name="p_name" size="10" maxlength="100"></td></tr>
		<tr><td>value:</td><td><input type="Text" name="p_value" size="40" maxlength="200"></td></tr>
		<tr><td>description:</td><td><input type="Text" name="p_description" size="70" maxlength="150"></td></tr>
		<tr><td colspan="2"><input type="Submit" value="insert"></td></tr>
		</table>
		</form>
	');
	html.e_page;

end if;

END insert_form;



---------------------------------------------------------
-- Name: 	list_all
-- Type: 	procedure
-- What: 	lister ut alle forekomster
-- Author:  	Frode Klevstul
-- Start date: 	15.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
-- beskrivelse av inn parametere:
--
-- [parameter]	[description]
---------------------------------------------------------
PROCEDURE list_all
IS
BEGIN
DECLARE

	CURSOR 	select_all IS
	SELECT 	parameter_pk, name, value, description
	FROM 	parameter
	ORDER BY name;

	v_parameter_pk	parameter.parameter_pk%type 	default NULL;
	v_name		parameter.name%type 		default NULL;
	v_value		parameter.value%type 		default NULL;
	v_description	parameter.description%type 	default NULL;
BEGIN

if ( login.timeout('param.startup')>0 AND login.right('param')>0 ) then

	html_top(2);
	htp.p('
		<table border="0" width="100%"><tr><td colspan="6">&nbsp;</td></tr>
		<tr bgcolor="#eeeeee"><td width="5%">pk:</td><td>name:</td><td>value:</td><td>description:</i></td><td align="center">&nbsp;</td><td align="center">&nbsp;</td></tr>
	');
	-- g�r igjennom "cursoren"...
	open select_all;
	while (1>0)	-- alltid sant -> g�r igjennom alle forekomstene
	loop
		fetch select_all into v_parameter_pk, v_name, v_value, v_description;
		exit when select_all%NOTFOUND;
		v_description := html.rem_tag(v_description);
		v_value := html.rem_tag(v_value);
		htp.p('<tr><td width="5%"><font color="#dddddd">'|| v_parameter_pk ||'</font></td><td><b>'|| v_name ||'</b></td><td>'|| v_value ||'</td><td><i>'|| v_description ||'</i></td><td bgcolor="#000000" align="center"><a href="param.edit_entry?p_parameter_pk='||v_parameter_pk||'"><font color="#ffffff">UPDATE</font></a></td><td bgcolor="#000000" align="center"><a href="param.delete_entry?p_parameter_pk='|| v_parameter_pk ||'"><font color="#ffffff">DELETE</font></a></td></tr>');
	end loop;
	close select_all;

	htp.p('</table>');
	html.e_page;

end if;

END;
END list_all;



-- **************************************************
-- "PRIVATE METODER" (KALLES IKKE DIREKTE FRA WEB)
-- **************************************************

---------------------------------------------------------
-- Name: 	insert_action
-- Type: 	procedure
-- What: 	inserts the entry into the database
-- Author:  	Frode Klevstul
-- Start date: 	15.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE insert_action
		(
			p_parameter_pk		in parameter.parameter_pk%type 	default NULL,
			p_name 			in parameter.name%type 		default NULL,
	                p_value  		in parameter.value%type 	default NULL,
	                p_description  		in parameter.description%type 	default NULL
                )
IS
BEGIN
DECLARE

	v_parameter_pk 	parameter.parameter_pk%type 	default NULL;	-- henter ut pk av sekvens
	v_check 	number	default 0;

BEGIN


if ( login.timeout('param.startup')>0 AND login.right('param')>0 ) then

	-- sjekker om det allerede finnes en forekomst med dette navnet
	SELECT count(*) INTO v_check
	FROM parameter
	WHERE name = p_name;

	if (v_check > 0 AND p_parameter_pk IS NULL) then
		htp.p('The name <b>'||p_name||'</b> alredy exists, choose another name.');
		html.e_page; -- skriver ut slutten p� html siden
	else
	begin
		-- henter ut neste nummer i sekvensen til tabellen
		if (p_parameter_pk is NULL AND p_name IS NOT NULL) then
			SELECT parameter_seq.NEXTVAL
			INTO v_parameter_pk
			FROM dual;
			-- legger inn i databasen
			INSERT INTO parameter
			(parameter_pk, name, value, description)
			VALUES (v_parameter_pk, p_name, p_value, p_description);
			commit;
		elsif (p_name IS NOT NULL) then
		begin
			UPDATE parameter
			SET name = p_name, value = p_value, description = p_description
			WHERE parameter_pk = p_parameter_pk;
			commit;
		end;
		end if;
	end;
	end if;
	list_all;

end if;

END;
END insert_action;


---------------------------------------------------------
-- Name: 	delete_entry
-- Type: 	procedure
-- What:
-- Author:  	Frode Klevstul
-- Start date: 	17.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE delete_entry
		(
			p_parameter_pk 		in parameter.parameter_pk%type	default NULL
                )
IS
BEGIN
DECLARE

	v_name		parameter.name%type	default NULL;

BEGIN

if ( login.timeout('param.startup')>0 AND login.right('param')>0 ) then

	SELECT name INTO v_name
	FROM parameter
	WHERE parameter_pk = p_parameter_pk;


	html_top(2);
	htp.p('
		<form action="param.delete_entry_2" method="post">
		<input type="hidden" name="p_parameter_pk" value="'|| p_parameter_pk ||'">
		<table>
		<tr><td colspan="2">Are you sure you want to delete the entry <b>'|| v_name ||'</b> with pk <b>'||p_parameter_pk||'</b>?</td></tr>
		<tr><td><input type="submit" name="p_confirm" value="yes"></td><td align="right"><input type="submit" name="p_confirm" value="no"></td></tr>
		</table>
		</form>
	');
	html.e_page;

end if;

END;
END delete_entry;


---------------------------------------------------------
-- Name: 	delete_entry_2
-- Type: 	procedure
-- What:
-- Author:  	Frode Klevstul
-- Start date: 	17.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE delete_entry_2
		(
			p_parameter_pk 		in parameter.parameter_pk%type	default NULL,
			p_confirm		in varchar2			default NULL
                )
IS
BEGIN

if ( login.timeout('param.startup')>0 AND login.right('param')>0 ) then

	if (p_confirm = 'yes') then
		DELETE FROM parameter
		WHERE parameter_pk = p_parameter_pk;
		commit;
	end if;
	list_all;

end if;

END delete_entry_2;



---------------------------------------------------------
-- Name: 	edit_entry
-- Type: 	procedure
-- What: 	edit a entry in html_code
-- Author:  	Frode Klevstul
-- Start date: 	17.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
--  beskrivelse av inn parametere:
--
--  [parameter]	[description]
---------------------------------------------------------
PROCEDURE edit_entry
		(
			p_parameter_pk 		in parameter.parameter_pk%type	default NULL
                )
IS
BEGIN
DECLARE

	v_name		parameter.name%type		default NULL;
	v_value		parameter.value%type		default NULL;
	v_description	parameter.description%type	default NULL;

BEGIN


if ( login.timeout('param.startup')>0 AND login.right('param')>0 ) then

	SELECT name, value, description INTO v_name, v_value, v_description
	FROM parameter
	WHERE parameter_pk = p_parameter_pk;

	v_description := html.rem_tag(v_description);
	v_value := html.rem_tag(v_value);

	html_top(2);
	htp.p('
		<form name="edit_form" action="param.insert_action" method="post">
		<input type="hidden" name="p_parameter_pk" value="'|| p_parameter_pk ||'">
		<table border="0">
		<tr><td colspan="2" align="center"><b>update an entry in the table "parameter"</b></td></tr>
		<tr><td>name:</td><td><input type="Text" name="p_name" size="50" maxlength="100" value="'||v_name||'"></td></tr>
		<tr><td>value:</td><td><input type="Text" name="p_value" size="50" maxlength="200" value="'||v_value||'"></td></tr>
		<tr><td>description:</td><td><input type="Text" name="p_description" size="70" maxlength="150" value="'||v_description||'"></td></tr>
		<tr><td colspan="2"><input type="Submit" value="Update"></td></tr>
		</table>
		</form>
	');
	html.e_page;

end if;

END;
END edit_entry;



---------------------------------------------------------
-- Name: 	html_top
-- Type: 	procedure
-- What: 	skriver ut html_koden p� toppen (linker)
-- Author:  	Frode Klevstul
-- Start date: 	25.05.2000
-- Desc:
-- --- Changes: ---
-- Date:	[date]
-- What:	[description]
-- Who: 	[name]
---------------------------------------------------------
-- beskrivelse av inn parametere:
--
-- [parameter]	[description]
-- p_link	gir oss nr p� linken vi er p� n�
--		1 = insert_html
--		2 = edit_html
---------------------------------------------------------
PROCEDURE html_top
		(
			p_link		in number default NULL
                )
IS
BEGIN

	html.b_adm_page; 	-- skriver ut starten p� html siden
	htp.p('<table width="100%"><tr><td align="center"><b>parameter</b></td></tr></table>');


	-- utskriving av linkene/menyen p� toppen av siden:
	if (p_link = '1') then
		htp.p('
		<table border="0" width="100%">
		<tr bgcolor="#cccccc"><td bgcolor="#aaaaaa" align="center" width="50%"><a href="param.insert_form"><font color="#ffffff">insert</font></a></td><td align="center"><a href="param.list_all">update/delete</a></td></tr>
		</table>
		');
	elsif (p_link = '2') then
	begin
		htp.p('
		<table border="0" width="100%">
		<tr bgcolor="#cccccc"><td align="center" width="50%"><a href="param.insert_form">insert</a></font></td><td align="center" bgcolor="#aaaaaa"><a href="param.list_all"><font color="#ffffff">update/delete</font></a></td></tr>
		</table>
		');
	end;
	end if;

END html_top;





-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/
show errors;
