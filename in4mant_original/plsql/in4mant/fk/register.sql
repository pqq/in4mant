set define off
PROMPT *** package: REGISTER ***

CREATE OR REPLACE PACKAGE register IS

	PROCEDURE startup;
	PROCEDURE layout;
	PROCEDURE new_event
		(
			p_registration_pk	in registration.registration_pk%type	default NULL,
			p_calendar_pk		in calendar.calendar_pk%type		default NULL,
			p_start_date		in varchar2				default NULL,
			p_end_date		in varchar2				default NULL,
			p_room_for		in registration.room_for%type		default NULL,
			p_type			in varchar2				default NULL,
			p_notification		in registration.notification%type	default NULL
		);
	PROCEDURE select_calevent
		(
			p_calendar_pk		in calendar.calendar_pk%type		default NULL
		);
	PROCEDURE list_registrations;
	PROCEDURE adm_list
		(
			p_registration_pk	registration.registration_pk%type	default NULL,
			p_type			varchar2				default NULL,
			p_user_pk		usr.user_pk%type			default NULL,
			p_from_email		varchar2				default NULL,
			p_from_name		varchar2				default NULL,
			p_subject		varchar2				default NULL,
			p_body			varchar2				default NULL,
			p_unique		varchar2				default NULL
		);
	PROCEDURE enter_for
		(
			p_registration_pk	in registration.registration_pk%type	default NULL,
			p_confirm		in varchar2				default NULL
		);
	PROCEDURE my_entries;
	PROCEDURE list_reg_status;
	PROCEDURE insert_reg_status
			(
				p_reg_status_pk		in reg_status.reg_status_pk%type	default NULL,
				p_name			in string_group.name%type		default NULL,
				p_description		in reg_status.description%type	 	default NULL
	                );
	PROCEDURE delete_reg_status
			(
				p_reg_status_pk 	in reg_status.reg_status_pk%type		default NULL,
				p_confirm		in varchar2					default NULL
	                );
	PROCEDURE update_reg_status
			(
				p_reg_status_pk 	in reg_status.reg_status_pk%type	default NULL
	                );

END;
/
show errors;


CREATE OR REPLACE PACKAGE BODY register IS


---------------------------------------------------------
-- Name: 	startup
-- Type: 	procedure
-- What: 	start procedure, to run the package
-- Author:  	Frode Klevstul
-- Start date: 	25.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

	register.layout;

END startup;




---------------------------------------------------------
-- Name: 	layout
-- Type: 	procedure
-- What: 	writes out the start page layout
-- Author:  	Frode Klevstul
-- Start date: 	22.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE layout
IS
BEGIN
if ( login.timeout('register.startup')>0 ) then


	html.b_page(NULL, NULL, NULL, 2);
	register.new_event;
	htp.p('<br><br>');
	register.list_registrations;
	html.e_page;


end if;
END layout;




---------------------------------------------------------
-- Name: 	new_event
-- Type: 	procedure
-- What: 	used for register new events, update and deleting event
-- Author:  	Frode Klevstul
-- Start date: 	22.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE new_event
	(
		p_registration_pk	in registration.registration_pk%type	default NULL,
		p_calendar_pk		in calendar.calendar_pk%type		default NULL,
		p_start_date		in varchar2				default NULL,
		p_end_date		in varchar2				default NULL,
		p_room_for		in registration.room_for%type		default NULL,
		p_type			in varchar2				default NULL,
		p_notification		in registration.notification%type	default NULL
	)
IS
BEGIN
DECLARE

	v_organization_pk	organization.organization_pk%type	default NULL;
	v_registration_pk	registration.registration_pk%type	default NULL;
	v_room_for		registration.room_for%type		default NULL;
	v_start_date		registration.start_date%type		default NULL;
	v_end_date		registration.end_date%type		default NULL;
	v_heading		calendar.heading%type			default NULL;
	v_calendar_pk		calendar.calendar_pk%type		default NULL;
	v_notification		registration.notification%type		default NULL;

BEGIN
if ( login.timeout('register.startup')>0 ) then

	-- ----------------------------------------------------
	-- sjekker om brukeren har registrert en organisasjon
	-- ----------------------------------------------------
        v_organization_pk := get.oid;
        if ( v_organization_pk IS NULL ) then
                htp.p( get.msg(1,'Not a client user!') );
                return;
	end if;



	-- ----------------------------------------------------
	-- skriv ut html form
	-- ----------------------------------------------------
	if (p_type IS NULL AND p_calendar_pk IS NULL) then
		html.b_box( get.txt('create_new_registration'), '100%', 'register.new_event');
		html.b_form('register.new_event');
		html.b_table;

		htp.p('<input type="hidden" name="p_type" value="insert">');
		htp.p('<tr><td colspan="2">'||get.txt('create_new_reg_desc')||'</td></tr>');
		htp.p('<tr><td colspan="2"><hr noshade></td></tr>');
		htp.p('<tr><td>'|| get.txt('reg_room_for') ||':</td><td>'|| html.text('p_room_for', '4', '6') ||'</td></tr>');
		htp.p('<tr><td>'|| get.txt('reg_choose_cal_event') ||':</td><td>'); register.select_calevent; htp.p('</td></tr>');
		htp.p('
			<tr><td>'|| get.txt('reg_start_date') ||':</td><td>
	                <input type="text" name="p_start_date" value="'||to_char(sysdate,get.txt('date_long'))||'"
	                onSelect=javascript:openWin(''date_time.startup?p_variable=p_start_date'',300,200)
	                onClick=javascript:openWin(''date_time.startup?p_variable=p_start_date'',300,200)>');
	                htp.p( html.popup( get.txt('change_start_date'),'date_time.startup?p_variable=p_start_date','300', '200') );
	                htp.p('</td></tr>');
		htp.p('
	                <tr><td>'|| get.txt('reg_end_date') ||':</td><td>
	                <input type="text" name="p_end_date" value="'||to_char( (sysdate+30), get.txt('date_long'))||'"
	                onSelect=javascript:openWin(''date_time.startup?p_variable=p_end_date&p_start_date='|| to_char((sysdate+30),get.txt('date_full'))||''',300,200)
	                onClick=javascript:openWin(''date_time.startup?p_variable=p_end_date&p_start_date='|| to_char((sysdate+30),get.txt('date_full'))||''',300,200)>');
	                htp.p( html.popup( get.txt('change_end_date'),'date_time.startup?p_variable=p_end_date&p_start_date='|| to_char((sysdate+30),get.txt('date_full')),'300', '200') );
	                htp.p('</td></tr>');
		htp.p('<tr><td>'|| get.txt('reg_notification') ||':</td><td><select name="p_notification"><option value="1">'||get.txt('yes')||'</option><option value="">'||get.txt('no')||'</option></select></td></tr>');
		htp.p('<tr><td colspan="2" align="right">'); html.submit_link(get.txt('reg_activate')); htp.p('</td></tr>');

		html.e_table;
		html.e_form;
		html.e_box;
	-- ----------------------------------------------------
	-- insert into db
	-- ----------------------------------------------------
	elsif (p_type = 'insert' AND p_calendar_pk IS NOT NULL) then

		SELECT	registration_seq.NEXTVAL
		INTO	v_registration_pk
		FROM	dual;

	        if ( p_room_for IS NOT NULL ) then
	                if ( owa_pattern.match(p_room_for, '^\d{1,}$') ) then
	                        v_room_for    := p_room_for;
	                end if;
	        end if;

		INSERT INTO registration
		(registration_pk, calendar_fk, start_date, end_date, room_for, notification)
		VALUES (v_registration_pk, p_calendar_pk, to_date(p_start_date, get.txt('date_long')), to_date(p_end_date, get.txt('date_long')), v_room_for, p_notification );
		commit;

		html.jump_to('register.startup');
	-- ----------------------------------------------------
	-- update db
	-- ----------------------------------------------------
	elsif (p_type = 'update_action' AND p_registration_pk IS NOT NULL) then

	        if ( p_room_for IS NOT NULL ) then
	                if ( owa_pattern.match(p_room_for, '^\d{1,}$') ) then
	                        v_room_for    := p_room_for;
	                end if;
	        end if;

		UPDATE	registration
		SET	start_date = to_date(p_start_date, get.txt('date_long')),
			end_date = to_date(p_end_date, get.txt('date_long')),
			room_for = v_room_for,
			notification = p_notification
		WHERE	registration_pk = p_registration_pk;
		commit;

		html.jump_to('register.startup');
	-- ----------------------------------------------------
	-- html update form
	-- ----------------------------------------------------
	elsif (p_type = 'update' AND p_registration_pk IS NOT NULL) then

		SELECT	heading, start_date, reg.end_date, room_for, calendar_pk, notification
		INTO	v_heading, v_start_date, v_end_date, v_room_for, v_calendar_pk, v_notification
		FROM	registration reg, calendar
		WHERE	calendar_fk = calendar_pk
		AND	registration_pk = p_registration_pk
		AND	organization_fk = v_organization_pk;

		html.b_page(NULL, NULL, NULL, 2);
		html.b_box( get.txt('update_registration'), '100%', 'register.new_event(update)');
		html.b_form('register.new_event');
		html.b_table;

		htp.p('<input type="hidden" name="p_type" value="update_action">');
		htp.p('<input type="hidden" name="p_registration_pk" value="'||p_registration_pk||'">');
		htp.p('<tr><td>'|| get.txt('reg_room_for') ||':</td><td>'|| html.text('p_room_for', '4', '6', v_room_for) ||'</td></tr>');
		htp.p('<tr><td>'|| get.txt('cal_heading') ||':</td><td>'); register.select_calevent(v_calendar_pk); htp.p('</td></tr>');
		htp.p('
			<tr><td>'|| get.txt('reg_start_date') ||':</td><td>
	                <input type="text" name="p_start_date" value="'||to_char(v_start_date,get.txt('date_long')) ||'"
	                onSelect=javascript:openWin(''date_time.startup?p_variable=p_start_date&p_start_date='|| to_char(v_start_date,get.txt('date_full'))||''',300,200)
	                onClick=javascript:openWin(''date_time.startup?p_variable=p_start_date&p_start_date='|| to_char(v_start_date,get.txt('date_full'))||''',300,200)>');
	                htp.p( html.popup( get.txt('change_start_date'),'date_time.startup?p_variable=p_start_date&p_start_date='|| to_char(v_start_date,get.txt('date_full')),'300', '200') );
	                htp.p('</td></tr>');
		htp.p('
	                <tr><td>'|| get.txt('reg_end_date') ||':</td><td>
	                <input type="text" name="p_end_date" value="'|| to_char(v_end_date, get.txt('date_long')) ||'"
	                onSelect=javascript:openWin(''date_time.startup?p_variable=p_end_date&p_start_date='|| to_char(v_end_date,get.txt('date_full'))||''',300,200)
	                onClick=javascript:openWin(''date_time.startup?p_variable=p_end_date&p_start_date='|| to_char(v_end_date,get.txt('date_full'))||''',300,200)>');
	                htp.p( html.popup( get.txt('change_end_date'),'date_time.startup?p_variable=p_end_date&p_start_date='|| to_char(v_end_date,get.txt('date_full')),'300', '200') );
	                htp.p('</td></tr>');
		htp.p('<tr><td>'|| get.txt('reg_notification') ||':</td><td><select name="p_notification"><option value="1">'||get.txt('yes')||'</option><option value="" '); if(v_notification IS NULL) then htp.p('selected'); end if; htp.p('>'||get.txt('no')||'</option></select></td></tr>');
		htp.p('<tr><td colspan="2" align="right">'); html.submit_link(get.txt('reg_update')); htp.p('</td></tr>');

		html.e_form;
		htp.p('<tr><td colspan="2" align="right">'); html.back(get.txt('go_back_without_update')); htp.p('</td></tr>');
		html.e_table;
		html.e_box;
		html.e_page;
	-- ----------------------------------------------------
	-- confirm delete
	-- ----------------------------------------------------
	elsif (p_type = 'delete' AND p_registration_pk IS NOT NULL) then

		SELECT	heading
		INTO	v_heading
		FROM	registration, calendar
		WHERE	calendar_fk = calendar_pk
		AND	registration_pk = p_registration_pk
		AND	organization_fk = v_organization_pk;

		html.b_page(NULL, NULL, NULL, 2);
		html.b_box( get.txt('delete_reg_event'), '100%', 'register.new_event(delete)');
		html.b_table;

		html.b_form('register.new_event');
		htp.p('<input type="hidden" name="p_type" value="delete_action">');
		htp.p('<input type="hidden" name="p_registration_pk" value="'|| p_registration_pk ||'">');
		htp.p('<tr><td>'|| get.txt('confirm_delete_reg') ||' '''|| v_heading ||'''?</td></tr>');
		htp.p('<tr><td align="right">'); html.submit_link( get.txt('yes') ); htp.p('</td></tr>');
		html.e_form;
		htp.p('<tr><td align="right">'); html.back( get.txt('go_back_without_delete') ); htp.p('</td></tr>');

		html.e_table;
		html.e_box;
		html.e_page;
	-- ----------------------------------------------------
	-- delete from db
	-- ----------------------------------------------------
	elsif (p_type = 'delete_action' AND p_registration_pk IS NOT NULL) then

		DELETE FROM registration
		WHERE	registration_pk = p_registration_pk
		AND	calendar_fk IN
			(
				SELECT	calendar_pk
				FROM	calendar
				WHERE	organization_fk = v_organization_pk
			);
		commit;

		html.jump_to('register.startup');
	else
		html.jump_to('register.startup');
	end if;

end if;
EXCEPTION WHEN NO_DATA_FOUND
	THEN htp.p( get.msg('wrong values') );
END;
END new_event;




---------------------------------------------------------
-- Name: 	select_calevent
-- Type: 	procedure
-- What: 	used for register new events
-- Author:  	Frode Klevstul
-- Start date: 	22.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE select_calevent
	(
		p_calendar_pk		in calendar.calendar_pk%type	default NULL
	)
IS
BEGIN
DECLARE

	CURSOR	select_all IS
	SELECT	calendar_pk, heading, event_date
	FROM	calendar
	WHERE	organization_fk = get.oid
	AND	event_date > sysdate
	AND	calendar_pk NOT IN
		(
			SELECT	calendar_fk
			FROM	registration
		);

	v_calendar_pk		calendar.calendar_pk%type	default NULL;
	v_heading		calendar.heading%type		default NULL;
	v_event_date		calendar.event_date%type	default NULL;

BEGIN


	if (p_calendar_pk IS NOT NULL) then

		SELECT	heading, event_date
		INTO	v_heading, v_event_date
		FROM	calendar
		WHERE	organization_fk = get.oid
		AND	calendar_pk = p_calendar_pk;

		htp.p( v_heading ||' ('|| to_char(v_event_date, get.txt('date_year')) ||')');
	else

		htp.p('<select name="p_calendar_pk">');
		open select_all;
		loop
			fetch select_all into v_calendar_pk, v_heading, v_event_date;
			exit when select_all%NOTFOUND;
			htp.p('<option value="'||v_calendar_pk||'">'|| v_heading ||' ('|| to_char(v_event_date, get.txt('date_year')) ||')</option>');
		end loop;
		if (select_all%ROWCOUNT = 0) then
			htp.p('<option value="">'|| get.txt('no_cal_events') ||'</option>');
		end if;
		close select_all;
		htp.p('</select>');
	end if;


EXCEPTION WHEN NO_DATA_FOUND THEN
	htp.p( get.msg('wrong values') );
END;
END select_calevent;




---------------------------------------------------------
-- Name: 	list_registrations
-- Type: 	procedure
-- What: 	used for register new events
-- Author:  	Frode Klevstul
-- Start date: 	22.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE list_registrations
IS
BEGIN
DECLARE

	CURSOR	select_all IS
	SELECT	registration_pk, heading, event_date, room_for, start_date, reg.end_date
	FROM	calendar, registration reg
	WHERE	organization_fk = get.oid
	AND	calendar_pk = calendar_fk
	ORDER BY event_date;

	v_registration_pk	registration.registration_pk%type	default NULL;
	v_heading		calendar.heading%type			default NULL;
	v_event_date		calendar.event_date%type		default NULL;
	v_room_for		registration.room_for%type		default NULL;
	v_start_date		registration.start_date%type		default NULL;
	v_end_date		registration.end_date%type		default NULL;
	v_date_string		varchar2(50)				default NULL;
	v_persons_all		number					default NULL;
	v_persons_acc		number					default NULL;
	v_persons_nacc		number					default NULL;

BEGIN
if ( login.timeout('register.startup')>0 ) then

	html.b_box( get.txt('list_registrations'), '100%', 'register.list_registrations');
	html.b_table;
	htp.p('
		<tr>
		<td>&nbsp;</td>
		<td><b>'|| get.txt('cal_heading') ||':</b></td>
		<td><b>'|| get.txt('reg_room_for') ||':</b></td>
		<td><b>'|| get.txt('reg_booking') ||':</b></td>
		<td><b>'|| get.txt('reg_adm_list') ||':</b></td>
		<td><b>'|| get.txt('reg_update') ||':</b></td>
		<td><b>'|| get.txt('reg_delete') ||':</b></td>
		</tr>
	');

	open select_all;
	loop
		fetch select_all into v_registration_pk, v_heading, v_event_date, v_room_for, v_start_date, v_end_date;
		exit when select_all%NOTFOUND;

		htp.p('<tr><td>');
		if ( v_start_date < sysdate AND v_end_date > sysdate) then
			htp.p( get.txt('item_stat_started') );
		elsif ( v_start_date < sysdate AND v_end_date < sysdate) then
			htp.p( get.txt('item_stat_stopped') );
		elsif ( v_start_date > sysdate ) then
			htp.p( get.txt('item_stat_waiting') );
		end if;

		if (v_event_date < sysdate) then
			v_date_string := '(<font color="#ff0000">'|| to_char(v_event_date, get.txt('date_year')) ||'</font>)';
		else
			v_date_string := '('|| to_char(v_event_date, get.txt('date_year')) ||')';
		end if;

		SELECT	count(*)
		INTO	v_persons_all
		FROM 	user_reg
		WHERE	registration_fk = v_registration_pk;

		SELECT	count(*)
		INTO	v_persons_acc
		FROM 	user_reg
		WHERE	registration_fk = v_registration_pk
		AND	reg_status_fk = 1;

		SELECT	count(*)
		INTO	v_persons_nacc
		FROM 	user_reg
		WHERE	registration_fk = v_registration_pk
		AND	reg_status_fk = 2;

		htp.p(' </td>
			<td>'|| v_heading||' '|| v_date_string ||'</td>
			<td align="center">'||v_room_for||'</td>
			<td align="left">'||get.txt('reg_tot')||':'||v_persons_all||' '||get.txt('reg_acc')||':'||v_persons_acc||' '||get.txt('reg_nacc')||':'||v_persons_nacc||'</td>
			<td>'); html.button_link(get.txt('reg_adm_list'), 'register.adm_list?p_registration_pk='||v_registration_pk||'&p_type=view'); htp.p('</td>
			<td>'); html.button_link(get.txt('reg_update'), 'register.new_event?p_registration_pk='||v_registration_pk||'&p_type=update'); htp.p('</td>
			<td>'); html.button_link(get.txt('reg_delete'), 'register.new_event?p_registration_pk='||v_registration_pk||'&p_type=delete'); htp.p('</td>
		 	</tr>
		 ');

	end loop;
	if (select_all%ROWCOUNT = 0) then
		htp.p('<tr><td colspan="4">'|| get.txt('no_registrations') ||'</td></tr>');
	end if;
	close select_all;

	html.e_table;
	html.e_box;

end if;
END;
END list_registrations;





---------------------------------------------------------
-- Name: 	adm_list
-- Type: 	procedure
-- What: 	used to administrate a registration list with users
-- Author:  	Frode Klevstul
-- Start date: 	24.10.2000
-- Desc:	p_unique m� tas med for at siden skal reloade riktig n�r man
---------------------------------------------------------
PROCEDURE adm_list
	(
		p_registration_pk	registration.registration_pk%type	default NULL,
		p_type			varchar2				default NULL,
		p_user_pk		usr.user_pk%type			default NULL,
		p_from_email		varchar2				default NULL,
		p_from_name		varchar2				default NULL,
		p_subject		varchar2				default NULL,
		p_body			varchar2				default NULL,
		p_unique		varchar2				default NULL
	)
IS
BEGIN
DECLARE

	CURSOR	select_new IS
	SELECT	user_fk, registration_date
	FROM	user_reg, registration, calendar
	WHERE	registration_fk = registration_pk
	AND	calendar_fk = calendar_pk
	AND	organization_fk = get.oid
	AND	registration_pk = p_registration_pk
	AND	reg_status_fk IS NULL
	ORDER BY registration_date ASC;

	CURSOR	select_other(v_reg_status_pk reg_status.reg_status_pk%type) IS
	SELECT	user_fk, registration_date
	FROM	user_reg, registration, calendar
	WHERE	registration_fk = registration_pk
	AND	calendar_fk = calendar_pk
	AND	organization_fk = get.oid
	AND	registration_pk = p_registration_pk
	AND	reg_status_fk = v_reg_status_pk
	ORDER BY registration_date ASC;

	CURSOR	select_cal(v_registration_pk in registration.registration_pk%type) IS
	SELECT	event_date, heading, room_for
	FROM	calendar, registration
	WHERE	calendar_fk = calendar_pk
	AND	registration_pk = v_registration_pk
	AND	organization_fk = get.oid;

	CURSOR	select_users
		(
			v_registration_pk in registration.registration_pk%type,
			v_reg_status_pk in user_reg.reg_status_fk%type
		) IS
	SELECT	email
	FROM	user_details ud, user_reg ur
	WHERE	ud.user_fk = ur.user_fk
	AND	ur.registration_fk = v_registration_pk
	AND	ur.reg_status_fk = v_reg_status_pk;

	CURSOR	select_users_2
		(
			v_registration_pk in registration.registration_pk%type
		) IS
	SELECT	email
	FROM	user_details ud, user_reg ur
	WHERE	ud.user_fk = ur.user_fk
	AND	ur.registration_fk = v_registration_pk
	AND	ur.reg_status_fk IS NULL;

	v_user_pk		usr.user_pk%type			default NULL;
	v_registration_date	user_reg.registration_date%type		default NULL;
	v_event_date		calendar.event_date%type		default NULL;
	v_heading		calendar.heading%type			default NULL;
	v_room_for		registration.room_for%type		default NULL;
	v_persons		number					default NULL;
	v_coverage		number					default NULL;
	v_email			user_details.email%type			default NULL;

	v_file			utl_file.file_type			default NULL;
	v_email_dir		varchar2(100)				default NULL;
	v_number		number					default NULL;
	v_message		varchar2(4000)				default NULL;

BEGIN
if ( login.timeout('register.adm_list?p_registration_pk='||p_registration_pk||'&p_type=view')>0 ) then

	-- --------------------------------------------------------------------------------
	-- view: lister ut en overikt over registrerte/p�meldte i forskjellige kategorier
	-- --------------------------------------------------------------------------------
	if (p_type = 'view') then
		html.b_page(NULL, NULL, NULL, 2);
		html.b_table;
		htp.p('<tr><td colspan="2">');
			-- ----------------------------------------------------------------------------
			-- skriver ut litt informasjon om kalenderhendelsen p�meldingen er tilknyttet
			-- ----------------------------------------------------------------------------
			html.b_box(get.txt('cal_event_info'), '100%', 'register.adm_list(cal_info)');
			html.b_table;

			open select_cal(p_registration_pk);
			loop
				fetch select_cal into v_event_date, v_heading, v_room_for;
				exit when select_cal%NOTFOUND;

				SELECT	count(*)
				INTO	v_persons
				FROM	user_reg
				WHERE	reg_status_fk = 1
				AND	registration_fk = p_registration_pk;

				v_coverage := ( (v_persons/v_room_for) * 100);

				htp.p('
					<tr>
						<td><b>'|| get.txt('cal_heading') ||':</b></td>
						<td>'||v_heading||' ('|| to_char(v_event_date, get.txt('date_year')) ||')</td>
					</tr>
					<tr>
						<td><b>'|| get.txt('reg_room_for') ||':</b></td>
						<td>'||v_room_for||'</td>
					</tr>
					<tr>
						<td><b>'|| get.txt('reg_coverage') ||':</b></td>
						<td>'||v_persons||' ('||ROUND(v_coverage,2)||'%)</td>
					</tr>
				');
			end loop;
			close select_cal;

			html.e_table;
			html.e_box;
		htp.p('</td></tr>');
		htp.p('<tr><td colspan="2"><hr noshade></td></tr>');
		htp.p('<tr><td colspan="2">');
			-- ------------------------------------------
			-- skriver ut en liste over nye p�meldinger
			-- ------------------------------------------
			html.b_box_2(get.txt('reg_new_users'), '100%', 'register.adm_list(new_users)', NULL, 2);
			html.b_table;
			htp.p('
				<tr><td colspan="5" align="right">
				<a href="register.adm_list?p_registration_pk='||p_registration_pk||'&p_type=email_form_0">'||get.txt('send_email_to_group')||'</a>&nbsp;'||
				html.popup( get.txt('print_page'), 'register.adm_list?p_type=print_preview_0&p_registration_pk='||p_registration_pk,'620', '650', '1' )
				||'</td></tr>
			');
			htp.p('
					<tr>
					<td valign="top"><b>'|| get.txt('name') ||':</b></td>
					<td valign="top"><b>'|| get.txt('the_email_address') ||':</b></td>
					<td valign="top"><b>'|| get.txt('registration_date') ||':</b></td>
					<td valign="top"><b>'|| get.txt('accept_user') ||':</b></td>
					<td valign="top"><b>'|| get.txt('dont_accept_user') ||':</b></td>
					</tr>
			');

			open select_new;
			loop
				fetch select_new into v_user_pk, v_registration_date;
				exit when select_new%NOTFOUND;
				htp.p('
					<tr>
					<td valign="top">'|| get.uname(v_user_pk) ||'</td>
					<td valign="top"><a href="tip.mailform?p_contact_email='||get.email(v_user_pk)||'&p_email_address='||get.email||'">'|| get.email(v_user_pk) ||'</a></td>
					<td valign="top">'|| to_char(v_registration_date, get.txt('date_long')) ||'</td>
					<td>'); html.button_link( get.txt('accept_user'), 'register.adm_list?p_type=accept&p_registration_pk='||p_registration_pk||'&p_user_pk='||v_user_pk); htp.p('</td>
					<td>'); html.button_link( get.txt('dont_accept_user'), 'register.adm_list?p_type=dont_accept&p_registration_pk='||p_registration_pk||'&p_user_pk='||v_user_pk); htp.p('</td>
					</tr>
				');
			end loop;
			close select_new;
			html.e_table;
			html.e_box_2;

		htp.p('</td></tr>');
		htp.p('<tr><td colspan="2">&nbsp;</td></tr>');
		htp.p('<tr><td valign="top" width="50%">');
			-- -------------------------------
			-- lister ut aksepterte brukere
			-- -------------------------------
			html.b_box_2(get.txt('reg_accepted_users'), '100%', 'register.adm_list(accepted)', NULL, 2);
			html.b_table;
			htp.p('
				<tr><td colspan="4" align="right">
				<a href="register.adm_list?p_registration_pk='||p_registration_pk||'&p_type=email_form_1">'||get.txt('send_email_to_group')||'</a>&nbsp;'||
				html.popup( get.txt('print_page'), 'register.adm_list?p_type=print_preview_1&p_registration_pk='||p_registration_pk,'620', '650', '1' )
				||'</td></tr>
			');

			open select_other(1); -- 1 = accepted
			loop
				fetch select_other into v_user_pk, v_registration_date;
				exit when select_other%NOTFOUND;
				htp.p('
					<tr>
					<td valign="top"><a href="tip.mailform?p_contact_email='||get.email(v_user_pk)||'&p_email_address='||get.email||'">'|| get.uname(v_user_pk) ||'</a></td>
					<td align="right">'); html.button_link( get.txt('dont_accept_user'), 'register.adm_list?p_type=dont_accept&p_registration_pk='||p_registration_pk||'&p_user_pk='||v_user_pk||'&p_unique='||to_char(sysdate, get.txt('date_full'))); htp.p('</td>
					</tr>
				');
			end loop;
			close select_other;
			html.e_table;
			html.e_box_2;

		htp.p('</td><td valign="top">');
			-- -----------------------------------
			-- lister ut ikke-aksepterte brukere
			-- -----------------------------------
			html.b_box_2(get.txt('reg_not_accepted_users'), '100%', 'register.adm_list(not_accepted)', NULL, 2);
			html.b_table;
			htp.p('
				<tr><td colspan="4" align="right">
				<a href="register.adm_list?p_registration_pk='||p_registration_pk||'&p_type=email_form_2">'||get.txt('send_email_to_group')||'</a>&nbsp;'||
				html.popup( get.txt('print_page'), 'register.adm_list?p_type=print_preview_2&p_registration_pk='||p_registration_pk,'620', '650', '1' )
				||'</td></tr>
			');

			open select_other(2); -- 2 = not accepted
			loop
				fetch select_other into v_user_pk, v_registration_date;
				exit when select_other%NOTFOUND;
				htp.p('
					<tr>
					<td valign="top"><a href="tip.mailform?p_contact_email='||get.email(v_user_pk)||'&p_email_address='||get.email||'">'|| get.uname(v_user_pk) ||'</a></td>
					<td align="right">'); html.button_link( get.txt('accept_user'), 'register.adm_list?p_type=accept&p_registration_pk='||p_registration_pk||'&p_user_pk='||v_user_pk||'&p_unique='||to_char(sysdate, get.txt('date_full'))); htp.p('</td>
					</tr>
				');
			end loop;
			close select_other;
			html.e_table;
			html.e_box_2;

		htp.p('</td></tr>');
		htp.p('<tr><td colspan="2""> <hr noshade> </td></tr>');
		htp.p('<tr><td colspan="2" align="right">'); html.button_link( get.txt('back_to_reg_overview'), 'register.startup'); htp.p('</td></tr>');
		html.e_table;
		html.e_page;
	-- ------------------------------------------
	-- accept: registrer brukeren som akseptert
	-- ------------------------------------------
	elsif(p_type = 'accept') then
		UPDATE	user_reg
		SET	reg_status_fk = 1
		WHERE	user_fk = p_user_pk
		AND	registration_fk = p_registration_pk;
		commit;

		html.jump_to('register.adm_list?p_type=view&p_registration_pk='||p_registration_pk);
	-- ----------------------------------------------------
	-- dont_accept: registrer brukeren som ikke-akseptert
	-- ----------------------------------------------------
	elsif(p_type = 'dont_accept') then
		UPDATE	user_reg
		SET	reg_status_fk = 2
		WHERE	user_fk = p_user_pk
		AND	registration_fk = p_registration_pk;
		commit;

		html.jump_to('register.adm_list?p_type=view&p_registration_pk='||p_registration_pk);
	-- --------------------------------------------------------------------------------------------------------------------
	-- print_preview_0 _1 _2: vis liste for utskrift med 0: nye brukere 1: aksepterte brukere 2: ikke-aksepterte brukere
	-- --------------------------------------------------------------------------------------------------------------------
	elsif( owa_pattern.match(p_type, '^print_preview_\d$') ) then
		html.b_page_2('600');
		html.b_table;

		open select_cal(p_registration_pk);
		loop
			fetch select_cal into v_event_date, v_heading, v_room_for;
			exit when select_cal%NOTFOUND;

		end loop;
		close select_cal;

		htp.p('
			<tr><td colspan="4"><h2>'|| get.oname ||': '|| v_heading ||' ('|| to_char(v_event_date, get.txt('date_year') ) ||')</h2></td></tr>
			<tr><td colspan="4">&nbsp;</td></tr>
		');

		htp.p('
			<tr>
			<td width="10%"><b>'|| get.txt('number') ||':</b></td>
			<td width="20%"><b>'|| get.txt('registration_date') ||':</b></td>
			<td width="30%"><b>'|| get.txt('user_name') ||':</b></td>
			<td width="40%"><b>'|| get.txt('the_email_address') ||':</b></td>
			</tr>
			<tr><td colspan="4"><hr noshade></td></tr>
		');

		-- --------------------------------------------------------
		-- utlisting av aksepterte eller ikke-aksepterte brukere
		-- --------------------------------------------------------
		if (p_type = 'print_preview_1') then
			open select_other(1);
		elsif (p_type = 'print_preview_2') then
			open select_other(2);
		end if;

		if (select_other%ISOPEN) then
			loop

				fetch select_other into v_user_pk, v_registration_date;
				exit when select_other%NOTFOUND;

				htp.p('
					<tr>
					<td width="10%"><b>'|| select_other%ROWCOUNT ||':</b></td>
					<td>'|| to_char(v_registration_date, get.txt('date_long') ) ||'</td>
					<td>'|| get.uname(v_user_pk) ||'</td>
					<td>'|| get.email(v_user_pk) ||'</td>
					</tr>
				');
			end loop;
			close select_other;
		end if;


		-- --------------------------------------------------------
		-- utlisting av alle nye brukere (uten status)
		-- --------------------------------------------------------
		if (p_type = 'print_preview_0') then
			open select_new;
			loop
				fetch select_new into v_user_pk, v_registration_date;
				exit when select_new%NOTFOUND;

				htp.p('
					<tr>
					<td width="10%"><b>'|| select_new%ROWCOUNT ||':</b></td>
					<td>'|| to_char(v_registration_date, get.txt('date_long') ) ||'</td>
					<td>'|| get.uname(v_user_pk) ||'</td>
					<td>'|| get.email(v_user_pk) ||'</td>
					</tr>
				');
			end loop;
			close select_new;
		end if;

		htp.p('
			<tr><td colspan="4">&nbsp;</td></tr>
			<tr><td align="right" colspan="4">'); html.close( get.txt('close_window') ); htp.p('</td></tr>
		');
		html.e_table;
		html.e_page_2;
	-- -----------------------
	-- skriv ut email formet
	-- -----------------------
	elsif( owa_pattern.match(p_type, '^email_form_\d$') ) then

		open select_cal(p_registration_pk);
		loop
			fetch select_cal into v_event_date, v_heading, v_room_for;
			exit when select_cal%NOTFOUND;
		end loop;
		close select_cal;

		html.b_page(NULL, NULL, NULL, 2);

		if (p_type = 'email_form_0') then
			html.b_box( get.txt('write_email_to_new_users'), '100%', 'register.adm_list(email_form)');
			html.b_form('register.adm_list');
			htp.p('<input type="hidden" name="p_registration_pk" value="'||p_registration_pk||'">');
			htp.p('<input type="hidden" name="p_type" value="send_email_0">');
			html.b_table;
			htp.p('<tr><td colspan="2">'|| get.txt('write_email_new_desc') ||'</td></tr>');
		elsif (p_type = 'email_form_1') then
			html.b_box( get.txt('write_email_to_accepted_users'), '100%', 'register.adm_list(email_form)');
			html.b_form('register.adm_list');
			htp.p('<input type="hidden" name="p_registration_pk" value="'||p_registration_pk||'">');
			htp.p('<input type="hidden" name="p_type" value="send_email_1">');
			html.b_table;
			htp.p('<tr><td colspan="2">'|| get.txt('write_email_acc_desc') ||'</td></tr>');
		elsif (p_type = 'email_form_2') then
			html.b_box( get.txt('write_email_to_not_acc_users'), '100%', 'register.adm_list(email_form)');
			html.b_form('register.adm_list');
			htp.p('<input type="hidden" name="p_registration_pk" value="'||p_registration_pk||'">');
			htp.p('<input type="hidden" name="p_type" value="send_email_2">');
			html.b_table;
			htp.p('<tr><td colspan="2">'|| get.txt('write_email_nacc_desc') ||'</td></tr>');
		end if;

		htp.p('<tr><td colspan="2">&nbsp;</td></tr>');
		htp.p('<tr><td>'|| get.txt('group_email_address') ||':</td><td>'|| html.text('p_from_email', '55', '55', get.oemail) ||'</td></tr>');
		htp.p('<tr><td>'|| get.txt('group_name') ||':</td><td>'||html.text('p_from_name', '55', '55', get.oname)||'</td></tr>');
		htp.p('<tr><td>'|| get.txt('email_subject') ||':</td><td>'|| html.text('p_subject', '55', '150', v_heading ||' ('|| to_char(v_event_date, get.txt('date_year')) ||')') ||'</td></tr>');
		htp.p('<tr><td valign="top">'|| get.txt('email_body') ||':</td><td><textarea name="p_body" ROWS="10" cols="55"></textarea></td></tr>');
		htp.p('<tr><td colspan="2" align="right">'); html.submit_link('send_email'); htp.p('</td></tr>');

		html.e_form;
		htp.p('<tr><td colspan="2" align="right">'); html.back( get.txt('go_back') ); htp.p('</td></tr>');
		html.e_table;
		html.e_box;
		html.e_page;


	-- --------------------------------------------
	-- send email til alle brukererne i en gruppe
	-- --------------------------------------------
	elsif( owa_pattern.match(p_type, '^send_email_\d$') ) then

		html.b_page(NULL, 5, 'register.adm_list?p_registration_pk='||p_registration_pk||'&p_type=view');
		html.b_box( get.txt('the_email_is_sendt_to') );

		-- -------------------------------------------
		-- henter ut neste nummer i email sekvensen
		-- -------------------------------------------
		SELECT  email_script_seq.NEXTVAL
		INTO    v_number
		FROM    DUAL;

		-- -------------------------------------------
		-- finner katalogen vi skal skrive til
		-- -------------------------------------------
		v_email_dir 	:= get.value('email_dir');

		-- -------------------------------------------
		-- �pner filen hvor vi skrive selve mailen
		-- -------------------------------------------
                v_file := utl_file.fopen( v_email_dir, v_number||'_message.txt' , 'w');

		-- -------------------------------------------
		-- setter email avsender
		-- -------------------------------------------
		IF ( p_from_email IS NOT NULL ) THEN
			v_email := p_from_email;
		ELSE
			v_email := get.value('email_info');
		END IF;

		-- -------------------------------------------
		-- skriver mailen til fil
		-- -------------------------------------------
utl_file.put_line(v_file, 'From: in4mant.com <'|| v_email ||'>
Reply-To: '|| v_email ||'
Subject: '|| p_subject ||'


'|| get.txt('from') ||' : '|| p_from_name ||'
'|| get.txt('from_email_address') ||' : '|| p_from_email ||'

'|| p_body ||'

-- in4mant.com --
' );
		-- -------------------------------------------
		-- lukker filen
		-- -------------------------------------------
                utl_file.fclose(v_file);



		-- -------------------------------------------
		-- �pner filen som vi skal skrive mottagerene av emailen til
		-- -------------------------------------------
		v_file := utl_file.fopen( v_email_dir, v_number||'_email-list.txt', 'a');

		if (p_type = 'send_email_0') then
			open select_users_2(p_registration_pk);
		elsif (p_type = 'send_email_1') then
			open select_users(p_registration_pk, 1);
		elsif (p_type = 'send_email_2') then
			open select_users(p_registration_pk, 2);
		end if;

		if (select_users%ISOPEN) then
			loop
				fetch select_users into v_email;
				exit when select_users%NOTFOUND;

				utl_file.put_line(v_file, v_email);
				htp.p(v_email||'<br>');
			end loop;
			close select_users;
		elsif (select_users_2%ISOPEN) then
			loop
				fetch select_users_2 into v_email;
				exit when select_users_2%NOTFOUND;

				utl_file.put_line(v_file, v_email);
				htp.p(v_email||'<br>');
			end loop;
			close select_users_2;
		end if;

		-- -------------------------------------------
		-- lukker filen
		-- -------------------------------------------
                utl_file.fclose(v_file);


		html.e_box;
		html.e_page;
	end if;

end if;
END;
END adm_list;




---------------------------------------------------------
-- Name: 	enter_for
-- Type: 	procedure
-- What: 	used for users to enter an event
-- Author:  	Frode Klevstul
-- Start date: 	24.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE enter_for
	(
		p_registration_pk	in registration.registration_pk%type	default NULL,
		p_confirm		in varchar2				default NULL
	)
IS
BEGIN
DECLARE


	v_heading		calendar.heading%type			default NULL;
	v_event_date		calendar.event_date%type		default NULL;
	v_start_date		registration.start_date%type		default NULL;
	v_end_date		registration.end_date%type		default NULL;
	v_calendar_pk		calendar.calendar_pk%type		default NULL;
	v_user_pk		usr.user_pk%type			default NULL;
	v_check			number					default NULL;

        v_file                  utl_file.file_type              	default NULL;
        v_number                number                          	default NULL;
        v_message               varchar2(4000)                  	default NULL;
        v_email_dir             varchar2(100)                   	default NULL;

	v_email			organization.email%type			default NULL;
BEGIN
if ( login.timeout('register.enter_for?p_registration_pk='||p_registration_pk)>0 ) then

	if (p_registration_pk IS NOT NULL) then
		SELECT	heading, event_date, start_date, reg.end_date, calendar_pk
		INTO	v_heading, v_event_date, v_start_date, v_end_date, v_calendar_pk
		FROM	calendar, registration reg
		WHERE	event_date > sysdate
		AND	calendar_pk = calendar_fk
		AND	registration_pk = p_registration_pk;
	else
		get.error_page(1, 'no registration_pk');
		return;
	end if;


	if (v_start_date > sysdate OR v_end_date < sysdate) then
		get.error_page(1, 'out of date', NULL, 'cal_util.show_calendar?p_calendar_pk='||v_calendar_pk);
		return;
	end if;

	if (p_confirm IS NULL) then
		html.b_page;
		html.b_box(get.txt('enter_for') ||' '''||v_heading||''' ('||v_event_date||')', '100%', 'register.enter_for');
		html.b_table;

		html.b_form('register.enter_for');
		htp.p('<input type="hidden" name="p_registration_pk" value="'||p_registration_pk||'">');
		htp.p('<input type="hidden" name="p_confirm" value="enter">');
		htp.p('<tr><td colspan="2">'|| get.txt('enter_for_desc') ||'</td></tr>');
		htp.p('<tr><td colspan="2">&nbsp;</td></tr>');
		htp.p('<tr><td width="20%">'|| get.txt('your_name') ||':</td><td>'|| get.uname ||'</td></tr>');
		htp.p('<tr><td width="20%">'|| get.txt('email_address') ||':</td><td>'|| get.email ||'</td></tr>');
		htp.p('<tr><td colspan="2">&nbsp;</td></tr>');
		htp.p('<tr><td align="right" colspan="2">'); html.submit_link( get.txt('enter_the_event') ); htp.p('</td></tr>');
		html.e_form;

		htp.p('<tr><td align="right" colspan="2">'); html.back( get.txt('go_back_without_entering') ); htp.p('</td></tr>');

		html.e_table;
		html.e_box;
		html.e_page;
	elsif (p_confirm = 'enter' AND p_registration_pk IS NOT NULL) then

		v_user_pk := get.uid;

		SELECT	count(*)
		INTO	v_check
		FROM	user_reg
		WHERE	user_fk = v_user_pk
		AND	registration_fk = p_registration_pk;

		if (v_check = 0) then
			INSERT INTO user_reg
			(user_fk, registration_fk, registration_date, reg_status_fk)
			values(v_user_pk, p_registration_pk, sysdate, NULL);
			commit;

			-- sender event. ut en beskjed om ny p�melding
			SELECT	count(*)
			INTO	v_check
			FROM	registration
			WHERE	registration_pk = p_registration_pk
			AND	notification = 1;

			if ( v_check = 1) then

				-- henter ut org. sin email adresse
				SELECT	email
				INTO	v_email
				FROM	registration r, calendar c, organization o
				WHERE	r.calendar_fk = c.calendar_pk
				AND	c.organization_fk = o.organization_pk
				AND	r.registration_pk = p_registration_pk;

	                        -- --------------------
	                        -- write notification email
	                        -- --------------------
	                        v_message 	:= get.txt('email_reg_notification');
	                        v_email_dir     := get.value('email_dir');
	                        v_message 	:= REPLACE (v_message, '[email]' , get.email);
	                        v_message 	:= REPLACE (v_message, '[event]' , v_heading||' ('||v_event_date||')');
	                        v_message 	:= REPLACE (v_message, '[link]' , 'register.adm_list?p_registration_pk='||p_registration_pk||'&p_type=view');

	                        SELECT  email_script_seq.NEXTVAL
	                        INTO    v_number
	                        FROM    DUAL;

	                        v_file := utl_file.fopen( v_email_dir, v_number||'_message.txt' ,'w');
	                        utl_file.put_line(v_file, v_message );
	                        utl_file.fclose(v_file);

	                        v_file := utl_file.fopen( v_email_dir, v_number||'_email-list.txt', 'w');
	                        utl_file.put_line(v_file, v_email);
	                        utl_file.fclose(v_file);
	                        -- ------------------
	                        -- end writing email
	                        -- ------------------
			end if;

		end if;

		html.b_page(NULL, 4, 'cal_util.show_calendar?p_calendar_pk='||v_calendar_pk);
		htp.p( get.txt('you_have_entered_the_event') );
		html.e_page;
	end if;

end if;
EXCEPTION WHEN NO_DATA_FOUND THEN
	htp.p( get.msg('wrong values') );
END;
END enter_for;





---------------------------------------------------------
-- Name: 	my_entries
-- Type: 	procedure
-- What: 	list out my own entries
-- Author:  	Frode Klevstul
-- Start date: 	24.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE my_entries
IS
BEGIN
DECLARE

	CURSOR	select_all (v_user_pk in usr.user_pk%type) IS
	SELECT	registration_pk, heading, event_date, registration_date, reg_status_fk, email, name, organization_pk
	FROM	user_reg, registration, calendar, organization
	WHERE	calendar_pk = calendar_fk
	AND	registration_fk = registration_pk
	AND	user_fk = v_user_pk
	AND	organization_fk = organization_pk
	AND	event_date > (sysdate - 30)
	ORDER BY event_date desc;

	v_registration_pk	registration.registration_pk%type	default NULL;
	v_heading		calendar.heading%type			default NULL;
	v_event_date		calendar.event_date%type		default NULL;
	v_registration_date	user_reg.registration_date%type		default NULL;
	v_name_sg_fk		reg_status.name_sg_fk%type		default NULL;
	v_reg_status_fk		user_reg.reg_status_fk%type		default NULL;
	v_email			organization.email%type			default NULL;
	v_name			organization.name%type			default NULL;
	v_organization_pk	organization.organization_pk%type	default NULL;

	v_user_pk		usr.user_pk%type			default NULL;
	v_status		varchar2(100)				default NULL;
	v_subject		varchar2(100)				default NULL;

BEGIN
if ( login.timeout('register.my_entries')>0 ) then

	v_user_pk := get.uid;

	html.b_page(NULL, NULL, NULL, NULL, 3);
	html.my_menu;
	html.b_box( get.txt('my_reg_entries'), '100%', 'register.my_entries');
	html.b_table;
	htp.p('<tr><td colspan="5">'|| get.txt('my_reg_entries_desc') ||'</td></tr>');
	htp.p('<tr><td colspan="5">&nbsp;</td></tr>');
	htp.p('
		<tr bgcolor="'|| get.value( 'c_'|| get.serv_name ||'_mmb' ) ||'">
		<td><b>'|| get.txt('source') ||':</b></td>
		<td><b>'|| get.txt('org_email') ||':</b></td>
		<td><b>'|| get.txt('cal_heading') ||':</b></td>
		<td><b>'|| get.txt('registration_date') ||':</b></td>
		<td><b>'|| get.txt('reg_status') ||':</b></td>
		</tr>
	');

	open select_all(v_user_pk);
	loop
		fetch select_all into v_registration_pk, v_heading, v_event_date, v_registration_date, v_reg_status_fk, v_email, v_name, v_organization_pk;
		exit when select_all%NOTFOUND;

		if (v_reg_status_fk IS NOT NULL) then
			SELECT	name_sg_fk
			INTO	v_name_sg_fk
			FROM	reg_status
			WHERE	reg_status_pk = v_reg_status_fk;

			v_status := get.text(v_name_sg_fk);
		else
			v_status := get.txt('reg_stat_not_given');
		end if;

		v_subject := v_heading ||' ('|| to_char(v_event_date, get.txt('date_year'))||')';

		htp.p('
			<tr>
			<td><a href="'|| get.olink(v_organization_pk, 'org_page.main?p_organization_pk='||v_organization_pk) ||'">'|| v_name ||'</a></td>
			<td><a href="tip.mailform?p_contact_email='||v_email||'&p_url=register.my_entries&p_from='||replace(get.uname, ' ', '+')||'&p_email_address='||get.email||'&p_subject='||replace(v_subject, ' ', '+')||'">'|| v_email ||'</a></td>
			<td>'|| v_subject ||'</td>
			<td>'|| to_char(v_registration_date, get.txt('date_long') ) ||'</td>
			<td>'|| v_status ||'</td>
			</tr>
		');

	end loop;
	close select_all;

	html.e_table;
	html.e_box;
	html.e_page;

end if;
END;
END my_entries;



/* --------------------------------------------- */
/* TABELLER FOR ADMINISTRASJON AV REG_STATUS 	 */
/* --------------------------------------------- */


---------------------------------------------------------
-- Name: 	list_reg_status
-- Type: 	procedure
-- What: 	procedure to list all reg_status
-- Author:  	Frode Klevstul
-- Start date: 	24.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE list_reg_status
IS
BEGIN
DECLARE

	CURSOR 	select_all IS
	SELECT 	reg_status_pk, name_sg_fk, description
	FROM	reg_status;

	v_reg_status_pk		reg_status.reg_status_pk%type		default NULL;
	v_name_sg_fk		reg_status.name_sg_fk%type		default NULL;
	v_description		reg_status.description%type		default NULL;

BEGIN
if ( login.timeout('register.list_reg_status')>0 AND login.right('register')>0 ) then

	html.b_adm_page('register');
	html.b_box('administrate "reg_status"');
	html.b_table;

	htp.p('<tr><td><b>pk:</b></td><td><b>name:</b></td><td><b>description:</b></td><td>&nbsp;</td><td>&nbsp;</td></tr>');

	open select_all;
	loop
		fetch select_all into v_reg_status_pk, v_name_sg_fk, v_description;
		exit when select_all%NOTFOUND;

		htp.p('<tr><td valign="top">'|| v_reg_status_pk ||'</td><td valign="top"><a href="multilang.list_all?p_name='||get.sg_name(v_name_sg_fk)||'">'|| get.sg_name(v_name_sg_fk) ||'</a></td><td valign="top">'|| v_description ||'</td>');
		htp.p('<td align="center">');
		html.button_link('update', 'register.update_reg_status?p_reg_status_pk='||v_reg_status_pk);
		htp.p('</td><td>');

		-- status kan ikke slettes uten � forandre register.adm_list, derfor er linjen under kommentert bort
		-- html.button_link('delete', 'register.delete_reg_status?p_reg_status_pk='||v_reg_status_pk);
		htp.p('&nbsp;');

		htp.p('</td></tr>');

	end loop;
	close select_all;
	html.e_table;

	-- status kan ikke uten videre legges til uten � forandre register.adm_list, derfor er bolken under kommentert bort
/*
	html.b_form('register.insert_reg_status');
	html.b_table('60%');
	htp.p('
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr bgcolor="#eeeeee"><td colspan="2" align="center"><b>add new entry in "reg_status"</b></td></tr>
		<tr><td>name:</td><td><input type="Text" name="p_name" size="40" maxlength="200"></td></tr>
		<tr><td>description:</td><td><input type="Text" name="p_description" size="50" maxlength="150"></td></tr>
		<tr><td colspan="2" align="right">'); html.submit_link('insert'); htp.p('
		</td></tr>
	');
	html.e_table;
	html.e_form;
*/


	html.e_box;
	html.e_adm_page;

end if;
END;
END list_reg_status;




---------------------------------------------------------
-- Name: 	insert_reg_status
-- Type: 	procedure
-- What: 	inserts the entry into the database
-- Author:  	Frode Klevstul
-- Start date: 	24.10.2000
---------------------------------------------------------
PROCEDURE insert_reg_status
		(
			p_reg_status_pk		in reg_status.reg_status_pk%type	default NULL,
			p_name			in string_group.name%type		default NULL,
			p_description		in reg_status.description%type	 	default NULL
                )
IS
BEGIN
DECLARE

	v_reg_status_pk		reg_status.reg_status_pk%type			default NULL;
	v_name			string_group.name%type				default NULL;
	v_description		reg_status.description%type			default NULL;

	v_string_group_pk	string_group.string_group_pk%type	default NULL;
	v_check			number					default NULL;

BEGIN
if ( login.timeout('register.list_reg_status')>0 AND login.right('register')>0 ) then


	-- sjekker om det allerede finnes en forekomst med dette navnet
	if ( p_reg_status_pk IS NOT NULL ) then
		SELECT 	count(*) INTO v_check
		FROM 	string_group, reg_status
		WHERE 	name = p_name
		AND	name_sg_fk = string_group_pk
		AND 	reg_status_pk <> p_reg_status_pk;
	else
		SELECT 	count(*) INTO v_check
		FROM 	string_group
		WHERE 	name = p_name;
	end if;

	if (v_check > 0) then
		get.error_page(1, 'there already exists an entry with name = <b>'||p_name||'</b>');
		return;
	else
	begin
		v_description := p_description;
		v_description := html.rem_tag(v_description);

		-- henter ut neste nummer i sekvensen til tabellen
		if (p_reg_status_pk is NULL AND p_name IS NOT NULL) then
			SELECT	reg_status_seq.NEXTVAL
			INTO	v_reg_status_pk
			FROM	dual;

			SELECT 	string_group_seq.NEXTVAL
			INTO	v_string_group_pk
			FROM 	dual;

			-- legger inn i databasen
			INSERT INTO string_group
			(string_group_pk, name, description)
			VALUES (v_string_group_pk, p_name, '[entry inserted by register package]');
			commit;

			INSERT INTO reg_status
			(reg_status_pk, name_sg_fk, description)
			VALUES (v_reg_status_pk, v_string_group_pk, p_description);
			commit;
		elsif (p_name IS NOT NULL) then
		begin
			UPDATE	string_group
			SET	name = p_name
			WHERE	string_group_pk =
				(
					SELECT	name_sg_fk
					FROM	reg_status
					WHERE	reg_status_pk = p_reg_status_pk
				);
			commit;

			UPDATE	reg_status
			SET	description = p_description
			WHERE	reg_status_pk = p_reg_status_pk;
			commit;

		end;
		end if;
	end;
	end if;

	html.jump_to('register.list_reg_status');


end if;
END;
END insert_reg_status;




---------------------------------------------------------
-- Name: 	delete_reg_status
-- Type: 	procedure
-- What: 	deletes an entry in the database
-- Author:  	Frode Klevstul
-- Start date: 	24.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE delete_reg_status
		(
			p_reg_status_pk 	in reg_status.reg_status_pk%type		default NULL,
			p_confirm		in varchar2					default NULL
                )
IS
BEGIN
DECLARE

	v_name_sg_fk	reg_status.name_sg_fk%type	default NULL;

BEGIN
if ( login.timeout('register.list_reg_status')>0 AND login.right('register')>0 ) then

	if (p_confirm IS NULL) then

		SELECT	name_sg_fk INTO v_name_sg_fk
		FROM	reg_status
		WHERE	reg_status_pk = p_reg_status_pk;

		html.b_adm_page('register');
		html.b_box('delete reg_status');
		html.b_form('register.delete_reg_status');
		html.b_table;

		htp.p('
			<input type="hidden" name="p_reg_status_pk" value="'|| p_reg_status_pk ||'">
			<tr><td colspan="2">Are you sure you want to delete the entry <b>'|| get.sg_name(v_name_sg_fk) ||'</b> with pk <b>'||p_reg_status_pk||'</b>?</td></tr>
			<tr><td><input type="submit" name="p_confirm" value="yes"></td><td align="right"><input type="submit" name="p_confirm" value="no"></td></tr>
			</form>
		');

		html.e_table;
		html.e_form;
		html.e_box;
		html.e_adm_page;

	else

		if (p_confirm = 'yes' AND p_reg_status_pk IS NOT NULL) then
			DELETE FROM reg_status
			WHERE reg_status_pk = p_reg_status_pk;
			commit;
		end if;

		html.jump_to('register.list_reg_status');

	end if;

end if;
END;
END delete_reg_status;




---------------------------------------------------------
-- Name: 	update_reg_status
-- Type: 	procedure
-- What: 	updates an entry
-- Author:  	Frode Klevstul
-- Start date: 	24.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE update_reg_status
		(
			p_reg_status_pk 	in reg_status.reg_status_pk%type	default NULL
                )
IS
BEGIN
DECLARE

	v_name_sg_fk	reg_status.reg_status_pk%type		default NULL;
	v_description	reg_status.description%type		default NULL;

BEGIN
if ( login.timeout('register.list_reg_status')>0 AND login.right('register')>0 ) then

	SELECT 	name_sg_fk, description INTO v_name_sg_fk, v_description
	FROM 	reg_status
	WHERE 	reg_status_pk = p_reg_status_pk;

	v_description := html.rem_tag(v_description);

	html.b_adm_page('register');
	html.b_box('update reg_status');
	html.b_form('register.insert_reg_status');
	html.b_table;

	htp.p('
		<tr><td colspan="2">&nbsp;</td></tr>
		<input type="hidden" name="p_reg_status_pk" value="'||p_reg_status_pk||'">
		<tr bgcolor="#eeeeee"><td colspan="2" align="center"><b>update entry in "reg_status"</b></td></tr>
		<tr><td>name:</td><td><input type="Text" name="p_name" size="30" maxlength="50" value="'||get.sg_name(v_name_sg_fk)||'"></td></tr>
		<tr><td>description:</td><td><input type="Text" name="p_description" size="50" maxlength="50" value="'||v_description||'"></td></tr>
		<tr><td colspan="2" align="right">'); html.submit_link('update'); htp.p('
		</td></tr>
	');


	html.e_table;
	html.e_form;
	html.e_box;
	html.e_adm_page;


end if;
END;
END update_reg_status;




-- ++++++++++++++++++++++++++++++++++++++++++++++ --

END; -- slutter pakke kroppen
/
show errors;
