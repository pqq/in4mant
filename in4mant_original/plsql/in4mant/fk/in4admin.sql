set define off
PROMPT *** package: in4admin ***

CREATE OR REPLACE PACKAGE in4admin IS
        PROCEDURE startup;
        PROCEDURE main_page;
END;
/
CREATE OR REPLACE PACKAGE BODY in4admin IS
---------------------------------------------------------------------
-- Name: startup
-- Type: procedure
-- What: prosedyre for � starte pakken
-- Made: Frode Klevstul
-- Date: 25.07.00
-- Chng:
---------------------------------------------------------------------

PROCEDURE startup
IS
BEGIN

        main_page;

END startup;



---------------------------------------------------------------------
-- Name: main_page
-- Type: procedure
-- What: prints out the overview over all the adm packages/procedures
-- Made: Frode Klevstul
-- Date: 25.07.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE main_page
IS
BEGIN
DECLARE


BEGIN
if (login.timeout('in4admin.startup') > 0 AND login.right('in4admin.main_page')>0 ) then


        html.b_page_2;
        html.main_title('In4mant Admin Area');
        html.b_table;

        htp.p('
                <tr><td colspan="2" align="center">Welcome '|| get.uname ||'. </td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr><td colspan="2" align="center"><a href="login.logout">Logout</a></td></tr>

                <tr><td colspan="2">
                <font color="#ff0000">WARNING:</font><dd>Don''t use any tools if you''re not 100% sure how to use it.
                <dd>Wrong use might cause to a total destruction of the entire in4mant site.</td></tr>

                <!-- TIME REPORT -->
                <tr><td colspan="2"><HR size="2" noshade="noshade"></td></tr>
                <tr><td colspan="2" align="center"><b>Time Report</b></td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                        <td align="left">1. <a href="time_report.startup">Time report</a></td>
                        <td>The time report module</td>
                </tr>

                <!-- STATISTICS -->
                <tr><td colspan="2"><HR size="2" noshade="noshade"></td></tr>
                <tr><td colspan="2" align="center"><b>Show statistics:</b></td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                        <td align="left">1. <a href="stat.res_page_serv?p_service_pk='||get.serv||'">Statistics</a></td>
                        <td>Look at statistics.</td>
                </tr>

                <!-- ERROR ADMINISTRATION -->
                <tr><td colspan="2"><HR size="2" noshade="noshade"></td></tr>
                <tr><td colspan="2" align="center"><b>Error administration:</b></td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                        <td align="left">1. <a href="error_fb.register">Errors</a></td>
                        <td>Here you can administrate error messages from the web.</td>
                </tr>


                <!-- ORGANIZATION ADMINISTRATION -->
                <tr><td colspan="2"><HR size="2" noshade="noshade"></td></tr>
                <tr><td colspan="2" align="center"><b>organization administration:</b></td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                        <td align="left" width="25%">1. <a href="org_adm.accepted">Accept new organizations</a></td>
                        <td>When a new organization is registrated, it must be accepted before it will be available</td>
                </tr>
                        <td align="left">2. <a href="org_adm.edit_info_name">Edit info names</a></td>
                        <td>An organization can have more values stored, than just in organization table.</td>
                </tr>
                <tr>
                        <td align="left">3. <a href="org_adm.edit_document_type">Edit document types</a></td>
                        <td>Documents that is published can be of different types.</td>
                </tr>
                <tr>
                        <td align="left">4. <a href="org_list.startup">Organizations on lists</a></td>
                        <td>An organization can be one one or more lists.</td>
                </tr>
                <tr>
                        <td align="left">5. <a href="org_adm.delete_org">Delete organization</a></td>
                        <td>Delete an organization.</td>
                </tr>
                <tr>
                        <td align="left">6. <a href="org_adm.edit_membership">Edit membership</a></td>
                        <td>Edit membership.</td>
                </tr>
                <tr>
                        <td align="left">7. <a href="contact.email_org">E-mail organizations</a></td>
                        <td>Send out E-mail to one ore more organizations.</td>
                </tr>
                <tr>
                        <td align="left">8. <a href="up_pack.startup">username / password</a></td>
                        <td>Find username and password for organizations.</td>
                </tr>

                <!-- USER/SECURITY ADMINISTRATION -->
                <tr><td colspan="2"><HR size="2" noshade="noshade"></td></tr>
                <tr><td colspan="2" align="center"><b>user/security administration:</b></td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                        <td align="left">1. <a href="modu.startup">User rights on modules</a></td>
                        <td>Some modules (packages/procedures) might be blocked for ordinary users. "Admin-" and "Super-users" can get privileges here.</td>
                </tr>
                <tr>
                        <td align="left">2. <a href="user_adm.monitor">Monitor users</a></td>
                        <td>Monitor all users that is logged in right now.</td>
                </tr>
                <tr>
                        <td align="left">3. <a href="user_adm.del_usr">Delete users</a></td>
                        <td>Delete users from database.</td>
                </tr>
                <tr>
                        <td align="left">3. <a href="user_adm.inspect">Inspect users</a></td>
                        <td>View and change user data.</td>
                </tr>



                <!-- LIST ADMINISTRATION -->
                <tr><td colspan="2"><HR size="2" noshade="noshade"></td></tr>
                <tr><td colspan="2" align="center"><b>list administration:</b></td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                        <td align="left">1. <a href="list_adm.startup">Lists and list types</a></td>
                        <td>Here you can administrate list and list types on all services.</td>
                </tr>



                <!-- SERIVCE ADMINISTRATION -->
                <tr><td colspan="2"><HR size="2" noshade="noshade"></td></tr>
                <tr><td colspan="2" align="center"><b>service administration:</b></td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                        <td align="left">1. <a href="servic.startup">Services</a></td>
                        <td>Here you can administrate service and it''s languages.</td>
                </tr>



                <!-- GEOGRAPHY ADMINISTRATION -->
                <tr><td colspan="2"><HR size="2" noshade="noshade"></td></tr>
                <tr><td colspan="2" align="center"><b>geography administration:</b></td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                        <td align="left">1. <a href="geo.startup">Geography</a></td>
                        <td>Administrate the geography information.</td>
                </tr>



                <!-- LANGUAGE/MULITLANG ADMINISTRATION -->
                <tr><td colspan="2"><HR size="2" noshade="noshade"></td></tr>
                <tr><td colspan="2" align="center"><b>language/multi-language administration:</b></td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                        <td align="left">1. <a href="multilang.startup">Multi-language</a></td>
                        <td>Administrate all text''s on the entire in4mant site.</td>
                </tr>
                <tr>
                        <td align="left">2. <a href="lang.startup">Language</a></td>
                        <td>Administrate the languages that is available on the site.</td>
                </tr>



                <!-- PARAMETER ADMINISTRATION -->
                <tr><td colspan="2"><HR size="2" noshade="noshade"></td></tr>
                <tr><td colspan="2" align="center"><b>parameter administration:</b></td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                        <td align="left">1. <a href="param.startup">Parameter</a></td>
                        <td>Administrate all parameters that is used on the in4mant      site.</td>
                </tr>



                <!-- HTML ADMINISTRATION -->
                <tr><td colspan="2"><HR size="2" noshade="noshade"></td></tr>
                <tr><td colspan="2" align="center"><b>html administration:</b></td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                        <td align="left">1. <a href="htmlcode.startup">HTML code</a></td>
                        <td>Administrate all html code on the entire site.</td>
                </tr>



                <!-- POLL ADMINISTRATION -->
                <tr><td colspan="2"><HR size="2" noshade="noshade"></td></tr>
                <tr><td colspan="2" align="center"><b>poll administration:</b></td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                        <td align="left">1. <a href="poll_util.edit_poll_type">Edit poll type</a></td>
                        <td>Administrate poll types.</td>
                </tr>
                <tr>
                        <td align="left">2. <a href="poll_util.edit_poll">Edit poll</a></td>
                        <td>Administrate polls.</td>
                </tr>
                <tr>
                        <td align="left">3. <a href="poll_util.accepted">Accept polls</a></td>
                        <td>Accept polls.</td>
                </tr>

                <!-- LINE OF THE LOAD ADMINISTRATION -->
                <tr><td colspan="2"><HR size="2" noshade="noshade"></td></tr>
                <tr><td colspan="2" align="center"><b>"line of the load" administration:</b></td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                        <td align="left">1. <a href="lines.startup">Edit line-of-the-load</a></td>
                        <td>Administrate line type, lines, and lines on service.</td>
                </tr>


                <!-- FORA ADMINISTRATION -->
                <tr><td colspan="2"><HR size="2" noshade="noshade"></td></tr>
                <tr><td colspan="2" align="center"><b>Fora administration:</b></td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                        <td align="left">1. <a href="fora_util.edit_fora_type">Edit fora types</a></td>
                        <td>Administrate fora types.</td>
                </tr>



                <!-- BULLETIN BOARD ADMINISTRATION -->
                <tr><td colspan="2"><HR size="2" noshade="noshade"></td></tr>
                <tr><td colspan="2" align="center"><b>Bulletin board administration:</b></td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                        <td align="left">1. <a href="bboard_adm.list_item_type">Edit item types</a></td>
                        <td>Administrate item types.</td>
                </tr>
                <tr>
                        <td align="left">2. <a href="bboard_adm.list_trade_type">Edit trade types</a></td>
                        <td>Administrate trade types.</td>
                </tr>
                <tr>
                        <td align="left">3. <a href="bboard_adm.list_payment_method">Edit payment methods</a></td>
                        <td>Administrate payment methods.</td>
                </tr>

                <!-- REGISTER ADMINISTRATION -->
                <tr><td colspan="2"><HR size="2" noshade="noshade"></td></tr>
                <tr><td colspan="2" align="center"><b>Register administration:</b></td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                        <td align="left">1. <a href="register.list_reg_status">Edit reg status</a></td>
                        <td>Administrate registration status.</td>
                </tr>

                <!-- PAGE ADMINISTRATION -->
                <tr><td colspan="2"><HR size="2" noshade="noshade"></td></tr>
                <tr><td colspan="2" align="center"><b>Page administration:</b></td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                        <td align="left">1. <a href="page_adm.edit_page_type">Edit page type</a></td>
                        <td>Here you can administrate page types.</td>
                </tr>

                <!-- EMAIL TEXT ADMINISTRATION -->
                <tr><td colspan="2"><HR size="2" noshade="noshade"></td></tr>
                <tr><td colspan="2" align="center"><b>Email text administration:</b></td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                        <td align="left">1. <a href="email_util.edit_email_text">Edit email text</a></td>
                        <td>Administrate email texts.</td>
                </tr>
                <tr>
                        <td align="left">2. <a href="email_util.edit_et_type">Edit email text types</a></td>
                        <td>Administrate email text types.</td>
                </tr>
                <tr>
                        <td align="left">3. <a href="email_util.edit_et_relation">Edit email text relation</a></td>
                        <td>Administrate email text relations.</td>
                </tr>


                <!-- MENU ADMINISTRATION -->
                <tr><td colspan="2"><HR size="2" noshade="noshade"></td></tr>
                <tr><td colspan="2" align="center"><b>Menu administration:</b></td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                        <td align="left">1. <a href="menu_i4.menu_type_adm">Menu type</a></td>
                        <td>Administrate menu types (drink menu, food menu etc.)</td>
                </tr>
                <tr>
                        <td align="left">2. <a href="menu_i4.serving_period_adm">Serving Period</a></td>
                        <td>Administrate serving periods (breakfast, dinner etc.)</td>
                </tr>
                <tr>
                        <td align="left">3. <a href="menu_i4.menu_object_type_adm">Menu object type</a></td>
                        <td>Administrate object types (salad, beer, redwine etc.)</td>
                </tr>
                <tr>
                        <td align="left">4. <a href="menu_i4.menu_serving_type_adm">Menu serving type</a></td>
                        <td>Administrate serving types (appetizer, dessert, kidsmenu, starter etc)</td>
                </tr>
                <tr>
                        <td align="left">5. <a href="menu_i4.compare_list_adm">Compare list</a></td>
                        <td>Administrate compare lists.</td>
                </tr>

                <!-- DRINK ADMINISTRATION -->
                <tr><td colspan="2"><HR size="2" noshade="noshade"></td></tr>
                <tr><td colspan="2" align="center"><b>Drink administration:</b></td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                        <td align="left">1. <a href="drink_util.edit_drink">Edit drink</a></td>
                        <td>Administrate drinks</td>
                </tr>
                <tr>
                        <td align="left">2. <a href="drink_util.edit_ingredient">Edit ingredients</a></td>
                        <td>Administrate drink ingredients</td>
                </tr>
                <tr>
                        <td align="left">3. <a href="drink_util.edit_ing_type">Edit ingredient type</a></td>
                        <td>Administrate drink ingredient types</td>
                </tr>
                <tr>
                        <td align="left">4. <a href="drink_util.edit_category">Edit category</a></td>
                        <td>Administrate drink categories</td>
                </tr>
                <tr>
                        <td align="left">5. <a href="drink_util.edit_glass">Edit glass</a></td>
                        <td>Administrate drink glasses</td>
                </tr>
                <tr>
                        <td align="left">6. <a href="drink_util.edit_measurement">Edit measurement</a></td>
                        <td>Administrate drink measurements</td>
                </tr>

                <tr><td colspan="2"><HR size="2" noshade="noshade"></td></tr>

        ');

        html.e_table;
        html.e_page_2;

end if;
END;
END main_page;





-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/
show errors;
