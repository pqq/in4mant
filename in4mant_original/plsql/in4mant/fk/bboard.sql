set define off
PROMPT *** package: BBOARD ***

CREATE OR REPLACE PACKAGE bboard IS

	PROCEDURE startup;
	PROCEDURE start_page;
	PROCEDURE list_item_type;
	PROCEDURE info
		(
			p_type			in number					default NULL
		);
	PROCEDURE left_pillar
		(
			p_trade_type_pk		in trade_type.trade_type_pk%type	default NULL,
			p_item_type_pk		in item_type.item_type_pk%type		default NULL,
			p_string		in varchar2				default NULL
		);
	PROCEDURE insert_item
		(
			p_item_pk		in item.item_pk%type				default NULL,
			p_trade_type_pk		in trade_type.trade_type_pk%type		default NULL,
			p_item_type_pk		in item_type.item_type_pk%type			default NULL,
			p_title			in item_text.title%type				default NULL,
			p_body			in item_text.body%type				default NULL,
			p_price			in item_text.price%type				default NULL,
			p_other			in item_text.other%type				default NULL,
			p_no_items		in varchar2					default NULL,
			p_url			in item.url%type				default NULL,
			p_price_type		in item.price_type%type				default NULL,
			p_freight_charge	in item.freight_charge%type			default NULL,
			p_payment_method_pk	in payment_method.payment_method_pk%type	default NULL,
			p_publish_date		in varchar2					default NULL,
			p_end_date		in varchar2					default NULL,
			p_contact_name		in item.contact_name%type			default NULL,
			p_contact_email		in item.contact_email%type			default NULL,
			p_contact_phone		in item.contact_phone%type			default NULL,
			p_geography_pk		in geography.geography_pk%type			default NULL,
			p_geo_name		in varchar2					default NULL,
			p_preview		in varchar2					default NULL
		);
	PROCEDURE select_item_type
		(
			p_item_type_pk		in item_type.item_type_pk%type			default NULL,
			p_type			in varchar2					default NULL
		);
	PROCEDURE select_trade_type
		(
			p_trade_type_pk		in trade_type.trade_type_pk%type		default NULL,
			p_type			in varchar2					default NULL
		);
	PROCEDURE select_payment
		(
			p_payment_method_pk	payment_method.payment_method_pk%type		default NULL
		);
	PROCEDURE own_items
		(
			p_item_pk		in item.item_pk%type				default NULL,
			p_type			in varchar2					default NULL,
			p_confirm		in varchar2					default NULL
		);
	PROCEDURE view_item
		(
			p_item_pk		in item.item_pk%type				default NULL,
			p_type			in varchar2					default NULL
		);
	PROCEDURE last_items
		(
			p_trade_type_pk		in trade_type.trade_type_pk%type		default NULL,
			p_item_type_pk		in item_type.item_type_pk%type			default NULL,
			p_type			in varchar2					default NULL
		);
	PROCEDURE trade_type_header
		(
			p_trade_type_pk		in trade_type.trade_type_pk%type	default NULL,
			p_item_type_pk		in item_type.item_type_pk%type		default NULL
		);
	PROCEDURE search
		(
			p_trade_type_pk		in trade_type.trade_type_pk%type		default NULL,
			p_item_type_pk		in item_type.item_type_pk%type			default NULL,
			p_string		in varchar2					default NULL,
			p_type			in varchar2					default NULL
		);

END;
/
show errors;

CREATE OR REPLACE PACKAGE BODY bboard IS


---------------------------------------------------------
-- Name: 	startup
-- Type: 	procedure
-- What: 	start procedure, to run the package
-- Author:  	Frode Klevstul
-- Start date: 	08.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

	start_page;

END startup;


---------------------------------------------------------
-- Name: 	start_page
-- Type: 	procedure
-- What: 	start_page for this module
-- Author:  	Frode Klevstul
-- Start date: 	08.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE start_page
IS
BEGIN
DECLARE

	CURSOR	select_all IS
	SELECT	trade_type_pk
	FROM	trade_type;

	v_trade_type_pk		trade_type.trade_type_pk%type	default NULL;

BEGIN

	-- legger inn statistikk
	stat.reg(NULL,NULL,NULL,NULL,NULL,NULL,25);

	html.b_page;
	html.empty_menu;
	htp.p('<table cellspacing="0">');

	-- --------------------------
	-- skriver ut venstre stolpe
	-- --------------------------
	htp.p('<tr><td valign="top" width="10%">');
	left_pillar;
	htp.p('</td>');


	-- --------------------------
	-- skriver ut h�yre del
	-- --------------------------
	htp.p(' <td valign="top" width="90%">
		<table border="0" width="100%" cellspacing="5">
		<tr><td>');
		info(3);
		htp.p('</td></tr>
	');


	-- g�r igjennom de forskjellige "trade_types" vi har og skriver ut de siste
	open select_all;
	loop
		fetch select_all into v_trade_type_pk;
		exit when select_all%NOTFOUND;

		htp.p('<tr><td>');
		bboard.last_items(v_trade_type_pk, NULL);
		htp.p('</td></tr>');

	end loop;
	close select_all;

	-- skriver ut de mest sette produktene
	htp.p('<tr><td>');
	bboard.last_items;
	htp.p('</td></tr>');

	htp.p('
		</td></tr>
		</table>
			</td>
		</tr>
	');

	html.e_table;
	html.e_page;

END;
END start_page;



---------------------------------------------------------
-- Name: 	left_pillar
-- Type: 	procedure
-- What: 	writes out the left pillar
-- Author:  	Frode Klevstul
-- Start date: 	08.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE left_pillar
	(
		p_trade_type_pk		in trade_type.trade_type_pk%type	default NULL,
		p_item_type_pk		in item_type.item_type_pk%type		default NULL,
		p_string		in varchar2				default NULL
	)
IS
BEGIN

	htp.p('<table cellspacing="0" cellpadding="10">');
	htp.p(' <tr><td>');
	bboard.search(p_trade_type_pk, p_item_type_pk, p_string);	-- s�k boks
	htp.p(' </td></tr>
		<tr><td>');
	list_item_type;							-- kategorier
	htp.p(' </td></tr>
		<tr><td>');
	info(2);							-- informasjon
	htp.p(' </td></tr>
		<tr><td>
	');
--	info(1);							-- informasjon
	htp.p(' </td></tr>');
	html.e_table;

END left_pillar;


---------------------------------------------------------
-- Name: 	list_item_type
-- Type: 	procedure
-- What: 	list out all different item types on a service
-- Author:  	Frode Klevstul
-- Start date: 	08.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE list_item_type
IS
BEGIN
DECLARE

	CURSOR	select_all IS
	SELECT	item_type_pk, string
	FROM	item_type it, item_type_on_service itos, string_group sg, groups g, strng s
	WHERE	itos.item_type_fk = it.item_type_pk
	AND	(
		itos.service_fk = get.serv OR
		itos.service_fk = 0
		)
	AND	it.name_sg_fk = sg.string_group_pk
	AND	sg.string_group_pk = g.string_group_fk
	AND	g.string_fk = string_pk
	AND	language_fk = get.lan
	ORDER BY string
	;

	v_item_type_pk		item_type.item_type_pk%type	default NULL;
	v_string		strng.string%type		default NULL;
	v_check			number				default NULL;

BEGIN

	html.b_box_2( get.txt('item_types'), '100%', 'bboard.list_item_type' );
	html.b_table;


	open select_all;
	loop
		fetch select_all into v_item_type_pk, v_string;
		exit when select_all%NOTFOUND;

		SELECT	count(*) INTO v_check
		FROM	item
		WHERE	item_type_fk = v_item_type_pk
		AND	publish_date < sysdate
		AND	end_date > sysdate;

		htp.p('<tr><td><a href="bboard.last_items?p_item_type_pk='||v_item_type_pk||'&p_type=layout">'|| v_string ||'</a> ('||v_check||')</td></tr>');

	end loop;
	close select_all;


	html.e_table;
	html.e_box_2;

END;
END list_item_type;




---------------------------------------------------------
-- Name: 	info
-- Type: 	procedure
-- What: 	writes out information
-- Author:  	Frode Klevstul
-- Start date: 	08.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE info
	(
		p_type	in number	default NULL
	)
IS
BEGIN

	if (p_type = 1) then
		html.b_box_2( get.txt('important_bboard'), '100%', 'bboard.info(1)' );
		html.b_table;
		htp.p('<tr><td> '|| get.txt('how_to_bboard') ||' </td></tr>');
		htp.p('<tr><td> '|| get.txt('licence_agreement_bboard') ||' </td></tr>');
		htp.p('<tr><td> '|| get.txt('responsibility_bboard') ||' </td></tr>');
		html.e_table;
		html.e_box_2;
	elsif (p_type = 2) then
		html.b_box_2( get.txt('other_bboard'), '100%', 'bboard.info(2)' );
		html.b_table;
		htp.p('<tr><td> <a href="bboard.startup">'|| get.txt('bboard_main_page') ||'</a></td></tr>');
		htp.p('<tr><td> <a href="bboard.insert_item">'|| get.txt('insert_item') ||'</a></td></tr>');
		htp.p('<tr><td> <a href="bboard.own_items">'|| get.txt('own_items') ||' </a></td></tr>');
		html.e_table;
		html.e_box_2;
	elsif (p_type = 3) then
		html.b_box( get.txt('info_bboard'), '100%', 'bboard.info(1)' );
		html.b_table;
		htp.p('<tr><td> '|| get.txt('info_bboard_desc') ||' </td></tr>');
		html.e_table;
		html.e_box;
	end if;

END info;



---------------------------------------------------------
-- Name: 	insert_item
-- Type: 	procedure
-- What: 	writes out a form for inserting items + inserts into db
-- Author:  	Frode Klevstul
-- Start date: 	08.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE insert_item
	(
		p_item_pk		in item.item_pk%type				default NULL,
		p_trade_type_pk		in trade_type.trade_type_pk%type		default NULL,
		p_item_type_pk		in item_type.item_type_pk%type			default NULL,
		p_title			in item_text.title%type				default NULL,
		p_body			in item_text.body%type				default NULL,
		p_price			in item_text.price%type				default NULL,
		p_other			in item_text.other%type				default NULL,
		p_no_items		in varchar2					default NULL,
		p_url			in item.url%type				default NULL,
		p_price_type		in item.price_type%type				default NULL,
		p_freight_charge	in item.freight_charge%type			default NULL,
		p_payment_method_pk	in payment_method.payment_method_pk%type	default NULL,
		p_publish_date		in varchar2					default NULL,
		p_end_date		in varchar2					default NULL,
		p_contact_name		in item.contact_name%type			default NULL,
		p_contact_email		in item.contact_email%type			default NULL,
		p_contact_phone		in item.contact_phone%type			default NULL,
		p_geography_pk		in geography.geography_pk%type			default NULL,
		p_geo_name		in varchar2					default NULL,
		p_preview		in varchar2					default NULL
	)
IS
BEGIN
DECLARE

	v_user_pk		usr.user_pk%type		default NULL;
	v_email			user_details.email%type		default NULL;
	v_first_name		user_details.first_name%type	default NULL;
	v_last_name		user_details.last_name%type	default NULL;
	v_phone_number		user_details.phone_number%type	default NULL;
	v_no_items		number				default NULL;

	v_item_pk		item.item_pk%type		default NULL;
	v_item_text_pk		item_text.item_text_pk%type	default NULL;
	v_url			item.url%type			default NULL;

BEGIN
if ( login.timeout('bboard.insert_item')>0 ) then

	v_user_pk := get.uid;

	SELECT 	email, first_name, last_name, phone_number
	INTO	v_email, v_first_name, v_last_name, v_phone_number
	FROM	user_details
	WHERE	user_fk = v_user_pk;

	html.b_page;
	htp.p('<table cellspacing="0">');

	-- --------------------------
	-- skriver ut venstre stolpe
	-- --------------------------
	htp.p('<tr><td valign="top" width="10%">');
	left_pillar;
	htp.p('</td><td valign="top">');


	-- ------------------------------------------
	-- skriver ut h�yre stolpe (hoved innhold)
	-- ------------------------------------------

	-- --------
	-- preview
	-- --------
	if (p_preview = 'yes') then

	        if ( p_no_items IS NOT NULL ) then
	        	-- ^ 		: Matches newline or the beginning of the target
	        	-- \d{1,}	: Matches digits, at least 1 occurences
	        	-- $ 		: Matches newline or the end of the target
	                if ( owa_pattern.match(p_no_items, '^\d{1,}$') ) then
	                        v_no_items    := to_number(p_no_items);
	                end if;
	        end if;

		html.b_form('bboard.insert_item');
		htp.p('
			<input type="hidden" name="p_item_pk" value="'||p_item_pk||'">
			<input type="hidden" name="p_trade_type_pk" value="'|| p_trade_type_pk ||'">
			<input type="hidden" name="p_item_type_pk" value="'|| p_item_type_pk ||'">
			<input type="hidden" name="p_title" value="'|| p_title ||'">
			<input type="hidden" name="p_body" value="'|| p_body ||'">
			<input type="hidden" name="p_price" value="'|| p_price ||'">
			<input type="hidden" name="p_other" value="'|| p_other ||'">
			<input type="hidden" name="p_no_items" value="'|| v_no_items ||'">
			<input type="hidden" name="p_url" value="'|| p_url ||'">
			<input type="hidden" name="p_price_type" value="'|| p_price_type ||'">
			<input type="hidden" name="p_freight_charge" value="'|| p_freight_charge ||'">
			<input type="hidden" name="p_payment_method_pk" value="'|| p_payment_method_pk ||'">
			<input type="hidden" name="p_publish_date" value="'|| p_publish_date ||'">
			<input type="hidden" name="p_end_date" value="'|| p_end_date ||'">
			<input type="hidden" name="p_contact_name" value="'|| p_contact_name ||'">
			<input type="hidden" name="p_contact_email" value="'|| p_contact_email ||'">
			<input type="hidden" name="p_contact_phone" value="'|| p_contact_phone ||'">
			<input type="hidden" name="p_geography_pk" value="'|| p_geography_pk ||'">
			<input type="hidden" name="p_preview" value="insert">
		');

		html.b_box( get.txt('insert_item_preview'), '100%', 'bboard.insert_item');
		html.b_table;
		htp.p('<tr><td colspan="2">'|| get.txt('bboard_item_preview_desc') ||'</td></tr>');
		htp.p('<tr><td colspan="2">&nbsp;</td></tr>');

		htp.p('<tr><td valign="top" width="40%">'|| get.txt('trade_type') ||':</td><td>'|| get.bb_text(p_trade_type_pk, 'trade_type') ||'</td></tr>');
		htp.p('<tr><td valign="top">'|| get.txt('item_type') ||':</td><td>'|| get.bb_text(p_item_type_pk, 'item_type') ||'</td></tr>');
		htp.p('<tr><td colspan="2">&nbsp;</td></tr>');

		htp.p('<tr><td valign="top">'|| get.txt('item_title') ||':</td><td>'|| p_title ||'</td></tr>');
		htp.p('<tr><td valign="top">'|| get.txt('item_body') ||':</td><td>'|| p_body ||'</td></tr>');
		htp.p('<tr><td valign="top">'|| get.txt('item_price') ||':</td><td>'|| p_price ||'</td></tr>');
		--htp.p('<tr><td valign="top">'|| get.txt('additional_information') ||':</td><td>'|| p_other ||'</td></tr>');

		v_url := p_url;
		-- ^ 		: Matches newline or the beginning of the target
		-- \w?		: Matches word characters (alphanumeric) [0-9, a-z, A-Z or _] 0 or 1 time
		if (NOT owa_pattern.match(v_url, '^http\w?://') AND v_url IS NOT NULL) then
			v_url := 'http://' || v_url;
		end if;
		htp.p('<tr><td valign="top">'|| get.txt('item_url') ||':</td><td><a href="'||v_url||'" target="new">'||v_url||'</a></td></tr>');
		htp.p('<tr><td valign="top">'|| get.txt('no_items') ||':</td><td>'|| v_no_items ||'</td></tr>');
		htp.p('<tr><td colspan="2">&nbsp;</td></tr>');

		htp.p('<tr><td valign="top">'|| get.txt('discuss_price') ||':</td><td>');
		if (p_price_type = 1) then
			htp.p( get.txt('yes') );
		elsif (p_price_type = 2) then
			htp.p( get.txt('no') );
		else
			htp.p('&nbsp;');
		end if;
		htp.p('</td></tr>');
		htp.p('<tr><td valign="top">'|| get.txt('freight_charge') ||':</td><td>');
		if (p_freight_charge = 1) then
			htp.p( get.txt('yes') );
		elsif (p_freight_charge = 2) then
			htp.p( get.txt('no') );
		else
			htp.p('&nbsp;');
		end if;
		htp.p('</td></tr>');
		htp.p('<tr><td valign="top">'|| get.txt('payment_method') ||':</td><td>'|| get.bb_text(p_payment_method_pk, 'payment_method') ||'</td></tr>');
		htp.p('<tr><td colspan="2">&nbsp;</td></tr>');

		htp.p('<tr><td>'|| get.txt('bboard_start_date') ||':</td><td>'|| p_publish_date ||'</td></tr>');
		htp.p('<tr><td>'|| get.txt('bboard_start_date') ||':</td><td>'|| p_end_date ||'</td></tr>');
		htp.p('<tr><td colspan="2">&nbsp;</td></tr>');

		htp.p('<tr><td valign="top">'|| get.txt('contact_name') ||':</td><td>'|| p_contact_name ||'</td></tr>');
		htp.p('<tr><td valign="top">'|| get.txt('contact_email') ||':</td><td>'|| p_contact_email ||'</td></tr>');
		htp.p('<tr><td valign="top">'|| get.txt('contact_phone') ||':</td><td>'|| p_contact_phone ||'</td></tr>');
		htp.p('<tr><td valign="top">'|| get.txt('contact_geography') ||':</td><td>'|| p_geo_name ||'</td></tr>');

		htp.p('<tr><td colspan="2" align="right">');
		if (p_item_pk IS NULL) then
			html.submit_link( get.txt('insert_ad') );
		else
			html.submit_link( get.txt('update_ad') );
		end if;
		htp.p('</td></tr>');
		html.e_form;
		htp.p('<tr><td colspan="2" align="right">'); html.back( get.txt('go_back_without_inserting_ad') ); htp.p('</td></tr>');

		html.e_table;
		html.e_box;

	-- ---------------
	-- insert into db
	-- ---------------
	elsif (p_preview = 'insert' AND p_title IS NOT NULL AND p_body IS NOT NULL) then

		if (p_item_pk IS NULL) then
			SELECT	item_seq.NEXTVAL
			INTO	v_item_pk
			FROM	dual;

			INSERT INTO item
			(item_pk, user_fk, url, no_items, price_type, freight_charge, viewed_times, insert_date, publish_date, end_date, contact_name, contact_email, contact_phone, trade_type_fk, item_type_fk, geography_fk, payment_method_fk)
			VALUES (v_item_pk, v_user_pk, p_url, p_no_items, p_price_type, p_freight_charge, 0, sysdate, to_date(p_publish_date, get.txt('date_long')), to_date(p_end_date, get.txt('date_long')), p_contact_name, p_contact_email, p_contact_phone, p_trade_type_pk, p_item_type_pk, p_geography_pk, p_payment_method_pk);
			commit;

			SELECT	item_text_seq.NEXTVAL
			INTO	v_item_text_pk
			FROM	dual;

			INSERT INTO item_text
			(item_text_pk, item_fk, language_fk, title, body, price, other)
			VALUES(v_item_text_pk, v_item_pk, get.lan, p_title, SUBSTR(p_body,1,4000), p_price, SUBSTR(p_other,1,500) );
			commit;

			SELECT 	email INTO v_email
			FROM	user_details
			WHERE	user_fk = v_user_pk;

			if (p_contact_email = v_email AND p_contact_phone IS NOT NULL) then
				UPDATE	user_details
				SET	phone_number = p_contact_phone
				WHERE	user_fk = v_user_pk;
				commit;
			end if;


			html.b_box( get.txt('item_inserted'), '100%', 'bboard.insert_item');
			html.b_table;
			htp.p('
				<tr><td>'|| get.txt('item_inserted_desc') ||'</td></tr>
			');
			html.e_table;
			html.e_box;

		else

			UPDATE 	item
			SET	url = p_url,
				no_items = p_no_items,
				price_type = p_price_type,
				freight_charge = p_freight_charge,
				publish_date = to_date(p_publish_date, get.txt('date_long')),
				end_date = to_date(p_end_date, get.txt('date_long')),
				contact_name = p_contact_name,
				contact_email = p_contact_email,
				contact_phone = p_contact_phone,
				trade_type_fk = p_trade_type_pk,
				item_type_fk = p_item_type_pk,
				geography_fk = p_geography_pk,
				payment_method_fk = p_payment_method_pk
			WHERE	item_pk = p_item_pk;
			commit;

			UPDATE 	item_text
			SET	title = p_title,
				body = SUBSTR(p_body, 1, 4000),
				price = p_price,
				other = SUBSTR(p_other, 1, 4000)
			WHERE	item_fk = p_item_pk
			AND	language_fk = get.lan;
			commit;

			html.jump_to('bboard.own_items');

		end if;
	-- ------------
	-- html form
	-- ------------
	else
		html.b_form('bboard.insert_item');
		htp.p('<input type="hidden" name="p_preview" value="yes">');
		html.b_box( get.txt('insert_item'), '100%', 'bboard.insert_item');
		html.b_table;

		htp.p('<tr><td colspan="2">'|| get.txt('insert_item_desc') ||'</td></tr>');
		htp.p('<tr><td colspan="2">&nbsp;</td></tr>');

		htp.p('<tr><td colspan="2">'|| get.txt('choose_trade_and_item_type') ||'</td></tr>');
		htp.p('<tr><td valign="top">'|| get.txt('trade_type') ||':</td><td>'); select_trade_type; htp.p('</td></tr>');
		htp.p('<tr><td valign="top">'|| get.txt('item_type') ||':</td><td>'); select_item_type; htp.p('</td></tr>');
		htp.p('<tr><td colspan="2">&nbsp;</td></tr>');

		htp.p('<tr><td colspan="2">'|| get.txt('write_info_about_item') ||'</td></tr>');
		htp.p('<tr><td valign="top">'|| get.txt('item_title') ||':</td><td><input type="text" name="p_title" size="40" maxlength="200"></td></tr>');
		htp.p('<tr><td valign="top">'|| get.txt('item_body') ||':</td><td><textarea name="p_body" cols="40" rows="5" wrap="virtual"></textarea></td></tr>');
		htp.p('<tr><td valign="top">'|| get.txt('item_price') ||':</td><td><input type="text" name="p_price" size="40" maxlength="100"></td></tr>');
		-- htp.p('<tr><td valign="top">'|| get.txt('additional_information') ||':</td><td><textarea name="p_other" cols="40" rows="3"></textarea></td></tr>');
		htp.p('<tr><td valign="top">'|| get.txt('item_url') ||':</td><td><input type="text" name="p_url" size="40" maxlength="150"></td></tr>');
		htp.p('<tr><td valign="top">'|| get.txt('no_items') ||':</td><td><input type="text" name="p_no_items" size="5" maxlength="10"></td></tr>');
		htp.p('<tr><td colspan="2">&nbsp;</td></tr>');

		htp.p('<tr><td colspan="2">'|| get.txt('additional_item_info') ||'</td></tr>');
		htp.p('<tr><td valign="top">'|| get.txt('discuss_price') ||':</td><td><select name="p_price_type"><option value="1">'||get.txt('yes')||'</option><option value="2">'||get.txt('no')||'</option></select></td></tr>');
		htp.p('<tr><td valign="top">'|| get.txt('freight_charge') ||':</td><td><select name="p_freight_charge"><option value="">'||get.txt('not_choosen')||'</option><option value="1">'||get.txt('yes')||'</option><option value="2">'||get.txt('no')||'</option></select></td></tr>');
		htp.p('<tr><td valign="top">'|| get.txt('payment_method') ||':</td><td>'); select_payment; htp.p('</td></tr>');
		htp.p('<tr><td colspan="2">&nbsp;</td></tr>');

		htp.p('<tr><td colspan="2">'|| get.txt('choose_start_end_date') ||'</td></tr>');
		htp.p('
			<tr><td>'|| get.txt('bboard_start_date') ||':</td><td>
	                <input type="text" name="p_publish_date" value="'||to_char(sysdate,get.txt('date_long'))||'"
	                onSelect=javascript:openWin(''date_time.startup?p_variable=p_publish_date'',300,200)
	                onClick=javascript:openWin(''date_time.startup?p_variable=p_publish_date'',300,200)>');
	                htp.p( html.popup( get.txt('change_start_date'),'date_time.startup?p_variable=p_publish_date','300', '200') );
	                htp.p('</td></tr>');
		htp.p('
	                <tr><td>'|| get.txt('bboard_end_date') ||':</td><td>
	                <input type="text" name="p_end_date" value="'||to_char( (sysdate+30), get.txt('date_long'))||'"
	                onSelect=javascript:openWin(''date_time.startup?p_variable=p_end_date&p_start_date='||to_char((sysdate+30),get.txt('date_full'))||''',300,200)
	                onClick=javascript:openWin(''date_time.startup?p_variable=p_end_date&p_start_date='||to_char((sysdate+30),get.txt('date_full'))||''',300,200)>');
	                htp.p( html.popup( get.txt('change_end_date'),'date_time.startup?p_variable=p_end_date&p_start_date='||to_char((sysdate+30),get.txt('date_full'))||'','300', '200') );
	                htp.p('</td></tr>');
		htp.p('<tr><td colspan="2">&nbsp;</td></tr>');

		htp.p('<tr><td colspan="2">'|| get.txt('item_contact_person_info') ||'</td></tr>');
		htp.p('<tr><td valign="top">'|| get.txt('contact_name') ||':</td><td><input type="text" name="p_contact_name" size="40" maxlength="150" value="'|| v_first_name ||' '|| v_last_name ||'"></td></tr>');
		htp.p('<tr><td valign="top">'|| get.txt('contact email') ||':</td><td><input type="text" name="p_contact_email" size="40" maxlength="150" value="'|| v_email ||'"></td></tr>');
		htp.p('<tr><td valign="top">'|| get.txt('contact_phone') ||':</td><td><input type="text" name="p_contact_phone" size="40" maxlength="150" value="'|| v_phone_number ||'"></td></tr>');
		htp.p('<tr><td valign="top">'|| get.txt('contact_geography') ||':</td>
				<td><input type="hidden" name="p_geography_pk" value="'|| get.ulpk ||'">
				<input type="text" size="30" name="p_geo_name" value="'|| get.uln ||'" onSelect=javascript:openWin(''select_geo.select_location?p_no_levels=4'',300,200) onClick=javascript:openWin(''select_geo.select_location?p_no_levels=4'',300,200)>
				'|| html.popup( get.txt('change_geo'), 'select_geo.select_location?p_no_levels=4', '300', '200') ||'
				</td></tr>');
		htp.p('<tr><td colspan="2">&nbsp;</td></tr>');

		htp.p('<tr><td colspan="2" align="right">'); html.submit_link( get.txt('preview_ad') ); htp.p('</td></tr>');

		html.e_table;
		html.e_box;
		html.e_form;
	end if;


	-- -----------------------
	-- avlutter h�yre stolpe
	-- -----------------------
	htp.p('</td></tr>');

	html.e_table;
	html.e_page;

end if;
END;
END insert_item;




---------------------------------------------------------
-- Name: 	select_item_type
-- Type: 	procedure
-- What: 	writes out a select for item type
-- Author:  	Frode Klevstul
-- Start date: 	08.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE select_item_type
	(
		p_item_type_pk		in item_type.item_type_pk%type		default NULL,
		p_type			in varchar2				default NULL
	)
IS
BEGIN
DECLARE

	CURSOR	select_all IS
	SELECT	item_type_pk, string
	FROM	item_type it, item_type_on_service itos, string_group sg, groups g, strng s
	WHERE	itos.item_type_fk = it.item_type_pk
	AND	(
		itos.service_fk = get.serv OR
		itos.service_fk = 0
		)
	AND	it.name_sg_fk = sg.string_group_pk
	AND	sg.string_group_pk = g.string_group_fk
	AND	g.string_fk = string_pk
	AND	language_fk = get.lan
	ORDER BY string
	;

	v_item_type_pk		item_type.item_type_pk%type	default NULL;
	v_string		strng.string%type		default NULL;

BEGIN

	htp.p('<select name="p_item_type_pk" size="1">');
	if (p_type IS NOT NULL) then
		htp.p('<option value="">'|| get.txt('all_item_types') ||'</option>');
	end if;

	open select_all;
	loop
		fetch select_all into v_item_type_pk, v_string;
		exit when select_all%NOTFOUND;

		if (p_item_type_pk = v_item_type_pk) then
			htp.p('<option value="'||v_item_type_pk||'" selected>'|| v_string ||'</option>');
		else
			htp.p('<option value="'||v_item_type_pk||'">'|| v_string ||'</option>');
		end if;
	end loop;
	close select_all;

	htp.p('</select>');

END;
END select_item_type;


---------------------------------------------------------
-- Name: 	select_trade_type
-- Type: 	procedure
-- What: 	writes out a select for trade type
-- Author:  	Frode Klevstul
-- Start date: 	08.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE select_trade_type
	(
		p_trade_type_pk		in trade_type.trade_type_pk%type	default NULL,
		p_type			in varchar2				default NULL
	)
IS
BEGIN
DECLARE

	CURSOR	select_all IS
	SELECT	trade_type_pk, string
	FROM	trade_type tt, string_group sg, groups g, strng s
	WHERE	tt.name_sg_fk = sg.string_group_pk
	AND	sg.string_group_pk = g.string_group_fk
	AND	g.string_fk = string_pk
	AND	s.language_fk = get.lan
	ORDER BY string
	;

	v_trade_type_pk		trade_type.trade_type_pk%type	default NULL;
	v_string		strng.string%type		default NULL;

BEGIN

	htp.p('<select name="p_trade_type_pk" size="1">');

	if (p_type IS NOT NULL) then
		htp.p('<option value="">'|| get.txt('all_trade_types') ||'</option>');
	end if;

	open select_all;
	loop
		fetch select_all into v_trade_type_pk, v_string;
		exit when select_all%NOTFOUND;

		if (p_trade_type_pk = v_trade_type_pk) then
			htp.p('<option value="'||v_trade_type_pk||'" selected>'|| v_string ||'</option>');
		else
			htp.p('<option value="'||v_trade_type_pk||'">'|| v_string ||'</option>');
		end if;
	end loop;

	close select_all;
	htp.p('</select>');

END;
END select_trade_type;


---------------------------------------------------------
-- Name: 	select_payment
-- Type: 	procedure
-- What: 	writes out a select for payment method
-- Author:  	Frode Klevstul
-- Start date: 	08.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE select_payment
	(
		p_payment_method_pk	in payment_method.payment_method_pk%type		default NULL
	)
IS
BEGIN
DECLARE

	CURSOR	select_all IS
	SELECT	payment_method_pk, string
	FROM	payment_method, string_group sg, groups g, strng s
	WHERE	name_sg_fk = sg.string_group_pk
	AND	sg.string_group_pk = g.string_group_fk
	AND	g.string_fk = string_pk
	AND	s.language_fk = get.lan
	ORDER BY string
	;

	v_payment_method_pk		payment_method.payment_method_pk%type	default NULL;
	v_string			strng.string%type			default NULL;

BEGIN

	htp.p('<select name="p_payment_method_pk" size="1">');
	open select_all;
	loop
		fetch select_all into v_payment_method_pk, v_string;
		exit when select_all%NOTFOUND;

		if (p_payment_method_pk = v_payment_method_pk) then
			htp.p('<option value="'||v_payment_method_pk||'" selected>'|| v_string ||'</option>');
		else
			htp.p('<option value="'||v_payment_method_pk||'">'|| v_string ||'</option>');
		end if;
	end loop;
	close select_all;
	htp.p('</select>');

END;
END select_payment;




---------------------------------------------------------
-- Name: 	own_items
-- Type: 	procedure
-- What: 	writes out a list of own items
-- Author:  	Frode Klevstul
-- Start date: 	11.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE own_items
	(
		p_item_pk	in item.item_pk%type	default NULL,
		p_type		in varchar2		default NULL,
		p_confirm	in varchar2		default NULL
	)
IS
BEGIN
DECLARE

	CURSOR	select_all(v_user_pk usr.user_pk%type) IS
	SELECT	item_pk, title, trade_type_fk, item_type_fk, publish_date, end_date
	FROM	item_text it, item i
	WHERE	it.item_fk = i.item_pk
	AND	it.language_fk = get.lan
	AND	user_fk = v_user_pk;

	CURSOR	select_all_2
		(
			v_user_pk usr.user_pk%type,
			v_item_pk item.item_pk%type
		) IS
	SELECT	item_pk, url, no_items, price_type, freight_charge, viewed_times,
		insert_date, publish_date, end_date, contact_name, contact_email,
		contact_phone, trade_type_fk, item_type_fk, geography_fk,
		payment_method_fk, title, body, price, other
	FROM	item_text it, item i
	WHERE	it.item_fk = i.item_pk
	AND	it.language_fk = get.lan
	AND	item_pk = v_item_pk
	AND	user_fk = v_user_pk
	;

	v_item_pk		item.item_pk%type			default NULL;
	v_url			item.url%type				default NULL;
	v_no_items		item.no_items%type			default NULL;
	v_price_type		item.price_type%type			default NULL;
	v_freight_charge	item.freight_charge%type		default NULL;
	v_viewed_times		item.viewed_times%type			default NULL;
	v_insert_date		item.insert_date%type			default NULL;
	v_publish_date		item.publish_date%type			default NULL;
	v_end_date		item.end_date%type			default NULL;
	v_contact_name		item.contact_name%type			default NULL;
	v_contact_email		item.contact_email%type			default NULL;
	v_contact_phone		item.contact_phone%type			default NULL;
	v_trade_type_pk		trade_type.trade_type_pk%type		default NULL;
	v_item_type_pk		item_type.item_type_pk%type		default NULL;
	v_geography_pk		geography.geography_pk%type		default NULL;
	v_payment_method_pk	payment_method.payment_method_pk%type	default NULL;

	v_title			item_text.title%type			default NULL;
	v_body			item_text.body%type			default NULL;
	v_price			item_text.price%type			default NULL;
	v_other			item_text.other%type			default NULL;

	v_user_pk		usr.user_pk%type			default NULL;

	v_check			number					default NULL;

	v_seller_string		varchar2(1000)				default NULL;
	v_status		varchar2(50)				default NULL;

BEGIN
if ( login.timeout('bboard.own_items')>0 ) then

	v_user_pk := get.uid;

	-- -----------------------------------
	-- sjekker at objektet tilh�rer brukeren
	-- -----------------------------------
	if (p_item_pk IS NOT NULL) then
		SELECT	count(*) INTO v_check
		FROM	item
		WHERE	item_pk = p_item_pk
		AND	user_fk = v_user_pk;

		if ( v_check = 0 ) then
			get.error_page(3, get.txt('invalid_item_pk'), NULL, 'bboard.own_items');
			return;
		end if;
	end if;

	html.b_page;
	htp.p('<table cellspacing="0">');

	-- ---------------------------
	-- skriver ut venstre stolpe
	-- ---------------------------
	htp.p('<tr><td valign="top" width="10%">');
	left_pillar;
	htp.p('</td><td valign="top">');

	-- ----------------------------------------
	-- skriver ut h�yre stolpe (hoved innhold)
	-- ----------------------------------------

	if (p_type IS NULL) then

		html.b_box( get.txt('own_items'), '100%', 'bboard.own_items');
		html.b_table;

		htp.p('<tr><td>&nbsp;</td><td><b>'|| get.txt('trade_type') ||':</b></td><td><b>'|| get.txt('item_title') ||':</b></td><td><b>'|| get.txt('item_type') ||':</b></td><td><b>'|| get.txt('edit_item') ||':</b></td><td><b>'|| get.txt('delete_item') ||':</b></td></tr>');
		open select_all(v_user_pk);
		loop
			fetch select_all into v_item_pk, v_title, v_trade_type_pk, v_item_type_pk, v_publish_date, v_end_date;
			exit when select_all%NOTFOUND;

			if ( v_publish_date < sysdate AND v_end_date > sysdate) then
				v_status := get.txt('item_stat_started');
			elsif ( v_publish_date < sysdate AND v_end_date < sysdate) then
				v_status := get.txt('item_stat_stopped');
			elsif ( v_publish_date > sysdate ) then
				v_status := get.txt('item_stat_waiting');
			end if;

			htp.p('<tr><td>'||v_status||'</td><td>'|| get.bb_text(v_trade_type_pk, 'trade_type') ||'</td><td><a href="bboard.own_items?p_item_pk='||v_item_pk||'&p_type=view">'|| v_title ||'</a></td><td>'|| get.bb_text(v_item_type_pk, 'item_type') ||'</td><td><a href="bboard.own_items?p_item_pk='||v_item_pk||'&p_type=update">'||get.txt('edit_item')||'</a></td><td><a href="bboard.own_items?p_item_pk='||v_item_pk||'&p_type=delete">'||get.txt('delete_item')||'</a></td></tr>');

		end loop;
		close select_all;

		html.e_table;
		html.e_box;

	elsif (p_type = 'view') then

		bboard.view_item(p_item_pk, 'own');

	elsif (p_type = 'update') then

		html.b_box( get.txt('update_item'), '100%', 'bboard.own_items(update)');
		html.b_table;

		open select_all_2(v_user_pk, p_item_pk);
		loop
			fetch select_all_2 into v_item_pk, v_url, v_no_items, v_price_type, v_freight_charge,
				v_viewed_times, v_insert_date, v_publish_date, v_end_date, v_contact_name,
				v_contact_email, v_contact_phone, v_trade_type_pk, v_item_type_pk, v_geography_pk,
				v_payment_method_pk, v_title, v_body, v_price, v_other;
			exit when select_all_2%NOTFOUND;

			html.b_form('bboard.insert_item');
			htp.p('<input type="hidden" name="p_preview" value="yes">');
			htp.p('<input type="hidden" name="p_item_pk" value="'||p_item_pk||'">');

			htp.p('<tr><td colspan="2">'|| get.txt('update_item_desc') ||'</td></tr>');
			htp.p('<tr><td colspan="2">&nbsp;</td></tr>');

			htp.p('<tr><td colspan="2">'|| get.txt('choose_trade_and_item_type') ||'</td></tr>');
			htp.p('<tr><td valign="top">'|| get.txt('trade_type') ||':</td><td>'); select_trade_type(v_trade_type_pk); htp.p('</td></tr>');
			htp.p('<tr><td valign="top">'|| get.txt('item_type') ||':</td><td>'); select_item_type(v_item_type_pk); htp.p('</td></tr>');
			htp.p('<tr><td colspan="2">&nbsp;</td></tr>');

			htp.p('<tr><td colspan="2">'|| get.txt('write_info_about_item') ||'</td></tr>');
			htp.p('<tr><td valign="top">'|| get.txt('item_title') ||':</td><td><input type="text" name="p_title" size="40" maxlength="200" value="'||v_title||'"></td></tr>');
			htp.p('<tr><td valign="top">'|| get.txt('item_body') ||':</td><td><textarea name="p_body" cols="40" rows="5" wrap="virtual">'||v_body||'</textarea></td></tr>');
			htp.p('<tr><td valign="top">'|| get.txt('item_price') ||':</td><td><input type="text" name="p_price" size="40" maxlength="100" value="'||v_price||'"></td></tr>');
			--htp.p('<tr><td valign="top">'|| get.txt('additional_information') ||':</td><td><textarea name="p_other" cols="40" rows="3">'||v_other||'</textarea></td></tr>');
			htp.p('<tr><td valign="top">'|| get.txt('item_url') ||':</td><td><input type="text" name="p_url" size="40" maxlength="150" value="'||v_url||'"></td></tr>');
			htp.p('<tr><td valign="top">'|| get.txt('no_items') ||':</td><td><input type="text" name="p_no_items" size="5" maxlength="10" value="'||v_no_items||'"></td></tr>');
			htp.p('<tr><td colspan="2">&nbsp;</td></tr>');

			htp.p('<tr><td colspan="2">'|| get.txt('additional_item_info') ||'</td></tr>');
			htp.p('<tr><td valign="top">'|| get.txt('discuss_price') ||':</td><td><select name="p_price_type"><option value="1">'||get.txt('yes')||'</option><option value="2"');  if (v_price_type=2) then htp.p('selected'); end if;  htp.p('>'||get.txt('no')||'</option></select></td></tr>');
			htp.p('<tr><td valign="top">'|| get.txt('freight_charge') ||':</td><td><select name="p_freight_charge"><option value="1">'||get.txt('yes')||'</option><option value="2"');  if (v_freight_charge=2) then htp.p('selected'); end if;  htp.p('>'||get.txt('no')||'</option></select></td></tr>');
			htp.p('<tr><td valign="top">'|| get.txt('payment_method') ||':</td><td>'); select_payment(v_payment_method_pk); htp.p('</td></tr>');
			htp.p('<tr><td colspan="2">&nbsp;</td></tr>');

			htp.p('<tr><td colspan="2">'|| get.txt('choose_start_end_date') ||'</td></tr>');
			htp.p('
				<tr><td>'|| get.txt('bboard_start_date') ||':</td><td>
		                <input type="text" name="p_publish_date" value="'||to_char(v_publish_date,get.txt('date_long'))||'"
		                onSelect=javascript:openWin(''date_time.startup?p_variable=p_publish_date&p_start_date='|| to_char(v_publish_date,get.txt('date_full'))||''',300,200)
		                onClick=javascript:openWin(''date_time.startup?p_variable=p_publish_date&p_start_date='|| to_char(v_publish_date,get.txt('date_full'))||''',300,200)>');
		                htp.p( html.popup( get.txt('change_start_date'),'date_time.startup?p_variable=p_publish_date&p_start_date='|| to_char(v_publish_date,get.txt('date_full')),'300', '200') );
		                htp.p('</td></tr>');
			htp.p('
		                <tr><td>'|| get.txt('bboard_end_date') ||':</td><td>
		                <input type="text" name="p_end_date" value="'||to_char( v_end_date, get.txt('date_long'))||'"
		                onSelect=javascript:openWin(''date_time.startup?p_variable=p_end_date&p_start_date='|| to_char(v_end_date,get.txt('date_full'))||''',300,200)
		                onClick=javascript:openWin(''date_time.startup?p_variable=p_end_date&p_start_date='|| to_char(v_end_date,get.txt('date_full'))||''',300,200)>');
		                htp.p( html.popup( get.txt('change_end_date'),'date_time.startup?p_variable=p_end_date&p_start_date='|| to_char(v_end_date,get.txt('date_full')), '300', '200'));
		                htp.p('</td></tr>');
			htp.p('<tr><td colspan="2">&nbsp;</td></tr>');

			htp.p('<tr><td colspan="2">'|| get.txt('item_contact_person_info') ||'</td></tr>');
			htp.p('<tr><td valign="top">'|| get.txt('contact_name') ||':</td><td><input type="text" name="p_contact_name" size="40" maxlength="150" value="'|| v_contact_name ||'"></td></tr>');
			htp.p('<tr><td valign="top">'|| get.txt('contact_email') ||':</td><td><input type="text" name="p_contact_email" size="40" maxlength="150" value="'|| v_contact_email ||'"></td></tr>');
			htp.p('<tr><td valign="top">'|| get.txt('contact_phone') ||':</td><td><input type="text" name="p_contact_phone" size="40" maxlength="150" value="'|| v_contact_phone ||'"></td></tr>');
			htp.p('<tr><td valign="top">'|| get.txt('contact_geography') ||':</td>
					<td><input type="hidden" name="p_geography_pk" value="'|| v_geography_pk ||'">
					<input type="text" size="30" name="p_geo_name" value="'|| get.locn(v_geography_pk) ||'" onSelect=javascript:openWin(''select_geo.select_location?p_no_levels=4'',300,200) onClick=javascript:openWin(''select_geo.select_location?p_no_levels=4'',300,200)>
					'|| html.popup( get.txt('change_geo'), 'select_geo.select_location?p_no_levels=4', '300', '200') ||'
					</td></tr>');
			htp.p('<tr><td colspan="2">&nbsp;</td></tr>');

			htp.p('<tr><td colspan="2" align="right">'); html.submit_link( get.txt('preview_ad') ); htp.p('</td></tr>');
			html.e_form;
			htp.p('<tr><td colspan="2" align="right">'); html.back( get.txt('back') ); htp.p('</td></tr>');

		end loop;
		close select_all_2;

		html.e_table;
		html.e_box;

	elsif (p_type = 'delete') then

		SELECT	title INTO v_title
		FROM	item_text
		WHERE	language_fk = get.lan
		AND	item_fk = p_item_pk;

		if (p_confirm IS NULL) then
			html.b_box( get.txt('delete_item'), '100%', 'bboard.own_items');
	                html.b_form('bboard.own_items');
	                htp.p('<input type="hidden" name="p_item_pk" value="'||p_item_pk||'">');
	                htp.p('<input type="hidden" name="p_type" value="delete">');
	                html.b_table;
	                htp.p('
	                        <tr><td>'|| get.txt('confirm_deleting_item') ||' <b>'''|| v_title ||'''</b></td></tr>
	                        <tr><td>&nbsp;</td></tr>
	                        <tr>
	                                <td align="right"><input type="submit" name="p_confirm" value="'||get.txt('delete_item')||'"></td>
	                        </tr>
	                        <tr>
	                                <td align="right">'); html.back( get.txt('dont_delete_item') ); htp.p('</td></tr>');
	                html.e_table;
	                html.e_form;
			html.e_box;
		else

			DELETE FROM item_text
			WHERE item_fk = p_item_pk;
			commit;

			DELETE FROM item
			WHERE item_pk = p_item_pk;
			commit;

			html.jump_to('bboard.own_items');

		end if;

	end if;



	-- ----------------------
	-- avlutter h�yre stolpe
	-- -----------------------
	htp.p('</td></tr>');

	html.e_table;
	html.e_page;


end if;
END;
END own_items;






---------------------------------------------------------
-- Name: 	view_item
-- Type: 	procedure
-- What: 	view info about item
-- Author:  	Frode Klevstul
-- Start date: 	11.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE view_item
	(
		p_item_pk	in item.item_pk%type	default NULL,
		p_type		in varchar2		default NULL
	)
IS
BEGIN
DECLARE

	CURSOR	select_all
		(
			v_item_pk item.item_pk%type
		) IS
	SELECT	item_pk, url, no_items, price_type, freight_charge, viewed_times,
		insert_date, publish_date, end_date, contact_name, contact_email,
		contact_phone, trade_type_fk, item_type_fk, geography_fk,
		payment_method_fk, title, body, price, other
	FROM	item_text it, item i
	WHERE	it.item_fk = i.item_pk
	AND	it.language_fk = get.lan
	AND	item_pk = v_item_pk;

	v_item_pk		item.item_pk%type			default NULL;
	v_url			item.url%type				default NULL;
	v_no_items		item.no_items%type			default NULL;
	v_price_type		item.price_type%type			default NULL;
	v_freight_charge	item.freight_charge%type		default NULL;
	v_viewed_times		item.viewed_times%type			default NULL;
	v_insert_date		item.insert_date%type			default NULL;
	v_publish_date		item.publish_date%type			default NULL;
	v_end_date		item.end_date%type			default NULL;
	v_contact_name		item.contact_name%type			default NULL;
	v_contact_email		item.contact_email%type			default NULL;
	v_contact_phone		item.contact_phone%type			default NULL;
	v_trade_type_pk		trade_type.trade_type_pk%type		default NULL;
	v_item_type_pk		item_type.item_type_pk%type		default NULL;
	v_geography_pk		geography.geography_pk%type		default NULL;
	v_payment_method_pk	payment_method.payment_method_pk%type	default NULL;

	v_title			item_text.title%type			default NULL;
	v_body			item_text.body%type			default NULL;
	v_price			item_text.price%type			default NULL;
	v_other			item_text.other%type			default NULL;

	v_user_pk		usr.user_pk%type			default NULL;
	v_check			number					default NULL;

	v_seller_string		varchar2(1000)				default NULL;

BEGIN

	-- ------------------------------------------------------------
	-- p_type:
	-- 'normal'	- vis frem "objekt" p� vanlig m�te (vises uten full layout)
	-- 'own'	- brukerens eget "objekt", vises ogs� selv
	--		  om "objektet" er utg�tt p� dato (vises med full layout)
	-- ------------------------------------------------------------

	-- ---------------------------------------------------
	-- sjekker at objektet tilh�rer brukeren/er publisert
	-- ---------------------------------------------------
	if (p_type = 'own') then
		v_user_pk := get.uid;

		SELECT	count(*) INTO v_check
		FROM	item
		WHERE	item_pk = p_item_pk
		AND	user_fk = v_user_pk;

		if ( v_check = 0 ) then
			get.error_page(3, get.txt('invalid_item_pk'), NULL, 'bboard.own_items');
			return;
		end if;
	elsif (p_type = 'normal') then
		SELECT	count(*) INTO v_check
		FROM	item
		WHERE	publish_date < sysdate
		AND	end_date > sysdate
		AND	item_pk = p_item_pk;

		if ( v_check = 0 ) then
			get.error_page(3, get.txt('invalid_item_pk'), NULL, 'bboard.own_items');
			return;
		else
			SELECT	viewed_times INTO v_check
			FROM	item
			WHERE	item_pk = p_item_pk;

			v_check := v_check + 1;

			UPDATE	item
			SET	viewed_times = v_check
			WHERE	item_pk = p_item_pk;
			commit;

		end if;
	else
		get.error_page(3, get.txt('invalid_item_pk'), NULL, 'bboard.own_items');
		return;
	end if;


	if ( p_type = 'normal' ) then
		html.b_page;
		htp.p('<table cellspacing="0">');
		-- ---------------------------
		-- skriver ut venstre stolpe
		-- ---------------------------
		htp.p('<tr><td valign="top" width="10%">');
		left_pillar;
		htp.p('</td><td valign="top">');
	end if;

	html.b_box( get.txt('item_details') );
	html.b_table;

	open select_all(p_item_pk);
	loop
		fetch select_all into v_item_pk, v_url, v_no_items, v_price_type, v_freight_charge,
			v_viewed_times, v_insert_date, v_publish_date, v_end_date, v_contact_name,
			v_contact_email, v_contact_phone, v_trade_type_pk, v_item_type_pk, v_geography_pk,
			v_payment_method_pk, v_title, v_body, v_price, v_other;
		exit when select_all%NOTFOUND;

		owa_pattern.change(v_body, '\n', '<br>', 'g');

		-- skriver ut tekst om oppslaget
		htp.p('
			<tr>
				<td colspan="2"><h2>'|| v_title ||'</h2></td>
			</tr>
			<tr>
				<td colspan="2"><i>'|| get.txt('item_price') ||':&nbsp;'|| v_price ||'</i></td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td colspan="2"><b>'|| v_body ||'</b></td>
			</tr>
			<tr><td colspan="2"><hr noshade></td></tr>
		');


		-- detaljinformasjon om oppslaget (skriv ut dersom not null)
		if (v_no_items IS NOT NULL) then
			htp.p('<tr><td width="40%">'|| get.txt('no_items') ||':</td><td>'|| v_no_items ||'</td></tr>');
		end if;

		if (v_url IS NOT NULL) then
	        	-- ^ 		: Matches newline or the beginning of the target
	        	-- \w?		: Matches word characters (alphanumeric) [0-9, a-z, A-Z or _] 0 or 1 time
	                if not( owa_pattern.match(v_url, '^http\w?://') ) then
	                        v_url := 'http://' || v_url;
	                end if;
	          	htp.p('<tr><td>'|| get.txt('link_to_item') ||':</td><td><a href="'||v_url||'" target="new">'|| v_url ||'</a></td></tr>');
		end if;

		if (v_freight_charge IS NOT NULL) then
			htp.p('<tr><td>'|| get.txt('freight_charge') ||':</td><td>'); if(v_freight_charge = 1)then htp.p( get.txt('yes') ); else htp.p( get.txt('no') ); end if; htp.p('</td></tr>');
		end if;

		if (v_price_type IS NOT NULL) then
			htp.p('<tr><td>'|| get.txt('discuss_price') ||':</td><td>'); if(v_price_type = 1)then htp.p( get.txt('yes') ); else htp.p( get.txt('no') ); end if; htp.p('</td></tr>');
		end if;

		if (v_payment_method_pk IS NOT NULL) then
			htp.p('<tr><td>'|| get.txt('payment_method') ||':</td><td>'|| get.bb_text(v_payment_method_pk, 'payment_method') ||'</td></tr>');
		end if;

		if (v_publish_date IS NOT NULL) then
			htp.p('<tr><td>'|| get.txt('bboard_start_date') ||':</td><td>'|| to_char(v_publish_date, get.txt('date_year')) ||'</td></tr>');
		end if;

		if (v_end_date IS NOT NULL) then
			htp.p('<tr><td>'|| get.txt('bboard_end_date') ||':</td><td>'|| to_char(v_end_date, get.txt('date_year')) ||'</td></tr>');
		end if;

		if (v_viewed_times IS NOT NULL) then
			htp.p('<tr><td>'|| get.txt('item_viewed_times') ||':</td><td>'|| v_viewed_times ||'</td></tr><tr><td colspan="2">&nbsp;</td></tr>');
		end if;

		if (v_geography_pk IS NOT NULL) then
			htp.p('<tr><td>'|| get.txt('contact_geography') ||':</td><td>'|| get.locn(v_geography_pk) ||'</td></tr>');
		end if;


		-- bygger opp feltet som skrives ut om selger (v_seller_string)
		v_seller_string := '<tr><td>'|| get.txt('contact_name') || ':</td><td>' ||v_contact_name ||'</td></tr>';
		if (v_contact_email IS NOT NULL) then
			v_seller_string := v_seller_string ||'<tr><td>'|| get.txt('contact_email_address') ||':</td><td><a href="tip.mailform?p_contact_email='||v_contact_email||'&p_subject='||REPLACE(v_title, ' ', '+')||'&p_url=bboard.view_item�p_item_pk='||p_item_pk||'[]p_type=normal&p_from='||REPLACE(get.uname, ' ', '+')||'&p_email_address='||get.email||'">'|| v_contact_email ||'</a></td></tr>';
		end if;
		if (v_contact_phone IS NOT NULL) then
			v_seller_string := v_seller_string ||'<tr><td>'|| get.txt('phone_number') ||':</td><td>'|| v_contact_phone ||'</td></tr>';
		end if;
		htp.p(v_seller_string);


		if (v_other IS NOT NULL) then
			htp.p('<tr><td>&nbsp;</td></tr><tr><td><i>'|| v_other ||'</i></td></tr><tr><td>&nbsp;</td></tr>');
		end if;

	end loop;
	close select_all;

	htp.p('<tr><td colspan="2"><hr noshade></td></tr>');
	htp.p('<tr><td colspan="2" align="right">');
	html.back( get.txt('back') );
	htp.p('</td></tr>');

        htp.p('<tr><td colspan="2">');
        tip.someone;
        htp.p('</td></tr>');

	html.e_table;
	html.e_box;

	if ( p_type = 'normal' ) then
		-- ----------------------
		-- avlutter h�yre stolpe
		-- -----------------------
		htp.p('</td></tr>');

		html.e_table;
		html.e_page;
	end if;


END;
END view_item;




---------------------------------------------------------
-- Name: 	last_items
-- Type: 	procedure
-- What: 	list out items of given trade- and item-type
-- Author:  	Frode Klevstul
-- Start date: 	16.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE last_items
	(
		p_trade_type_pk		in trade_type.trade_type_pk%type	default NULL,
		p_item_type_pk		in item_type.item_type_pk%type		default NULL,
		p_type			in varchar2				default NULL
	)
IS
BEGIN
DECLARE

	CURSOR	select_all
		(
			v_trade_type_pk		in trade_type.trade_type_pk%type,
			v_item_type_pk		in item_type.item_type_pk%type
		) IS
	SELECT	item_pk, title, price
	FROM	item_text it, item i
	WHERE	it.item_fk = i.item_pk
	AND	it.language_fk = get.lan
	AND	i.publish_date < sysdate
	AND	i.end_date > sysdate
	AND	i.trade_type_fk = v_trade_type_pk
	AND	i.item_type_fk = v_item_type_pk
	ORDER BY publish_date DESC
	;

	CURSOR	select_all_2
		(
			v_trade_type_pk		in trade_type.trade_type_pk%type
		) IS
	SELECT	item_pk, title, item_type_fk, price
	FROM	item_text it, item i
	WHERE	it.item_fk = i.item_pk
	AND	it.language_fk = get.lan
	AND	i.publish_date < sysdate
	AND	i.end_date > sysdate
	AND	i.trade_type_fk = v_trade_type_pk
	ORDER BY publish_date DESC
	;

	CURSOR	select_all_3 IS
	SELECT	item_pk, title, item_type_fk, trade_type_fk, price
	FROM	item_text it, item i
	WHERE	it.item_fk = i.item_pk
	AND	it.language_fk = get.lan
	AND	i.publish_date < sysdate
	AND	i.end_date > sysdate
	ORDER BY viewed_times DESC
	;

	CURSOR	select_all_4
		(
			v_item_type_pk		in item_type.item_type_pk%type
		) IS
	SELECT	item_pk, title, trade_type_fk, price
	FROM	item_text it, item i
	WHERE	it.item_fk = i.item_pk
	AND	it.language_fk = get.lan
	AND	i.publish_date < sysdate
	AND	i.end_date > sysdate
	AND	i.item_type_fk = v_item_type_pk
	ORDER BY publish_date DESC
	;

	v_item_pk		item.item_pk%type			default NULL;
	v_trade_type_pk		trade_type.trade_type_pk%type		default NULL;
	v_item_type_pk		item_type.item_type_pk%type		default NULL;
	v_title			item_text.title%type			default NULL;
	v_price			item_text.price%type			default NULL;

	v_trade_type_string	varchar2(100)				default NULL;
	v_item_type_string	varchar2(100)				default NULL;

BEGIN

	-- ----------------------------------------------------
	-- p_type:
	-- 'layout' (NOT NULL)	- da vises listen med layout
	-- NULL			- listen vises uten layout
	-- ----------------------------------------------------

	if (p_type IS NOT NULL) then
		html.b_page;
		htp.p('<table cellspacing="0">');
		-- ---------------------------
		-- skriver ut venstre stolpe
		-- ---------------------------
		htp.p('<tr><td valign="top" width="10%">');
		left_pillar;
		htp.p('</td><td valign="top">');

		-- ----------------------------------------
		-- skriver ut h�yre stolpe (hoved innhold)
		-- ----------------------------------------
		bboard.trade_type_header(p_trade_type_pk, p_item_type_pk);
	end if;


	-- ---------------------------------------
	-- vi har: trade_type_pk og item_type_pk
	-- ---------------------------------------
	if (p_trade_type_pk IS NOT NULL AND p_item_type_pk IS NOT NULL) then

		v_trade_type_string 	:= get.bb_text(p_trade_type_pk, 'trade_type');
		v_item_type_string 	:= get.bb_text(p_item_type_pk, 'item_type');

		html.b_box( get.txt('last_items')||' '''||v_trade_type_string||': '||v_item_type_string||'''', '100%', 'bboard.last_items('||p_trade_type_pk||')');
		html.b_table;

		htp.p('<tr><td width="30%"><b>'|| get.txt('trade_type') ||':</b></td><td width="30%"><b>'|| get.txt('item_title') ||':</b></td><td width="20%"><b>'|| get.txt('item_price') ||':</b></td><td width="20%"><b>'|| get.txt('item_type') ||':</b></td></tr>');
		open select_all(p_trade_type_pk, p_item_type_pk);
		loop
			fetch select_all into v_item_pk, v_title, v_price;
			exit when select_all%NOTFOUND;

			htp.p('<tr><td>'|| v_trade_type_string ||'</td><td><a href="bboard.view_item?p_item_pk='||v_item_pk||'&p_type=normal">'|| v_title ||'</a></td><td>'|| v_price ||'</td><td>'|| v_item_type_string ||'</td></tr>');
			if (p_type IS NULL) then
				exit when select_all%ROWCOUNT = get.value('item_no');
			end if;

		end loop;
		close select_all;

		html.e_table;
		html.e_box;

	-- ---------------------------------------
	-- vi har: trade_type_pk
	-- ---------------------------------------
	elsif (p_item_type_pk IS NULL AND p_trade_type_pk IS NOT NULL) then

		v_trade_type_string 	:= get.bb_text(p_trade_type_pk, 'trade_type');

		html.b_box( get.txt('last_items')||' '''||v_trade_type_string||'''', '100%', 'bboard.last_items('||p_trade_type_pk||')');
		html.b_table;

		htp.p('<tr><td width="30%"><b>'|| get.txt('trade_type') ||':</b></td><td width="30%"><b>'|| get.txt('item_title') ||':</b></td><td width="20%"><b>'|| get.txt('item_price') ||':</b></td><td width="20%"><b>'|| get.txt('item_type') ||':</b></td></tr>');
		open select_all_2(p_trade_type_pk);
		loop
			fetch select_all_2 into v_item_pk, v_title, v_item_type_pk, v_price;
			exit when select_all_2%NOTFOUND;

			htp.p('<tr><td>');
			if (p_type IS NULL) then
				htp.p('<a href="bboard.last_items?p_trade_type_pk='||p_trade_type_pk||'&p_item_type_pk='||v_item_type_pk||'&p_type=normal">'|| v_trade_type_string ||'</a>');
			else
				htp.p( v_trade_type_string );
			end if;
			htp.p('</td><td><a href="bboard.view_item?p_item_pk='||v_item_pk||'&p_type=normal">'|| v_title ||'</a></td><td>'|| v_price ||'</td><td><a href="bboard.last_items?p_trade_type_pk='||p_trade_type_pk||'&p_item_type_pk='||v_item_type_pk||'&p_type=layout">'|| get.bb_text(v_item_type_pk, 'item_type') ||'</a></td></tr>');

			if (p_type IS NULL) then
				exit when select_all_2%ROWCOUNT = get.value('item_no');
			end if;

		end loop;
		close select_all_2;

		html.e_table;
		html.e_box;

	-- ---------------------------------------
	-- vi har: item_type_pk
	-- ---------------------------------------
	elsif (p_trade_type_pk IS NULL AND p_item_type_pk IS NOT NULL) then

		v_item_type_string 	:= get.bb_text(p_item_type_pk, 'item_type');

		html.b_box( get.txt('last_items')||' '''||v_item_type_string||'''', '100%', 'bboard.last_items');
		html.b_table;

		htp.p('<tr><td width="30%"><b>'|| get.txt('trade_type') ||':</b></td><td width="30%"><b>'|| get.txt('item_title') ||':</b></td><td width="20%"><b>'|| get.txt('item_price') ||':</b></td><td width="20%"><b>'|| get.txt('item_type') ||':</b></td></tr>');
		open select_all_4(p_item_type_pk);
		loop
			fetch select_all_4 into v_item_pk, v_title, v_trade_type_pk, v_price;
			exit when select_all_4%NOTFOUND;

			htp.p('<tr><td><a href="bboard.last_items?p_trade_type_pk='||v_trade_type_pk||'&p_item_type_pk='||p_item_type_pk||'&p_type=normal">'|| get.bb_text(v_trade_type_pk, 'trade_type') ||'</td><td><a href="bboard.view_item?p_item_pk='||v_item_pk||'&p_type=normal">'|| v_title ||'</a></td><td>'|| v_price ||'</td><td><a href="bboard.last_items?p_trade_type_pk='||v_trade_type_pk||'&p_item_type_pk='||p_item_type_pk||'&p_type=layout">'|| v_item_type_string ||'</a></td></tr>');
			if (p_type IS NULL) then
				exit when select_all_4%ROWCOUNT = get.value('item_no');
			end if;

		end loop;
		close select_all_4;

		html.e_table;
		html.e_box;

	-- ---------------------------------------
	-- vi har ingenting -> mest sette oppslag
	-- ---------------------------------------
	else

		html.b_box( get.txt('top_viewed_items'), '100%', 'bboard.last_items(top_viewed)');
		html.b_table;

		htp.p('<tr><td width="30%"><b>'|| get.txt('trade_type') ||':</b></td><td width="30%"><b>'|| get.txt('item_title') ||':</b></td><td width="20%"><b>'|| get.txt('item_price') ||':</b></td><td width="20%"><b>'|| get.txt('item_type') ||':</b></td></tr>');
		open select_all_3;
		loop
			fetch select_all_3 into v_item_pk, v_title, v_item_type_pk, v_trade_type_pk, v_price;
			exit when select_all_3%NOTFOUND;

			htp.p('<tr><td><a href="bboard.last_items?p_trade_type_pk='||v_trade_type_pk||'&p_type=normal">'|| get.bb_text(v_trade_type_pk, 'trade_type') ||'</a></td><td><a href="bboard.view_item?p_item_pk='||v_item_pk||'&p_type=normal">'|| v_title ||'</a></td><td>'|| v_price ||'</td><td><a href="bboard.last_items?p_trade_type_pk='||v_trade_type_pk||'&p_item_type_pk='||v_item_type_pk||'&p_type=layout">'|| get.bb_text(v_item_type_pk, 'item_type') ||'</a></td></tr>');
			if (p_type IS NULL) then
				exit when select_all_3%ROWCOUNT = get.value('item_no');
			end if;

		end loop;
		close select_all_3;

		html.e_table;
		html.e_box;

	end if;



	if (p_type IS NOT NULL) then
		-- ----------------------
		-- avlutter h�yre stolpe
		-- -----------------------
		htp.p('</td></tr>');

		html.e_table;
		html.e_page;
	end if;



END;
END last_items;


---------------------------------------------------------
-- Name: 	trade_type_header
-- Type: 	procedure
-- What: 	writes out a select for trade type
-- Author:  	Frode Klevstul
-- Start date: 	08.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE trade_type_header
	(
		p_trade_type_pk		in trade_type.trade_type_pk%type	default NULL,
		p_item_type_pk		in item_type.item_type_pk%type		default NULL
	)
IS
BEGIN
DECLARE

	CURSOR	select_all IS
	SELECT	trade_type_pk, string
	FROM	trade_type tt, string_group sg, groups g, strng s
	WHERE	tt.name_sg_fk = sg.string_group_pk
	AND	sg.string_group_pk = g.string_group_fk
	AND	g.string_fk = string_pk
	AND	s.language_fk = get.lan
	ORDER BY string
	;

	v_trade_type_pk		trade_type.trade_type_pk%type	default NULL;
	v_string		strng.string%type		default NULL;

BEGIN

	htp.p('<table><tr><td>'|| UPPER(get.bb_text(p_item_type_pk, 'item_type')) ||':</td><td>&nbsp;|&nbsp;</td>');
	open select_all;
	loop
		fetch select_all into v_trade_type_pk, v_string;
		exit when select_all%NOTFOUND;

		if (p_trade_type_pk = v_trade_type_pk) then
			htp.p('<td><b>'|| v_string ||'</b></td>');
		else
			htp.p('<td><a href="bboard.last_items?p_item_type_pk='||p_item_type_pk||'&p_trade_type_pk='||v_trade_type_pk||'&p_type=layout">'|| v_string ||'</a></td>');
		end if;
		htp.p('<td>&nbsp;|&nbsp;</td>');
	end loop;
	close select_all;
	htp.p('</tr></table>');

END;
END trade_type_header;


---------------------------------------------------------
-- Name: 	search
-- Type: 	procedure
-- What: 	search in items
-- Author:  	Frode Klevstul
-- Start date: 	18.10.2000
-- Desc:
---------------------------------------------------------
PROCEDURE search
	(
		p_trade_type_pk		in trade_type.trade_type_pk%type	default NULL,
		p_item_type_pk		in item_type.item_type_pk%type		default NULL,
		p_string		in varchar2				default NULL,
		p_type			in varchar2				default NULL
	)
IS
BEGIN
DECLARE

	CURSOR	select_all
		(
			v_trade_type_pk		in trade_type.trade_type_pk%type,
			v_item_type_pk		in item_type.item_type_pk%type,
			v_string		in varchar2
		) IS
	SELECT	item_pk, title, trade_type_fk, item_type_fk
	FROM	item_text it, item i
	WHERE	it.item_fk = i.item_pk
	AND	it.language_fk = get.lan
	AND	i.publish_date < sysdate
	AND	i.end_date > sysdate
	AND	i.trade_type_fk = v_trade_type_pk
	AND	i.item_type_fk = v_item_type_pk
	AND	(
			UPPER(it.title) LIKE '%'||UPPER(v_string)||'%' OR
			UPPER(it.body) LIKE '%'||UPPER(v_string)||'%' OR
			UPPER(it.price) LIKE '%'||UPPER(v_string)||'%' OR
			UPPER(it.other) LIKE '%'||UPPER(v_string)||'%'
		)
	;


	CURSOR	select_all_2
		(
			v_trade_type_pk		in trade_type.trade_type_pk%type,
			v_string		in varchar2
		) IS
	SELECT	item_pk, title, trade_type_fk, item_type_fk
	FROM	item_text it, item i
	WHERE	it.item_fk = i.item_pk
	AND	it.language_fk = get.lan
	AND	i.publish_date < sysdate
	AND	i.end_date > sysdate
	AND	i.trade_type_fk = v_trade_type_pk
	AND	(
			UPPER(it.title) LIKE '%'||UPPER(v_string)||'%' OR
			UPPER(it.body) LIKE '%'||UPPER(v_string)||'%' OR
			UPPER(it.price) LIKE '%'||UPPER(v_string)||'%' OR
			UPPER(it.other) LIKE '%'||UPPER(v_string)||'%'
		)
	;


	CURSOR	select_all_3
		(
			v_item_type_pk		in item_type.item_type_pk%type,
			v_string		in varchar2
		) IS
	SELECT	item_pk, title, trade_type_fk, item_type_fk
	FROM	item_text it, item i
	WHERE	it.item_fk = i.item_pk
	AND	it.language_fk = get.lan
	AND	i.publish_date < sysdate
	AND	i.end_date > sysdate
	AND	i.item_type_fk = v_item_type_pk
	AND	(
			UPPER(it.title) LIKE '%'||UPPER(v_string)||'%' OR
			UPPER(it.body) LIKE '%'||UPPER(v_string)||'%' OR
			UPPER(it.price) LIKE '%'||UPPER(v_string)||'%' OR
			UPPER(it.other) LIKE '%'||UPPER(v_string)||'%'
		)
	;


	CURSOR	select_all_4
		(
			v_string		in varchar2
		) IS
	SELECT	item_pk, title, trade_type_fk, item_type_fk
	FROM	item_text it, item i
	WHERE	it.item_fk = i.item_pk
	AND	it.language_fk = get.lan
	AND	i.publish_date < sysdate
	AND	i.end_date > sysdate
	AND	(
			UPPER(it.title) LIKE '%'||UPPER(v_string)||'%' OR
			UPPER(it.body) LIKE '%'||UPPER(v_string)||'%' OR
			UPPER(it.price) LIKE '%'||UPPER(v_string)||'%' OR
			UPPER(it.other) LIKE '%'||UPPER(v_string)||'%'
		)
	;


	v_item_pk		item.item_pk%type			default NULL;
	v_title			item_text.title%type			default NULL;
	v_item_type_pk		item_type.item_type_pk%type		default NULL;
	v_trade_type_pk		trade_type.trade_type_pk%type		default NULL;

BEGIN


	if ( p_type IS NULL) then
		html.b_box_2( get.txt('search_items'), '100%', 'bboard.search');
		html.b_form('bboard.search', 'search_form');
		htp.p('<input type="hidden" name="p_type" value="show_result">');
		html.b_table;
		htp.p('
			<tr><td colspan="2">'|| get.txt('bboard_search_desc') ||'</td></tr>
			<tr><td>'|| get.txt('trade_type') ||':</td><td>'); bboard.select_trade_type(p_trade_type_pk, 'all'); htp.p('</td></tr>
			<tr><td>'|| get.txt('item_type') ||':</td><td>'); bboard.select_item_type(p_item_type_pk, 'all'); htp.p('</td></tr>
			<tr><td>'|| get.txt('bboard_string') ||':</td><td>'|| html.text('p_string', 15, 200, p_string) ||'</td></tr>
			<tr><td colspan="2" align="right">
		');
		html.submit_link( get.txt('search_items') );
		htp.p('</td></tr>');
		html.e_table;
		html.e_form;
		html.e_box_2;
	else
		html.b_page;
		htp.p('<table cellspacing="0">');
		-- ---------------------------
		-- skriver ut venstre stolpe
		-- ---------------------------
		htp.p('<tr><td valign="top" width="10%">');
		left_pillar(p_trade_type_pk, p_item_type_pk, p_string);
		htp.p('</td><td valign="top">');

		-- ----------------------------------------
		-- skriver ut h�yre stolpe (hoved innhold)
		-- ----------------------------------------
		html.b_box( get.txt('item_search'), '100%', 'bboard.search(result)');
		html.b_table;

		htp.p('<tr><td width="30%"><b>'|| get.txt('trade_type') ||':</b></td><td width="50%"><b>'|| get.txt('item_title') ||':</b></td><td width="20%"><b>'|| get.txt('item_type') ||':</b></td></tr>');

		if (p_trade_type_pk IS NOT NULL AND p_item_type_pk IS NOT NULL) then
			open select_all(p_trade_type_pk, p_item_type_pk, p_string);
			loop
				fetch select_all into v_item_pk, v_title, v_trade_type_pk, v_item_type_pk;
				exit when select_all%NOTFOUND;

				htp.p('<tr><td>'|| get.bb_text(v_trade_type_pk, 'trade_type') ||'</td><td><a href="bboard.view_item?p_item_pk='||v_item_pk||'&p_type=normal">'|| v_title ||'</a></td><td>'|| get.bb_text(v_item_type_pk, 'item_type') ||'</td></tr>');

			end loop;
			close select_all;

		elsif (p_trade_type_pk IS NOT NULL AND p_item_type_pk IS NULL) then
			open select_all_2(p_trade_type_pk, p_string);
			loop
				fetch select_all_2 into v_item_pk, v_title, v_trade_type_pk, v_item_type_pk;
				exit when select_all_2%NOTFOUND;

				htp.p('<tr><td>'|| get.bb_text(v_trade_type_pk, 'trade_type') ||'</td><td><a href="bboard.view_item?p_item_pk='||v_item_pk||'&p_type=normal">'|| v_title ||'</a></td><td>'|| get.bb_text(v_item_type_pk, 'item_type') ||'</td></tr>');

			end loop;
			close select_all_2;

		elsif (p_trade_type_pk IS NULL AND p_item_type_pk IS NOT NULL) then
			open select_all_3(p_item_type_pk, p_string);
			loop
				fetch select_all_3 into v_item_pk, v_title, v_trade_type_pk, v_item_type_pk;
				exit when select_all_3%NOTFOUND;

				htp.p('<tr><td>'|| get.bb_text(v_trade_type_pk, 'trade_type') ||'</td><td><a href="bboard.view_item?p_item_pk='||v_item_pk||'&p_type=normal">'|| v_title ||'</a></td><td>'|| get.bb_text(v_item_type_pk, 'item_type') ||'</td></tr>');

			end loop;
			close select_all_3;

		elsif (p_trade_type_pk IS NULL AND p_item_type_pk IS NULL) then
			open select_all_4( p_string );
			loop
				fetch select_all_4 into v_item_pk, v_title, v_trade_type_pk, v_item_type_pk;
				exit when select_all_4%NOTFOUND;

				htp.p('<tr><td>'|| get.bb_text(v_trade_type_pk, 'trade_type') ||'</td><td><a href="bboard.view_item?p_item_pk='||v_item_pk||'&p_type=normal">'|| v_title ||'</a></td><td>'|| get.bb_text(v_item_type_pk, 'item_type') ||'</td></tr>');

			end loop;
			close select_all_4;

		end if;

		html.e_table;
		html.e_box;

		-- ----------------------
		-- avlutter h�yre stolpe
		-- -----------------------
		htp.p('</td></tr>');

		html.e_table;
		html.e_page;
	end if;

END;
END search;




-- ++++++++++++++++++++++++++++++++++++++++++++++ --

END; -- slutter pakke kroppen
/
show errors;
