set define off
PROMPT *** package: MENU_I4 ***

CREATE OR REPLACE PACKAGE menu_i4 IS

    PROCEDURE startup;
    PROCEDURE menu_type_adm
		(
			p_task			in varchar2						default 'list',
			p_menu_type_pk	in menu_type.menu_type_pk%type	default NULL,
			p_description	in menu_type.description%type	default NULL,
			p_name			in string_group.name%type		default NULL
		);
	PROCEDURE serving_period_adm
		(
			p_task				in varchar2									default 'list',
			p_serving_period_pk	in serving_period.serving_period_pk%type	default NULL,
			p_description		in serving_period.description%type			default NULL,
			p_name				in string_group.name%type					default NULL
		);
	PROCEDURE menu_object_type_adm
		(
			p_task					in varchar2										default 'list',
			p_menu_object_type_pk	in menu_object_type.menu_object_type_pk%type	default NULL,
			p_description			in menu_object_type.description%type			default NULL,
			p_menu_type_pk			in menu_type.menu_type_pk%type					default NULL,
			p_name					in string_group.name%type						default NULL
		);
	PROCEDURE menu_serving_type_adm
		(
			p_task					in varchar2										default 'list',
			p_menu_serving_type_pk	in menu_serving_type.menu_serving_type_pk%type	default NULL,
			p_description			in menu_serving_type.description%type			default NULL,
			p_menu_type_pk			in menu_type.menu_type_pk%type					default NULL,
			p_name					in string_group.name%type						default NULL
		);
	PROCEDURE compare_list_adm
		(
			p_task				in varchar2									default 'list',
			p_compare_list_pk	in compare_list.compare_list_pk%type		default NULL,
			p_description		in compare_list.description%type			default NULL,
			p_name				in string_group.name%type					default NULL
		);

END;
/
show errors;


CREATE OR REPLACE PACKAGE BODY menu_i4 IS

---------------------------------------------------------
-- Name:        startup
-- Type:        procedure
-- What:        start procedure
-- Author:      Frode Klevstul
-- Start date:  30.07.2001
-- Desc:
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN
	htp.p('
		<h2>menu_i4.sql</h2>
		1) <a href="menu_i4.menu_type_adm">meny_type_adm</a> <br>
		2) <a href="menu_i4.serving_period_adm">serving_period_adm</a> <br>
		3) <a href="menu_i4.menu_object_type_adm">menu_object_type_adm</a> <br>
		4) <a href="menu_i4.menu_serving_type_adm">menu_serving_type_adm</a> <br>
		5) <a href="menu_i4.compare_list_adm">compare_list_adm</a> <br>
	');
END startup;




---------------------------------------------------------
-- Name:        menu_type_adm
-- Type:        procedure
-- What:        administrate menu_type table
-- Author:      Frode Klevstul
-- Start date:  06.08.2001
-- Desc:
---------------------------------------------------------
PROCEDURE menu_type_adm
	(
		p_task			in varchar2						default 'list',
		p_menu_type_pk	in menu_type.menu_type_pk%type	default NULL,
		p_description	in menu_type.description%type	default NULL,
		p_name			in string_group.name%type		default NULL
	)
IS
BEGIN
DECLARE
	
	CURSOR 	c_select_all IS
	SELECT 	menu_type_pk, name_sg_fk, description, activated
	FROM	menu_type;

	v_menu_type_pk		menu_type.menu_type_pk%type			default NULL;
	v_name_sg_fk		menu_type.name_sg_fk%type			default NULL;
	v_description		menu_type.description%type			default NULL;
	v_activated			menu_type.activated%type			default NULL;

	v_name				string_group.name%type				default NULL;

	v_check				number								default NULL;
	v_string_group_pk	string_group.string_group_pk%type	default NULL;

BEGIN
if ( login.timeout('menu_i4.menu_type_adm')>0 and login.right('menu_i4')>0 ) then

	html.b_adm_page('menu_i4.menu_type_adm');

	-- ------------------------------------------------------------------
	-- p_task = 'list': lister ut alle eksisterende forekomster
	-- ------------------------------------------------------------------
	if (p_task = 'list') then

		-- ---
		-- lister ut alle forekomster
		-- ---
		html.b_box('All menues');
		html.b_table;

		htp.p('
			<tr>
				<td><b>pk:</b></td>
				<td><b>name:</b></td>
				<td><b>status:</b></td>
				<td><b>update:</b></td>
				<td><b>delete:</b></td>
			</tr>
		');

		open c_select_all;
		loop
			fetch c_select_all into v_menu_type_pk, v_name_sg_fk, v_description, v_activated;
			exit when c_select_all%NOTFOUND;
			htp.p('
				<tr>
					<td>'|| v_menu_type_pk ||'</td>
					<td><a href="multilang.list_all?p_name='||get.sg_name(v_name_sg_fk)||'">'|| get.sg_name(v_name_sg_fk) ||'</a></td><td>');

					if (v_activated IS NULL) then
						html.button_link('deactivated','menu_i4.menu_type_adm?p_task=activate&p_menu_type_pk='||v_menu_type_pk);
					else
						html.button_link('activated','menu_i4.menu_type_adm?p_task=deactivate&p_menu_type_pk='||v_menu_type_pk);
					end if;
					
					htp.p('</td><td>');
					html.button_link('update', 'menu_i4.menu_type_adm?p_task=update&p_menu_type_pk='||v_menu_type_pk);
					htp.p('</td><td>');
					html.button_link('delete', 'menu_i4.menu_type_adm?p_task=delete&p_menu_type_pk='||v_menu_type_pk);
			
			htp.p('
					</td>
				</tr>
			');
		end loop;
		close c_select_all;

		html.e_table;
		html.e_box;

		htp.p('<br><br>');

		-- ---
		-- form for � legge inn ny forekomst
		-- ---
		html.b_box('New menu', '50%');
		html.b_form('menu_i4.menu_type_adm');
		html.b_table;
		htp.p('
			<input type="hidden" name="p_task" value="insert">
			<tr>
				<td>Menu name:</td><td>'|| html.text('p_name', 30, 50) ||'</td>
			</tr>
			<tr>
				<td>Description:</td><td>'|| html.text('p_description', 50, 150) ||'</td>
			</tr>
			<tr>
				<td colspan="2" align="right">'); html.submit_link('Submit'); htp.p('</td>
			</tr>
		');
		html.e_table;
		html.e_form;
		html.e_box;
		

	-- ------------------------------------------------------------------
	-- p_task = 'insert': legger inn i databasen
	-- ------------------------------------------------------------------
	elsif (p_task = 'insert') then
		
		-- ---
		-- sjekker om forekomsten i "multilang" finnes fra f�r
		-- ---
		SELECT 	count(*) INTO v_check
		FROM 	string_group
		WHERE 	name = p_name;
		
		if (v_check > 0) then
			htp.p( get.msg(1, 'there already exists an entry with name = <b>'''||p_name||'''</b>') );
			return;
		end if;
		
		-- ---
		-- henter ut prim�rn�kler av sekvensen
		-- ---
		SELECT	menu_type_seq.NEXTVAL
		INTO	v_menu_type_pk
		FROM	dual;

		SELECT 	string_group_seq.NEXTVAL
		INTO	v_string_group_pk
		FROM 	dual;

		-- ---
		-- insert statements
		-- ---
		INSERT INTO string_group
		(string_group_pk, name, description)
		VALUES (v_string_group_pk, p_name, '[entry inserted by menu_i4 package]');
		commit;

		INSERT INTO menu_type
		(menu_type_pk, name_sg_fk, description, activated)
		VALUES (v_menu_type_pk, v_string_group_pk, p_description, NULL);
		commit;

		html.jump_to('menu_i4.menu_type_adm');


	-- ------------------------------------------------------------------
	-- p_task = 'update': oppdaterer forekomst i databasen
	-- ------------------------------------------------------------------
	elsif ( owa_pattern.match(p_task,'update') ) then


		if ( owa_pattern.match(p_task,'update_action') ) then

			SELECT	count(*) INTO v_check
			FROM	string_group
			WHERE	name = p_name
			AND		string_group_pk <> 
				( 	SELECT	name_sg_fk
					FROM	menu_type
					WHERE	menu_type_pk = p_menu_type_pk
				);

			if (v_check > 0) then
				htp.p( get.msg(1, 'there already exists an entry with name = <b>'''||p_name||'''</b>') );
				return;
			end if;
	

			UPDATE	string_group
			SET		name = p_name
			WHERE	string_group_pk = 
				(
					SELECT	name_sg_fk
					FROM	menu_type
					WHERE	menu_type_pk = p_menu_type_pk
				);
			commit;

			UPDATE	menu_type
			SET		description	= p_description
			WHERE	menu_type_pk = p_menu_type_pk;
			commit;

			html.jump_to('menu_i4.menu_type_adm');

		else
			
			SELECT	sg.name, mt.description, mt.activated
			INTO	v_name, v_description, v_activated
			FROM	string_group sg, menu_type mt
			WHERE	sg.string_group_pk = mt.name_sg_fk
			AND		menu_type_pk = p_menu_type_pk;

			-- ---
			-- form for � oppdatere forekomst
			-- ---
			html.b_box('Update menu', '50%');
			html.b_form('menu_i4.menu_type_adm');
			html.b_table;
			htp.p('
				<input type="hidden" name="p_task" value="update_action">
				<input type="hidden" name="p_menu_type_pk" value="'|| p_menu_type_pk ||'">
				<tr>
					<td>Menu name:</td><td>'|| html.text('p_name', 30, 50, v_name) ||'</td>
				</tr>
				<tr>
					<td>Description:</td><td>'|| html.text('p_description', 50, 150, v_description) ||'</td>
				</tr>
				<tr>
					<td colspan="2" align="right">'); html.submit_link('Submit'); htp.p('</td>
				</tr>
			');
			html.e_table;
			html.e_form;
			html.e_box;


		end if;


	-- ------------------------------------------------------------------
	-- p_task = 'delete': sletter fra databasen
	-- ------------------------------------------------------------------
	elsif ( owa_pattern.match(p_task,'delete') ) then

		if ( owa_pattern.match(p_task,'delete_action') ) then

			-- henter info og sletter riktig forekomst fra multilang modulen
			SELECT	name_sg_fk
			INTO	v_name_sg_fk
			FROM	menu_type
			WHERE	menu_type_pk = p_menu_type_pk;

			-- cascade delete: holder � slette fra "groups" tabellen
			DELETE FROM string_group
			WHERE string_group_pk = v_name_sg_fk;
			commit;

			DELETE FROM groups
			WHERE string_group_fk = v_name_sg_fk;
			commit;

			-- strengt tatt er denne "deleten" un�dvendig grunnet "cascade delete"
			DELETE FROM menu_type
			WHERE menu_type_pk = p_menu_type_pk;
			commit;

			html.jump_to('menu_i4.menu_type_adm');

		else

			SELECT	name_sg_fk
			INTO	v_name_sg_fk
			FROM	menu_type
			WHERE	menu_type_pk = p_menu_type_pk;

			html.b_box('Delete menu', '50%');
			html.b_form('menu_i4.menu_type_adm');
			html.b_table;
		
	        htp.p('
	                <input type="hidden" name="p_task" value="delete_action">
	                <input type="hidden" name="p_menu_type_pk" value="'|| p_menu_type_pk ||'">
	                <tr><td> Are you sure you want to delete the menu '''|| get.sg_name(v_name_sg_fk) ||'''?</td></tr>
	                <tr><td>'); html.submit_link( 'delete menu' ); htp.p('</td></tr>
	                <tr><td>'); html.back( 'don''t delete menu' ); htp.p('</td></tr>
	        ');
	
			html.e_table;
			html.e_form;
			html.e_box;


		end if;


	-- ------------------------------------------------------------------
	-- p_task = 'activate': aktiver (deaktiver) meny
	-- ------------------------------------------------------------------
	elsif ( owa_pattern.match(p_task,'activate') ) then

		if ( owa_pattern.match(p_task,'deactivate') ) then
			UPDATE	menu_type
			SET		activated = NULL
			WHERE	menu_type_pk = p_menu_type_pk;
			commit;
		else
			UPDATE	menu_type
			SET		activated = 1
			WHERE	menu_type_pk = p_menu_type_pk;
			commit;
		end if;

		html.jump_to('menu_i4.menu_type_adm');

	end if;


	html.e_adm_page;


end if;
END;
END menu_type_adm;










---------------------------------------------------------
-- Name:        serving_period_adm
-- Type:        procedure
-- What:        administrate serving_period table
-- Author:      Frode Klevstul
-- Start date:  22.08.2001
-- Desc:
---------------------------------------------------------
PROCEDURE serving_period_adm
	(
		p_task				in varchar2									default 'list',
		p_serving_period_pk	in serving_period.serving_period_pk%type	default NULL,
		p_description		in serving_period.description%type			default NULL,
		p_name				in string_group.name%type					default NULL
	)
IS
BEGIN
DECLARE
	
	CURSOR 	c_select_all IS
	SELECT 	serving_period_pk, name_sg_fk, description
	FROM	serving_period;

	v_serving_period_pk	serving_period.serving_period_pk%type	default NULL;
	v_name_sg_fk		serving_period.name_sg_fk%type			default NULL;
	v_description		serving_period.description%type			default NULL;

	v_name				string_group.name%type					default NULL;

	v_check				number									default NULL;
	v_string_group_pk	string_group.string_group_pk%type		default NULL;

BEGIN
if ( login.timeout('menu_i4.serving_period_adm')>0 and login.right('menu_i4')>0 ) then

	html.b_adm_page('menu_i4.serving_period_adm');

	-- ------------------------------------------------------------------
	-- p_task = 'list': lister ut alle eksisterende forekomster
	-- ------------------------------------------------------------------
	if (p_task = 'list') then

		-- ---
		-- lister ut alle forekomster
		-- ---
		html.b_box('All serving_periods');
		html.b_table;

		htp.p('
			<tr>
				<td><b>pk:</b></td>
				<td><b>name:</b></td>
				<td><b>update:</b></td>
				<td><b>delete:</b></td>
			</tr>
		');

		open c_select_all;
		loop
			fetch c_select_all into v_serving_period_pk, v_name_sg_fk, v_description;
			exit when c_select_all%NOTFOUND;
			htp.p('
				<tr>
					<td>'|| v_serving_period_pk ||'</td>
					<td><a href="multilang.list_all?p_name='||get.sg_name(v_name_sg_fk)||'">'|| get.sg_name(v_name_sg_fk) ||'</a>');
					htp.p('</td><td>');
					html.button_link('update', 'menu_i4.serving_period_adm?p_task=update&p_serving_period_pk='||v_serving_period_pk);
					htp.p('</td><td>');
					html.button_link('delete', 'menu_i4.serving_period_adm?p_task=delete&p_serving_period_pk='||v_serving_period_pk);
			htp.p('
					</td>
				</tr>
			');
		end loop;
		close c_select_all;

		html.e_table;
		html.e_box;

		htp.p('<br><br>');

		-- ---
		-- form for � legge inn ny forekomst
		-- ---
		html.b_box('New serving_period', '50%');
		html.b_form('menu_i4.serving_period_adm');
		html.b_table;
		htp.p('
			<input type="hidden" name="p_task" value="insert">
			<tr>
				<td>serving_period name:</td><td>'|| html.text('p_name', 30, 50) ||'</td>
			</tr>
			<tr>
				<td>Description:</td><td>'|| html.text('p_description', 50, 150) ||'</td>
			</tr>
			<tr>
				<td colspan="2" align="right">'); html.submit_link('Submit'); htp.p('</td>
			</tr>
		');
		html.e_table;
		html.e_form;
		html.e_box;
		

	-- ------------------------------------------------------------------
	-- p_task = 'insert': legger inn i databasen
	-- ------------------------------------------------------------------
	elsif (p_task = 'insert') then
		
		-- ---
		-- sjekker om forekomsten i "multilang" finnes fra f�r
		-- ---
		SELECT 	count(*) INTO v_check
		FROM 	string_group
		WHERE 	name = p_name;
		
		if (v_check > 0) then
			htp.p( get.msg(1, 'there already exists an entry with name = <b>'''||p_name||'''</b>') );
			return;
		end if;
		
		-- ---
		-- henter ut prim�rn�kler av sekvensen
		-- ---
		SELECT	serving_period_seq.NEXTVAL
		INTO	v_serving_period_pk
		FROM	dual;

		SELECT 	string_group_seq.NEXTVAL
		INTO	v_string_group_pk
		FROM 	dual;

		-- ---
		-- insert statements
		-- ---
		INSERT INTO string_group
		(string_group_pk, name, description)
		VALUES (v_string_group_pk, p_name, '[entry inserted by menu_i4 package]');
		commit;

		INSERT INTO serving_period
		(serving_period_pk, name_sg_fk, description)
		VALUES (v_serving_period_pk, v_string_group_pk, p_description);
		commit;

		html.jump_to('menu_i4.serving_period_adm');


	-- ------------------------------------------------------------------
	-- p_task = 'update': oppdaterer forekomst i databasen
	-- ------------------------------------------------------------------
	elsif ( owa_pattern.match(p_task,'update') ) then


		if ( owa_pattern.match(p_task,'update_action') ) then

			SELECT	count(*) INTO v_check
			FROM	string_group
			WHERE	name = p_name
			AND		string_group_pk <> 
				( 	SELECT	name_sg_fk
					FROM	serving_period
					WHERE	serving_period_pk = p_serving_period_pk
				);

			if (v_check > 0) then
				htp.p( get.msg(1, 'there already exists an entry with name = <b>'''||p_name||'''</b>') );
				return;
			end if;
	

			UPDATE	string_group
			SET		name = p_name
			WHERE	string_group_pk = 
				(
					SELECT	name_sg_fk
					FROM	serving_period
					WHERE	serving_period_pk = p_serving_period_pk
				);
			commit;

			UPDATE	serving_period
			SET		description	= p_description
			WHERE	serving_period_pk = p_serving_period_pk;
			commit;

			html.jump_to('menu_i4.serving_period_adm');

		else
			
			SELECT	sg.name, sp.description
			INTO	v_name, v_description
			FROM	string_group sg, serving_period sp
			WHERE	sg.string_group_pk = sp.name_sg_fk
			AND		serving_period_pk = p_serving_period_pk;

			-- ---
			-- form for � oppdatere forekomst
			-- ---
			html.b_box('Update serving_period', '50%');
			html.b_form('menu_i4.serving_period_adm');
			html.b_table;
			htp.p('
				<input type="hidden" name="p_task" value="update_action">
				<input type="hidden" name="p_serving_period_pk" value="'|| p_serving_period_pk ||'">
				<tr>
					<td>serving_period name:</td><td>'|| html.text('p_name', 30, 50, v_name) ||'</td>
				</tr>
				<tr>
					<td>Description:</td><td>'|| html.text('p_description', 50, 150, v_description) ||'</td>
				</tr>
				<tr>
					<td colspan="2" align="right">'); html.submit_link('Submit'); htp.p('</td>
				</tr>
			');
			html.e_table;
			html.e_form;
			html.e_box;


		end if;


	-- ------------------------------------------------------------------
	-- p_task = 'delete': sletter fra databasen
	-- ------------------------------------------------------------------
	elsif ( owa_pattern.match(p_task,'delete') ) then

		if ( owa_pattern.match(p_task,'delete_action') ) then

			-- henter info og sletter riktig forekomst fra multilang modulen
			SELECT	name_sg_fk
			INTO	v_name_sg_fk
			FROM	serving_period
			WHERE	serving_period_pk = p_serving_period_pk;

			DELETE FROM string_group
			WHERE string_group_pk = v_name_sg_fk;
			commit;

			DELETE FROM groups
			WHERE string_group_fk = v_name_sg_fk;
			commit;

			-- strengt tatt er denne "deleten" un�dvendig grunnet "cascade delete"
			DELETE FROM serving_period
			WHERE serving_period_pk = p_serving_period_pk;
			commit;			

			html.jump_to('menu_i4.serving_period_adm');

		else

			SELECT	name_sg_fk
			INTO	v_name_sg_fk
			FROM	serving_period
			WHERE	serving_period_pk = p_serving_period_pk;

			html.b_box('Delete serving_period', '50%');
			html.b_form('menu_i4.serving_period_adm');
			html.b_table;
		
	        htp.p('
	                <input type="hidden" name="p_task" value="delete_action">
	                <input type="hidden" name="p_serving_period_pk" value="'|| p_serving_period_pk ||'">
	                <tr><td> Are you sure you want to delete the serving_period '''|| get.sg_name(v_name_sg_fk) ||'''?</td></tr>
	                <tr><td>'); html.submit_link( 'delete serving_period' ); htp.p('</td></tr>
	                <tr><td>'); html.back( 'don''t delete serving_period' ); htp.p('</td></tr>
	        ');
	
			html.e_table;
			html.e_form;
			html.e_box;


		end if;


	end if;


	html.e_adm_page;


end if;
END;
END serving_period_adm;

































---------------------------------------------------------
-- Name:        menu_object_type_adm
-- Type:        procedure
-- What:        administrate menu_object_type table
-- Author:      Frode Klevstul
-- Start date:  22.08.2001
-- Desc:
---------------------------------------------------------
PROCEDURE menu_object_type_adm
	(
		p_task					in varchar2										default 'list',
		p_menu_object_type_pk	in menu_object_type.menu_object_type_pk%type	default NULL,
		p_description			in menu_object_type.description%type			default NULL,
		p_menu_type_pk			in menu_type.menu_type_pk%type					default NULL,
		p_name					in string_group.name%type						default NULL
	)
IS
BEGIN
DECLARE
	
	CURSOR 	c_select_all IS
	SELECT 	mot.menu_object_type_pk, mot.name_sg_fk, mot.description, mt.name_sg_fk
	FROM	menu_object_type mot, menu_type mt
	WHERE	mot.menu_type_fk = mt.menu_type_pk;

	v_menu_object_type_pk	menu_object_type.menu_object_type_pk%type	default NULL;
	v_name_sg_fk			menu_object_type.name_sg_fk%type			default NULL;
	v_description			menu_object_type.description%type			default NULL;
	v_name_sg_fk_mt			menu_type.name_sg_fk%type					default NULL;
	v_menu_type_pk			menu_type.menu_type_pk%type					default NULL;

	v_name					string_group.name%type						default NULL;

	v_check					number										default NULL;
	v_string_group_pk		string_group.string_group_pk%type			default NULL;

BEGIN
if ( login.timeout('menu_i4.menu_object_type_adm')>0 and login.right('menu_i4')>0 ) then

	html.b_adm_page('menu_i4.menu_object_type_adm');

	-- ------------------------------------------------------------------
	-- p_task = 'list': lister ut alle eksisterende forekomster
	-- ------------------------------------------------------------------
	if (p_task = 'list') then

		-- ---
		-- lister ut alle forekomster
		-- ---
		html.b_box('All menu_object_types');
		html.b_table;

		htp.p('
			<tr>
				<td><b>pk:</b></td>
				<td><b>name:</b></td>
				<td><b>menu:</b></td>
				<td><b>update:</b></td>
				<td><b>delete:</b></td>
			</tr>
		');

		open c_select_all;
		loop
			fetch c_select_all into v_menu_object_type_pk, v_name_sg_fk, v_description, v_name_sg_fk_mt;
			exit when c_select_all%NOTFOUND;
			htp.p('
				<tr>
					<td>'|| v_menu_object_type_pk ||'</td>
					<td><a href="multilang.list_all?p_name='||get.sg_name(v_name_sg_fk)||'">'|| get.sg_name(v_name_sg_fk) ||'</a>');
					htp.p('</td><td>'|| get.sg_name(v_name_sg_fk_mt) ||'</td><td>');
					html.button_link('update', 'menu_i4.menu_object_type_adm?p_task=update&p_menu_object_type_pk='||v_menu_object_type_pk);
					htp.p('</td><td>');
					html.button_link('delete', 'menu_i4.menu_object_type_adm?p_task=delete&p_menu_object_type_pk='||v_menu_object_type_pk);
			htp.p('
					</td>
				</tr>
			');
		end loop;
		close c_select_all;

		html.e_table;
		html.e_box;

		htp.p('<br><br>');

		-- ---
		-- form for � legge inn ny forekomst
		-- ---
		html.b_box('New menu_object_type', '60%');
		html.b_form('menu_i4.menu_object_type_adm');
		html.b_table;
		htp.p('
			<input type="hidden" name="p_task" value="insert">
			<tr>
				<td>menu_object_type name:</td><td>'|| html.text('p_name', 30, 50) ||'</td>
			</tr>
			<tr>
				<td>Description:</td><td>'|| html.text('p_description', 50, 150) ||'</td>
			</tr>
			<tr>
				<td>Menu relation:</td><td>'); html.select_menu_type(NULL, 1); htp.p('</td>
			</tr>
			<tr>
				<td colspan="2" align="right">'); html.submit_link('Submit'); htp.p('</td>
			</tr>
		');
		html.e_table;
		html.e_form;
		html.e_box;
		

	-- ------------------------------------------------------------------
	-- p_task = 'insert': legger inn i databasen
	-- ------------------------------------------------------------------
	elsif (p_task = 'insert') then
		
		-- ---
		-- sjekker om forekomsten i "multilang" finnes fra f�r
		-- ---
		SELECT 	count(*) INTO v_check
		FROM 	string_group
		WHERE 	name = p_name;
		
		if (v_check > 0) then
			htp.p( get.msg(1, 'there already exists an entry with name = <b>'''||p_name||'''</b>') );
			return;
		end if;
		
		-- ---
		-- henter ut prim�rn�kler av sekvensen
		-- ---
		SELECT	menu_object_type_seq.NEXTVAL
		INTO	v_menu_object_type_pk
		FROM	dual;

		SELECT 	string_group_seq.NEXTVAL
		INTO	v_string_group_pk
		FROM 	dual;

		-- ---
		-- insert statements
		-- ---
		INSERT INTO string_group
		(string_group_pk, name, description)
		VALUES (v_string_group_pk, p_name, '[entry inserted by menu_i4 package]');
		commit;

		INSERT INTO menu_object_type
		(menu_object_type_pk, name_sg_fk, description, menu_type_fk)
		VALUES (v_menu_object_type_pk, v_string_group_pk, p_description, p_menu_type_pk);
		commit;

		html.jump_to('menu_i4.menu_object_type_adm');


	-- ------------------------------------------------------------------
	-- p_task = 'update': oppdaterer forekomst i databasen
	-- ------------------------------------------------------------------
	elsif ( owa_pattern.match(p_task,'update') ) then


		if ( owa_pattern.match(p_task,'update_action') ) then

			SELECT	count(*) INTO v_check
			FROM	string_group
			WHERE	name = p_name
			AND		string_group_pk <> 
				( 	SELECT	name_sg_fk
					FROM	menu_object_type
					WHERE	menu_object_type_pk = p_menu_object_type_pk
				);

			if (v_check > 0) then
				htp.p( get.msg(1, 'there already exists an entry with name = <b>'''||p_name||'''</b>') );
				return;
			end if;
	

			UPDATE	string_group
			SET		name = p_name
			WHERE	string_group_pk = 
				(
					SELECT	name_sg_fk
					FROM	menu_object_type
					WHERE	menu_object_type_pk = p_menu_object_type_pk
				);
			commit;

			UPDATE	menu_object_type
			SET		description	= p_description,
					menu_type_fk = p_menu_type_pk
			WHERE	menu_object_type_pk = p_menu_object_type_pk;
			commit;

			html.jump_to('menu_i4.menu_object_type_adm');

		else
			
			SELECT	sg.name, mot.description, mot.menu_type_fk
			INTO	v_name, v_description, v_menu_type_pk
			FROM	string_group sg, menu_object_type mot
			WHERE	sg.string_group_pk = mot.name_sg_fk
			AND		menu_object_type_pk = p_menu_object_type_pk;

			-- ---
			-- form for � oppdatere forekomst
			-- ---
			html.b_box('Update menu_object_type', '60%');
			html.b_form('menu_i4.menu_object_type_adm');
			html.b_table;
			htp.p('
				<input type="hidden" name="p_task" value="update_action">
				<input type="hidden" name="p_menu_object_type_pk" value="'|| p_menu_object_type_pk ||'">
				<tr>
					<td>menu_object_type name:</td><td>'|| html.text('p_name', 30, 50, v_name) ||'</td>
				</tr>
				<tr>
					<td>Description:</td><td>'|| html.text('p_description', 50, 150, v_description) ||'</td>
				</tr>
				<tr>
					<td>Menu relation:</td><td>'); html.select_menu_type(v_menu_type_pk, 1); htp.p('</td>
				</tr>
				<tr>
					<td colspan="2" align="right">'); html.submit_link('Submit'); htp.p('</td>
				</tr>
			');
			html.e_table;
			html.e_form;
			html.e_box;


		end if;


	-- ------------------------------------------------------------------
	-- p_task = 'delete': sletter fra databasen
	-- ------------------------------------------------------------------
	elsif ( owa_pattern.match(p_task,'delete') ) then

		if ( owa_pattern.match(p_task,'delete_action') ) then

			-- henter info og sletter riktig forekomst fra multilang modulen
			SELECT	name_sg_fk
			INTO	v_name_sg_fk
			FROM	menu_object_type
			WHERE	menu_object_type_pk = p_menu_object_type_pk;

			DELETE FROM string_group
			WHERE string_group_pk = v_name_sg_fk;
			commit;

			DELETE FROM groups
			WHERE string_group_fk = v_name_sg_fk;
			commit;

			-- strengt tatt er denne "deleten" un�dvendig grunnet "cascade delete"
			DELETE FROM menu_object_type
			WHERE menu_object_type_pk = p_menu_object_type_pk;
			commit;			

			html.jump_to('menu_i4.menu_object_type_adm');

		else

			SELECT	name_sg_fk
			INTO	v_name_sg_fk
			FROM	menu_object_type
			WHERE	menu_object_type_pk = p_menu_object_type_pk;

			html.b_box('Delete menu', '50%');
			html.b_form('menu_i4.menu_object_type_adm');
			html.b_table;
		
	        htp.p('
	                <input type="hidden" name="p_task" value="delete_action">
	                <input type="hidden" name="p_menu_object_type_pk" value="'|| p_menu_object_type_pk ||'">
	                <tr><td> Are you sure you want to delete the menu_object_type '''|| get.sg_name(v_name_sg_fk) ||'''?</td></tr>
	                <tr><td>'); html.submit_link( 'delete menu_object_type' ); htp.p('</td></tr>
	                <tr><td>'); html.back( 'don''t delete menu_object_type' ); htp.p('</td></tr>
	        ');
	
			html.e_table;
			html.e_form;
			html.e_box;


		end if;


	end if;


	html.e_adm_page;


end if;
END;
END menu_object_type_adm;











































---------------------------------------------------------
-- Name:        menu_serving_type_adm
-- Type:        procedure
-- What:        administrate "menu_serving_type" table
-- Author:      Frode Klevstul
-- Start date:  22.08.2001
-- Desc:
---------------------------------------------------------
PROCEDURE menu_serving_type_adm
	(
		p_task					in varchar2										default 'list',
		p_menu_serving_type_pk	in menu_serving_type.menu_serving_type_pk%type	default NULL,
		p_description			in menu_serving_type.description%type			default NULL,
		p_menu_type_pk			in menu_type.menu_type_pk%type					default NULL,
		p_name					in string_group.name%type						default NULL
	)
IS
BEGIN
DECLARE
	
	CURSOR 	c_select_all IS
	SELECT 	mst.menu_serving_type_pk, mst.name_sg_fk, mst.description, mt.name_sg_fk
	FROM	menu_serving_type mst, menu_type mt
	WHERE	mst.menu_type_fk = mt.menu_type_pk;

	v_menu_serving_type_pk	menu_serving_type.menu_serving_type_pk%type	default NULL;
	v_name_sg_fk			menu_serving_type.name_sg_fk%type			default NULL;
	v_description			menu_serving_type.description%type			default NULL;
	v_name_sg_fk_mt			menu_type.name_sg_fk%type					default NULL;
	v_menu_type_pk			menu_type.menu_type_pk%type					default NULL;

	v_name					string_group.name%type						default NULL;

	v_check					number										default NULL;
	v_string_group_pk		string_group.string_group_pk%type			default NULL;

BEGIN
if ( login.timeout('menu_i4.menu_serving_type_adm')>0 and login.right('menu_i4')>0 ) then

	html.b_adm_page('menu_i4.menu_serving_type_adm');

	-- ------------------------------------------------------------------
	-- p_task = 'list': lister ut alle eksisterende forekomster
	-- ------------------------------------------------------------------
	if (p_task = 'list') then

		-- ---
		-- lister ut alle forekomster
		-- ---
		html.b_box('All menu_serving_types');
		html.b_table;

		htp.p('
			<tr>
				<td><b>pk:</b></td>
				<td><b>name:</b></td>
				<td><b>menu:</b></td>
				<td><b>update:</b></td>
				<td><b>delete:</b></td>
			</tr>
		');

		open c_select_all;
		loop
			fetch c_select_all into v_menu_serving_type_pk, v_name_sg_fk, v_description, v_name_sg_fk_mt;
			exit when c_select_all%NOTFOUND;
			htp.p('
				<tr>
					<td>'|| v_menu_serving_type_pk ||'</td>
					<td><a href="multilang.list_all?p_name='||get.sg_name(v_name_sg_fk)||'">'|| get.sg_name(v_name_sg_fk) ||'</a>');
					htp.p('</td><td>'|| get.sg_name(v_name_sg_fk_mt) ||'</td><td>');
					html.button_link('update', 'menu_i4.menu_serving_type_adm?p_task=update&p_menu_serving_type_pk='||v_menu_serving_type_pk);
					htp.p('</td><td>');
					html.button_link('delete', 'menu_i4.menu_serving_type_adm?p_task=delete&p_menu_serving_type_pk='||v_menu_serving_type_pk);
			htp.p('
					</td>
				</tr>
			');
		end loop;
		close c_select_all;

		html.e_table;
		html.e_box;

		htp.p('<br><br>');

		-- ---
		-- form for � legge inn ny forekomst
		-- ---
		html.b_box('New menu_serving_type', '60%');
		html.b_form('menu_i4.menu_serving_type_adm');
		html.b_table;
		htp.p('
			<input type="hidden" name="p_task" value="insert">
			<tr>
				<td>menu_serving_type name:</td><td>'|| html.text('p_name', 30, 50) ||'</td>
			</tr>
			<tr>
				<td>Description:</td><td>'|| html.text('p_description', 50, 150) ||'</td>
			</tr>
			<tr>
				<td>Menu relation:</td><td>'); html.select_menu_type(NULL, 1); htp.p('</td>
			</tr>
			<tr>
				<td colspan="2" align="right">'); html.submit_link('Submit'); htp.p('</td>
			</tr>
		');
		html.e_table;
		html.e_form;
		html.e_box;
		

	-- ------------------------------------------------------------------
	-- p_task = 'insert': legger inn i databasen
	-- ------------------------------------------------------------------
	elsif (p_task = 'insert') then
		
		-- ---
		-- sjekker om forekomsten i "multilang" finnes fra f�r
		-- ---
		SELECT 	count(*) INTO v_check
		FROM 	string_group
		WHERE 	name = p_name;
		
		if (v_check > 0) then
			htp.p( get.msg(1, 'there already exists an entry with name = <b>'''||p_name||'''</b>') );
			return;
		end if;
		
		-- ---
		-- henter ut prim�rn�kler av sekvensen
		-- ---
		SELECT	menu_serving_type_seq.NEXTVAL
		INTO	v_menu_serving_type_pk
		FROM	dual;

		SELECT 	string_group_seq.NEXTVAL
		INTO	v_string_group_pk
		FROM 	dual;

		-- ---
		-- insert statements
		-- ---
		INSERT INTO string_group
		(string_group_pk, name, description)
		VALUES (v_string_group_pk, p_name, '[entry inserted by menu_i4 package]');
		commit;

		INSERT INTO menu_serving_type
		(menu_serving_type_pk, name_sg_fk, description, menu_type_fk)
		VALUES (v_menu_serving_type_pk, v_string_group_pk, p_description, p_menu_type_pk);
		commit;

		html.jump_to('menu_i4.menu_serving_type_adm');


	-- ------------------------------------------------------------------
	-- p_task = 'update': oppdaterer forekomst i databasen
	-- ------------------------------------------------------------------
	elsif ( owa_pattern.match(p_task,'update') ) then


		if ( owa_pattern.match(p_task,'update_action') ) then

			SELECT	count(*) INTO v_check
			FROM	string_group
			WHERE	name = p_name
			AND		string_group_pk <> 
				( 	SELECT	name_sg_fk
					FROM	menu_serving_type
					WHERE	menu_serving_type_pk = p_menu_serving_type_pk
				);

			if (v_check > 0) then
				htp.p( get.msg(1, 'there already exists an entry with name = <b>'''||p_name||'''</b>') );
				return;
			end if;
	

			UPDATE	string_group
			SET		name = p_name
			WHERE	string_group_pk = 
				(
					SELECT	name_sg_fk
					FROM	menu_serving_type
					WHERE	menu_serving_type_pk = p_menu_serving_type_pk
				);
			commit;

			UPDATE	menu_serving_type
			SET		description	= p_description,
					menu_type_fk = p_menu_type_pk
			WHERE	menu_serving_type_pk = p_menu_serving_type_pk;
			commit;

			html.jump_to('menu_i4.menu_serving_type_adm');

		else
			
			SELECT	sg.name, mst.description, mst.menu_type_fk
			INTO	v_name, v_description, v_menu_type_pk
			FROM	string_group sg, menu_serving_type mst
			WHERE	sg.string_group_pk = mst.name_sg_fk
			AND		menu_serving_type_pk = p_menu_serving_type_pk;

			-- ---
			-- form for � oppdatere forekomst
			-- ---
			html.b_box('Update menu_serving_type', '60%');
			html.b_form('menu_i4.menu_serving_type_adm');
			html.b_table;
			htp.p('
				<input type="hidden" name="p_task" value="update_action">
				<input type="hidden" name="p_menu_serving_type_pk" value="'|| p_menu_serving_type_pk ||'">
				<tr>
					<td>menu_serving_type name:</td><td>'|| html.text('p_name', 30, 50, v_name) ||'</td>
				</tr>
				<tr>
					<td>Description:</td><td>'|| html.text('p_description', 50, 150, v_description) ||'</td>
				</tr>
				<tr>
					<td>Menu relation:</td><td>'); html.select_menu_type(v_menu_type_pk, 1); htp.p('</td>
				</tr>
				<tr>
					<td colspan="2" align="right">'); html.submit_link('Submit'); htp.p('</td>
				</tr>
			');
			html.e_table;
			html.e_form;
			html.e_box;


		end if;


	-- ------------------------------------------------------------------
	-- p_task = 'delete': sletter fra databasen
	-- ------------------------------------------------------------------
	elsif ( owa_pattern.match(p_task,'delete') ) then

		if ( owa_pattern.match(p_task,'delete_action') ) then

			-- henter info og sletter riktig forekomst fra multilang modulen
			SELECT	name_sg_fk
			INTO	v_name_sg_fk
			FROM	menu_serving_type
			WHERE	menu_serving_type_pk = p_menu_serving_type_pk;

			DELETE FROM string_group
			WHERE string_group_pk = v_name_sg_fk;
			commit;

			DELETE FROM groups
			WHERE string_group_fk = v_name_sg_fk;
			commit;

			-- strengt tatt er denne "deleten" un�dvendig grunnet "cascade delete"
			DELETE FROM menu_serving_type
			WHERE menu_serving_type_pk = p_menu_serving_type_pk;
			commit;			

			html.jump_to('menu_i4.menu_serving_type_adm');

		else

			SELECT	name_sg_fk
			INTO	v_name_sg_fk
			FROM	menu_serving_type
			WHERE	menu_serving_type_pk = p_menu_serving_type_pk;

			html.b_box('Delete menu', '50%');
			html.b_form('menu_i4.menu_serving_type_adm');
			html.b_table;
		
	        htp.p('
	                <input type="hidden" name="p_task" value="delete_action">
	                <input type="hidden" name="p_menu_serving_type_pk" value="'|| p_menu_serving_type_pk ||'">
	                <tr><td> Are you sure you want to delete the menu_serving_type '''|| get.sg_name(v_name_sg_fk) ||'''?</td></tr>
	                <tr><td>'); html.submit_link( 'delete menu_serving_type' ); htp.p('</td></tr>
	                <tr><td>'); html.back( 'don''t delete menu_serving_type' ); htp.p('</td></tr>
	        ');
	
			html.e_table;
			html.e_form;
			html.e_box;


		end if;


	end if;


	html.e_adm_page;


end if;
END;
END menu_serving_type_adm;











































---------------------------------------------------------
-- Name:        compare_list_adm
-- Type:        procedure
-- What:        administrate "compare_list" table
-- Author:      Frode Klevstul
-- Start date:  23.08.2001
-- Desc:
---------------------------------------------------------
PROCEDURE compare_list_adm
	(
		p_task				in varchar2									default 'list',
		p_compare_list_pk	in compare_list.compare_list_pk%type		default NULL,
		p_description		in compare_list.description%type			default NULL,
		p_name				in string_group.name%type					default NULL
	)
IS
BEGIN
DECLARE
	
	CURSOR 	c_select_all IS
	SELECT 	compare_list_pk, name_sg_fk, description
	FROM	compare_list;

	v_compare_list_pk	compare_list.compare_list_pk%type		default NULL;
	v_name_sg_fk		compare_list.name_sg_fk%type			default NULL;
	v_description		compare_list.description%type			default NULL;

	v_name				string_group.name%type					default NULL;

	v_check				number									default NULL;
	v_string_group_pk	string_group.string_group_pk%type		default NULL;

BEGIN
if ( login.timeout('menu_i4.compare_list_adm')>0 and login.right('menu_i4')>0 ) then

	html.b_adm_page('menu_i4.compare_list_adm');

	-- ------------------------------------------------------------------
	-- p_task = 'list': lister ut alle eksisterende forekomster
	-- ------------------------------------------------------------------
	if (p_task = 'list') then

		-- ---
		-- lister ut alle forekomster
		-- ---
		html.b_box('All compare_lists');
		html.b_table;

		htp.p('
			<tr>
				<td><b>pk:</b></td>
				<td><b>name:</b></td>
				<td><b>update:</b></td>
				<td><b>delete:</b></td>
			</tr>
		');

		open c_select_all;
		loop
			fetch c_select_all into v_compare_list_pk, v_name_sg_fk, v_description;
			exit when c_select_all%NOTFOUND;
			htp.p('
				<tr>
					<td>'|| v_compare_list_pk ||'</td>
					<td><a href="multilang.list_all?p_name='||get.sg_name(v_name_sg_fk)||'">'|| get.sg_name(v_name_sg_fk) ||'</a>');
					htp.p('</td><td>');
					html.button_link('update', 'menu_i4.compare_list_adm?p_task=update&p_compare_list_pk='||v_compare_list_pk);
					htp.p('</td><td>');
					html.button_link('delete', 'menu_i4.compare_list_adm?p_task=delete&p_compare_list_pk='||v_compare_list_pk);
			htp.p('
					</td>
				</tr>
			');
		end loop;
		close c_select_all;

		html.e_table;
		html.e_box;

		htp.p('<br><br>');

		-- ---
		-- form for � legge inn ny forekomst
		-- ---
		html.b_box('New compare_list', '50%');
		html.b_form('menu_i4.compare_list_adm');
		html.b_table;
		htp.p('
			<input type="hidden" name="p_task" value="insert">
			<tr>
				<td>compare_list name:</td><td>'|| html.text('p_name', 30, 50) ||'</td>
			</tr>
			<tr>
				<td>Description:</td><td>'|| html.text('p_description', 50, 150) ||'</td>
			</tr>
			<tr>
				<td colspan="2" align="right">'); html.submit_link('Submit'); htp.p('</td>
			</tr>
		');
		html.e_table;
		html.e_form;
		html.e_box;
		

	-- ------------------------------------------------------------------
	-- p_task = 'insert': legger inn i databasen
	-- ------------------------------------------------------------------
	elsif (p_task = 'insert') then
		
		-- ---
		-- sjekker om forekomsten i "multilang" finnes fra f�r
		-- ---
		SELECT 	count(*) INTO v_check
		FROM 	string_group
		WHERE 	name = p_name;
		
		if (v_check > 0) then
			htp.p( get.msg(1, 'there already exists an entry with name = <b>'''||p_name||'''</b>') );
			return;
		end if;
		
		-- ---
		-- henter ut prim�rn�kler av sekvensen
		-- ---
		SELECT	compare_list_seq.NEXTVAL
		INTO	v_compare_list_pk
		FROM	dual;

		SELECT 	string_group_seq.NEXTVAL
		INTO	v_string_group_pk
		FROM 	dual;

		-- ---
		-- insert statements
		-- ---
		INSERT INTO string_group
		(string_group_pk, name, description)
		VALUES (v_string_group_pk, p_name, '[entry inserted by menu_i4 package]');
		commit;

		INSERT INTO compare_list
		(compare_list_pk, name_sg_fk, description)
		VALUES (v_compare_list_pk, v_string_group_pk, p_description);
		commit;

		html.jump_to('menu_i4.compare_list_adm');


	-- ------------------------------------------------------------------
	-- p_task = 'update': oppdaterer forekomst i databasen
	-- ------------------------------------------------------------------
	elsif ( owa_pattern.match(p_task,'update') ) then


		if ( owa_pattern.match(p_task,'update_action') ) then

			SELECT	count(*) INTO v_check
			FROM	string_group
			WHERE	name = p_name
			AND		string_group_pk <> 
				( 	SELECT	name_sg_fk
					FROM	compare_list
					WHERE	compare_list_pk = p_compare_list_pk
				);

			if (v_check > 0) then
				htp.p( get.msg(1, 'there already exists an entry with name = <b>'''||p_name||'''</b>') );
				return;
			end if;
	

			UPDATE	string_group
			SET		name = p_name
			WHERE	string_group_pk = 
				(
					SELECT	name_sg_fk
					FROM	compare_list
					WHERE	compare_list_pk = p_compare_list_pk
				);
			commit;

			UPDATE	compare_list
			SET		description	= p_description
			WHERE	compare_list_pk = p_compare_list_pk;
			commit;

			html.jump_to('menu_i4.compare_list_adm');

		else
			
			SELECT	sg.name, cl.description
			INTO	v_name, v_description
			FROM	string_group sg, compare_list cl
			WHERE	sg.string_group_pk = cl.name_sg_fk
			AND		compare_list_pk = p_compare_list_pk;

			-- ---
			-- form for � oppdatere forekomst
			-- ---
			html.b_box('Update compare_list', '50%');
			html.b_form('menu_i4.compare_list_adm');
			html.b_table;
			htp.p('
				<input type="hidden" name="p_task" value="update_action">
				<input type="hidden" name="p_compare_list_pk" value="'|| p_compare_list_pk ||'">
				<tr>
					<td>compare_list name:</td><td>'|| html.text('p_name', 30, 50, v_name) ||'</td>
				</tr>
				<tr>
					<td>Description:</td><td>'|| html.text('p_description', 50, 150, v_description) ||'</td>
				</tr>
				<tr>
					<td colspan="2" align="right">'); html.submit_link('Submit'); htp.p('</td>
				</tr>
			');
			html.e_table;
			html.e_form;
			html.e_box;


		end if;


	-- ------------------------------------------------------------------
	-- p_task = 'delete': sletter fra databasen
	-- ------------------------------------------------------------------
	elsif ( owa_pattern.match(p_task,'delete') ) then

		if ( owa_pattern.match(p_task,'delete_action') ) then

			-- henter info og sletter riktig forekomst fra multilang modulen
			SELECT	name_sg_fk
			INTO	v_name_sg_fk
			FROM	compare_list
			WHERE	compare_list_pk = p_compare_list_pk;

			DELETE FROM string_group
			WHERE string_group_pk = v_name_sg_fk;
			commit;

			DELETE FROM groups
			WHERE string_group_fk = v_name_sg_fk;
			commit;

			-- strengt tatt er denne "deleten" un�dvendig grunnet "cascade delete"
			DELETE FROM compare_list
			WHERE compare_list_pk = p_compare_list_pk;
			commit;			

			html.jump_to('menu_i4.compare_list_adm');

		else

			SELECT	name_sg_fk
			INTO	v_name_sg_fk
			FROM	compare_list
			WHERE	compare_list_pk = p_compare_list_pk;

			html.b_box('Delete compare_list', '50%');
			html.b_form('menu_i4.compare_list_adm');
			html.b_table;
		
	        htp.p('
	                <input type="hidden" name="p_task" value="delete_action">
	                <input type="hidden" name="p_compare_list_pk" value="'|| p_compare_list_pk ||'">
	                <tr><td> Are you sure you want to delete the compare_list '''|| get.sg_name(v_name_sg_fk) ||'''?</td></tr>
	                <tr><td>'); html.submit_link( 'delete compare_list' ); htp.p('</td></tr>
	                <tr><td>'); html.back( 'don''t delete compare_list' ); htp.p('</td></tr>
	        ');
	
			html.e_table;
			html.e_form;
			html.e_box;


		end if;


	end if;


	html.e_adm_page;


end if;
END;
END compare_list_adm;






-- ++++++++++++++++++++++++++++++++++++++++++++++ --

END; -- slutter pakke kroppen
/
show errors;
