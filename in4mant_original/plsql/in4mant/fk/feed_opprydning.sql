delete from cal_feed_log where cal_feed_log_pk in (
select CAL_FEED_LOG_PK
from calendar c, cal_feed_log cfl
where c.calendar_pk = cfl.calendar_fk
and c.event_date > sysdate
and c.event_date < sysdate + 14
)
;
commit;

