set define off
PROMPT *** package: htmf (HTML Functions) ***

CREATE OR REPLACE PACKAGE htmf IS

        empty_array     owa_util.ident_arr;
        FUNCTION getcode (
                                p_name  in html_code.name%type default NULL
                                ) RETURN varchar2;
        FUNCTION b_page (
                         p_width         in number               default get.value('width'),
                         p_reload        IN NUMBER               DEFAULT NULL,
                         p_jump          IN VARCHAR2             DEFAULT NULL,
                         p_type          IN NUMBER               DEFAULT NULL,
                         p_selected      IN NUMBER                       DEFAULT NULL,
                         p_service_pk    IN service.service_pk%TYPE      DEFAULT NULL,
                         p_language_pk   IN la.language_pk%type          default NULL
                                ) RETURN varchar2;
        FUNCTION e_page RETURN varchar2;
        FUNCTION b_page_2 (
                                p_width         in number       default get.value('width'),
                                p_reload        IN NUMBER       DEFAULT NULL,
                                p_jump          IN VARCHAR2     DEFAULT NULL,
                                p_type          IN NUMBER       DEFAULT NULL,
                                p_onload        IN VARCHAR2     DEFAULT NULL
                                ) RETURN varchar2;
        FUNCTION e_page_2 RETURN varchar2;
        FUNCTION b_box (
                                p_title         in varchar2     default 'insert title',
                                p_width         in varchar2     default '100%',
                                p_help_package  in varchar2     default NULL,
                                p_archive       in varchar2     default NULL
                                ) RETURN varchar2;
        FUNCTION e_box RETURN varchar2;
        FUNCTION b_form (
                                p_action        in varchar2     default NULL,
                                p_name          in varchar2     default 'form',
                                p_target        in varchar2     default NULL
                                ) RETURN varchar2;
        FUNCTION e_form RETURN varchar2;
        FUNCTION b_table (
                                p_width         in varchar2     default '100%',
                                p_border        in varchar2     default '0'
                                ) return varchar2;
        FUNCTION e_table return varchar2;

        FUNCTION submit_link (
                                p_text          in varchar2     default NULL,
                                p_form          in varchar2     default 'form',
                                p_name          in varchar2     default NULL
                                ) return varchar2;
        FUNCTION main_menu (
                                p_selected      in number       default NULL
                                ) return varchar2;

        FUNCTION org_menu (
                                p_organization_pk       in organization.organization_pk%type    default NULL
                                ) return varchar2;
        FUNCTION org_adm_menu (
                                p_selected      in number       default NULL
                                ) return varchar2;

        FUNCTION home_menu return varchar2;
        FUNCTION www_menu return varchar2;
        FUNCTION empty_menu return varchar2;
        procedure test ( p_pk           IN calendar.calendar_pk%TYPE            DEFAULT 1337 );
END;
/
show errors;


CREATE OR REPLACE PACKAGE BODY htmf IS

procedure test ( p_pk           IN calendar.calendar_pk%TYPE            DEFAULT 1337 )
is
begin
DECLARE
        v_html_code     long             default NULL;
        v_file          utl_file.file_type                      default NULL;
        v_email_dir     varchar2(100)                           default NULL;
        v_number        number                                  default NULL;
        v_length        int              default NULL;
        v_chr           int                                  default 1;
begin
        SELECT  email_script_seq.NEXTVAL
        INTO    v_number
        FROM    dual;

        v_email_dir     := get.value('email_dir')||'/email_error';
        v_html_code := cal_util.return_calendar(p_pk);
        v_length := length( v_html_code );
        v_file := utl_file.fopen( v_email_dir, v_number||'.html' ,'a');
--      htp.p('Lengden p� html siden er '||v_length||'<br>');
        loop
--              htp.p('Skriver ut bostav '||v_chr||' og tusen bokstaver fremover.<br>');
                utl_file.put(v_file, substr(v_html_code,v_chr,1000));
                v_chr := v_chr + 1000;
                exit when v_length < v_chr;
        end loop;

        utl_file.fclose(v_file);

        htp.p( v_html_code );
--      htp.p( cal_util.return_calendar(p_pk) );
end;
end test;


---------------------------------------------------------
-- Name:        getcode
-- Type:        function
-- What:        returns html code from html_code table
-- Author:      Frode Klevstul
-- Start date:  18.05.2000
-- Desc:
-- --- Changes: ---
-- Date:        [date]
-- What:        [description]
-- Who:         [name]
---------------------------------------------------------
-- beskrivelse av inn parametere:
--
-- [parameter]  [description]
---------------------------------------------------------
FUNCTION getcode (
                p_name  in html_code.name%type default NULL
        ) RETURN varchar2
IS
        v_code  html_code.code%type default NULL;
BEGIN

        SELECT code INTO v_code
        FROM html_code
        WHERE name = p_name;

        RETURN v_code;

EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
        v_code := get.msg(1, 'there is no entry in html_code table with name = <b>'||p_name||'</b>');
        RETURN v_code;
END getcode;


-- **************************************
-- prosedyrer som erstatter HTML tagger
-- **************************************
---------------------------------------------------------
-- Name:        b_page
-- HTML:
-- Author:      Frode Klevstul, 07.07.2000
---------------------------------------------------------
FUNCTION b_page
        (
                p_width         in number       default get.value('width'),
                p_reload        IN NUMBER       DEFAULT NULL,
                p_jump          IN VARCHAR2     DEFAULT NULL,
                p_type          IN NUMBER       DEFAULT NULL,
                p_selected      IN NUMBER                       DEFAULT NULL,
                p_service_pk    IN service.service_pk%TYPE      DEFAULT NULL,
                p_language_pk   IN la.language_pk%type          default NULL
        ) return varchar2
IS
BEGIN
DECLARE
        v_code          varchar2(5000)                  default NULL;
        v_tmp           varchar2(200)                   default NULL;
        v_service_name  service.description%TYPE        default NULL;
        v_service_pk    service.service_pk%TYPE         DEFAULT NULL;
        v_width         varchar2(10)                    DEFAULT NULL;
        v_path          varchar2(100)                   default NULL;
        v_url           varchar2(100)                   default NULL;

BEGIN

        IF (p_width IS NULL) THEN
                v_width := get.value('width');
        ELSE
                v_width := p_width;
        END IF;


        v_service_name  := get.serv_name(p_service_pk);
        IF ( p_service_pk is NULL ) THEN
           v_service_pk    := get.serv;
        ELSE
           v_service_pk    := p_service_pk;
        END IF;
        v_code := htmf.getcode('b_page');

        v_url := get.serv_url(v_service_pk);

        IF ( v_url LIKE 'admin.bytur.no' ) THEN
           v_code := replace( v_code, '[base]', '<base href="http://admin.bytur.no/cgi-bin/">');
        ELSE
           v_code := replace( v_code, '[base]', '<base href="http://bytur.no/cgi-bin/">');
        END IF;

        IF ( p_reload IS NOT NULL AND p_jump IS NULL) THEN
                v_code := REPLACE (v_code, '[reload]', '<META HTTP-EQUIV="REFRESH" CONTENT="'||p_reload||'">' );
        ELSE
                v_code := REPLACE (v_code, '[reload]', '');
        END IF;
        IF ( p_jump IS NOT NULL ) THEN
                v_code := REPLACE (v_code, '[jump]', '<META HTTP-EQUIV="REFRESH" Content="'||p_reload||';URL='||p_jump||'">' );
        ELSE
                v_code := REPLACE (v_code, '[jump]', '');
        END IF;

        -- legger inn banner utifra tjeneste man er inne paa
        IF (v_service_pk = 1) THEN
                v_code := REPLACE (v_code, '[banner_link]', html.getcode('banner_student') );
        ELSIF (v_service_pk = 5) THEN
                v_code := REPLACE (v_code, '[banner_link]', html.getcode('banner_studnyhet') );
        ELSIF (v_service_pk = 3) THEN
                v_code := REPLACE (v_code, '[banner_link]', html.getcode('banner_uteliv') );
        ELSE
                v_code := REPLACE (v_code, '[banner_link]', html.getcode('banner_general') );
        END IF;

        v_code := REPLACE (v_code, '[icons_dir]', get.value('icons_dir') );
        v_code := REPLACE (v_code, '[width]', v_width );
        v_code := REPLACE (v_code, '[css]', getcode('css') );
        v_code := REPLACE (v_code, '[javascript]', html.getcode('javascript') );
        v_code := REPLACE (v_code, '[ad_button]', get.txt('ad_button') );
        v_code := REPLACE (v_code, '[c_service_bbm]' , get.value( 'c_'|| v_service_name ||'_bbm' ));
        v_code := REPLACE (v_code, '[c_service_mmb]' , get.value( 'c_'|| v_service_name ||'_mmb' ));
        v_code := REPLACE (v_code, '[c_service_bbs]' , get.value( 'c_'|| v_service_name ||'_bbs' ));

--        v_code := REPLACE (v_code, '[refresh]' , owa_util.get_cgi_env('REQUEST_URI') );
        v_code := REPLACE (v_code, '[refresh]' , '/cgi-bin/main_page.startup' );



        -- man kan sende med p_type for aa spare tid, hvis ikke hentes HTTP_REFERER
        if (p_type = 1) then
                return( concat(v_code, htmf.main_menu(p_selected)) );
        elsif (p_type = 2) then
                return( concat(v_code, htmf.org_adm_menu(p_selected)) );
        elsif (p_type = 3) then
                return( concat(v_code, htmf.www_menu) );
        elsif (p_type IS NULL) then
                v_path := get.serv_url;

                if ( owa_pattern.match( v_path, 'www.in4mant.com', 'i') or owa_pattern.match( v_path, '^in4mant.com', 'i') ) then
                        return( concat(v_code, htmf.www_menu) );
                elsif ( owa_pattern.match( v_path, 'admin.in4mant.com', 'i') ) then
                        return( concat(v_code, htmf.org_adm_menu(p_selected)) );
                elsif ( owa_pattern.match( v_path, '.in4mant.com', 'i') ) then
                        return( concat(v_code,htmf.main_menu(p_selected)) );
                end if;

        end if;

        return (v_code);

END;
END b_page;


---------------------------------------------------------
-- Name:        e_page
-- HTML:
-- Author:      Frode Klevstul, 30.05.2000
---------------------------------------------------------
FUNCTION e_page RETURN varchar2
IS
        v_code  varchar2(4000)          default NULL;
        v_year  VARCHAR2(4)             DEFAULT NULL;
BEGIN
   SELECT to_char(sysdate,'YYYY')
     INTO v_year
     FROM dual;

   v_code := getcode('e_page');
   v_code := REPLACE (v_code, '[year]', v_year );
   v_code := REPLACE (v_code, '[icons_dir]', get.value('icons_dir') );
   v_code := REPLACE (v_code, '[c_service_mmb]' , get.value( 'c_'|| get.serv_name ||'_mmb' ) );
        return ( v_code );

END e_page;

---------------------------------------------------------
-- Name:        b_page_2
-- HTML:
-- Author:      Frode Klevstul, 07.07.2000
---------------------------------------------------------
FUNCTION b_page_2
        (
                p_width         in number       default get.value('width'),
                p_reload        IN NUMBER       DEFAULT NULL,
                p_jump          IN VARCHAR2     DEFAULT NULL,
                p_type          IN NUMBER       DEFAULT NULL,
                p_onload        IN VARCHAR2     DEFAULT NULL
        ) RETURN varchar2
IS
BEGIN
DECLARE
        v_code  varchar2(5000)          default NULL;
BEGIN

        v_code := getcode('b_page_2');
        IF ( p_reload IS NOT NULL ) THEN
                v_code := REPLACE (v_code, '[reload]', '<META HTTP-EQUIV="REFRESH" CONTENT="'||p_reload||'">' );
        ELSE
                v_code := REPLACE (v_code, '[reload]', '');
        END IF;
        IF ( p_jump IS NOT NULL ) THEN
                v_code := REPLACE (v_code, '[jump]', '<META HTTP-EQUIV="REFRESH" Content="'||p_reload||';URL='||p_jump||'">' );
        ELSE
                v_code := REPLACE (v_code, '[jump]', '');
        END IF;
        IF ( p_onload IS NOT NULL ) THEN
                v_code := REPLACE (v_code, '[onload]', ' onload="'||p_onload||'"' );
        ELSE
                v_code := REPLACE (v_code, '[onload]', '');
        END IF;
        v_code := REPLACE (v_code, '[width]', p_width );
        v_code := REPLACE (v_code, '[css]', getcode('css') );
        v_code := REPLACE (v_code, '[javascript]', html.getcode('javascript') );
        v_code := REPLACE (v_code, '[c_service_bbm]' , get.value( 'c_'|| get.serv_name ||'_bbm' ) );
        v_code := REPLACE (v_code, '[c_service_bbs]' , get.value( 'c_'|| get.serv_name ||'_bbs' ) );
        return (v_code);

END;
END b_page_2;

---------------------------------------------------------
-- Name:        e_page_2
-- HTML:
-- Author:      Frode Klevstul, 22.08.2000
---------------------------------------------------------
FUNCTION e_page_2 RETURN varchar2
IS
BEGIN
        return ( getcode('e_page_2') );

END e_page_2;

---------------------------------------------------------
-- Name:        b_box
-- HTML:
-- Author:      Frode Klevstul, 30.05.2000
---------------------------------------------------------
FUNCTION b_box
        (
                p_title         in varchar2     default 'insert title',
                p_width         in varchar2     default '100%',
                p_help_package  in varchar2     default NULL,           -- pakken som hjelpeteksten skal skrives ut for
                p_archive       in varchar2     default NULL
        ) RETURN varchar2
IS
        v_code          varchar2(4000)          default NULL;
        v_service_name  VARCHAR2(50)            DEFAULT NULL;
BEGIN

        v_service_name := get.serv_name;

        v_code := getcode('b_box');
        v_code := REPLACE (v_code, '[title]', p_title );
        v_code := REPLACE (v_code, '[width]', p_width );
        v_code := REPLACE (v_code, '[c_service_mmb]' , get.value( 'c_'|| v_service_name ||'_mmb' ) );

        if (p_help_package IS NOT NULL) then
                v_code := REPLACE (v_code, '<!-- ht -->' , html.popup( get.txt('?'), 'help.module?p_package='|| p_help_package , 400, 500, 0, 1) );
        end if;
        if (p_archive IS NOT NULL) then
                v_code := REPLACE (v_code, '<!-- archive -->','<a href="'||p_archive||'">'||get.txt('archive')||'</a>' );
        end if;

        return ( v_code );

END b_box;



---------------------------------------------------------
-- Name:        e_box
-- HTML:
-- Author:      Frode Klevstul, 30.05.2000
---------------------------------------------------------
FUNCTION e_box RETURN varchar2
IS
BEGIN

        return ( getcode('e_box') );

END e_box;



---------------------------------------------------------
-- Name:        b_form
-- HTML:        <FORM>
-- Author:      Frode Klevstul, 30.05.2000
---------------------------------------------------------
FUNCTION b_form
        (
                p_action        in varchar2     default NULL,
                p_name          in varchar2     default 'form',
                p_target        in varchar2     default NULL
        ) RETURN varchar2
IS
        v_code  varchar2(4000)          default NULL;
BEGIN

        v_code := getcode('b_form');
        v_code := REPLACE (v_code, '[target]', p_target );
        v_code := REPLACE (v_code, '[action]', p_action );
        v_code := REPLACE (v_code, '[name]', p_name );
        return ( v_code );

END b_form;

---------------------------------------------------------
-- Name:        e_form
-- HTML:        </FORM>
-- Author:      Frode Klevstul, 30.05.2000
---------------------------------------------------------
FUNCTION e_form RETURN varchar2
IS
BEGIN
        return('</form>');
END e_form;


---------------------------------------------------------
-- Name:        b_table
-- HTML:        <TABLE>
-- Author:      Frode Klevstul, 30.05.2000
---------------------------------------------------------
FUNCTION b_table
        (
                p_width         in varchar2     default '100%',
                p_border        in varchar2     default '0'
        ) return varchar2
IS
        v_code  varchar2(4000)          default NULL;
BEGIN

        v_code := getcode('b_table');
        v_code := REPLACE (v_code, '[width]', p_width );
        v_code := REPLACE (v_code, '[border]', p_border );
        return ( v_code );

END b_table;

---------------------------------------------------------
-- Name:        e_table
-- HTML:        </TABLE>
-- Author:      Frode Klevstul, 30.05.2000
---------------------------------------------------------
FUNCTION e_table return varchar2
IS
BEGIN

        return ('</table>');

END e_table;


---------------------------------------------------------
-- Name:        submit_link
-- HTML:
-- Author:      Frode Klevstul, 08.06.2000
---------------------------------------------------------
FUNCTION submit_link
        (
                p_text          in varchar2     default NULL,
                p_form          in varchar2     default 'form',
                p_name          in varchar2     default NULL
        ) return varchar2
IS
        v_code  html_code.code%type             default NULL;
BEGIN

        if ( p_name is not null ) then
                v_code := getcode(p_name);
        else
                v_code := getcode('submit_link');
        end if;
        v_code := REPLACE (v_code, '[form]', p_form );
        v_code := REPLACE (v_code, '[text]', p_text );
        v_code := REPLACE (v_code, '[name]', '' );

        return( v_code );

END submit_link;






---------------------------------------------------------------------
-- Name: main_menu
-- Type: function
-- What: writes out the main menu
-- Made: Frode Klevstul
-- Date: 15.08.2000
-- Chng:
---------------------------------------------------------------------
FUNCTION main_menu(
                p_selected      in number       default NULL
        ) return varchar2
IS
BEGIN
DECLARE
        v_code  html_code.code%type             default NULL;
BEGIN

        v_code := getcode('main_menu');

        if (p_selected = 1) then
               v_code := REPLACE (v_code, '[home_button]', get.txt('home_button2') );
        else
               v_code := REPLACE (v_code, '[home_button]', get.txt('home_button') );
        end if;

        if (p_selected = 2) then
                v_code := REPLACE (v_code, '[my_page_button]', get.txt('my_page_button2') );
        else
                v_code := REPLACE (v_code, '[my_page_button]', get.txt('my_page_button') );
        end if;

        if (p_selected = 3) then
                v_code := REPLACE (v_code, '[organizations_button]', get.txt('organizations_button2') );
        else
                v_code := REPLACE (v_code, '[organizations_button]', get.txt('organizations_button') );
        end if;

        if (p_selected = 4) then
                v_code := REPLACE (v_code, '[whatsup_button]', get.txt('whatsup_button2') );
        else
                v_code := REPLACE (v_code, '[whatsup_button]', get.txt('whatsup_button') );
        end if;

        v_code := REPLACE (v_code, '[c_service_mmb]' , get.value( 'c_'|| get.serv_name ||'_mmb' ) );
        return( v_code );

END;
END main_menu;


---------------------------------------------------------------------
-- Name: org_menu
-- Type: function
-- What: writes out an menu for an organization
-- Made: Frode Klevstul
-- Date: 15.08.2000
-- Chng:
---------------------------------------------------------------------
FUNCTION org_menu
        (
                p_organization_pk       in organization.organization_pk%type    default NULL
        ) return varchar2
IS
BEGIN
DECLARE
        v_code  html_code.code%type             default NULL;
BEGIN

        v_code := getcode('org_menu');
        v_code := REPLACE (v_code, '[olink]', get.olink(p_organization_pk) );
        v_code := REPLACE (v_code, '[org_pk]', p_organization_pk );
        v_code := REPLACE (v_code, '[lan_pk]', get.lan );

        v_code := REPLACE (v_code, '[org_page_button]', get.txt('org_page_button') );
        v_code := REPLACE (v_code, '[org_news_button]', get.txt('org_news_button') );
        v_code := REPLACE (v_code, '[org_calendar_button]', get.txt('org_calendar_button') );
        v_code := REPLACE (v_code, '[org_fora_button]', get.txt('org_fora_button') );
        v_code := REPLACE (v_code, '[org_album_button]' , get.txt('org_album_button') );
        v_code := REPLACE (v_code, '[i4_menu]' , html.i4_menu );
        return ( v_code );

END;
END org_menu;

---------------------------------------------------------------------
-- Name: org_adm_menu
-- Type: function
-- What: writes out the main menu
-- Made: Frode Klevstul
-- Date: 15.08.2000
-- Chng:
---------------------------------------------------------------------
FUNCTION org_adm_menu
        (
                p_selected      in number       default NULL
        ) return varchar2
IS
BEGIN
DECLARE
        v_code                  html_code.code%type                     default NULL;
        v_organization_pk       organization.organization_pk%type       default NULL;

BEGIN

        v_organization_pk := get.oid;

        v_code := getcode('org_adm_menu');
        v_code := REPLACE (v_code, '[c_service_mmb]' , get.value( 'c_'|| get.serv_name ||'_mmb' ) );

        if (p_selected = 1) then
                v_code := REPLACE (v_code, '[org_adm_home_button]', get.txt('org_adm_home_button2') );
        else
                v_code := REPLACE (v_code, '[org_adm_home_button]', get.txt('org_adm_home_button') );
        end if;

        if (p_selected = 2) then
                v_code := REPLACE (v_code, '[org_adm_tools_button]', get.txt('org_adm_tools_button2') );
        else
                v_code := REPLACE (v_code, '[org_adm_tools_button]', get.txt('org_adm_tools_button') );
        end if;


        -- dersom brukeren er logget inn viser vi logout button, hvis ikke vises login button
        if ( login.timeout(NULL, 1) > 0) then
                v_code := REPLACE (v_code, '[login_status_button]', get.txt('logout_button') );
                v_code := REPLACE (v_code, '[login_status_url]', 'login.logout' );

                v_code := REPLACE (v_code, '[org_home]', '<a href="[olink]org_page.main?p_organization_pk=[org_pk]" target="new_window">[org_page_button]</a>');
                v_code := REPLACE (v_code, '[olink]', get.olink(v_organization_pk) );
                v_code := REPLACE (v_code, '[org_pk]', v_organization_pk );
                v_code := REPLACE (v_code, '[org_page_button]', get.txt('org_page_button') );
        else
                v_code := REPLACE (v_code, '[login_status_button]', get.txt('login_button') );
                v_code := REPLACE (v_code, '[login_status_url]', 'admin.startup' );

                v_code := REPLACE (v_code, '[org_home]', NULL);
        end if;


        v_code := REPLACE (v_code, '[contact_us_button]', get.txt('contact_us_button') );
        v_code := REPLACE (v_code, '[about_button]', get.txt('about_button') );
        v_code := REPLACE (v_code, '[help_button]', get.txt('help_button') );
        return ( v_code );

END;
END org_adm_menu;

---------------------------------------------------------------------
-- Name: home_menu
-- Type: function
-- What: writes out home menu
-- Made: Frode Klevstul
-- Date: 03.01.2001
-- Chng:
---------------------------------------------------------------------
FUNCTION home_menu return varchar2
IS
BEGIN
DECLARE
        v_code          html_code.code%type             default NULL;

BEGIN

        v_code := getcode('home_menu');
        v_code := REPLACE (v_code, '[doc_archive_button]', get.txt('doc_archive_button') );
        v_code := REPLACE (v_code, '[cal_archive_button]', get.txt('cal_archive_button') );
        v_code := REPLACE (v_code, '[last_comments_button]', get.txt('last_comments_button') );
        v_code := REPLACE (v_code, '[last_albums_button]', get.txt('last_albums_button') );
        v_code := REPLACE (v_code, '[last_pictures_button]', get.txt('last_pictures_button') );
        v_code := REPLACE (v_code, '[i4_menu]' , html.i4_menu );
        v_code := REPLACE (v_code, '[p_language_pk]' , get.lan );
        return ( v_code );

END;
END home_menu;


---------------------------------------------------------------------
-- Name: www_menu
-- Type: function
-- What: writes out the menu on the front page
-- Made: Frode Klevstul
-- Date: 14.01.2001
-- Chng:
---------------------------------------------------------------------
FUNCTION www_menu return varchar2
IS
BEGIN
DECLARE
        v_code          html_code.code%type             default NULL;

BEGIN

        v_code := getcode('www_menu');
        v_code := REPLACE (v_code, '[c_service_mmb]' , get.value( 'c_'|| get.serv_name ||'_mmb' ) );
        v_code := REPLACE (v_code, '[contact_us_button]', get.txt('contact_us_button') );
        v_code := REPLACE (v_code, '[about_button]', get.txt('about_button') );
        v_code := REPLACE (v_code, '[help_button]', get.txt('help_button') );
        return ( v_code );

END;
END www_menu;

---------------------------------------------------------------------
-- Name: empty_menu
-- Type: function
-- What: writes out home menu
-- Made: Frode Klevstul
-- Date: 03.01.2001
-- Chng:
---------------------------------------------------------------------
FUNCTION empty_menu return varchar2
IS
BEGIN
DECLARE
        v_code          html_code.code%type             default NULL;

BEGIN

        v_code := getcode('empty_menu');
        v_code := REPLACE (v_code, '[i4_menu]' , html.i4_menu );
        v_code := REPLACE (v_code, '[p_language_pk]' , get.lan );
        return( v_code );

END;
END empty_menu;




-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/
show errors;
