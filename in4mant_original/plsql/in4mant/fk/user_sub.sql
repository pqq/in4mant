set define off
PROMPT *** package: user_sub ***

CREATE OR REPLACE PACKAGE user_sub IS

    PROCEDURE startup;
    PROCEDURE frame
                (
                        p_url   in varchar2             default NULL
                );
        PROCEDURE print_top;
    PROCEDURE MYinfo;
    PROCEDURE plain_page;
    PROCEDURE my_lists;
    PROCEDURE insert_list
            (
                    p_list_pk       in list.list_pk%type    default NULL,
                    p_language_pk   in la.language_pk%type  default NULL
            );
    PROCEDURE delete_list
            (
                    p_list_pk       in list.list_pk%type    default NULL,
                    p_language_pk   in la.language_pk%type  default NULL
            );
    PROCEDURE my_org;
    PROCEDURE list_added;
    PROCEDURE sel_personal_list
            (
                    p_organization_pk       in list_org.organization_fk%type        default NULL
            );
    PROCEDURE sel_personal_list_2
            (
                    p_list_pk               in list.list_pk%type            default NULL
            );
    PROCEDURE insert_org
            (
                    p_organization_fk       in list_org.organization_fk%type        default NULL,
                    p_language_pk           in la.language_pk%type                  default NULL
            );
    PROCEDURE org_added;
    PROCEDURE delete_org
            (
                    p_organization_fk       in list_org.organization_fk%type        default NULL,
                    p_list_fk               in list_org.list_fk%type                default NULL
            );

END;
/
CREATE OR REPLACE PACKAGE BODY user_sub IS


---------------------------------------------------------
-- Name:        startup
-- Type:        procedure
-- What:        start procedure, to run the package
-- Author:      Frode Klevstul
-- Start date:  15.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN
if (login.timeout('user_sub.startup') > 0 ) then

        frame;

end if;
END startup;


---------------------------------------------------------
-- Name:        frame
-- Type:        procedure
-- What:        start procedure, to run the package
-- Author:      Frode Klevstul
-- Start date:  18.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE frame
        (
                p_url   in varchar2             default NULL
        )
IS
BEGIN
DECLARE

        v_height                NUMBER                                  DEFAULT NULL;
        v_url                   varchar2(100)                           default NULL;

BEGIN

        v_height := get.value('frame_row_top_height');

        if (p_url IS NOT NULL) then
                v_url := p_url;
                v_url := REPLACE(v_url, '��', '?');
        else
                v_url := 'user_sub.plain_page';
        end if;

        htp.p('
                <html><head>
                <meta http-equiv="Content-Type" content="text/html">
                <title>in4mant.com</title>
                </head>

                <frameset cols="*,750,*" border="0" frameborder="0">

                        <frame name="Left" src="fora_util.plain" scrolling="no" noresize>

                        <frameset rows="'||v_height||',*,30" border=0 frameborder="0">
                                <frame name="Navigation" src="user_sub.print_top" marginwidth="0" marginheight="3" scrolling="no" frameborder="no" border=0 noresize>
                                <frameset cols="380,*">
                                        <frame name="MYinfo" src="user_sub.MYinfo" marginwidth="3" marginheight="3" scrolling="auto" frameborder="no" border="0" scrolling="auto" noresize>
                                        <frame name="ADDinfo" src="'|| v_url ||'" scrolling="auto" noresize>
                                </frameset>
                                <frame name="Footer" src="html.bottom" scrolling="no" noresize>
                        </frameset>

                        <frame name="Right" src="fora_util.plain" scrolling="no" noresize>
                </frameset>

                </html>
        ');

END;
END frame;


---------------------------------------------------------------------
-- Name: print_top
-- Type: procedure
-- What: Toppen (menyen) p� siden
-- Made: Frode Klevstul
-- Date: 23.11.2000
-- Chng:
---------------------------------------------------------------------
PROCEDURE print_top
IS
BEGIN
        html.b_page(NULL, NULL, NULL, NULL, 3);
        html.my_menu;
        htp.p('<tr><td colspan="2">&nbsp;</td></tr>');
        html.e_page_2;
END print_top;


---------------------------------------------------------
-- Name:        MYinfo
-- Type:        procedure
-- What:        writes out my_lists and my_org
-- Author:      Frode Klevstul
-- Start date:  10.11.2000
-- Desc:
---------------------------------------------------------
PROCEDURE MYinfo
IS
BEGIN
if (login.timeout('user_sub.frames') > 0 ) then

        html.b_page_3(350);
        user_sub.my_lists;
        user_sub.my_org;
        html.e_page_2;

end if;
END MYinfo;



---------------------------------------------------------
-- Name:        plain_page
-- Type:        procedure
-- What:        written out when a users enters the frameset
-- Author:      Frode Klevstul
-- Start date:  09.11.2000
-- Desc:
---------------------------------------------------------
PROCEDURE plain_page
IS
BEGIN
if (login.timeout('user_sub.frames') > 0 ) then
   html.b_page_3(340);
   html.b_box( get.txt('my_subscribtion') );
   html.b_table;
   htp.p('<tr><td>'|| get.txt('my_subscribtion_desc') ||'</td></tr>');
   html.e_table;
   html.e_box;
   html.e_page_2;
end if;
END plain_page;


---------------------------------------------------------
-- Name:        my_lists
-- Type:        procedure
-- What:        lists out the lists the user has
-- Author:      Frode Klevstul
-- Start date:  15.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE my_lists
IS
BEGIN
DECLARE


        CURSOR  select_all(v_user_pk IN usr.user_pk%type) IS
        SELECT  list_pk, l.name_sg_fk, language_code, s.name_sg_fk, s.service_pk, language_pk
        FROM    list l, list_user lu, list_type lt, la, service s, string_group sg, groups g, strng s
        WHERE   list_pk = list_fk
        AND     list_type_pk = list_type_fk
        AND     service_fk = service_pk
        AND     lu.language_fk = language_pk
        AND     user_fk = v_user_pk
        AND     lt.list_type_pk > 0
        AND     lt.description NOT LIKE 'my_personal_list%'
        AND     sg.string_group_pk = l.name_sg_fk
        AND     g.string_group_fk = sg.string_group_pk
        AND     s.string_pk = g.string_fk
        AND     s.language_fk = get.lan
        ORDER BY string
        ;


        TYPE array_table IS TABLE OF VARCHAR2(100) INDEX BY BINARY_INTEGER;
        a_counter               array_table;
        a_s_name_sg_fk          array_table;
        a_l_name_sg_fk          array_table;
        a_service_fk            array_table;
        a_language_code_1       array_table;
        a_language_code_2       array_table;
        a_language_code_3       array_table;
        a_language_code_4       array_table;
        a_language_code_5       array_table;
        a_language_pk_1         array_table;
        a_language_pk_2         array_table;
        a_language_pk_3         array_table;
        a_language_pk_4         array_table;
        a_language_pk_5         array_table;


        v_list_pk       list.list_pk%type               default NULL;
        v_l_name_sg_fk  list.name_sg_fk%type            default NULL;
        v_language_code la.language_code%type           default NULL;
        v_s_name_sg_fk  service.name_sg_fk%type         default NULL;
        v_service_pk    service.service_pk%type         default NULL;
        v_language_pk   la.language_pk%type             default NULL;

        v_counter               number                          default 0;
        v_check                 number                          default 0;

BEGIN
if (login.timeout('user_sub.my_org') > 0 ) then

        -- ----------------------------------------
        -- henter ut alle relevante lister
        -- ----------------------------------------
        OPEN select_all( get.uid );
        LOOP
                fetch select_all into v_list_pk, v_l_name_sg_fk, v_language_code, v_s_name_sg_fk, v_service_pk, v_language_pk;
                exit when select_all%NOTFOUND;

                -- ------------------------
                -- legger inn i "arrayet"
                -- ------------------------
                v_counter                       := v_counter + 1;
                a_counter(v_counter)            := v_list_pk;           -- en hjelpe teller (for � g� igjennom arrayet)
                a_s_name_sg_fk(v_list_pk)       := v_s_name_sg_fk;      -- fk til navnet p� tjenesten
                a_l_name_sg_fk(v_list_pk)       := v_l_name_sg_fk;      -- fk til navnet p� listen
                a_service_fk(v_list_pk)         := v_service_pk;        -- fk til tjenesten (for � finne antall spr�k p� tjenesten)

                -- denne algoritmen tar MAKSIMALT h�yde for 5 forskjellige spr�k pr. liste
                IF (NOT a_language_code_1.EXISTS(v_list_pk)) THEN
                        a_language_pk_1(v_list_pk)              := v_language_pk;
                        a_language_code_1(v_list_pk)            := v_language_code;
                ELSIF (NOT a_language_code_2.EXISTS(v_list_pk)) THEN
                        a_language_pk_2(v_list_pk)              := v_language_pk;
                        a_language_code_2(v_list_pk)            := v_language_code;
                ELSIF (NOT a_language_code_3.EXISTS(v_list_pk)) THEN
                        a_language_pk_3(v_list_pk)              := v_language_pk;
                        a_language_code_3(v_list_pk)            := v_language_code;
                ELSIF (NOT a_language_code_4.EXISTS(v_list_pk)) THEN
                        a_language_pk_4(v_list_pk)              := v_language_pk;
                        a_language_code_4(v_list_pk)            := v_language_code;
                ELSIF (NOT a_language_code_5.EXISTS(v_list_pk)) THEN
                        a_language_pk_5(v_list_pk)              := v_language_pk;
                        a_language_code_5(v_list_pk)            := v_language_code;
                END IF;
        end loop;
        close select_all;


        html.b_box( get.txt('my_lists'), '100%', 'user_sub.my_lists' );
        html.b_table;
        htp.p('
                <tr><td colspan="9">'|| get.txt('my_lists_desc') ||'</td></tr>
                <tr><td colspan="9">&nbsp;</td></tr>
                <tr>
                        <td><b>'|| get.txt('list_name') ||'</b>:</td>
                        <td><b>'|| get.txt('service') ||'</b>:</td>
                        <td colspan="5"><b>'|| get.txt('language') ||'</b>:</td>
                        <td><b>'|| get.txt('change_lang') ||'</b>:</td>
                        <td><b>'|| get.txt('delete') ||'</b>:</td>
                </tr>
        ');


        -- --------------------------------------------
        -- g�r igjennom arrayet, og skriver ut verdiene
        -- --------------------------------------------
        v_counter := 0;
        FOR v_counter IN 1 .. a_counter.COUNT   -- l�per igjennom hele arrayet, fra 1 til tot. antall
        LOOP
                v_list_pk := a_counter(v_counter);


                -- sjekker at alle verdier som skal skrives ut finnes
                IF ( a_s_name_sg_fk.EXISTS(v_list_pk) ) then
                        htp.p('<tr><td valign="top">'|| get.text( a_l_name_sg_fk(v_list_pk) ) ||' ('|| get.no_org_list(v_list_pk) ||')</td><td valign="top">'|| get.text(a_s_name_sg_fk(v_list_pk)) ||'</td>');

                        -- skriver ut riktig spr�k
                        if ( a_language_code_1.EXISTS(v_list_pk) ) then
                                htp.p('<td>');
                                --html.button_link_2( get.txt(a_language_code_1(v_list_pk)), 'user_sub.delete_list?p_list_pk='||v_list_pk||'&p_language_pk='||a_language_pk_1(v_list_pk));
                                htp.p( get.txt(a_language_code_1(v_list_pk)) );
                                htp.p('</td>' );
                        else
                                htp.p('<td>&nbsp;</td>');
                        end if;
                        if ( a_language_code_2.EXISTS(v_list_pk) ) then
                                htp.p('<td>');
                                --html.button_link_2( get.txt(a_language_code_2(v_list_pk)), 'user_sub.delete_list?p_list_pk='||v_list_pk||'&p_language_pk='||a_language_pk_2(v_list_pk));
                                htp.p( get.txt(a_language_code_2(v_list_pk)) );
                                htp.p('</td>' );
                        else
                                htp.p('<td>&nbsp;</td>');
                        end if;
                        if ( a_language_code_3.EXISTS(v_list_pk) ) then
                                htp.p('<td>');
                                --html.button_link_2( get.txt(a_language_code_3(v_list_pk)), 'user_sub.delete_list?p_list_pk='||v_list_pk||'&p_language_pk='||a_language_pk_3(v_list_pk));
                                htp.p( get.txt(a_language_code_3(v_list_pk)) );
                                htp.p('</td>' );
                        else
                                htp.p('<td>&nbsp;</td>');
                        end if;
                        if ( a_language_code_4.EXISTS(v_list_pk) ) then
                                htp.p('<td>');
                                --html.button_link_2( get.txt(a_language_code_4(v_list_pk)), 'user_sub.delete_list?p_list_pk='||v_list_pk||'&p_language_pk='||a_language_pk_4(v_list_pk));
                                htp.p( get.txt(a_language_code_4(v_list_pk)) );
                                htp.p('</td>' );
                        else
                                htp.p('<td>&nbsp;</td>');
                        end if;
                        if ( a_language_code_5.EXISTS(v_list_pk) ) then
                                htp.p('<td>');
                                --html.button_link_2( get.txt(a_language_code_5(v_list_pk)), 'user_sub.delete_list?p_list_pk='||v_list_pk||'&p_language_pk='||a_language_pk_5(v_list_pk));
                                htp.p( get.txt(a_language_code_5(v_list_pk)) );
                                htp.p('</td>' );
                        else
                                htp.p('<td>&nbsp;</td>');
                        end if;

                        SELECT  count(*) INTO v_check
                        FROM    service_on_lang
                        WHERE   service_fk = a_service_fk(v_list_pk);

                        -- kun mulig � forandre spr�k dersom list.tjeneste er p� flere enn ett spr�k
                        if (v_check > 1) then
                                htp.p('<td>');
                                html.button_link_2(get.txt('change'), 'user_sub.sel_personal_list_2?p_list_pk='||v_list_pk, 'ADDinfo');
                                htp.p('</td>');
                        else
                                htp.p('<td>&nbsp;</td>');
                        end if;

                        -- link for � slette listen fra portef�lgen
                        htp.p('<td>');
                        html.button_link_2( get.txt('delete'), 'user_sub.delete_list?p_list_pk='||v_list_pk);
                        htp.p('</td></tr>' );


                        -- sletter fra array tabellen
                        a_counter.delete(v_counter);
                        IF (a_language_code_1.EXISTS(v_list_pk)) THEN a_language_code_1.delete(v_list_pk); END IF;
                        IF (a_language_code_2.EXISTS(v_list_pk)) THEN a_language_code_2.delete(v_list_pk); END IF;
                        IF (a_language_code_3.EXISTS(v_list_pk)) THEN a_language_code_3.delete(v_list_pk); END IF;
                        IF (a_language_code_4.EXISTS(v_list_pk)) THEN a_language_code_4.delete(v_list_pk); END IF;
                        IF (a_language_code_5.EXISTS(v_list_pk)) THEN a_language_code_5.delete(v_list_pk); END IF;
                        IF (a_language_pk_1.EXISTS(v_list_pk)) THEN a_language_pk_1.delete(v_list_pk); END IF;
                        IF (a_language_pk_2.EXISTS(v_list_pk)) THEN a_language_pk_2.delete(v_list_pk); END IF;
                        IF (a_language_pk_3.EXISTS(v_list_pk)) THEN a_language_pk_3.delete(v_list_pk); END IF;
                        IF (a_language_pk_4.EXISTS(v_list_pk)) THEN a_language_pk_4.delete(v_list_pk); END IF;
                        IF (a_language_pk_5.EXISTS(v_list_pk)) THEN a_language_pk_5.delete(v_list_pk); END IF;
                        IF (a_s_name_sg_fk.EXISTS(v_list_pk)) THEN a_s_name_sg_fk.delete(v_list_pk); END IF;
                END IF;
        END LOOP;

        html.e_table;

        html.b_table('100%');
        htp.p('<tr><td align="right">');
        html.button_link( get.txt('add_list'), 'select_list.sel_type?p_type=user_sub.sel_personal_list_2' , 'ADDinfo');
        htp.p('</td></tr>');
        html.e_table;

        html.e_box;


end if;
END;
END my_lists;


---------------------------------------------------------
-- Name:        insert_list
-- Type:        procedure
-- What:        insert a list in list_user
-- Author:      Frode Klevstul
-- Start date:  15.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE insert_list
        (
                p_list_pk       in list.list_pk%type    default NULL,
                p_language_pk   in la.language_pk%type  default NULL
        )
IS
BEGIN
DECLARE

        CURSOR  all_lang (v_service_fk IN service_on_lang.service_fk%type) IS
        SELECT  language_fk
        FROM    service_on_lang
        WHERE   service_fk = v_service_fk;

        v_user_pk       usr.user_pk%type        default NULL;
        v_check         number                  default NULL;
        v_language_pk   la.language_pk%type     default NULL;
        v_service_pk    service.service_pk%type default NULL;

BEGIN
if (login.timeout('user_sub.my_lists') > 0 ) then

        v_user_pk := get.uid;

        -- dersom pk = -1000 skal brukeren abonnere p� list.service alle sine spr�k
        if (p_language_pk = -1000) then

                SELECT  service_fk INTO v_service_pk
                FROM    list l, list_type lt
                WHERE   l.list_pk = p_list_pk
                AND     l.list_type_fk = lt.list_type_pk;

                -- finn alle spr�kene som "servicen" er p�
                open all_lang(v_service_pk);
                loop
                        fetch all_lang into v_language_pk;
                        exit when all_lang%NOTFOUND;

                        -- sjekker om brukeren har denne listen fra f�r
                        SELECT  count(*) INTO v_check
                        FROM    list_user
                        WHERE   list_fk = p_list_pk
                        AND     user_fk = v_user_pk
                        AND     language_fk = v_language_pk;

                        if ( v_check = 0 ) then
                                INSERT INTO list_user
                                (user_fk, list_fk, language_fk)
                                VALUES (v_user_pk, p_list_pk, v_language_pk);
                                commit;
                        end if;
                end loop;
                close all_lang;

        elsif (p_language_pk IS NOT NULL) then

                -- sletter denne org, dersom brukeren har den fra f�r (dersom forandring av spr�k)
                DELETE FROM list_user
                WHERE user_fk = v_user_pk
                AND list_fk = p_list_pk;

                INSERT INTO list_user
                (user_fk, list_fk, language_fk)
                VALUES (v_user_pk, p_list_pk, p_language_pk);
                commit;
        else
                html.b_page_2;
                htp.p( get.txt('please_choose_a_language') );
                html.back( get.txt('back') );
                html.e_page_2;
                return;
        end if;


        if (p_language_pk IS NOT NULL) then
                user_sub.list_added;
        end if;


end if;
END;
END insert_list;


---------------------------------------------------------
-- Name:        delete_list
-- Type:        procedure
-- What:        delete a list from list_user
-- Author:      Frode Klevstul
-- Start date:  15.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE delete_list
        (
                p_list_pk       in list.list_pk%type    default NULL,
                p_language_pk   in la.language_pk%type  default NULL
        )
IS
BEGIN
DECLARE
        v_user_pk       usr.user_pk%type        default NULL;

BEGIN
if (login.timeout('user_sub.my_lists') > 0 ) then

        if ( p_list_pk IS NOT NULL ) then
                v_user_pk := get.uid;

                if (p_language_pk IS NULL) then
                        DELETE FROM list_user
                        WHERE user_fk = v_user_pk
                        AND list_fk = p_list_pk;
                        commit;
                else
                        DELETE FROM list_user
                        WHERE user_fk = v_user_pk
                        AND list_fk = p_list_pk
                        AND language_fk = p_language_pk;
                        commit;
                end if;
        end if;

        MYinfo;

end if;

END;
END delete_list;


---------------------------------------------------------
-- Name:        my_org
-- Type:        procedure
-- What:        lists out the users organizations
-- Author:      Frode Klevstul
-- Start date:  15.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE my_org
IS
BEGIN
DECLARE

        CURSOR  select_all(v_user_pk IN usr.user_pk%type) IS
        SELECT  lo.organization_fk, la.language_name, la.language_code, s.name_sg_fk,
                l.list_pk, lt.service_fk, o.name
        FROM    list l, list_user lu, list_type lt, la, service s, list_org lo, organization o
        WHERE   l.list_pk = lu.list_fk
        AND     l.list_pk = lo.list_fk
        AND     lt.list_type_pk = l.list_type_fk
        AND     lt.service_fk = s.service_pk
        AND     lt.language_fk = la.language_pk
        AND     lu.user_fk = v_user_pk
        AND     lt.list_type_pk < 0
        AND     lt.description LIKE 'my_personal_list%'
        AND     o.organization_pk = lo.organization_fk
        ORDER BY o.name;


        TYPE array_table IS TABLE OF VARCHAR2(100) INDEX BY BINARY_INTEGER;
        a_counter               array_table;
        a_s_name_sg_fk          array_table;
        a_service_fk            array_table;
        a_language_name_1       array_table;
        a_language_name_2       array_table;
        a_language_name_3       array_table;
        a_language_name_4       array_table;
        a_language_name_5       array_table;
        a_list_pk_1             array_table;
        a_list_pk_2             array_table;
        a_list_pk_3             array_table;
        a_list_pk_4             array_table;
        a_list_pk_5             array_table;
        a_language_code_1       array_table;
        a_language_code_2       array_table;
        a_language_code_3       array_table;
        a_language_code_4       array_table;
        a_language_code_5       array_table;

        v_organization_fk       list_org.organization_fk%TYPE   DEFAULT NULL;
        v_language_name         la.language_name%TYPE           DEFAULT NULL;
        v_language_code         la.language_code%TYPE           DEFAULT NULL;
        v_s_name_sg_fk          service.name_sg_fk%TYPE         DEFAULT NULL;
        v_list_pk               list.list_pk%TYPE               DEFAULT NULL;
        v_service_fk            list_type.service_fk%TYPE       DEFAULT NULL;
        v_name                  organization.name%TYPE          DEFAULT NULL;

        v_counter               number                          default 0;
        v_check                 number                          default 0;

BEGIN
IF (login.timeout('user_sub.my_org') > 0 ) THEN

        -- ----------------------------------------
        -- henter ut alle relevante organisasjoner
        -- ----------------------------------------
        OPEN select_all( get.uid );
        LOOP
                FETCH select_all INTO   v_organization_fk, v_language_name, v_language_code,
                                        v_s_name_sg_fk, v_list_pk, v_service_fk, v_name;
                EXIT WHEN select_all%NOTFOUND;

                -- ------------------------
                -- legger inn i "arrayet"
                -- ------------------------
                v_counter                               := v_counter + 1;
                a_counter(v_counter)                    := v_organization_fk;
                a_s_name_sg_fk(v_organization_fk)       := v_s_name_sg_fk;
                a_service_fk(v_organization_fk)         := v_service_fk;

                -- denne algoritmen tar MAKSIMALT h�yde for 5 forskjellige spr�k pr. selskap/tjeneste
                IF (NOT a_language_name_1.EXISTS(v_organization_fk)) THEN
                        a_language_name_1(v_organization_fk)    := v_language_name;
                        a_language_code_1(v_organization_fk)    := v_language_code;
                        a_list_pk_1(v_organization_fk)          := v_list_pk;
                ELSIF (NOT a_language_name_2.EXISTS(v_organization_fk)) THEN
                        a_language_name_2(v_organization_fk)    := v_language_name;
                        a_language_code_2(v_organization_fk)    := v_language_code;
                        a_list_pk_2(v_organization_fk)          := v_list_pk;
                ELSIF (NOT a_language_name_3.EXISTS(v_organization_fk)) THEN
                        a_language_name_3(v_organization_fk)    := v_language_name;
                        a_language_code_3(v_organization_fk)    := v_language_code;
                        a_list_pk_3(v_organization_fk)          := v_list_pk;
                ELSIF (NOT a_language_name_4.EXISTS(v_organization_fk)) THEN
                        a_language_name_4(v_organization_fk)    := v_language_name;
                        a_language_code_4(v_organization_fk)    := v_language_code;
                        a_list_pk_4(v_organization_fk)          := v_list_pk;
                ELSIF (NOT a_language_name_5.EXISTS(v_organization_fk)) THEN
                        a_language_name_5(v_organization_fk)    := v_language_name;
                        a_language_code_5(v_organization_fk)    := v_language_code;
                        a_list_pk_5(v_organization_fk)          := v_list_pk;
                END IF;
        END LOOP;
        CLOSE select_all;


        html.b_box( get.txt('my_organizations'), '100%', 'user_sub.my_org' );
        html.b_table;
        htp.p('
                <tr><td colspan="9">'|| get.txt('my_org_desc') ||'</td></tr>
                <tr><td colspan="9">&nbsp;</td></tr>
        ');
        htp.p('<tr><td><b>'|| get.txt('org_name') ||'</b>:</td><td><b>'|| get.txt('service') ||'</b>:</td><td colspan="5"><b>'|| get.txt('language') ||'</b>:</td><td><b>'|| get.txt('change_lang') ||'</b>:</td><td><b>'|| get.txt('delete') ||'</b>:</td></tr>');

        -- --------------------------------------------
        -- g�r igjennom arrayet, og skriver ut verdiene
        -- --------------------------------------------
        v_counter := 0;
        FOR v_counter IN 1 .. a_counter.COUNT   -- l�per igjennom hele arrayet, fra 1 til tot. antall
        LOOP
                v_organization_fk := a_counter(v_counter);

                -- sjekker at alle verdier som skal skrives ut finnes
                IF ( a_s_name_sg_fk.EXISTS(v_organization_fk) ) then
                        htp.p('<tr><td valign="top"><a href="JavaScript:OpenLink('''|| get.olink(v_organization_fk, 'org_page.main?p_organization_pk='|| v_organization_fk) ||''')">'|| get.oname(v_organization_fk) ||'</a></td><td valign="top">'|| get.text(a_s_name_sg_fk(v_organization_fk)) ||'</td>');

                        -- skriver ut riktig spr�k
                        IF ( a_language_name_1.EXISTS(v_organization_fk) ) then
                                htp.p('<td>');
                                --html.button_link_2( get.txt(a_language_code_1(v_organization_fk)), 'user_sub.delete_org?p_organization_fk='||v_organization_fk ||'&p_list_fk='||a_list_pk_1(v_organization_fk));
                                htp.p( get.txt(a_language_code_1(v_organization_fk)) );
                                htp.p('</td>' );
                        ELSE
                                htp.p('<td>&nbsp;</td>');
                        END IF;
                        IF ( a_language_name_2.EXISTS(v_organization_fk) ) then
                                htp.p('<td>');
                                --html.button_link_2( get.txt(a_language_code_2(v_organization_fk)), 'user_sub.delete_org?p_organization_fk='||v_organization_fk ||'&p_list_fk='||a_list_pk_2(v_organization_fk));
                                htp.p( get.txt(a_language_code_2(v_organization_fk)) );
                                htp.p('</td>' );
                        ELSE
                                htp.p('<td>&nbsp;</td>');
                        END IF;
                        IF ( a_language_name_3.EXISTS(v_organization_fk) ) then
                                htp.p('<td>');
                                --html.button_link_2( get.txt(a_language_code_3(v_organization_fk)), 'user_sub.delete_org?p_organization_fk='||v_organization_fk ||'&p_list_fk='||a_list_pk_3(v_organization_fk));
                                htp.p( get.txt(a_language_code_3(v_organization_fk)) );
                                htp.p('</td>' );
                        ELSE
                                htp.p('<td>&nbsp;</td>');
                        END IF;
                        IF ( a_language_name_4.EXISTS(v_organization_fk) ) then
                                htp.p('<td>');
                                --html.button_link_2( get.txt(a_language_code_4(v_organization_fk)), 'user_sub.delete_org?p_organization_fk='||v_organization_fk ||'&p_list_fk='||a_list_pk_4(v_organization_fk));
                                htp.p( get.txt(a_language_code_4(v_organization_fk)) );
                                htp.p('</td>' );
                        ELSE
                                htp.p('<td>&nbsp;</td>');
                        END IF;
                        IF ( a_language_name_5.EXISTS(v_organization_fk) ) then
                                htp.p('<td>');
                                --html.button_link_2( get.txt(a_language_code_5(v_organization_fk)), 'user_sub.delete_org?p_organization_fk='||v_organization_fk ||'&p_list_fk='||a_list_pk_5(v_organization_fk));
                                htp.p( get.txt(a_language_code_5(v_organization_fk)) );
                                htp.p('</td>' );
                        ELSE
                                htp.p('<td>&nbsp;</td>');
                        END IF;

                        SELECT  count(*) INTO v_check
                        FROM    service_on_lang
                        WHERE   service_fk = a_service_fk(v_organization_fk);

                        -- kun mulig � forandre spr�k dersom org.tjeneste er p� flere enn ett spr�k
                        IF (v_check > 1) then
                                htp.p('<td>');
                                html.button_link_2(get.txt('change'), 'user_sub.sel_personal_list?p_organization_pk='||v_organization_fk, 'ADDinfo');
                                htp.p('</td>');
                        ELSE
                                htp.p('<td>&nbsp;</td>');
                        END IF;

                        -- link for � slette org. fra portef�lgen
                        htp.p('<td>');
                        html.button_link_2( get.txt('delete'), 'user_sub.delete_org?p_organization_fk='||v_organization_fk );
                        htp.p('</td></tr>' );


                        -- sletter fra array tabellen
                        a_counter.delete(v_counter);
                        IF (a_language_name_1.EXISTS(v_organization_fk)) THEN a_language_name_1.delete(v_organization_fk); END IF;
                        IF (a_language_name_2.EXISTS(v_organization_fk)) THEN a_language_name_2.delete(v_organization_fk); END IF;
                        IF (a_language_name_3.EXISTS(v_organization_fk)) THEN a_language_name_3.delete(v_organization_fk); END IF;
                        IF (a_language_name_4.EXISTS(v_organization_fk)) THEN a_language_name_4.delete(v_organization_fk); END IF;
                        IF (a_language_name_5.EXISTS(v_organization_fk)) THEN a_language_name_5.delete(v_organization_fk); END IF;
                        IF (a_language_code_1.EXISTS(v_organization_fk)) THEN a_language_code_1.delete(v_organization_fk); END IF;
                        IF (a_language_code_2.EXISTS(v_organization_fk)) THEN a_language_code_2.delete(v_organization_fk); END IF;
                        IF (a_language_code_3.EXISTS(v_organization_fk)) THEN a_language_code_3.delete(v_organization_fk); END IF;
                        IF (a_language_code_4.EXISTS(v_organization_fk)) THEN a_language_code_4.delete(v_organization_fk); END IF;
                        IF (a_language_code_5.EXISTS(v_organization_fk)) THEN a_language_code_5.delete(v_organization_fk); END IF;
                        IF (a_list_pk_1.EXISTS(v_organization_fk)) THEN a_list_pk_1.delete(v_organization_fk); END IF;
                        IF (a_list_pk_2.EXISTS(v_organization_fk)) THEN a_list_pk_2.delete(v_organization_fk); END IF;
                        IF (a_list_pk_3.EXISTS(v_organization_fk)) THEN a_list_pk_3.delete(v_organization_fk); END IF;
                        IF (a_list_pk_4.EXISTS(v_organization_fk)) THEN a_list_pk_4.delete(v_organization_fk); END IF;
                        IF (a_list_pk_5.EXISTS(v_organization_fk)) THEN a_list_pk_5.delete(v_organization_fk); END IF;
                        IF (a_s_name_sg_fk.EXISTS(v_organization_fk)) THEN a_s_name_sg_fk.delete(v_organization_fk); END IF;
                END IF;
        END LOOP;

        html.e_table;

        html.b_table('100%');
        htp.p('<tr><td align="right">');
        html.button_link( get.txt('add_organization'), 'select_org.list_out?p_url=user_sub.sel_personal_list', 'ADDinfo' );
        htp.p('</td></tr>');
        html.e_table;

        html.e_box;

END IF;

END;
END my_org;


---------------------------------------------------------
-- Name:        list_added
-- Type:        procedure
-- What:        brukes for � "re-loade" frame settet med riktig innhold
-- Author:      Frode Klevstul
-- Start date:  18.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE list_added
IS
BEGIN
DECLARE

        v_code          varchar2(4000)          default NULL;
        v_color         VARCHAR2(10)            DEFAULT NULL;
        v_javascript    varchar2(500)           default NULL;

BEGIN
        v_code := html.getcode('b_page_3');

        v_code := REPLACE (v_code, '[width]', '100%' );
        v_code := REPLACE (v_code, '[reload]', '');
        v_code := REPLACE (v_code, '[jump]', '');
        v_code := REPLACE (v_code, '[css]', html.getcode('css') );
        v_code := REPLACE (v_code, '[javascript]', html.getcode('javascript') );

        v_color := get.value('c_'|| get.serv_name ||'_bbs' );
        v_code := REPLACE (v_code, '[c_service_bbs]' , v_color );

        v_javascript := '
                <SCRIPT language="JavaScript">
                        refresh(''MYinfo'', ''user_sub.MYinfo'');
                </SCRIPT>
        ';

        htp.p(v_code ||''|| v_javascript);
        htp.p( get.txt('the_list_is_successfull_added_to_your_lists') );
        html.button_link( get.txt('add_new_list'), 'select_list.sel_type?p_type=user_sub.sel_personal_list_2', 'ADDinfo');

        html.e_page_2;

END;
END list_added;


---------------------------------------------------------
-- Name:        sel_personal_list
-- Type:        procedure
-- What:        select personal list to add organization on (dep. on choosen lang)
-- Author:      Frode Klevstul
-- Start date:  18.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE sel_personal_list
        (
                p_organization_pk       in list_org.organization_fk%type        default NULL
        )
IS
BEGIN
DECLARE


        CURSOR  all_lang (v_service_fk IN service_on_lang.service_fk%type) IS
        SELECT  language_fk, language_name
        FROM    service_on_lang, la
        WHERE   service_fk = v_service_fk
        AND     language_fk = language_pk;

        v_check                 number                                  default NULL;
        v_check_2               number                                  default NULL;

        v_no_documents          number                                  default NULL;
        v_name_sg_fk            service.name_sg_fk%type                 default NULL;
        v_service_pk            service.service_pk%type                 default NULL;
        v_language_fk           service_on_lang.language_fk%type        default NULL;
        v_language_fk_2         service_on_lang.language_fk%type        default NULL;
        v_language_name         la.language_name%type                   default NULL;
        v_user_pk               usr.user_pk%type                        default NULL;

BEGIN
if (login.timeout('user_sub.my_lists') > 0 ) then

   -- henter ut tjenesten som selskapet er med p�
   SELECT  service_fk, name_sg_fk INTO v_service_pk, v_name_sg_fk
   FROM    org_service, service
   WHERE   organization_fk = p_organization_pk
   AND     service_fk = service_pk;

   select count(*)
   into v_check
   from service_on_lang
   where service_fk = v_service_pk;

   if ( v_check > 1 ) THEN

        -- html kode
        html.b_page_3(280);
        html.b_box( get.txt('choose_language_on_news') , '100%', 'user_sub.sel_personal_list');
        html.b_table;

        -- antall dokumenter som dette selskapet har publisert
        SELECT  count(*) INTO v_no_documents
        FROM    document
        WHERE   organization_fk = p_organization_pk;
        htp.p('<tr><td>'|| get.oname(p_organization_pk) ||' '|| get.txt('has_total_published') ||' '|| v_no_documents ||' '|| get.txt('documents') );

        htp.p( get.txt('on') ||' '|| get.text(v_name_sg_fk) ||' '|| get.txt('the_service') ||'.</td></tr>');

        -- html kode
        html.b_form('user_sub.insert_org');
        htp.p('<tr><td><br>'|| get.txt('select_language') ||':</td></tr>');
        htp.p('<input type="hidden" name="p_organization_fk" value="'|| p_organization_pk ||'">');


        -- sjekker om brukeren allerede abonnerer p� selskapet, skriver event. ut <checked> p� riktig sted
        v_user_pk := get.uid;

        -- sjekker antall spr�k brukeren abonnerer p� fra dette selskapet
        SELECT  count(*) INTO v_check_2
        FROM    list_user lu, list l, list_org lo, list_type lt
        WHERE   lu.list_fk = l.list_pk
        AND     l.list_type_fk = lt.list_type_pk
        AND     lo.list_fk = l.list_pk
        AND     lo.organization_fk = p_organization_pk
        AND     lu.user_fk = v_user_pk
        AND     lt.service_fk = v_service_pk
        AND     lt.description LIKE 'my_personal_list%'
        AND     lt.list_type_pk < 0;

        -- dersom ett spr�k m� vi finne ut hvilket
        if (v_check_2 = 1) then
                SELECT  lt.language_fk INTO v_language_fk_2
                FROM    list_user lu, list l, list_org lo, list_type lt
                WHERE   lu.list_fk = l.list_pk
                AND     l.list_type_fk = lt.list_type_pk
                AND     lo.list_fk = l.list_pk
                AND     lo.organization_fk = p_organization_pk
                AND     lu.user_fk = v_user_pk
                AND     lt.service_fk = v_service_pk
                AND     lt.description LIKE 'my_personal_list%'
                AND     lt.list_type_pk < 0;
        end if;

        -- l�per igjennom alle spr�k p� organisasjones tjeneste
        v_check := 0;
        open all_lang(v_service_pk);
        loop
                fetch all_lang into v_language_fk, v_language_name;
                exit when all_lang%NOTFOUND;
                v_check := v_check + 1;
                if ( v_language_fk = v_language_fk_2) then -- dersom spr�ket er det samme som brukeren har fra f�r -> checked
                        htp.p('<tr><td><input type="Radio" name="p_language_pk" value="'|| v_language_fk ||'" checked> '|| v_language_name ||'</td></tr>');
                else
                        htp.p('<tr><td><input type="Radio" name="p_language_pk" value="'|| v_language_fk ||'"> '|| v_language_name ||'</td></tr>');
                end if;
        end loop;
        close all_lang;


        -- dersom to spr�k -> gi brukeren valgmulighet for � abonnere p� BEGGE
        if (v_check = 2) then
                if (v_check_2 > 1) then
                        htp.p('<tr><td><input type="Radio" name="p_language_pk" value="-1000" checked> '|| get.txt('both') ||'</td></tr>');
                else
                        htp.p('<tr><td><input type="Radio" name="p_language_pk" value="-1000"> '|| get.txt('both') ||'</td></tr>');
                end if;
        -- dersom tre (eller flere) spr�k -> gi brukeren valgmulighet for � abonnere p� ALLE
        elsif (v_check > 2) then
                if (v_check_2 > 1) then
                        htp.p('<tr><td><input type="Radio" name="p_language_pk" value="-1000" checked> '|| get.txt('all') ||'</td></tr>');
                else
                        htp.p('<tr><td><input type="Radio" name="p_language_pk" value="-1000"> '|| get.txt('all') ||'</td></tr>');
                end if;
        end if;

        htp.p('<tr><td>');
        html.submit_link( get.txt('add_organization') );
        htp.p('</td></tr>');

        html.e_form;

        htp.p('<tr><td>');
        html.back( get.txt('go_back_without_adding_org') );
        htp.p('</td></tr>');

        html.e_table;
        html.e_box;
        html.e_page_2;

else
   -- ett spr�k -> gir ikke brukeren mulighet for � velge spr�k
   select language_fk
   into v_language_fk
   from service_on_lang
   where service_fk = v_service_pk
   ;
   html.jump_to('user_sub.insert_org?p_organization_fk='||p_organization_pk||'&p_language_pk='||v_language_fk);
end if;
end if;

END;
END sel_personal_list;



---------------------------------------------------------
-- Name:        sel_personal_list_2
-- Type:        procedure
-- What:        select personal list to add "my lists" on (language)
-- Author:      Frode Klevstul
-- Start date:  26.09.2000
-- Desc:
---------------------------------------------------------
PROCEDURE sel_personal_list_2
        (
                p_list_pk               in list.list_pk%type            default NULL
        )
IS
BEGIN
DECLARE


        CURSOR  all_lang (v_service_fk IN service_on_lang.service_fk%type) IS
        SELECT  language_fk, language_name
        FROM    service_on_lang, la
        WHERE   service_fk = v_service_fk
        AND     language_fk = language_pk;


        v_no_org                number                                  default NULL;
        v_name_sg_fk            list.name_sg_fk%type                    default NULL;
        v_name_sg_fk_2          service.name_sg_fk%type                 default NULL;
        v_service_pk            service.service_pk%type                 default NULL;
        v_language_fk           service_on_lang.language_fk%type        default NULL;
        v_language_fk_2         service_on_lang.language_fk%type        default NULL;
        v_language_name         la.language_name%type                   default NULL;
        v_user_pk               usr.user_pk%type                        default NULL;

        v_check                 number                                  default NULL;
        v_check_2               number                                  default NULL;


BEGIN
if (login.timeout('user_sub.my_lists') > 0 ) then
   -- henter ut informasjon om denne listen
   SELECT  l.name_sg_fk, s.name_sg_fk, service_pk INTO v_name_sg_fk, v_name_sg_fk_2, v_service_pk
   FROM    list l, list_type lt, service s
   WHERE   l.list_pk = p_list_pk
   AND     l.list_type_fk = lt.list_type_pk
   AND     lt.service_fk = s.service_pk;

   select count(*)
   into v_check
   from service_on_lang
   where service_fk = v_service_pk
   ;

   if ( v_check > 1 ) THEN

        -- html kode
        html.b_page_3(280);
        html.b_box( get.txt('choose_language_on_news'), '100%', 'user_sub.select_personal_list_2' );
        html.b_table;

        -- antall organisasjoner p� denne listen
        v_no_org := get.no_org_list(p_list_pk);


        htp.p('<tr><td>'|| get.txt('there_are') ||' '|| v_no_org ||' '|| get.txt('organizations') ||' '||get.txt('on')||' '|| get.text(v_name_sg_fk_2) ||' '||get.txt('the_list')||' <b>'|| get.text(v_name_sg_fk) ||'</b>' );


        -- html kode
        html.b_form('user_sub.insert_list');
        htp.p('<tr><td><br>'|| get.txt('select_language') ||':</td></tr>');
        htp.p('<input type="hidden" name="p_list_pk" value="'|| p_list_pk ||'">');

        -- --------
        -- sjekker om brukeren allerede abonnerer p� listen, skriver event. ut <checked> p� riktig sted
        -- --------
        v_user_pk := get.uid;

        -- sjekker antall spr�k brukeren abonnerer p� fra denne listen
        SELECT  count(*) INTO v_check_2
        FROM    list_user
        WHERE   user_fk = v_user_pk
        AND     list_fk = p_list_pk;


        -- dersom ett spr�k m� vi finne ut hvilket
        if (v_check_2 = 1) then
                SELECT  language_fk INTO v_language_fk_2
                FROM    list_user
                WHERE   user_fk = v_user_pk
                AND     list_fk = p_list_pk;
        end if;

        -- l�per igjennom alle spr�k p� organisasjones tjeneste
        v_check := 0;
        open all_lang(v_service_pk);
        loop
                fetch all_lang into v_language_fk, v_language_name;
                exit when all_lang%NOTFOUND;
                v_check := v_check + 1;
                if ( v_language_fk = v_language_fk_2) then -- dersom spr�ket er det samme som brukeren har fra f�r -> checked
                        htp.p('<tr><td><input type="Radio" name="p_language_pk" value="'|| v_language_fk ||'" checked> '|| v_language_name ||'</td></tr>');
                else
                        htp.p('<tr><td><input type="Radio" name="p_language_pk" value="'|| v_language_fk ||'"> '|| v_language_name ||'</td></tr>');
                end if;
        end loop;
        close all_lang;


        -- ett spr�k -> gir ikke brukeren mulighet for � velge spr�k
        if (v_check = 1) then
                html.jump_to('user_sub.insert_list?p_list_pk='||p_list_pk||'&p_language_pk='||v_language_fk);
                return;
        -- dersom to spr�k -> gi brukeren valgmulighet for � abonnere p� BEGGE
        elsif (v_check = 2) then
                if (v_check_2 > 1) then
                        htp.p('<tr><td><input type="Radio" name="p_language_pk" value="-1000" checked> '|| get.txt('both') ||'</td></tr>');
                else
                        htp.p('<tr><td><input type="Radio" name="p_language_pk" value="-1000"> '|| get.txt('both') ||'</td></tr>');
                end if;
        -- dersom tre (eller flere) spr�k -> gi brukeren valgmulighet for � abonnere p� ALLE
        elsif (v_check > 2) then
                if (v_check_2 > 1) then
                        htp.p('<tr><td><input type="Radio" name="p_language_pk" value="-1000" checked> '|| get.txt('all') ||'</td></tr>');
                else
                        htp.p('<tr><td><input type="Radio" name="p_language_pk" value="-1000"> '|| get.txt('all') ||'</td></tr>');
                end if;
        end if;

        htp.p('<tr><td>');
        html.submit_link( get.txt('add_list') );
        htp.p('</td></tr>');

        html.e_form;

        htp.p('<tr><td>');
        html.back( get.txt('go_back_without_adding_list') );
        htp.p('</td></tr>');

        html.e_table;
        html.e_box;
        html.e_page_2;
else
   select language_fk
   into v_language_fk
   from service_on_lang
   where service_fk = v_service_pk
   ;
   html.jump_to('user_sub.insert_list?p_list_pk='||p_list_pk||'&p_language_pk='||v_language_fk);
end if;
end if;

END;
END sel_personal_list_2;


---------------------------------------------------------
-- Name:        insert_org
-- Type:        procedure
-- What:        insert an organization to list_org (MY org)
-- Author:      Frode Klevstul
-- Start date:  18.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE insert_org
        (
                p_organization_fk       in list_org.organization_fk%type        default NULL,
                p_language_pk           in la.language_pk%type                  default NULL
        )
IS
BEGIN
DECLARE

        CURSOR  all_lang (v_service_fk IN service_on_lang.service_fk%type) IS
        SELECT  language_fk
        FROM    service_on_lang
        WHERE   service_fk = v_service_fk;


        CURSOR  user_list (v_service_fk in service.service_pk%type, v_user_pk in usr.user_pk%type , v_organization_fk in list_org.organization_fk%type) IS
        SELECT  list_pk
        FROM    list l, list_type lt, list_user lu, list_org lo
        WHERE   list_type_pk = list_type_fk
        AND     list_type_pk < 0
        AND     description LIKE 'my_personal_list%'
        AND     service_fk = v_service_fk
        AND     lu.list_fk = l.list_pk
        AND     lu.user_fk = v_user_pk
        AND     lo.list_fk = l.list_pk
        AND     lo.organization_fk = v_organization_fk;


        v_check         number                          default NULL;
        v_service_fk    list_type.service_fk%type       default NULL;
        v_language_fk   list_type.language_fk%type      default NULL;
        v_user_pk       usr.user_pk%type                default NULL;
        v_list_pk       list.list_pk%type               default NULL;
        v_list_pk_2     list.list_pk%type               default NULL;
        v_list_type_pk  list_type.list_type_pk%type     default NULL;

BEGIN
if (login.timeout('user_sub.my_org') > 0 ) then

        -- ------------------------------------------------------
        -- HENTER N�DVENDIG INFORMASJON OM BRUKER OG ORGANISASJON
        -- ------------------------------------------------------
        v_service_fk := get.oserv( p_organization_fk ); -- finner service som org er med p�
        v_user_pk := get.uid; -- henter ut bruker id'en


        -- --------------------------------------------------------------------------------------
        -- SJEKKER/OPPRETTER RIKTIGE LISTER OG LISTE TYPER FOR BRUKEREN P� ORG.SERVICE SINE SPR�K
        -- --------------------------------------------------------------------------------------
        -- finner alle spr�kene som "servicen" er p�
        open all_lang(v_service_fk);
        while (1>0)     -- alltid sant -> g�r igjennom alle forekomstene
        loop
                fetch all_lang into v_language_fk;
                exit when all_lang%NOTFOUND;

                -- m� sjekke om brukeren har "personal" liste
                -- p� angitt spr�k
                SELECT  count(*) INTO v_check
                FROM    list_user lu, list l, list_type lt
                WHERE   lu.list_fk = l.list_pk
                AND     l.list_type_fk = lt.list_type_pk
                AND     lu.user_fk = v_user_pk
                AND     lt.description LIKE 'my_personal_list%'
                AND     lt.list_type_pk < 0
                AND     lt.language_fk = v_language_fk
                AND     lt.service_fk = v_service_fk;

                -- dersom "personal" liste p� et spr�k ikke finnes
                -- s� oppretter vi dette
                if ( v_check = 0 ) then

                        -- sjekker om riktig liste_type finnes
                        SELECT  count(*) INTO v_check
                        FROM    list_type
                        WHERE   language_fk = v_language_fk
                        AND     service_fk = v_service_fk
                        AND     list_type_pk < 0
                        AND     description LIKE 'my_personal_list%';

                        -- oppretter listetypen dersom den ikke finnes
                        if (v_check = 0) then
                                SELECT  list_type_seq2.NEXTVAL
                                INTO    v_list_type_pk
                                FROM    dual;

                                INSERT INTO list_type
                                (list_type_pk, language_fk, service_fk, description)
                                VALUES (v_list_type_pk, v_language_fk, v_service_fk, 'my_personal_list[user_sub.insert_org]');
                                commit;
                        end if;

                        SELECT  list_seq.NEXTVAL
                        INTO    v_list_pk
                        FROM    dual;

                        SELECT  list_type_pk INTO v_list_type_pk
                        FROM    list_type
                        WHERE   language_fk = v_language_fk
                        AND     service_fk = v_service_fk
                        AND     list_type_pk < 0
                        AND     description LIKE 'my_personal_list%';

                        -- oppretter "personal" liste
                        INSERT INTO list
                        (list_pk, list_type_fk)
                        VALUES(v_list_pk, v_list_type_pk);
                        commit;

                        INSERT INTO list_user
                        (user_fk, list_fk, language_fk)
                        VALUES (v_user_pk, v_list_pk, v_language_fk);
                        commit;

                end if;
        end loop;
        close all_lang;



        -- dersom pk = -1000 skal brukeren abonnere p� org.service alle sine spr�k
        if (p_language_pk = -1000) then

                -- finn alle spr�kene som "servicen" er p�
                open all_lang(v_service_fk);
                while (1>0)     -- alltid sant -> g�r igjennom alle forekomstene
                loop
                        fetch all_lang into v_language_fk;
                        exit when all_lang%NOTFOUND;

                        SELECT  list_pk INTO v_list_pk
                        FROM    list l, list_type lt, list_user lu
                        WHERE   list_type_pk = list_type_fk
                        AND     list_type_pk < 0
                        AND     description LIKE 'my_personal_list%'
                        AND     lt.language_fk = v_language_fk
                        AND     service_fk = v_service_fk
                        AND     lu.list_fk = l.list_pk
                        AND     lu.user_fk = v_user_pk;

                        -- sjekker om brukeren har denne organisasjonen fra f�r
                        SELECT  count(*) INTO v_check
                        FROM    list_org
                        WHERE   organization_fk = p_organization_fk
                        AND     list_fk = v_list_pk;

                        if ( v_check = 0 ) then
                                INSERT INTO list_org
                                (organization_fk, list_fk)
                                VALUES (p_organization_fk, v_list_pk);
                                commit;
                        end if;

                end loop;
                close all_lang;

        elsif (p_language_pk IS NOT NULL) then

                -- henter ut riktig liste
                SELECT  list_pk INTO v_list_pk
                FROM    list l, list_type lt, list_user lu
                WHERE   list_type_pk = list_type_fk
                AND     list_type_pk < 0
                AND     description LIKE 'my_personal_list%'
                AND     lt.language_fk = p_language_pk
                AND     service_fk = v_service_fk
                AND     lu.list_fk = l.list_pk
                AND     lu.user_fk = v_user_pk;

                -- sletter denne org, dersom brukeren har den fra f�r (hvis forandring av spr�k)
                open user_list(v_service_fk, v_user_pk, p_organization_fk);
                loop
                        fetch user_list into v_list_pk_2;
                        exit when user_list%NOTFOUND;

                        DELETE FROM list_org
                        WHERE organization_fk = p_organization_fk
                        AND list_fk = v_list_pk_2;
                        commit;

                end loop;
                close user_list;

                -- sjekker om brukeren har denne organisasjonen fra f�r
                SELECT  count(*) INTO v_check
                FROM    list_org
                WHERE   organization_fk = p_organization_fk
                AND     list_fk = v_list_pk;

                if ( v_check = 0 ) then
                        INSERT INTO list_org
                        (organization_fk, list_fk)
                        VALUES (p_organization_fk, v_list_pk);
                        commit;
                end if;
        else
                html.b_page_3(350);
                htp.p( get.txt('please_choose_a_language') );
                html.back( get.txt('back') );
                html.e_page_2;
                return;
        end if;

        org_added;

end if;
END;
END insert_org;



---------------------------------------------------------
-- Name:        org_added
-- Type:        procedure
-- What:        brukes for � "re-loade" frame settet med riktig innhold
-- Author:      Frode Klevstul
-- Start date:  20.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE org_added
IS
BEGIN
DECLARE

        v_code  varchar2(4000)          default NULL;
        v_color VARCHAR2(10)            DEFAULT NULL;

BEGIN
        v_code := html.getcode('b_page_3');
        v_code := REPLACE (v_code, '[width]', '100%' );
        v_code := REPLACE (v_code, '[reload]', '');
        v_code := REPLACE (v_code, '[jump]', '');
        v_code := REPLACE (v_code, '[css]', html.getcode('css') );
        v_code := REPLACE (v_code, '[javascript]', html.getcode('javascript') );


        v_color := get.value('c_'|| get.serv_name ||'_bbs' );
        v_code := REPLACE (v_code, '[c_service_bbs]' , v_color );

        v_code := v_code ||'<SCRIPT language="JavaScript">refresh(''MYinfo'', ''user_sub.MYinfo'');</SCRIPT>';

        htp.p(v_code);
        htp.p( get.txt('the_organization_is_successfull_added_to_your_orga') );
        html.button_link( get.txt('add_new_organization'), 'select_org.list_out?p_url=user_sub.sel_personal_list', 'ADDinfo');
        html.e_page_2;

END;
END org_added;


---------------------------------------------------------
-- Name:        delete_org
-- Type:        procedure
-- What:        delete an organization list_org
-- Author:      Frode Klevstul
-- Start date:  20.07.2000
-- Desc:
---------------------------------------------------------
PROCEDURE delete_org
        (
                p_organization_fk       in list_org.organization_fk%type        default NULL,
                p_list_fk               in list_org.list_fk%type                default NULL
        )
IS
BEGIN
DECLARE
        v_user_pk       usr.user_pk%type        default NULL;
        v_list_fk       list_user.list_fk%type  default NULL;

BEGIN
if (login.timeout('user_sub.my_org') > 0 ) then

        v_user_pk := get.uid;

        if (p_list_fk IS NOT NULL) then
                DELETE FROM list_org
                WHERE organization_fk = p_organization_fk
                AND list_fk = p_list_fk;
                commit;
        else
                DELETE FROM list_org
                WHERE organization_fk = p_organization_fk
                AND list_fk IN
                        (
                                SELECT  list_pk
                                FROM    list l, list_user lu, list_type lt, list_org lo
                                WHERE   lu.list_fk = l.list_pk
                                AND     l.list_type_fk = lt.list_type_pk
                                AND     lo.list_fk = l.list_pk
                                AND     lt.description LIKE 'my_personal_list%'
                                AND     lu.user_fk = v_user_pk
                        );
                commit;
        end if;

        MYinfo;

end if;
END;
END delete_org;



-- ++++++++++++++++++++++++++++++++++++++++++++++ --

end; -- slutter pakke kroppen
/
show errors;
