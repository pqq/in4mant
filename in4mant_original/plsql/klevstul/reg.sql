set define off
PROMPT *** package: reg ***

/* -------------------------------------------------------------------
 
drop table registration;

create table registration(
	email			varchar2(150)	not null,
	event			varchar2(150)	not null,
	name			varchar2(150),
	reg_date		date,
	changeable		varchar2(150)
);

alter table registration add constraint primKey primary key (email, event);

commit;

----------------------------------------------------------------------- */

CREATE OR REPLACE PACKAGE reg IS


	procedure startup;

	procedure overview;

	procedure user_reg
		(
			p_email			in varchar2			default null,
			p_name			in varchar2			default null,
			p_event			in varchar2			default null,
			p_changeable	in varchar2			default null
		);


	-- tempor�rer prosedyrer, kan slettes etterhvert...

	procedure cs;

	procedure IHLodi03;

	procedure cancel;


END;
/
show errors;



CREATE OR REPLACE PACKAGE BODY reg IS

---------------------------------------------------------
-- Name:        startup
-- What:        starts up this package
-- Author:      Frode Klevstul
-- Start date:  15.01.2003
---------------------------------------------------------
PROCEDURE startup
IS
BEGIN

	overview;

END startup;



---------------------------------------------------------
-- Name:        overview
-- What:        list out all entries
-- Author:      Frode Klevstul
-- Start date:  17.01.2003
---------------------------------------------------------
PROCEDURE overview
IS
BEGIN
DECLARE

	cursor	select_all is
	select	distinct(event)
	from	registration
	order by event asc;

	v_event		registration.event%type			default null;

BEGIN
	
	htp.p('Overview:<br><br>');

	open select_all;
	loop
		fetch select_all into v_event;
		exit when select_all%NOTFOUND;
		
		htp.p('<a href="reg.user_reg?p_event='||v_event||'">'|| v_event ||'</a><br>');
		
	end loop;
	close select_all;

END;
END overview;



---------------------------------------------------------
-- Name:        user_reg
-- What:        register event, and lists out entries
-- Author:      Frode Klevstul
-- Start date:  17.04.2002
---------------------------------------------------------
PROCEDURE user_reg
	(
		p_email			in varchar2			default null,
		p_name			in varchar2			default null,
		p_event			in varchar2			default null,
		p_changeable	in varchar2			default null
	)
IS
BEGIN
DECLARE

	v_count			number							default 0;
	v_email			registration.email%type			default null;
	v_name			registration.name%type			default null;
	v_changeable	registration.changeable%type	default null;

	cursor	select_all is
	select	email, name, changeable
	from	registration
	where	event = p_event
	order by name asc;

BEGIN
	
	if (p_email is not null and p_name is not null and p_event is not null) then

		select 	count(*) 
		into 	v_count
		from 	registration
		where 	email = p_email
		and		event = p_event;
		
		if (v_count = 0) then
			insert into registration (email, name, reg_date, changeable, event)
			values (p_email, p_name, sysdate, p_changeable, p_event);
			commit;
		end if;

	end if;

	htp.p('
		<html>
		<head><title>'||p_event||'</title></head>
		<LINK HREF="http://klevstul.com/kstyle.css" REL="STYLESHEET" TYPE="text/css">
		<body bgcolor="#127510">
		<center>
		<table border="0" width="600" bgcolor="#00ff00" cellpadding="5">
		<tr><td>&nbsp;</td></tr>
		<tr><td align="center"><h1>'||p_event||'</h1><h2>registration</h2> </td></tr>
		<tr><td>
			For more info, send email to <a href="mailto:frode@klevstul.com">frode@klevstul.com</a>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td align="left">
			<table>
				<form action="reg.user_reg" method="post">
				<input type="hidden" name="p_event" value="'||p_event||'">
				<tr><td colspan="2"><b>Yeeesss, I''m in!!!</b></td></tr>
				<tr><td>name:</td><td><input type="text" name="p_name" maxlength="30" size="25"></td></tr>
				<tr><td>nick:</td><td><input type="text" name="p_changeable" maxlength="30" size="25"></td></tr>
				<tr><td>email:</td><td><input type="text" name="p_email" maxlength="150" size="25"></td></tr>
				<tr><td colspan="2" align="right"><input type="submit" value="sign me on..."></td></tr>
				</form>
			</table>
		</td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>And these are ready to rock:</td></tr>
		<tr><td>
			<table border="0">
			<tr>
				<td width="30">&nbsp;</td>
				<td width="150"><b>name:</b></td>
				<td width="150"><b>nick:</b></td>
				<td width="170"><b>email:</b></td>
			</tr>
	');

	open select_all;
	loop
		fetch select_all into v_email, v_name, v_changeable;
		exit when select_all%NOTFOUND;
		htp.p('
				<tr>
				<td align="right"><b>'||select_all%ROWCOUNT||'</b>&nbsp;&nbsp;</td>
				<td><nobr>'||v_name||'</nobr></td>
				<td><nobr>'||v_changeable||'</nobr></td>
				<td><nobr><a href="mailto:'||v_email||'">'||v_email||'</a></nobr></td>
				</tr>
		');
	end loop;
	close select_all;
	
	htp.p('
		</table>
		</td></tr>
		</table>
		</center>
		</body>
		</html>
	');	
	


END;
END user_reg;





/* -------------------- */
/* TEMPORARY PROCEDURES */
/* -------------------- */


procedure cs
is
begin
	user_reg(null,null,'Counter-Strike 29.01.03', null);
end;


procedure IHLodi03
is
begin
	user_reg(null,null,'IH Lodi 2003', null);
end;


PROCEDURE cancel
IS
BEGIN

	htp.p('
		<html>
		<head><title>Goodbye Party</title></head>
		<LINK HREF="http://klevstul.com/kstyle.css" REL="STYLESHEET" TYPE="text/css">
		<body bgcolor="#127510">
		<center>
		<table border="0" width="600" bgcolor="#00ff00" cellpadding="5">
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr><td colspan="2" align="center"><h1>GOODBYE-PARTY</h1> <h2>(eller au revoire..)</h2></td></tr>
		<tr><td colspan="2"><font size="2">

			Hei alle sammen,
			<br><br>
			Vi ser oss dessverre n�dt til � avlyse den planlagte festen l�rdag den 25, da for mye skjer p� for kort tid fremover! <br>
			(sannheten er at vi vil drikke opp barskapet selv..)
			<br><br>			
			Vi h�per vi ser de fleste av dere f�r vi drar likevel, og takker for den gode responsen..!<br>
			(vi skulle bare sjekke hvor mange venner vi har..)
			<br><br>
			Oppfordrer dessuten alle til � ta turen innom om dere by accident skulle v�re i Sydney-traktene i �ret som kommer.
			<br><br>
			Med �nske om et fortreffelig �r 2003!!
			<br><br>
			:)Kathrine og Frode


			</font>
		</td></tr>

		</table>
		</center>
		</body>
		</html>
	');	
	

END cancel;





-- ++++++++++++++++++++++++++++++++++++++++++++++ --

END; -- ends package body
/
show errors;

