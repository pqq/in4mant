set define off
PROMPT *** package: update_stock ***


CREATE OR REPLACE PACKAGE update_stock IS

	procedure run;
	procedure go
		(
			p_product_pk		in product.product_pk%type	default null,
			p_amount			in int						default null
		);

END;
/
show errors;



CREATE OR REPLACE PACKAGE BODY update_stock IS


---------------------------------------------------------
-- Name:        run
-- What:        starts the package
-- Author:      Frode Klevstul
-- Start date:  02.10.2003
---------------------------------------------------------
PROCEDURE run
IS
BEGIN
DECLARE

	v_time          date            default NULL;

BEGIN

    v_time := sysdate;

	
	htp.p('
		<html>
		<head><title>Klevstul Skiing Equipment</title></head>
		<LINK HREF="http://klevstul.com/kstyle.css" REL="STYLESHEET" TYPE="text/css">
		<body bgcolor="#ffffff">

		'|| v_time ||'

		</body>
		</html>
	');	

END;
END run;



---------------------------------------------------------
-- Name:        go
-- What:        show content of the xml file
-- Author:      Frode Klevstul
-- Start date:  02.10.2003
---------------------------------------------------------
PROCEDURE go
	(
		p_product_pk		in product.product_pk%type	default null,
		p_amount			in int						default null
	)
IS
BEGIN
DECLARE

	v_count 		int 								default null;
	v_supplier_pk	supplier.supplier_pk%type			default null;
	v_no_instock	supplier_product.no_instock%type	default null;
	v_orderlog_pk	orderlog.orderlog_pk%type			default null;
	v_productname	product.productname%type			default null;

BEGIN

	if (p_product_pk is null or p_amount is null) then
	htp.p('
		<html>
		<head><title>Klevstul Skiing Equipment</title></head>
		<LINK HREF="http://klevstul.com/kstyle.css" REL="STYLESHEET" TYPE="text/css">
		<body bgcolor="#ffffff">

		<form action="update_stock.go" method="GET">
		<table border="0" width="200">
		<tr><td>product_pk:</td><td><input type="text" name="p_product_pk" maxlength="4" size="10"></td></tr>
		<tr><td>amount:</td><td><input type="text" name="p_amount" maxlength="4" size="10"></td></tr>
		<tr><td colspan="2" align="right"><input type="submit" value="Submit"></td></tr>
		</table>
		</form>

		</body>
		</html>
	');	
	else

		select max(supplier_pk)
		into v_supplier_pk
		from supplier;

		select count(*) 
		into v_count
		from supplier_product
		where supplier_fk = v_supplier_pk
		and product_fk = p_product_pk
		and no_instock is not null;


		if (v_count > 0) then
			select no_instock
			into v_no_instock
			from supplier_product
			where supplier_fk = v_supplier_pk
			and product_fk = p_product_pk;

			v_no_instock := v_no_instock - p_amount;

			update supplier_product
			set no_instock = v_no_instock
			where supplier_fk = v_supplier_pk
			and product_fk = p_product_pk;
			commit;
			
			select	productname
			into	v_productname
			from	product
			where	product_pk = p_product_pk;
			
			select	orderlog_seq.nextval
			into	v_orderlog_pk
			from	dual;
			
			insert into orderlog
			(orderlog_pk, partner, product_pk, productname, amount, timestamp)
			values (v_orderlog_pk, 'FreePowder.com', p_product_pk, v_productname, p_amount, sysdate);
			commit;

			htp.p('Database updated: product_pk='||p_product_pk||' amount='||p_amount||'<br>');
			htp.p('In stock after update: '||v_no_instock||' items<br>');
			htp.p('<a href="update_stock.go">go back</a>');
		else
			htp.p('No product in database with product_pk='||p_product_pk||'<br>');
			htp.p('<a href="update_stock.go">go back</a>');
		end if;
		
	end if;

END;
END go;




-- ++++++++++++++++++++++++++++++++++++++++++++++ --

END; -- ends package body
/
show errors;

