set define off
PROMPT *** package: xml ***


CREATE OR REPLACE PACKAGE xml IS

	procedure run;
	procedure get
		(
			p_id			in int		default null
		);

END;
/
show errors;



CREATE OR REPLACE PACKAGE BODY xml IS


---------------------------------------------------------
-- Name:        run
-- What:        starts the package
-- Author:      Frode Klevstul
-- Start date:  04.09.2003
---------------------------------------------------------
PROCEDURE run
IS
BEGIN
DECLARE

	v_time          date            default NULL;

BEGIN

    v_time := sysdate;

	
	htp.p('
		<html>
		<head><title>Klevstul Skiing Equipment</title></head>
		<LINK HREF="http://klevstul.com/kstyle.css" REL="STYLESHEET" TYPE="text/css">
		<body bgcolor="#ffffff">

		'|| v_time ||'

		</body>
		</html>
	');	

END;
END run;



---------------------------------------------------------
-- Name:        get
-- What:        show content of the xml file
-- Author:      Frode Klevstul
-- Start date:  04.09.2003
---------------------------------------------------------
PROCEDURE get
	(
		p_id			in int		default null
	)
IS
BEGIN
DECLARE

	cursor c_brand is
	select brand_pk, brandname
	from brand;

	v_brand_pk 	brand.brand_pk%type default null;
	v_brandname	brand.brandname%type default null;

	cursor c_product is
	select product_pk, productname, brand_fk, cat_lev1, cat_lev2, cat_lev3
	from product;

	v_product_pk	product.product_pk%type		default null;
	v_productname	product.productname%type	default null;
	v_brand_fk	product.brand_fk%type		default null;
	v_cat_lev1	product.cat_lev1%type		default null;
	v_cat_lev2	product.cat_lev2%type		default null;
	v_cat_lev3	product.cat_lev3%type		default null;

	cursor c_product_specification is
	select product_fk, specification_fk, value
	from product_specification;

	v_product_fk		product_specification.product_fk%type		default null;
	v_specification_fk	product_specification.specification_fk%type	default null;
	v_value			product_specification.value%type		default null;

	cursor c_specification is
	select specification_pk, name, suffix
	from specification;

	v_specification_pk	specification.specification_pk%type		default null;
	v_name			specification.name%type				default null;
	v_suffix		specification.suffix%type			default null;

	cursor c_supplier is
	select supplier_pk, suppliername
	from supplier;

	v_supplier_pk		supplier.supplier_pk%type			default null;
	v_suppliername		supplier.suppliername%type			default null;

	cursor c_supplier_product is
	select supplier_fk, product_fk, no_instock, price
	from supplier_product;

	v_supplier_fk		supplier_product.supplier_fk%type		default null;
	v_no_instock		supplier_product.no_instock%type		default null;
	v_price			supplier_product.price%type			default null;

BEGIN

owa_util.mime_header('text/xml');

htp.p('<?xml version="1.0" encoding="ISO-8859-1"?>');
htp.p('<catalog>');

	open c_brand;
	loop
		fetch c_brand into v_brand_pk, v_brandname;
		exit when c_brand%NOTFOUND;
		htp.p('	<brand>');
		htp.p('		<brand_pk>'||v_brand_pk||'</brand_pk>');
		htp.p('		<brandname>'||v_brandname||'</brandname>');
		htp.p('	</brand>');
	end loop;
	close c_brand;

	open c_product;
	loop
		fetch c_product into v_product_pk, v_productname, v_brand_fk, v_cat_lev1, v_cat_lev2, v_cat_lev3;
		exit when c_product%NOTFOUND;
		htp.p('	<product>');
		htp.p('		<product_pk>'||v_product_pk||'</product_pk>');
		htp.p('		<productname>'||v_productname||'</productname>');
		htp.p('		<brand_fk>'||v_brand_fk||'</brand_fk>');
		htp.p('		<cat_lev1>'||v_cat_lev1||'</cat_lev1>');
		htp.p('		<cat_lev2>'||v_cat_lev2||'</cat_lev2>');
		htp.p('		<cat_lev3>'||v_cat_lev3||'</cat_lev3>');
		htp.p('	</product>');
	end loop;
	close c_product;

        open c_product_specification;
        loop
                fetch c_product_specification into v_product_fk, v_specification_fk, v_value;
                exit when c_product_specification%NOTFOUND;
                htp.p('	<product_specification>');
                htp.p('		<product_fk>'||v_product_fk||'</product_fk>');   
                htp.p('		<specification_fk>'||v_specification_fk||'</specification_fk>');
                htp.p('		<value>'||v_value||'</value>');
                htp.p('	</product_specification>');
        end loop;
        close c_product_specification;

        open c_specification;
        loop
                fetch c_specification into v_specification_pk, v_name, v_suffix;
                exit when c_specification%NOTFOUND;
                htp.p('	<specification>');
                htp.p('		<specification_pk>'||v_specification_pk||'</specification_pk>');
                htp.p('		<name>'||v_name||'</name>');
                htp.p('		<suffix>'||v_suffix||'</suffix>');
                htp.p('	</specification>');
        end loop;
        close c_specification;

        open c_supplier;
        loop
                fetch c_supplier into v_supplier_pk, v_suppliername;
                exit when c_supplier%NOTFOUND;
                htp.p('	<supplier>');
                htp.p('		<supplier_pk>'||v_supplier_pk||'</supplier_pk>');
                htp.p('		<suppliername>'||v_suppliername||'</suppliername>');
                htp.p('	</supplier>');
        end loop;
        close c_supplier;

        open c_supplier_product;
        loop
                fetch c_supplier_product into v_supplier_fk, v_product_fk, v_no_instock, v_price;
                exit when c_supplier_product%NOTFOUND;
                htp.p('	<supplier_product>');
                htp.p('		<supplier_fk>'||v_supplier_fk||'</supplier_fk>');
                htp.p('		<product_fk>'||v_product_fk||'</product_fk>');
                htp.p('		<no_instock>'||v_no_instock||'</no_instock>');
                htp.p('		<price>'||v_price||'</price>');
                htp.p('	</supplier_product>');
        end loop;
        close c_supplier_product; 


htp.p('</catalog>');

END;
END get;




-- ++++++++++++++++++++++++++++++++++++++++++++++ --

END; -- ends package body
/
show errors;

